#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 22 14:44:09 2016

@author: m160897
# =============================================================================
# """
# =============================================================================
#import argparse
import sys
import getpass
import os
from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import QObject
from six import *
import subprocess
from rilcontourlib.dataset.rcc_DatasetInterface import ProjectDatasetDefinition
from rilcontourlib.dataset.rcc_multiusermanager import MultiUserManager
from rilcontourlib.util.rcc_util import FileSaveThreadManager, MessageBoxUtil
from rilcontourlib.util.FileUtil import FileUtil
import wget
import pkg_resources
#from watchdog.observers.polling import PollingObserver
import gc
    
#import tempfile

"""
    ApplicationEventFilter

    Captures application wide keystrokes and stores up/down state of shift and control keys So keys states can be checked later.

        Key state saved using:   def setKeyEvent (self, key, pressed)
        Key state tested using:  def getKeyState (self, key)
"""

    

class ApplicationEventFilter (QObject) :
      def __init__ (self):
          QObject.__init__ (self)
          self._windowList = []
          self._projectDataset = []

      def eventFilter (self, source, event) :
          if (event.type () == QtCore.QEvent.ApplicationDeactivate) :
              for projects in self._projectDataset :
                  projects.setApplicationActivate (False)
          elif (event.type () == QtCore.QEvent.ApplicationActivate):
              for projects in self._projectDataset :
                  projects.setApplicationActivate (True)    


          if (len (self._projectDataset) > 0) :
              tempWindowList = []
              for mainWindow in self._windowList :
                  try :
                      mainWindow.isActiveWindow()
                  except :
                      tempWindowList.append (mainWindow)                      
                      print ("Main window unexpectedly closed")
              for  mainWindow in tempWindowList :
                  self._windowList.remove (mainWindow)
                      
                 
          if event.type() == QtCore.QEvent.Close :
                #print ("Close Event")
                if (not event.spontaneous() or event.isAccepted()) :
                    return False                
                if (len (self._projectDataset) > 0) :
                    for project in self._projectDataset :
                        if project.getProjectWindowFocus () :                                                        
                            project.closeProject ()
                            print ("Closing Project window")
                            project = None
                            return True                        
                    
                    for project in self._projectDataset :
                        activewindow = project.getActiveWindow ()
                        if (activewindow != None and activewindow.hasFocus ()) :
                            scanPhasedlg = project._projectDataset.getScanPhaseSelectionDlg ()
                            if (scanPhasedlg != None and activewindow == scanPhasedlg):
                                scanPhasedlg.close ()
                                return True
                            else:
                                for mainWindow in self._windowList :
                                    if (mainWindow == activewindow):
                                        mainWindow.close ()
                                        print ("Closing Main window")
                                        return True                    
                                
                else:
                    return True
          return False #QApplication.eventFilter(self, source, event)

      def addProjectDataset (self, ds):
          if (ds not in self._projectDataset) :
              self._projectDataset.append (ds)

      def removeProjectDataset (self, ds):
          if (ds in self._projectDataset) :
              self._projectDataset.append (ds)
          
      def addWindow (self, window) :
          if (window not in self._windowList) :
              self._windowList.append (window)

      def removeWindow (self, window):
          if (window in self._windowList) :
              self._windowList.remove (window)

class TestGitForRilContourUpdates ():
        @staticmethod        
        def _RemoveSpaces (lines):
            returnlines = []
            for line in lines :                
                temp = []
                subline = line.split (" ")
                for element in subline :
                    sub_str = element.strip ()
                    if (sub_str == '\\r') :
                        sub_str = ""
                    if (len (sub_str) > 0) :
                        temp.append (sub_str)
                if (len (temp) > 0) :
                    returnlines.append (temp)
            return returnlines  
        
        @staticmethod        
        def _getVersionNumber (name) :
             returnNum = []
             numList = name.split (".")
             for num in numList :
                returnNum.append (int (num))
             return tuple (returnNum)
            
        @staticmethod        
        def _isName (name) :            
            try :
               TestGitForRilContourUpdates._getVersionNumber (name)
               return False
            except :
                return True                
        
        @staticmethod        
        def _limitPackagesTo (lst, packageName) :
            returnLst = []    
            for line in lst :
                if line[len (line) - 1] == packageName :                    
                    returnLst.append (line[0:len (line) - 1])
            return returnLst
    
        @staticmethod        
        def getInstalledVersion () :
            try :
               return pkg_resources.get_distribution("rilcontour").version
            except :
               return None
            
        
        @staticmethod        
        def _isVersionGreater (oldV, newV) :
            versionLen = min (len (oldV), len (newV))
            for index in range (versionLen) :
                if (newV[index] > oldV[index]) :
                    return True
                elif (newV[index] < oldV[index]) :
                    return False                
            if (len (newV) > versionLen) :
                for index in range (versionLen, len (newV)) :
                    if newV[index] > 0 :
                        return True
            return False
                
        @staticmethod        
        def _getMostRecentVersion (lst):
            maxVersion = None
            for line in lst :                                
                for element in line :
                    if (element == "*" or element == ".") :
                        continue
                    if (TestGitForRilContourUpdates._isName (element)):
                        continue 
                    version = TestGitForRilContourUpdates._getVersionNumber (element)
                    break
                if (maxVersion == None) :
                    maxVersion = version
                elif (TestGitForRilContourUpdates._isVersionGreater (maxVersion, version)) :
                    maxVersion = version                                    
            return maxVersion
                
        @staticmethod
        def _executeCommand (command):
            proc=subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, )
            output=proc.communicate()[0]
            sout = str (output)
            strlines = TestGitForRilContourUpdates._RemoveSpaces (sout.split ("\\n"))
            found_first = -1
            found_last = -1
            for index, lines in enumerate (strlines) :                
                if (found_first == -1 and lines[0] == "rilcontour") :
                    found_first = index
                    found_last = index                  
                elif ((found_first != -1) and (lines[0] == "rilcontour")) :                    
                    found_last = index                    
                elif ((found_first != -1) and (lines[0] != "rilcontour")) :
                    break
            if (found_first == -1) :
                return (False, [])
            returnLst = []
            for index in range (found_first, found_last + 1) :                
                returnLst.append (strlines[index])
            return (True, returnLst)
        
        @staticmethod
        def _getVersionStr (version) :
            numLst = list (version)    
            numLst[len (numLst)-1] -= 1 #Hack package manager reports version -1
            strLst = []
            for num in numLst :
                strLst.append (str (num)) 
            return ".".join (strLst)
        
        @staticmethod
        def _removePackagesSoftware (lst, softwareName) :
            returnLst = []
            for line in lst :
                if (line[0] == softwareName) :
                   returnLst.append (line[1:])
                else:
                   returnLst.append (line)
            return returnLst
        
        @staticmethod
        def _testGitForUpdates () :
            installedVersion = TestGitForRilContourUpdates.getInstalledVersion ()
            if (installedVersion is None):
               return (False, "Unknown installed version", "")
            found, newlst = TestGitForRilContourUpdates._executeCommand ("conda search rilcontour")                        
            if (found) :                
                newlst = TestGitForRilContourUpdates._limitPackagesTo (newlst, "philbrik")
                newlst = TestGitForRilContourUpdates._removePackagesSoftware (newlst, "rilcontour")
                #newlst = TestGitForRilContourUpdates._removePackagesSoftware (newlst, ".")                                
                mostRecentVersion = TestGitForRilContourUpdates._getMostRecentVersion (newlst)
                if (mostRecentVersion is not None) :
                    mostRecentVersion = TestGitForRilContourUpdates._getVersionStr (mostRecentVersion)                    
                    if (TestGitForRilContourUpdates._isVersionGreater (installedVersion.split ("."), mostRecentVersion.split ("."))) :
                        return (True, installedVersion, mostRecentVersion)
                    else:
                        return (False, installedVersion, mostRecentVersion)
                else:
                    return (False, "Unknown max version", "")
                                
            else:
                return (False, "CheckInstalled", "")

        @staticmethod
        def testForSoftwareUpdates (VersionCheck = True):
            if not VersionCheck :
                print ("RIL-Contour update checking skipped.")
                return
            try:
                updateAvaialbe, installedVersion, currentGitVersion = TestGitForRilContourUpdates._testGitForUpdates ()
            except Exception as ex:
                msg = str (ex)
                print (msg)
                updateAvaialbe = False
                installedVersion = ""
                currentGitVersion = ""
            if (updateAvaialbe) :
                MessageBoxUtil.showMessage ("Software updates to rilcontour are available", "Rilcontour version " + currentGitVersion + " is avaiable for download.\nClick details for update instructions.", "Currently, rilcontour version: " + installedVersion + "is installed locally.\n\n Directions to upgrade to rilcontour version " + currentGitVersion + ".\n1) Quit rilcontour.\n2) In a terminal/console window execute the command 'conda update rilcontour'.\n 3) Following successful software update, restart rilcontour by executing the command 'rilcontour' in the terminal/console window.\n\nNote: If software installation fails during 'unlinking rilcontour' try issueing the upgraded command (conda update rilcontour) with administrator access.")
                print ("Software updates to rilcontour are availab; Installed version " + installedVersion + ", Git Version " + currentGitVersion  )
            elif (installedVersion != "" and currentGitVersion != "") :
                print ("Rilcontour software is up-to-date; Installed version " + installedVersion + ", Git Version " + currentGitVersion  )
            elif (installedVersion == "CheckInstalled") :
                print ("Could not detect rilcontour installation. Please check that rilcontour has been installed in Anaconda.")
            elif (installedVersion == "philbrik channel not detected") :
                print ("Error could not detect philbrik Anaconda channel. Unable to check for Rilcontour software updates.")
            elif (installedVersion == "Unknown installed version") :
                print ("Error could not detect rilcontour Anaconda installation.")
            elif (installedVersion == "Unknown max version") :
                print ("Error could not detect most current Anaconda version of Rilcontour.")
            else:
                print ("A error occured testing Rilcontour software for updates. Software update requires python Anaconda installation and an internet connection.")


def getBinaryResources (path, filelist) :
    try :
        if (not os.path.exists (path)) :
            os.makedirs (path)
            print ("Creating " + path)
        for file in filelist :        
            try:
                fpath = os.path.join (path, file)
                if (not os.path.exists (fpath)) :
                    url = "https://gitlab.com/Philbrick/rilcontour/raw/master/rilcontour/icons/" + file
                    wget.download(url, out=fpath)            
                    print ("Downloading " + file)
            except:
                print ("Could not download resource file: " + file)
    except:
        print ("Could not create: " + path)


        
    
def main ():    
    RILContourCommandLine = {}
    try :
        itemIndex = 0
        while itemIndex < len (sys.argv) :
            try :
                item = sys.argv[itemIndex]
                lowerItem = item.lower ()
                if lowerItem in ["-admin", "--admin"] :        
                    RILContourCommandLine["superuser"] = True
                if lowerItem in ["-administrator", "--administrator"] :        
                    RILContourCommandLine["superuser"] = True
                if lowerItem in ["-nocache", "--nocache"] :        
                    RILContourCommandLine["nocache"] = True
                if lowerItem in ["-safemode", "--safemode"] :        
                    RILContourCommandLine["safemode"] = True
                if lowerItem  in ["-noversion", "--noversion"] :        
                    RILContourCommandLine["noversion"] = True
                if lowerItem  in ["-superuser", "--superuser"] :        
                    RILContourCommandLine["superuser"] = True
                if lowerItem in ["-fileunlock", "--fileunlock"] :
                    RILContourCommandLine["fileunlock"] = True
                if lowerItem in ["-version", "--version"] :
                    RILContourCommandLine["printversion"] = True
                if lowerItem in ["-weaklocks", "--weaklocks"] :
                    RILContourCommandLine["weaklocks"] = True
                if lowerItem in ["-demo", "--demo"] :
                    RILContourCommandLine["demo"] = True
                if lowerItem in ["-forcereloadfscache", "--forcereloadfscache"] :
                    RILContourCommandLine["ReloadFileSystemCache"] = True
                if lowerItem in ["-projectpath", "--projectpath"] :
                    itemIndex += 1
                    if itemIndex < len (sys.argv) :
                        RILContourCommandLine["projectpath"] = sys.argv[itemIndex]
                if lowerItem in ["-niftisearchpath", "--niftisearchpath"] :
                    itemIndex += 1
                    if itemIndex < len (sys.argv) :
                        RILContourCommandLine["niftisearchpath"] = sys.argv[itemIndex]
                if lowerItem in ["-user", "--user"] :
                    itemIndex += 1
                    if itemIndex < len (sys.argv) :
                        RILContourCommandLine["user"] =  sys.argv[itemIndex]
                if lowerItem in ["-userhomepath", "--userhomepath", "--userhome", "-userhome"] :
                    itemIndex += 1
                    if itemIndex < len (sys.argv) :
                        RILContourCommandLine["userhomepath"] = sys.argv[itemIndex]
                if lowerItem in ["--roisearchpath", "-roisearchpath"] :
                    itemIndex += 1
                    if itemIndex < len (sys.argv) :
                        RILContourCommandLine["roirootdir"] = sys.argv[itemIndex]
                
                if lowerItem in ["--multiuseryaml", "-multiuseryaml"] :
                    itemIndex += 1
                    if itemIndex < len (sys.argv) :
                        RILContourCommandLine["multiuseryaml"] = sys.argv[itemIndex]
                        
                if lowerItem in ["-?", "--?", "--help", "-help"] :        
                    print ("")
                    print ("RIL-Contour commandline options:")
                    print ("")
                    print ("-- Define filesharing --------------")
                    print ("-MultiUserYaml [path] : YAML file describing multiuser file sharing settings.")
                    print ("")
                    print ("-- Override system settings --------")
                    print ("-User [user name]    : Alternative username to run RIL-Contour (Default: system username)")
                    print ("-UserHomepath [path] : Alternative homepath for user (Default: system's user home)")
                    print ("")
                    print ("-- Project to load at startup ------")
                    print ("-ProjectPath [path]     : RIL-Contour project to opened.")
                    print ("-NIfTISearchpath [path] : Root directory to load NIfTI source data.")
                    print ("-ROISearchpath [path]   : Root directory to load ROI data.")
                    print ("")
                    print ("-- File Lock Privlages -------------")
                    print ("-Admin : Allows locked files to be unlocked. Use cautiously.")
                    print ("")
                    print ("-- Other ---------------------------")
                    print ("-Help      : Help")        
                    print ("-NoCache   : Start RIL-Contour without using saved startup cache.")
                    print ("-NoVersion : Do not perform version checking at startup.")
                    #print ("-WeakLocks : Do not perform strong POSIX file locking. Not compatiable with multiuser.")
                    print ("-SafeMode  : Do not open previously opened datasets at startup.")
                    print ("-Version   : RIL-Contour version.")
                    print ("-ForceReloadFSCache : Force Reload FS-Cache. Slow for large projects")
                    print ("-Demo      : Disable saving")
                    return
                itemIndex += 1
            except:
                pass
    except:
        RILContourCommandLine["safemode"] = True
        
    #RILContourCommandLine["ReloadFileSystemCache"] = True
    #RILContourCommandLine["weaklocks"] = True
    RILContourCommandLine["superuser"] = True
    #RILContourCommandLine["noversion"] = True      #Disable version checking.  Bug bug re-enable
    #RILContourCommandLine["multiuseryaml"] = "~/test.yaml"
    #RILContourCommandLine["user"] = "alex"
    #RILContourCommandLine["userhomepath"] = "~/test_home/alex"
    #RILContourCommandLine["safemode"] = True
    #RILContourCommandLine["ReloadFileSystemCache"] = True
    #RILContourCommandLine["demo"] = True
        
        
    ReplaceTokens = {}
    if "user" in RILContourCommandLine :
        ReplaceTokens["<USERNAME>"] = RILContourCommandLine["user"]
    else:
        ReplaceTokens["<USERNAME>"] = getpass.getuser ()
    ReplaceTokens["<SYSTEM_USERNAME>"] = getpass.getuser ()
    resolveKeysList = ["projectpath", "niftisearchpath", "userhomepath", "roirootdir", "multiuseryaml"]
    for resolveKey in resolveKeysList :
        if resolveKey in RILContourCommandLine :
            argPath = RILContourCommandLine[resolveKey]
            TryMakeTerminalDir = resolveKey in ["userhomepath", "roirootdir"]
            RILContourCommandLine[resolveKey] = FileUtil.resolvePath (argPath, ReplaceTokens, TryMakeTerminalDir = TryMakeTerminalDir)
            if not os.path.exists ( RILContourCommandLine[resolveKey]) :
                print ("")
                print ("Error could not resolve path for: " + resolveKey)
                print ("")
                print ("path: " + RILContourCommandLine[resolveKey] )
                return
                
        
    multiuserFileSharingManager = None    
    if "multiuseryaml" in RILContourCommandLine :
        RILContourCommandLine["nocache"] = True        
        multiuserFileSharingManager = MultiUserManager (RILContourCommandLine["multiuseryaml"])
        if multiuserFileSharingManager is None or not multiuserFileSharingManager.didLoadFromFile ():
            print ("")
            print ("A error occured attempting to load: " + str (RILContourCommandLine["multiuseryaml"]))
            print ("")
            del RILContourCommandLine["multiuseryaml"]
    
        errors = multiuserFileSharingManager.validateYaml ()
        if len (errors) :
            print ("")
            print ("Warrning possible errors in command line multi-user YAML: " + str (RILContourCommandLine["multiuseryaml"]))
            print ("")
            for line in errors :
                print (line)
            return
    if "multiuseryaml" in RILContourCommandLine :
        if "user" in RILContourCommandLine and "userhomepath" not in RILContourCommandLine :
            print ("")
            print ("A error: if a non-system username is specified then an non-system home path must also be specified.")
            print ("")
            return
        
    if multiuserFileSharingManager is not None and multiuserFileSharingManager.isInitalized()  :        
            username = getpass.getuser ()
            if "user" in RILContourCommandLine :
                username = RILContourCommandLine["user"]
            if not "superuser" in RILContourCommandLine and not multiuserFileSharingManager.isUserDefined (username) :
                print ("")
                print ("A error: username %s is not included in the projects multiuser sharing yaml definition." % username)
                print ("")
                return
                
                
    RilcontourVersion = TestGitForRilContourUpdates.getInstalledVersion ()
    if (RilcontourVersion is None) :         
         RilcontourVersion = "Could not detect RIL-Contour version."    
      
    returnVal = 1
    startPath = os.getcwd ()
    path, filename = os.path.split (__file__)

    if "printversion" in RILContourCommandLine :
        print ("")
        print ("Installed RIL-Contour version: " + RilcontourVersion)
        print ("Running from: " + path)
        print ("Python version: " + sys.version)
        return    
    
    print ("rilcontour running from:" + path)
    sys.path.append  (path)
    RunningDir = path    #hack to correct for launching in package.
    getBinaryResources (os.path.join (RunningDir , "icons"), ["selectionarrow.png", "contour.png", "zoomin.png", "zoomout.png", "undo.png", "redo.png", "plus.png", "minus.png"])
    if (os.getcwd () == path):
        os.chdir ("..")
        os.chdir ("..")
    else:
        path, filename = os.path.split (path)
        if (os.getcwd () == path):
            os.chdir ("..")

    app = QApplication (sys.argv)
    app.setQuitOnLastWindowClosed (True)
    appeventfilter = ApplicationEventFilter ()
    app.installEventFilter(appeventfilter)
   
  
    TestGitForRilContourUpdates.testForSoftwareUpdates (VersionCheck = "noversion" not in RILContourCommandLine)     
    try :
        watchDogDirectoryObserver = None
        """try :
            watchDogDirectoryObserver = PollingObserver(1200)
            watchDogDirectoryObserver.start()
            print ("Watchdog Polling Observer Started")
        except:
            print ("Could not start watchdog directory monitor.")
            watchDogDirectoryObserver = None"""
                
        globalProjectList = []
        FileSaveThread = FileSaveThreadManager ()    
        try :
            datasetWindow = None
            ProjectDataset = ProjectDatasetDefinition (app,appeventfilter, globalProjectList, BasePath = RunningDir, ROISaveThread =  FileSaveThread,  watchDogDirectoryObserver = watchDogDirectoryObserver, RILContourCommandLine = RILContourCommandLine, MultiUserFileSharingManager = multiuserFileSharingManager)
            try:
                if ("safemode" in RILContourCommandLine and RILContourCommandLine["safemode"]):
                    ProjectDataset.initDefaultSettings ()
                elif (not ProjectDataset.initLastLoadedProject (RILContourCommandLine = RILContourCommandLine)) :
                    if ProjectDataset.isRunningFromCommandlineSepcifiedDataset () :
                        print ("A error occured starting RIL-Contour.")
                        return
                    else:
                        ProjectDataset.initDefaultSettings ()
                         
                if not ProjectDataset.isRunningFromCommandlineSepcifiedDataset () :
                    datasetWindow = ProjectDataset.getNiftiDatasetDlg ()        
                    datasetWindow.show ()
            
                returnVal = app.exec_()
                app.removeEventFilter(appeventfilter)
            finally:
                for project in globalProjectList :
                    try :
                        project.cleanupProject ()
                    except:
                        pass
            del datasetWindow
            del ProjectDataset        
            if (len (globalProjectList) != 0) :
                print ("Warrning: Projects remaining in project list")
            del globalProjectList
            gc.collect ()
        finally:
            FileSaveThread.closeAndDeleteThread ()
        del FileSaveThread 
        if (RILContourCommandLine is not None and "Temp_RIL_Path" in RILContourCommandLine) :
            try :
                os.remove (RILContourCommandLine["Temp_RIL_Path"])
            except:
                print ("Warrning could not remove temp file: " + str(RILContourCommandLine["Temp_RIL_Path"]))
                pass    
        del multiuserFileSharingManager
        del app
        gc.collect ()
    finally:
        if watchDogDirectoryObserver is not None :
            try :
                watchDogDirectoryObserver.stop ()
                #watchDogDirectoryObserver.join ()
                print ("Watchdog stopped")
            except:
                pass
        
    print ("Done!")
    os.chdir (startPath)
    return returnVal


if __name__ == "__main__" :    
    main ()
    try :
        sys.exit (0)
    except:
        pass

