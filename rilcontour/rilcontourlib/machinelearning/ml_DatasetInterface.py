import os
import tempfile
import shutil
try :    
    try :
        import cv2
        print ("Imported OpenCV")
    except:
        print ("OpenCV failed to import")
        raise        
    
    try :
        import tensorflow as tf            
        try :
            print  ("Imported tensorflow version: " + tf.version.VERSION)
        except:
            print ("Imported Tensorflow")        
        try :
            tf.compat.v1.disable_eager_execution()        
            print ("Eager mode disabled")
        except:
            pass
    except:
        print ("Tensorflow failed to import")
        raise
    try :
        if tf.version.VERSION[0] == '1' or tf.version.VERSION[0] == '0' :
            import keras     
            print ("Imported keras")
        else:
            import tensorflow.keras as keras        
            print ("Imported tensorflow.keras")
    except:    
        try:
            import keras              
            print ("Imported keras")
        except:            
            print ("Keras failed to import")
            raise
    Keras_AndTensorFlowLoaded = True    
except:
    Keras_AndTensorFlowLoaded = False

class SessionWrapper :
        
    @staticmethod 
    def get_session  (session = None) :
        try :
            if tf.version.VERSION[0] == '1' or tf.version.VERSION[0] == '0' :
                if session is None :
                    return keras.backend.get_session ()
                else:
                    return keras.backend.get_session (session)
            else:
                if session is None :
                    return tf.compat.v1.keras.backend.get_session ()
                else:
                    return tf.compat.v1.keras.backend.get_session (session)
        except:
            try:
                if session is None :
                    return tf.compat.v1.keras.backend.get_session ()
                else:
                    return tf.compat.v1.keras.backend.get_session (session)                
            except:
                if session is None :
                    return keras.backend.get_session ()
                else:
                    return keras.backend.get_session (session)                
    
    @staticmethod 
    def set_session  (session ) :
        try :
            if tf.version.VERSION[0] == '1' or tf.version.VERSION[0] == '0' :
                return keras.backend.set_session (session)
            else:                
                return tf.compat.v1.keras.backend.set_session (session)
        except:
            try:
                return tf.compat.v1.keras.backend.set_session (session)
            except:
                return keras.backend.set_session (session)

    @staticmethod 
    def get_tf_session () :
         try :
            if tf.version.VERSION[0] == '1' or tf.version.VERSION[0] == '0' :
                return tf.Session ()
            else:                
                return tf.compat.v1.Session()
         except:
            try:
                return tf.compat.v1.Session()
            except:
                return tf.Session ()

class TFObj :
    def __init__ (self, mlInterface) :        
        self._tfSession = None
        self._KerasModel = None
        self._MLInterface = mlInterface
        self._MLModelRetrained = False
        
    def clear (self) :        
        if Keras_AndTensorFlowLoaded :
            if (self._tfSession is not None) :  
                SessionWrapper.set_session (self._tfSession)
                keras.backend.clear_session ()
                self._tfSession = SessionWrapper.get_session ()
                SessionWrapper.set_session(self._MLInterface.getGetDefaultSession ())
            else:
                self._tfSession = SessionWrapper.get_tf_session ()
        self._KerasModel = None
        self._MLModelRetrained = False
        
    def setSession (self, session) :        
        self._tfSession = session
    
    def isLoaded (self) :
        return (self._KerasModel is not None) 
        
    def setKerasModel (self, kModel) :
        self._KerasModel = kModel

    def getKerasModel (self) :
        return self._KerasModel
        
    def getGraphAndSession (self) :
         return self._tfSession.graph, self._tfSession 
     
    def getSession (self) :        
        return self._tfSession 
    
    def setKerasModelRetrained (self) :
        self._MLModelRetrained = True
     
    def wasKerasModelRetrained (self):
        return self._MLModelRetrained
    
class ML_KerasModelLoaderInterface:
    def __init__ (self) :   
        if Keras_AndTensorFlowLoaded :
            try :
                config = tf.ConfigProto()
                config.gpu_options.allow_growth = True
                tf.Session(config=config)
            except:
                try :
                    try :
                        physical_devices = tf.config.list_physical_devices('GPU')
                    except:
                        physical_devices =  tf.config.experimental.list_physical_devices('GPU')
                    for device in physical_devices :
                        tf.config.experimental.set_memory_growth(device, True)
                except:
                    pass
            self._default_kerasSession =  SessionWrapper.get_session ()
        self._currentLoadedModelPath = None
        self._origionalModel = TFObj (self)
        self._linearModelModel = TFObj (self)
   
    def getGetDefaultSession (self) :
        return self._default_kerasSession
        
    @staticmethod
    def isMLSupportEnabled () :
        global Keras_AndTensorFlowLoaded
        return Keras_AndTensorFlowLoaded
    
    def closeOpenInterfaces (self) :
        self._currentLoadedModelPath = None        
        #keras.backend.clear_session ()
        #self._default_kerasSession =  keras.backend.get_session()
        if self._origionalModel is not None :            
            self._origionalModel.clear ()
        if self._linearModelModel is not None :
            self._linearModelModel.clear ()
        
    def buildCustomKerasObjects (self, text) :        
        try:
            self._CustomKerasObjectsDir = None            
            indent = " "
            buildCustomClass = []
            buildCustomClass += ["class CustomKerasObjects :"]
            textlines = text.split ("\n")
            for line in textlines :
                buildCustomClass += [indent + line]
            buildCustomClass += [""]
            buildCustomClass += ["self._CustomKerasObjectsDir = CustomKerasObjects()"]
            customobject = "\n".join (buildCustomClass)
            exec (customobject, globals (), locals ())     
            itemList = dir (self._CustomKerasObjectsDir)
            customObjDir = {}
            for item in itemList :
                if callable (getattr (self._CustomKerasObjectsDir, item)) :
                   if not item.startswith('__') and not item.endswith('__') :
                        method = self._CustomKerasObjectsDir.__getattribute__(item)
                        customObjDir[item] = method
            return customObjDir
        except:
            return {}    
        
    def getOrigionalModel (self, Current_HDF5_KerasModel_Path, Custom_Objects = None, ModelLoadExceptionList = None) :
         if (not ML_KerasModelLoaderInterface.isMLSupportEnabled ()) : 
            return None
         if (self._currentLoadedModelPath == Current_HDF5_KerasModel_Path and self._origionalModel.isLoaded ()) :
            return self._origionalModel.getKerasModel ()
         else:
            loadedModel = self.loadKerasModel  (Current_HDF5_KerasModel_Path, Custom_Objects = Custom_Objects, ModelLoadExceptionList = ModelLoadExceptionList)
            return loadedModel.getKerasModel ()
        
    def loadKerasModel (self, Current_HDF5_KerasModel_Path, Custom_Objects = None, LinearOutput = False, ModelLoadExceptionList=None, OnlyLoadCustomModelLoader = False):
        if (not ML_KerasModelLoaderInterface.isMLSupportEnabled ()) : 
            return None
        if (self._currentLoadedModelPath == Current_HDF5_KerasModel_Path) :
            if not LinearOutput and self._origionalModel.isLoaded () :                
                return self._origionalModel
            elif LinearOutput and self._linearModelModel.isLoaded () :
                return self._linearModelModel   
        try :
            if (self._currentLoadedModelPath != Current_HDF5_KerasModel_Path or not self._origionalModel.isLoaded ()) :
                if self._origionalModel is not None :                    
                    self._origionalModel.clear ()
                if self._linearModelModel is not None :                    
                    self._linearModelModel.clear ()                              
                session1 = self._origionalModel.getSession ()
                with session1.graph.as_default():                                                        
                    SessionWrapper.set_session(session1)                
                    with session1.as_default():                                                            
                        keras.backend.set_learning_phase (False) 
                        if Custom_Objects is not None and len (Custom_Objects) > 0:
                            if ("CustomModelLoader" in Custom_Objects) :
                                try :
                                    kModel = Custom_Objects["CustomModelLoader"](Current_HDF5_KerasModel_Path)
                                except Exception as exp:
                                    print ("Model Loading Exception: " + str (exp))
                                    if ModelLoadExceptionList is not None :
                                        ModelLoadExceptionList.append (str (exp))
                                    kModel = keras.models.load_model (Current_HDF5_KerasModel_Path, custom_objects = Custom_Objects)
                            else:        
                                kModel = keras.models.load_model (Current_HDF5_KerasModel_Path, custom_objects = Custom_Objects)
                        else:
                            kModel = keras.models.load_model (Current_HDF5_KerasModel_Path)
                        keras.backend.set_learning_phase (False)
                        self._origionalModel.setSession (session1)
                        self._origionalModel.setKerasModel (kModel)
                self._currentLoadedModelPath  = Current_HDF5_KerasModel_Path      
                SessionWrapper.set_session(self._default_kerasSession)
            if not LinearOutput :
                return self._origionalModel
            elif not OnlyLoadCustomModelLoader :
                origionalModel = self._origionalModel.getKerasModel ()
                LastLayer = origionalModel.layers[-1]                
                oldActivation = LastLayer.activation
                LastLayer.activation = keras.activations.linear                    
                self._linearModelModel = self._loadTemporaryKerasModel (origionalModel, Custom_Objects = Custom_Objects, origionalModelPath = self._currentLoadedModelPath)
                LastLayer.activation = oldActivation
                return self._linearModelModel 
            else:
                origionalModel = self._origionalModel.getKerasModel ()
                self._linearModelModel = self._loadTemporaryKerasModel (origionalModel, Custom_Objects = Custom_Objects, origionalModelPath = self._currentLoadedModelPath, OnlyLoadCustomModelLoader = OnlyLoadCustomModelLoader)
                return self._linearModelModel 
        except Exception as exp:            
            print ("Model Loading Exception: " + str (exp))
            if ModelLoadExceptionList is not None :
                ModelLoadExceptionList.append (str (exp))
            #keras.backend.clear_session ()
            #self._default_kerasSession =  keras.backend.get_session()
            if self._origionalModel is not None :
                self._origionalModel.clear ()
            if self._linearModelModel is not None :
                self._linearModelModel.clear ()
            raise
        return None
    
    def loadKerasModelFromSource (self, Current_HDF5_KerasModelWeight_Path, CreateModelFunction, LoadWeights = True):
        if (not ML_KerasModelLoaderInterface.isMLSupportEnabled ()) : 
            return None
        if (self._currentLoadedModelPath == Current_HDF5_KerasModelWeight_Path) :
            if self._origionalModel.isLoaded () :                
                return self._origionalModel
        ModelLoadExceptionList = []
        try :
            if (self._currentLoadedModelPath != Current_HDF5_KerasModelWeight_Path or not self._origionalModel.isLoaded ()) :
                if self._origionalModel is not None :
                    self._origionalModel.clear ()
                if self._linearModelModel is not None :
                    self._linearModelModel.clear ()                              
                session1 = self._origionalModel.getSession ()
                with session1.graph.as_default():                                                        
                    SessionWrapper.set_session(session1)                
                    with session1.as_default():                                                            
                        keras.backend.set_learning_phase (False) 
                        try :
                            kModel = CreateModelFunction (Current_HDF5_KerasModelWeight_Path, LoadWeights = LoadWeights)
                        except :
                            kModel = CreateModelFunction (Current_HDF5_KerasModelWeight_Path)
                        keras.backend.set_learning_phase (False)
                        self._origionalModel.setSession (session1)
                        self._origionalModel.setKerasModel (kModel)
                self._currentLoadedModelPath  = Current_HDF5_KerasModelWeight_Path      
                SessionWrapper.set_session(self._default_kerasSession)
            return self._origionalModel
        except Exception as exp:            
            print ("Model Loading Exception: " + str (exp))
            if ModelLoadExceptionList is not None :
                ModelLoadExceptionList.append (str (exp))
            #keras.backend.clear_session ()
            #self._default_kerasSession =  keras.backend.get_session()
            if self._origionalModel is not None :
                self._origionalModel.clear ()
            if self._linearModelModel is not None :
                self._linearModelModel.clear ()
            raise
        return None
    
    def clearKerasModelIfLoaded (self, Current_HDF5_KerasModel_Path) :
        if (self._currentLoadedModelPath == Current_HDF5_KerasModel_Path) :             
            #keras.backend.clear_session ()
            #self._default_kerasSession =  keras.backend.get_session()
            if self._origionalModel is not None :
                self._origionalModel.clear ()
            if self._linearModelModel is not None :
                self._linearModelModel.clear ()
            self._currentLoadedModelPath = None            
     
        
         
    def _loadTemporaryKerasModel (self, model, Custom_Objects = None, origionalModelPath = None, OnlyLoadCustomModelLoader = False):
        if (not ML_KerasModelLoaderInterface.isMLSupportEnabled ()) : 
            return None
        """Applies modifications to the model layers to create a new Graph. For example, simply changing
        `model.layers[idx].activation = new activation` does not change the graph. The entire graph needs to be updated
        with modified inbound and outbound tensors because of change in layer building function.
        Args:
            model: The `keras.models.Model` instance.
        Returns:
            The modified model with changes applied. Does not mutate the original `model`.
        """
       
        # The strategy is to save the modified model and load it back. This is done because setting the activation
        # in a Keras layer doesnt actually change the graph. We have to iterate the entire graph and change the
        # layer inbound and outbound nodes with modified tensors. This is doubly complicated in Keras 2.x since
        # multiple inbound and outbound nodes are allowed with the Graph API.
        
        RemoveModelPath = True
        SaveModelPath = True
        
        if origionalModelPath is not None :
            linearPath = origionalModelPath.split (".")
            linearPath[-1] = "lhdf5" 
            model_path = ".".join (linearPath)
            RemoveModelPath = False
            if os.path.exists (model_path) :
                SaveModelPath  = False
            else:
                SaveModelPath  = True
                try:
                    shutil.copyfile (origionalModelPath, model_path) 
                except:
                    try:
                        os.remove (model_path)
                    except:
                        pass
                    modelhandle, model_path = tempfile.mkstemp(suffix='temp_keras_model', prefix = ".hdf5")
                    os.close (modelhandle)
                    RemoveModelPath = True
        else:            
            modelhandle, model_path = tempfile.mkstemp(suffix='temp_keras_model', prefix = ".hdf5")
            os.close (modelhandle)
            RemoveModelPath = True
            
        oldSession = SessionWrapper.get_session ()
        try:
            
            if (SaveModelPath) :
                graph1, session1 = self._origionalModel.getGraphAndSession ()
                with graph1.as_default():
                    SessionWrapper.set_session (session1) 
                    with session1.as_default():   
                        keras.backend.set_learning_phase (False) 
                        #hack to save weights, errored with uninitalized variables error.  
                        #* saves default weights: model.get_weights ()
                        #* calls session1.run(tf.global_variables_initializer()) to initalize all remaining variables
                        #* calls set weights to restore weights
                        #* saves model.
                        tempWeights = model.get_weights ()
                        session1.run(tf.global_variables_initializer())
                        model.set_weights (tempWeights)
                        print ("Saving Keras Model: " + model_path)
                        model.save(model_path)
            
            print ("Loading Keras Model: " + model_path)
            session1 = self._linearModelModel.getSession ()
            with session1.graph.as_default():       
                SessionWrapper.set_session(session1)
                with session1.as_default():
                     keras.backend.set_learning_phase (False) 
                     if Custom_Objects is not None and len (Custom_Objects) > 0:
                            if ("CustomModelLoader" in Custom_Objects) :
                                try :
                                    try :
                                        kModel = Custom_Objects["CustomModelLoader"](origionalModelPath, LoadLinearModel = True)                                        
                                    except:
                                        kModel = Custom_Objects["CustomModelLoader"](model_path)
                                except:                                    
                                    if OnlyLoadCustomModelLoader :
                                        kModel = None
                                    else:
                                        kModel = keras.models.load_model (model_path, custom_objects = Custom_Objects)                                    
                            else:        
                                if OnlyLoadCustomModelLoader :
                                    kModel = None
                                else:
                                    kModel = keras.models.load_model (model_path, custom_objects = Custom_Objects)
                     else:
                         if OnlyLoadCustomModelLoader :
                             kModel = None
                         else:
                             kModel = keras.models.load_model (model_path)
                     if kModel is not None :
                         keras.backend.set_learning_phase (False)                              
                         self._linearModelModel.setSession (session1)
                         self._linearModelModel.setKerasModel (kModel)
                         return self._linearModelModel
                     return None
        finally:
            SessionWrapper.set_session(oldSession)
            if (RemoveModelPath) :
                os.remove(model_path)
                print ("Removing Keras Model: " + model_path)