#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 10 17:57:20 2019

@author: KennethPhilbrick
"""
import skimage
import tensorflow as tf
if tf.version.VERSION[0] == '1' or tf.version.VERSION[0] == '0' :
    import keras
    from keras.layers import Input, Conv2D, MaxPooling2D, concatenate, Conv2DTranspose
    from keras.models import Model
    from keras import optimizers
else:
    try:
        import tensorflow.keras as keras
        from tensorflow.keras.layers import Input, Conv2D, MaxPooling2D, concatenate, Conv2DTranspose
        from tensorflow.keras.models import Model
        from tensorflow.keras import optimizers
    except:
        import keras
        from keras.layers import Input, Conv2D, MaxPooling2D, concatenate, Conv2DTranspose
        from keras.models import Model
        from keras import optimizers
    
from rilcontourlib.machinelearning.ml_DatasetInterface import SessionWrapper
import numpy as np
from six import *
import os
import shutil
#from kpml.dataset.imaging.augmentation.jitter import Jitter
from kpml.dataset.imaging.augmentation.scale import Scale
from scipy import ndimage
import random 
import math
import json
from PyQt5.QtCore import pyqtSignal, QObject
from skimage.transform import resize  
from rilcontourlib.util.FileUtil import ScanDir


class CashedDeepGrowModel :
    def __init__ (self, ROIUID, DeepGrowModelRepositoryPath, basemodelPath) :
        try:
            self._ROIUID = ROIUID
            try :
                if not os.path.exists (DeepGrowModelRepositoryPath) :
                    os.mkdir (DeepGrowModelRepositoryPath)
            except :
                pass
            self._ROIDGModel = os.path.join (DeepGrowModelRepositoryPath,str (self._ROIUID))
            try :
                if not os.path.exists (self._ROIDGModel) :
                    os.mkdir (self._ROIDGModel)
            except :
                pass
            try:
                fileIterator = ScanDir.scandir(self._ROIDGModel)
                for fobj in fileIterator :
                    if fobj.name.endswith (".h5") :
                        self._ROIDGModel = fobj.path
                        return      
            finally:
                try:
                    fileIterator.close ()
                except:
                    pass
                      
            self._ROIDGModel = os.path.join (self._ROIDGModel,"dg_weights_0.h5")
            shutil.copyfile (basemodelPath, self._ROIDGModel)
            self.setModelVersion (0)                
                
        except :
            self._cleanup ()
    
    def getModelVersion (self) :
        try :
            dirname,_ = os.path.split (self._ROIDGModel)
            versionfile = os.path.join (dirname,"version.txt")
            with open (versionfile,"rt") as infile:
                versionNumber = json.loads (infile.read ())
            return versionNumber
        except:
            return 0

    def setModelPath (self, newPath) :
        if os.path.exists (self._ROIDGModel) :
            try :
                os.remove (self._ROIDGModel)
            except:
                pass
        self._ROIDGModel = newPath
            
        
    def isBaselineModel (self) :
        return (self.getModelVersion () == 0)
        
    def setModelVersion (self, versionNumber) :
        dirname,_ = os.path.split (self._ROIDGModel)
        versionfile = os.path.join (dirname,"version.txt")
        try :        
            with open (versionfile,"wt") as outfile :
                   outfile.write (json.dumps (versionNumber))             
        except:
            self._cleanup ()
        
    def deleteModel (self) :
        self._cleanup () 
        
    def _cleanup (self) :
        if self._ROIDGModel is not None :
            dgDir,_ = os.path.split (self._ROIDGModel)
            try:
                try:
                    fileIterator =  ScanDir.scandir(dgDir)
                    for fobj in fileIterator :                        
                        try :
                            os.remove (fobj.path)
                        except :
                            pass
                finally:
                    try:
                        fileIterator.close ()
                    except:
                        pass
                try:
                    os.rmdir (dgDir)
                except:
                    pass
            except:
                pass
        self._ROIUID = None
        self._ROIDGModel = None
    
    def getROIUID (self) :
        return self._ROIUID 
    
    def hasModel (self) :
        return self._ROIDGModel is not None and self._ROIUID is not None and os.path.exists (self._ROIDGModel)
    
    def getModelPath (self) :
        return self._ROIDGModel

class UIUpdate (QObject, keras.callbacks.Callback) :
    
        TrainingStatus = pyqtSignal(['PyQt_PyObject','PyQt_PyObject'])  
        
        def __init__ (self,batchPerEpoch, epochMax) :
            super (UIUpdate,self).__init__()
            self._epochCounter = int (1)
            self._batch = int (1)
            self._batchPerEpoch = int (batchPerEpoch)
            self._maxEpoch = int (epochMax)
            self._TotalBatch = float (self._maxEpoch * self._batchPerEpoch)
            self._TotalBatchCount = 1.0
            
        def on_batch_end(self, batch, logs={}):
            loss = float (logs['loss'])
            if self._batchPerEpoch > 1 :
                epochLog = "Epoch: %d/%d, Batch: %d/%d, Loss: %0.4f" %   (self._epochCounter, self._maxEpoch, self._batch, self._batchPerEpoch, loss)
            else:
                epochLog = "Epoch: %d/%d, Loss: %0.4f" %   (self._epochCounter, self._maxEpoch, loss)
            precentDone = int (100.0* self._TotalBatchCount / self._TotalBatch)
            self.TrainingStatus.emit (epochLog, precentDone)    
            self._batch += 1
            self._TotalBatchCount += 1.0
        
        def on_epoch_end(self, epoch, logs=None):
            self._epochCounter += 1
            self._batch = int (1)
    
class DeepGrowROIModelInterface :
    
    class DataGenerator (keras.utils.Sequence) :
        def __init__ (self, BatchSize, TrainingData):
            self._BatchSize = int (BatchSize)
            self._TrainingData = TrainingData
            self._IndexList = []
            self._ErosionMask = []
            structure = np.ones ((3,3),dtype=np.uint8)
            for index in range  (len (self._TrainingData)) :
                image, mask = self._TrainingData[index]
                maxDim = max (image.shape[0],image.shape[1])
                if (maxDim != 512) :
                    scalefactor = 512.0 / float (maxDim)  
                    resizeShape = (min(int (scalefactor * float(image.shape[0])),512), min(int (scalefactor * float(image.shape[1])), 512))
                    image = resize (image, resizeShape, anti_aliasing = True, preserve_range = True, mode = "reflect", order = 1)
                    mask = resize (mask, resizeShape, anti_aliasing = False, preserve_range = True, mode = "reflect", order = 0)   
                image = Scale.centerClip (image, (512,512), 0) 
                mask = Scale.centerClip (mask, (512,512), 0) 
                    
                label, features = ndimage.label (mask,structure)
                for featureIndex in range (1, features + 1) :
                    connectedMask = (label == features).astype (np.float32)
                    self._TrainingData[index] = (image, connectedMask)
                    self._ErosionMask.append (DeepGrowROIModelInterface.DataGenerator.getOptimalErosionMask (self._TrainingData[index][1]))
        
        @staticmethod
        def getXYMinMax (sliceData) :
           xMin = None
           xMax = None
           yMin = None
           yMax = None
           for x in range (sliceData.shape[0]) :
               if np.any (sliceData[x,:]) :
                   xMin = x
                   break
           if (xMin is None):
               return (xMin, yMin, xMax, yMax)
           for x in range (sliceData.shape[0]-1,-1,-1) :
               if np.any (sliceData[x,:]) :
                   xMax = x
                   break
           for y in range (sliceData.shape[1]-1,-1,-1) :
               if np.any (sliceData[:,y]) :
                   yMax = y
                   break
           for y in range (sliceData.shape[1]) :
               if np.any (sliceData[:,y]) :
                   yMin = y
                   break
           return (xMin, yMin, xMax, yMax)
       
        @staticmethod
        def getOptimalErosionMask (mask):
            data = mask > 0
            xMin, yMin, xMax, yMax = DeepGrowROIModelInterface.DataGenerator.getXYMinMax (data)
            clippedData = data[xMin:xMax+1, yMin:yMax + 1]
            for count in range (10) :
               erosion = ndimage.morphology.binary_erosion (clippedData)
               if np.sum (erosion) > 25 :
                   clippedData = erosion
               else:
                   break
            data[xMin:xMax+1, yMin:yMax + 1] = clippedData       
            return data 
            
        def __len__ (self) :
            return int (max(math.ceil (float (len (self._TrainingData)) / float (self._BatchSize)), 1))
        
        def __getitem__ (self, indx) :            
            TrainingData = np.zeros ((self._BatchSize, 512,512, 2), dtype = np.float32)
            PredictionData = np.zeros ((self._BatchSize, 512,512, 1), dtype = np.float32)
            for batchIndex in range (self._BatchSize) :
                if len (self._IndexList) <= 0 :
                    self._IndexList = list (range (len (self._TrainingData)))
                    random.shuffle (self._IndexList)
                index = self._IndexList.pop ()
                inputData, predictionMask = self._TrainingData[index]
                inputData = inputData.astype (np.float32)
                inputData -= np.mean (inputData)
                inputData /= np.std (inputData)
        
                xPos, yPos = self._ErosionMask[index].nonzero ()
                Mask = np.zeros ((512, 512), dtype = np.float32)
                index = random.randint (0,len (xPos)-1)
                xPos = xPos[index]
                yPos = yPos[index]
                Mask [xPos, yPos] = 1.0
                Mask = ndimage.filters.gaussian_filter(Mask, 10.0) * 10000.0
                
                #dx, dy = random.randint (-5,5), random.randint (-5,5)
                #angle = random.randrange (-5,5)    
                #scale = random.randrange (-5,5)            
                #scale = float (100 + scale)/100.0           
                
                #inputData = ndimage.interpolation.rotate (inputData, angle, mode ="constant", reshape=True, cval=0, order=1)
                #Mask = ndimage.interpolation.rotate (Mask, angle, mode ="constant", reshape=True, cval=0, order=1)
                #predictionMask = ndimage.interpolation.rotate (predictionMask, angle, mode ="constant", reshape=True, cval=0, order=0)
                
                #Mask = Jitter.jitter (Mask, dx, dy, mode="constant", cval = 0) 
                #inputData = Jitter.jitter (inputData, dx, dy, mode="constant", cval = 0) 
                #predictionMask = Jitter.jitter (predictionMask, dx, dy, mode="constant", cval = 0) 
                
                #Mask = Scale.scale (Mask, scale, mode="min", order = 1) 
                #inputData = Scale.scale (inputData, scale, mode="min", order = 1) 
                #predictionMask = Scale.scale (predictionMask, scale, mode="min", order = 0) 
             
               
                                
                TrainingData[batchIndex,:,:,0] = inputData.astype (np.float32)
                TrainingData[batchIndex,:,:,1] = Mask
                PredictionData[batchIndex,:,:,0] = predictionMask.astype (np.float32)

            return TrainingData, PredictionData
    
    
        
        
    def __init__ (self, HomePath, PythonProjectBasePath) :
        basemodelPath = os.path.join (PythonProjectBasePath, "models", "dg1_weights.h5")
        DeepGrowModelRepositoryPath = os.path.join (HomePath, "dgroi_models")
        self._baseModelPath = basemodelPath
        self._DeepGrowModelRepositoryPath = DeepGrowModelRepositoryPath
        self._ROIModels = {}
        try :
            if not os.path.exists (DeepGrowModelRepositoryPath) :
                    os.mkdir (DeepGrowModelRepositoryPath)
        except :
            pass
    
    def clearAllDGModels (self) :
        try:
            PathQueue = [self._DeepGrowModelRepositoryPath]
            removeDirList = []
            while len (PathQueue) > 0 :
                try :
                    path = PathQueue.pop ()                
                    filelist = os.listdir (path)
                    for filename in filelist :
                        filePath = os.path.join (path,filename)
                        if os.path.isdir (filePath):
                            PathQueue.append (filePath)
                            removeDirList.append (filePath)
                        else:
                            try:
                                os.remove (filePath)                            
                            except:
                                print ("Could not remove file: " + filePath)
                except:
                    pass            
            removeDirList.reverse ()
            for dirPath in removeDirList  :
                try :
                    os.rmdir (dirPath)
                except:
                    print ("Could not remove dir: " + dirPath)
        except:
            pass
            
                
    def getROIModel (self, ROIUID):
        if ROIUID not in self._ROIModels :
            self._ROIModels[ROIUID] = CashedDeepGrowModel (ROIUID, self._DeepGrowModelRepositoryPath, self._baseModelPath)
        if not self._ROIModels[ROIUID].hasModel () :
            del self._ROIModels[ROIUID]
            return None
        return self._ROIModels[ROIUID]
    
    def deleteROIModel (self, ROIUID) :
        self.getROIModel (ROIUID)
        if ROIUID in self._ROIModels :
            try :
                self._ROIModels[ROIUID].deleteModel ()
            except:
                pass
            del self._ROIModels[ROIUID]
    
    @staticmethod
    def _diceloss(y_true, y_pred):
        import tensorflow as tf
        # Calculates the precision
        _epsilon = 10 ** -7
        # Determine axes to pass to tf.reduce_sum
        # Calculate intersections and unions per class
        intersections = tf.reduce_sum(y_true * y_pred)
        unions = tf.reduce_sum(y_true + y_pred)
        dice_scores = (2.0 * intersections + _epsilon) / (unions + _epsilon)  # Calculate Dice scores per class
        return 1.0 - dice_scores  
    

    def _progressCallback (self, msg, precentDone) :
        if self._ProgressDialog is not None :
            self._ProgressDialog.setLabelText (msg)
            self._ProgressDialog.setValue (precentDone)
            if self._App is not None :
               self._App.processEvents ()
               
    def trainModel (self, ROIUID, trainingDataList, MLInterface, BatchSize, ProgressDialog = None, App = None) :
        try :
           
            dgROI_Model = self.getROIModel (ROIUID)
            if dgROI_Model is None :
                return False
            path = dgROI_Model.getModelPath ()
            if not os.path.exists (path) :
                return False 
            predictionModel = MLInterface.loadKerasModelFromSource (path, DeepGrowMLInterface._modelLoadFunction, LoadWeights = False)
            graph, session = predictionModel.getGraphAndSession ()
            defaultSession = SessionWrapper.get_session()
            try:
                self._ProgressDialog = ProgressDialog
                self._App = App
                with graph.as_default():
                    SessionWrapper.set_session(session)
                    with session.as_default():
                        try :
                            model = predictionModel.getKerasModel ()    
                            for layerIndex in range (1,7) :
                                model.layers[layerIndex].trainable = False
                            model.compile (optimizers.Adam (lr=0.0001), loss = DeepGrowROIModelInterface._diceloss)         
                            model.load_weights (path)       
                            dataGenerator = DeepGrowROIModelInterface.DataGenerator (BatchSize, trainingDataList)      
                            
                            pathDir, pathname = os.path.split (path)                            
                            version = int (dgROI_Model.getModelVersion ())
                            version += 1
                            path = os.path.join (pathDir, "dg_weights_%d.h5" % version)
                            checkPointCallback = keras.callbacks.ModelCheckpoint(path, monitor='loss', verbose=0, save_best_only=True, save_weights_only=True, mode='auto', period=1)
                            progressBarCallback = UIUpdate (len (dataGenerator), 5)
                            progressBarCallback.TrainingStatus.connect (self._progressCallback)
                            model.fit_generator (generator= dataGenerator,use_multiprocessing = False, workers = 1, shuffle = True, verbose = 1, epochs = 5, callbacks = [checkPointCallback, progressBarCallback])      
                            
                            if os.path.exists (path) :
                                dgROI_Model.setModelVersion (version)
                                dgROI_Model.setModelPath (path)                                                                               
                                print ("Saving Model " + path)
                                return True
                            return False
                        except Exception as exc:                                 
                            print ("Model Prediction Exception: " + str (exc))
                            self.deleteROIModel (ROIUID)
                            return False
            finally:
                SessionWrapper.set_session(defaultSession)
                del self._ProgressDialog
                del self._App
        except:
            return False
    
class DeepGrowMLInterface :
    
    @staticmethod
    def _modelLoadFunction (weightPath, LoadWeights = True):
        channels = 2
        inputs = Input((512, 512, channels))
        conv1 = Conv2D(16, (3, 3), activation='relu', padding='same')(inputs)
        conv1 = Conv2D(16, (3, 3), activation='relu', padding='same')(conv1)
        pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)
    
        conv2 = Conv2D(32, (3, 3), activation='relu', padding='same')(pool1)
        conv2 = Conv2D(32, (3, 3), activation='relu', padding='same')(conv2)
        pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)
    
        conv3 = Conv2D(64, (3, 3), activation='relu', padding='same')(pool2)
        conv3 = Conv2D(64, (3, 3), activation='relu', padding='same')(conv3)
        pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)
    
        conv4 = Conv2D(128, (3, 3), activation='relu', padding='same')(pool3)
        conv4 = Conv2D(128, (3, 3), activation='relu', padding='same')(conv4)
        pool4 = MaxPooling2D(pool_size=(2, 2))(conv4)
    
        conv45 = Conv2D(256, (3, 3), activation='relu', padding='same')(pool4)
        conv45 = Conv2D(256, (3, 3), activation='relu', padding='same')(conv45)
        pool45 = MaxPooling2D(pool_size=(2, 2))(conv45)
    
        conv55 = Conv2D(512, (3, 3), activation='relu', padding='same')(pool45)
        conv55 = Conv2D(512, (3, 3), activation='relu', padding='same')(conv55)
        pool55 = MaxPooling2D(pool_size=(2, 2))(conv55)
    
        conv56 = Conv2D(1024, (3, 3), activation='relu', padding='same')(pool55)
        conv56 = Conv2D(1024, (3, 3), activation='relu', padding='same')(conv56)
        
        up54 = concatenate([Conv2DTranspose(512, (2, 2), strides=(2, 2), padding='same')(conv56), conv55], axis=3)
        conv57 = Conv2D(512, (3, 3), activation='relu', padding='same')(up54)
        conv57 = Conv2D(512, (3, 3), activation='relu', padding='same')(conv57)
        
        up55 = concatenate([Conv2DTranspose(256, (2, 2), strides=(2, 2), padding='same')(conv57), conv45], axis=3)
        conv58 = Conv2D(256, (3, 3), activation='relu', padding='same')(up55)
        conv58 = Conv2D(256, (3, 3), activation='relu', padding='same')(conv58)
    
        up6 = concatenate([Conv2DTranspose(128, (2, 2), strides=(2, 2), padding='same')(conv58), conv4], axis=3)
        conv6 = Conv2D(128, (3, 3), activation='relu', padding='same')(up6)
        conv6 = Conv2D(128, (3, 3), activation='relu', padding='same')(conv6)
    
        up7 = concatenate([Conv2DTranspose(64, (2, 2), strides=(2, 2), padding='same')(conv6), conv3], axis=3)
        conv7 = Conv2D(64, (3, 3), activation='relu', padding='same')(up7)
        conv7 = Conv2D(64, (3, 3), activation='relu', padding='same')(conv7)
    
        up8 = concatenate([Conv2DTranspose(32, (2, 2), strides=(2, 2), padding='same')(conv7), conv2], axis=3)
        conv8 = Conv2D(32, (3, 3), activation='relu', padding='same')(up8)
        conv8 = Conv2D(32, (3, 3), activation='relu', padding='same')(conv8)
    
        up9 = concatenate([Conv2DTranspose(16, (2, 2), strides=(2, 2), padding='same')(conv8), conv1], axis=3)
        conv9 = Conv2D(16, (3, 3), activation='relu', padding='same')(up9)
        conv9 = Conv2D(16, (3, 3), activation='relu', padding='same')(conv9)
    
        conv10 = Conv2D(1, (1, 1), activation='sigmoid')(conv9)
    
        model = Model(inputs=[inputs], outputs=[conv10])
        if LoadWeights :
            model.load_weights (weightPath)
        return model
    
    @staticmethod
    def getModelVersion (DeepGrowROIModels, ROIUID) :
        try :
            if DeepGrowROIModels is not None and ROIUID is not None :
                model = DeepGrowROIModels.getROIModel (ROIUID)
                if model is not None :
                    path = model.getModelPath ()
                    if os.path.exists (path) :
                        return model.getModelVersion ()
        except:
            pass
        return None
                        
    @staticmethod
    def predictDeepGrowSegementation (SliceData, Mask, MLInterface, PythonProjectBasePath, DeepGrowROIModels = None, ROIUID = None) :
        Current_HDF5_KerasModelWeight_Path = None
        try :
            if DeepGrowROIModels is not None and ROIUID is not None :
                model = DeepGrowROIModels.getROIModel (ROIUID)
                if model is not None :
                    path = model.getModelPath ()
                    if os.path.exists (path) :
                        Current_HDF5_KerasModelWeight_Path = path
        except :
            Current_HDF5_KerasModelWeight_Path = None
        if Current_HDF5_KerasModelWeight_Path is None :   
            Current_HDF5_KerasModelWeight_Path = os.path.join (PythonProjectBasePath, "models", "dg1_weights.h5")
        #print("loadingModel " + Current_HDF5_KerasModelWeight_Path)
        if not os.path.exists (Current_HDF5_KerasModelWeight_Path) :
            print ("Deep grow model not found.")
            print ("")
            print (Current_HDF5_KerasModelWeight_Path)
            return None
        #if input dimensions are > 512 x 512 
        #shrink to fit largest dimension at 512 x 512
        #pad image with 0 to fit to 512x512
        if SliceData.shape != Mask.shape :
            print ("Shapes of slice data and mask do not match")
            return None
        
        SliceData = SliceData.astype (np.float)
        SliceData -= np.mean (SliceData)
        SliceData /= np.std (SliceData)
      
        OrigionalImageSize = None
        ImagePadded = None
        if SliceData.shape[0] != 512 or SliceData.shape[1] != 512 :
            newShape = None
            if SliceData.shape[0] > SliceData.shape[1] and SliceData.shape[0] != 512 :
                scaleFactor = 512.0/float (SliceData.shape[0])
                newShape = (512, int (scaleFactor * SliceData.shape[1]))
            elif  SliceData.shape[1] != 512 :
                scaleFactor = 512.0/float (SliceData.shape[1])
                newShape = (int (scaleFactor * SliceData.shape[0]), 512)
            if newShape is not None :
                OrigionalImageSize = SliceData.shape
                SliceData = skimage.transform.resize (SliceData, newShape, mode = "constant", cval = 0, preserve_range = True, anti_aliasing = True, clip = True, order = 1)
                Mask = skimage.transform.resize (Mask, newShape, mode = "constant", cval = 0, preserve_range = True, anti_aliasing = True, clip = True, order = 1)
            
            if SliceData.shape[0] != 512 or SliceData.shape[1] != 512 :
                padxl =  int ((512 - SliceData.shape[0]) / 2)
                padxr =  int (512 - SliceData.shape[0] - padxl)
                padyl =  int ((512 - SliceData.shape[1]) / 2)
                padyr =  int (512 - SliceData.shape[1] - padyl)
                Mask = np.pad (Mask , [[padxl,padxr],[padyl,padyr]], mode = "constant", constant_values = 0) 
                SliceData = np.pad (SliceData , [[padxl,padxr],[padyl,padyr]], mode = "constant", constant_values = 0) 
                ImagePadded = (padxl, padxr, padyl, padyr)
        
        predictionModel = MLInterface.loadKerasModelFromSource (Current_HDF5_KerasModelWeight_Path, DeepGrowMLInterface._modelLoadFunction)
        modelInput = np.zeros ((1,512,512, 2), dtype=np.float32)
        modelInput[0,:,:,0] = np.swapaxes (SliceData, 0, 1)
        modelInput[0,:,:,1] = np.swapaxes (Mask, 0, 1)  * 10000.0
     
        graph, session = predictionModel.getGraphAndSession ()
        defaultSession = SessionWrapper.get_session()
        predictionMask = None
        try:
            with graph.as_default():
                SessionWrapper.set_session(session)
                with session.as_default():
                    try :
                        model = predictionModel.getKerasModel ()                                        
                        predictionMask = model.predict (modelInput)[0]                    
                    except Exception as exc:                                 
                        print ("Model Prediction Exception: " + str (exc))
                        predictionMask = None
                        pass
        finally:
            SessionWrapper.set_session(defaultSession)
            
        if predictionMask is None :
            return None
        
        prediction = np.swapaxes (predictionMask[:,:,0], 0, 1) 
        if ImagePadded is not None :
            padxl, padxr, padyl, padyr = ImagePadded
            prediction = prediction[padxl:(prediction.shape[0] - padxr), padyl:(prediction.shape[1] - padyr)]
        if OrigionalImageSize is not None :
             prediction = skimage.transform.resize (prediction, OrigionalImageSize, mode = "constant", cval = 0, preserve_range = True, anti_aliasing = True, clip = True, order = 1)
        return (prediction >= 0.5).astype (np.uint8)

