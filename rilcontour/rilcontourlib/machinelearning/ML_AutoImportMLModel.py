#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug  5 15:25:33 2018

@author: KennethPhilbrick
"""

import os
from PyQt5 import QtCore
from PyQt5.QtWidgets import   QProgressDialog
from rilcontourlib.ui.RCC_ML_ModelTree import ML_ModelFileIO, ML_ModelVersionNode, ML_ModelTree
from rilcontourlib.ui.RCC_ML_ModelImportDlg import RCC_ModelImportDlg
from rilcontourlib.util.rcc_util import  FileInterface, FileObject
from rilcontourlib.util.DateUtil import DateUtil, TimeUtil
from rilcontourlib.util.FileUtil import ScanDir

class ML_AutoImportMLModel :
    def __init__ (self, ProjectDataset) :
        self._ProjectDataset = ProjectDataset
        self._loadFromFile ()
    
    def resetLog (self) :
        self._ImportTxtLog = []
        self._ImportFileLog = {}
        self._saveToFile ()
         
    def resetModel (self) :
        self._IsEnabled = False
        self._DirPath = ""
        self._ImportTxtLog = []
        self._LastRunTxt = "never"
        self._ImportFileLog = {}
        self._saveToFile ()
    
    def appendTextLog (self, lineList, Date = False) :
        if len (lineList) <= 0 :
            return
        if len (self._ImportTxtLog) > 0 :
            self._ImportTxtLog.append ("<hr>")
        dateStr = DateUtil.dateToString (DateUtil.today ())
        timeStr = TimeUtil.timeToString(TimeUtil.getTimeNow ())
        AutoImportedDateTimeStr = dateStr + " at " +timeStr
        self._ImportTxtLog.append ("<b>Date Time: </b>"+AutoImportedDateTimeStr+"<br>")
        for line in lineList :
            self._ImportTxtLog.append (line + "<br>")
        
        
    def setEnabled (self, val):
        if val != self._IsEnabled :
            self._IsEnabled = val
            self._saveToFile ()
    
    def _initFromFileObj (self, fobj) :
        self._IsEnabled = fobj.getParameter ("IsEnabled")
        self._DirPath = fobj.getParameter ("DirPath")
        self._ImportTxtLog = fobj.getParameter ("ImportTxtLog")
        self._LastRunTxt = fobj.getParameter ("LastRunTxt")
        self._ImportFileLog = fobj.getParameter ("ImportFileLog")
        
    def getFileObject (self) :
        fobj = FileObject ("AutoImportMLSettings")                        
        fobj.setParameter ("IsEnabled", self._IsEnabled)
        fobj.setParameter ("DirPath", self._DirPath)
        fobj.setParameter ("ImportTxtLog", self._ImportTxtLog)
        fobj.setParameter ("LastRunTxt", self._LastRunTxt)
        fobj.setParameter ("ImportFileLog", self._ImportFileLog)
        return fobj
        
    def _saveToFile (self) :
       homepath = self._ProjectDataset.getMLHomeFilePath ()
       autoimportfilepath = os.path.join (homepath, "AutoimportMLSettings.txt")
       try :
           fobj = FileInterface ()             
           fobj.addFileObjects (self.getFileObject ())            
           fobj.writeDataFile (autoimportfilepath)      
       except :
           self.appendTextLog (["A unexpected error occured saving the ML auto import settings file to:", "   " + autoimportfilepath], Date = True)
           
        
    def _loadFromFile (self) :
        self._ImportTxtLog = []
        homepath = self._ProjectDataset.getMLHomeFilePath ()
        if (homepath is not None and os.path.isdir (homepath) and os.path.exists (homepath)) :
            try :
                autoimportfilepath = os.path.join (homepath, "AutoimportMLSettings.txt")
                if os.path.isfile (autoimportfilepath) and os.path.exists (autoimportfilepath) :
                     f = FileInterface ()
                     f.readDataFile (autoimportfilepath)       
                     for fileobj in f.getFileObjects () :
                          if fileobj.getFileObjectName () == "AutoImportMLSettings" :     
                              self._initFromFileObj (fileobj)
                              break
                else:
                    self.resetModel ()
            except : 
                self._IsEnabled = False
                self.resetModel ()
                self.appendTextLog (["A unexpected error occured reading the ML auto import settings file."], Date = True)
        else:
            self._IsEnabled = False
            self.appendTextLog (["Error: Could not access MLModel home path at " + str (homepath)], Date = True)
        return True
   
    def _autoImportMLModel (self, mlModelDef, hdf5Path, modelPath, modelLastChanged, MLTreeModel, ImportLog) :
        AutoImported = (DateUtil.today (), TimeUtil.getTimeNow ())
        if (RCC_ModelImportDlg.importModel (mlModelDef, hdf5Path, self._ProjectDataset.getMLHomeFilePath (), AutoImported = AutoImported)) :
            self._ImportFileLog[modelPath] = modelLastChanged
            dateStr = DateUtil.dateToString (AutoImported[0])
            timeStr = TimeUtil.timeToString(AutoImported[1])
            AutoImportedDateTimeStr = dateStr + " at " +timeStr
            ImportLog.append ("Imported " + mlModelDef.getModelName () + " @ " + AutoImportedDateTimeStr)
            return True
        return False
    
    def _ignoreImportMLFile (self, msg, mlModelDef, hdf5Path, modelPath, modelLastChanged, ImportLog) :
         self._ImportFileLog[modelPath] = modelLastChanged
         dateStr = DateUtil.dateToString (DateUtil.today ())
         timeStr = TimeUtil.timeToString(TimeUtil.getTimeNow ())
         AutoImportedDateTimeStr = dateStr + " at " +timeStr
         ImportLog.append (msg + mlModelDef.getModelName () + " @ " + AutoImportedDateTimeStr)
         
    def runNow (self, MLTreeModel, parent = None):
        ImportLog = []
        ModelImported = False
        dateImported = DateUtil.today ()
        timeImported = TimeUtil.getTimeNow ()
        dateStr = DateUtil.dateToString (dateImported)
        timeStr = TimeUtil.timeToString(timeImported)
        self._LastRunTxt = dateStr + " at " +timeStr
        if self._IsEnabled :
            if os.path.isdir (self._DirPath) and os.path.exists (self._DirPath) :                
                progdialog = QProgressDialog(parent = parent)
                progdialog.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
                progdialog.setMinimumDuration (5)
                progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)                
                progdialog.setWindowTitle ("Auto-importing ML Models")            
                progdialog.setWindowModality(QtCore.Qt.ApplicationModal)                
                progdialog.setRange(0,0)                
                progdialog.setMinimumWidth(300)
                progdialog.setMinimumHeight(100)
                try:                    
                    progdialog.forceShow()   
                    progdialog.setValue (0)
                    self._ProjectDataset.getApp ().processEvents ()
                    filePairListFound = []
                    try:                     
                        fileIterator = ScanDir.scandir(self._DirPath)
                        for fobj in fileIterator :                            
                            self._ProjectDataset.getApp ().processEvents ()
                            if (fobj.name.lower ().endswith (".hdf5")) :                                
                                hdf5_filepath = fobj.path
                                model_filepath = hdf5_filepath[:-len (".hdf5")] + ".model"
                                if (os.path.isfile (model_filepath) and os.path.exists (model_filepath)) :
                                    try :
                                        timeLastChanged = os.path.getmtime (model_filepath)
                                        if (model_filepath not in self._ImportFileLog) or timeLastChanged  > self._ImportFileLog[model_filepath] :
                                            filePairListFound.append ((hdf5_filepath, model_filepath, timeLastChanged))
                                    except:
                                        pass
                    finally:
                        try:
                            fileIterator.close ()
                        except:
                            pass
                    progdialog.setRange(0,len (filePairListFound))
                    for importModelIndex, modeltuple in enumerate(filePairListFound) :
                        progdialog.setValue (importModelIndex)
                        self._ProjectDataset.getApp ().processEvents ()
                        hdf5Path, modelPath, modelLastChanged = modeltuple
                        try :   
                            mlModelDef = ML_ModelFileIO.loadModelDef (modelPath, MLTreeModel.getProjectDataset ())
                            if (not MLTreeModel.doesModelDefinitionOrTreeNodeExist (mlModelDef)) :
                                if (self._autoImportMLModel (mlModelDef, hdf5Path, modelPath, modelLastChanged, MLTreeModel, ImportLog)) :
                                    ML_ModelTree.createTreeNode (MLTreeModel, Model = mlModelDef, HandleDuplicateVersion="Replace", ProjectDataset = self._ProjectDataset)
                                    ModelImported = True
                            else:
                                existing_MLModelDef = MLTreeModel.getExistingTreeNodeModelDef (mlModelDef)
                                if (existing_MLModelDef.isAutoImportedModel ()):
                                    existingModelVersion = existing_MLModelDef.getModelDescriptionLog ().getVersion ()
                                    existingModelVersionCreationDateTime = existing_MLModelDef.getModelDescriptionLog ().getSortableCreationDateTime ()
                                    
                                    candiateModelVersion = mlModelDef.getModelDescriptionLog ().getVersion ()
                                    candiateModelVersionCreationDateTime = mlModelDef.getModelDescriptionLog ().getSortableCreationDateTime ()
                                    
                                    existingModelVersionTestNode = ML_ModelVersionNode (existingModelVersion, None, None)
                                    candiateModelVersionTestNode = ML_ModelVersionNode (candiateModelVersion, None, None)
                                    
                                    if (existingModelVersionTestNode.isVersionLessThan (candiateModelVersionTestNode)) :
                                        if (self._autoImportMLModel (mlModelDef, hdf5Path, modelPath, modelLastChanged, MLTreeModel, ImportLog)) :
                                            ML_ModelTree.createTreeNode (MLTreeModel, Model = mlModelDef, HandleDuplicateVersion="Replace" , ProjectDataset = self._ProjectDataset)
                                            ModelImported = True
                                    elif (existingModelVersionTestNode.isVersionEqual (candiateModelVersionTestNode) and existingModelVersionCreationDateTime < candiateModelVersionCreationDateTime) :
                                        if (self._autoImportMLModel (mlModelDef, hdf5Path, modelPath, modelLastChanged, MLTreeModel, ImportLog)) :
                                            ML_ModelTree.createTreeNode (MLTreeModel, Model = mlModelDef, HandleDuplicateVersion="Replace" , ProjectDataset = self._ProjectDataset)
                                            ModelImported = True
                                    else:
                                        self._ignoreImportMLFile ("Model import skipped. Current model was created more recently or has a greater version number.", mlModelDef, hdf5Path, modelPath, modelLastChanged, ImportLog)
                                else:
                                    self._ignoreImportMLFile ("Model import skipped. Current model was created/or edited manually.", mlModelDef, hdf5Path, modelPath, modelLastChanged, ImportLog)
                        except : 
                            ImportLog.append ("")
                            ImportLog.append ("A unexpected error occured processing: ")
                            ImportLog.append ("     HDF5: " + hdf5Path)
                            ImportLog.append ("    model: " + modelPath)
                            ImportLog.append ("")
                    
                finally:                    
                    progdialog.close ()
                    self._ProjectDataset.getApp ().processEvents ()
                self._saveToFile ()
            else:
                ImportLog.append ("Could not find auto-import directory: " + self._DirPath)
            
            self.appendTextLog (ImportLog, Date = True)
        return ModelImported 
        
        
    def getLog (self) :
        return "<br>".join (self._ImportTxtLog)
    
    def getLastRun (self) :
        return self._LastRunTxt
    
    def isEnabled (self) :
        return self._IsEnabled
    
    def getDirectory (self) :
        return self._DirPath
    
    def setDirectory (self, val):
        if val != self._DirPath :
            self._DirPath = val
            self._saveToFile ()