from rilcontourlib.util.rcc_util import FileObject, Tag, MessageBoxUtil
from rilcontourlib.util.DateUtil import DateUtil, TimeUtil
from rilcontourlib.util.PandasCompatibleTable import PandasCompatibleTable
import skimage
from rilcontourlib.dataset.roi.rcc_roi import ROIDef
import numpy as np
import os
import math
import copy
from rilcontourlib.util.rcc_util import  FileInterface
import tensorflow as tf
try:
    if tf.version.VERSION[0] == '1' or tf.version.VERSION[0] == '0' :
        import keras     
    else:
        import tensorflow.keras as keras
except:
    import keras    
from skimage import transform
from skimage.filters import gaussian
import cv2
import sys
from scipy.stats import rankdata
from rilcontourlib.machinelearning.ml_DatasetInterface import SessionWrapper
            
class ML_PredictionPreferedViewUtil :
    @staticmethod 
    def getPreferedSliceViewAndSliceSelection (PreferedViewList, ContourWindow)  :
        NiftiVolume = ContourWindow.NiftiVolume
        if NiftiVolume is None :
            return None, None
        AxisDict = {}
        if PreferedViewList is not None and len (PreferedViewList) > 0 :
            if "Axial" in PreferedViewList :                
                AxisDict["Axial"] = NiftiVolume.getDisplayAxis ("Axial")
            if "Coronal" in PreferedViewList :                
                AxisDict["Coronal"] = NiftiVolume.getDisplayAxis ("Coronal")
            if "Sagittal" in PreferedViewList :                
                AxisDict["Sagittal"] = NiftiVolume.getDisplayAxis ("Sagittal")            
        if len (AxisDict) == 0 :
            AxisDict["Axial"] = NiftiVolume.getDisplayAxis ("Axial")
        selectedSliceView = None               
        XYSliceView = ContourWindow.ui.XYSliceView
        YZSliceView = ContourWindow.ui.YZSliceView
        XZSliceView = ContourWindow.ui.XZSliceView
        for sliceview in [XYSliceView, YZSliceView, XZSliceView] :
            for key in AxisDict.keys () :
                if sliceview.getSliceAxis () == AxisDict[key][0]  :
                   if selectedSliceView is None or sliceview.isSelected () or (not selectedSliceView.isSelected () and sliceview is XYSliceView) :
                       selectedSliceView = sliceview    
        
        if selectedSliceView.getSliceAxis () == "Z" :
            sliceSelector = ContourWindow.ui.XYSliceSelectorWidget 
        elif selectedSliceView.getSliceAxis () == "X" :
            sliceSelector = ContourWindow.ui.YZSliceVerticalSelector
        else:
            sliceSelector = ContourWindow.ui.XZSliceVerticalSelector
        
        return selectedSliceView, sliceSelector
        
class ML_KerasModel :
    
    def __init__ (self, ProjectDataset) :        
        self._Origional_HDF5_KerasModel_Path = None
        self._Current_HDF5_KerasModel_Path = None
        self._customKerasPythonCode = None
        self._customKerasObj = None
        self._ProjectDataset = ProjectDataset        
     
    def setCustomObjectPythonCode (self, customCode) :
        if (customCode != self._customKerasPythonCode) :
            self._customKerasPythonCode = customCode
            self._customKerasObj = None
       
        
    def getCustomObjectPythonCode (self) :
        return self._customKerasPythonCode
        
    def getProjectDataset (self) :
        return self._ProjectDataset 
    
    def getFileObject (self) :
        fobj = FileObject ("ML_KerasModel")        
        fobj.setParameter ("Current_HDF5_KerasModel_Path", self._Current_HDF5_KerasModel_Path)
        fobj.setParameter ("Origional_HDF5_KerasModel_Path", self._Origional_HDF5_KerasModel_Path)
        fobj.setParameter ("CustomPythonObjCode", self._customKerasPythonCode)        
        return fobj

    def copy (self) :
        newModel = ML_KerasModel (self._ProjectDataset)
        newModel._Origional_HDF5_KerasModel_Path = self._Origional_HDF5_KerasModel_Path
        newModel._Current_HDF5_KerasModel_Path = self._Current_HDF5_KerasModel_Path
        newModel._customKerasPythonCode = self._customKerasPythonCode        
        return newModel
        
    @staticmethod 
    def loadFromFileObj (fobj, ProjectDataset) :     
        ml = ML_KerasModel (ProjectDataset)
        ml._Current_HDF5_KerasModel_Path = fobj.getParameter ("Current_HDF5_KerasModel_Path")    
        ml._Origional_HDF5_KerasModel_Path = fobj.getParameter ("Origional_HDF5_KerasModel_Path")    
        if fobj.hasParameter ("CustomPythonObjCode") :  
            ml._customKerasPythonCode  = fobj.getParameter ("CustomPythonObjCode")    
        else:
            ml._customKerasPythonCode = None        
        return ml

    def getHtml (self) :            
            html = ["Origional keras model: " +  self._Origional_HDF5_KerasModel_Path]
            html += ["Model currently stored: " +  self._Current_HDF5_KerasModel_Path]                
            if self._customKerasPythonCode is not None and len (self._customKerasPythonCode.strip ()) > 0 :
                html += ["Custom keras pythoncode:"]            
                linelist = self._customKerasPythonCode.split ("\n") 
                for line in linelist :
                    html += [line]            
                
            return "<br>".join (html)

    def getOrigionalModelPath (self) :
        return self._Origional_HDF5_KerasModel_Path
    
    def getCurrentModelPath (self) :
        return self._Current_HDF5_KerasModel_Path
    
    def setOrigionalModelPath (self, path) :
        if (self._Origional_HDF5_KerasModel_Path != path) :
                self._Origional_HDF5_KerasModel_Path = path
                return True
        return False
    
    def setModelPath (self, path) :
        if (self._Current_HDF5_KerasModel_Path != path) :
            self._ProjectDataset.getKerasModelLoader().clearKerasModelIfLoaded (self._Current_HDF5_KerasModel_Path)            
            self._Current_HDF5_KerasModel_Path = path
            return True
        return False

                     
  
    def getKerasModelForInspection (self, LinearOutput = None) :
        if (self._Current_HDF5_KerasModel_Path is None or not os.path.isfile (self._Current_HDF5_KerasModel_Path)) :
            return None
        else:
            if self._customKerasObj is None :
                self._customKerasObj = self._ProjectDataset.getKerasModelLoader().buildCustomKerasObjects (self._customKerasPythonCode)
            try :
                #custom_objects = self.buildCustomKerasObjects ()
                if LinearOutput is not None :
                     try :
                         return self._ProjectDataset.getKerasModelLoader().loadKerasModel (self._Current_HDF5_KerasModel_Path,  self._customKerasObj, LinearOutput = LinearOutput).getKerasModel ()
                     except:
                         pass
                return self._ProjectDataset.getKerasModelLoader().getOrigionalModel (self._Current_HDF5_KerasModel_Path,  self._customKerasObj)
            except:
                print ("A Error occured trying to load the keras model." )
        return None
        
    def canCustomLoadLinearKerasModelFromSource (self) :
        if (self._Current_HDF5_KerasModel_Path is None or not os.path.isfile (self._Current_HDF5_KerasModel_Path)) :
            return False
        else:
            if self._customKerasObj is None :
                self._customKerasObj = self._ProjectDataset.getKerasModelLoader().buildCustomKerasObjects (self._customKerasPythonCode)
            try :
                if self._customKerasObj is not None and "CustomModelLoader" in self._customKerasObj :                                            
                    return self._ProjectDataset.getKerasModelLoader().loadKerasModel (self._Current_HDF5_KerasModel_Path,  self._customKerasObj, LinearOutput = True, OnlyLoadCustomModelLoader=True) is not None
                return False
            except:
                print ("A Error occured trying to load the keras model." )
        return False
    
    def getKerasModelEnviroment (self, LinearOutput=False) :
        if (self._Current_HDF5_KerasModel_Path is None or not os.path.isfile (self._Current_HDF5_KerasModel_Path)) :
            return None
        else:
            if self._customKerasObj is None :
                self._customKerasObj = self._ProjectDataset.getKerasModelLoader().buildCustomKerasObjects (self._customKerasPythonCode)
            try :
                #custom_objects = buildCustomKerasObjects ()
                return self._ProjectDataset.getKerasModelLoader().loadKerasModel (self._Current_HDF5_KerasModel_Path,  self._customKerasObj, LinearOutput)
            except:
                print ("A Error occured trying to load the keras model." )
        return None

    def modelPredict (self, modelInput, LinearOutput=False, ReturnErrorMsg = False) :
        if (modelInput is None) :
            return None
        kerasModelEnv = self.getKerasModelEnviroment (LinearOutput=LinearOutput) 
        if kerasModelEnv is None :
            return None
        graph, session = kerasModelEnv.getGraphAndSession ()
        
        prediction = None            
        defaultSession = SessionWrapper.get_session()
        ErrorMsg = None
        try:
            with graph.as_default():
                SessionWrapper.set_session(session)
                with session.as_default():
                    try :
                        model = kerasModelEnv.getKerasModel ()                                        
                        prediction = model.predict (modelInput)                    
                    except Exception as exc:               
                        ErrorMsg = str (exc)
                        print ("Model Prediction Exception: " + ErrorMsg)
        finally:
            SessionWrapper.set_session(defaultSession)
        if ReturnErrorMsg :
            return prediction, ErrorMsg
        return prediction        
    
    def modelTrain (self, modelInput, modelInputGenerator, LoopCount) :
        if (modelInput is None) :
            return None
        kerasModelEnv = self.getKerasModelEnviroment (LinearOutput=False) 
        if kerasModelEnv is None :
            return None
        graph, session = kerasModelEnv.getGraphAndSession ()
        defaultSession = SessionWrapper.get_session()
        try:
            with graph.as_default():
                SessionWrapper.set_session(session)
                with session.as_default():
                    try :
                        model = kerasModelEnv.getKerasModel ()  
                        for _ in range (LoopCount) :
                            result_tuple = modelInputGenerator (modelInput, LoopCount)
                            if len (result_tuple) >= 2 :
                               x, y = result_tuple[0], result_tuple[1]
                               sample_weight = None
                               if len (result_tuple) >= 3 :
                                   sample_weight =  result_tuple[2]
                               model.train_on_batch(x, y, sample_weight=sample_weight, class_weight=None)
                        kerasModelEnv.setKerasModelRetrained ()
                    except:                                 
                        pass
        finally:
            SessionWrapper.set_session(defaultSession)    
    
    def getModelInputShape (self) :
        kerasModel = self.getKerasModelForInspection () 
        if kerasModel is None :
            return None
        return kerasModel.input_shape

    def getModelOutputShape (self) :
        kerasModel = self.getKerasModelForInspection () 
        if kerasModel is None :
            return None
        return kerasModel.output_shape


class ML_ModelInputTransformation  :
    def __init__ (self, AffineTransformation = None, resizeMode = "Shrink (preserve dim) & Crop",  constantValue= 0, MissingSliceMode = "Constant", PredictOnPatch = False) :
        if (AffineTransformation is None) :
            self._2DAffineTransformation = np.eye (2)      
        else:
            self._2DAffineTransformation = np.copy (AffineTransformation)                    
        self._resizeMode = resizeMode
        self._shapeScalingMode = "reflect"
        self._constantValue = constantValue
        self._missingSliceMode = MissingSliceMode
        self._AppliedTransformationStack = []
        self._PredictOnPatch = PredictOnPatch
        self._PatchX, self._PatchY = 0, 0
        self._offAxisClipMask = None
        
    def isPredictOnPatchSet (self) :
        return self._PredictOnPatch
    
    def setPatchPredictionCoordinates (self, cX, cY) :
        self._PatchX, self._PatchY = cX, cY
        
    def getConstantFillValue (self) :
        return self._constantValue
    
    def getHtml (self) :
            html = ["<table><tr><td>Affine transformation:"]
            html += ["</td><td><table><tr><td>" +  str(self._2DAffineTransformation[0,0]) + "</td>","<td>" +  str(self._2DAffineTransformation[0,1]) + "</td></tr>"]
            html += ["<tr><td>" +  str (self._2DAffineTransformation[1,0]) + "</td>","<td>" +  str(self._2DAffineTransformation[1,1]) + "</td></tr></table></td></tr></table>"]
            html += ["<br>"]
            if (self._PredictOnPatch) :
                html += ["Predict on patch: True<br>"]
            else:
                html += ["Predict on patch: False<br>"]
            html += ["Shape scaling mode: " + self._shapeScalingMode+"<br>"]
            html += ["Resize mode: " + self._resizeMode + "<br>"]
            html += ["Constant value: " + str (self._constantValue) + "<br>"]   
            html += ["Missing slice: " + str (self._missingSliceMode)]               
            return "".join (html)
        
    def getAffineTransform (self) :
        return np.copy (self._2DAffineTransformation)
    
    def getMissingSliceMode (self) :
        return self._missingSliceMode
    
    def getResizeMode (self) :
        return self._resizeMode
    
    @staticmethod
    def transformPoint (AffineTransformation, cX, cY, NiftiPreviewSlice, Shape = None) :
        # do a fast 2x2 affine transformation, assumes only symerical flipping and 
        # 90 degree rotation will be done.
        if not np.any (AffineTransformation - np.eye (2)) :
            return (cX, cY)      
        if Shape is not None :
            xdim, ydim = Shape
        else:
            xdim, ydim = NiftiPreviewSlice.shape                
        halfXDim = int ((xdim)/2)
        halfYDim = int ((ydim)/2)
        cX -= halfXDim
        cY -= halfYDim        
        vector = np.zeros ((2,), dtype = np.int)                
        vector[0] = -halfXDim
        vector[1] = -halfYDim            
        newVector1 = np.matmul (AffineTransformation, vector)        
        vector[0] = xdim-halfXDim
        vector[1] = ydim-halfYDim            
        newVector2 = np.matmul (AffineTransformation, vector)        
        dx = max(newVector1[0],newVector2[0]) - min(newVector1[0],newVector2[0] + 1)
        dy = max(newVector1[1],newVector2[1]) - min(newVector1[1],newVector2[1] + 1)
        newCenterX = int (dx / 2)
        newCenterY = int (dy / 2)        
        vector[0] = cX
        vector[1] = cY            
        newVector3 = np.matmul (AffineTransformation, vector)        
        return (newVector3[0] + newCenterX , newVector3[1] + newCenterY)
                
    @staticmethod
    def transformImage (AffineTransformation, NiftiPreviewSlice, Object_Memory_Cache = None) :
        # do a fast 2x2 affine transformation, assumes only symerical flipping and 
        # 90 degree rotation will be done.
        if not np.any (AffineTransformation - np.eye (2)) :
            return np.copy (NiftiPreviewSlice)
        # center the image
        xdim, ydim = NiftiPreviewSlice.shape                
        halfXDim = int ((xdim)/2)
        halfYDim = int ((ydim)/2)
        
        if (Object_Memory_Cache is None) :
            offset = np.zeros ((2,), dtype = np.int)
            vector = np.zeros ((2,), dtype = np.int)
        else:
            try:
                offset = Object_Memory_Cache._offsetMem
                vector = Object_Memory_Cache._vectorMem
            except:
                Object_Memory_Cache._offsetMem = np.zeros ((2,), dtype = np.int)
                Object_Memory_Cache._vectorMem = np.zeros ((2,), dtype = np.int)
                offset = Object_Memory_Cache._offsetMem
                vector = Object_Memory_Cache._vectorMem
                
        offset[0] = halfXDim
        offset[1] = halfYDim        
        
        #allocate new memory for output image may not be the same size as the input
        #if dimensions are not symertical
        if (xdim == ydim) :
            newSlice = np.zeros ((xdim, ydim), dtype = NiftiPreviewSlice.dtype)
        else:              
            vector[0] = -halfXDim
            vector[1] = -halfYDim            
            newVector = np.matmul (AffineTransformation, vector)            
            newX1, newY1 = newVector[0], newVector[1]
            
            vector[0] = xdim -halfXDim
            vector[1] = ydim -halfYDim
            newVector = np.matmul (AffineTransformation, vector)            
            newX2, newY2 = newVector[0], newVector[1]
            
            newXdim = int (max (newX2,newX1) - min (newX2,newX1))
            newYdim = int (max (newY2,newY1) - min (newY2,newY1))
            newSlice = np.zeros ((newXdim, newYdim), dtype = NiftiPreviewSlice.dtype)
                
        #allocate temp memory to store xcomponent and ycomponent of the transformation        
        if (Object_Memory_Cache is None) :
            tempVec = np.zeros ((2, max(xdim,ydim), 2), dtype=np.int)   
        else:
            try:
                tempVec = Object_Memory_Cache._tempVec  
                if (tempVec.shape[1] < max(xdim,ydim)) :
                    Object_Memory_Cache._tempVec = np.zeros ((2, max(xdim,ydim), 2), dtype=np.int)        
                    tempVec = Object_Memory_Cache._tempVec       
            except:
                Object_Memory_Cache._tempVec = np.zeros ((2, max(xdim,ydim), 2), dtype=np.int)        
                tempVec = Object_Memory_Cache._tempVec            
            
        #compute x component of the transformation, reversing the centering opperation
        xComponent =  AffineTransformation[:,0]
        xVec = np.arange (xdim)
        offsetVec = xVec - halfXDim
        tempVec[0, xVec, 0] =  xComponent[0] * offsetVec + offset[0]
        tempVec[0, xVec, 1] =  xComponent[1] * offsetVec + offset[0]
            
        #compute y component of the transformation
        yComponent =  AffineTransformation[:,1]
        ylist = np.arange (ydim)
        offsetVec = ylist - halfYDim
        tempVec[1, ylist, 0] = yComponent[0] * offsetVec + offset[1]
        tempVec[1, ylist, 1] = yComponent[1] * offsetVec + offset[1]
            
        #determine the output offset to ensure that the transformation maps the output into the output memory
        x1, y1 = tempVec[0,0,:] + tempVec[1,0,:]
        x2, y2 = tempVec[0,xdim-1,:] + tempVec[1,ydim-1,:]
        xoutputOffset = min (x1,x2)
        youtputOffset = min (y1,y2)        
        offset[0] = xoutputOffset      
        offset[1] = youtputOffset              
        
        #compute the transformation
        for x in xVec :
            xoffset = tempVec[0,x,:] - offset            
            newVector = xoffset + tempVec[1,ylist,:]                 
            newSlice[newVector[:,0], newVector[:,1]] = NiftiPreviewSlice[x, ylist]                
        return newSlice
    
    def copy (self) :
        return ML_ModelInputTransformation (self._2DAffineTransformation, self._resizeMode,   self._constantValue, self._missingSliceMode, self._PredictOnPatch)           
    
    def getParameters (self):
        return (self._shapeScalingMode, self._missingSliceMode, self._resizeMode, self._constantValue, self._PredictOnPatch)
    
    def getFileObject (self) :
        fobj = FileObject ("ML_ModelInputTransformation")
        fobj.setParameter ("2DAffineTransformation", self._2DAffineTransformation.tolist ())                
        fobj.setParameter ("MissingSliceMode", self._missingSliceMode)
        fobj.setParameter ("resizeMode", self._resizeMode)
        fobj.setParameter ("ConstantValue", self._constantValue)
        fobj.setParameter ("PredictOnPatch", self._PredictOnPatch)        
        return fobj
    
    @staticmethod
    def loadFromFileObj (fobj) :
        transformation = fobj.getParameter ("2DAffineTransformation")        
        AffineTransformation = np.array (transformation, dtype=np.int)                        
        if (fobj.hasParameter ("MissingSliceMode")) :
            missingSliceMode = fobj.getParameter ("MissingSliceMode")
        else:
            missingSliceMode = "Constant"
        resizeMode = fobj.getParameter ("resizeMode") 
        if (fobj.hasParameter ("ConstantValue")) :
            constantValue = fobj.getParameter ("ConstantValue")
        else:
            constantValue = 0        
        if (fobj.hasParameter ("PredictOnPatch")) :
            PredictOnPatch = fobj.getParameter ("PredictOnPatch")
        else:
            PredictOnPatch = False
        return ML_ModelInputTransformation (AffineTransformation = AffineTransformation, resizeMode = resizeMode,  constantValue= constantValue, MissingSliceMode = missingSliceMode, PredictOnPatch = PredictOnPatch) 
    
    @staticmethod
    def getClipRange (outW, inW) :
        if outW == inW :
            return 0, outW, 0, inW 
        if outW < inW :
            offset = int ((inW - outW) / 2)
            return 0, outW, offset, offset + outW
        offset = int ((outW - inW) / 2)
        return offset, offset + inW, 0, inW 

    def apply2DTransform (self, sliceView, rawImageData, sliceIndex, inputdatashape, primarySlice) :          
        
        def _getSlice (index, rawImageData, sliceView, primarySlice) :
            if sliceView.getSliceAxis () == "Z" and  sliceView.areAxisProjectionControlsEnabledForAxis () :
                projectionControls = sliceView.getAxisProjectionControls ()
                rawImageData, clipMask = projectionControls.getNIfTIProjection (sliceView.getCoordinate (), sliceView.getNIfTIVolume (), UseCache = True, SliceIndex = index)
                if  primarySlice :
                    clipMask = np.logical_not (clipMask)
                    if np.any (clipMask) :
                        self._offAxisClipMask = clipMask
                        if self._missingSliceMode == "Constant" :
                            rawImageData[clipMask] = self._constantValue
                        else:
                            rawImageData[clipMask] = 0
                    else:
                        self._offAxisClipMask = None
                return rawImageData
            else:    
                return rawImageData[:,:,index]
            
        self._AppliedTransformationStack = []
        if (sliceIndex < 0 or sliceIndex >= rawImageData.shape[2]) :
            if self._missingSliceMode == "Reflection" :                
                if (sliceIndex < 0) :                                            
                    sliceIndex = -sliceIndex
                    sliceIndex = sliceIndex % (rawImageData.shape[3])
                    sliceData = _getSlice (sliceIndex,  rawImageData, sliceView, primarySlice)
                else:
                    sliceIndex = sliceIndex % (rawImageData.shape[3])                
                    sliceData = _getSlice ((rawImageData.shape[3] - 1) - sliceIndex,  rawImageData, sliceView, primarySlice)
            else:
                if (sliceIndex < 0) :
                    sliceData = _getSlice (0, rawImageData, sliceView, primarySlice)
                else:
                    sliceData = _getSlice (rawImageData.shape[2] - 1,   rawImageData, sliceView, primarySlice)
                if self._missingSliceMode == "Constant" :
                    sliceData[:,:] = self._constantValue
        else:
            sliceData = _getSlice (sliceIndex, rawImageData, sliceView, primarySlice)
            
            
        resizeMode = self._resizeMode     
        image = ML_ModelInputTransformation.transformImage (self._2DAffineTransformation, sliceData, Object_Memory_Cache = self)                 
        self._AppliedTransformationStack.append (("Affine", None))
        x,y = inputdatashape[0], inputdatashape[1]                
        if (x is None and y is None) :
            resizeMode = "Resize (non-uniform)"
            x,y  = image.shape[0], image.shape[1]
            
        if (self._PredictOnPatch and (image.shape[0] > x or image.shape[1] > y)) : 
            patchCenterX, patchCenterY = ML_ModelInputTransformation.transformPoint (self._2DAffineTransformation, self._PatchX, self._PatchY, sliceData) 
            halfX = int (x / 2)
            halfY = int (y / 2)
            if image.shape[0] > x and image.shape[1] > y :
                lX = int (max (0,patchCenterX - halfX))
                rX = int (min (image.shape[0],lX + x))
                if (rX - lX < x) :
                    lX = rX - x
                lY = int (max (0,patchCenterY - halfY))
                rY = int (min (image.shape[1],lY + y))
                if (rY - lY < y) :
                    lY = rY - y
                self._AppliedTransformationStack.append (("Patch", (image.shape, lX, rX, lY, rY)))                                
                image = image[lX: rX,  lY:rY]
            elif image.shape[0] > x :
                lX = int (max (0,patchCenterX - halfX))
                rX = int (min (image.shape[0],lX + x))          
                if (rX - lX < x) :
                    lX = rX - x
                self._AppliedTransformationStack.append (("Patch", (image.shape, lX, rX, None, None)))                                
                image = image[lX: rX,  ::]
            elif image.shape[1] > y :
                lY = int (max (0,patchCenterY - halfY))
                rY = int (min (image.shape[1],lY + y))
                if (rY - lY < y) :
                    lY = rY - y
                self._AppliedTransformationStack.append (("Patch", (image.shape, None, None, lY, rY)))
                image = image[::,  lY: rY]
                
        if (image.shape[0] != x or image.shape[1] != y) :
            if (resizeMode == "Shrink (preserve dim) & crop") :
                sx = min (x, image.shape[0])
                sy = min (y, image.shape[1])   
                DimPerservingScaling = True
            elif (resizeMode == "Shrink/grow (preserve dim) & crop") :  
                sx = x
                sy = y    
                DimPerservingScaling = True
            else:
                DimPerservingScaling = False
            if (DimPerservingScaling) :
                if (image.shape[0] != sx or sy != image.shape[1]) :
                    xsf = sx / image.shape[0]
                    ysf = sy / image.shape[1]
                    minSf = min (xsf, ysf)
                    sx =  int (minSf * image.shape[0])
                    sy =  int (minSf * image.shape[1])
                    self._AppliedTransformationStack.append (("Resize",image.shape))
                    inputImageDtype = image.dtype
                    image = skimage.transform.resize (image, (sx,sy), order = 1, mode = self._shapeScalingMode, preserve_range  = True,  cval = self._constantValue)
                    if (image.dtype != inputImageDtype) :
                        image = image.astype (inputImageDtype)
                resizeMode = "Center & crop"
            
            if resizeMode == "Center & crop"  :
                output = np.zeros ((x,y), dtype = np.float)
                output[:,:] = self._constantValue
                ox1, ox2, sx1,sx2 = ML_ModelInputTransformation.getClipRange (output.shape[0], image.shape[0])
                oy1, oy2, sy1,sy2 = ML_ModelInputTransformation.getClipRange (output.shape[1], image.shape[1])
                output[ox1:ox2,oy1:oy2] = image[sx1:sx2, sy1:sy2]
                self._AppliedTransformationStack.append (("Crop",(ox1,ox2,oy1,oy2, sx1,sx2, sy1,sy2, image.shape)))
                image = output
            elif resizeMode == "Resize (non-uniform)" :
                self._AppliedTransformationStack.append (("Resize",image.shape))
                inputImageDtype = image.dtype
                image = skimage.transform.resize (image, (x,y), order = 1, mode = self._shapeScalingMode, preserve_range  = True,  cval = self._constantValue)                        
                if (image.dtype != inputImageDtype) :
                    image = image.astype (inputImageDtype)
        return image
   
    def reverse2D (self, sliceData, outputSliceShape, CVal = 0, ResizeOrder = 1)  :            
        image = np.copy (sliceData)
        
        if image.shape[1] == 1 and image.shape[0] > 1 :           # small hack to make Z's Skull stripping algortithm run.
            flattenDim = image.shape[0] 
            testdim = int (math.sqrt (flattenDim))
            if testdim * testdim == flattenDim :
                image = image.reshape ((testdim, testdim))    # ToDo:  Add the ability to generalize model transformation reversal.
            
        for transformIndex in range (len (self._AppliedTransformationStack) - 1, -1, -1) :
            transform = self._AppliedTransformationStack[transformIndex]
            if transform[0] == "Affine" :
                if np.any (self._2DAffineTransformation - np.eye (2)) :
                    inverseTransform = np.linalg.inv(self._2DAffineTransformation)
                    image = ML_ModelInputTransformation.transformImage (inverseTransform, image, Object_Memory_Cache = self)                
            elif transform[0] == "Patch" :
                origionalShape, stX, endX, stY, edY =  transform[1]
                outputImage = np.zeros (origionalShape, dtype = image.dtype)
                outputImage[...] = CVal
                if (stX is not None and stY is not None) :
                    outputImage[stX:endX, stY:edY] = image
                elif (stX is not None) :
                    outputImage[stX:endX, :] = image
                elif (stY is not None) :
                    outputImage[:, stY:edY] = image
                image = outputImage            
            elif transform[0] == "Resize" :
                resizeInputShape = transform[1]
                inputImageDtype = image.dtype
                image = skimage.transform.resize (image, resizeInputShape, order = ResizeOrder, mode = self._shapeScalingMode, preserve_range  = True, cval = CVal)        
                if (image.dtype != inputImageDtype) :
                    image = image.astype (inputImageDtype)
            elif transform[0] == "Crop" :                            
                ox1,ox2,oy1,oy2, sx1,sx2, sy1,sy2, cropInputShape = transform[1]
                outputImage = np.zeros (cropInputShape, dtype = image.dtype)
                if CVal != 0 :
                    outputImage[...] = CVal
                outputImage[sx1:sx2,sy1:sy2] = image[ox1:ox2,oy1:oy2]
                image = outputImage 
        if self._offAxisClipMask is not None :
            image[self._offAxisClipMask] = 0
        return image


class ML_ModelNormalization :
    def __init__ (self, NormalizationMode="None",FixedMean=0,FixedSD=1.0,CustomCode=None,Window=1000,Level=100,Norm=True,NormLow=0,NormHigh=1) :
        self._mode = NormalizationMode
        self._fixedMean = FixedMean
        self._fixedSD = FixedSD        
        self._CustomCode = CustomCode
        self._volumeSDMeanCache = None
        self._Window = Window
        self._Level = Level
        self._Normalize = Norm
        self._NormalizeLow = NormLow
        self._NormalizeHigh = NormHigh
        self.setColorNormMode (False)
    
    
    def getWindow (self) :
        return self._Window 
    
    def getLevel (self) :
        return self._Level 
    
    def getLowNorm (self) :
        return self._NormalizeLow 
    
    def getHighNorm (self) :
        return self._NormalizeHigh 
    
    def isWindowLevelNormalized (self) :
        return self._Normalize
        
    def getHtml (self) :
            html = ["Input normalization: " + self._mode]
            if self._mode ==  "FixedMeanSD" :
                html += ["Mean: " + str (self._fixedMean), "SD: "+ str (self._fixedSD)]
            if self._mode ==  "WindowLevel" :
                html += ["Window:" + str (self._Window), "Level: " + str (self._Level)]
                if (self._Normalize) :
                    html += ["Normalize From: %d to %d" % (self._NormalizeLow, self._NormalizeHigh)]
            if self._mode ==  "CustomCode" :
                html += ["Custom code:", self._CustomCode.replace ("\n","<br>")]
            return "<br>".join (html)
 
    def getMode (self) :
        return self._mode 
    
    def getFixedMean (self) :
        return self._fixedMean 
    
    def getFixedSD (self) :
        return self._fixedSD 
    
    def getCustomCode (self) :
        return self._CustomCode 
    
    def getNormalizationParameters (self) :
        return (self._mode, self._fixedMean, self._fixedSD, self._CustomCode, self._Window,  self._Level, self._Normalize, self._NormalizeLow, self._NormalizeHigh)

    def copy (self) :
        return ML_ModelNormalization (self._mode, self._fixedMean, self._fixedSD, self._CustomCode, self._Window,  self._Level, self._Normalize, self._NormalizeLow, self._NormalizeHigh)           

    @staticmethod
    def loadFromFileObj (fobj) :      
        mode = fobj.getParameter ("Mode")
        fixedMean = fobj.getParameter ("FixedMean")
        fixedSD = fobj.getParameter ("FixedSD")
        CustomCode = fobj.getParameter ("CustomCode")    
        if fobj.hasParameter ("Window") :
            Window = fobj.getParameter ("Window")    
        else:
            Window = 1000
        if fobj.hasParameter ("Level") :            
            Level = fobj.getParameter ("Level")    
        else:
            Level= 100
        if fobj.hasParameter ("Norm") :            
            Norm = fobj.getParameter ("Norm")    
        else:
            Norm= True
        if fobj.hasParameter ("NormLow") :                        
            NormLow = fobj.getParameter ("NormLow")    
        else:
            NormLow= 0
        if fobj.hasParameter ("NormHigh") :                        
            NormHigh = fobj.getParameter ("NormHigh")    
        else:
            NormHigh = 1
        return ML_ModelNormalization (mode, fixedMean, fixedSD, CustomCode, Window, Level, Norm, NormLow, NormHigh)
    
    def getFileObject (self) :
        fobj = FileObject ("ModelNormalization")
        fobj.setParameter ("Mode", self._mode)
        fobj.setParameter ("FixedMean", self._fixedMean)
        fobj.setParameter ("FixedSD", self._fixedSD)
        fobj.setParameter ("CustomCode", self._CustomCode)        
        
        fobj.setParameter ("Window", self._Window)
        fobj.setParameter ("Level", self._Level)
        fobj.setParameter ("Norm", self._Normalize)
        fobj.setParameter ("NormLow", self._NormalizeLow)        
        fobj.setParameter ("NormHigh", self._NormalizeHigh)                         
        return fobj
    
    def setColorNormMode (self, mode) :
        self._runningInColorImageMode = mode
        
    def isRunningInColorNormMode (self) :
        return self._runningInColorImageMode
    
    def applyColorNorm2D (self,  sliceData, rawVolumeData):
        if self.isRunningInColorNormMode () :
            return self.apply2D (sliceData, rawVolumeData, ForceRun = True)
        return sliceData
        
    def apply2D (self,  sliceData, rawVolumeData, ForceRun = False) :
        if self._mode == "None" or (self.isRunningInColorNormMode () and not ForceRun):
            return sliceData
        elif self._mode == "FixedMeanSD" :
            try :                
                return (sliceData - self._fixedMean) / self._fixedSD
            except :
                return None            
        elif self._mode == "VolumeMeanSD" :  
            if self._volumeSDMeanCache is None or self._volumeSDMeanCache["Image"] is not rawVolumeData :
                    if self._volumeSDMeanCache is None :
                        self._volumeSDMeanCache = {}                    
                    self._volumeSDMeanCache["Image"] = rawVolumeData
                    self._volumeSDMeanCache["Mean"] = np.mean (rawVolumeData)
                    self._volumeSDMeanCache["SD"] = np.std (rawVolumeData)
                    if self._volumeSDMeanCache["SD"] <= 0 :
                        self._volumeSDMeanCache["SD"] = 0.00001
            return (sliceData - self._volumeSDMeanCache["Mean"]) / self._volumeSDMeanCache["SD"]
        elif self._mode == "SliceMeanSD" :            
            meanVal = np.mean (sliceData)
            meanData = (sliceData - meanVal)
            SD = math.sqrt(np.var (meanData,ddof=1))
            if (SD == 0) :
                SD = 0.00001
            return meanData / SD            
        elif self._mode == "WindowLevel" :      
            window = max (int (self._Window), 0)
            lhalf = int (window / 2)
            lower = self._Level - lhalf
            upper = lower + window
            sliceData = np.clip (sliceData, lower, upper)
            if (not self._Normalize) :
               return sliceData    
            else:
               sliceData = sliceData - lower
               maxValue = upper - lower 
               if (maxValue <= 0) :
                   sliceData[...] = 0
               else:
                   sliceData = sliceData.astype (np.float) / maxValue                  
               return (sliceData *  (self._NormalizeHigh - self._NormalizeLow)) + self._NormalizeLow               
            
        elif self._mode == "CustomCode" :
            #"def getNormalizedSlice (NiftVolumePath, NiftiVolume,sliceIndex):",
            #"    return NiftiVolume[:,:,sliceIndex]"]
            try :
                sliceData = np.copy (sliceData)  
                rawVolumeData = np.copy (rawVolumeData)  
                
                indent = " "
                buildCustomClass = []
                buildCustomClass += ["class CustomNormalization :"]
                textlines = self._CustomCode.split ("\n")
                buildCustomClass += [indent + "@staticmethod"]
                for line in textlines :
                    buildCustomClass += [indent + line]
                buildCustomClass += [""]
                buildCustomClass += ["self._CustomKerasObjectsDir = CustomNormalization()"]
                customobject = "\n".join (buildCustomClass)
                print ("--------------------------------")
                print ("building custom normalization code")
                try :
                    exec (customobject, globals (), locals ())                     
                except Exception as e:
                    print ("Exception occured in custom normalization code")
                    print (str (e))
                    sliceData = None
                    print ("--------------------------------")            
                customObjDir = {}
                try :
                    itemList = dir (self._CustomKerasObjectsDir)                    
                    for item in itemList :
                        if callable (getattr (self._CustomKerasObjectsDir, item)) :
                           if not item.startswith('__') and not item.endswith('__') :
                                method = self._CustomKerasObjectsDir.__getattribute__(item)
                                customObjDir[item] = method
                except:
                    pass    
                if "getNormalizedSlice" in customObjDir :
                    try :
                        print ("Running Custom Normalization Code")
                        try :                    
                            sliceData = customObjDir["getNormalizedSlice"] (sliceData,rawVolumeData)
                        except:
                            sliceData = customObjDir["getNormalizedSlice"] (sliceData)
                        return sliceData
                    except Exception as e:
                        print ("Exception occured in custom normalization code")
                        print (str (e))
                        sliceData = None
                        print ("--------------------------------")
                        MessageBoxUtil.showMessage (WindowTitle="Model Normalization Exception",WindowMessage = str (e))                    
                    return sliceData
                else:
                    print ("getNormalizedSlice object not found.")
                    return None
            except Exception as exp:            
                print ("Model Normalization Exception: " + str (exp))
                MessageBoxUtil.showMessage (WindowTitle="Model Normalization Exception",WindowMessage = str (exp))                    
                raise 
        
        
class ML_ModelNote :
    def __init__ (self, user ="Unknown", version=1, description="", date = None, time = None) :
        if (date is None) :
            self._modelCreationDate = DateUtil.today ()
        else:
            self._modelCreationDate = date
        if (time is None) :
            self._modelCreationTime = TimeUtil.getTimeNow ()
        else:
            self._modelCreationTime = time
        self._user = user
        self._version = version.strip ()
        self._description = description.strip ()
        if (self._version == "") :
            self._version = self.getSortableCreationDateTime ()
    
    def copy (self) :
        return ML_ModelNote (self._user, self._version, self._description, self._modelCreationDate, self._modelCreationTime)           

    def getSortableCreationDateTime (self) :
        return DateUtil.getSortableString (self._modelCreationDate) + "." + TimeUtil.timeToSortableString (self._modelCreationTime)
        
    def getUser (self) :
        return self._user
    
    def getVersion (self) :
        return self._version
    
    def getDescription (self) :
        return self._description
    
    def getDate (self) :
        return self._modelCreationDate
    
    def getTime (self) :
        return self._modelCreationTime
    
    @staticmethod
    def loadFromFileObj (fobj) :      
        date = fobj.getParameter ("CreationDate")
        time = fobj.getParameter ("CreationTime")
        user = fobj.getParameter ("User")
        version = fobj.getParameter ("Version")
        description = fobj.getParameter ("Description")
        return ML_ModelNote (user, version, description, date, time)
    
    def getFileObject (self, key) :
        fobj = FileObject ("ML_ModelDescription_" + str (key))
        fobj.setParameter ("CreationDate", self._modelCreationDate)
        fobj.setParameter ("CreationTime", self._modelCreationTime)
        fobj.setParameter ("User", self._user)
        fobj.setParameter ("Version", self._version)
        fobj.setParameter ("Description", self._description)
        return fobj
        
class ML_ModelLog :
    def __init__ (self, modelLog = None) :
        if (modelLog is None) :
            self._modelLog = []    
        else:
            self._modelLog =  modelLog 
    
    def copy (self) :
        newlog = ML_ModelLog ()
        for log in self._modelLog :
           newlog._modelLog.append (log.copy ())
        return newlog
            
    def appendLog (self, user, version, description) :
        currentDescription = self.getCurrentModelDescription ()
        if (currentDescription is None) :
            self._modelLog.append (ML_ModelNote (user, version, description))
            return True
        elif currentDescription.getVersion () != version or currentDescription.getDescription () != description  :
            self._modelLog.append (ML_ModelNote (user, version, description))
            return True
        return False                
        
    def getCurrentModelDescription (self) :
        if len (self._modelLog) > 0 :
            return self._modelLog[len (self._modelLog) - 1]
        return None
    
    def getModelLog (self) :
        return self._modelLog
    
    def getFileObject (self) :
        fobj = FileObject ("ML_ModelLog")
        fobj.setParameter ("DescriptionCount", len (self._modelLog))
        for index, obj in enumerate (self._modelLog) :
            fobj.addInnerFileObject (obj.getFileObject (index) )        
        return fobj
    
    @staticmethod
    def loadFromFileObj (fobj) :      
        modelLog = []    
        count = fobj.getParameter ("DescriptionCount")
        for index in range (count) :            
            obj = fobj.getFileObject ("ML_ModelDescription_" + str (index))
            modelLog.append (ML_ModelNote.loadFromFileObj (obj))            
        return ML_ModelLog (modelLog)
    
class ModelInputOutputDescription :
    def __init__ (self, preferedAxisList = ["Axial"], orientation = "UnInitalized", inputDataSize = None, outputDataSize = None, isChannelLast = True) :
        #add channel indicator
        self._channelLast = isChannelLast
        self._inputDataOrientation = orientation
        self._inputDataSize  = inputDataSize
        self._outputDataSize = outputDataSize
        self._ModelDataAxis = copy.copy (preferedAxisList)
    
    def isInputOutputDescriptionValid (self): 
        return self._inputDataOrientation is not None and self._inputDataSize is not None and self._outputDataSize is not None
    
    def getModelAxisList (self) :
        return copy.copy (self._ModelDataAxis)
    
    def setModelAxis (self, AxisList) :
        self._ModelDataAxis = []
        for item in AxisList :
            if item in ["Axial", "Sagittal", "Coronal"] and item not in self._ModelDataAxis :
                self._ModelDataAxis.append (item)      
        if len (AxisList) == 0 :
             self._ModelDataAxis = ["Axial"]    
             
    def getHtml (self) :                     
            html = ["Channel last: " +  str (self._channelLast)]
            html += ["Model axis: " +  ", ".join (self._ModelDataAxis)]    
            #html += ["Input orientation: " +  str (self._inputDataOrientation)]            
            html += ["Input data dimensions: " +  str (self._inputDataSize)]            
            html += ["Output data dimensions: " +  str (self._outputDataSize)]            
            return "<br>".join (html)

    def copy (self) :
        return ModelInputOutputDescription (self._ModelDataAxis, self._inputDataOrientation, self._inputDataSize, self._outputDataSize, self._channelLast)
    
    def getInputImageShape (self):
        inputshape = copy.copy (self._inputDataSize)
        del inputshape[0]
        if (not self.isChannelLast()) :
            del inputshape[0]
        else:
            del inputshape[len (inputshape) - 1]
        return inputshape
    
    def isModelOutput2D (self) :
        return len (self._outputDataSize) == 4 
    
    def isModelClassificationOutput (self) :
        return len (self._outputDataSize) == 2
    
    def getInputSliceBlockSize (self) :
        if not self.isChannelLast () :
            return self._inputDataSize [1]
        elif self.isInputData2D () :    
            return self._inputDataSize [3]
        elif self.isInputData3D () :    
            return self._inputDataSize [3]
        return None
        
    def isChannelLast (self) :
        return self._channelLast
    
    def getInputDataOrientation (self) :
        return self._inputDataOrientation

    def getInputDataSize (self) :
        return self._inputDataSize
    
    def isInputData2D (self) :        
        return len (self._inputDataSize) == 4
    
    def isInputData3D (self) :        
        return len (self._inputDataSize) == 5

    def getOutputDataSize (self):
        return self._outputDataSize
     
    def getFileObject (self) :        
        fobj = FileObject ("ML_ModelInputOutputDescription")
        fobj.setParameter ("InputDataOrientation", self._inputDataOrientation)        
        fobj.setParameter ("InputDataSize", self._inputDataSize)
        fobj.setParameter ("OutputDataSize", self._outputDataSize)
        fobj.setParameter ("IsChannelLast",self._channelLast)   
        fobj.setParameter ("ModelAxis", self._ModelDataAxis)
        return fobj
        
    @staticmethod
    def loadFromFileObj (fobj) :              
        inputDataOrientation = fobj.getParameter ("InputDataOrientation")
        inputDataSize = fobj.getParameter ("InputDataSize")
        outputDataSize = fobj.getParameter ("OutputDataSize")
        channelLast = fobj.getParameter ("IsChannelLast")        
        if fobj.hasParameter ("ModelAxis") :  
            ModelDataAxis  = fobj.getParameter ("ModelAxis")    
        else:
            ModelDataAxis = ["Axial"]
        return ModelInputOutputDescription (ModelDataAxis, inputDataOrientation, inputDataSize, outputDataSize, channelLast)    
                                
class ML_ModelPredictBase  :
    def __init__ (self, ProjectDataset) :
        self._modelID = None
        self._SinglePredict = True #Predictions only set one target to True        
        self._modelName = ""    
        self._PredictProbThresholdsSet = False
        self._kerasModel = ML_KerasModel (ProjectDataset)                
        self._modelLog = ML_ModelLog ()        
        self._modelInputOutputDescription = ModelInputOutputDescription ()                
        self._modelInputTransformation = ML_ModelInputTransformation ()        
        self._modelInputNormalization = ML_ModelNormalization ()        
        self._modelExistingDataFlags = []
        self._modelChanged = []
        self._TreePath = []
        self._modelPath = ""  
        self._modelAutoImported = False        
        self._shouldApplyModelOnImaging = False
        
    def doesModelSupportApplicationOnImaging (self) :
        return False
    
    def shouldApplyModelOnImaging (self) :
        return self._shouldApplyModelOnImaging
    
    def setApplyModelOnImaging (self, val) :
        self._shouldApplyModelOnImaging = val
        
    def getKerasCustomObjectPythonCode (self) :
        return self._kerasModel.getCustomObjectPythonCode ()
        
    def setKerasCustomObjectPythonCode (self, customObjects) :
        self._kerasModel.setCustomObjectPythonCode (customObjects)
        
    def innerCopy (self, newModel) :        
        newModel._modelPath = self._modelPath
        newModel._modelID = None 
        newModel._TreePath = copy.copy (self._TreePath)
        newModel._SinglePredict = self._SinglePredict
        newModel._PredictProbThresholdsSet = self._PredictProbThresholdsSet
        newModel._modelName = self._modelName     
        newModel._kerasModel = self._kerasModel.copy ()
        newModel._modelLog = self._modelLog.copy ()
        newModel._modelInputOutputDescription = self._modelInputOutputDescription.copy ()
        newModel._modelInputTransformation = self._modelInputTransformation.copy ()
        newModel._modelInputNormalization = self._modelInputNormalization.copy ()        
        newModel._modelExistingDataFlags = copy.copy (self._modelExistingDataFlags)
        newModel._modelChanged = []
        newModel._modelAutoImported = self._modelAutoImported         
        newModel._shouldApplyModelOnImaging = self._shouldApplyModelOnImaging        

    def isAutoImportedModel (self) :
        return self._modelAutoImported is not None
        
    def setModelAutoImported (self, autoImported) :
        self._modelAutoImported = autoImported
    
    def getHtmlSummary (self) :
        currentLog = self.getModelDescriptionLog ()
        summary = ["<html><body>",
                  "<font style='font-size:16px'><b>Model Name: </b>" + self._modelName + "</font><br><br>",
                  "<font style='font-size:14px'><b>Version:</b> " + currentLog.getVersion () + "</font><br>",
                  "<font style='font-size:14px'><b>Last changed:</b> " + DateUtil.dateToString (currentLog.getDate ()) + " @ " + TimeUtil.timeToString (currentLog.getTime()) + " by " + currentLog.getUser ()+ "</font><br>"]
        if (self._modelAutoImported is not None) :
            try :
               dateImported = self._modelAutoImported[0]
               timeImported = self._modelAutoImported[1]
               dateStr = DateUtil.dateToString (dateImported)
               timeStr = TimeUtil.timeToString(timeImported)
               modelAutoImportedStr =  dateStr + " at " +timeStr
            except:
                modelAutoImportedStr = "Error constructing model auto-import description"
            summary += ["<font style='font-size:14px'><b>Auto-imported: </b>"+modelAutoImportedStr+"</font><br>"]
        if len (currentLog.getDescription ().strip ()) > 0 :
            descriptionSummary = ["<br><br>",
                                  "<font style='font-size:14px'><b>Description</b><br>" + currentLog.getDescription ().replace ("\n","\n<br/>") + "</font>"]
            summary += descriptionSummary

        if (self._SinglePredict) :
            singlePredMode = "True"
        else:
            singlePredMode = "False"
        
        multiSliceVotingStr = self.getMultiSliceVoting ()
        if (multiSliceVotingStr is None) :
            multiSliceVotingStr = ""
        else:
            multiSliceVotingStr = "; Multi-slice voting: " + multiSliceVotingStr
        modelDetailedDescription = ["<hr>",
                  "<font style='font-size:12px'><b>Detailed (" +self.getModelType () +" Model)</b></font>",                  
                  "<p><font style='font-size:12px'><b>Input/Output</b><br>"+self._modelInputOutputDescription.getHtml () +"</font></p>",
                  "<p><font style='font-size:12px'><b>Input Transformation</b>"+self._modelInputTransformation.getHtml () +"</font></p>",
                  "<p><font style='font-size:12px'><b>Normalization</b><br>"+self._modelInputNormalization.getHtml () +"</font></p>",                 
                  "<p><font style='font-size:12px'><b>Handle existing data</b>: " + " ".join (self._modelExistingDataFlags)  +"</font></p>",                                    
                  "<hr><p><font style='font-size:12px'><b>Prediction</b> (Single prediction: " + singlePredMode + multiSliceVotingStr+")",                                                      
                  self.getPredictionHtmlDescription (self._PredictProbThresholdsSet) +"</font></p><hr>",
                  "<p><font style='font-size:12px'><b>Model Location</b><br>"+self._kerasModel.getHtml () +"</font></p>",
                  "</body></html>"]
        
        summary += modelDetailedDescription
        return "\n".join (summary)
    
    def setModelPath (self, newPath) :
        self._modelPath = newPath
    
    def getModelPath (self) :
        return self._modelPath
        
    def setTreePath (self, newPath) :
        if ("".join (newPath) != "".join (self._TreePath)) :
            self._TreePath = copy.copy (newPath)
            self._modelChanged.append ("TreePath")                        
        
    def resetModelChanged (self) :
        self._modelChanged = []
        
    def isModelChanged (self) :
        return (len (self._modelChanged) > 0)
    
    def setSinglePrediction (self, val) :
        if (val != self._SinglePredict) :
            self._SinglePredict = val
            self._modelChanged.append ("SinglePrediction=" +str (val))
    
    def getSinglePrediction  (self) :
        return self._SinglePredict
    
    def setPredictionProbThreshold (self, val) :
        if (val != self._PredictProbThresholdsSet) :
            self._PredictProbThresholdsSet = val
            self._modelChanged.append ("PredictProbThresholdsSet=" +str (val))
            
    def getPredictionProbThreshold (self) :
        return self._PredictProbThresholdsSet
        
    def setModelID (self, modelID) :
        if self._modelID != modelID :
            self._modelID = modelID            
        
    def setModelName (self, modelName) :
        if self._modelName != modelName :
            self._modelName = modelName
            self._modelChanged.append ("ModelName=" + modelName)
            
    def getModelName (self) :
        return self._modelName
    
    def setOrigionalKerasModelHDF5Path (self, path) :
        if (self._kerasModel.setOrigionalModelPath (path)) :
            self._modelChanged.append ("OrigionalModelPath=" + path)
            
    def setKerasModelHDF5Path (self, path) :
        if (self._kerasModel.setModelPath (path)) :
            self._modelChanged.append ("KerasModePath=" + path)
    
    def getOrigionalKerasModelHDF5Path (self):
        return self._kerasModel.getOrigionalModelPath ()
            
    def getCurrentKerasModelHDF5Path (self) :
        return self._kerasModel.getCurrentModelPath ()            
    
    def getKerasModel (self) :
        return self._kerasModel
       
    def getModelDescriptionLog (self) :
        return self._modelLog.getCurrentModelDescription ()
    
    def setModelDescriptionLog (self, user, version, description):
        if (self._modelLog.appendLog (user, version, description)) :
            self._modelChanged.append ("ModelDescriptionLogChanged")

    def getModelInputOutputDescription (self):
        return self._modelInputOutputDescription
    
    def getModelAxisList (self) :
        return self.getModelInputOutputDescription().getModelAxisList ()
    
    def setModelInputOutputDescription (self, preferedAxisList, orientation, inputDataSize, outputDataSize, isChannelLast):
        changed = False
        if (tuple(self._modelInputOutputDescription.getModelAxisList()) != tuple(preferedAxisList)):
            changed = True
        elif (self._modelInputOutputDescription.getInputDataOrientation() != orientation):
            changed = True
        elif (self._modelInputOutputDescription.getInputDataSize() != inputDataSize):
            changed = True
        elif (self._modelInputOutputDescription.getOutputDataSize() != outputDataSize):
            changed = True
        elif (self._modelInputOutputDescription.isChannelLast () != isChannelLast) :
            changed = True        
        if (changed) :
            self._modelInputOutputDescription = ModelInputOutputDescription (preferedAxisList, orientation, inputDataSize, outputDataSize, isChannelLast)
            self._modelChanged.append ("ModelInputOutputDescription")
    
    def getModelInputTransformation (self):
        return self._modelInputTransformation
            
    
    def setModelInputTransformation (self, affin, missingSliceMode, resizeMode, constantValue, PredictOnPatch):
        changed = False
        shapeScalingMode = "reflect"
        if (np.any (self._modelInputTransformation.getAffineTransform () - affin)) :
            changed = True  
        elif (( shapeScalingMode, missingSliceMode, resizeMode, constantValue, PredictOnPatch) != self._modelInputTransformation.getParameters ()):
            changed = True
        if (changed) : 
            self._modelInputTransformation = ML_ModelInputTransformation  (affin, resizeMode,  constantValue, missingSliceMode, PredictOnPatch)
            self._modelChanged.append ("ModelInputTransformation")    
    
    def setModelInputTransformationPredictionPatchCenter (self, cX, cY) :
        self._modelInputTransformation.setPatchPredictionCoordinates (cX, cY)
        
    def getModelNormalization (self) :
        return self._modelInputNormalization 
   
    def setModelNormalization (self, NormalizationMode="None",FixedMean=0,FixedSD=1.0,CustomCode=None,Window=1000,Level=100,Norm=True,LowNorm=0.0,HighNorm=1.0) :        
         test = ML_ModelNormalization (NormalizationMode, FixedMean, FixedSD, CustomCode,Window,Level,Norm,LowNorm,HighNorm) 
         if (test.getNormalizationParameters () != self._modelInputNormalization.getNormalizationParameters ()) :        
             self._modelInputNormalization = test
             self._modelChanged.append ("ModelNormalization")
        
    def getHowToHandlePreExistingDataFlags (self):
        return copy.copy (self._modelExistingDataFlags)
    
    def setHowToHandlePreExistingDataFlags (self, val):
        #if val not in ["DoNotAlterExistingData", "RemoveExistingDataFromSlice", "RemoveAllExistingData", "AddToExistingData"] :
        #    print(1/0) # cause a exception             
        if val not in self._modelExistingDataFlags :
            self._modelChanged.append ("HowToHandleExistingDataFlag")
            self._modelExistingDataFlags.append (val)
    
    def _addToFileObject (self, fobj):        
        fobj.setParameter ("Name", self._modelName)
        fobj.setParameter ("ModelTreePath", self._TreePath)        
        fobj.setParameter ("SinglePrediction", self._SinglePredict)
        fobj.setParameter ("ExistingDataFlags", self._modelExistingDataFlags)
        fobj.setParameter ("PredictinProbThresholdsSet", self._PredictProbThresholdsSet)
        fobj.setParameter ("ModelAutoImported", self._modelAutoImported)        
        fobj.setParameter ("shouldApplyModelOnImaging", self._shouldApplyModelOnImaging)
        fobj.addInnerFileObject (self._kerasModel.getFileObject ())
        fobj.addInnerFileObject (self._modelLog.getFileObject ())
        fobj.addInnerFileObject (self._modelInputTransformation.getFileObject ())
        fobj.addInnerFileObject (self._modelInputOutputDescription.getFileObject ())
        fobj.addInnerFileObject (self._modelInputNormalization.getFileObject ())
        return fobj
    
    def _initFromFileObject (self, fobj, ProjectDataset):
        self._modelName = fobj.getParameter ("Name")
        self._TreePath = fobj.getParameter ("ModelTreePath")        
        self._SinglePredict = fobj.getParameter ("SinglePrediction")        
        self._PredictProbThresholdsSet = fobj.getParameter ("PredictinProbThresholdsSet")
        self._modelExistingDataFlags = fobj.getParameter ("ExistingDataFlags")                
        obj = fobj.getFileObject ("ML_KerasModel")
        self._kerasModel = ML_KerasModel.loadFromFileObj (obj, ProjectDataset)         
        obj = fobj.getFileObject ("ML_ModelLog")
        self._modelLog = ML_ModelLog.loadFromFileObj (obj)         
        obj = fobj.getFileObject ("ML_ModelInputTransformation")
        self._modelInputTransformation = ML_ModelInputTransformation.loadFromFileObj (obj)        
        obj = fobj.getFileObject ("ML_ModelInputOutputDescription")
        self._modelInputOutputDescription = ModelInputOutputDescription.loadFromFileObj (obj)        
        obj = fobj.getFileObject ("ModelNormalization")
        self._modelInputNormalization = ML_ModelNormalization.loadFromFileObj (obj)
        if fobj.hasParameter ("ModelAutoImported") :
            self._modelAutoImported = fobj.getParameter ("ModelAutoImported")
        else:
            self._modelAutoImported = False                
        if fobj.hasParameter ("shouldApplyModelOnImaging") :
            self._shouldApplyModelOnImaging = fobj.getParameter ("shouldApplyModelOnImaging")
        else:
            self._shouldApplyModelOnImaging = False
        
    def _L2NormalizeGradient(self, x, IgnoreNan = False):
        # utility function to normalize a tensor by its L2 norm        
        if IgnoreNan :
            validIndex = np.logical_not(np.isnan (x)) 
            result = np.copy(x)
            result[...] = np.nan
            if np.any (validIndex) :                
                result[validIndex] = x[validIndex] / (np.sqrt(np.average(np.square(x[validIndex]))) + 1e-5)
            return result
        else:
            return x / (np.sqrt(np.average(np.square(x))) + 1e-5)
    
    def _findOptimal (self, start, stop, precentageStep, gradientMap, maxV,App) :
        maxIndex = start
        maxDiff = None
        start = min (max (start, 0), 100)
        stop = min (max (stop, 0), 100)
        lastCount = -1
        index = float(start)
        maxV = float (maxV)
        for index in range (start, stop + precentageStep, precentageStep) :
            if App is not None :
                App.processEvents ()
            thresholdValue = maxV * (float (index) / 100.0)
            thresholdValue = min (max (0, thresholdValue),maxV)
                
            count = np.sum (gradientMap >= thresholdValue)
            if (lastCount < 0) :
                lastCount = count
            else:
                diff = (lastCount - count)
                lastCount = count
                if maxDiff is None or (maxDiff < diff):
                    maxIndex = index
                    maxDiff = diff
        return maxIndex
                    
    
    def computeOptimalColorizeGradientClipThreshold (self, gradientMap, App = None):
        nonNanIndex = np.logical_not(np.isnan (gradientMap))
        if not np.any (nonNanIndex) :
            return None
        nonNanGradMap = gradientMap[nonNanIndex]
        minV = np.min (nonNanGradMap)   
        nonNanGradMap -=  float (minV)        
        maxV = float (np.max (nonNanGradMap))     
       
        optimalPoint = self._findOptimal (0, 100, 20, nonNanGradMap, maxV, App)
        optimalPoint = self._findOptimal (optimalPoint-20, optimalPoint, 5, nonNanGradMap, maxV,App)
        optimalPoint = self._findOptimal (optimalPoint-5, optimalPoint, 1, nonNanGradMap, maxV,App)
        return min (max (optimalPoint, 0), 100)
    
    #@staticmethod
    #def castToArrayType (val, array) :
    #    return (np.array ([val],dtype=array.dtype))[0]        
        
    def colorizeGradientMap (self, gradientMap, clipPrecent = None, BW = False, ColorByOrder=True, scaleWithinRange = True, ClipGradientMask = None, ZeroCenter = False):  
        if (gradientMap is None) :
            return None                                  
        if ClipGradientMask is None :
            ClipGradientMask = np.copy (gradientMap)
        else:
            ClipGradientMask = np.copy (ClipGradientMask)
            
        nonNanIndex = np.logical_not(np.isnan (gradientMap))
        if not np.any (nonNanIndex) :
            return None
        else:                    
            colorized = np.copy (gradientMap)        
            if not ZeroCenter :
                minV = float (np.min (gradientMap[nonNanIndex]))            
                colorized[nonNanIndex] -= minV        
            maxV = float (np.max (colorized[nonNanIndex]))            
            nanIndex = np.logical_not (nonNanIndex)
            
            if (clipPrecent is not None) :
                #sortedArray = np.sort (colorized.flatten ())                
                #thresholdValue = sortedArray[int ((sortedArray.shape[0]-1) * clipPrecent)]
                minV = float (np.min (ClipGradientMask[nonNanIndex]))
                np.array([])
                ClipGradientMask[nonNanIndex] -= minV
                maxClipGradient = float (np.max (ClipGradientMask[nonNanIndex]))
                thresholdValue = maxClipGradient * clipPrecent
                clipIndex =  np.zeros (ClipGradientMask.shape, dtype = np.bool)
                clipIndex[nonNanIndex] = ClipGradientMask[nonNanIndex] < thresholdValue
                clipIndex[nanIndex] = True
            else :
                clipIndex = nanIndex

            nonNanIndex = np.logical_not (clipIndex)
            if (np.any (nonNanIndex)) :
                if (ZeroCenter) :         
                    maxV = np.max (np.abs(colorized[nonNanIndex]))
                    if (maxV <= 0) :
                        colorized[nonNanIndex] = 0
                    else:
                        colorized[nonNanIndex] /= maxV
                        colorized[nonNanIndex] += 1.0            
                        colorized[nonNanIndex] /= 2.0
                        colorized[nonNanIndex] = 255.0 * (1.0 - colorized[nonNanIndex])
                        colorized[nonNanIndex] = np.clip (colorized[nonNanIndex], 0,255)
                else:
                    if (ColorByOrder):                
                        colorized[nonNanIndex] = rankdata(colorized[nonNanIndex], method='min').astype (np.float) - 1.0                
                        maxV =  np.max (colorized[nonNanIndex])
                    
                    if (scaleWithinRange) :
                        minV = np.min (colorized[nonNanIndex])
                        colorized[nonNanIndex] -= minV
                        maxV = np.max(colorized[nonNanIndex])
                        
                    colorized[nonNanIndex] *= -255.0 / (maxV + sys.float_info.epsilon)#  255.0 - (255.0* colorized)    
                    colorized[nonNanIndex] += 255.0        
                
            
            if (not BW) :
                output = cv2.applyColorMap(colorized.astype (np.uint8)  , cv2.COLORMAP_RAINBOW) 
            else:
                output = np.zeros ((colorized.shape[0], colorized.shape[1], 3), dtype = np.uint8)
                colorized = colorized.astype (np.uint8)
                for index in range (3) :
                    output[:,:,index] = colorized
                
            output = output.astype (np.int)
            if np.any (clipIndex) : 
                for index in range (3) :
                    mask = output[:,:, index] 
                    mask[clipIndex] = -1
                    output[:,:, index] = mask                
            return output            
            
    def getModelVisualizationLayers (self) :
        model = self._kerasModel.getKerasModelForInspection (LinearOutput = True)
        if (model is None) :
            return None
        ML_Layers = model.layers
        listOfConvLayers = []
        activationExclusionList = ["linear"]
        try :
            Conv2DType = tf.keras.layers.Conv2D        
            ActivationType = tf.keras.layers.Activation
            RELUType = tf.keras.layers.ReLU
            LeakyRELUType = tf.keras.layers.LeakyReLU 
            PReLUType = tf.keras.layers.PReLU 
            ELUType = tf.keras.layers.ELU 
            THReluType =tf.keras.layers.ThresholdedReLU  
            SoftMaxType = tf.keras.layers.Softmax             
        except:
            Conv2DType = keras.layers.convolutional.Conv2D    
            ActivationType = keras.layers.core.Activation
            RELUType = keras.layers.ReLU
            LeakyRELUType = keras.layers.LeakyReLU 
            PReLUType = keras.layers.PReLU 
            ELUType = keras.layers.ELU 
            THReluType =keras.layers.ThresholdedReLU  
            SoftMaxType = keras.layers.Softmax 
        
        for layerIndex,layer in enumerate(ML_Layers):
            layerType = type(layer)                                   
            if layerType is Conv2DType:
                if layer.activation.__name__ not in activationExclusionList :
                    listOfConvLayers.append ((layerIndex, layer))
            elif layerType is ActivationType :
                if layer.activation.__name__ not in activationExclusionList and (len (layer.output_shape) == 4 or len (layer.output_shape) == 5):
                    listOfConvLayers.append ((layerIndex,layer))
            elif layerType is LeakyRELUType or layerType is PReLUType or layerType is ELUType or layerType is THReluType or layerType is SoftMaxType :
                listOfConvLayers.append ((layerIndex,layer))
            else:
                try :
                    if layerType is RELUType :
                        listOfConvLayers.append ((layerIndex,layer))
                except:
                    pass                            
        return listOfConvLayers
    
    def getModelLayerName (self, layer) :
        return layer.name
    
    def doesModelHaveCustomCodeWithLinearActivation (self) :
        try :
            return self._kerasModel.canCustomLoadLinearKerasModelFromSource ()
        except:
            return False
    

            

 
    
    def isLastLayerSigmoidOrSoftMax (self) :
        try :
            model = self._kerasModel.getKerasModelForInspection ()
            if (model is None) :
                return False
            LastLayer = model.layers[-1]
            Result =  (LastLayer.activation.__name__ == "softmax") or (LastLayer.activation.__name__ == "sigmoid")
            return Result
        except :
            return False
    
   
    def _getCAMLayer (self, model) :
        try :
          
            LastLayer = model.layers[-1]
            layer = LastLayer
            while layer != None :
                if type(layer) == keras.layers.pooling.GlobalAveragePooling2D or type(layer) == keras.layers.pooling.GlobalMaxPooling2D :
                    return layer     
                if type(layer) == keras.layers.Conv2D or type(layer) == keras.layers.Conv3D :
                    return None     
                if (len (layer._inbound_nodes) != 1) :
                    return False
                node = layer._inbound_nodes[0]
                if (len (node.inbound_layers) != 1) :
                    return None
                layer = node.inbound_layers[0]
        except:
            return None
        return None  
    
    
        
    def _computeCAM (self, sliceView, inputData, OutputIndex, isChannelLast, rawImageData, App = None):
        if App is not None :
            App.processEvents ()
        kerasModelEnv = self._kerasModel.getKerasModelEnviroment (LinearOutput=True)
        if kerasModelEnv is None :
            return None
        graph, session = kerasModelEnv.getGraphAndSession ()
        defaultSession = SessionWrapper.get_session()
        try :
            with graph.as_default():
                SessionWrapper.set_session(session)
                with session.as_default():
                    model = kerasModelEnv.getKerasModel ()
                    if (model is not None) :
                        CAMInputLayer = self._getCAMLayer (model)
                        LastLayer = model.layers[-1]
                    
                        computeChannelWeight = keras.backend.function([CAMInputLayer.output], [LastLayer.output])    
                        channelMask = np.zeros ((1, CAMInputLayer.output.shape[1]), dtype = np.float)
                        
                        ChannelCount = CAMInputLayer.output.shape[1]
                        channelWeightList = np.zeros ((ChannelCount, LastLayer.output.shape[1]), dtype=np.float)
                        for channelIndex in range (CAMInputLayer.output.shape[1]) :
                            if App is not None :
                                App.processEvents ()
                            channelMask[0,channelIndex] = 1.0
                            if channelIndex > 0 :
                                 channelMask[0,channelIndex-1] = 0.0
                            channelWeight = computeChannelWeight ([channelMask])[0] 
                            channelWeightList[channelIndex,:] = channelWeight[0,:]
                       
                        computeCAMLayerInput = keras.backend.function([model.input], [CAMInputLayer.input])    
                        finalLayerChannel = computeCAMLayerInput ([inputData])[0]
                        CAMResult = None
                        for channel_index in range (ChannelCount) :
                            if App is not None :
                                App.processEvents ()
                            
                            if (len (finalLayerChannel.shape) == 4) : # if output is 2D
                                if isChannelLast :
                                   channelOutput = finalLayerChannel[0,:,:,channel_index]
                                else:
                                   channelOutput = finalLayerChannel[0,channel_index, :,:]
                            else:  # output is 3D
                                if isChannelLast :
                                   middleSlice = int ((finalLayerChannel.shape[3] -1) / 2)
                                   channelOutput = finalLayerChannel[0,:,:,middleSlice,channel_index]
                                else:
                                   middleSlice = int ((finalLayerChannel.shape[4] -1) / 2)
                                   channelOutput = finalLayerChannel[0,channel_index, :,:, middleSlice]
                            if (CAMResult is None) :
                                CAMResult = channelOutput * channelWeightList[channel_index, OutputIndex]
                            else:
                                CAMResult += channelOutput * channelWeightList[channel_index, OutputIndex]
                        if (CAMResult is not None) :
                            CAMResult -= np.min (CAMResult)
                            maxVal = np.max (CAMResult)
                            if (maxVal > 0) :
                                CAMResult /= maxVal
                            
                            if (isChannelLast) :
                                inputSliceShape = (int (inputData.shape [1]), int (inputData.shape [2]))
                            else:
                                inputSliceShape = (int (inputData.shape [2]), int(inputData.shape [3]))
                           
                            CAMResult = transform.resize (CAMResult, inputSliceShape , order = 0, mode = "reflect")        
                            sliceShape = sliceView.getSliceShape ()           
                            CAMResult = self._modelInputTransformation.reverse2D (CAMResult, sliceShape, CVal = np.nan)               
                        return CAMResult
                    return None
        finally:
            SessionWrapper.set_session(defaultSession)
            
    def doesModelSupportsCAM (self) :
        model = self._kerasModel.getKerasModelForInspection ()
        if model is None :
            return False
        LastLayer = model.layers[len (model.layers) - 1]
        if len (LastLayer.output.shape) != 2 :
            return False
        return (self._getCAMLayer (model) != None)
                
    def _getGradientFunction (self, model, inputData, OutputIndex, isChannelLast, PredictionGradientMask):
        try :
            if (self.isLastLayerSigmoidOrSoftMax () or self.doesModelHaveCustomCodeWithLinearActivation()) :
                LastLayer = model.layers[-1]
                OutputTensors = LastLayer.output
                OutputTensorShape = list (OutputTensors.shape)
                OutputTensorShape[0] = 1
                for index in range (len (OutputTensorShape)) :
                    try: 
                        OutputTensorShape[index] = int (OutputTensorShape[index])
                    except:                        
                        OutputTensorShape[index] = PredictionGradientMask.shape[index -1]
                                                
                inputGradent = np.zeros (tuple (OutputTensorShape), dtype=np.float32) 
                if (isChannelLast) :
                    if PredictionGradientMask is None :
                        inputGradent[...,OutputIndex] = 1
                    else:
                        maskShapeLst = list (PredictionGradientMask.shape)
                        maskShapeLst.append (OutputTensorShape[len (OutputTensorShape) - 1])
                        maskData = np.zeros (tuple (maskShapeLst), dtype=np.float32) 
                        maskData[...,OutputIndex] = (PredictionGradientMask != 0).astype (np.float)
                        inputGradent[0,...] = maskData
                else:
                    if PredictionGradientMask is None :
                        inputGradent[0,OutputIndex,...] = 1
                    else:
                        maskShapeLst = [OutputTensorShape[1]]
                        maskShapeLst += list (PredictionGradientMask.shape)
                        maskData = np.zeros (tuple (maskShapeLst), dtype=np.float32) 
                        maskData[OutputIndex,...] = (PredictionGradientMask != 0).astype (np.float)
                        inputGradent[0,...] = maskData
             
                return tf.gradients (OutputTensors, inputData, grad_ys = inputGradent)[0]  
            else:
                return None
        except:
            return None
    
    def _BuildSliceInputData (self, sliceView, singleSliceDataInputData, transformedVolume, sliceIndex, inputSliceBlockSizeMiddle, inputSliceBlockSize, ModelInputImageShape, rawImageData) :
        isChannelLast = self._modelInputOutputDescription.isChannelLast ()
        if (singleSliceDataInputData is None) :
           
           if self._modelInputOutputDescription.isInputData2D () :   
               if (isChannelLast) :
                   singleSliceDataInputData = np.zeros ((1,transformedVolume.shape[0],transformedVolume.shape[1],inputSliceBlockSize), dtype = transformedVolume.dtype)                                
               else:
                   singleSliceDataInputData = np.zeros ((1,inputSliceBlockSize,transformedVolume.shape[0],transformedVolume.shape[1]), dtype = transformedVolume.dtype)                            
           else:
               if (isChannelLast) :
                   singleSliceDataInputData = np.zeros ((1,transformedVolume.shape[0],transformedVolume.shape[1],inputSliceBlockSize,1), dtype = transformedVolume.dtype)                                
               else:
                   singleSliceDataInputData = np.zeros ((1,1,transformedVolume.shape[0],transformedVolume.shape[1],inputSliceBlockSize), dtype = transformedVolume.dtype)                     
                
        if self._modelInputOutputDescription.isInputData2D () :                           
            if (isChannelLast) :                 
                singleSliceDataInputData[0,:,:,inputSliceBlockSizeMiddle] = transformedVolume[:,:]
            else:                                                
                singleSliceDataInputData[0,inputSliceBlockSizeMiddle,:,:] = transformedVolume[:,:]
        else:
            if (isChannelLast) : 
                singleSliceDataInputData[0,:,:,inputSliceBlockSizeMiddle,0] = transformedVolume[:,:]
            else:                                
                singleSliceDataInputData[0,0,:,:,inputSliceBlockSizeMiddle] = transformedVolume[:,:]

        for sl, buffersliceIndex in enumerate (range (sliceIndex-inputSliceBlockSizeMiddle, sliceIndex-inputSliceBlockSizeMiddle + inputSliceBlockSize)) :
            if (buffersliceIndex != sliceIndex):
                transformedVolume = self._modelInputTransformation.apply2DTransform (sliceView, rawImageData, buffersliceIndex, ModelInputImageShape, False)
                if (transformedVolume is None) :
                    print ("Data Transformation Failed")
                    MessageBoxUtil.showMessage (WindowTitle="ML Prediction",WindowMessage = "Prediction failed in data transformation pre-processing.")
                    return singleSliceDataInputData, "TransformFailed"
                        
                transformedVolume = self._modelInputNormalization.apply2D  (transformedVolume, rawImageData)
                if (transformedVolume is None) :
                    print ("Normalization Failed")
                    MessageBoxUtil.showMessage (WindowTitle="ML Prediction",WindowMessage = "Prediction failed in data normalization pre-processing. If custom code is being run check normalization code.") 
                    return singleSliceDataInputData, "NormFailed"
                if self._modelInputOutputDescription.isInputData2D () :   
                    if (isChannelLast) :
                        singleSliceDataInputData[0,:,:,sl] = transformedVolume[:,:]
                    else:
                        singleSliceDataInputData[0,sl,:,:] = transformedVolume[:,:]
                else:
                    if (isChannelLast) :
                        singleSliceDataInputData[0,:,:,sl,0] = transformedVolume[:,:]
                    else:
                        singleSliceDataInputData[0,0,:,:,sl] = transformedVolume[:,:]         
           
        singleSliceDataInputData[0,...] = self._modelInputNormalization.applyColorNorm2D  (singleSliceDataInputData[0,...], rawImageData)
        return singleSliceDataInputData, None    
        
            

    
    
    def _computeGenericCamMap (self, sliceView, isChannelLast, OutputIndex, singleSliceDataInputData, rawImageData, layerIndex, GradCamMap = False, SaliencyClassActivationMap = False, PredictionGradientMask = None, App = None, SaliencyClassActivationMapSignMask = None, ExportRelativeActivationSAMMap = False)  :        
        if (App is not None) :
                App.processEvents ()
        kerasModelEnv = self._kerasModel.getKerasModelEnviroment (LinearOutput=True)
        if kerasModelEnv is None :
            return None
        graph, session = kerasModelEnv.getGraphAndSession ()
        defaultSession = SessionWrapper.get_session()
        try:
            with graph.as_default():
                SessionWrapper.set_session(session)
                with session.as_default():
                    model = kerasModelEnv.getKerasModel ()
                    targetLayer = model.get_layer(index = layerIndex)#model.get_layer(name = layerName)
                    inputData = model.input
                    ImageGrad = self._getGradientFunction (model, targetLayer.output, OutputIndex, isChannelLast, PredictionGradientMask)     
                    if ImageGrad is None :
                        return None
                    saliencyMapFunction = keras.backend.function([inputData], [targetLayer.output ,ImageGrad])    
                    try :                                         
                        layerOutput, gradientPrediction = saliencyMapFunction ([singleSliceDataInputData])            
                        layerOutput = layerOutput[0]
                        gradientPrediction = gradientPrediction[0]
                    except:
                        return None
                    if (len (gradientPrediction.shape) != 3 and len (gradientPrediction.shape) != 4)  or (len (layerOutput.shape) != 3 and len (layerOutput.shape) != 4):
                        return None
                            
                    gradientPrediction = self._L2NormalizeGradient (gradientPrediction, IgnoreNan = False)           
                    if len (layerOutput.shape) == 3 : # 2D
                        if (isChannelLast) :
                           mapsize = float (gradientPrediction.shape[0] * gradientPrediction.shape[1])
                           layerOutputShape =  (layerOutput.shape[0], layerOutput.shape[1])
                           channelCount = gradientPrediction.shape[2]
                        else:
                           mapsize = float (gradientPrediction.shape[1] * gradientPrediction.shape[2])
                           layerOutputShape =  (layerOutput.shape[1], layerOutput.shape[2])
                           channelCount = gradientPrediction.shape[0]
                    elif len (layerOutput.shape) == 4 : # 3D
                        if (isChannelLast) :
                           mapsize = float (gradientPrediction.shape[0] * gradientPrediction.shape[1] * gradientPrediction.shape[2])
                           layerOutputShape =  (layerOutput.shape[0], layerOutput.shape[1], gradientPrediction.shape[2])
                           channelCount = gradientPrediction.shape[3]
                        else:
                           mapsize = float (gradientPrediction.shape[1] * gradientPrediction.shape[2]* gradientPrediction.shape[3])
                           layerOutputShape =  (layerOutput.shape[1], layerOutput.shape[2], gradientPrediction.shape[3])
                           channelCount = gradientPrediction.shape[0]
                        
                    outputmap = np.zeros (layerOutputShape, dtype = np.float)  
                    TotalGradientMask = None
                    if (ExportRelativeActivationSAMMap) :
                        NegativeLayerOutputCorrection = False
                    else:
                        NegativeLayerOutputCorrection = np.min (layerOutput) < 0
                    for classIndex in range (channelCount) :
                        if (App is not None) :
                            App.processEvents ()
                        
                        if len (layerOutput.shape) == 3 : # 2D
                            if (isChannelLast) :
                                layerChannelOutput = layerOutput[:, :, classIndex]
                                gradientChannel = gradientPrediction[ :, :, classIndex]
                            else:
                                layerChannelOutput = layerOutput[classIndex, :, :]
                                gradientChannel = gradientPrediction[ classIndex, :, :]
                        elif len (layerOutput.shape) == 4 : # 3D
                            if (isChannelLast) :
                                layerChannelOutput = layerOutput[:, :, :, classIndex]
                                gradientChannel = gradientPrediction[ :, :, :, classIndex]
                            else:
                                layerChannelOutput = layerOutput[classIndex, :, :, :]
                                gradientChannel = gradientPrediction[ classIndex, :, :, :]
                        #layerD = np.clip(layerD,0,np.max (layerD))
                        if (np.any (gradientChannel) and np.any (layerChannelOutput)) :
                          
                            if (SaliencyClassActivationMap) :        
                                correctedGradient = gaussian(gradientChannel.astype(np.float), sigma=0.75)                                                                                                         
                                lessThanZeroIndex = correctedGradient < 0
                                greaterThanZeroIndex = correctedGradient > 0      
                                if NegativeLayerOutputCorrection :
                                    if (TotalGradientMask is None):
                                        TotalGradientMask = correctedGradient != 0
                                    else:
                                        TotalGradientMask[correctedGradient != 0] = True
                                if (SaliencyClassActivationMapSignMask is None or (1 in SaliencyClassActivationMapSignMask and -1 in SaliencyClassActivationMapSignMask)) :
                                    if NegativeLayerOutputCorrection :
                                        layerChannelOutput -= np.min (layerChannelOutput)
                                    layerDMax = np.max (layerChannelOutput)
                                    invertOutput = -layerDMax + layerChannelOutput
                                    invertOutput[invertOutput > 0] = 0
                                    if (ExportRelativeActivationSAMMap) :
                                        invertOutput = np.abs (invertOutput)
                                    lessThanZeroIndex = correctedGradient < 0
                                    greaterThanZeroIndex = correctedGradient > 0      
                                    outputmap[lessThanZeroIndex] += invertOutput[lessThanZeroIndex] * correctedGradient[lessThanZeroIndex] 
                                    outputmap[greaterThanZeroIndex] += layerChannelOutput[greaterThanZeroIndex] * correctedGradient[greaterThanZeroIndex]     
                                    #print ("Pos and negat")
                                elif (-1 in SaliencyClassActivationMapSignMask) :
                                    if NegativeLayerOutputCorrection :
                                        layerChannelOutput -= np.min (layerChannelOutput)
                                    #layerDMax = np.max (layerChannelOutput)
                                    #invertOutput = -layerDMax + layerChannelOutput
                                    invertOutput = -layerChannelOutput
                                    invertOutput[invertOutput > 0] = 0
                                    if (ExportRelativeActivationSAMMap) :
                                        invertOutput = np.abs (invertOutput)
                                    lessThanZeroIndex = correctedGradient < 0                            
                                    outputmap[lessThanZeroIndex] += invertOutput[lessThanZeroIndex] * correctedGradient[lessThanZeroIndex] 
                                    #print ("negat")
                                elif (1 in SaliencyClassActivationMapSignMask) :
                                    if NegativeLayerOutputCorrection :
                                        layerChannelOutput -= np.min (layerChannelOutput)
                                    greaterThanZeroIndex = correctedGradient > 0      
                                    outputmap[greaterThanZeroIndex] += layerChannelOutput[greaterThanZeroIndex] * correctedGradient[greaterThanZeroIndex]      
                                    #print ("Pos")
                            elif (GradCamMap) :
                                #GradCam
                                classweight = float (np.sum (gradientChannel)) / mapsize    
                                outputmap += classweight * layerChannelOutput 
                            else:
                                return None
                    
                    if (SaliencyClassActivationMap and NegativeLayerOutputCorrection) :      
                        if (TotalGradientMask is not None) :
                            outputmap[TotalGradientMask] -= np.min (outputmap[TotalGradientMask]) 
                    
                    if  not ExportRelativeActivationSAMMap :                        
                        maxV = np.max (outputmap)            
                        np.clip (outputmap, 0, maxV, out = outputmap)        
                
                    try :
                        if (isChannelLast) :
                            inputSliceShape = (int (inputData.shape [1]), int (inputData.shape [2]))
                        else:
                            inputSliceShape = (int (inputData.shape [2]), int(inputData.shape [3]))
                    except:
                        inputSliceShape = (int (singleSliceDataInputData.shape[1]), int (singleSliceDataInputData.shape[2]))
                    if len (layerOutput.shape) == 4 : # 3D
                        middleSlice = int ((outputmap.shape[2] -1) / 2)
                        outputmap = outputmap[:,:,middleSlice]
                    
                    outputmap = transform.resize (outputmap, inputSliceShape , order = 0, mode = "reflect")        
                    sliceShape = sliceView.getSliceShape ()    
                    outputmap = self._modelInputTransformation.reverse2D (outputmap, sliceShape, CVal = np.nan)                                                                   
                    return outputmap
        finally:            
           SessionWrapper.set_session(defaultSession)
    
    def _generateIntegradedGradBaselineRawImage (self, inputvolume, IntegratedGradientsFunction) :
                if IntegratedGradientsFunction is not None :
                    if IntegratedGradientsFunction == "zero" :
                        return np.zeros (inputvolume.shape, dtype = inputvolume.dtype)
                    elif IntegratedGradientsFunction == "blur" :                
                        return  gaussian (inputvolume, sigma = 25, multichannel = False, preserve_range = True)
                    elif IntegratedGradientsFunction == "gaussian" :                                
                        return inputvolume + np.random.normal (scale = 3.0 * np.std (inputvolume), size = inputvolume.shape)
                    
    def _getSaliencyOutputSlice (self, gradientPrediction, isChannelLast):
        if len (gradientPrediction.shape) == 3 : # 2D output 
            if (isChannelLast) :
                middleGradientPrediction = gradientPrediction.shape[2]
                middleGradientPrediction = int ((middleGradientPrediction -1) / 2)
                gradientPrediction = gradientPrediction[:,:,middleGradientPrediction]
            else:
                middleGradientPrediction = gradientPrediction.shape[0]
                middleGradientPrediction = int ((middleGradientPrediction -1) / 2)
                gradientPrediction = gradientPrediction[middleGradientPrediction,:,:]
                
        elif len (gradientPrediction.shape) == 4 : # 3D output 
            if (isChannelLast) :
                middleGradientPrediction = gradientPrediction.shape[2]
                middleGradientPrediction = int ((middleGradientPrediction -1) / 2)
                gradientPrediction = gradientPrediction[:,:,middleGradientPrediction,0]
            else:
                middleGradientPrediction = gradientPrediction.shape[3]
                middleGradientPrediction = int ((middleGradientPrediction -1) / 2)
                gradientPrediction = gradientPrediction[0,:,:,middleGradientPrediction]
        return gradientPrediction
    
    def _computeIntegratedGradient (self, sliceView, IntegratedGradientsFunction,IntegratedGradientsSteps, sliceIndex, rawImageData, singleSliceDataInputData, inputSliceBlockSizeMiddle, inputSliceBlockSize, ModelInputImageShape, predictionOutputChannel, isChannelLast, predictionMask,  App, ProgressDialog) :
            if ProgressDialog is not None:
                if ProgressDialog.wasCanceled() :
                    return None
                ProgressDialog.setValue (0)
            stepCount = float (IntegratedGradientsSteps)
            sliceShape = sliceView.getSliceShape ()  
            
            transformedVolumeTargetImage = np.zeros (singleSliceDataInputData.shape, dtype = rawImageData.dtype)
  
            for sl, buffersliceIndex in enumerate (range (sliceIndex-inputSliceBlockSizeMiddle, sliceIndex-inputSliceBlockSizeMiddle + inputSliceBlockSize)) :
                transformedVolume = self._modelInputTransformation.apply2DTransform (sliceView, rawImageData, buffersliceIndex, ModelInputImageShape, False)
                if (transformedVolume is None) :
                    print ("Data Transformation Failed")
                    continue
                if self._modelInputOutputDescription.isInputData2D () :   
                    if (isChannelLast) :
                        transformedVolumeTargetImage[0,:,:,sl] = transformedVolume[:,:]
                    else:
                        transformedVolumeTargetImage[0,sl,:,:] = transformedVolume[:,:]
                else:
                    if (isChannelLast) :
                        transformedVolumeTargetImage[0,:,:,sl,0] = transformedVolume[:,:]
                    else:
                        transformedVolumeTargetImage[0,0,:,:,sl] = transformedVolume[:,:]         
            SliceCount = sl + 1
            
            integradedGradientBaselineRawImage =  self._generateIntegradedGradBaselineRawImage (transformedVolumeTargetImage, IntegratedGradientsFunction)            
            
            def generateIntegradedGradientBaselineNormalizedImage (integradedGradientBaselineRawImage, integradedGradientBaselineNormImage = None):
                if integradedGradientBaselineNormImage is None :
                    integradedGradientBaselineNormImage = np.copy (integradedGradientBaselineRawImage)
                for sl in range (SliceCount) :
                    if self._modelInputOutputDescription.isInputData2D () :   
                        if (isChannelLast) :
                            transformedVolume = integradedGradientBaselineRawImage[0,:,:,sl] 
                        else:
                            transformedVolume = integradedGradientBaselineRawImage[0,sl,:,:] 
                    else:
                        if (isChannelLast) :
                            transformedVolume = integradedGradientBaselineRawImage[0,:,:,sl,0] 
                        else:
                            transformedVolume = integradedGradientBaselineRawImage[0,0,:,:,sl] 
                    transformedVolume = self._modelInputNormalization.apply2D  (transformedVolume, integradedGradientBaselineRawImage)
                    if self._modelInputOutputDescription.isInputData2D () :   
                        if (isChannelLast) :
                            integradedGradientBaselineNormImage[0,:,:,sl] = transformedVolume[:,:]
                        else:
                            integradedGradientBaselineNormImage[0,sl,:,:] = transformedVolume[:,:]
                    else:
                        if (isChannelLast) :
                            integradedGradientBaselineNormImage[0,:,:,sl,0] = transformedVolume[:,:]
                        else:
                            integradedGradientBaselineNormImage[0,0,:,:,sl] = transformedVolume[:,:]   
                return integradedGradientBaselineNormImage
            
            integradedGradientBaselineNormImage = generateIntegradedGradientBaselineNormalizedImage (integradedGradientBaselineRawImage)         
          
            if (App is not None) :
                App.processEvents ()
        
            kerasModelEnv = self._kerasModel.getKerasModelEnviroment (LinearOutput=True)
            if kerasModelEnv is None :
                return None
            graph, session = kerasModelEnv.getGraphAndSession ()
            defaultSession = SessionWrapper.get_session ()
            try :
                with graph.as_default():
                    SessionWrapper.set_session(session)
                    with session.as_default():
                        model = kerasModelEnv.getKerasModel ()
            
                        #saliencyModel = self._setModelOutputToLinearAndLoad (model)
                        inputData = model.input
                        ImageGrad = self._getGradientFunction (model, inputData, predictionOutputChannel, isChannelLast, predictionMask)
                        if ImageGrad is None :
                            return None
                        saliencyMapFunction = keras.backend.function([inputData], [ImageGrad])    
                       
                        def _saliency (inputData) :
                            try :                                         
                                gradientPrediction = saliencyMapFunction ([inputData])[0]            
                                gradientPrediction = gradientPrediction[0]
                            except:
                                return None
                            if len (gradientPrediction.shape) != 3 and len (gradientPrediction.shape) != 4 : 
                                return None
                            return self._getSaliencyOutputSlice (gradientPrediction, isChannelLast)
                          
                        
                        PriorPrediction = _saliency (integradedGradientBaselineNormImage) 
                        gradientPredictionSum = np.zeros (PriorPrediction.shape, dtype = np.float)
                        if PriorPrediction is None :
                            return PriorPrediction
                        intermediateSliceInput = None
                        if ProgressDialog is not None:
                            ProgressDialog.setRange (0, IntegratedGradientsSteps + 1)
                        for index in range (1, IntegratedGradientsSteps) :
                            #print (index)
                            if ProgressDialog is not None:
                                if ProgressDialog.wasCanceled() :
                                    return None
                                ProgressDialog.setValue (index)
                            inputRawImage = integradedGradientBaselineRawImage + (float (index) / stepCount) * (transformedVolumeTargetImage - integradedGradientBaselineRawImage)
                            intermediateSliceInput = generateIntegradedGradientBaselineNormalizedImage (inputRawImage, intermediateSliceInput)  
                            prediction = _saliency (intermediateSliceInput)
                            gradientPredictionSum += (prediction + PriorPrediction) / 2.0
                            PriorPrediction = prediction   
                            if (App is not None) :
                                App.processEvents ()
                        if ProgressDialog is not None:
                            if ProgressDialog.wasCanceled() :
                                return None
                        prediction = _saliency (singleSliceDataInputData)
                        gradientPredictionSum += (prediction + PriorPrediction) / 2.0
                        gradientPredictionSum /= float (IntegratedGradientsSteps)
            finally:
                SessionWrapper.set_session(defaultSession)
        
            baseline2DInput = self._getSaliencyOutputSlice (integradedGradientBaselineNormImage[0,...], isChannelLast)
            final2DInput = self._getSaliencyOutputSlice (singleSliceDataInputData[0,...], isChannelLast)
            gradientPrediction =   ((final2DInput-baseline2DInput) * gradientPredictionSum) #  np.abs
            gradientPrediction = self._modelInputTransformation.reverse2D (gradientPrediction, sliceShape, CVal = np.nan)             
            print ("Done computing IG")
            return gradientPrediction
        
    def _computeSaliencyMap (self, sliceView, isChannelLast, OutputIndex, singleSliceDataInputData, PredictionGradientMask = None, App = None)    :        
        if (App is not None) :
            App.processEvents ()
        
        kerasModelEnv = self._kerasModel.getKerasModelEnviroment (LinearOutput=True)
        if kerasModelEnv is None :
            return None
        graph, session = kerasModelEnv.getGraphAndSession ()
        defaultSession = SessionWrapper.get_session ()
        try :
            with graph.as_default():
                SessionWrapper.set_session(session)
                with session.as_default():
                    model = kerasModelEnv.getKerasModel ()
        
                    #saliencyModel = self._setModelOutputToLinearAndLoad (model)
                    inputData = model.input
                    ImageGrad = self._getGradientFunction (model, inputData, OutputIndex, isChannelLast, PredictionGradientMask)
                    if ImageGrad is None :
                        return None
                    saliencyMapFunction = keras.backend.function([inputData], [ImageGrad])    
                    try :                                         
                        gradientPrediction = saliencyMapFunction ([singleSliceDataInputData])[0]            
                        gradientPrediction = gradientPrediction[0]
                    except:
                        return None
                    if len (gradientPrediction.shape) != 3 and len (gradientPrediction.shape) != 4 : 
                        return None
                    
                    gradientPrediction = self._getSaliencyOutputSlice (gradientPrediction, isChannelLast)
                    
                    if (App is not None) :
                        App.processEvents ()
                    sliceShape = sliceView.getSliceShape ()  
                    gradientPrediction = self._modelInputTransformation.reverse2D (gradientPrediction, sliceShape, CVal = np.nan) 
                    #return self._L2NormalizeGradient (gradientPrediction, IgnoreNan = True)    
                    return gradientPrediction   
        finally:
            SessionWrapper.set_session(defaultSession)
               
        


class ML_PredictionMap :
    def __init__ (self, initFromTree = None) :
        if (initFromTree is None) :
            self._PredictionTree = {}
        else:
            self._PredictionTree = initFromTree
    
    def getTree (self) :
        return self._PredictionTree 
    
    def copy (self) :
        newROITree = {}
        for index,value in self._PredictionTree.items () :            
            if value is None :
                newROITree[index] = None
            else:
                newROITree[index] = {}
                for pred_key, predvalue in value.items () :
                    newROITree[index][pred_key] = predvalue
        return ML_PredictionMap (newROITree)
        
    def getFileObject (self) :
        fobj = FileObject ("PredictionMap")  
        fobj.setParameter ("Map", self._PredictionTree)
        return fobj
    
    @staticmethod
    def loadFromFileObject (fobj) :
        fileMap = fobj.getParameter ("Map")
        ml = ML_PredictionMap ()
        ml._PredictionTree = {}
        for key,val in fileMap.items () :
            try :
                ml._PredictionTree[int (key)] = val
            except:
                ml._PredictionTree[key] = val
        return ml
    
    def setPredictionMap (self, newMap):
        changed = False
        if (newMap is None and len (self._PredictionTree) > 0) :
            changed = True        
        elif len (self._PredictionTree) != len (newMap._PredictionTree) :
            changed = True
        if (not changed and len (self._PredictionTree) > 0) :
            for key in self._PredictionTree.keys () :
                if key not in newMap._PredictionTree :
                    changed = True
                    break
                else:
                    val  = self._PredictionTree[key]
                    val2 = newMap._PredictionTree[key]
                    if (val is None and val2 is None):
                        continue
                    if ((val is None) and (val2 is not None)) or ((val2 is not None) and (val is None)) :
                        changed = True
                        break
                    if len (val) != len (val2) :
                        changed = True
                        break                    
                    for eK, eVal in val.items() :
                        if eK not in val2 :
                            changed = True
                            break
                        if val2[eK] != eVal :
                            changed = True
                            break                    
                    if (changed) :
                        break
        if changed :
            self._PredictionTree = {} 
            for key, val in newMap._PredictionTree.items ():
                if val is None :
                    self._PredictionTree[key] =  None
                else:
                    self._PredictionTree[key] =  {}
                    for ke, ve in val.items () :
                        self._PredictionTree[key][ke] = ve                    
            return True
        return False
            
        
        
class ML_ApplyROI2DPrediction (ML_ModelPredictBase) :
    
    def __init__ (self, ProjectDataset) :
        ML_ModelPredictBase.__init__(self, ProjectDataset)
        self._PredictionMap = ML_PredictionMap () 
    
    def getType (self) :
        return "ML_ApplyROI2DPrediction"       
    
    def getModelType (self) :
        return "Segmentation"
    
    def isClassificationModel (self) :
        return False

    def getMultiSliceVoting (self) :
        return None
    
    def setMultiSliceVoting (self, voting):
        return
    
    def doesModelSupportApplicationOnImaging (self) :
        return len (self._PredictionMap.getTree ()) == 1
    
    def getPredictionHtmlDescription (self, thresholdset) :
        html = []
        first = True
        if self.shouldApplyModelOnImaging () :
             html += "</br>Applying model on imaging</br>"
        else:
            try:
                for key, tagDictionary in self._PredictionMap.getTree ().items ():           
                    if first :
                        first = False
                                                    
                        html += ["<table Width=500><tr><th>Model Output</th>"]
                        html += ["<th>ROI Name</th>"] 
                        html += ["<th>ROI Radlex</h>"]       
                        if thresholdset  :            
                            html += ["<th>Threshold</th>"]                           
                        html += ["</tr>"] 
                    html += ["<tr>"] 
                    html += ["<td style='text-align:center'>" + str (key) + "</td>"]            
                    if (tagDictionary is None) :
                        html += ["<td style='text-align:center'>Value not set</td>"]          
                        html += ["<td style='text-align:center'>Value not set</td>"]          
                        if thresholdset  :            
                            html += ["<td>Value not set</td>"]          
                    else:
                        try :
                            roidef = tagDictionary["ROIDef"]            
                            roi = ROIDef ()
                            roi.setDef (roidef)
                            name = roi.getName ()
                            radlex = roi.getRadlexID ()
                            color = roi.getColor ()
                            colorstr = ''.join('{:02x}'.format(clr) for clr in [int (color[0]), int(color[1]), int (color[2])])
                            colortagHex = colorstr.upper ()                            
                        except:
                            name = "Value not set"
                            radlex = "Value not set"   
                            colortagHex = ""                                                    
                        html += ["<td style='color:#"+colortagHex+"; text-align:center'>" +str (name) + "</td>"]
                        html += ["<td style='color:#"+colortagHex+"; text-align:center'>" + str (radlex)+  "</td>"]            
                        if thresholdset  :            
                            if "Threshold" in tagDictionary :
                                html += ["<td style='color:#"+colortagHex+"; text-align:center'>"+ str(tagDictionary["Threshold"]) +  "</td>"]          
                            else:
                                html += ["<td style='color:#"+colortagHex+"; text-align:center'>"+"Value not set</td>"]          
                    html += ["</tr>"]
                html += ["</table>"]
            except:
                html = ["<h3>A error occured generating summary</h3>"]
        return "".join (html)
    
   
    
    def isSegmentationModel (self) :
        return True
    
    def getTreePath (self) :        
        modelsTreePath = copy.copy (self._TreePath)
        if (modelsTreePath[0].lower () != "segmentation") :
            modelsTreePath = ["Segmentation"] + modelsTreePath
            self.setTreePath (modelsTreePath) 
        return modelsTreePath
                    
    
    def getFileObject (self) :
        fobj = FileObject ("ML_ApplyROI2DPrediction")                        
        fobj = ML_ModelPredictBase._addToFileObject (self, fobj)        
        fobj.addInnerFileObject (self._PredictionMap.getFileObject ())        
        return fobj
    
    @staticmethod
    def loadFromFileObj (fobj, ProjectDataset) :
        model = ML_ApplyROI2DPrediction (ProjectDataset)
        model._initFromFileObject (fobj, ProjectDataset)
        obj = fobj.getFileObject ("PredictionMap")    
        model._PredictionMap = ML_PredictionMap.loadFromFileObject (obj)
        return model    
    
    @staticmethod
    def isPredictionMapMissingUUID (model) : 
        GeneratedUUID = False        
        for value in model._PredictionMap.getTree().values () :
            if value is not None and "ROIDef" in value :
                if len (value["ROIDef"]) < 10 :
                    rBlank = ROIDef ()
                    rBlank.setDef (value["ROIDef"]) 
                    value["ROIDef"] = rBlank.getDef ()
                    GeneratedUUID = True
        return GeneratedUUID
    
    def getPredictionMap (self) :
        return self._PredictionMap.copy ()
    
    def getROIPredictionList (self):
        roiList = []
        for prediction in self._PredictionMap.getTree().values ():
            if (prediction is not None and "ROIDef" in prediction):
                predictionROI = ROIDef ()
                predictionROI.setDef (prediction["ROIDef"])
                predictionROI.setLocked (False)
                roiList.append (predictionROI)                            
        return roiList
    
    
    def copy (self) :
        model = ML_ApplyROI2DPrediction (self.getKerasModel ().getProjectDataset ())   
        ML_ModelPredictBase.innerCopy (self, model)
        model._PredictionMap = model._PredictionMap.copy ()
        return model                
    
    def setPredictionMap (self, predictionMap) :
        if self._PredictionMap.setPredictionMap (predictionMap) :
           self._modelChanged.append ("predictionMap")                 
    
    
    def _shouldQuantifySliceSegmentation (self, sliceIndex, roiDictionary, options) :                
        if self.shouldApplyModelOnImaging () :
            return False
        applyModelOn = options["QuantifyROIMetricsFor"]
        if (applyModelOn == "AllSlices") :
            return True
        sliceDictionary = roiDictionary.getROISliceDictionary ()
        return sliceIndex in sliceDictionary 
        """if (applyModelOn == "SlicesWithAnySegmentationData") :            
            return sliceIndex in sliceDictionary 
        if (applyModelOn == "SlicesWithROISegmentationData") :            
            return sliceIndex in sliceDictionary """
    
    def _shouldQuantifySliceSegmentationForROI (self, sliceIndex, roiDictionary, options, roiName) :                                                                                                   
        roiDefs = roiDictionary.getROIDefs ()
        if self.shouldApplyModelOnImaging () :
            return False
        if  roiDefs.isROIHidden (roiName) :
            return False
        applyModelOn = options["QuantifyROIMetricsFor"]
        if (applyModelOn == "AllSlices") :
            return True
        sliceDictionary = roiDictionary.getROISliceDictionary ()        
        if (sliceIndex not in sliceDictionary) :
            return False            
        if (applyModelOn == "SlicesWithAnySegmentationData") :            
            return True
        if (applyModelOn == "SlicesWithROISegmentationData") :            
            objList = sliceDictionary[sliceIndex]
            roiDefs = roiDictionary.getROIDefs ()
            for obj in objList :
                if obj.getName (roiDefs) == roiName :
                    return True
            return False
        

        
    def quantifyMLModelSegmentation (self, sliceSelectionWidget, sliceView, niftiVolume, roiDictionary, options, App = None, LimitSegmentationToSelectedROI = False, ProgressDialog = None) :                       
        resultLogTxtList = []
        resultLogTxtList.append ("<b> Segmentation Quantification</b>")
        resultLogTxtList.append ("<b>Name: </b>" + self.getModelName ())                
        resultLogTxtList.append ("")
        tableResults = None
        if not self._modelInputOutputDescription.isInputOutputDescriptionValid () : 
            resultLogTxtList.append ("")
            MessageBoxUtil.showMessage (WindowTitle="ML Prediction",WindowMessage = "Unknown model size a error occured loading the model.") 
            resultLogTxtList.append ("Unknown model size a error occured loading the model.")            
            return resultLogTxtList, tableResults        
        if (App is not None) :
            App.processEvents()                
        print ("Running Segmentation Quantification")
        
        ReturnColorNiftiVolumeData = False
        if niftiVolume.hasColorImageData () :
            inputDataSize = self._modelInputOutputDescription.getInputDataSize () 
            if len (inputDataSize) == 4 and inputDataSize[-1] == 3 :
                ReturnColorNiftiVolumeData = True
        self._modelInputNormalization.setColorNormMode (ReturnColorNiftiVolumeData)
        
        rawImageData =  sliceView.getNIfTIVolumeSliceViewProjection (ReturnColorNiftiVolumeData =ReturnColorNiftiVolumeData)    
        if options["ApplyToAllSlices"] == True:
            firstSlice = 0
            LastSliceRangeIndex = rawImageData.shape[2] - 1
        else:
            sliceRange = sliceSelectionWidget.getSelection ()
            firstSlice = min (sliceRange)
            LastSliceRangeIndex = max (sliceRange)
            
        isChannelLast = self._modelInputOutputDescription.isChannelLast ()                                    
        inputdata = None        
        predictedROIList = []
        predictionTree = self._PredictionMap.getTree ()                        
        predictionKeyList = list (predictionTree.keys ())                
        roiDefs = roiDictionary.getROIDefs ()
        ROIIdentifiedToSegment = False            
        ROITableList = []
        predictionROIKeyNameList = []        
        for outputValue in predictionKeyList :                            
            oldROIListLen = len (predictedROIList)
            modelROIDefName = "None"
            if predictionTree[outputValue] is not None :
                modelROIDef = predictionTree[outputValue]["ROIDef"]            
                roi = ROIDef ()
                roi.setDef (modelROIDef)
                del modelROIDef            
                modelROIDefName = roi.getName ()                
                roiName = roiDefs.findBestMatchingROIName (roi)                
                if (roiName != "None") :
                    roi = roiDefs.getROIDefCopyByName  (roiName)  
                    if (roi.getType ().lower () == "area") :                        
                        if not LimitSegmentationToSelectedROI or roiDefs.isROINameSelected (roiName) :                                            
                            if roiName in predictionROIKeyNameList :
                                predictionROIKeyNameList.append (roiName)
                                predictedROIList.append (predictionROIKeyNameList.index (roiName)) 
                            else:
                                predictionROIKeyNameList.append (roiName)
                                predictedROIList.append (roi) 
                                ROIIdentifiedToSegment = True                                
                                ROITableList.append ("<tr><td><center>" + roiName + "</center></td><td><center>"+ str(roi.getRadlexID ()) +"</center></td><td><center>" + roi.getType ()+"</center></td></tr>")
            if oldROIListLen == len (predictedROIList) :
                predictedROIList.append (None)  
                if LimitSegmentationToSelectedROI :                    
                    predictionROIKeyNameList.append ("None")
                else:
                    predictionROIKeyNameList.append (modelROIDefName)
        if (len (ROITableList) > 0) :
            ROITableList = ["<table width='100%'><tr><td><center><b>ROI Name</b></center><td><center><b>Radlex ID</b></center><td><center><b>ROI Type</b></center></td></tr>"] + ROITableList + ["</table>"]                
            resultLogTxtList.append ("".join(ROITableList))
        
        if not ROIIdentifiedToSegment :
            resultLogTxtList.append ("")
            resultLogTxtList.append ("No ROI selected for quantification. Make sure ML model has ROI defined and the desired ROI are selected if the limit segmentation to ROI selection option is enabled.")
            print ("Model has no segmentable ROI")           
            MessageBoxUtil.showMessage (WindowTitle="ML Segementation Quantification",WindowMessage = "Quantification not run. No ROI selected for segmentation. Make sure ML model has ROI defined and the desired ROI are selected if ROI selection options are enabled.") 
            return resultLogTxtList, tableResults
        
        inputSliceBlockSize = self._modelInputOutputDescription.getInputSliceBlockSize ()                
        isChannelLast = self._modelInputOutputDescription.isChannelLast ()                
        if ReturnColorNiftiVolumeData :
            inputSliceBlockSizeMiddle = 0
        else:
            inputSliceBlockSizeMiddle = int ((inputSliceBlockSize -1) / 2)
        ModelInputImageShape = self._modelInputOutputDescription.getInputImageShape ()
         
        if (ProgressDialog is not None) :
            ProgressDialog.setRange (0,LastSliceRangeIndex + 1 - firstSlice)                         
        
        resultLogTxtList.append ("")
        resultLogTxtList.append ("<b>Processing Slices: </b>")

        tableSummaryMetrics = {}                            
        tableLog = []
        tableLog.append ("<table width='100%'>")
        tableLog.append ("<th>")
        ROIWidth = 0
        ROIColCount = 0
        for outputValue in predictionKeyList :   
            if (App is not None) :
                App.processEvents()
            PredicitionROI = predictedROIList[outputValue]
            if (PredicitionROI is not None and not isinstance (PredicitionROI, int)) :
              ROIWidth += 1
        ROIColCount = ROIWidth
        ROIWidth += 1
        ROIWidth = str(float (100.0/ROIWidth))
        tableLog.append ("<td width='"+ROIWidth+"%'>")
        tableLog.append ("<center><b>Slice</b></center>")
        tableLog.append ("</td width='"+ROIWidth+"%'>")
        diceJacardTable = PandasCompatibleTable ()    
        diceJacardTableHeader = ["Slice"]
        for outputValue in predictionKeyList :   
            if (App is not None) :
                App.processEvents()
            PredicitionROI = predictedROIList[outputValue]
            if (PredicitionROI is not None and not isinstance (PredicitionROI, int)) :
                tableLog.append ("<td width='"+ROIWidth+"%'>")
                tableLog.append ("<table width='100%'>")
                tableLog.append ("<tr>")
                tableLog.append ("<td width='100%' colspan='2'>")
                roiName = PredicitionROI.getName ()
                tableSummaryMetrics[roiName] = {}
                tableSummaryMetrics[roiName]["ExistingMask"] = int (0)
                tableSummaryMetrics[roiName]["PredictedMask"] = int (0)
                tableSummaryMetrics[roiName]["MaskUnion"] = int (0)
                
                tableLog.append ("<center><b>"+roiName+"</b></center>")
                tableLog.append ("</td>")
                tableLog.append ("</tr>")
                tableLog.append ("<tr>")
                tableLog.append ("<td width='50%'>")
                tableLog.append ("<center><b>Dice</b></center>")
                tableLog.append ("</td>")
                tableLog.append ("<td width='50%'>")
                tableLog.append ("<center><b>Jacard</b></center>")
                tableLog.append ("</td>")
                tableLog.append ("</tr>")
                tableLog.append ("</table>")
                tableLog.append ("</td>") 
                diceJacardTableHeader.append (roiName + " (Dice)")
                diceJacardTableHeader.append (roiName + " (Jacard)")
        diceJacardTable.setColumnHeaderList (0,diceJacardTableHeader)
        tableLog.append ("</th>")
                              
        if ("PatchCenterPos" in options):
            cX, cY = options["PatchCenterPos"]
            cX, cY = sliceView.transformSliceMapPointToScreen (cX, cY)
            self.setModelInputTransformationPredictionPatchCenter (cX, cY)         
        
        dim = len (predictionKeyList)                
        if dim > 1 :
            ConfusionMatrix = np.zeros ((dim,dim), dtype=np.uint)    
        else:
            ConfusionMatrix = None            
        
        for sliceIndex in range (firstSlice, LastSliceRangeIndex + 1) :             
            TableSliceVector = []
            if (App is not None) :
                App.processEvents ()     
            if (ProgressDialog is not None) :
                ProgressDialog.setValue (sliceIndex - firstSlice)
                if ProgressDialog.wasCanceled () :
                    break
            tableLog.append ("<tr>")           
            if self._shouldQuantifySliceSegmentation (sliceIndex, roiDictionary, options) :                                                                                                   
                TableSliceVector.append (sliceIndex)
                tableLog.append ("<td width='"+ROIWidth+"%'><center>" + str (sliceIndex) + "</center></td>")
                transformedVolume = self._modelInputTransformation.apply2DTransform (sliceView, rawImageData, sliceIndex, ModelInputImageShape, True)      
                if (transformedVolume is None) :
                    tableLog.append ("<td width='100%' colspan='" +str(ROIColCount)+"'>Data transformaion failed.</td>")
                    TableSliceVector.append ("Data transformaion failed.")
                    print ("Data Transformation Failed")        
                    MessageBoxUtil.showMessage (WindowTitle="ML Segementation Quantification",WindowMessage = "Quantification failed in data transformation pre-processing.") 
                    break
                transformedVolume = self._modelInputNormalization.apply2D  (transformedVolume, rawImageData)
                if (transformedVolume is None) :
                    tableLog.append ("<td width='100%' colspan='" +str(ROIColCount)+"'>Data normalization failed.</td>")
                    TableSliceVector.append ("Data normalization failed.")
                    print ("Normalization Failed")            
                    MessageBoxUtil.showMessage (WindowTitle="ML Segementation Quantification",WindowMessage = "Quantification failed in data normalization pre-processing. If custom code is being run check normalization code.") 
                    break
                
                inputdata, returnResult = self._BuildSliceInputData (sliceView, inputdata, transformedVolume, sliceIndex, inputSliceBlockSizeMiddle, inputSliceBlockSize, ModelInputImageShape, rawImageData)
                if (returnResult is not None) :
                    if (returnResult == "TransformFailed") :
                        tableLog.append ("<td width='100%'  colspan='" +str(ROIColCount)+"'>Data transformaion failed.</td>")
                        TableSliceVector.append ("Data transformaion failed.")
                        break
                    elif (returnResult == "NormFailed") :
                        tableLog.append ("<td width='100%'  colspan='" +str(ROIColCount)+"'>Data normalization failed.</td>")
                        TableSliceVector.append ("Data normalization failed.")
                        break
                try :
                    slicePrediction,errorMsg = self._kerasModel.modelPredict  (inputdata, ReturnErrorMsg = True) 
                except Exception as msg:
                    slicePrediction = None
                    errorMsg = str (msg)
                if (slicePrediction is None) :
                    if errorMsg is not None :
                        WindowMessage = "Prediction failed. " + errorMsg
                    else:
                        WindowMessage = "Prediction failed."
                    tableLog.append ("<td width='100%'  colspan='" +str(ROIColCount)+"'>" + WindowMessage + "</td>")
                    TableSliceVector.append (WindowMessage)
                    print (WindowMessage)       
                    MessageBoxUtil.showMessage (WindowTitle="ML Segementation Quantification",WindowMessage = WindowMessage) 
                    break
                slicePrediction = slicePrediction[0,...]
                
                if (not self._modelInputOutputDescription.isModelOutput2D ()) : 
                    if (isChannelLast) :
                        #if output is 2D but on flattened 
                        outputsize = self._modelInputOutputDescription.getOutputDataSize ()
                        inputsize = self._modelInputOutputDescription.getInputDataSize ()
                        if (len (outputsize) == 3 and outputsize[1] == inputsize[1] * inputsize[2]):
                            slicePrediction = np.reshape (slicePrediction, (inputsize[1], inputsize[2],slicePrediction.shape[-1]))
                        else:
                            outputMiddle = int ((slicePrediction.shape[2] -1) / 2)
                            slicePrediction  = slicePrediction [:,:, outputMiddle, :]   
                    else:
                        outputMiddle = int ((slicePrediction.shape[3] -1) / 2)
                        slicePrediction = slicePrediction [:,:,:, outputMiddle]   
                                                       
                if self.getSinglePrediction () :
                    if (isChannelLast) :
                        maxResult = np.argmax (slicePrediction, axis = -1)                
                    else:
                        maxResult = np.argmax (slicePrediction, axis = 0)   
                                               
                ExistingSegmentationCombinedGroundTruth = None         
                backgroundPredictionIndex = None
                if ConfusionMatrix is not None :
                    for predictionIndex, outputValue in enumerate (predictionKeyList) :
                        PredicitionROI = predictedROIList[outputValue] 
                        if PredicitionROI is None :                   
                            backgroundPredictionIndex = predictionIndex
                        elif not isinstance (PredicitionROI, int) :
                            roiName = PredicitionROI.getName ()                                                                                     
                            if roiDictionary.isROIDefined (roiName):                                        
                              obj = roiDictionary.getROIObject (roiName)    
                              if (obj.isROIArea ()) :                                  
                                  existingSliceMap = obj.getSliceViewObjectMask (sliceView, SliceNumber = sliceIndex, ClipOverlayingROI = roiDictionary.getROIDefs ().hasSingleROIPerVoxel ())
                                  if existingSliceMap is not None :
                                      if ExistingSegmentationCombinedGroundTruth is None :
                                          ExistingSegmentationCombinedGroundTruth = np.zeros (existingSliceMap.shape, dtype=np.int)
                                          ExistingSegmentationCombinedGroundTruth[...] = -1
                                      if np.any (existingSliceMap) :
                                          ExistingSegmentationCombinedGroundTruth[existingSliceMap > 0] = predictionIndex                            
                
                if ExistingSegmentationCombinedGroundTruth is not None and backgroundPredictionIndex is not None :
                     ExistingSegmentationCombinedGroundTruth[ExistingSegmentationCombinedGroundTruth == -1] = backgroundPredictionIndex
                
                CombinedROIPredictons = {}                    
                for predictionIndex, outputValue in enumerate (predictionKeyList) :   
                    if (App is not None) :
                        App.processEvents()
                    PredicitionROI = predictedROIList[outputValue]
                    predictedValue = predictionTree[outputValue] 
                    if (isChannelLast) :
                        predictedMask = slicePrediction[:,:, outputValue]
                    else:
                        predictedMask = slicePrediction[outputValue, :,:]                                    
                    if self.getSinglePrediction () :
                        predictedMask = (outputValue == maxResult).astype (np.float) * predictedMask                                
                    if (self.getPredictionProbThreshold ()) : 
                        predictedMask = predictedMask >= predictedValue["Threshold"]
                    else:                                
                        predictedMask = predictedMask > 0
                    predictedMask = predictedMask.astype (np.uint8)                    
                    sliceShape = sliceView.getSliceShape ()
                    predictedMask = self._modelInputTransformation.reverse2D (predictedMask, sliceShape, ResizeOrder = 0) 
                    predictedMask = sliceView.transformImageScreenViewToSliceMap (predictedMask)
                    existingSliceMap = np.zeros ((predictedMask.shape[0],predictedMask.shape[1]), dtype=np.uint8)
                    if (PredicitionROI is None) :
                        if (existingSliceMap.shape == predictedMask.shape) :                               
                            if ExistingSegmentationCombinedGroundTruth is not None :
                                expectedGT_ForPrediction = ExistingSegmentationCombinedGroundTruth[predictedMask>0]                            
                                for GT in expectedGT_ForPrediction[expectedGT_ForPrediction != -1] :                                
                                    ConfusionMatrix[predictionIndex, GT ] += 1
                    else:
                        if not isinstance (PredicitionROI, int) :
                            roiName = PredicitionROI.getName ()                        
                            if not self._shouldQuantifySliceSegmentationForROI (sliceIndex, roiDictionary, options, roiName) :                                                                                                   
                                predictedMask[...] = 0
                            elif roiDictionary.isROIDefined (roiName):                                        
                                obj = roiDictionary.getROIObject (roiName)    
                                if (obj.isROIArea ()) :                                
                                    existingSliceMap = obj.getSliceViewObjectMask (sliceView, SliceNumber = sliceIndex, ClipOverlayingROI = roiDictionary.getROIDefs ().hasSingleROIPerVoxel ())
                        else:
                            DuplicateROI = predictedROIList[PredicitionROI]
                            roiName = DuplicateROI.getName ()                        
                            if not self._shouldQuantifySliceSegmentationForROI (sliceIndex, roiDictionary, options, roiName) :                                                                                                   
                                predictedMask[...] = 0
                        if roiName not in CombinedROIPredictons :
                            CombinedROIPredictons[roiName] = (predictedMask, existingSliceMap)
                        else:
                            CombinedROIPredictons[roiName][0][predictedMask > 0] = predictedMask[predictedMask > 0]
                        
                for predictionIndex, outputValue in enumerate (predictionKeyList) :
                    PredicitionROI = predictedROIList[outputValue]
                    if PredicitionROI is not None and not isinstance (PredicitionROI, int) :
                        roiName = PredicitionROI.getName ()        
                        predictedMask, existingSliceMap = CombinedROIPredictons[roiName]
                        if (existingSliceMap.shape == predictedMask.shape) :                              
                            #confusion start                       
                            if ExistingSegmentationCombinedGroundTruth is not None :
                                expectedGT_ForPrediction = ExistingSegmentationCombinedGroundTruth[predictedMask>0]                            
                                for GT in expectedGT_ForPrediction[expectedGT_ForPrediction != -1] :                                
                                    ConfusionMatrix[predictionIndex, GT ] += 1                                                      
                            #confusion end
                            
                            nonZeroCountExistingMask = np.sum (existingSliceMap )
                            nonZeroCountPredictedMask = np.sum (predictedMask )                                
                            if (nonZeroCountPredictedMask == 0 and nonZeroCountExistingMask == 0) :
                                diceMetric = None
                                jacardMetric = None
                            else:
                                unionSum = np.sum (predictedMask[existingSliceMap > 0])
                                
                                predictionROIName = PredicitionROI.getName ()
                                
                                tableSummaryMetrics[predictionROIName]["ExistingMask"] += int (nonZeroCountExistingMask)
                                tableSummaryMetrics[predictionROIName]["PredictedMask"] += int (nonZeroCountPredictedMask)
                                tableSummaryMetrics[predictionROIName]["MaskUnion"] += int (unionSum)              
                                diceMetric = (2.0 * float (unionSum)) / float (nonZeroCountExistingMask + nonZeroCountPredictedMask)
                                jacardMetric =  float (unionSum) / float (nonZeroCountExistingMask + nonZeroCountPredictedMask - unionSum)                                                                  
                            
                            tableLog.append ("<td width='"+ROIWidth+"%'>")
                            tableLog.append ("<table width='100%'>")
                            tableLog.append ("<tr>")
                            tableLog.append ("<td width='50%'>")
                            if (diceMetric is not None) :
                                tableLog.append ("<center>%0.3f</center>" % diceMetric)
                                TableSliceVector.append (diceMetric)
                            else:
                                TableSliceVector.append ("NA")
                                tableLog.append ("<center>NA</center>")
                            tableLog.append ("</td>")
                            tableLog.append ("<td width='50%'>")
                            if (jacardMetric is not None) :
                                tableLog.append ("<center>%0.3f</center>" % jacardMetric)
                                TableSliceVector.append (jacardMetric)
                            else:
                                TableSliceVector.append ("NA")
                                tableLog.append ("<center>NA</center>")
                            tableLog.append ("</td>")
                            tableLog.append ("</tr>")
                            tableLog.append ("</table>")
                            tableLog.append ("</td>")     
                        else:
                            tableLog.append ("<td width='"+ROIWidth+"%'  colspan='2'>Segmentation output shape and ground-truth segmenation shape do not match.</td>")
                del CombinedROIPredictons
            tableLog.append ("</tr>")  
            diceJacardTable.insertDataVector (diceJacardTable.getRowCount (), 0, TableSliceVector)    
            
        tableLog.append ("<tr>")
        tableLog.append ("<td width='"+ROIWidth+"%'><center><b>Summary</b></center></td>")  
        TableSliceVector = ["Volume summary"]
        for outputValue in predictionKeyList:   
                if (App is not None) :
                    App.processEvents()
                PredicitionROI = predictedROIList[outputValue]
                if (PredicitionROI is not None and not isinstance (PredicitionROI, int)) :
                    predictionROIName = PredicitionROI.getName ()
                    nonZeroCountExistingMask = tableSummaryMetrics[predictionROIName]["ExistingMask"]
                    nonZeroCountPredictedMask = tableSummaryMetrics[predictionROIName]["PredictedMask"]
                    unionSum = tableSummaryMetrics[predictionROIName]["MaskUnion"]                                              
                    if (nonZeroCountPredictedMask == 0 and nonZeroCountExistingMask == 0) :
                        diceMetric = None
                        jacardMetric = None                        
                    else:                            
                        diceMetric = (2.0 * float (unionSum)) / float (nonZeroCountExistingMask + nonZeroCountPredictedMask)
                        jacardMetric =  float (unionSum) / float (nonZeroCountExistingMask + nonZeroCountPredictedMask - unionSum)                                                                  
                        
                    tableLog.append ("<td width='"+ROIWidth+"%'>")
                    tableLog.append ("<table width='100%'>")
                    tableLog.append ("<tr>")
                    tableLog.append ("<td width='50%'>")
                    if (diceMetric is not None) :
                        tableLog.append ("<center><b>%0.3f</b></center>" % diceMetric)
                        TableSliceVector.append (diceMetric)
                    else:
                        tableLog.append ("<center><b>NA</b></center>")
                        TableSliceVector.append ("NA")
                    tableLog.append ("</td>")
                    tableLog.append ("<td width='50%'>")
                    if (jacardMetric is not None) :
                        tableLog.append ("<center><b>%0.3f</b></center>" % jacardMetric)
                        TableSliceVector.append (jacardMetric)
                    else:
                        tableLog.append ("<center><b>NA</b></center>")
                        TableSliceVector.append ("NA")
                    tableLog.append ("</td>")
                    tableLog.append ("</tr>")
                    tableLog.append ("</table>")                       
                    tableLog.append ("</td>")                             
        if len (TableSliceVector) > 1 :
            diceJacardTable.insertDataVector (diceJacardTable.getRowCount ()+2, 0, TableSliceVector)    
        if diceJacardTable is not None and diceJacardTable.getRowCount () > 0 :
            if tableResults is None :
                tableResults = {}
            tableResults["Dice/Jacard Metrics"] = diceJacardTable
        tableLog.append ("</tr>")                      
        tableLog.append ("</table>")                                           
        ConfusionKeyLen = len (predictionROIKeyNameList)
        if ConfusionMatrix is not None and ConfusionKeyLen  > 1 :
            confusionMatrixTable = PandasCompatibleTable ()            
            tableLog.append ("<hr><p><center><b>Prediction Confusion Matrix</b><center></p>")                                        
            tableLog.append ("<p><center>Predicted (Vertical) vs Truth (Horizontal)</center></p><p>")
            tableLog.append ("<table width='100%'>")            
            tableLog.append ("<tr><td><b><u>Prediction</u></b></td>")            
            tableheader = ["Prediction"]
            FirstColumnNone = True                        
            for truthIndex, key in enumerate(predictionROIKeyNameList) : 
                groundTruthROI = predictedROIList[truthIndex]       
                if not isinstance (groundTruthROI, int) :
                    if groundTruthROI is not None  or FirstColumnNone :
                        if groundTruthROI is None :
                            FirstColumnNone = False
                        headerName = str(key)
                        tableLog.append ("<td><b><u>"+headerName+" (Truth)</u></b></td>")
                        tableheader.append (headerName + " (Truth)")
                
                
            confusionMatrixTable.setColumnHeaderList (0, tableheader)
            tableLog.append ("</tr>")
            FirstNoneRow = True   
            predictionVectorCount = 0
            for predictedIndex in range (ConfusionKeyLen) :     
                if not isinstance (predictedROIList[predictedIndex], int) :
                    predictionName = str(predictionROIKeyNameList[predictedIndex])                
                    if predictionName != "None" :
                        datavector = []                                        
                        datavector.append (predictionName)                
                        FirstNoneColumn = True                    
                        for TruthIndex in range (ConfusionKeyLen) :                        
                            if predictedROIList[TruthIndex] is not None :                        
                                confusionValue = int (ConfusionMatrix[predictedIndex, TruthIndex])                            
                                datavector.append (confusionValue)
                            elif FirstNoneColumn :
                                FirstNoneColumn = False    
                                confusionValue = 0
                                for NoneTruthIndex in range (ConfusionKeyLen) :
                                    if predictedROIList[NoneTruthIndex] is None :
                                        confusionValue += int (ConfusionMatrix[predictedIndex, NoneTruthIndex])                            
                                datavector.append (confusionValue)
                        
                        tableLog.append ("<tr>")                                    
                        for confusionValue in datavector :
                            tableLog.append ("<td>")                            
                            tableLog.append (str(confusionValue))                    
                            tableLog.append ("</td>")
                        tableLog.append ("</tr>")            
                        confusionMatrixTable.insertDataVector (predictionVectorCount, 0, datavector)      
                        predictionVectorCount+=1                    
                    elif FirstNoneRow : 
                        FirstNoneRow = False
                        datavector = []                                    
                        datavector.append (predictionName)                    
                        for NonePredictedIndex in range (ConfusionKeyLen) :                
                            if "None" == str(predictionROIKeyNameList[NonePredictedIndex]) :
                                FirstNoneColumn = True                    
                                datavectorIndex = 1
                                for TruthIndex in range (ConfusionKeyLen) :                                
                                    if predictedROIList[TruthIndex] is not None :
                                        confusionValue = int (ConfusionMatrix[NonePredictedIndex, TruthIndex])                                                                        
                                        if datavectorIndex == len (datavector) :
                                            datavector.append (confusionValue)                                    
                                        else:
                                            datavector[datavectorIndex] += confusionValue
                                        datavectorIndex += 1
                                    elif FirstNoneColumn :
                                        FirstNoneColumn = False    
                                        confusionValue = 0
                                        for NoneTruthIndex in range (ConfusionKeyLen) :
                                            if predictedROIList[NoneTruthIndex] is None :
                                                confusionValue += int (ConfusionMatrix[NonePredictedIndex, NoneTruthIndex])                                    
                                        if datavectorIndex == len (datavector) :
                                            datavector.append (confusionValue)                                    
                                        else:
                                            datavector[datavectorIndex] += confusionValue
                                        datavectorIndex += 1                            
                        tableLog.append ("<tr>")
                        for confusionValue in datavector :
                            tableLog.append ("<td>")                            
                            tableLog.append (str(confusionValue))                    
                            tableLog.append ("</td>")
                        tableLog.append ("</tr>")                                    
                        confusionMatrixTable.insertDataVector (predictionVectorCount, 0, datavector)      
                        predictionVectorCount+=1                       
            tableLog.append ("</table>")
            if confusionMatrixTable is not None and confusionMatrixTable.getRowCount () > 0 :
                if tableResults is None :
                    tableResults = {}
                tableResults["Confusion Matrix"] = confusionMatrixTable
        resultLogTxtList.append ("".join (tableLog))           
        return (resultLogTxtList, tableResults) 
       
    def _shouldApplyToSlice (self, sliceIndex, roiDictionary, predictedROIList, options, sliceDictionary) :                       
        applyModelOn = options["ApplyOn"]
        if (applyModelOn == "SelectedSlicesWithData") :
            if sliceIndex not in sliceDictionary :
                return False
        elif (applyModelOn == "SelectedSlicesWithOutData") :
            if sliceIndex in sliceDictionary :
                return False
                                    
        if (options["ExistingData"] != "DoNotAlter"):
            return True
                
        return sliceIndex not in sliceDictionary or len (sliceDictionary[sliceIndex]) == 0
        
    
    
                            
    def getVisualization (self, sliceIndex, sliceView, options, App = None, ProgressDialog = None) :                     
        if self.shouldApplyModelOnImaging () :
            return None
        if (App is not None) :
            App.processEvents ()
        
        if not self._modelInputOutputDescription.isInputOutputDescriptionValid () :    
            MessageBoxUtil.showMessage (WindowTitle="ML Prediction",WindowMessage = "Unknown model size a error occured loading the model.") 
            return None
        
        inputSliceBlockSize = self._modelInputOutputDescription.getInputSliceBlockSize ()                
        isChannelLast = self._modelInputOutputDescription.isChannelLast ()
        
        singleSliceDataInputData = None
        inputSliceBlockSizeMiddle = int ((inputSliceBlockSize -1) / 2)
        ModelInputImageShape = self._modelInputOutputDescription.getInputImageShape ()
        
        if ("PatchCenterPos" in options):
            cX, cY = options["PatchCenterPos"]
            cX, cY = sliceView.transformSliceMapPointToScreen (cX, cY)
            self.setModelInputTransformationPredictionPatchCenter (cX, cY) 
        
        ReturnColorNiftiVolumeData = False
        if sliceView.getNIfTIVolume ().hasColorImageData () :
            inputDataSize = self._modelInputOutputDescription.getInputDataSize () 
            if len (inputDataSize) == 4 and inputDataSize[-1] == 3 :
                ReturnColorNiftiVolumeData = True
                inputSliceBlockSizeMiddle = 0
        self._modelInputNormalization.setColorNormMode (ReturnColorNiftiVolumeData)
        
        rawImageData = sliceView.getNIfTIVolumeSliceViewProjection (ReturnColorNiftiVolumeData =ReturnColorNiftiVolumeData)
                               
        transformedVolume = self._modelInputTransformation.apply2DTransform (sliceView, rawImageData, sliceIndex, ModelInputImageShape, True)      
        if (transformedVolume is None) :
            print ("Data Transformation Failed")
            return None
        transformedVolume = self._modelInputNormalization.apply2D  (transformedVolume, rawImageData)
        if (transformedVolume is None) :
            print ("Normalization Failed")                
            return None
        
        singleSliceDataInputData, returnResult = self._BuildSliceInputData (sliceView, singleSliceDataInputData, transformedVolume, sliceIndex, inputSliceBlockSizeMiddle, inputSliceBlockSize, ModelInputImageShape, rawImageData)
        if (returnResult is not None) :
            if (returnResult == "TransformFailed") :
                return None
            elif (returnResult == "NormFailed") :
                return None
                    
        if (App is not None) :
            App.processEvents ()
                       
        try :
            predictionOutputChannel = int (options["PredictionOutputChannel"])
        except:
            return None
        if (predictionOutputChannel < 0) :
            return None 
        
        try :
             slicePrediction = self._kerasModel.modelPredict  (singleSliceDataInputData) 
        except:
            return None
        if slicePrediction is None :
            return None
        slicePrediction = slicePrediction[0, ...]        
        
        isChannelLast = self._modelInputOutputDescription.isChannelLast ()
        threshold = None 
        sliceShape = sliceView.getSliceShape ()
        
        if (options["Visualization"] == "PredictionProbability") :
            if (self._modelInputOutputDescription.isModelOutput2D ()) : 
                predictionMask = slicePrediction
            else:
                if (isChannelLast) :
                    outputsize = self._modelInputOutputDescription.getOutputDataSize ()
                    inputsize = self._modelInputOutputDescription.getInputDataSize ()
                    if (len (outputsize) == 3 and outputsize[1] == inputsize[1] * inputsize[2]):
                        predictionMask = np.reshape (slicePrediction, (inputsize[1], inputsize[2],slicePrediction.shape[-1]))
                    else:
                        outputMiddle = int ((slicePrediction.shape[2] -1) / 2)
                        predictionMask  = slicePrediction [:,:, outputMiddle, :]   
                else:
                    outputMiddle = int ((slicePrediction.shape[3] -1) / 2)
                    predictionMask = slicePrediction [:,:,:, outputMiddle]   
                
            if isChannelLast :
                if (predictionMask.shape[-1] == 1) :
                    predictionMask = predictionMask[...,0]
                else:
                    predictionMask = predictionMask[...,predictionOutputChannel]
            else:
                if (predictionMask.shape[0] == 1) :
                    predictionMask = predictionMask[0,...]
                else:
                    predictionMask = predictionMask[predictionOutputChannel,...]
            predictionMask = self._modelInputTransformation.reverse2D (predictionMask, sliceShape, CVal = np.nan, ResizeOrder = 0) 
            #predictionMask = sliceView.transformImageScreenViewToSliceMap (predictionMask)                                                                          
            return predictionMask
            
        if self.getPredictionProbThreshold () :
            threshold = self._getPredictionROIThreshold (predictionOutputChannel)
            if threshold is not None :
                if isChannelLast :
                    predictionMask = (slicePrediction[...,predictionOutputChannel] >= threshold).astype(np.float)
                else:
                    predictionMask = (slicePrediction[predictionOutputChannel,...] >= threshold).astype(np.float)
        if threshold is None :
            if isChannelLast :
                if (slicePrediction.shape[-1] == 1) :
                    predictionMask = (slicePrediction[...,0] > 0.5).astype(np.float)
                else:
                    prediction = np.argmax (slicePrediction, axis = len (slicePrediction.shape) - 1)
                    predictionMask = (prediction == predictionOutputChannel).astype(np.float)
            else:
                if (slicePrediction.shape[0] == 1) :
                    predictionMask = (slicePrediction[0,...] > 0.5).astype(np.float)
                else:
                    prediction = np.argmax (slicePrediction, axis = 0)
                    predictionMask = (prediction == predictionOutputChannel).astype(np.float)
            
        
        if "IntegratedGradientsFunction" in options and options["IntegratedGradientsFunction"] is not None :
            IntegratedGradientsFunction = options["IntegratedGradientsFunction"]
            if "IntegratedGradientsSteps" in options :
                IntegratedGradientsSteps = options["IntegratedGradientsSteps"]
            else:
                IntegratedGradientsSteps = 1
           
            ig = self._computeIntegratedGradient ( sliceView, IntegratedGradientsFunction,IntegratedGradientsSteps, sliceIndex, rawImageData, singleSliceDataInputData, inputSliceBlockSizeMiddle, inputSliceBlockSize, ModelInputImageShape, predictionOutputChannel, isChannelLast, predictionMask,  App, ProgressDialog) 
            try:
                if "abs(IntegratedGradient)" == options["Visualization"] :
                    ig = np.abs (ig)
            except:
                pass
            return ig
       
                
        #model = self._kerasModel.getKerasModel (LinearOutput=False)
        #if (model is None) :
        #    return None          
        if (App is not None) :
            App.processEvents ()
            
        if (options["Visualization"] == "PosGradRangeSaliencyMap") :
            sMap = self._computeSaliencyMap (sliceView, isChannelLast, predictionOutputChannel, singleSliceDataInputData,  PredictionGradientMask = predictionMask, App = App)
            try :
                sMap[sMap < 0] = 0
            except:
                pass
            resultMap = sMap
        elif (options["Visualization"] == "SaliencyMap") :
            resultMap= self._computeSaliencyMap (sliceView, isChannelLast, predictionOutputChannel, singleSliceDataInputData,  PredictionGradientMask = predictionMask, App = App)
        elif (options["Visualization"] == "AbsSaliencyMap") :
            resultMap = self._computeSaliencyMap (sliceView, isChannelLast, predictionOutputChannel, singleSliceDataInputData, PredictionGradientMask = predictionMask, App = App)
            try :
                resultMap = np.abs (resultMap)
            except:
                pass
        elif (options["Visualization"] == "GradCam") :
            resultMap= self._computeGenericCamMap (sliceView,isChannelLast, predictionOutputChannel, singleSliceDataInputData, rawImageData, options["SelectedModelLayerIndex"], GradCamMap = True, SaliencyClassActivationMap = False, PredictionGradientMask = predictionMask, App = App)
        elif (options["Visualization"] == "SaliencyActivation") :
            resultMap= self._computeGenericCamMap (sliceView,isChannelLast, predictionOutputChannel, singleSliceDataInputData, rawImageData, options["SelectedModelLayerIndex"], GradCamMap = False, SaliencyClassActivationMap = True, PredictionGradientMask = predictionMask, App = App, SaliencyClassActivationMapSignMask =[1,-1])
        elif (options["Visualization"] == "PosSaliencyActivation") :
            resultMap= self._computeGenericCamMap (sliceView,isChannelLast, predictionOutputChannel, singleSliceDataInputData, rawImageData, options["SelectedModelLayerIndex"], GradCamMap = False, SaliencyClassActivationMap = True, PredictionGradientMask = predictionMask, App = App, SaliencyClassActivationMapSignMask =[1,])
        elif (options["Visualization"] == "RelativeSaliencyActivation") :
            resultMap= self._computeGenericCamMap (sliceView,isChannelLast, predictionOutputChannel, singleSliceDataInputData, rawImageData, options["SelectedModelLayerIndex"], GradCamMap = False, SaliencyClassActivationMap = True, PredictionGradientMask = predictionMask, App = App, SaliencyClassActivationMapSignMask =[1,-1], ExportRelativeActivationSAMMap = True)
        elif (options["Visualization"] == "NegSaliencyActivation") :
            resultMap= self._computeGenericCamMap (sliceView,isChannelLast, predictionOutputChannel, singleSliceDataInputData, rawImageData, options["SelectedModelLayerIndex"], GradCamMap = False, SaliencyClassActivationMap = True, PredictionGradientMask = predictionMask, App = App, SaliencyClassActivationMapSignMask =[-1,])
        elif (options["Visualization"] == "ClassActivation") :
           resultMap= self._computeCAM (sliceView, singleSliceDataInputData, predictionOutputChannel, isChannelLast, rawImageData, App = App)
        #return sliceView.transformImageScreenViewToSliceMap (resultMap)          
        return resultMap
                     
     
    def getPredictionROINameList (self):
        TagList = []
        for index, roiDictionary in enumerate(self._PredictionMap.getTree ().values ()):   
            if roiDictionary is not None :
                try :
                    modelROIDef = roiDictionary["ROIDef"]            
                    roi = ROIDef ()
                    roi.setDef (modelROIDef)
                    del modelROIDef
                    TagList.append ((index, roi.getName ()))            
                except:
                   pass
        return TagList
    
    def _getPredictionROIThreshold (self, index):
        try:
            tree = self._PredictionMap.getTree ()
            if tree is not None and index in tree :
                if "Threshold" in tree[index]  :
                    return float (tree[index]["Threshold"])
        except :
            pass
        return None

    @staticmethod
    def _getROIIndexNumber (tpl) :
        return tpl[0]
    
    def applyMLModel (self, sliceSelectionWidget, sliceView, niftiVolume, roiDictionary, options, App = None, LimitSegmentationToSelectedROI = False, ProgressDialog = None, ProjectDataset = None) :         
        if (App is not None) :
            App.processEvents()
        if not self._modelInputOutputDescription.isInputOutputDescriptionValid () :                 
            resultLogTxtList = []
            ROICreatedList = []
            MessageBoxUtil.showMessage (WindowTitle="ML Prediction",WindowMessage = "Unknown model size a error occured loading the model.") 
            resultLogTxtList.append ("Unknown model size a error occured loading the model.")            
            return resultLogTxtList, ROICreatedList
        self._rollbackMLModelDictionary = roiDictionary
        roiDictionary._SaveUndoPoint (CallDictionaryChangedListner = False) 
        self._selectionState = (sliceSelectionWidget, sliceSelectionWidget.getSelectionDescription ())               
        if ("PatchCenterPos" in options):
            cX, cY = options["PatchCenterPos"]
            cX, cY = sliceView.transformSliceMapPointToScreen (cX, cY)
            self.setModelInputTransformationPredictionPatchCenter (cX, cY)
        try:        
            resultLogTxtList = []
        
            resultLogTxtList.append ("<b>Running Segmentation Model</b>")
            resultLogTxtList.append ("<b>Name: </b>" + self.getModelName ())                
            resultLogTxtList.append ("")
            print ("Running Segmentation Model")
                    
            resultLogTxtList.append ("Applying to ROI:")
            ROICreatedList = []
            
            ReturnColorNiftiVolumeData = False
            if niftiVolume.hasColorImageData () :
                inputDataSize = self._modelInputOutputDescription.getInputDataSize () 
                if len (inputDataSize) == 4 and inputDataSize[-1] == 3 :
                    ReturnColorNiftiVolumeData = True
            self._modelInputNormalization.setColorNormMode (ReturnColorNiftiVolumeData)
            
            rawImageData =  sliceView.getNIfTIVolumeSliceViewProjection (ReturnColorNiftiVolumeData  = ReturnColorNiftiVolumeData)
            #rawImageData = niftiVolume.getImageData()
            if options["ApplyToAllSlices"] == True:
                firstSlice = 0
                LastSliceRangeIndex = rawImageData.shape[2] - 1
            else:
                sliceRange = sliceSelectionWidget.getSelection ()
                firstSlice = min (sliceRange)
                LastSliceRangeIndex = max (sliceRange)
                
            isChannelLast = self._modelInputOutputDescription.isChannelLast ()                                    
            inputdata = None
            
            createUndefinedROI = options["CreateUndefinedROI"] and not LimitSegmentationToSelectedROI
            if ProjectDataset is not None and ProjectDataset.isProjectReadOnly () :
                createUndefinedROI = False
                
            predictedROIList = []
            predictionTree = self._PredictionMap.getTree ()                        
            roiDefs = roiDictionary.getROIDefs ()
            ROIIdentifiedToSegment = False
            ROITableList = []
            for outputValue in predictionTree.keys () :                            
                AddedROIPrediction = False
                
                if predictionTree[outputValue] is not None :
                    modelROIDef = predictionTree[outputValue]["ROIDef"]            
                    roi = ROIDef ()
                    roi.setDef (modelROIDef)
                    del modelROIDef
                    
                    roiName = roiDefs.findBestMatchingROIName (roi)
                    if (roiName == "None" and createUndefinedROI) :
                        newRoiName = roi.getName ()
                        if (roiDefs.hasROI (newRoiName)) :
                            newRoiName += "_" + str(roi.getType ())
                            newRoiName = roiDefs.getUniqueName (newRoiName, MatchingROI = roi)
                            if (roiDefs.hasROI (newRoiName)) :
                                roiName = newRoiName
                        if (roiName == "None") :
                            roi  = ROIDef  (roiIDNumber = None, name = newRoiName, color = roi.getColor (), roitype = roi.getType (), mask = roi.getMask (), Hidden = False, RadlexID = roi.getRadlexID (), Locked=False, ROIDefUIDObj = roi.getDefinedUID())
                            roiDefs.appendROIDef (roi, FireChangedEvent = True)
                            ROICreatedList.append (roi)                                                            
                            predictedROIList.append (roi) 
                            AddedROIPrediction = True
                            ROIIdentifiedToSegment = True
                            ROITableList.append ("<tr><td><center>" + roiName + "</center></td><td><center>"+ str(roi.getRadlexID ()) +"</center></td><td><center>" + roi.getType ()+"</center></td></tr>")
                
                    if (roiName != "None") :
                        if not LimitSegmentationToSelectedROI or roiDefs.isROINameSelected (roiName) :
                            if (not roiDefs.isROIHidden (roiName)):
                                roi = roiDefs.getROIDefCopyByName  (roiName)    
                                predictedROIList.append (roi) 
                                AddedROIPrediction = True
                                ROIIdentifiedToSegment = True
                                ROITableList.append ("<tr><td><center>" + roiName + "</center></td><td><center>"+ str(roi.getRadlexID ()) +"</center></td><td><center>" + roi.getType ()+"</center></td></tr>")
                if (not AddedROIPrediction) :
                    predictedROIList.append (None)  
            
            if (len (ROITableList) > 0) :
                ROITableList = ["<table width='100%'><tr><td><center><b>ROI Name</b></center><td><center><b>Radlex ID</b></center><td><center><b>ROI Type</b></center></td></tr>"] + ROITableList + ["</table>"]                
                resultLogTxtList.append ("".join(ROITableList))
                    
            if not ROIIdentifiedToSegment :
                resultLogTxtList.append ("")
                if ProjectDataset is not None and ProjectDataset.isProjectReadOnly () :
                    resultLogTxtList.append ("Project is readonly. Could not create segmentation ROI for project.")    
                    print ("Could not create ROI for segmentation model. Project is readonly.")
                resultLogTxtList.append ("No ROI selected for segmentation. Make sure ML model has ROI defined and the desired ROI are selected if the limit segmentation to ROI selection option is enabled.")
                print ("Model has no segmentable ROI")
                if ProjectDataset is not None and ProjectDataset.isProjectReadOnly () :
                    MessageBoxUtil.showMessage (WindowTitle="ML Prediction",WindowMessage = "Prediction not run. Could not create ROI for segmentation model. Project is read-only.") 
                else:
                    MessageBoxUtil.showMessage (WindowTitle="ML Prediction",WindowMessage = "Prediction not run. No ROI selected for segmentation. Make sure ML model has ROI defined and the desired ROI are selected if ROI selection options are enabled.") 
                return resultLogTxtList, ROICreatedList
            
            if options["ExistingData"] == "RemoveAll" :
                printHeader = False                    
                for predictedROI in predictedROIList :    
                    if predictedROI is not None :
                        roiDictionary.removeROIEntry (predictedROI.getName (), first = False)                             
                        if not printHeader :
                            resultLogTxtList.append ("")
                            resultLogTxtList.append ("Removing all existing slices for ROI:")
                            printHeader = True
                        resultLogTxtList.append (predictedROI.getName ())
                if (printHeader) :
                    resultLogTxtList.append ("")
            
            inputSliceBlockSize = self._modelInputOutputDescription.getInputSliceBlockSize ()                
            isChannelLast = self._modelInputOutputDescription.isChannelLast ()               
            if ReturnColorNiftiVolumeData :
                inputSliceBlockSizeMiddle = 0
            else:
                inputSliceBlockSizeMiddle = int ((inputSliceBlockSize -1) / 2)
            ModelInputImageShape = self._modelInputOutputDescription.getInputImageShape ()
            
            if (sliceView.getSliceAxis () == "Z") :
                if sliceView.areAxisProjectionControlsEnabledForAxis () :
                    projectionDictionary = sliceView.getAxisProjectionControls().getROIProjectionSliceAxis (niftiVolume, roiDictionary, sliceView.getCoordinate ()) 
                    roiSliceDictionary = roiDictionary.convertAxisProjectiontoROIDictionary (projectionDictionary)
                else:
                    roiSliceDictionary = roiDictionary.getROISliceDictionary ()   
            elif (sliceView.getSliceAxis () == "X") :
                roiSliceDictionary, _ = roiDictionary.getROIXYSliceDictionary (niftiVolume, ProjectDataset) 
            elif (sliceView.getSliceAxis () == "Y") :
                _, roiSliceDictionary = roiDictionary.getROIXYSliceDictionary (niftiVolume, ProjectDataset)
                
            if (ProgressDialog is not None) :
                ProgressDialog.setRange (0,LastSliceRangeIndex + 1 - firstSlice)                         
            
            resultLogTxtList.append ("")
            resultLogTxtList.append ("<b>Segmenting Slices: </b>")
                            
            SliceLog = []
            try :
                sliceSelectionWidget.setSelectedSlice (firstSlice)
            except:
                pass
            newSliceVolumeMemory = {}
        
            predictionROIDefined = False
            for roi in predictedROIList :            
                if roi is not None :
                    predictionROIDefined = True
                    break
                                
            if (predictionROIDefined) :      
                SliceViewObjectsChanged = set ()                
                for sliceIndex in range (firstSlice, LastSliceRangeIndex + 1) :         
                    if (len (SliceLog) > 0) :
                        resultLogTxtList.append (" ".join (SliceLog))
                        SliceLog = []
                        
                    if (App is not None) :
                        App.processEvents ()     
                    if (ProgressDialog is not None) :
                        ProgressDialog.setValue (sliceIndex - firstSlice)
                        if ProgressDialog.wasCanceled () :
                            break
                    if not self._shouldApplyToSlice (sliceIndex, roiDictionary, predictedROIList, options, roiSliceDictionary) :                                                                                                   
                        SliceLog.append ("Slice " + str (sliceIndex) + " skipped")
                    else:
                        SliceLog.append ("Slice " + str (sliceIndex))
                       
                        PreExistingSegmentationCache = {}
                        #create a hidden slice mask
                        HiddenSliceMask = None
                        if sliceIndex in roiSliceDictionary :
                            objList = roiSliceDictionary[sliceIndex]                            
                            for obj in objList :                                    
                                objName = obj.getName (roiDefs)
                                if obj.isROIArea () : #and roiDefs.isROILocked (objName) or options["ExistingData"] == "AddToData" :
                                    sliceMap = obj.getSliceViewObjectMask (sliceView, SliceNumber = sliceIndex, ClipOverlayingROI = roiDictionary.getROIDefs ().hasSingleROIPerVoxel ())
                                    if roiDefs.isROILocked (obj.getName (roiDefs)) :
                                        if HiddenSliceMask is None :
                                            HiddenSliceMask = np.copy (sliceMap)
                                        else:
                                            HiddenSliceMask[sliceMap > 0] = 1                                       
                                    PreExistingSegmentationCache[objName] = sliceMap > 0                                          
                        
                        transformedVolume = self._modelInputTransformation.apply2DTransform (sliceView, rawImageData, sliceIndex, ModelInputImageShape, True)      
                        if (transformedVolume is None) :
                            SliceLog.append ("Slice Data Transformaion Failed.")
                            resultLogTxtList.append (" ".join (SliceLog))
                            print ("Data Transformation Failed")        
                            MessageBoxUtil.showMessage (WindowTitle="ML Prediction",WindowMessage = "Prediction failed in data transformation pre-processing.") 
                            return resultLogTxtList, ROICreatedList
                        
                        transformedVolume = self._modelInputNormalization.apply2D  (transformedVolume, rawImageData)
                        if (transformedVolume is None) :
                            SliceLog.append ("Slice Data Normalization Failed.")
                            print ("Normalization Failed")            
                            MessageBoxUtil.showMessage (WindowTitle="ML Prediction",WindowMessage = "Prediction failed in data normalization pre-processing. If custom code is being run check normalization code.") 
                            resultLogTxtList.append (" ".join (SliceLog))
                            return resultLogTxtList, ROICreatedList
                        
                        inputdata, returnResult = self._BuildSliceInputData (sliceView, inputdata, transformedVolume, sliceIndex, inputSliceBlockSizeMiddle, inputSliceBlockSize, ModelInputImageShape, rawImageData)
                        if (returnResult is not None) :
                            if (returnResult == "TransformFailed") :
                                SliceLog.append ("Slice Data Transformaion Failed.")
                                resultLogTxtList.append (" ".join (SliceLog))
                                return resultLogTxtList, ROICreatedList
                            elif (returnResult == "NormFailed") :
                                SliceLog.append ("Slice Data Normalization Failed.")
                                resultLogTxtList.append (" ".join (SliceLog))
                                return resultLogTxtList, ROICreatedList
                
                        try :
                            slicePrediction, errorMsg = self._kerasModel.modelPredict  (inputdata, ReturnErrorMsg = True) 
                        except Exception as msg:
                            slicePrediction = None
                            errorMsg = str(msg)
                        if (slicePrediction is None) :
                            if errorMsg is not None :
                                WindowMessage = "Prediction failed. " + errorMsg
                            else:
                                WindowMessage = "Prediction failed."
                            SliceLog.append (WindowMessage)
                            print (WindowMessage)     
                            MessageBoxUtil.showMessage (WindowTitle="ML Prediction",WindowMessage = WindowMessage) 
                            resultLogTxtList.append (" ".join (SliceLog))
                            return resultLogTxtList, ROICreatedList
                        slicePrediction = slicePrediction[0,...]
                        
                        if (not self._modelInputOutputDescription.isModelOutput2D ()) : 
                            if (isChannelLast) :
                                outputsize = self._modelInputOutputDescription.getOutputDataSize ()
                                inputsize = self._modelInputOutputDescription.getInputDataSize ()
                                if (len (outputsize) == 3 and outputsize[1] == inputsize[1] * inputsize[2]):
                                    slicePrediction = np.reshape (slicePrediction, (inputsize[1], inputsize[2],slicePrediction.shape[-1]))
                                else:
                                    outputMiddle = int ((slicePrediction.shape[2] -1) / 2)
                                    slicePrediction  = slicePrediction [:,:, outputMiddle, :]   
                            else:
                                outputMiddle = int ((slicePrediction.shape[3] -1) / 2)
                                slicePrediction = slicePrediction [:,:,:, outputMiddle]   
                        SetSliceIndex = False
                        if self.shouldApplyModelOnImaging () :  #applying model to change imaging voxel values
                              if (isChannelLast) :
                                  predictedMask = slicePrediction[:,:, 0]
                              else:
                                  predictedMask = slicePrediction[0, :,:]    
                              sliceShape = sliceView.getSliceShape ()
                              predictedMask = self._modelInputTransformation.reverse2D (predictedMask, sliceShape, ResizeOrder = 0, CVal = np.nan)                                                                    
                              if np.any (np.logical_not(np.isnan (predictedMask))):
                                  newSliceVolumeMemory[sliceIndex] = sliceView.transformImageScreenViewToSliceMap (predictedMask)  #save slice volume memory update volume after slice prediction                          
                        else:
                            if self.getSinglePrediction () :
                                if (isChannelLast) :
                                    maxResult = np.argmax (slicePrediction, axis = -1)                
                                else:
                                    maxResult = np.argmax (slicePrediction, axis = 0)                
                            
                            #sort predictions in order to correcty remove and edit overlapping ROI.
                            SortedROILst = []
                            for outputValue in predictionTree.keys () :   
                                if (App is not None) :
                                    App.processEvents()
                                PredicitionROI = predictedROIList[outputValue]
                                if (PredicitionROI is not None) :                                
                                    roiName = PredicitionROI.getName ()                                                        
                                    indexNumber = roiDefs.getROIIndex (roiName)
                                    if indexNumber != -1 :
                                        SortedROILst.append ((indexNumber, PredicitionROI, outputValue))
                            SortedROILst = sorted (SortedROILst, key = ML_ApplyROI2DPrediction._getROIIndexNumber)
                            ROIOrderSortedPredictionKeys = []
                            for roiTpl in SortedROILst :
                                ROIOrderSortedPredictionKeys.append(roiTpl[2])
                            del SortedROILst
                            
                            if ProjectDataset is None :
                                 FileSaveThreadManger  = None
                            else:
                                 FileSaveThreadManger = ProjectDataset.getROISaveThread ()
                            
                            combinedPredictionMask = None                            
                            PredictedSliceMasks = {}
                            for outputValue in ROIOrderSortedPredictionKeys :                                   
                                if (App is not None) :
                                    App.processEvents()
                                PredicitionROI = predictedROIList[outputValue]
                                if (PredicitionROI is not None) :                                    
                                    predictedValue = predictionTree[outputValue] 
                                    if (isChannelLast) :
                                        predictedMask = slicePrediction[:,:, outputValue]
                                    else:
                                        predictedMask = slicePrediction[outputValue, :,:]    
                                                                        
                                    if self.getSinglePrediction () :
                                        predictedMask = (outputValue == maxResult).astype (np.float) * predictedMask                                
                                    if (self.getPredictionProbThreshold ()) : 
                                        predictedMask = predictedMask >= predictedValue["Threshold"]
                                    else:                                
                                        predictedMask = predictedMask > 0
                                    predictedMask = predictedMask.astype (np.uint8)                                        
                                    roiName = PredicitionROI.getName ()
                                    if roiName not in PredictedSliceMasks :
                                        PredictedSliceMasks[roiName] = predictedMask
                                    else:
                                        PredictedSliceMasks[roiName][predictedMask > 0] = 1.0
                            callDictionaryChangeListener = False
                            for roiName, predictedMask in PredictedSliceMasks.items () :                                
                                    SliceChanged = False
                                    IsSliceHumanEdited = False                    
                                    hasPrediction = np.any (predictedMask)
                                    if roiDictionary.isROIDefined (roiName):                                               
                                       obj = roiDictionary.getROIObject (roiName)                
                                       if sliceView.getSliceAxis () == "Z" :
                                           IsSliceHumanEdited = obj.doesSliceContainHumanEditedContour (sliceIndex)                                            
                                       if options["ExistingData"] == "Remove" and not hasPrediction and roiName in PreExistingSegmentationCache and np.any (PreExistingSegmentationCache[roiName]) :                                                                                                                                                
                                            print ("Removing ROISlice Roi slice " + roiName + " index %d" % sliceIndex)
                                            obj.removeSlice (sliceIndex, RemoveAllSliceContours = True, Axis=sliceView.getSliceAxis (), SharedROIMask = roiDictionary.getSharedVolumeCacheMem (),  SliceViewObjectsChanged = SliceViewObjectsChanged, RemoveProjectedSlices = True, SliceView = sliceView)
                                            #print ("End Removing ROISlice Roi slice " + roiName + " index %d" % sliceIndex)
                                            SliceChanged = True
                                    else:
                                        obj = None
                                    if hasPrediction :                                 
                                        sliceShape = sliceView.getSliceShape ()                                                                                
                                        predictedMask = self._modelInputTransformation.reverse2D (predictedMask, sliceShape, ResizeOrder = 0)                                                                    
                                        predictedMask = sliceView.transformImageScreenViewToSliceMap (predictedMask)
                                        PredictionMaskChanged = False
                                               
                                        if options["ExistingData"] == "AddToData" :                                            
                                             if obj is not None :                                               
                                                obj = roiDictionary.getROIObject (roiName)     
                                                if (obj.isROIArea ()) :
                                                    for name, prexistingSegmentation in PreExistingSegmentationCache.items () :                                                        
                                                        if name == roiName :
                                                            predictedMask[prexistingSegmentation] = 1                                                                                                                                 
                                                        else:
                                                            predictedMask[prexistingSegmentation] = 0                                                                                                        
                                                    PredictionMaskChanged = True
                                        else:
                                            IsSliceHumanEdited = False
                                                                                                                                                      
                                        if (HiddenSliceMask is not None) :
                                            predictedMask[HiddenSliceMask > 0] = 0                                            
                                            PredictionMaskChanged = True
                                        
                                        if not PredictionMaskChanged or np.any (predictedMask)  :
                                            SliceLog.append ("Seg: " + roiName)
                                            
                                            if (not roiDictionary.isROIDefined (roiName) and obj is not None) : # if ROI was removed.
                                                roiDictionary.addROI (roiName.getName (), obj, SaveUndoPt = False)                        
                                                
                                            if roiDictionary.isROIinPerimeterMode () :
                                                roiDictionary.createROISliceFromBinarySliceMask (sliceIndex, roiName, predictedMask , IsSliceHumanEdited =IsSliceHumanEdited, FileSaveThreadManger = FileSaveThreadManger, ProjectDataset = ProjectDataset, VolumeShape = niftiVolume.getSliceDimensions())   
                                            else:                                                
                                                if roiName in PreExistingSegmentationCache :
                                                    origionalMap = PreExistingSegmentationCache[roiName]
                                                    ChangeMap = origionalMap.astype (np.int8) - predictedMask.astype (np.int8)
                                                else:
                                                    ChangeMap = np.copy (predictedMask)                                                   
                                                if np.any (ChangeMap != 0 ) :
                                                    print ("Creating Roi slice " + roiName + " index %d" % sliceIndex)
                                                    roiDictionary.createROISliceFromBinarySliceMaskForSliceView (sliceView, sliceIndex, roiName, predictedMask, ChangeMap , IsSliceHumanEdited = IsSliceHumanEdited, SkipFileSave = True, SliceViewObjectsChanged = SliceViewObjectsChanged, CallDictionaryChangeListener = False) 
                                                    callDictionaryChangeListener = True
                                                    #print ("End Creating Roi slice " + roiName  + " index %d" % sliceIndex)
                                            SliceChanged = True
                                            if combinedPredictionMask is None :
                                                combinedPredictionMask = predictedMask > 0 
                                            else:
                                                combinedPredictionMask[predictedMask > 0] = True                                                                            
                                    if (not SetSliceIndex and SliceChanged) :
                                        try :
                                            sliceSelectionWidget.setSelectedSlice (sliceIndex)
                                            SetSliceIndex = True
                                        except:
                                            pass
                            if callDictionaryChangeListener :
                               roiDictionary._CallDictionaryChangedListner  (SkipFileSave=True)
                            if (App is not None) :
                                App.processEvents()
                                                           
                            if np.any (combinedPredictionMask) is not None and roiDefs.hasSingleROIPerVoxel () :
                                #remove overlapping voxels if in single ROI mode                                                        
                                if sliceIndex in roiSliceDictionary :
                                    objList = roiSliceDictionary[sliceIndex]                                    
                                    for obj in objList :                                 
                                        existingObjName = obj.getName (roiDefs)
                                        if existingObjName not in PredictedSliceMasks :                                                                                    
                                            if obj.isROIArea () and not roiDefs.isROILocked (existingObjName) and obj.hasSliceInSliceView (sliceIndex, sliceView, roiDictionary) :
                                                existingObjSliceMap = obj.getSliceViewObjectMask  (sliceView, SliceNumber = sliceIndex, ClipOverlayingROI = False)                                                                                                
                                                origionalMap = np.copy (existingObjSliceMap)
                                                existingObjSliceMap[combinedPredictionMask] = 0                                                                                                
                                                if np.any (existingObjSliceMap) :
                                                    if roiDictionary.isROIinPerimeterMode () :
                                                        IsSliceHumanEdited = obj.doesSliceContainHumanEditedContour (sliceIndex)
                                                        roiDictionary.createROISliceFromBinarySliceMask (sliceIndex, existingObjName, existingObjSliceMap , IsSliceHumanEdited = IsSliceHumanEdited, FileSaveThreadManger = FileSaveThreadManger, ProjectDataset = ProjectDataset, VolumeShape = niftiVolume.getSliceDimensions())
                                                    else:                                                        
                                                        ChangeMap = origionalMap.astype (np.int8) - existingObjSliceMap.astype (np.int8)                                                        
                                                        if sliceView.getSliceAxis () == "Z" :
                                                            IsSliceHumanEdited = obj.doesSliceContainHumanEditedContour (sliceIndex)                                            
                                                        else:
                                                            IsSliceHumanEdited = False
                                                        roiDictionary.createROISliceFromBinarySliceMaskForSliceView (sliceView, sliceIndex, existingObjName, existingObjSliceMap, ChangeMap , IsSliceHumanEdited = IsSliceHumanEdited, SkipFileSave = True, SliceViewObjectsChanged = SliceViewObjectsChanged)  
                                                else:
                                                    obj.removeSlice (sliceIndex, RemoveAllSliceContours = True, Axis=sliceView.getSliceAxis (), SharedROIMask = roiDictionary.getSharedVolumeCacheMem (),  SliceViewObjectsChanged = SliceViewObjectsChanged, RemoveProjectedSlices = True, SliceView = sliceView)
                                                    
                        if (not SetSliceIndex) :
                            try :
                                sliceSelectionWidget.setSelectedSlice (sliceIndex)
                                SetSliceIndex = True
                            except:
                                pass
                        if (App is not None) :
                            App.processEvents()
                
                #close 
                for obj in SliceViewObjectsChanged :
                    obj.endCrossSliceChange ()
                if not ProgressDialog.wasCanceled () :
                    if options["FilterROIKeepOnlyLargest3dIsland"] :
                        try :   
                            from rilcontourlib.ui.rcc_contourwindow import RCC_ContourWindow                    
                            try:                    
                                contourWindow = sliceView
                                while contourWindow is not None :
                                    if isinstance (contourWindow, RCC_ContourWindow) :
                                        break
                                    contourWindow = contourWindow.parent ()
                            except:
                                contourWindow = None
                                                    
                            if contourWindow is not None :
                                ObjList = [objname for objname in PredictedSliceMasks.keys ()]
                                if len (ObjList) > 0 :
                                    contourWindow.KeepOnlyLargestObjectFilter (ObjList = ObjList, CopyObjectsForUndoRedo  = False, IslandROIName = "None", ProgressDialog = ProgressDialog)
                        except:
                            pass 
        
            # update slice volume memory now after slice prediction
            if len (newSliceVolumeMemory) > 0 :
                if sliceView.areAxisProjectionControlsEnabledForAxis () :
                    axisProjection = sliceView.getAxisProjectionControls()
                    dtype = niftiVolume.get_data_dtype ()
                    coord = sliceView.getCoordinate ()
                    niftiVolume = sliceView.getNIfTIVolume ()
                    for sliceIndex in newSliceVolumeMemory.keys () :      
                        predictionMaskData =  newSliceVolumeMemory[sliceIndex]
                        rawImageData, inputClipMask = axisProjection.getNIfTIProjection (coord, niftiVolume, UseCache = True, SliceIndex = sliceIndex)
                        ImagingChanged = rawImageData - predictionMaskData
                        ChangeMask = np.logical_not(np.isnan (predictionMaskData))
                        np.logical_and (ImagingChanged != 0, ChangeMask, out = ChangeMask)
                        np.logical_and (inputClipMask, ChangeMask, out = ChangeMask)
                        axisProjection.setNIfTISliceChangeMasks (sliceView, ChangeMask, predictionMaskData.astype (dtype), sliceIndex) 
                        App.processEvents()
                else:
                    dtype = niftiVolume.get_data_dtype ()
                    for sliceIndex in newSliceVolumeMemory.keys () :                    
                        predictionMaskData = newSliceVolumeMemory[sliceIndex]
                        niftiVolume.setSliceData (sliceIndex, predictionMaskData.astype (dtype), SetMask = np.logical_not(np.isnan (predictionMaskData)), Axis=sliceView.getSliceAxis ())
                        App.processEvents()
                    
            if (len (SliceLog) > 0) :
                resultLogTxtList.append (" ".join (SliceLog))           
            roiDictionary._CallDictionaryChangedListner ()
            self._rollbackMLModelDictionary = None                                                                                                 
            return (resultLogTxtList, ROICreatedList) 
        finally :
            try : # restore selection
                self._selectionState[0].resetSelectionFromDescription (self._selectionState[1])
            except:
                pass
            if (self._rollbackMLModelDictionary is not None) :
                self._rollbackMLModelDictionary.undo ()
                self._rollbackMLModelDictionary = None
                    
     
        
class ML_ApplyTagPrediction (ML_ModelPredictBase) :
    def __init__ (self, ProjectDataset) :        
        ML_ModelPredictBase.__init__(self, ProjectDataset)
        self._PredictionMap = ML_PredictionMap () 
        self._MultiSliceVoting = "Average"
        
    def getType (self) :
        return "ML_ApplyTagPrediction"
    
    def isClassificationModel (self) :
        return True

    
    def getModelType (self) :
        return "Classification"
    
    def getMultiSliceVoting (self) :
        return self._MultiSliceVoting
    
    def setMultiSliceVoting (self, voting):
        self._MultiSliceVoting = voting
    
    def printClassificationSummary (self, sliceIndex, slicePrediction, resultLogTxtList) :
        sliceProbList = []
        for index in range (slicePrediction.shape[0]) :
            sliceProbList.append ("%0.3f" % slicePrediction[index])
        sliceProbList = ", ".join (sliceProbList)
        maxIndex = np.argmax (slicePrediction, axis = 0)
        if (sliceIndex != -1) :
            line = "Slice[" + str (sliceIndex) + "]: " + sliceProbList + " Pred="+ str (maxIndex)
        else:
            line = "Summary: " + sliceProbList + " Pred="+ str (maxIndex) 
        resultLogTxtList.append (line)
        print (line)
        
       
   
    
                                                         
    def getVisualization (self, sliceIndex, sliceView, options, App = None, ProgressDialog = None) :                     
        if (not self._modelInputOutputDescription.isModelClassificationOutput ()) :            
            return None
        else:   
            if (App is not None) :
                App.processEvents ()
            if not self._modelInputOutputDescription.isInputOutputDescriptionValid () :             
                MessageBoxUtil.showMessage (WindowTitle="ML Prediction",WindowMessage = "Unknown model size a error occured loading the model.")                 
                return None
            
            isChannelLast = self._modelInputOutputDescription.isChannelLast ()            
            inputSliceBlockSize = self._modelInputOutputDescription.getInputSliceBlockSize ()                
           
            
            singleSliceDataInputData = None
            inputSliceBlockSizeMiddle = int ((inputSliceBlockSize -1) / 2)
            ModelInputImageShape = self._modelInputOutputDescription.getInputImageShape ()
            
            if ("PatchCenterPos" in options):
                cX, cY = options["PatchCenterPos"]
                cX, cY = sliceView.transformSliceMapPointToScreen (cX, cY)
                self.setModelInputTransformationPredictionPatchCenter (cX, cY)
            
            ReturnColorNiftiVolumeData = False
            if sliceView.getNIfTIVolume ().hasColorImageData () :
                inputDataSize = self._modelInputOutputDescription.getInputDataSize () 
                if len (inputDataSize) == 4 and inputDataSize[-1] == 3 :
                    ReturnColorNiftiVolumeData = True            
                    inputSliceBlockSizeMiddle = 0
            self._modelInputNormalization.setColorNormMode (ReturnColorNiftiVolumeData)
            
            rawImageData = sliceView.getNIfTIVolumeSliceViewProjection (ReturnColorNiftiVolumeData =ReturnColorNiftiVolumeData)
            
            transformedVolume = self._modelInputTransformation.apply2DTransform (sliceView, rawImageData, sliceIndex, ModelInputImageShape, True)      
            if (transformedVolume is None) :
                print ("Data Transformation Failed")
                return None
            transformedVolume = self._modelInputNormalization.apply2D  (transformedVolume, rawImageData)
            if (transformedVolume is None) :
                print ("Normalization Failed")                
                return None        
            
            singleSliceDataInputData, returnResult = self._BuildSliceInputData (sliceView, singleSliceDataInputData, transformedVolume, sliceIndex, inputSliceBlockSizeMiddle, inputSliceBlockSize, ModelInputImageShape, rawImageData)
            if (returnResult is not None) :
                return None
            
            try :
                predictionOutputChannel = int (options["PredictionOutputChannel"])
            except:
                return None
            if (predictionOutputChannel < 0) :
                return None 
            predictionMask = None
             
            if "IntegratedGradientsFunction" in options and options["IntegratedGradientsFunction"] is not None :
                IntegratedGradientsFunction = options["IntegratedGradientsFunction"]
                if "IntegratedGradientsSteps" in options :
                    IntegratedGradientsSteps = options["IntegratedGradientsSteps"] 
                else:
                    IntegratedGradientsSteps = 1
                ig = self._computeIntegratedGradient (sliceView, IntegratedGradientsFunction,IntegratedGradientsSteps, sliceIndex, rawImageData, singleSliceDataInputData, inputSliceBlockSizeMiddle, inputSliceBlockSize, ModelInputImageShape, predictionOutputChannel, isChannelLast, predictionMask,  App, ProgressDialog) 
                try:
                    if "abs(IntegratedGradient)" == options["Visualization"] :
                        ig = np.abs (ig)
                except:
                    pass
                return ig
            
            #model = self._kerasModel.getKerasModel (LinearOutput=options["Visualization"] == "ClassActivation")
            #if (model is None) :
            #    return None         
            if (App is not None) :
                App.processEvents ()
              
            try :
                if (options["Visualization"] == "PosGradRangeSaliencyMap") :
                    sMap = self._computeSaliencyMap (sliceView, isChannelLast, predictionOutputChannel, singleSliceDataInputData,  App = App)
                    try :
                        sMap[sMap < 0] = 0
                    except:
                        pass
                    result= sMap
                elif (options["Visualization"] == "SaliencyMap") :
                    result= self._computeSaliencyMap (sliceView, isChannelLast, predictionOutputChannel, singleSliceDataInputData,  PredictionGradientMask = predictionMask, App = App)
                elif (options["Visualization"] == "AbsSaliencyMap") :
                    result= self._computeSaliencyMap (sliceView, isChannelLast, predictionOutputChannel, singleSliceDataInputData,  PredictionGradientMask = predictionMask, App = App)
                    try:
                        result = np.abs(result)
                    except:
                        pass
                elif (options["Visualization"] == "GradCam") :
                    result= self._computeGenericCamMap (sliceView,isChannelLast, predictionOutputChannel, singleSliceDataInputData, rawImageData, options["SelectedModelLayerIndex"], GradCamMap = True, SaliencyClassActivationMap = False, App = App)
                elif (options["Visualization"] == "SaliencyActivation") :
                    result= self._computeGenericCamMap (sliceView,isChannelLast, predictionOutputChannel, singleSliceDataInputData, rawImageData, options["SelectedModelLayerIndex"], GradCamMap = False, SaliencyClassActivationMap = True, PredictionGradientMask = predictionMask, App = App, SaliencyClassActivationMapSignMask =[1,-1])
                elif (options["Visualization"] == "PosSaliencyActivation") :
                    result= self._computeGenericCamMap (sliceView,isChannelLast, predictionOutputChannel, singleSliceDataInputData, rawImageData, options["SelectedModelLayerIndex"], GradCamMap = False, SaliencyClassActivationMap = True, PredictionGradientMask = predictionMask, App = App, SaliencyClassActivationMapSignMask =[1,])
                elif (options["Visualization"] == "RelativeSaliencyActivation") :
                    result= self._computeGenericCamMap (sliceView,isChannelLast, predictionOutputChannel, singleSliceDataInputData, rawImageData, options["SelectedModelLayerIndex"], GradCamMap = False, SaliencyClassActivationMap = True, PredictionGradientMask = predictionMask, App = App, SaliencyClassActivationMapSignMask =[1,-1], ExportRelativeActivationSAMMap = True)
                elif (options["Visualization"] == "NegSaliencyActivation") :
                    result= self._computeGenericCamMap (sliceView,isChannelLast, predictionOutputChannel, singleSliceDataInputData, rawImageData, options["SelectedModelLayerIndex"], GradCamMap = False, SaliencyClassActivationMap = True, PredictionGradientMask = predictionMask, App = App, SaliencyClassActivationMapSignMask =[-1,])
                elif (options["Visualization"] == "ClassActivation") :
                   result= self._computeCAM (sliceView, singleSliceDataInputData, predictionOutputChannel, isChannelLast,rawImageData, App = App)
                #return sliceView.transformImageScreenViewToSliceMap (result)
                return result
            except:
                return None
    
    def getSlicePrediction (self,sliceIndex, sliceView, niftiVolume, LinearOutput = False):
        if (not self._modelInputOutputDescription.isModelClassificationOutput ()) :            
            return None
        else:            
            ReturnColorNiftiVolumeData = False
            if niftiVolume.hasColorImageData () :
                inputDataSize = self._modelInputOutputDescription.getInputDataSize () 
                if len (inputDataSize) == 4 and inputDataSize[-1] == 3 :
                    ReturnColorNiftiVolumeData = True
            self._modelInputNormalization.setColorNormMode (ReturnColorNiftiVolumeData)
            
            rawImageData =  sliceView.getNIfTIVolumeSliceViewProjection (ReturnColorNiftiVolumeData =ReturnColorNiftiVolumeData)            
                            
            inputSliceBlockSize = self._modelInputOutputDescription.getInputSliceBlockSize ()                
            singleSliceDataInputData = None
            if ReturnColorNiftiVolumeData :
                inputSliceBlockSizeMiddle = 0
            else:
                inputSliceBlockSizeMiddle = int ((inputSliceBlockSize -1) / 2)
            ModelInputImageShape = self._modelInputOutputDescription.getInputImageShape ()
            
                                            
            transformedVolume = self._modelInputTransformation.apply2DTransform (sliceView, rawImageData, sliceIndex, ModelInputImageShape, True)      
            if (transformedVolume is None) :
                print ("Data Transformation Failed")
                return None
            transformedVolume = self._modelInputNormalization.apply2D  (transformedVolume, rawImageData)
            if (transformedVolume is None) :
                print ("Normalization Failed")                
                return None
            
            singleSliceDataInputData, returnResult = self._BuildSliceInputData (sliceView, singleSliceDataInputData, transformedVolume, sliceIndex, inputSliceBlockSizeMiddle, inputSliceBlockSize, ModelInputImageShape, rawImageData)
            if (returnResult is not None) :
                 return None
              
            try :
                slicePrediction = self._kerasModel.modelPredict (singleSliceDataInputData, LinearOutput = LinearOutput)
                return slicePrediction[0,...]
            except:
                return None
    
    
    def applyMLModel (self, sliceSelectionWidget, sliceView,niftiVolume, roiDictionary, options, App = None, LimitSegmentationToSelectedROI = False, ProgressDialog = None, ProjectDataset = None) : 
        if (App is not None) :
            App.processEvents()
        if not self._modelInputOutputDescription.isInputOutputDescriptionValid () : 
            resultLogTxtList = []
            MessageBoxUtil.showMessage (WindowTitle="ML Prediction",WindowMessage = "Unknown model size a error occured loading the model.") 
            resultLogTxtList.append ("Unknown model size a error occured loading the model.")            
            return resultLogTxtList, None
        sliceResults = None
        setDescriptionPrediction = options["SetDescriptionPrediction"]
        Mode = options["VotingMode"]             
        counter = 1        
        resultLogTxtList = []
        if ("PatchCenterPos" in options):
            cX, cY = options["PatchCenterPos"]
            cX, cY = sliceView.transformSliceMapPointToScreen (cX, cY)
            self.setModelInputTransformationPredictionPatchCenter (cX, cY)
        if (not self._modelInputOutputDescription.isModelClassificationOutput ()) :            
            MessageBoxUtil.showMessage (WindowTitle="ML Prediction",WindowMessage = "Classification models with multi-dimensional output are not supported.") 
        else:
            if self._shouldApplyToModel (roiDictionary, options) :
                ReturnColorNiftiVolumeData = False
                if niftiVolume.hasColorImageData () :
                    inputDataSize = self._modelInputOutputDescription.getInputDataSize () 
                    if len (inputDataSize) == 4 and inputDataSize[-1] == 3 :
                        ReturnColorNiftiVolumeData = True
                self._modelInputNormalization.setColorNormMode (ReturnColorNiftiVolumeData)
                
                rawImageData =  sliceView.getNIfTIVolumeSliceViewProjection (ReturnColorNiftiVolumeData =ReturnColorNiftiVolumeData)
                if options["ApplyToAllSlices"] == True:
                    firstSlice = 0
                    LastSliceRangeIndex = rawImageData.shape[2] - 1
                else:
                    sliceRange =  sliceSelectionWidget.getSelection ()
                    firstSlice = min (sliceRange)
                    LastSliceRangeIndex = max (sliceRange)
                    
                        
                inputSliceBlockSize = self._modelInputOutputDescription.getInputSliceBlockSize ()                
                
                singleSliceDataInputData = None
                if ReturnColorNiftiVolumeData :
                    inputSliceBlockSizeMiddle = 0
                else:
                    inputSliceBlockSizeMiddle = int ((inputSliceBlockSize -1) / 2)
                ModelInputImageShape = self._modelInputOutputDescription.getInputImageShape ()
                resultLogTxtList.append ("")
                resultLogTxtList.append ("<b>Running Classification Model</b>")
                resultLogTxtList.append ("<b>Name: </b>" + self.getModelName ())                
                print ("Running Classification Model")
                if (ProgressDialog is not None) :
                    ProgressDialog.setRange (0,LastSliceRangeIndex + 1 - firstSlice)                         
                                
                for sliceIndex in range (firstSlice, LastSliceRangeIndex + 1) :   
                        if (App is not None) :
                            App.processEvents ()         
                        if (ProgressDialog is not None) :
                            ProgressDialog.setValue (sliceIndex - firstSlice)
                            if ProgressDialog.wasCanceled () :
                                break
                        transformedVolume = self._modelInputTransformation.apply2DTransform (sliceView, rawImageData, sliceIndex, ModelInputImageShape, True)      
                        if (transformedVolume is None) :
                            print ("Data Transformation Failed")
                            resultLogTxtList.append ("Data Transformation Failed")           
                            MessageBoxUtil.showMessage (WindowTitle="ML Prediction",WindowMessage = "Prediction failed in data transformation pre-processing.") 
                            return resultLogTxtList, None
                        transformedVolume = self._modelInputNormalization.apply2D  (transformedVolume, rawImageData)
                        if (transformedVolume is None) :
                            print ("Normalization Failed")
                            MessageBoxUtil.showMessage (WindowTitle="ML Prediction",WindowMessage = "Prediction failed in data normalization pre-processing. If custom code is being run check normalization code.") 
                            resultLogTxtList.append ("Normalization Failed")
                            return resultLogTxtList, None
                        
                        singleSliceDataInputData, returnResult = self._BuildSliceInputData (sliceView, singleSliceDataInputData, transformedVolume, sliceIndex, inputSliceBlockSizeMiddle, inputSliceBlockSize, ModelInputImageShape, rawImageData)
                        if (returnResult is not None) :
                            if (returnResult == "TransformFailed") :
                                resultLogTxtList.append ("Data Transformation Failed")           
                                return  (resultLogTxtList, None)
                            elif (returnResult == "NormFailed") :
                                resultLogTxtList.append ("Data Normalization Failed")           
                                return  (resultLogTxtList, None)
            
                        try :
                            slicePrediction, errorMsg = self._kerasModel.modelPredict  (singleSliceDataInputData, ReturnErrorMsg = True) 
                        except Exception as msg:
                            slicePrediction = None
                            errorMsg = str (msg)
                        if (slicePrediction is None) :
                            if errorMsg is not None :
                                WindowMessage = "Prediction failed. " + errorMsg
                            else:
                                WindowMessage = "Prediction failed."
                            print (WindowMessage)   
                            resultLogTxtList.append (WindowMessage)
                            MessageBoxUtil.showMessage (WindowTitle="ML Prediction",WindowMessage = WindowMessage) 
                            return resultLogTxtList, None
                            
                        slicePrediction = slicePrediction[0,...]
                        
                        self.printClassificationSummary (sliceIndex, slicePrediction, resultLogTxtList)
                        
                        if (Mode == "Majority voting (Binary)") :
                            maxResult = np.max (slicePrediction)
                            slicePrediction = (slicePrediction == maxResult).astype (np.int)
                        elif (Mode == "Majority voting (Probability)") :
                            maxResult = np.max (slicePrediction)
                            slicePrediction = (slicePrediction == maxResult).astype (np.float) * slicePrediction                            

                        if (sliceResults is None) :
                            sliceResults = np.copy (slicePrediction)
                        elif Mode in ["Average", "Majority voting (Binary)", "Majority voting (Probability)"]:
                            sliceResults += slicePrediction
                            if Mode == "Average" :
                                counter += 1                        
                        elif Mode == "Max" :
                            for index in range (sliceResults.shape[0]) :
                                sliceResults[index] = max (slicePrediction[index], sliceResults[index])
                if counter > 1 :
                    sliceResults = sliceResults / counter                
            resultLogTxtList.append ("")   
            if sliceResults is None :
                print ("Error results not computed")
                resultLogTxtList.append ("Error results not computed")
                MessageBoxUtil.showMessage (WindowTitle="ML Prediction",WindowMessage = "Prediction failed during model prediction.")                           
                return resultLogTxtList, None
            
            #determine if classifier should run in single predition mode or multiple prediction mide
            if self.getSinglePrediction () :
                singlePredictionMode = True  # if set to true then run in mode
            else:
                singlePredictionMode = True  # else if all tags are have the same prediction name then run in single prediction mode
                if sliceResults.shape[0] > 1 :
                    predictionTree = self._PredictionMap.getTree ()                    
                    tagName = None
                    for predictionIndex in range (sliceResults.shape[0]) :
                        predictedValue = predictionTree[predictionIndex] 
                        if (predictedValue is not None) :
                            predictedTag = Tag.createFromDef(predictedValue["TagDefTuple"])   
                            if tagName is None : 
                                tagName = predictedTag.getName ()
                            elif (predictedTag.getName () != tagName) :
                                singlePredictionMode = False
                                break
                  
            if (singlePredictionMode) :  
                resultLogTxtList.append ("<b>Single-Classification Prediction (Voting Mode: "+ Mode +")</br>")
                self.printClassificationSummary (-1, sliceResults, resultLogTxtList)
                
                index = np.argmax (sliceResults, axis = 0)                
                predictionTree = self._PredictionMap.getTree ()
                predictedValue = predictionTree[index]
                if predictedValue is not None :
                    predictedTag = Tag.createFromDef(predictedValue["TagDefTuple"])                            
                    if (not self.getPredictionProbThreshold ()) : #thresholds set then just choose max                    
                        value = predictedValue["TrueValue"]                
                    else: #thresholds are set                     
                        if (float (predictedValue["TrueThreshold"]) <= float (sliceResults[index])):                        
                            value = predictedValue["TrueValue"]                
                        else:
                            value = predictedValue["FalseValue"]                
                    ML_ApplyTagPrediction._setTagValue (roiDictionary, predictedTag, value, setDescriptionPrediction)
                    resultLogTxtList.append ("Predict Index: %d, Value: %s" % (index, str(value)))
            else:                
                resultLogTxtList.append ("Multi-Classification Prediction  (Voting Mode: "+ Mode +")")
                self.printClassificationSummary (-1, sliceResults, resultLogTxtList)
                
                predictionTree = self._PredictionMap.getTree ()     
                resultlst = []
                for predictionIndex in range (sliceResults.shape[0]) :
                    predictedValue = predictionTree[predictionIndex] 
                    if (predictedValue is not None) :
                        predictedTag = Tag.createFromDef(predictedValue["TagDefTuple"])                                                
                        if (float (predictedValue["TrueThreshold"]) <= float (sliceResults[predictionIndex])):                        
                            value = predictedValue["TrueValue"]                
                        else:
                            value = predictedValue["FalseValue"]                
                        ML_ApplyTagPrediction._setTagValue (roiDictionary, predictedTag, value, setDescriptionPrediction)
                        resultlst.append (str (value))
                resultLogTxtList.append ("Predicting: " + ", ".join (resultlst))
        print ("Finished classification prediction")
        return resultLogTxtList, None
    
    def getPredictionTagNameList (self):
        TagList = []
        for index, tagDictionary in enumerate(self._PredictionMap.getTree ().values ()):           
            if tagDictionary is not None :                
                try :
                    tag = Tag.createFromDef(tagDictionary["TagDefTuple"])            
                    tagName = tag.getName()
                    value = tagDictionary["TrueValue"]    
                    TagList.append ((index, tagName + "set to " + str (value)))
                except:
                    pass
        return TagList
    
    def doesSetDescriptionTag (self):
        for  tagDictionary in self._PredictionMap.getTree ().values ():           
            try :
                tag = Tag.createFromDef(tagDictionary["TagDefTuple"])            
                tagName = tag.getName()
                if (tagName == "Description") :
                    return True
            except:
                pass
        return False
    
    def getPredictionHtmlDescription (self, thresholdset) :        
        html = []
        first = True
        try:
            for key, tagDictionary in self._PredictionMap.getTree ().items ():           
                if first :
                    first = False
                    if thresholdset  :            
                        html += ["<table Width=800><tr><th>Model Output</th>"]
                    else:
                        html += ["<table Width=500><tr><th>Model Output</th>"]
                    html += ["<th>Tag Name</th>"]       
                    if thresholdset  :            
                        html += ["<th>True Threshold</th>"]
                    html += ["<th>True Value</th>"]          
                    if thresholdset  :            
                        html += ["<th>False Value</th></tr>"]
                html += ["<tr>"] 
                html += ["<td style='text-align:center'>" + str (key) + "</td>"]            
                if tagDictionary is None :
                    html += ["<td style='text-align:center'>Value not set</td>"]
                    if thresholdset :
                        html += ["<td style='text-align:center'>Value not set</td>"]
                    html += ["<td style='text-align:center'>Value not set</td>"]
                    if thresholdset :
                        html += ["<td style='text-align:center'>Value not set</td>"]
                else:
                    try :
                        tag = Tag.createFromDef(tagDictionary["TagDefTuple"])            
                        tagName = tag.getName()
                    except:
                        tagName = "Value not set"
                    html += ["<td style='text-align:center'>" + tagName + "</td>"]
                    if thresholdset :
                        if "TrueThreshold" in tagDictionary :            
                            html += ["<td style='text-align:center'>" + str (tagDictionary["TrueThreshold"]) + "</td>"]            
                        else:
                            html += ["<td style='text-align:center'>Value not set </td>"]            
                    html += ["<td style='text-align:center'>" + str (tagDictionary["TrueValue"]) + "</td>"]
                    if thresholdset  :
                        if "FalseValue" in tagDictionary :            
                            html += ["<td style='text-align:center'>" + str(tagDictionary["FalseValue"]) + "</td>"]
                        else:
                            html += ["<td style='text-align:center'>Value not set </td>"]            
                html += ["</tr>"]
            html += ["</table>"]
        except:
            html = ["<h3>A error occured generating summary</h3>"]
        return "".join (html)
    
    def _shouldApplyToModel (self, roiDictionary, options) :         
                
        #options["ApplyOn"] = applyModelOn  Currently has no meaning for classification model if classification model were to add points as slice flags then this would change
        try :
            predictionTree = self._PredictionMap.getTree ()            
            UndefinedTags = False
            for predictionIndex in predictionTree.keys () :
                predictedValue = predictionTree[predictionIndex] 
                if predictedValue is None :
                    MessageBoxUtil.showMessage (WindowTitle="Model Exception",WindowMessage = "Model predictions are undefined.")
                    return False                     
                else:
                    predictedTag = Tag.createFromDef(predictedValue["TagDefTuple"])
                    if (predictedTag.getType () == "Description") :
                        if roiDictionary.getDescription ().lower () == "unknown" :
                            UndefinedTags = True
                            break
                    else:
                        tagManger = roiDictionary.getDataFileTagManager ()
                        if tagManger.getTagByIdentifer (predictedTag) is None :            
                            UndefinedTags = True
                            break
                
            existingData = options["ExistingData"] 
            if (existingData == "DoNotAlter" and not UndefinedTags) :
                return False
            return True
        except:
            return False
         
    
    def getTreePath (self) :        
        modelsTreePath = copy.copy (self._TreePath)
        if (modelsTreePath[0].lower () != "classification") :
            modelsTreePath = ["Classification"] + modelsTreePath
            self.setTreePath (modelsTreePath) 
        return modelsTreePath
    
    def isSegmentationModel (self) :
        return False
    
    def getFileObject (self) :
        fobj = FileObject ("ML_ApplyTagPrediction")                        
        fobj = ML_ModelPredictBase._addToFileObject (self, fobj)
        fobj.addInnerFileObject (self._PredictionMap.getFileObject ())
        fobj.setParameter ("MultiSliceVoting", self._MultiSliceVoting) 
        ML_ModelPredictBase._addToFileObject (self, fobj)        
        return fobj
    
    @staticmethod
    def loadFromFileObj (fobj, ProjectDataset) :
        model = ML_ApplyTagPrediction (ProjectDataset)
        model._initFromFileObject (fobj, ProjectDataset)
        obj = fobj.getFileObject ("PredictionMap")
        model._PredictionMap = ML_PredictionMap.loadFromFileObject (obj)
        if (fobj.hasParameter ("MultiSliceVoting")) :
             model.setMultiSliceVoting (fobj.getParameter ("MultiSliceVoting"))
        else:
            model.setMultiSliceVoting ("Average")
        return model    
    
    def getPredictionMap (self) :
        return self._PredictionMap.copy ()
    
    def copy (self) :
        model = ML_ApplyTagPrediction (self.getKerasModel ().getProjectDataset ())   
        ML_ModelPredictBase.innerCopy (self, model)
        model._PredictionMap = model._PredictionMap.copy ()
        return model                
    
    def setPredictionMap (self, predictionMap) :        
        if self._PredictionMap.setPredictionMap (predictionMap) :
           self._modelChanged.append ("predictionMap")                          
    
    @staticmethod
    def _setTagValue (ROIDict, targetTag, outputstr, setDescriptionPrediction) :
        if targetTag is not None :
            if targetTag.getType () == "Description" :
                if (setDescriptionPrediction) :
                    tagmanger = ROIDict.getDataFileTagManager ()
                    tagmanger.setInternalTag ("ML_DatasetDescription", outputstr)
                    ROIDict.setScanPhase ("Unknown")
                else:
                    ROIDict.setScanPhase (outputstr)
            else:
                targetTagName = targetTag.getName ()
                tagmanger = ROIDict.getDataFileTagManager ()
                for tagIndex in range (tagmanger.getCustomTagCount ()) :
                    tag = tagmanger.getCustomTagByIndex (tagIndex)
                    if (tag.getName () == targetTagName) :
                        tag.setValue (outputstr)
                        return 
                tag = targetTag.copy ()
                tag.setValue (outputstr)
                tagmanger.addTag (tag)
       
         

    

class ML_ModelFileIO :
    
    @staticmethod 
    def loadModelDef (modelPath, ProjectDataset) :
        f = FileInterface ()
        f.readDataFile (modelPath)        
        for fileobj in f.getFileObjects () :
            loadedmodel = None
            if fileobj.getFileObjectName () == "ML_ApplyTagPrediction" :                                    
                    loadedmodel = ML_ApplyTagPrediction.loadFromFileObj (fileobj, ProjectDataset)
                    
                    
            elif fileobj.getFileObjectName () == "ML_ApplyROI2DPrediction" :                    
                    loadedmodel = ML_ApplyROI2DPrediction.loadFromFileObj (fileobj, ProjectDataset)
                    try :
                        if modelPath.startswith (ProjectDataset.getMLHomeFilePath ()) :                    
                            if ML_ApplyROI2DPrediction.isPredictionMapMissingUUID (loadedmodel) : #test is ML model is missing UUID
                                try :
                                    ML_ModelFileIO.saveModelDef (loadedmodel, modelPath) # save model if UUID were generated
                                except:
                                    print ("ERROR: could not save ML model.")
                    except:
                        print ("ERROR: occured testing ML Model ROIDef.")
                        
            if (loadedmodel is not None) :
                loadedmodel.setModelPath (modelPath)
                currentFilename = loadedmodel.getCurrentKerasModelHDF5Path ()
                if (currentFilename is None or not os.path.isfile (currentFilename) or not os.path.exists (currentFilename)) and modelPath.lower().endswith (".model") :
                    tryPath = modelPath[:-len (".model")] + ".hdf5"
                    if os.path.isfile (tryPath) and  os.path.exists (tryPath) :
                         loadedmodel.setKerasModelHDF5Path (tryPath)
                         
                return loadedmodel
        return None
    
    @staticmethod
    def saveModelDef (model, path) :
        model.setModelPath (path)        
        fobj = FileInterface ()             
        fobj.addFileObjects (model.getFileObject ())            
        fobj.writeDataFile (path)      

