import os
from six import *
from rilcontourlib.util.FileUtil import FileUtil
from rilcontourlib.util.DateUtil import TimeUtil, DateUtil
from uuid import uuid4
import time
import shutil
from rilcontourlib.util.rcc_util import FileInterface, FileObject, FileChangeMonitor
import tempfile
import copy
from rilcontourlib.dataset.roi.rcc_roi import ROIDictionary

class FS_Version (object):        
    def __init__ (self,LiveDocumentPath, VersionNumber = None, PLockUID = None, FSHandle = None, VersionDocPath = None, Comment = None, PreviousVersionUID = None, VersionParam = None, RolledBackUID = None, ProjectFileInterface = None) :        
        self._LiveDocumentPath = LiveDocumentPath
        if VersionParam is not None :        
            self._versionParams = VersionParam
            try:
                if self._versionParams["LiveDoc"]["UID"] is None :
                    dirPath, _ = os.path.split (self._LiveDocumentPath)
                    for item in self._versionParams["V_DocRelativePath"] :
                       dirPath = os.path.join (dirPath, item)
                    if os.path.exists (dirPath) :                                                
                       self._versionParams["LiveDoc"]["UID"] = ROIDictionary.getDocumentUIDFromFile (dirPath, ProjectFileInterface)                
            except:
                pass                
        else:
            self._versionParams = {}
            self._versionParams["VersionUID"] = "VersionUID_"+ str (uuid4()) + "_" + str (time.time ())
            self._versionParams["PLockUID"] = PLockUID
            self._versionParams["V_Number"] = VersionNumber
            self._versionParams["PreviousVersionUID"] = PreviousVersionUID
            self._versionParams["LiveDoc"] = {}
            self._versionParams["RollBackOriginalVersionUID"] = RolledBackUID
            self._versionParams["User"] = {}
            if FSHandle is None :                
                self._versionParams["LiveDoc"]["UID"] = "DocumentDoesNotExist" 
                self._versionParams["LiveDoc"]["ChangeMonitor"] = None
                self._versionParams["User"]["Name"] = None
                self._versionParams["User"]["UID"] = None
            else:                
                self._versionParams["LiveDoc"]["UID"] = FSHandle.getDocumentUID ()
                fileChangeMon = FileChangeMonitor (FSHandle.getPath () )
                self._versionParams["LiveDoc"]["ChangeMonitor"] = fileChangeMon.getMonitorDescription ()
                self._versionParams["User"]["Name"] = FSHandle.getUserName ()
                self._versionParams["User"]["UID"] = FSHandle.getUserUID ()                
            self._versionParams["Created"] = {"Time" : TimeUtil.getTimeNow (), "Date" : DateUtil.today ()}              
            self._versionParams["V_DocRelativePath"] = VersionDocPath            
            if Comment is None :
                Comment = ""
            self._versionParams["Comment"] = Comment
    
    def copy (self, FileChangeMonitor = None) :
        returnVersion = copy.deepcopy (self)
        if FileChangeMonitor is not None :
            returnVersion._versionParams["LiveDoc"]["ChangeMonitor"] = FileChangeMonitor.getMonitorDescription ()
        return returnVersion
    
    def getLiveDocPath (self) :
        return self._LiveDocumentPath
    
    def getPreviousVersionUID (self) :
        return self._versionParams["PreviousVersionUID"]
   
    def wasParentVersion (self, testParent) :
        parent = self.getPreviousVersionUID ()
        if testParent is None and parent is None :
            return True
        if parent is None or testParent is None:
            return False
        return parent == testParent.getVersionUID ()
        
    def getLiveDocumentChangeMonitor (self, FilePath) :
        changeMonDescription = self._versionParams["LiveDoc"]["ChangeMonitor"]
        if changeMonDescription is None :
            return None
        fc = FileChangeMonitor ()
        fc.initFromDescription (changeMonDescription, CorrectFilePath = FilePath)
        return fc 
    
    def hasDocumentChanged (self) :
        liveDocPath = self.getLiveDocPath ()
        changeMon = self.getLiveDocumentChangeMonitor (liveDocPath)
        if not os.path.exists (liveDocPath) :
            if changeMon is not None :
                return True # document removed after version made
            return False
        else:        
            if changeMon is None :
                return True # document created after version made
            dirname, _ = os.path.split (liveDocPath)
            versionFilePath = dirname
            for item in self.getVersionDocumentRelativePathList () :
                versionFilePath = os.path.join (versionFilePath, item)
            return changeMon.hasFileChanged (versionFilePath, ByteCompare = True, IgnoreFileName = True)

    def wasDocumentCreatedByUser (self, userName, userID) :
        uName = self.getUserName ()
        uID = self.getUserUID ()
        if uID is not None and userID is not None :
           return (uID == userID) 
        if userName is not None and uName is not None :
            return (userName.lower() == uName.lower())    
        return False
    
    def isRollback (self) :
        return self.getOriginalVersionUID () is not None
    
    def getOriginalVersionUID (self) :
        if "RollBackOriginalVersionUID" not in self._versionParams :
            return None
        return self._versionParams["RollBackOriginalVersionUID"]
    
    def isVersionForDocument (self, docuid) :        
        return self.getLiveDocUID () == docuid
    
    def getVersionUID (self) :
        return self._versionParams["VersionUID"]
    
    def getPLockUID (self) :
        return self._versionParams["PLockUID"]
    
    def getVersionNumber (self) :
        return self._versionParams["V_Number"]    
        
    def getLiveDocUID (self) :
        return self._versionParams["LiveDoc"]["UID"]
    
    def getUserName (self) :
        return self._versionParams["User"]["Name"]
    
    def getUserUID (self) :
        return self._versionParams["User"]["UID"]
    
    def getTimeCreated (self) :
        return self._versionParams["Created"]["Time"]
    
    def getDateCreated (self) :
        return self._versionParams["Created"]["Date"]
    
    def getVersionDocumentRelativePathList (self) :
        return self._versionParams["V_DocRelativePath"]
    
    def getVersionComment (self) :
        return self._versionParams["Comment"]
    
    def getFileObject (self, index) :
        fobj = FileObject ("Ver_" + str(index)) 
        fobj.setParameter ("Ver_Param", self._versionParams)
        return fobj
    
    @staticmethod
    def createFromFileObject (LiveDocumentPath, fobj, ProjectFileInterface) :
        if not fobj.hasParameter ("Ver_Param") :
            return None        
        versionParams = fobj.getParameter ("Ver_Param") 
        return FS_Version (LiveDocumentPath, VersionParam = versionParams, ProjectFileInterface = ProjectFileInterface)          

class FS_FileVersionManager :
    
    def __init__ (self, roiDatasetPath, ProjectFileInterface) :        
        self._versionDirName = "v"
        self._ProjectFileInterface = ProjectFileInterface
        self._roiDatasetPath = roiDatasetPath        
        self._fileVersionManagerPath = FileUtil.changeExtention (roiDatasetPath, "_ROI.txt",".ver")        
        baseDirName, filename = os.path.split (self._fileVersionManagerPath)
        if not os.path.isdir (baseDirName) :
            self._fileVersionManagerPath = None
        else:
            versionDir = os.path.join (baseDirName, self._versionDirName)
            if not os.path.isdir (versionDir) :
                try :
                    os.mkdir (versionDir)
                except:
                    print ("")
                    print ("ERROR: could not create version directory, " + versionDir)
                    print ("")
                    pass
            if not os.path.isdir (versionDir) :
                self._fileVersionManagerPath = None
            else:
                self._fileVersionManagerPath = os.path.join (versionDir, filename)

    def getFilePermissions (self) :
        try :
            return FileUtil.getFilePermissions (self._roiDatasetPath)
        except:
            return None
        
    def isEnabled (self) : 
        return self._fileVersionManagerPath is not None
    
    @staticmethod    
    def _read (fileVersionManagerPath, ProjectFileInterface):
        VersionList = []    
        try :
            if os.path.exists (fileVersionManagerPath) : 
                fi = FileInterface ()
                fi.readDataFile (fileVersionManagerPath)        
                header = fi.getFileHeader ()         
                if header.hasParameter ("VersionCount") :
                   vc = header.getParameter ("VersionCount")
                   dirName, fname = os.path.split (fileVersionManagerPath)
                   basedirName, _ = os.path.split (dirName)
                   fname = FileUtil.changeExtention (fname, ".ver", "_ROI.txt")         
                   targetFilePath = os.path.join (basedirName, fname)
                   for versionCount in range (vc)  :
                        try :
                            objectName = "Ver_" + str (int (versionCount))
                            fobj = fi.getFileObject (objectName)
                            if (fobj is not None ) :                                
                                version = FS_Version.createFromFileObject (targetFilePath, fobj, ProjectFileInterface)
                                if version is not None :
                                    VersionList.append (version)
                        except:
                            pass
        except:
            pass        
        finally:
            return VersionList
                
    @staticmethod    
    def _save (fileVersionManagerPath, VList, FileSavePermissions = None) :
        fi = FileInterface ()
        header = fi.getFileHeader ()
        header.setParameter ("VersionCount", len (VList))
        for index, version in enumerate (VList) :
            fi.addFileObject (version.getFileObject (index))
        fi.writeDataFile (fileVersionManagerPath, FileSavePermissions = FileSavePermissions)
        
   
        
    def getDocumentVersionHistory (self, FSHandle) :
        if self._fileVersionManagerPath is None :
            return None # could not create version file path
        returnLst = []        
        DocUID = FSHandle.getDocumentUID ()
        if DocUID is None :
            return returnLst
                
        VersionList = FS_FileVersionManager._read (self._fileVersionManagerPath, self._ProjectFileInterface)
        for fileVersion in VersionList :
            if fileVersion.isVersionForDocument (DocUID) :
                returnLst.append (fileVersion)
        return returnLst
    
    def isLiveDocumentDifferentThanVersion (self, version) : 
        if self._fileVersionManagerPath is None :
            return None # could not create version file path
        return version.hasDocumentChanged ()
    
    def getVersionParent (self, version):    
        if self._fileVersionManagerPath is None :
            return None # could not create version file path
        VersionList = FS_FileVersionManager._read (self._fileVersionManagerPath, self._ProjectFileInterface)
        for fileVersion in VersionList :
            if version.wasParentVersion (fileVersion):
                return fileVersion
        return None    
    
    def removeFileVersionsAfter (self, startVersionUID) :
         if self._fileVersionManagerPath is None :
            return None # could not create version file path
         VersionList = FS_FileVersionManager._read (self._fileVersionManagerPath, self._ProjectFileInterface)        
         startVersionFound = None         
         removeVersionList = []
         for index, version in enumerate (VersionList) :             
             if version.getVersionUID () != startVersionUID :
                 continue
             else:
                 startVersionFound = index
                 sourceVersionFile = self.getVersionedDocumentPath (version)
                 if sourceVersionFile is None :
                     return
                 filepermissions = self.getFilePermissions ()
                 shutil.copyfile (sourceVersionFile, self._roiDatasetPath)
                 FileUtil.setFilePermissions (self._roiDatasetPath, filepermissions)
                 print ("Rolling Back From: " + sourceVersionFile)
                 print ("Rolling Back   To: " + self._roiDatasetPath)
                 break
         if startVersionFound is not None :
             removeVersionList = list (range (startVersionFound + 1, len (VersionList)))             
             if len (removeVersionList) > 0 :
                removeVersionList.reverse ()
                removeVersionFileLst = []
                for index in removeVersionList :
                    removeVersionFileLst.append (self.getVersionedDocumentPath (VersionList[index]))
                    del VersionList[index]
                FS_FileVersionManager._save (self._fileVersionManagerPath, VersionList, FileSavePermissions = self.getFilePermissions ())
                for fname in removeVersionFileLst : 
                    if fname is not None :
                        try:
                            os.remove (fname)
                        except:
                            print ("")
                            print ("ERROR: trying to remove redunant version file: " + fname)
                            print ("")
       
            
                    
    def removeUncessaryFileVersionsBetween (self, startVersionUID, endVersionUID) :
         if self._fileVersionManagerPath is None :
            return None # could not create version file path
         VersionList = FS_FileVersionManager._read (self._fileVersionManagerPath, self._ProjectFileInterface)        
         startVersionFound = False
         previousRollback = None
         removeVersionList = []
         for index, version in enumerate (VersionList) :
             if not startVersionFound :
                 if version.getVersionUID () != startVersionUID :
                     continue
                 else:
                    startVersionFound = True
                    continue
             if version.isRollback () :
                if previousRollback is not None :
                    removeVersionList.append (previousRollback)
                previousRollback = index
             else:
                 previousRollback = None
             if version.getVersionUID () == endVersionUID :
                break
         if len (removeVersionList) > 0 :
            removeVersionList.reverse ()
            removeVersionFileLst = []
            for index in removeVersionList :
                removeVersionFileLst.append (self.getVersionedDocumentPath (VersionList[index]))
                del VersionList[index]
            FS_FileVersionManager._save (self._fileVersionManagerPath, VersionList, FileSavePermissions = self.getFilePermissions ())
            for fname in removeVersionFileLst : 
                if fname is not None :
                    try:
                        os.remove (fname)
                    except:
                        print ("")
                        print ("ERROR: trying to remove redunant version file: " + fname)
                        print ("")
    
    def versionDocument (self, FSHandle, Comment = None, VersionList = None, RolledBackVersion = None, ForceVersion = False) :
        if self._fileVersionManagerPath is None :
            return None # could not create version file path
        path = FSHandle.getPath ()
        if not os.path.exists (path) :
            return None 
     
        if not FSHandle.flushFileToDisk () :
            return None
                    
        fileVersionPath = None
        try :
            PLock = FSHandle.getCurrentFilePLock ()
            if PLock is None or "PLockUID" not in PLock :
                PLockUID = None
            else:
                PLockUID = PLock["PLockUID"]
        except:
            return None
        try :            
            if VersionList is None :
                VersionList = FS_FileVersionManager._read (self._fileVersionManagerPath, self._ProjectFileInterface)                
            if not ForceVersion and len (VersionList) > 0 :
                if not VersionList[-1].hasDocumentChanged () :
                    return VersionList # file not changed from previous version. Versioning skipped
              
            if len (VersionList) <= 0 :
                nextVersionNumber = 1 
            else:               
                 nextVersionNumber = VersionList[-1].getVersionNumber() + 1
            _, fname = os.path.split (path)
            LiveDocumentPath = path
            fname = FileUtil.changeExtention (fname, "_ROI.txt",".v" + str (nextVersionNumber))            
            fileVersionPath, _ = os.path.split (self._fileVersionManagerPath)
            fileVersionPath = os.path.join (fileVersionPath, fname)
            shutil.copyfile (path, fileVersionPath)    
            FileUtil.setFilePermissions (fileVersionPath, self.getFilePermissions ())            
            if len (VersionList) <= 0 :
                PreviousVersionUID = None
            else:
                PreviousVersionUID = VersionList[-1].getVersionUID ()
                       
            if RolledBackVersion is None :
                RolledBackUID = None
            else:
                RolledBackUID = RolledBackVersion.getVersionUID ()
            if Comment is not None :
                Comment = Comment.replace ("\n","<br>")
            versionLog = FS_Version (LiveDocumentPath, VersionNumber = nextVersionNumber, PLockUID = PLockUID, FSHandle = FSHandle, VersionDocPath = [self._versionDirName,fname], Comment = Comment, PreviousVersionUID = PreviousVersionUID, RolledBackUID = RolledBackUID)            
            VersionList.append (versionLog)
            FS_FileVersionManager._save (self._fileVersionManagerPath, VersionList, FileSavePermissions = self.getFilePermissions ())
            return VersionList
        except:
            if os.path.exists (fileVersionPath) :
                try:
                    os.remove (fileVersionPath) 
                except:
                    pass
            return None
    
    @staticmethod
    def _indentComments (comments) : 
        IndentedLines = [] 
        commentLines = comments.split ("<br>")
        for cm in commentLines :
            IndentedLines.append ("&nbsp;&nbsp;&nbsp;&nbsp;" + cm)
        return "<br>".join (IndentedLines)
    
    @staticmethod
    def _getVersionFromUID (uid, versionList) :
        if uid is None :
            return None
        for version in versionList :
            if version.getVersionUID () == uid :
                return version
        return None
    
    
    def syncToVersionedDocument (self, version, FSHandle, Comment = None) :
        if self._fileVersionManagerPath is None :
            return None # could not create version file path
        path = FSHandle.getPath ()
        if not os.path.exists (path) :
            return None
              
        if not FSHandle.flushFileToDisk () :
            return None
        try :            
            VersionList = FS_FileVersionManager._read (self._fileVersionManagerPath, self._ProjectFileInterface)                            
            if len (VersionList) <= 0 :
                return None        
            
            IsLastVersionSelected = VersionList[-1].getVersionUID () == version.getVersionUID ()
            if VersionList[-1].isRollback () :
                 IsPreviosVersionRollback = VersionList[-1]
            else:
                IsPreviosVersionRollback = None 
                
            RolledBackVersion = version
            if RolledBackVersion.isRollback () :
                origionalVersion = FS_FileVersionManager._getVersionFromUID (RolledBackVersion.getOriginalVersionUID (), VersionList)
                if origionalVersion is not None :
                    RolledBackVersion = origionalVersion.copy (FileChangeMonitor = RolledBackVersion.getLiveDocumentChangeMonitor ())
                    
            if not version.isVersionForDocument (FSHandle.getDocumentUID ()) :
                return None     

            versionedFile = self.getVersionedDocumentPath (version)                
            if versionedFile is None :
                return None
            if not os.path.exists (versionedFile) :
                print ("Error versioned file missing.")
                return None            
            roiDataDirName, _ = os.path.split (self._roiDatasetPath)
            temppath = None    
            filePermission = self.getFilePermissions ()            
            try:
                handle ,temppath = tempfile.mkstemp (dir = roiDataDirName)
                os.close (handle) 
                shutil.copyfile (self._roiDatasetPath,temppath)
                try :
                    shutil.copyfile (versionedFile, self._roiDatasetPath)  
                    if not IsLastVersionSelected :
                        previousVersionNumber = RolledBackVersion.getVersionNumber ()
                        BaseComment = "<b>Syncing to previous version #%d</b>" % (previousVersionNumber)
                        previousFileComments = RolledBackVersion.getVersionComment ()
                        if previousFileComments is not None and previousFileComments != "" :
                            BaseComment +="<hr>"
                            BaseComment +="<center>Version %d File Comments</center>" % (previousVersionNumber)
                            BaseComment +="<hr>\n"
                            BaseComment += FS_FileVersionManager._indentComments (previousFileComments)
                            BaseComment +="<hr>\n"
                        if Comment is None :
                            Comment = BaseComment
                        else:
                            Comment =  BaseComment + "\n" + Comment
                        VersionList = self.versionDocument (FSHandle, Comment = Comment , VersionList = VersionList, RolledBackVersion = RolledBackVersion)
                        if IsPreviosVersionRollback is not None and not FSHandle.hasPLockDataFile ():
                            try:
                                del VersionList[VersionList.index (IsPreviosVersionRollback)]
                                FS_FileVersionManager._save (self._fileVersionManagerPath, VersionList, FileSavePermissions = self.getFilePermissions ())
                                os.remove (self.getVersionedDocumentPath (IsPreviosVersionRollback))
                            except:
                                pass
                    if VersionList is not None :
                        return VersionList[-1]
                    else:
                        shutil.copyfile (temppath, self._roiDatasetPath)                
                        return None
                except:
                    shutil.copyfile (temppath, self._roiDatasetPath)
            finally:
                FileUtil.setFilePermissions (self._roiDatasetPath, filePermission)
                if temppath is not None :
                    try:
                        os.remove (temppath)
                    except:
                        pass
        except:
            pass
        return None        
    
    def getVersionNumberAndPathFromUID (self, UID) :
        if self._fileVersionManagerPath is None :
            return None, None # could not create version file path
        if UID is None :
            return None, None
        VersionList = FS_FileVersionManager._read (self._fileVersionManagerPath, self._ProjectFileInterface)     
        version = self._getVersionFromUID (UID, VersionList)
        if version is None :
           return None, None
        return version.getVersionNumber (), self.getVersionedDocumentPath (version)
    
       
    def getVersionedDocumentPath (self, version) :
        if self._fileVersionManagerPath is None :
            return None # could not create version file path
        if version is None :
            return None
        dirName, _ = os.path.split (self._roiDatasetPath)
        for name in version.getVersionDocumentRelativePathList () :
            dirName = os.path.join (dirName, name)
        return dirName
