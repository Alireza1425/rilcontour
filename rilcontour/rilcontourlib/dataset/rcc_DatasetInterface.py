#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 14 11:48:59 2016

@author: m160897
"""
import tempfile
import os
import copy
import random
import weakref
import json
from uuid import uuid4
import shutil
import gc
import numpy as np
import shlex
#import time
try:
    from pathlib import Path
    print ("Importing: pathlib")
except:
    from pathlib2 import Path
    print ("Importing: pathlib2")
from PyQt5 import QtCore, QtGui
from PyQt5.QtCore import QModelIndex, QThread, pyqtSignal, QObject
from PyQt5.QtWidgets import  QApplication, QDialog, QFileDialog, QProgressDialog

from rilcontourlib.dataset.rcc_DatasetROIInterface import RCC_ROIDataset_PASSCARA, RCC_ROIDataset_FileSystemInterface
from rilcontourlib.dataset.roi.rcc_roi import ROIDictionary, ROIDefinitions
from rilcontourlib.util.FileUtil import FileUtil
from rilcontourlib.util.LongFileNameAPI import LongFileNameAPI, LongFileNameCache
from rilcontourlib.util.rcc_util import FileObject, FileInterface, ANTSRegistrationLog, DicomVolume, DatasetTagManager, DatasetUIHeader, PenSettings, RelativePathUtil, PythonVersionTest, FileChangeMonitor, MessageBoxUtil, PlatformMP, PlatformThreading
from rilcontourlib.util.rcc_NIfTIDataFileHandlers import PesscaraDF, FileSystemDF
from rilcontourlib.ui.rcc_contourwindow import RCC_ContourWindow
from rilcontourlib.ui.rcc_datasetui import RCC_DatasetDlg
from rilcontourlib.pesscara.rcc_pesscarainterface import RCC_PesscaraInterface
from rilcontourlib.ui.qt_ui_autogen.RCC_SplashScreenAutogen import Ui_RCC_SplashScreen
import pkg_resources
import pandas as pd
from rilcontourlib.dataset.rcc_DatasetROIInterface import RCC_ROIDataset_FileSystemObject
import time
from rilcontourlib.machinelearning.ml_DatasetInterface import ML_KerasModelLoaderInterface
from rilcontourlib.dataset.RCC_ProjFileInterface import ProjFileInterface
import getpass
from rilcontourlib.dataset.RCC_ROIFileLocks import PersistentFSLock, NonPersistentFSLock
from rilcontourlib.util.DateUtil import TimeUtil, DateUtil
from rilcontourlib.ui.RCC_FileLockUI import LoadROIWarrningDlg
from rilcontourlib.dataset.rcc_multiusermanager import MultiUserManager
import multiprocessing
from rilcontourlib.util.FileUtil import ScanDir
from collections import OrderedDict
from functools import partial

class RCC_SplashScreenDlg (QDialog) :
    def __init__ (self, ProjectDataset, parent) :        
        QDialog.__init__ (self, parent)        
        self._wasCancled = False
        self._StopLoadingButtonEnabled = False
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)         
        self.ui = Ui_RCC_SplashScreen ()        
        self.ui.setupUi (self)         
        self._SplashStarted = time.time()
        self._ProjectDataset = ProjectDataset
        self.setWindowTitle ("Starting RIL-Contour")      
        self.setWindowFlags( QtCore.Qt.WindowStaysOnTopHint |   QtCore.Qt.FramelessWindowHint)    #|    QtCore.Qt.SplashScreen ) #  
        self.setWindowModality(QtCore.Qt.ApplicationModal)        
        self.setProgressRangeIndeterminate ()
        self.setMessage ("Initializing")
        self.ui.splashCanceledBtn.setEnabled (False)
        self.ui.splashCanceledBtn.setAutoDefault (False)
        self.ui.splashCanceledBtn.setDefault (False)
        self.ui.splashCanceledBtn.setVisible (False)
        self.setSplashMessageWidth (381)        
        if (ProjectDataset is not None) :
            basepath = ProjectDataset.getPythonProjectBasePath ()
            iconPath = os.path.join (basepath , "icons")
            graphic = QtGui.QPixmap(os.path.join (iconPath,"radiology-informatics-main.png"))      
            self.ui.splashGraphic.setPixmap(graphic)        
        self.canceled = self.ui.splashCanceledBtn.clicked
        self.ui.splashCanceledBtn.clicked.connect (self.setCanceled)       
        try :
            RilcontourVersion = pkg_resources.get_distribution("rilcontour").version
        except :
            RilcontourVersion = "Could not detect RIL-Contour version."       
        
        self.ui.Version.setText ("Starting RIL-Contour version %s" % RilcontourVersion)
        self.setModal (True)
        self.show()        
        self.setValue (0)
        self.update ()
        self.processSystemEvents ()        
        
    
    def setCanceled (self) :
        self._wasCancled = True
        self.ui.splashCanceledBtn.setEnabled (False)
        
    def wasCanceled (self) :
        return self._wasCancled 
    
    def processSystemEvents (self) :
        if (self._ProjectDataset is not None) :
            self._ProjectDataset.getApp ().processEvents()
            
    def setProgressRangeIndeterminate (self):
        self.setProgressRange (0,0)
        
    def setProgressRange (self, start,stop) :
        self.ui.splashProgress.setRange (start,stop)
    
    def setMaximum (self, maxProgress)  :
        self.ui.splashProgress.setMaximum (maxProgress)
    
    def setMinimum (self, minProgress)  :
        self.ui.splashProgress.setMinimum (minProgress)
    
    def setSplashMessageWidth (self, width):
        h = self.ui.splashMessage.height ()
        self.ui.splashMessage.resize (width, h)    
       
    def _updateUI (self) :
        if (not self._StopLoadingButtonEnabled and self.getTimeElapsed () > 10) :            
            self.ui.splashCanceledBtn.setEnabled (True)
            self.ui.splashCanceledBtn.setVisible (True)
            self.setSplashMessageWidth (261)
            self._StopLoadingButtonEnabled = True
        self.processSystemEvents ()
        
    def setProgress (self, val) :
        self.ui.splashProgress.setValue (val)
        self._updateUI ()
    
    def setValue (self, val) :
        self.setProgress (val)
    
    def getValue (self) :
        return self.ui.splashProgress.value ()
        
    def setMessage (self, msg) :
        self.ui.splashMessage.setText (msg)
        self._updateUI ()
   
    def getTimeElapsed (self):
        return int (time.time() - self._SplashStarted)
    
    def setLabelText (self, msg) :
        self.setMessage (msg)
        
    def close (self):
        time.sleep (max (0, 5 - self.getTimeElapsed ()))
        QDialog.close (self)
        
   

class InternalROIClipBoard :
    def __init__ (self) :
        self._internalROIClipBoard = []
        
    def clearClipboard (self) :
        self._internalROIClipBoard = []
        
    def getCopiedROICount (self) :
        return len (self._internalROIClipBoard)
    
    def getCopiedROIByIndex (self, index) :
        if (index >= 0 and index <= len (self._internalROIClipBoard)) :
            roiDef, roiObj, voxelSize = self._internalROIClipBoard[index]
            if (roiDef is not None) :
                roiDef = roiDef.copy ()
            if (roiObj is not None) :
                roiObj = roiObj.copy ()            
            return (roiDef, roiObj, voxelSize)
        return (None, None, None)
    
    def addToClipBoard (self, roiDef, roiObj, voxelSize) :
        if (roiDef is not None) :
            self._internalROIClipBoard.append ((roiDef, roiObj, voxelSize))

      
        
class ProjectDatasetDefinition :

    def __init__ (self, app, appeventfilter, globalProjectList, BasePath = "", ROISaveThread = None, watchDogDirectoryObserver = None, RILContourCommandLine = None, MultiUserFileSharingManager = None) :            
        self._MultiProcessingManager = None
        self._DeepGrowROIModelInterface = None
        self._fileLockUID = "ProjectFileLockUID_" + str (uuid4()) + "_" + str (time.time ())
        self._setUserRootHomePath (ProjFileInterface.getRootHomePath (RILContourCommandLine))        
        self._UIHeaderChanged = False
        self._NiftiDatasetDlg= None 
        self._watchDogDirectoryObserver = watchDogDirectoryObserver
        self._project_lock_file = None
        self._project_lock_signature = None
        self._penSettings = None
        self._expandedTreeNodes = {}
        self._openAtStartFileList = []
        self._dialogImagingImportState = None
        self._expandedTreeNodesChanged = False
        self._projectPenSettingsChanged = False
        self._openFilesAtStartChanged = False
        self._ProjectFileInterface = ProjFileInterface(RILContourCommandLine = RILContourCommandLine, MultiUserFileSharingManager = MultiUserFileSharingManager) 
        self._setProjectReadOnly (False)            
        self._isRunningFromCommandlineSepcifiedDataset = False
        self._internalROIClipBoard = InternalROIClipBoard ()
        self._exportMaskDlgSettings = ()
        self._FirstDataSetLoaded = True
        self._BasePath = BasePath 
        self._ROISaveThread = ROISaveThread
        self._appeventfilter = appeventfilter
        self._appeventfilter.addProjectDataset (self)
        self._ContouringWindowList = []
        self._scanPhaseSelectionDlg = None
        self._projectWindowFocus = False
        self._activeWindow = None        
        self._globalProjectList = globalProjectList
        
        self._ProjectDescriptionContext = ""
        self._ApplicationActivate = True
        self._projectScanPhaseList = []
        self._selectedDataset = None
        if (ML_KerasModelLoaderInterface.isMLSupportEnabled ()) :
            print ("ML Enabled: Keras and Tensorflow Detected")
        else:
            print ("ML Disabled")
        self._ML_KerasModelLoaderInterface = None
        self._DatasetTagManager = DatasetTagManager ()
        self._TempStorageDir = tempfile.gettempdir()
        if (self not in self._globalProjectList) :
            self._globalProjectList.append (self)         
    

        
    def getImportImagingDlgState (self) :
        if self._dialogImagingImportState is None :
            self._dialogImagingImportState = {}
            self._dialogImagingImportState["Path"] = ""
            self._dialogImagingImportState["Organization"] = "Directory"
            self._dialogImagingImportState["PreferredFormat"] = ".nii.gz"
            self._dialogImagingImportState["PatientIDMapping"] = "subject_00-000-000"
            return self._dialogImagingImportState
        return self._dialogImagingImportState
    
    def setImportImagingDlgState (self, state, SaveChangesToProject = True) :
        changed = not SaveChangesToProject 
        if not changed :
            if self._dialogImagingImportState is None and state is not None :
               changed = True
        if not changed :
            for key, value in state.items () :
                if key not in self._dialogImagingImportState :
                    changed = True
                    break
                if self._dialogImagingImportState[key] != value :
                    changed = True
                    break
        if changed :
            self._dialogImagingImportState = copy.copy (state)
            if SaveChangesToProject :
                self.saveProject ()
        
    def isPesscaraInterface (self) :
        return self.getROIDatasetInterface ().isPesscaraInterface ()
    
    def areLongFileNamesShown (self) :
        try :            
            if self.isPesscaraInterface () :
                return False
            return not self.getNIfTIDatasetInterface().showShortFileName ()
        except :
            return False
        
    def getMultiprocessingManager (self) :
        if self._MultiProcessingManager is None :
            self._MultiProcessingManager = multiprocessing.Manager ()
        return self._MultiProcessingManager 
    
    def getDeepGrowROIModelInterface (self):
        if not ML_KerasModelLoaderInterface.isMLSupportEnabled () :
            return None
        else:
            from rilcontourlib.machinelearning.ml_DeepGrow import DeepGrowROIModelInterface
            if (self._DeepGrowROIModelInterface is None) :
                self._DeepGrowROIModelInterface = DeepGrowROIModelInterface (self.getHomeFilePath (), self.getPythonProjectBasePath ())
            return self._DeepGrowROIModelInterface 
    
    def _setProjectReadOnly (self, val) :
        self._ProjectFileInterface.setProjectReadOnly (val)
        
    def isProjectReadOnly (self) :
        return self._ProjectFileInterface.isProjectReadOnly ()
        
    def getRILContourCommandLine (self) :
        return self._ProjectFileInterface.getRILContourCommandLine ()
    
    def getWatchDogDirectoryObserver (self) :
        return self._watchDogDirectoryObserver
    
    def getProjectFileInterface (self) :
        return self._ProjectFileInterface
    
    def getDocumentUIDFileLogInterface (self) :
        return self._ProjectFileInterface.copy ()   
    
    def isInSafeCommandLineMode (self) :
        commandline = self.getRILContourCommandLine ()
        return commandline is not None and "safemode" in commandline
    
    def isInNoCacheCommandLineMode (self) :
        commandline = self.getRILContourCommandLine ()
        return commandline is not None and "nocache" in commandline
    
    def isInMultiUserCommandLineMode (self) :
        commandline = self.getRILContourCommandLine ()
        return commandline is not None and "multiuseryaml" in commandline
    
    def isInSuperUserCommandLineMode (self):
        commandline = self.getRILContourCommandLine ()
        return (commandline is not None and "superuser" in commandline)
     
    def isUserNameCommandLineMode (self):
         commandline = self.getRILContourCommandLine ()
         return (commandline is not None and "user" in commandline)
     
    def isRunningInDemoMode (self):
         commandline = self.getRILContourCommandLine ()
         return (commandline is not None and "demo" in commandline)
    
    def isHomePathCommandLineMode (self):
      commandline = self.getRILContourCommandLine ()
      return (commandline is not None and "userhomepath" in commandline)
      
    def lockProjectFile (self, filepath) :  
        
        # not best way to do this but test if project contains multi user sharing
        ProjectContainsEmbeddedMultiUserDef = False
        if not self.isInMultiUserCommandLineMode () and  not self.isInSuperUserCommandLineMode () and os.path.exists (filepath) :  # modes make test results uncenessary
            try:
                fobj = FileInterface ()   
                fobj.readDataFile (filepath)                       
                header = fobj.getFileHeader ()     
                    
                if header.hasParameter ("MultiUserSharingYAML") :
                    ProjectBasedMultiuserSharingYAMLDef = header.getParameter ("MultiUserSharingYAML")
                    if ProjectBasedMultiuserSharingYAMLDef is not None and len (ProjectBasedMultiuserSharingYAMLDef) > 0 :
                        mgr = MultiUserManager (None)
                        mgr.initFromText (ProjectBasedMultiuserSharingYAMLDef, filepath)
                        if mgr.isInitalized () :
                            ProjectContainsEmbeddedMultiUserDef = True              
            except:
                print ("Error occured testing the project for an embedded multi-user YAML")
                
        if (ProjectContainsEmbeddedMultiUserDef) or (self.isInMultiUserCommandLineMode () and not self.isInSuperUserCommandLineMode ()) or (os.path.join (self.getHomeFilePath (), "RCC_VisSettings.prj") == filepath) :
                OpenFile = True # open locked
                self._project_lock_file = None
                self._project_lock_signature = None
                self._setProjectReadOnly (not self.isInSuperUserCommandLineMode ())                    
                return OpenFile   
        lockfilePath = None
        lockSignature = None
        OpenFile = False        
        try :
            if filepath is not None and os.path.exists (filepath) :
                try: 
                    FSUID = self.getFileSystemUID ()
                    userName = "ProjectLockedBeforeUserNameLoaded"
                    userUID = userName #uid may not be valid 
                    lockfilePath = filepath[:-3] + "lock"
                    failCount = 0
                    while (True) :
                        lockSignature = NonPersistentFSLock.createLockFile (lockfilePath, userName, FSUID, userUID, WeakLocksEnabled = self.getProjectFileInterface().weakLocksCommandLineParameterEnabled (), FileHandleUID = self._fileLockUID) 
                        if lockSignature is None :
                            failCount += 1
                            if failCount > 5 :
                                OpenFile = True # open locked
                                self._project_lock_file = None
                                self._project_lock_signature = None
                                self._setProjectReadOnly (not self.isInSuperUserCommandLineMode ())                    
                                return OpenFile   
                        else: 
                           if "LockSignature" in lockSignature and lockSignature["LockSignature"] == FSUID :
                               OpenFile = True # open locked
                               break                           
                           else:
                              if "LockSignature" in lockSignature  :                                  
                                  details = lockSignature["LockSignature"] 
                              else:
                                  details = None 
                              
                              if self.isRunningInDemoMode () :
                                  NonPersistentFSLock.unlock (lockfilePath)                                                                    
                              else:
                                  unlockDataset = LoadROIWarrningDlg.showLoadROIWarrningDlg ("Loading Poject Warrning", "RIL-Contour detects this project is opened by another user. If you are the only person editing this project, this may occur if you have RIL-Contour open in another window, or if your previous session crashed. For projects involving 2+ users, it's recommended you take advantage of RIL-Contour's multi-user features. For more information, please see the GitLab page. Opening the project as readonly will allow you to annotate images. Modifiying saved project settings requires read/write access. How do you wish to proceed?", details = details, ReadWrite = True, ReadOnly = True)#self.isInSuperUserCommandLineMode (), ReadOnly = True)
                                  if (unlockDataset == "Readwrite") :
                                      NonPersistentFSLock.unlock (lockfilePath)                                                                    
                                  elif (unlockDataset == "Readonly") :
                                      OpenFile = True # open locked
                                      lockfilePath = None
                                      lockSignature = None                               
                                      break
                                  else:    
                                      lockfilePath = None
                                      lockSignature = None                               
                                      OpenFile = False
                                      break
                except:
                   OpenFile = False                   
        finally:
            if (lockSignature is None) :
                self._project_lock_file = None
                self._project_lock_signature = None
                self._setProjectReadOnly (not self.isInSuperUserCommandLineMode ())            
            else:
                self._project_lock_file = lockfilePath
                FileUtil.setFilePermissions (lockfilePath, FileUtil.getFilePermissions (filepath))                
                self._project_lock_signature = lockSignature                
                self._setProjectReadOnly (False)            
        return OpenFile
            
    @staticmethod
    def _getWindowFilePath (activeWindow) :
        try :
            loadedFile = activeWindow.getLoadedNIfTIFile ()
            if loadedFile is not None :
                fp = loadedFile.getFilePath ()
                if os.path.exists (fp) :
                    return fp
        except:
            pass
        return None
                
    def _getOpenFileList (self) :
        fileList = []
        currentWindow = self._activeWindow
        for activeWindow in self.getActiveContourWindowList () : 
            if activeWindow != currentWindow :
                path = ProjectDatasetDefinition._getWindowFilePath (activeWindow)
                if path is not None :
                    fileList.append (path)
        path = ProjectDatasetDefinition._getWindowFilePath (currentWindow)
        if path is not None :
            fileList.append (path)                
        return fileList
    

        
    def getKerasModelLoader (self) :  
        if (self._ML_KerasModelLoaderInterface is None) :
            self._ML_KerasModelLoaderInterface = ML_KerasModelLoaderInterface ()
        return self._ML_KerasModelLoaderInterface
            
    def reloadWindowROIDefs (self,IgnoreWindow = None) :
        for window in self.getActiveContourWindowList () :
            if (window is not IgnoreWindow) :
                window.reloadROIDefs ()
                
    def saveROIDefToProject (self, ROIDefs, IgnoreWindow = None) :
        ROIDefs.saveROIDefsToProjectFile ()        
        self.reloadWindowROIDefs (IgnoreWindow=IgnoreWindow)
                     
                     
    def getROISaveThread (self) :
        return self._ROISaveThread
    
    def setProjectDescriptionContext (self, val) :
        if (self._ProjectDescriptionContext != val) :
            self._ProjectDescriptionContext = val
            try:                            
                self.saveProject ()
            except:      
                print ("A error occured trying to save project settings")  
            for window in self.getActiveContourWindowList () :
                window.reloadROIDefs () # reload ROI defs to reload windows
            return True
        return False
                
    def getPenSettings (self) :
        if (self._penSettings is  None) :
            self._penSettings = PenSettings ()        
        return self._penSettings.copy ()
    
    def getExpandedTreeNodes (self) : 
        return self._expandedTreeNodes
    
    def getOpenAtStartFileList (self) :
        return self._openAtStartFileList
    
    def setExpandedTreeNodes (self, settings) : 
        self._expandedTreeNodes = settings
        self._expandedTreeNodesChanged = True
        
    def setPenSettings (self, settings, aw) :        
        self._penSettings = settings
        for window in self.getActiveContourWindowList () :
            if window != aw :
                try :                        
                    window.setPenSettingsFromProject (settings)
                except :
                    pass
        self._projectPenSettingsChanged = True
        
    def getInternalClipboard (self) :
        return self._internalROIClipBoard
        
    def getProjectTempStorageDir (self) :
        return self._TempStorageDir
    
    def setProjectTempStorageDir (self, val) :        
        if (os.path.isdir (val)) :
            if ( val != self._TempStorageDir ) :
                self._TempStorageDir = val
                if (self._NIfTI_DatasetInterface is not None) :
                    try :
                        self._NIfTI_DatasetInterface.setTempDir (self._TempStorageDir)
                    except :
                        print ("Could not set nifti dataset temp dir")
                if (self._ROI_DatasetInterface is not None) :
                    try :
                        self._ROI_DatasetInterface.setTempDir (self._TempStorageDir)
                    except :
                        print ("Could not set roi dataset temp dir")
                try:                            
                    self.saveProject ()
                except:      
                   print ("A error occured trying to save project settings")     
    
    def getDescriptionContext (self) :
        try :
            context = self._ProjectDescriptionContext.strip ()
            if (context is None or context == "") :
                return self.getProjectName ()
            return self._ProjectDescriptionContext
        except:
            return self.getProjectName ()
    
    def getSelectedDataset (self) :
        return self._selectedDataset
    
    def isROIDataStoredInTatic (self) : 
       if self.getROIDatasetInterface () is not None :
           isPesscara = self.getROIDatasetInterface ().isPesscaraInterface ()
           #print ("ROI Data is stored in Pesscara: ", isPesscara)
           return isPesscara
       return False
         
    def addNewTagsToProject (self, tagManager) :
           self.saveProject (DatasetTags = tagManager)

        
    def getProjectTagManger (self):
        return self._DatasetTagManager
    
    def getExportVolumeMaskSettings (self) :
        return self._exportMaskDlgSettings
        
    def setExportVolumeMaskSettings (self, val) :
        self._exportMaskDlgSettings = val
    
    def setUserName  (self, val) :
        if self.getProjectFileInterface().setUserName (val) :
            try :
                self.saveProject ()
            except:
                print ("Could not save project settings.")

    def isUserNameUnknown (self) :
        username = self.getProjectFileInterface().getUserName ()
        return (username is None or username == "" or username.upper() == "UNKNOWN")     
            

    def getMPProcessTimingFilePath (self) :
        try:            
            homepath = self.getHomeFilePath () 
            if (not os.path.isdir (homepath) and not os.path.exists (homepath)):
                os.mkdir (homepath)
            timingPath = os.path.join (homepath,"MPTiming.dat")
            return timingPath
        except:
            return None 
        
    def getFileSystemUID (self) :
        return self.getProjectFileInterface().getFileSystemUID ()
       
    
    def getUserUID (self) :
        return self.getProjectFileInterface ().getUserUID ()
        
            
    def getUserName (self, ShowWindow = True) :
        if (ShowWindow and self.isUserNameUnknown () and self.getNiftiDatasetDlg () != None) :
            self.getNiftiDatasetDlg ().showSetUserName ()
        if (self.isUserNameUnknown ()) :            
            return "Unknown"
        return self.getProjectFileInterface().getUserName ()
    
    def getPythonProjectBasePath (self):
        return self._BasePath
    
    def getMLHomeFilePath (self) :
        try :
            homePath = self.getHomeFilePath ()
            ML_ModelPath = os.path.join (homePath, "ML_Models")
            if (not os.path.isdir (ML_ModelPath) and not os.path.exists (ML_ModelPath)):
                os.mkdir (ML_ModelPath)
            if os.path.isdir (ML_ModelPath) :
                return ML_ModelPath
        except:
            return None
        
    def _setUserRootHomePath (self, val):
        self._userRootHomePath = val
                
    def getHomeFilePath (self):
        try:
            home = self._userRootHomePath
            homepath = os.path.join (home,".rilcontour")
            if (not os.path.isdir (homepath) and not os.path.exists (homepath)):
                os.mkdir (homepath)
            if (os.path.isdir (homepath)) :
                return homepath
            else:
                return self.getPythonProjectBasePath ()
        except:
            return self.getPythonProjectBasePath ()        
    
    def getScanPhaseSelectionDlg (self) :
        return self._scanPhaseSelectionDlg
    
    def setScanPhaseSelectionDlg (self, dlg) :
        self._scanPhaseSelectionDlg = dlg
    
    def getScanPhaseList (self):        
        return copy.copy (self._projectScanPhaseList)
        
    def getProjectName (self) :
        return self.getProjectFileInterface().getProjectName ()
                    
    def setLastProjectOpened (self, path):
        try :        
            lastprojectPath = os.path.join (self.getHomeFilePath (), "LastProject.txt")
            projectfile = open (lastprojectPath, "wt")
            projectfile.write (path)
            projectfile.close ()
        except:
            print ("Error Could not save last project opened.")
            
    def getLastProjectOpened (self):
        homePath = ""
        try :
            homePath = self.getHomeFilePath ()                        
            lastprojectPath = os.path.join (homePath, "LastProject.txt")
            if (not os.path.isfile (lastprojectPath)) :
                lastprojectPath = os.path.join (self.getPythonProjectBasePath (), "LastProject.txt")
            print ("Opening saved last project settings from: " + lastprojectPath)
            projectfile = open (lastprojectPath, "rt")
            path = projectfile.readline  ()
            projectfile.close ()
            path.strip ()
            if (len (path) > 0 and os.path.isfile (path)) :
                print ("Last opened project: " + path)
                return path
            else:
                print ("Could not find file '%s'. Opening default project file 'RCC_VisSettings.prj'." % path)
                if (os.path.isdir (homePath)) :
                    return os.path.join (homePath, "RCC_VisSettings.prj")
                return  "RCC_VisSettings.prj"            
        except :
            
            if (os.path.isdir (homePath)) :
               return os.path.join (homePath, "RCC_VisSettings.prj")
            return "RCC_VisSettings.prj"
        
    
        
    def isRunningFromCommandlineSepcifiedDataset (self):
        self._isRunningFromCommandlineSepcifiedDataset
        
    def initLastLoadedProject (self, RILContourCommandLine = None, ShowDialog=True)  :
        try :
            if RILContourCommandLine is not None and "projectpath" in RILContourCommandLine: 
                self._isRunningFromCommandlineSepcifiedDataset = True    
                return self.loadSettingsFromFile (RILContourCommandLine["projectpath"], ShowDialog = ShowDialog)                     
            else:                
                self._isRunningFromCommandlineSepcifiedDataset = False
                projectPath = FileUtil.correctOSPath (self.getLastProjectOpened ())
                return self.loadSettingsFromFile (projectPath, ShowDialog)            
        except:
            return False
            
    def initDefaultSettings (self, ShowDialog=True) :
            projectFI = self.getProjectFileInterface()
            projectFI.clearProjectUID()
            projectFI.clearUserName()
            projectFI.clearProjectDatasetCache ()       
            projectFI.clearFilePermissions()
            self._DatasetTagManager = DatasetTagManager ()        
            projectFI.setUserName (getpass.getuser ())     
            projectFI.setMultiUserFileSharingManager (None)    
            basePath = self.getHomeFilePath ()
            try:                 
                roiDatasetPath = os.path.join (basePath, "ROIDatasets")
                if (not os.path.isdir (roiDatasetPath)) :                                          
                    FileUtil.createPath (roiDatasetPath)
            except :
                roiDatasetPath = "ROIDatasets"
                if (not os.path.isdir (roiDatasetPath)) :                                          
                    FileUtil.createPath (roiDatasetPath)
            try:                 
                datasetPath = os.path.join (basePath, "Datasets")
                if (not os.path.isdir (datasetPath)) :                                          
                    FileUtil.createPath (datasetPath)
            except :
                datasetPath = "Datasets"
                if (not os.path.isdir (datasetPath)) :                                          
                    FileUtil.createPath (datasetPath)                              
            
            dummyprojectfile = os.path.join (basePath, "RCC_VisSettings.prj")
            projectFI.setProjectPath (dummyprojectfile)
            projectFI.setProjectName ("Default Project")
        
            ROI_DatasetInterface = RCC_ROIDataset_FileSystemInterface (path = roiDatasetPath, ProjectName = "DefaultDataset", ProjectDataset = self, RILContourCommandLine = self.getRILContourCommandLine () )    
            ROI_DatasetInterface.updateRelativePathIfNecessary (basePath) 
            ROI_DatasetInterface.loadDataset (App=self.getApp ())                                     
            projectFI.initalizeEmptyProjectDatasetCache ()                    
            projectFI.loadProjectDatasetCache ()
            NIfTI_DatasetInterface = RCC_NiftiFileSystemDataInterface ( ROI_DatasetInterface,  RootSearchPath = [datasetPath], parent=self.getNiftiDatasetDlg (), ProjectDataset = self, ProjectDirPath=basePath)
                
            try :
                self.initSettingsFromClasses (dummyprojectfile,  "Default Project", ROI_DatasetInterface, NIfTI_DatasetInterface, ShowDialog = ShowDialog, UserName = projectFI.getUserName())                  
                self.setLastProjectOpened (dummyprojectfile)
            except :                
                dummyprojectfile = "RCC_VisSettings.prj"
                self.initSettingsFromClasses (dummyprojectfile,  "Default Project", ROI_DatasetInterface, NIfTI_DatasetInterface, ShowDialog = ShowDialog, UserName = projectFI.getUserName())                   
                self.setLastProjectOpened (dummyprojectfile)
            self._initProjectScanPhaseList (None)
            self.setProjectTempStorageDir (tempfile.gettempdir())
            self.getDocumentUIDFileLogInterface ()
            


            
    def areNIfTIImagingStoredOnFileSystem (self) :
        try :
            return self._NIfTI_DatasetInterface.getInterfaceName () == "RCC_NiftiFileSystemDataInterface"
        except:
            return False
        
    def canChangeUserName (self) :
        if self._NIfTI_DatasetInterface is None :
            return True
        return self._NIfTI_DatasetInterface.getInterfaceName () != "RCC_NiftiFileSystemDataInterface" 
            
    def initSettingsFromClasses (self, projectPath, projectName, ROI_DatasetInterface, NIfTI_DatasetInterface, ShowDialog=True, UserName = None) :
        projectFI = self.getProjectFileInterface()
        projectFI.clearProjectUID()
        projectFI.clearProjectDatasetCache ()
        projectFI.clearFilePermissions()
        if NIfTI_DatasetInterface.getInterfaceName () == "RCC_NiftiFileSystemDataInterface" : 
            UserName = getpass.getuser ()
        
        projectFI.setFilePermissions(FileUtil.getFilePermissions (projectPath))
        projectFI.setProjectPath (projectPath)
        projectFI.setProjectName (projectName)
        projectFI.setUserName (UserName)
        
        self._ROI_DatasetInterface = ROI_DatasetInterface                    
        self._NIfTI_DatasetInterface = NIfTI_DatasetInterface                
        self.saveProject (projectPath, ForceSave = True)
        try :
            roiDef = ROIDefinitions (projectFI)
            roiDef.setProjectFilePath (projectPath)
            roiDef.saveROIDefsToProjectFile ()
        except:
            print ("Error occured initalizing project ROIDefs")        
        self._initProjectScanPhaseList (None)
        
        if (ShowDialog) :
            self._NiftiDatasetDlg  = RCC_DatasetDlg (self, parent = None)                     
        else:
            self._NiftiDatasetDlg = None
                
        if (ShowDialog) :
            #self._NiftiDatasetDlg  = RCC_DatasetDlg (self, parent = None)                     
            contouringWindow = self.createContouringWindow ()                
            self._NiftiDatasetDlg.setGeometry (contouringWindow.x (), contouringWindow.y (), self._NiftiDatasetDlg.width (),contouringWindow.height ())        
            contouringWindow.setGeometry (contouringWindow.x ()+self._NiftiDatasetDlg.width (), contouringWindow.y (), contouringWindow.width (),contouringWindow.height ())
        self.setProjectTempStorageDir (tempfile.gettempdir())
        self.getDocumentUIDFileLogInterface ()
            
    def createProjectFromWizard (self, wizard, ShowDialog=True) :                    
        
        project = ProjectDatasetDefinition (self.getApp () , self._appeventfilter, self._globalProjectList, self.getPythonProjectBasePath (), ROISaveThread = self.getROISaveThread (), watchDogDirectoryObserver = self.getWatchDogDirectoryObserver (), RILContourCommandLine = self.getRILContourCommandLine (), MultiUserFileSharingManager = None)        
        projectFI = project.getProjectFileInterface()
        projectFI.setProjectPath (wizard._projectPath)
        projectFI.setProjectName (wizard._projectName)        
        if (wizard._filesystemROISave != "PESSCARA") :            
            ROI_DatasetInterface = RCC_ROIDataset_FileSystemInterface (path = wizard._filesystemROISave, ProjectName = wizard._projectName, ProjectDataset = project, RILContourCommandLine = self.getRILContourCommandLine ())   
            projectDirPath, _ = os.path.split (wizard._projectPath)
            ROI_DatasetInterface.updateRelativePathIfNecessary (projectDirPath)     
            ROI_DatasetInterface.loadDataset (App=self.getApp ())
        else:
            ROI_DatasetInterface = RCC_ROIDataset_PASSCARA (wizard._pesscaraNiftiInterface, wizard._projectName, ProjectDataset = project)
        
        projectFI.initalizeEmptyProjectDatasetCache ()                    
        projectFI.loadProjectDatasetCache ()      
        
        if (wizard._fileSystemNIfTISouceDir != "") :
            projectDirPath, _ = os.path.split (wizard._projectPath)
            NIfTI_DatasetInterface = RCC_NiftiFileSystemDataInterface (ROI_DatasetInterface, RootSearchPath = [wizard._fileSystemNIfTISouceDir], ProjectDataset = project, ProjectDirPath = projectDirPath)  #parent= self.getNiftiDatasetDlg ())
        else :            
            NIfTI_DatasetInterface = RCC_NiftiPesscaraDataInterface (ROI_DatasetInterface, NewPesscaraSession =(wizard._pesscaraNiftiInterface, wizard._pesscaraNiftiSubjectList), ProjectDataset = self)# parent=self.getNiftiDatasetDlg ())                
            
        
        try :            
            project.initSettingsFromClasses (wizard._projectPath, wizard._projectName, ROI_DatasetInterface, NIfTI_DatasetInterface, ShowDialog)
            
            project.getNiftiDatasetDlg().show ()             
            try :
                self.setLastProjectOpened (wizard._projectPath)
            except  :
                print ("Could not set last opened project.")            
            return project
        except:
            print ("Error Could Not Save Project")
            project.closeProject ()
            return None
    
    def findNIfTIinExistingOpenProject (self, path_src, IngnoreProject = None) :
        ignoreProjectSet = [None]
        if IngnoreProject is not None :
            ignoreProjectSet.append (IngnoreProject) 
        for project in self._globalProjectList  :
            if project in ignoreProjectSet :
               continue  
            niftiDatasetInterface = project.getNIfTIDatasetInterface ()
            if niftiDatasetInterface is not None :
                niftiDataset = niftiDatasetInterface.findFileInDataset (path_src)
                if (niftiDataset is not None) :
                    return project, niftiDataset
        return None, None
    
    def getActiveWindowShowingNifti (self, niftiDataset, AcceptEmpty = False) :
        for window in self.getActiveContourWindowList () :
            if (window.getSelectedNIfTIDataset () is not None and window.getSelectedNIfTIDataset () == niftiDataset):
                return window
            elif (AcceptEmpty and window.getSelectedNIfTIDataset () is None and window.getLoadedNIfTIFile () is None) :
                return window
        return None
                      
    def activateExistingOpenProject (self, path) :
        for project in self._globalProjectList  :
            if project.getPath () == path :
                dlg = project.getNiftiDatasetDlg()
                if dlg is not None :
                    dlg.activateWindow ()
                    dlg.raise_()                    
                aw = project.getActiveWindow ()
                if aw is not None :
                    project.setActiveWindow (aw)
                return True
        return False
        
    
    def _openPreviousOpenedFilesAtStartup (self) :
        if (self.getNIfTIDatasetInterface ().getInterfaceName () == "RCC_NiftiFileSystemDataInterface") :
            fileList = self.getOpenAtStartFileList ()
            if len (fileList) > 0 :
                niftiDatasetInterface = self.getNIfTIDatasetInterface ()
                if niftiDatasetInterface is not None :
                    for path_src in fileList :
                        try :
                            niftiDataset = niftiDatasetInterface.findFileInDataset (path_src)
                            if (niftiDataset is not None) :
                                aw = self.getActiveWindow ()
                                if (aw is not None and aw.getLoadedNIfTIFile () is None):  # No loaded nifti
                                    aw.DatasetTreeSelectionChanged (niftiDataset, NIfTIFilePath = None)
                                    self.setActiveWindow (aw)   
                                else:
                                    self.createContouringWindow ()
                                    aw = self.getActiveWindow ()
                                    if (aw is not None) :
                                        aw.DatasetTreeSelectionChanged (niftiDataset, NIfTIFilePath = None)
                                        self.setActiveWindow (aw)     
                        except:
                            pass
                        
    def createProjectFromFile (self, path, ShowDialog=True) :            
        newProj = ProjectDatasetDefinition (self.getApp (), self.getAppEventFilter (), self._globalProjectList, self.getPythonProjectBasePath (), ROISaveThread = self.getROISaveThread (), watchDogDirectoryObserver = self.getWatchDogDirectoryObserver (), RILContourCommandLine = self.getRILContourCommandLine(), MultiUserFileSharingManager = None)
        if newProj.loadSettingsFromFile (path, ShowDialog) :
            newProj.getNiftiDatasetDlg().show ()        
            return newProj
        return None
       
        
    def hasMultiUserProjectDef (self, projectUID, projectPath):
        try :
            if projectUID is not None :
                _, fname = os.path.split (projectPath)
                testPath = os.path.join (self.getHomeFilePath (), "MultiUserProjects")
                if os.path.exists (testPath) :
                    testPath = os.path.join (testPath,projectUID)
                    if os.path.exists (testPath) :
                       multiuserprojectPath = os.path.join (testPath, fname)
                       if os.path.exists (multiuserprojectPath) :
                           return multiuserprojectPath
            return None
        except:
            return None
    
    def loadMultiUserProjectSettings (self, path) :    
        fobj = FileInterface ()              
        fobj.readDataFile (path)     
        header = fobj.getFileHeader ()                 
        if (header.hasParameter ("TempStorageDir")):
            tempStoragedir = header.getParameter ("TempStorageDir")
            if not os.path.exists (tempStoragedir) or not os.path.isdir (tempStoragedir) :
                tempStoragedir = tempfile.gettempdir()
        else:
            tempStoragedir = tempfile.gettempdir()
        if (header.hasParameter ("ExpandedTreeNodes")):
            self._expandedTreeNodes = header.getParameter ("ExpandedTreeNodes")                
        else:
            self._expandedTreeNodes = {}
        self._expandedTreeNodesChanged = False
        
        if (header.hasParameter ("OpenFilesAtStart")) :
            self._openAtStartFileList = header.getParameter ("OpenFilesAtStart")                
        else:
            self._openAtStartFileList = []
        self._openFilesAtStartChanged = False

        if (header.hasFileObject ("RCC_PenSettings")) :
            self._penSettings = PenSettings (fobj = header.getFileObject ("RCC_PenSettings"))        
            self._projectPenSettingsChanged = False
        return header, tempStoragedir
         
    def loadSettingsFromFile (self, path, ShowDialog=True) :
        projectFI = self.getProjectFileInterface()
        projectFI.clearProjectUID()
        projectFI.clearUserName()
        projectFI.clearProjectPath()
        projectFI.clearFilePermissions()
        if not os.path.exists (path) :
            projectFI.clearProjectUID()
            self.initDefaultSettings (ShowDialog = ShowDialog)
            return True
        if not self.isRunningInDemoMode () and not self.lockProjectFile (path) :
            projectFI.clearProjectUID()
            self.initDefaultSettings (ShowDialog = ShowDialog)
            return  True
        LoadingDeafultProject =  (path == os.path.join (self.getHomeFilePath (), "RCC_VisSettings.prj"))
        if LoadingDeafultProject :
            self._setProjectReadOnly (True)                
        initDefault = True
        try:
            self._ProjectFileInterface.aquireFileLock ()   
            projectFI.setFilePermissions(FileUtil.getFilePermissions (path))
            projectFI.setProjectPath (path)
            try : 
                fobj = FileInterface ()   
                fobj.readDataFile (path)                       
                header = fobj.getFileHeader ()     
                
                if not self.isInMultiUserCommandLineMode () and header.hasParameter ("MultiUserSharingYAML") and not LoadingDeafultProject :
                    try:
                       ProjectBasedMultiuserSharingYAMLDef = header.getParameter ("MultiUserSharingYAML")
                       if ProjectBasedMultiuserSharingYAMLDef is not None and len (ProjectBasedMultiuserSharingYAMLDef) > 0 :
                           mgr = projectFI.getMultiUserFileSharingManager ()
                           if mgr is None :
                               mgr = MultiUserManager (None)
                           mgr.initFromText (ProjectBasedMultiuserSharingYAMLDef, path)
                           if mgr.isInitalized () :
                               if self.isUserNameCommandLineMode () and not self.isHomePathCommandLineMode () :
                                   MessageBoxUtil.showMessage ("Multiuser init error", "A error: if a non-system username is specified then an non-system home path must also be specified.")
                                   return True
                               else:
                                   projectFI.setMultiUserFileSharingManager (mgr)
                                   YamlDefWarrnings = mgr.validateYaml ()
                                   if len (YamlDefWarrnings) > 0 :
                                        print ("")
                                        print ("Warrning possible errors in the multi-user YAML definition embedded in the loaded project.")
                                        print ("")
                                        for line in YamlDefWarrnings :
                                            print (line)
                                        MessageBoxUtil.showMessage ("Multiuser File-sharing", "Warrning possible errors in the multi-user YAML definition embedded in the loaded project. Click details for more info","\n".join (YamlDefWarrnings)) 
                                            
                                   if not self.isInSuperUserCommandLineMode () and not self.isProjectReadOnly () :
                                       print ("Warrning code should not be hit project should allready be set to read only.")
                                       self._unlockProject ()
                                       self._project_lock_file = None
                                       self._project_lock_signature = None
                                       self._setProjectReadOnly (True)                
                    except:
                        pass
              
                if self.isInMultiUserCommandLineMode ()  :
                    if header.hasFileObject ("RCC_ROIDataset_PESSCARAInterface") :
                        MessageBoxUtil.showMessage ("Initalization Error", "Tactic based projects are not compatiable with multiuser project settings. Multiuser project settings are only compatiable with file-system based projects.")
                        return True                    
                elif projectFI.isInMultiUserMode () and header.hasFileObject ("RCC_ROIDataset_PESSCARAInterface") : 
                    projectFI.setMultiUserFileSharingManager (None)    
                if header.hasFileObject ("RCC_ROIDataset_PESSCARAInterface") :
                    if header.hasParameter ("UserName") :
                        projectFI.setUserName (header.getParameter ("UserName"))                                                                  
                else:
                    projectFI.setUserName (getpass.getuser ())                      
                try:
                    if LoadingDeafultProject :
                        projectFI.setUserName (getpass.getuser ())
                except:
                    pass
                
                manager = projectFI.getMultiUserFileSharingManager ()
                if manager is not None and manager.isInitalized () and not manager.isUserDefined (projectFI.getUserName ()) :
                    if not self.isInSuperUserCommandLineMode ():
                        username = projectFI.getUserName ()
                        MessageBoxUtil.showMessage ("User not recognized", "The user '%s' is not included in the projects multiuser sharing yaml. To open this project add %s to the multiuser sharing yaml definition." % (username, username))
                        return True
                    
                if header.hasParameter ("ProjectUID"):
                    projectFI.setProjectUID (header.getParameter ("ProjectUID") )
                    
                if (header.hasParameter ("ProjectName")):
                    projectFI.setProjectName (header.getParameter ("ProjectName"))
                else:
                    return True
                
                if (header.hasParameter ("DescriptionContext")):
                    self._ProjectDescriptionContext = header.getParameter ("DescriptionContext")                
                else:
                    self._ProjectDescriptionContext = projectFI.getProjectName ()
           
                if header.hasParameter ("ImportImagingDlgState") :
                    self.setImportImagingDlgState (header.getParameter ("ImportImagingDlgState"), SaveChangesToProject = False)
                   
                     
                loadedMultiUserPojectSettings = False                
                if header.hasFileObject ("RCC_ROIDataset_FileSystemInterface")  :                                            
                    projectUID = projectFI.getProjectUID ()
                    if  projectUID is not None :  
                        multiUserProjectPath = self.hasMultiUserProjectDef (projectUID, path)
                        if (multiUserProjectPath is not None and os.path.exists (multiUserProjectPath)) :
                            try :                            
                                fsObjUIHeader, tempStoragedir = self.loadMultiUserProjectSettings (multiUserProjectPath)   
                                projectFI.setMutliUserProjectPath (multiUserProjectPath)
                                loadedMultiUserPojectSettings = True
                            except:
                                loadedMultiUserPojectSettings = False
                            
                if not loadedMultiUserPojectSettings :
                    if (header.hasParameter ("TempStorageDir")):
                        tempStoragedir = header.getParameter ("TempStorageDir")
                        if not os.path.exists (tempStoragedir) or not os.path.isdir (tempStoragedir) :
                            tempStoragedir = tempfile.gettempdir()
                    else:
                        tempStoragedir = tempfile.gettempdir()
                    
                    if (header.hasParameter ("ExpandedTreeNodes")):
                        self._expandedTreeNodes = header.getParameter ("ExpandedTreeNodes")                
                    else:
                        self._expandedTreeNodes = {}
                    self._expandedTreeNodesChanged = False
                    
                    if (header.hasParameter ("OpenFilesAtStart")) :
                        self._openAtStartFileList = header.getParameter ("OpenFilesAtStart")                
                    else:
                        self._openAtStartFileList = []
                    self._openFilesAtStartChanged = False
            
                    if (header.hasFileObject ("RCC_PenSettings")) :
                        self._penSettings = PenSettings (fobj = header.getFileObject ("RCC_PenSettings"))        
                        self._projectPenSettingsChanged = False
                    fsObjUIHeader = header
        
                progdialog = RCC_SplashScreenDlg (self, self.getNiftiDatasetDlg ())                             
                progdialog.setValue (0)
                progdialog.setProgressRangeIndeterminate ()
                progdialog.setMessage ("Initializing")
                self.getApp().processEvents ()
                time.sleep (1)
                if (header.hasFileObject ("RCC_ROIDataset_FileSystemInterface")):
                    roiFileSystemInterface = header.getFileObject ("RCC_ROIDataset_FileSystemInterface")                
                    self._ROI_DatasetInterface = RCC_ROIDataset_FileSystemInterface (FObject = roiFileSystemInterface, ProjectDataset = self, RILContourCommandLine = self.getRILContourCommandLine ())                          
                    projectDirPath, _ = os.path.split (path)
                    self._ROI_DatasetInterface.updateRelativePathIfNecessary (projectDirPath)                    
                    RetryFindROIPath=True
                    
                    if not FileUtil.testCanReadWriteFromDirectory (self._ROI_DatasetInterface.getROIDatasetRootPath ()) :
                        MessageBoxUtil.showMessage ("File Permission Error", "Unable to create directories and write to the ROI data directory. You likely do not have file privlages to write to this location. RIL-Contour requires users level write privlages to the ROI data directory.")
                        
                    while (not os.path.exists (self._ROI_DatasetInterface.getROIDatasetRootPath ()) and RetryFindROIPath) : 
                        dlg= QFileDialog()
                        dlg.setWindowTitle('ROI Directory was not found; Select a select the directory to load ROI From' )
                        dlg.setViewMode( QFileDialog.Detail )    
                        dlg.setFileMode (QFileDialog.Directory)
                        dlg.setNameFilters( [dlg.tr('All Files (*)')] )
                        dlg.setDefaultSuffix( '' )
                        dlg.setAcceptMode (QFileDialog.AcceptOpen)
                        result = dlg.exec_()
                        if result == QDialog.Rejected :
                            RetryFindROIPath = False
                        elif result == QDialog.Accepted and (len (dlg.selectedFiles())== 1):
                            path = dlg.selectedFiles()[0]                                                    
                            if os.path.exists (path) :
                                if not FileUtil.testCanReadWriteFromDirectory (path) :
                                    MessageBoxUtil.showMessage ("File Permission Error", "Unable to create directories and write to the ROI data directory. You likely do not have file privlages to write to this location. RIL-Contour requires users level write privlages to the ROI data directory.")
                                else:
                                    self._ROI_DatasetInterface.setROIDatasetRootPath (path)
                                    self._ROI_DatasetInterface.updateRelativePathIfNecessary (projectDirPath)                    
                    self._ROI_DatasetInterface.loadDataset (App=self.getApp (), progressDialog = progdialog)                    
                    
                elif (header.hasFileObject ("RCC_ROIDataset_PESSCARAInterface")):                   
                    roiPesscaraInterface = header.getFileObject ("RCC_ROIDataset_PESSCARAInterface")                
                    self._ROI_DatasetInterface = RCC_ROIDataset_PASSCARA (ProjectName=projectFI.getProjectName(),   FObject = roiPesscaraInterface,  ProjectDataset = self)
                else:
                   return True
               
                try :
                    if self.isInNoCacheCommandLineMode ()  :
                        projectFI.initalizeEmptyProjectDatasetCache ()                       
                    projectFI.loadProjectDatasetCache ()
                    if (header.hasFileObject ("RCC_NiftiFileSystemDataInterface")):                   
                        projectDirPath, _ = os.path.split (path)                    
                        self._NIfTI_DatasetInterface = RCC_NiftiFileSystemDataInterface ( self._ROI_DatasetInterface, FObject = header.getFileObject ("RCC_NiftiFileSystemDataInterface"), parent=self.getNiftiDatasetDlg (), ProjectDataset = self, ProjectDirPath = projectDirPath, RILContourCommandLine = self.getRILContourCommandLine(), UIFObjectHeader = fsObjUIHeader.getFileObject ("RCC_NiftiFileSystemDataInterface"), SplashScreen = progdialog)
                    elif (header.hasFileObject ("RCC_NiftiPesscaraDataInterface")):                                    
                        self._NIfTI_DatasetInterface = RCC_NiftiPesscaraDataInterface ( self._ROI_DatasetInterface, FObject = header.getFileObject ("RCC_NiftiPesscaraDataInterface"), parent=self.getNiftiDatasetDlg (), ProjectDataset = self, UIFObjectHeader = fsObjUIHeader.getFileObject ("RCC_NiftiPesscaraDataInterface"), SplashScreen = progdialog)     
                    else:
                        return True
                except:
                    try :
                        self._NiftiDatasetDlg.close ()
                    except:
                        pass
                    self._NiftiDatasetDlg = None
                    raise
                del progdialog
                
                if (header.hasFileObject ("RCC_ScanPhaseList")):                                    
                    self._initProjectScanPhaseList (header.getFileObject ("RCC_ScanPhaseList"))
                else:
                    self._initProjectScanPhaseList (None)

                self._DatasetTagManager.loadTagDatasetFromFile (None, FileHeader = header, AddInternalTags = False, AddOwnershipAndLockingInternalTags = False)
                    
                self.saveProject ( projectFI.getProjectPath(), FileLocked = True)                            
            
                if (ShowDialog) :    
                    if (ShowDialog) :
                        self._NiftiDatasetDlg  = RCC_DatasetDlg (self, parent = None)             
                    contouringWindow = self.createContouringWindow ()                
                    self._NiftiDatasetDlg.setGeometry (contouringWindow.x (), contouringWindow.y (), self._NiftiDatasetDlg.width (),contouringWindow.height ())        
                    contouringWindow.setGeometry (contouringWindow.x ()+self._NiftiDatasetDlg.width (), contouringWindow.y (), contouringWindow.width (),contouringWindow.height ())                
                    if not self.isInSafeCommandLineMode () :
                        self._openPreviousOpenedFilesAtStartup ()
                self.setProjectTempStorageDir (tempStoragedir)
                self.setLastProjectOpened (path)   
                initDefault = False
                return True
            except :                    
                projectFI.clearProjectUID()
                self._unlockProject ()
                return True
        finally:
            self._ProjectFileInterface.releaseFileLock ()
            if (initDefault) :
                self.initDefaultSettings (ShowDialog = ShowDialog)
            try :
                if self.isRunningInDemoMode () :
                    print ("Running in demo mode")
                    roiInterface = self.getDeepGrowROIModelInterface ()
                    if roiInterface is not None :
                        roiInterface.clearAllDGModels ()                        
                        print ("Done cleaning up deepgrow.")
            except:
                pass
   
                                                                                                            
    def _initProjectScanPhaseList (self, fileobject = None):        
        if (fileobject != None) :
            try :
                scanPhaseList = []                
                count = int (fileobject.getParameter ("ScanPhaseLstCount"))
                for phaseCount in range (count) :
                    phase = fileobject.getParameter ("Phase_" + str (phaseCount + 1))
                    scanPhaseList.append (phase)                    
                self._projectScanPhaseList = scanPhaseList
                return
            except :
                print ("A error occured loading the scan phase")                
        scanPhaseList = []
        scanPhaseList.append ("Unknown")
        for index in range (10) :
            scanPhaseList.append ("Scan Phase " + str (index + 1))
        self._projectScanPhaseList = scanPhaseList
            
    def _getProjectScanPhaseListFileObject (self) :
        fobj = FileObject ("RCC_ScanPhaseList")
        fobj.setParameter ("ScanPhaseLstCount", len (self._projectScanPhaseList))
        for count, phase in enumerate (self._projectScanPhaseList) :
            fobj.setParameter ("Phase_" + str(count + 1), phase)
        return fobj        
    
    def setSelectionCoordinate (self, caller_window, Coordinate) :
        for window in self._ContouringWindowList :
            try :
                if (window != caller_window) :
                    window.setGobalCoordinate (Coordinate)
            except : 
                print ("Error occured setting window selection coordinate")
        
    def setProjectTagManger (self, tagManager):
        self._DatasetTagManager = tagManager
        if (tagManager != None) :
            try:                            
                self.saveProject ()
            except:      
               print ("A error occured trying to save project settings")   
        for window in self.getActiveContourWindowList () :
            try :
                window.reloadROIDefs ()             
            except:
                print ("A error occured trying to update the dataset UI")       
                     
    def setScanPhaseList (self, lst):
        scanPhaseChanged = False
        if (len (lst) != len (self._projectScanPhaseList)) : 
            scanPhaseChanged = True
        else:
            for index in range (len (lst)) :
                if lst[index] != self._projectScanPhaseList[index] :
                    scanPhaseChanged = True
                    break
        if (scanPhaseChanged) :
            self._projectScanPhaseList = copy.copy (lst)
            try :
                self.saveProject ()
            except: 
                print ("could not save project")
            for window in self._ContouringWindowList :
                try :                
                    window.scanPhaseListChanged (self._projectScanPhaseList )
                except :
                    print ("Error updating window scan phase list")
            if self.getScanPhaseSelectionDlg () != None :
                try :                
                    self.getScanPhaseSelectionDlg ().scanPhaseListChanged (self._projectScanPhaseList )
                except :
                    print ("Error updating scan phase dialog scan phase list")

    def updateProjectDatasetTreeNodeFormat (self, SelectedNiftiData):
        if (SelectedNiftiData is None) :
            return
        if self.getScanPhaseSelectionDlg () is not None :
            try :                
                self.getScanPhaseSelectionDlg ().setProjectDatasetTreeNodeUpdated (SelectedNiftiData)
            except :
                print ("Error setting selected scan phase in scan phase selection dlg") 
                
    def setSelectedScanPhase (self, SelectedNiftiData, ScanPhase):    
        if (SelectedNiftiData is None) :
            return
        
        SelectedNiftiData.setScanPhaseTxt (ScanPhase)
        aw = self.getActiveWindow ()
        for window in self._ContouringWindowList :
            if (aw != window) :
                try :                
                    window.setSelectedScanPhase ( SelectedNiftiData, ScanPhase )
                except :
                    print ("Error setting selected scan phase in contour window")
        if self.getScanPhaseSelectionDlg () != None :
            if (aw != self.getScanPhaseSelectionDlg ()) :
                try :                
                    self.getScanPhaseSelectionDlg ().setSelectedScanPhase (SelectedNiftiData, ScanPhase)
                except :
                    print ("Error setting selected scan phase in scan phase selection dlg")
    
    def saveMultiUserProjectSettings (self, projectUID, projectPath) :
        _, fname = os.path.split (projectPath)
        testPath = os.path.join (self.getHomeFilePath (), "MultiUserProjects")
        if not os.path.exists (testPath) :
            os.mkdir (testPath)
        if not os.path.exists (testPath) :
            return
        testPath = os.path.join (testPath,projectUID)
        if not os.path.exists (testPath) :
            os.mkdir (testPath)
        if not os.path.exists (testPath) :
            return
        multiuserprojectPath = os.path.join (testPath, fname)
        
        fobj = FileInterface ()      
        if os.path.exists (multiuserprojectPath) :
            try :
                fobj.readDataFile (multiuserprojectPath)         # Project UID and Header writen into project file elsewhere. copy accross
            except :
                fobj = FileInterface ()
        header = fobj.getFileHeader ()  
        projectFI = self.getProjectFileInterface()
        header.setParameter ("ProjectName", projectFI.getProjectName())
        header.setParameter ("ProjectUID", projectFI.getProjectUID ()) 
        header.setParameter ("UserName", projectFI.getUserName (Actual=True))
        header.setParameter ("TempStorageDir",self.getProjectTempStorageDir () )                
        header.setParameter ("ExpandedTreeNodes", self._expandedTreeNodes)
        header.setParameter ("OpenFilesAtStart", self._getOpenFileList ())
        self._expandedTreeNodesChanged = False
        self._openFilesAtStartChanged = False
                    
        while (header.hasFileObject ("RCC_NiftiFileSystemDataInterface")) :
            header.removeFileObject ("RCC_NiftiFileSystemDataInterface")
        
        while (header.hasFileObject ("RCC_NiftiPesscaraDataInterface")) :
            header.removeFileObject ("RCC_NiftiPesscaraDataInterface")
        
        while (header.hasFileObject ("RCC_ROIDataset_FileSystemInterface")) :
            header.removeFileObject ("RCC_ROIDataset_FileSystemInterface")
                
        while (header.hasFileObject ("RCC_ROIDataset_PESSCARAInterface")) :
            header.removeFileObject ("RCC_ROIDataset_PESSCARAInterface")
                    
        while (header.hasFileObject ("TagManager")) :
            header.removeFileObject ("TagManager")
                                
        header.addInnerFileObject (self._NIfTI_DatasetInterface.getFileObject ())
        header.addInnerFileObject (self._ROI_DatasetInterface.getFileObject ())      
        header.addInnerFileObject (self.getPenSettings().getFileObject ())
        self._projectPenSettingsChanged = False 
        header.setParameter ("DescriptionContext",self.getDescriptionContext () )
        fobj.writeDataFile (multiuserprojectPath)  
        
        
    def saveProject (self, path = None, DatasetTags = None, FileLocked = False, ForceSave = False, SaveAsPath = None, ROIFSPath = None, NIfTIFSPath = None) :
        if self.isRunningInDemoMode () :
            return
        projectFI = self.getProjectFileInterface()
        if (path is None) :
            path =  projectFI.getProjectPath()
        try :
            try:
                if (not FileLocked) :
                    self._ProjectFileInterface.aquireFileLock ()
                if SaveAsPath is not None :
                    projectUID = None
                else:
                    projectUID = self.getProjectFileInterface ().getProjectUID ()
                print ("Saving Project")
                if self.isProjectReadOnly () and not ForceSave :
                    if projectUID is not None :
                        SavingDeafultProject =  (path == os.path.join (self.getHomeFilePath (), "RCC_VisSettings.prj"))
                        if not SavingDeafultProject :
                            self.saveMultiUserProjectSettings (projectUID, path)
                else:
                    fobj = FileInterface ()    
                    if os.path.exists (path) :
                        try :
                            fobj.readDataFile (path)         # Project UID and Header writen into project file elsewhere. copy accross
                        except :
                            fobj = FileInterface ()
                    header = fobj.getFileHeader ()  
                    header.setParameter ("ProjectUID", projectUID) 
                    header.setParameter ("ProjectName", projectFI.getProjectName())
                    header.setParameter ("UserName", projectFI.getUserName (Actual=True))
                    header.setParameter ("TempStorageDir",self.getProjectTempStorageDir () )                
                    header.setParameter ("ExpandedTreeNodes", self._expandedTreeNodes)
                    header.setParameter ("OpenFilesAtStart", self._getOpenFileList ())
                    header.setParameter ("ImportImagingDlgState", self.getImportImagingDlgState ())
                    usermanager = projectFI.getMultiUserFileSharingManager ()
                    if usermanager is not None and usermanager.isInitalized () and usermanager.didLoadFromText () : 
                        header.setParameter ("MultiUserSharingYAML", usermanager.getYAMLText ())       
                    else:
                        header.setParameter ("MultiUserSharingYAML", "")       
                    self._expandedTreeNodesChanged = False
                    self._openFilesAtStartChanged = False
                    
                    while (header.hasFileObject ("RCC_NiftiFileSystemDataInterface")) :
                        header.removeFileObject ("RCC_NiftiFileSystemDataInterface")
        
                    while (header.hasFileObject ("RCC_NiftiPesscaraDataInterface")) :
                        header.removeFileObject ("RCC_NiftiPesscaraDataInterface")
        
                    while (header.hasFileObject ("RCC_ROIDataset_FileSystemInterface")) :
                        header.removeFileObject ("RCC_ROIDataset_FileSystemInterface")
                
                    while (header.hasFileObject ("RCC_ROIDataset_PESSCARAInterface")) :
                        header.removeFileObject ("RCC_ROIDataset_PESSCARAInterface")
                                                        
                    if NIfTIFSPath is not None and ROIFSPath is not None :
                        Temp_ROI_DatasetInterface = RCC_ROIDataset_FileSystemInterface (path = ROIFSPath, ProjectName = projectFI.getProjectName(), ProjectDataset = self, RILContourCommandLine = None)   
                        if SaveAsPath is not None : 
                            Temp_ROI_DatasetInterface.updateRelativePathIfNecessary (SaveAsPath)
                            projectDirPath, _ = os.path.split (SaveAsPath)
                        else:
                            Temp_ROI_DatasetInterface.updateRelativePathIfNecessary (path)     
                            projectDirPath, _ = os.path.split (path)
                        Temp_NIfTI_DatasetInterface = RCC_NiftiFileSystemDataInterface (Temp_ROI_DatasetInterface, RootSearchPath = [NIfTIFSPath], ProjectDataset = self, RILContourCommandLine = None, ProjectDirPath = projectDirPath, CreatingSaveCopy = True)  #parent= self.getNiftiDatasetDlg ())
                        Temp_NIfTI_DatasetInterface.setUIHeader (self._NIfTI_DatasetInterface.getUIHeader ())
                        header.addInnerFileObject (Temp_ROI_DatasetInterface.getFileObject ())
                        header.addInnerFileObject (Temp_NIfTI_DatasetInterface.getFileObject ())        
                    else:    
                        header.addInnerFileObject (self._NIfTI_DatasetInterface.getFileObject ())
                        header.addInnerFileObject (self._ROI_DatasetInterface.getFileObject ())      
                    header.addInnerFileObject (self._getProjectScanPhaseListFileObject ())   
                    header.addInnerFileObject (self.getPenSettings().getFileObject ())
                    self._projectPenSettingsChanged = False 
                    header.setParameter ("DescriptionContext",self.getDescriptionContext () )
                                                            
                    if DatasetTags is not None :
                        self._DatasetTagManager.loadTagDatasetFromFile (None, FileHeader = header, AddInternalTags = False, AddOwnershipAndLockingInternalTags = False)  
                        self._DatasetTagManager.addMissingFileTagsToGlobalDataset (DatasetTags)
                    
                    while (header.hasFileObject ("TagManager")) :
                            header.removeFileObject ("TagManager")
                            
                    self._DatasetTagManager.saveToFile (header) 
                    if SaveAsPath is not None :
                        fobj.writeDataFile (SaveAsPath)      
                    else:
                       fobj.writeDataFile (path)               
                    if (header.hasFileObject ("RCC_ROIDataset_FileSystemInterface")) :
                        if projectUID is not None :
                            self.saveMultiUserProjectSettings (projectUID, path)                        
                return True
            finally:
                if (not FileLocked) :
                    self._ProjectFileInterface.releaseFileLock ()
        except :
            try:
                MessageBoxUtil.showMessage ("ERROR", "A error occured trying to save the project datafile. Check file permissions and disk space.")
            except :
                pass
            print ("Error could not save project")
            return False
                                           
       
    def getApp (self) :
        return QApplication.instance()
    
    def updateActiveWindowDatasetSelection (self) :
        window = self._activeWindow         
        if (window is None) :
            if (self._selectedDataset is not None) :
                self._selectedDataset.setSelected (False)                
                self._selectedDataset = None
        else:            
            dataset = window.getSelectedNIfTIDataset ()
            if self._selectedDataset != dataset :
                if (self._selectedDataset is not None) :
                    self._selectedDataset.setSelected (False)                
                if (dataset is not None) :
                    dataset.setSelected (True)  
                    
                    #Micro Hack - Datasets load on a thread, the QProgress dialog is modal, so it blocks UI interactions.
                    #Currently there is no way for the dataset loading completion to signal saving the project. This signal is tricky as there is a bit of a 
                    #Race condition between loading the dataset and finishing initalizing the project.  
                    #This hack takes advantage of the modal dialog and first user interation to signal that the project settings should be saved to capture
                    #any changes to the project.
                    if (self._FirstDataSetLoaded):
                        self.saveProject ()
                        self._FirstDataSetLoaded = False
                        
                self._selectedDataset = dataset
        self.getNiftiDatasetDlg ().setTreeViewSelectedDataset (self._selectedDataset)
        self._openFilesAtStartChanged = True
                
    def setActiveWindow (self, window) :                        
        if (self._activeWindow != window) :
            if (self._activeWindow is not None) :
                try :
                    self._activeWindow.setActiveContourWindow (False)
                except:
                    print ("Window does not have setActiveContourWindow method.")
                    
            self._activeWindow = window            
            
            if (self._activeWindow is not None) :
                try :
                    self._activeWindow.setActiveContourWindow (True)
                except:
                    print ("Window does not have setActiveContourWindow method.")            
        self.updateActiveWindowDatasetSelection ()    
        self.getNiftiDatasetDlg ().raise_ ()

            
    def getActiveWindow (self):
        #print ("Get Active Window")
        #print ((self, self._activeWindow))
        return self._activeWindow
    
    def getActiveContourWindowList (self) :
        return self._ContouringWindowList
        
    def createContouringWindow (self) :     
        rcc_contourWindow = RCC_ContourWindow (ProjectDataset=self, Parent=self.getNiftiDatasetDlg ())        
        self._ContouringWindowList.append (rcc_contourWindow)        
        self.getAppEventFilter ().addWindow (rcc_contourWindow) 
        rcc_contourWindow.show ()                            
        self.setActiveWindow (rcc_contourWindow)
        rcc_contourWindow.raise_()
        return rcc_contourWindow
        
    def closeScanPhaseWindow (self, window) :
        if (self.getActiveWindow () == window):
            self.setActiveWindow (None)            
            self._scanPhaseSelectionDlg = None
        if (len (self._ContouringWindowList) > 0) :
            lastwindow = len (self._ContouringWindowList) - 1
            self._ContouringWindowList[lastwindow].activateWindow ()
            
    def closeContouringWindow (self, window):
        self._openFilesAtStartChanged = True
        if (self.getActiveWindow () == window):
            self.setActiveWindow (None)
        if (window in self._ContouringWindowList) :
            self._ContouringWindowList.remove (window)        
        self.getAppEventFilter ().removeWindow (window)
        if (len (self._ContouringWindowList) > 0) :            
            lastwindow = len (self._ContouringWindowList) - 1
            self._ContouringWindowList[lastwindow].activateWindow ()
        elif (self._scanPhaseSelectionDlg != None) :
            self._scanPhaseSelectionDlg.activateWindow ()
    
    def _unlockProject (self) :
        #unlock project
        if self.isProjectReadOnly ():
            return 
        try :            
            if self._project_lock_file is not None :
                try :
                    NonPersistentFSLock.tryToUnlockFile (self._project_lock_file, self._project_lock_signature["LockSignature"], FileHandleUID = self._fileLockUID) 
                except:
                    print ("A error occured unlocking %s" % self._project_lock_file)   
        except:
            pass
    
    def setUIHeaderChanged (self) :
        self._UIHeaderChanged = True
        
    def isUIHeaderChanged (self) :
        return self._UIHeaderChanged
        
    def closeProject (self) :        
        try:
            self.getProjectFileInterface().saveProjectDatasetCache ()
        except:
            print ("Error could not save project cache. The disk or directory where the project is stored may have been deleted or become unavailable.")
        if (self._projectPenSettingsChanged or self._expandedTreeNodesChanged or self._openFilesAtStartChanged, self.isUIHeaderChanged ()) :        
            try :
                self.saveProject ()
            except:
                print ("Error could not save project state. The disk or directory where the project is stored may have been deleted or become unavailable.")
        self._appeventfilter.removeProjectDataset (self)
        if (self._scanPhaseSelectionDlg is not None) :
            self._scanPhaseSelectionDlg.close ()
            self._scanPhaseSelectionDlg = None
            
        removelist = copy.copy (self._ContouringWindowList)
        for window in removelist :
            window.close ()
        self._ContouringWindowList = []     
        if (self in self._globalProjectList) :
            self._globalProjectList.remove (self)        
        else:
            print ("project not found in project list")
        if (self.getNiftiDatasetDlg () is not None):
            self.getNiftiDatasetDlg ().close ()
        if (self.getKerasModelLoader () is not None) :
            self.getKerasModelLoader ().closeOpenInterfaces ()        
        if self._NIfTI_DatasetInterface is not None :
            self._NIfTI_DatasetInterface.close ()
        del self._ROI_DatasetInterface
        del self._NIfTI_DatasetInterface
        self._ROI_DatasetInterface = None                    
        self._NIfTI_DatasetInterface = None       
        gc.collect ()
        try:
            self._unlockProject ()
        except:
            print ("Error could not unlock the project. The disk or directory where the project is stored may have been deleted or become unavailable.")
    
    def cleanupProjectOnAppClose (self) :
        try :
            self.closeProject ()
        except:
            try :
                removelist = copy.copy (self._ContouringWindowList)
                for window in removelist :
                    if window is not None :
                        try :
                            window.close ()
                        except:
                            try :
                                window.shutdownExceptionCloseFileHandle ()
                            except:
                                pass
                self._unlockProject ()
            except:
                pass
        
         
    def setApplicationActivate (self, val) :
        self._ApplicationActivate = val
    
    def isApplicationActive (self) :
        return self._ApplicationActivate
    
    def setProjectWindowFocus (self, val) :
        #print (("Project Window Focus", val))
        self._projectWindowFocus = val
    
    def getProjectWindowFocus (self) :
        return self._projectWindowFocus 
    
    def getPath (self) :
        return  self.getProjectFileInterface().getProjectPath()
    
    def getFilename (self) :
        path, filename = os.path.split (self.getPath ())
        return filename
        
    def getNiftiDatasetDlg (self) : 
        try:
            return self._NiftiDatasetDlg
        except:
            return None
    
    def getROIDatasetInterface (self) :
        return self._ROI_DatasetInterface
    
    def getNIfTIDatasetInterface (self) :
        return self._NIfTI_DatasetInterface
                
    def getActiveContourWindowCount (self) :
        return len (self.getActiveContourWindowList ())

    def getAppEventFilter (self) :
        return self._appeventfilter
        

class AbstractTreeWidgetNode (QObject):
        
        childNodeChangedROIDatasetIndicator = pyqtSignal(['PyQt_PyObject', 'PyQt_PyObject'])  
        treeNodeChanged = pyqtSignal(['PyQt_PyObject'])  
        childNodeChangedPLockStatus = pyqtSignal(['PyQt_PyObject'])  
        treeNodeDescriptionChanged = pyqtSignal(['PyQt_PyObject'])  

        def __init__ (self) :
            super(AbstractTreeWidgetNode, self).__init__ ()
            self._childNodeChangedPLockStatus_called = False
            self._childNodeROIIndicatorChanged_called = False      
            self._childTreeNodeDescriptionChangedCalled = False
            self.clearROIDatasetIndicatorCache ()
        
        def clearROIDatasetIndicatorCache (self) :
            self._ROIDatasetIndicatorCache = None
            
        def isRootNode (self) :
            parent = self.getParent ()
            while (parent is not None) :                
                if len (parent.getChildernLst ()) > 1 :
                    return False
                parent = parent.getParent ()
            return True
        
        def hasOneChild (self) :            
            node = self
            while len (node.getChildernLst ()) == 1 :
                node = node.getChildernLst ()[0]
            if len (node.getChildernLst ()) == 0 :
                return True
            return False
            
        def isDelayedCacheLoadingSet (self) :
            return False
        
        def delayedCacheLoading (self, AquireFileLock = True, InitalizeChildNodes = True, SetParentLocked = True) :
            return
        
        def fireTreeNodeChangedEvent (self) :
            self.treeNodeChanged.emit (self)
            
        def fireTreeNodeDescriptionChangedEvent (self, AquireFileLock) :
            self.treeNodeDescriptionChanged.emit (AquireFileLock)
        
        def callNodeChangedPLockStatus (self, AquireFileLock = True) :
            self.childNodeChangedPLockStatus.emit (AquireFileLock)                 
            
        def callChildNodeROIIndicatorChanged (self, hasdata, AquireFileLock = True) :
            self.childNodeChangedROIDatasetIndicator.emit (hasdata, AquireFileLock)                 
        
        def initCache (self, tags, antslog, nodeLock, plocked, description, hasdata):
             tree = self._tree
             if (tree is not None) :                 
                 treePath = self.getTreePath ()                 
                 PLockStatus = PersistentFSLock.getLockStatusForCurrentUser (plocked, tree._ProjectDataset.getProjectFileInterface ())                                
                 self._setCurrentUserPLockStatus (PLockStatus)   
                 SettingsDict = {}
                 SettingsDict["Tags"] = tags.copy ()
                 SettingsDict["AntsLog"] = antslog
                 SettingsDict["PLock"] = plocked
                 SettingsDict["NodeLocked"] = nodeLock
                 SettingsDict["ScanPhase"] = description
                 SettingsDict["ContoursExist"] = hasdata   
                 self._ROIDatasetIndicatorCache = hasdata                    
                 tree._ProjectDataset.getProjectFileInterface().setProjectDatasetCache (treePath, SettingsDict, Key=None, DictSet = True)                 
                 self.callChildNodeROIIndicatorChanged (hasdata)                 
                 self.callNodeChangedPLockStatus ()                 
                 self.fireTreeNodeChangedEvent ()
            
        
        def getUICacheTag (self, tagidentifer, AquireFileLock = True):
            tree = self._tree
            if (tree is None) :
                return None                 
            treePath = self.getTreePath ()
            UITagCache = tree._ProjectDataset.getProjectFileInterface().getProjectDatasetUICache (AquireFileLock = AquireFileLock)  
            if tagidentifer in UITagCache :
                if treePath in UITagCache[tagidentifer] :
                    return UITagCache[tagidentifer][treePath]
            tags = self.getTags (AquireFileLock = AquireFileLock)
            result = tags.getTagByIdentifer (tagidentifer)
            if tagidentifer not in UITagCache :
                UITagCache[tagidentifer] = {}
            if treePath not in UITagCache[tagidentifer] :
                if result is not None :
                    UITagCache[tagidentifer][treePath] = result.getTextValue ()
                else:
                    UITagCache[tagidentifer][treePath] = None
            return result
            
        def _setTagsInitalized (self) :
            return 
        
        def _initTagsIfNecessary (self, result, AquireFileLock = True) :
            return result
        
        def getTags(self, AquireFileLock = True, SkipInitalization = False)  :
             self.delayedCacheLoading (AquireFileLock = AquireFileLock)
             tree = self._tree
             if (tree is None) :
                 result = DatasetTagManager ()
             else:
                 treePath = self.getTreePath ()
                 result = tree._ProjectDataset.getProjectFileInterface().getProjectDatasetCache (treePath, Key="Tags", Default = "NONE", AquireFileLock = AquireFileLock)             
                 if isinstance (result, str) and result == "NONE" :
                     result = DatasetTagManager  ()
                 elif result is None :
                     result =DatasetTagManager  ()
             if not SkipInitalization :
                 result = self._initTagsIfNecessary (result, AquireFileLock = AquireFileLock)
             return result
        
        def setTags(self, tags, FireTreeNodeChangedEvent = True, AquireFileLock = True, NodePLockedState = None) :    
             self.delayedCacheLoading (AquireFileLock = AquireFileLock)                      
             tree = self._tree
             if (tree is not None) :                 
                tagCopy = tags.copy () 
                self._addInternalTags (tagCopy, AquireFileLock = AquireFileLock, NodePLockedState = NodePLockedState)
                treePath = self.getTreePath ()
                result = tree._ProjectDataset.getProjectFileInterface().getProjectDatasetCache (treePath, Key="Tags", Default = "NONE", AquireFileLock = AquireFileLock)
                self._setTagsInitalized ()
                if isinstance (result, DatasetTagManager) :
                    if result.equals (tagCopy) :
                        return False
                    
                tree._ProjectDataset.getProjectFileInterface().setProjectDatasetCache (treePath, tagCopy, Key="Tags", AquireFileLock = AquireFileLock)
                UITagCache = tree._ProjectDataset.getProjectFileInterface().getProjectDatasetUICache (AquireFileLock = AquireFileLock)  
                for tagidentifer in UITagCache.keys () :
                    result = tagCopy.getTagByIdentifer (tagidentifer)
                    if result is not None :
                        UITagCache[tagidentifer][treePath] = result.getTextValue ()
                    else:
                        UITagCache[tagidentifer][treePath] = None
                if FireTreeNodeChangedEvent :
                    self.fireTreeNodeChangedEvent ()  
                    return False
                return True
             return False                    
            
        def isNodeAntsRegistered (self, AquireFileLock = True) :
            self.delayedCacheLoading (AquireFileLock = AquireFileLock)
            tree = self._tree
            if (tree is None) :
                return False
            antsLog = tree._ProjectDataset.getProjectFileInterface().getProjectDatasetCache (self.getTreePath (), Key="AntsLog", Default = "NONE", AquireFileLock = AquireFileLock)
            if isinstance (antsLog, str) and antsLog == "NONE" :
                return False
            return antsLog is not None 
        
        def setANTSRegisteredLog (self, log, AquireFileLock = True) :
            self.delayedCacheLoading (AquireFileLock = AquireFileLock)
            tree = self._tree
            if (tree is not None) :
                tree._ProjectDataset.getProjectFileInterface().setProjectDatasetCache (self.getTreePath (), log, Key="AntsLog", AquireFileLock = AquireFileLock)
        
        def getAntsRegisteredDate (self,AquireFileLock = True) :
            self.delayedCacheLoading (AquireFileLock = AquireFileLock)
            tree = self._tree
            if (tree is None) :
                return None
            antsLog = tree._ProjectDataset.getProjectFileInterface().getProjectDatasetCache (self.getTreePath (), Key="AntsLog", Default = "NONE", AquireFileLock = AquireFileLock)
            if isinstance (antsLog, str) and antsLog == "NONE" :
                return None
            if (antsLog is None) :
                return None
            dateResult = antsLog.getLastRegisterDate ()
            if dateResult is None :
                return None
            year, month, day = dateResult
            return str (month) + "/" + str (day) + "/" + str (year)
        
        def getAntsRegisteredDateTuple (self, AquireFileLock = True) :
            self.delayedCacheLoading (AquireFileLock = AquireFileLock)
            tree = self._tree
            if (tree is None) :
                return (None,None, None)
            antsLog = tree._ProjectDataset.getProjectFileInterface().getProjectDatasetCache (self.getTreePath (), Key="AntsLog", Default = "NONE", AquireFileLock = AquireFileLock)
            if isinstance (antsLog, str) and antsLog == "NONE" :
                return (None,None,None)
            if (antsLog is None) :
                return (None,None,None)
            year, month, day = antsLog.getLastRegisterDate ()
            return (month,day,year)
        
        def setNodeROIFileLock  (self, nodeLock, AquireFileLock = True) :
            self.delayedCacheLoading (AquireFileLock = AquireFileLock)
            tree = self._tree
            if (tree is not None) :
               tree._ProjectDataset.getProjectFileInterface().setProjectDatasetCache (self.getTreePath (), nodeLock, Key="NodeLocked", AquireFileLock = AquireFileLock)
               
        def getROIFileLock  (self, AquireFileLock = True) :
            self.delayedCacheLoading (AquireFileLock = AquireFileLock)
            tree = self._tree
            if (tree is None) :
                return "unlocked"
            nodeLock = tree._ProjectDataset.getProjectFileInterface().getProjectDatasetCache (self.getTreePath (), Key="NodeLocked", Default = "unlocked", AquireFileLock = AquireFileLock)
            if nodeLock is None or nodeLock == False:
                return "unlocked"
            if nodeLock == True :
                return "locked_other"
            return nodeLock        
        
        
                     
        def _addInternalTags (self, tags, AquireFileLock = True, NodePLockedState = None):
            PLock = self.getNodePersistentFileLock (AquireFileLock = AquireFileLock)
            if NodePLockedState is None :
                currentNodeStatus = self.getCurrentUserPLockStatus (AquireFileLock = AquireFileLock) 
            else:
                currentNodeStatus = NodePLockedState
            if currentNodeStatus == "OtherUserLocked" :
               currentNodeStatus = "Other user"
            elif currentNodeStatus == "UserLocked" :
               currentNodeStatus = "Acquired"
            elif currentNodeStatus == "Unlocked" :
               currentNodeStatus = ""
            elif currentNodeStatus == "UserGroupLocked" :
               currentNodeStatus = "Group"
            if PLock is None or not isinstance (PLock, dict):
                tags.setInternalTag ("File owner", "")
                tags.setInternalTag ("File status", currentNodeStatus)
                tags.setInternalTag ("Date acquired", "")                
                tags.setInternalTag ("Time acquired", "")                                                                                                                            
                tags.setInternalTag ("PLock", "None")
                tags.setInternalTag ("Assign comment", "")
            elif ("Closed" in PLock and PLock["Closed"] is not None) :
                tags.setInternalTag ("File owner", "")
                tags.setInternalTag ("File status", currentNodeStatus)
                tags.setInternalTag ("Date acquired", "")                
                tags.setInternalTag ("Time acquired", "")                                                                                                                            
                tags.setInternalTag ("PLock", "Closed")
                tags.setInternalTag ("Assign comment", "")
            elif ("Created" in PLock and PLock["Created"] is not None) :                
                tags.setInternalTag ("PLock", "Open")                
                tags.setInternalTag ("Date acquired", "")    
                tags.setInternalTag ("Time acquired", "")  
                tags.setInternalTag ("File status", currentNodeStatus) 
                                    
                if currentNodeStatus in ["Group", "Acquired"] and "OpenComment" in PLock and PLock["OpenComment"] is not None:
                    tags.setInternalTag ("Assign comment", PLock["OpenComment"])          
                else:
                    tags.setInternalTag ("Assign comment", "")       
                                                
                if "UserName" in PLock and PLock["UserName"] is not None:
                    tree = self._tree
                    userName = PLock["UserName"]
                    if tree is not None :
                         multiFileSharing = tree._ProjectDataset.getProjectFileInterface().getMultiUserFileSharingManager ()
                         if multiFileSharing is not None :
                             userName = multiFileSharing.getUserNickname (userName)
                    tags.setInternalTag ("File owner", userName)
                elif "GroupName" in PLock and PLock["GroupName"] is not None:                
                    tags.setInternalTag ("File owner", PLock["GroupName"])
                else:
                    tags.setInternalTag ("File owner", "Unknown")                                
                
                if "Created" in PLock and PLock["Created"] is not None : 
                    if  "Date" in PLock["Created"] and PLock["Created"]["Date"] is not None:     
                        tags.setInternalTag ("Date acquired", PLock["Created"]["Date"])                                            
                    if  "Time" in PLock["Created"] and PLock["Created"]["Time"] is not None:     
                        tags.setInternalTag ("Time acquired", PLock["Created"]["Time"])     
            
        def _setPLockStateTags (self, FireTreeNodeChangedEvent=True, AquireFileLock = True, NodePLockedState = None) :            
            self.setTags (self.getTags (AquireFileLock = AquireFileLock, SkipInitalization = True), FireTreeNodeChangedEvent= FireTreeNodeChangedEvent, AquireFileLock = AquireFileLock, NodePLockedState = NodePLockedState) 
                        
        def _setCurrentUserPLockStatus (self, PLockStatus, FireTreeNodeChangedEvent = True, AquireFileLock = True) :
            tree = self._tree
            if (tree is not None) :
                tree._ProjectDataset.getProjectFileInterface().setProjectDatasetCache (self.getTreePath (), PLockStatus, Key="CurrentUserPLock", AquireFileLock = AquireFileLock)
                self._setPLockStateTags (FireTreeNodeChangedEvent= FireTreeNodeChangedEvent,AquireFileLock = AquireFileLock, NodePLockedState = PLockStatus)                
                
        def setNodePersistentFileLock  (self, nodeLock, FireChildNodeChangeEvent= True, FireTreeNodeChangeEvent = True, AquireFileLock = True, UserGroupList = None) :
            self.delayedCacheLoading (AquireFileLock = AquireFileLock)
            CallChildNodeChangeEvent = False
            CallNodeChangeEvent = False
            tree = self._tree
            if (tree is not None) :               
               try :
                   if AquireFileLock :
                       tree._ProjectDataset.getProjectFileInterface().aquireFileLock ()
                   tree._ProjectDataset.getProjectFileInterface().setProjectDatasetCache (self.getTreePath (), nodeLock, Key="PLock",  AquireFileLock = False)                                                         
                   PLockStatus = PersistentFSLock.getLockStatusForCurrentUser (nodeLock, tree._ProjectDataset.getProjectFileInterface (), UserGroupList = UserGroupList)                            
                   currentStatus = self.getCurrentUserPLockStatus (AquireFileLock = False)                              
                   if currentStatus != PLockStatus:                       
                       self._setCurrentUserPLockStatus (PLockStatus, FireTreeNodeChangedEvent = False, AquireFileLock = False)               
                       CallChildNodeChangeEvent = True
                       CallNodeChangeEvent = True
                       if FireChildNodeChangeEvent :
                           self.callNodeChangedPLockStatus (AquireFileLock = AquireFileLock)                 
                           CallChildNodeChangeEvent = False
                       if FireTreeNodeChangeEvent :
                           self.fireTreeNodeChangedEvent ()
                           CallNodeChangeEvent = False
               finally:
                    if AquireFileLock :
                        tree._ProjectDataset.getProjectFileInterface().releaseFileLock ()
            return CallChildNodeChangeEvent, CallNodeChangeEvent
        
        def getNodePersistentFileLock  (self, AquireFileLock = True) :
            self.delayedCacheLoading (AquireFileLock = AquireFileLock)
            tree = self._tree
            if (tree is None) :
                return None
            nodeLock = tree._ProjectDataset.getProjectFileInterface().getProjectDatasetCache (self.getTreePath (), Key="PLock",Default = "NONE", AquireFileLock = AquireFileLock)
            if isinstance (nodeLock, str) and nodeLock == "NONE" :
                return None
            return nodeLock
                             
        def getCurrentUserPLockStatus (self, AquireFileLock = True, UseDecompressionCache = True) :
            self.delayedCacheLoading (AquireFileLock = AquireFileLock)
            tree = self._tree
            if (tree is None) :
                return "Unlocked"
            nodeLock = tree._ProjectDataset.getProjectFileInterface().getProjectDatasetCache (self.getTreePath (), Key="CurrentUserPLock", Default = "Unlocked", AquireFileLock = AquireFileLock, UseDecompressionCache = UseDecompressionCache)
            if nodeLock is None :
                return "Unlocked"
            return nodeLock
               
        def setDescription (self, description, AquireFileLock = True, FireTreeNodeChangedEvent = True, FireTreeNodeDescriptionChangeEvent = True):
            self.delayedCacheLoading (AquireFileLock = AquireFileLock)
            tree = self._tree        
            if (tree is None) :
                change = False
            else:
               change = tree._ProjectDataset.getProjectFileInterface().setProjectDatasetCache (self.getTreePath (), description, Key="ScanPhase", AquireFileLock = AquireFileLock)               
               if change :
                   if FireTreeNodeDescriptionChangeEvent :
                       self.fireTreeNodeDescriptionChangedEvent (AquireFileLock)
                   if FireTreeNodeChangedEvent :
                       self.fireTreeNodeChangedEvent ()
                       change = False
            return change
           
               
        def getDescription (self, AquireFileLock = True):
            self.delayedCacheLoading (AquireFileLock = AquireFileLock)
            tree = self._tree
            if (tree is None) :
                return "Unknown"
            description = tree._ProjectDataset.getProjectFileInterface().getProjectDatasetCache (self.getTreePath (), Key="ScanPhase", Default = "Unknown", AquireFileLock =AquireFileLock)
            if description is None :
                return "Unknown"
            return description
            
        def setROIDatasetIndicator (self, val, FireChildNodeChangedEvent = True, FireTreeNodeChangedEvent = True, AquireFileLock = True) :
            if self._ROIDatasetIndicatorCache is not None and self._ROIDatasetIndicatorCache == val :
                return False, False
            self._ROIDatasetIndicatorCache = val
            
            if self.describesNiftiDataFile () :
                self.delayedCacheLoading (AquireFileLock = AquireFileLock)
            tree = self._tree
            CallFireChildNodeChangedEvent = False
            CallFireTreeNodeChangedEvent = False
            if (tree is not None) :
                   tree._ProjectDataset.getProjectFileInterface().setProjectDatasetCache (self.getTreePath (), val, Key="ContoursExist", AquireFileLock = AquireFileLock)
                   CallFireChildNodeChangedEvent = True
                   CallFireTreeNodeChangedEvent = True
                   if FireChildNodeChangedEvent :
                       self.callChildNodeROIIndicatorChanged (val, AquireFileLock = AquireFileLock)
                       CallFireChildNodeChangedEvent = False
                   if FireTreeNodeChangedEvent :
                       self.fireTreeNodeChangedEvent ()
                       CallFireTreeNodeChangedEvent = False
            return CallFireChildNodeChangedEvent, CallFireTreeNodeChangedEvent
        
        def getROIDatasetIndicator (self, AquireFileLock = True, InitalizeChildNodes = True) :
            if self._ROIDatasetIndicatorCache is not None  :
                return self._ROIDatasetIndicatorCache 
            self.delayedCacheLoading (AquireFileLock = AquireFileLock, InitalizeChildNodes = InitalizeChildNodes)
            tree = self._tree
            if (tree is None) :
                return False
            NodeROIDatasetIndicator = tree._ProjectDataset.getProjectFileInterface().getProjectDatasetCache (self.getTreePath (), Key="ContoursExist", Default = False, AquireFileLock = AquireFileLock)
            self._ROIDatasetIndicatorCache = NodeROIDatasetIndicator
            if NodeROIDatasetIndicator is None :
                return False
            return NodeROIDatasetIndicator
            
        def getROIDatasetMLDescriptionPrediction (self, AquireFileLock = True) :
            tags = self.getTags (AquireFileLock = AquireFileLock)                                
            if tags is not None and tags.hasInternalTag ("ML_DatasetDescription"):
                return tags.getInternalTag ("ML_DatasetDescription")
            return "Unknown"
                
        def setROIDatasetMLDescriptionPrediction (self, val, FireTreeNodeChangedEvent = True, AquireFileLock = True) :  
               tags = self.getTags ()                                
               tags.setInternalTag ("ML_DatasetDescription", val)
               self.setTags (tags, FireTreeNodeChangedEvent = FireTreeNodeChangedEvent, AquireFileLock= AquireFileLock)
        
        def initTreeWidgetCacheStateIfNecessary (self, FireTreeNodeChangedEvent = True, AquireFileLock = True) :
            self.delayedCacheLoading (AquireFileLock = AquireFileLock)
            tree = self._tree
            if (tree is not None) :
                
                if (not tree._ProjectDataset.getProjectFileInterface().hasProjectDatasetCache (self.getTreePath (), AquireFileLock = AquireFileLock) ) :
                    try:
                        if AquireFileLock :
                            tree._ProjectDataset.getProjectFileInterface().aquireFileLock ()
                        self.setTags (DatasetTagManager (),FireTreeNodeChangedEvent  = False, AquireFileLock = False)
                        self.setANTSRegisteredLog (ANTSRegistrationLog (None), AquireFileLock = False)
                        self.setDescription ("Unknown", AquireFileLock = False, FireTreeNodeChangedEvent = False, FireTreeNodeDescriptionChangeEvent = False)
                        self.setROIDatasetMLDescriptionPrediction  ("Unknown",FireTreeNodeChangedEvent  = False, AquireFileLock = False)
                        self.setROIDatasetIndicator (False, FireChildNodeChangedEvent = False, FireTreeNodeChangedEvent  = False, AquireFileLock = False)
                        if FireTreeNodeChangedEvent :
                            self.fireTreeNodeChangedEvent ()
                    finally:
                        if AquireFileLock :
                            tree._ProjectDataset.getProjectFileInterface().releaseFileLock ()
       
            
             
        def _childNodeROIIndicatorChanged (self, val, AquireFileLock = True) :            
            if  not self._childNodeROIIndicatorChanged_called :
                try :
                    self._childNodeROIIndicatorChanged_called = True
                    if self.isDelayedCacheLoadingSet ()  :
                        if not val :
                            return
                        elif self._ROIDatasetIndicatorCache != True :
                            self._ROIDatasetIndicatorCache = val
                            tree = self._tree
                            if (tree is not None) :
                                tree._ProjectDataset.getProjectFileInterface().setProjectDatasetCache (self.getTreePath (), val, Key="ContoursExist")
                            self.callChildNodeROIIndicatorChanged (val, AquireFileLock = AquireFileLock)
                            self.fireTreeNodeChangedEvent ()
                            return
                    currentValue = self.getROIDatasetIndicator ()        
                    if val is not None and val :                
                        if val != currentValue :
                            self.setROIDatasetIndicator (val)
                    elif val != currentValue :
                       found = False
                       lst = self.getChildernLst ()
                       if len (lst) > 0 :
                           for child in lst :
                               if (child.getROIDatasetIndicator ()) :
                                   found = True 
                                   break
                           if found != currentValue or val is not None:
                               self.setROIDatasetIndicator (found)
                finally:
                    self._childNodeROIIndicatorChanged_called = False
              
       
        def _childNodeChangedPLockStatus (self, AquireFileLock = True):   
                if self.isDelayedCacheLoadingSet () :
                    return                    
                tree = self._tree                    
                if (tree is not None) :
                    childernList = self.getChildernLst ()
                    if len (childernList) > 0 :
                        if not  self._childNodeChangedPLockStatus_called :
                            try :
                                self._childNodeChangedPLockStatus_called = True
                                childernState = set ()   
                                for child in childernList :
                                    if not child.isDelayedCacheLoadingSet () :
                                        childernState.add ( child.getCurrentUserPLockStatus (AquireFileLock = AquireFileLock))
                                if len (childernState) == 0 :
                                    return 
                                                                  
                                if len (childernState) == 1 :
                                   nodeLockedState = childernState.pop ()                                   
                                elif "UserLocked" in childernState :
                                    nodeLockedState = "UserLocked"
                                elif "UserGroupLocked" in childernState :
                                    nodeLockedState = "UserGroupLocked"
                                elif "Unlocked" in childernState :
                                    nodeLockedState = "Unlocked"
                                else:
                                    nodeLockedState = "OtherUserLocked"
                                if (nodeLockedState != self.getCurrentUserPLockStatus (AquireFileLock = AquireFileLock)) :
                                    self._setCurrentUserPLockStatus (nodeLockedState, FireTreeNodeChangedEvent = False, AquireFileLock =AquireFileLock)      
                                    self.callNodeChangedPLockStatus (AquireFileLock = AquireFileLock)                 
                                    self.fireTreeNodeChangedEvent ()
                            finally:
                                self._childNodeChangedPLockStatus_called = False
                                
        def _childNodeDescriptionChanged  (self, AquireFileLock = True) :
            if self.isDelayedCacheLoadingSet () :
                return                    
            tree = self._tree                    
            if (tree is not None) :
                childernList = self.getChildernLst ()
                if len (childernList) > 0 :
                    if not self._childTreeNodeDescriptionChangedCalled :
                        try :
                            self._childTreeNodeDescriptionChangedCalled = True
                            nodeDescription = "Unknown"
                            for child in childernList :
                                if not child.isDelayedCacheLoadingSet () :
                                    childDescription = child.getDescription (AquireFileLock = AquireFileLock)
                                    if childDescription != "Unknown" :
                                        if nodeDescription == "Unknown" :
                                            nodeDescription = childDescription   
                                            if childDescription == "Multiple" :
                                                break                                        
                                        elif nodeDescription != childDescription :
                                            nodeDescription = "Multiple"                                            
                            self.setDescription  (nodeDescription, AquireFileLock = AquireFileLock, FireTreeNodeChangedEvent = True)                                
                        finally :
                            self._childTreeNodeDescriptionChangedCalled = False
                    
                    
class PesscaraTreeWidgetNode (AbstractTreeWidgetNode) :
    
        def __init__ (self, pesscaraInterface, tree, parent, hasROIDataCodeList, row, subject, exam, series, Siblings = None):                                    
            super(PesscaraTreeWidgetNode, self).__init__ ()
            self._initalizeThreadLoadingState ()
            self._tree = tree
            if tree is not None :
                self.treeNodeChanged.connect (tree.treeNodeChanged)
            self._TreeTextPath = None
            self._childLoadingThread = None
            self._pesscaraInterface = pesscaraInterface
            self._NodeObjTupl = (subject, exam, series)                                    
            subCode = None
            eCode = None
            sCode = None
            if (subject is not None) :
                subCode = subject.code
            if (exam is not None) :
                eCode = exam.code
            if (series is not None) :
                sCode = series.code                
            self._NodeIDTupl = (subCode, eCode, sCode)
            self._text = ""                    
            self._row = row            
            self._childrenList = []
            self._parent = parent
            if parent is not None :
                self.childNodeChangedROIDatasetIndicator.connect (parent._childNodeROIIndicatorChanged)
                self.childNodeChangedPLockStatus.connect (parent._childNodeChangedPLockStatus)
                self.treeNodeDescriptionChanged.connect (parent._childNodeDescriptionChanged)                
            self._selected = False
            self._datasetOpened = False
            self._finalizer = None
            if PythonVersionTest.IsPython3 () :
                self._finalizer = weakref.finalize (self, PesscaraTreeWidgetNode._finalizeObj, self._childLoadingThread)
            if (self.isSubject ()) :
                self.setText (self.getSubjectName ())
            elif (self.isExam ()):                
                self.setText (self.getExamName ())
            else:                
                self.setText (self.getSeriesName ())             
            
            if parent is not None :
                if Siblings is None :
                    suffix = ""                        
                else:
                    suffix = PesscaraTreeWidgetNode.getUniqueSuffix (Siblings, self)
            else:
                suffix = PesscaraTreeWidgetNode.getUniqueSuffix (tree._rootMemory, self)
            if suffix != "" :
                self.setText (self.getText () + suffix)             
            
            self.initTreeWidgetCacheStateIfNecessary ()
            if (hasROIDataCodeList is not None) :
                #if (self._parent is None or self._parent.getNodeROIDatasetIndicator () == True) :                                            
                if (self.isSubject ()) :
                    self.setROIDatasetIndicator (subject.code in hasROIDataCodeList)
                elif (self.isExam ()):
                    self.setROIDatasetIndicator (exam.code in hasROIDataCodeList)
                else:
                    self.setROIDatasetIndicator (series.code in hasROIDataCodeList)              
            self.callNodeChangedPLockStatus ()                 
            self.callChildNodeROIIndicatorChanged (self.getROIDatasetIndicator ())
            
        if PythonVersionTest.IsPython2 () :
            def __del__ (self) :
                PesscaraTreeWidgetNode._finalizeObj (self._childLoadingThread)
        
        
        @staticmethod
        def getUniqueSuffix (nodelst, currentNode) :
            suffix = ""
            tenativeTxt = currentNode.getText ()
            txtLst = []
            for node in nodelst :
                txt = node.getText ()
                if txt.startswith (tenativeTxt) :
                   txtLst.append (txt)  
            if len (txtLst) == 0 :
                return suffix   
            counter = 1 
            while True :
                suffix = "_" + str (counter)
                if tenativeTxt + suffix not in txtLst :
                   return  suffix
                counter += 1
                
            
        def _removeFinalizer (self) :
            if self._finalizer is not None :            
                self._finalizer.detach()
                self._finalizer = None 
        
        @staticmethod
        def _finalizeObj (childLoadingThread) :
            if (childLoadingThread is not None) :
                childLoadingThread.exiting = True            
        
        def getTreePathList (self):
            path = [self.getText  ()]
            parent = self.getParent ()
            while (parent != None) :
                parent_text = parent.getText ()
                path = [parent_text] +  path                    
                parent = parent.getParent ()  
            return path
        
            
        def getTreePath (self):
            if self._TreeTextPath is not None :
                return self._TreeTextPath
            path = self.getText  ()
            parent = self.getParent ()
            while (parent != None) :
                parent_text = parent.getText ()
                path = os.path.join (parent_text, path)                    
                parent = parent.getParent ()  
            self._TreeTextPath = path
            return path
        
        def describesNiftiDataFile (self) :
            return self.getSeries () is not None
        
        def isDatasetOpen (self) :
            return self._datasetOpened
        
        def setDatasetOpen (self, val) :
            if (self._datasetOpened != val) :                            
                self._datasetOpened = val    
                self.fireTreeNodeChangedEvent ()
                if (self.hasParent ()):
                    self.getParent ().setDatasetOpen (val)  
                                   
        def setSelected (self, val) :
            if (self._selected != val) :                            
                self._selected = val    
                self.fireTreeNodeChangedEvent ()
                if (self.hasParent ()):
                    self.getParent ().setSelected (val)                        
        
        def isSelected (self) :
            return self._selected 
        
        def getNodeType (self) :
            return "PesscaraTreeWidgetNode"
            
        def equals (self, testTreeNode):
            try :
                return (self._NodeIDTupl[0] == testTreeNode._NodeIDTupl[0] and self._NodeIDTupl[1] == testTreeNode._NodeIDTupl[1] and self._NodeIDTupl[2] == testTreeNode._NodeIDTupl[2])
            except :
                return False
            
        def getExportMaskFilesystemPath (self) :
            return ""
                    
        def getPesscaraInterace (self) :
             return self._pesscaraInterface
             
        def setPesscaraDataSetSource (self, pesscaraInterfaceFileObj, SubjectCode, ExamCode, SeriesCode) :
           return
            
        def getSaveFileNifitFileSourceFileObject (self)  :
            f = FileObject ("NIfTI_Data_Source_Description")
            f.setParameter ("DataLocation","PESSCARA")
            subjectCode, examCode, seriesCode = self.getNodeIDTupl ()
            f.addInnerFileObject (self._pesscaraInterface.getFileObject ())
            f.setParameter ("PESSCARA_SubjectCode",subjectCode)
            f.setParameter ("PESSCARA_ExamCode",examCode)
            f.setParameter ("PESSCARA_SeriesCode",seriesCode)            
            f.setParameter ("PESSCARA_Context", self.getPesscaraContex ())
            return f    
            
        def getNodeROIDatasetIndicator (self):            
            return self.getROIDatasetIndicator ()
                            
        def getExam (self):
            subject, exam, series = self._NodeObjTupl 
            return exam
            
        def getSubject (self):
            subject, exam, series = self._NodeObjTupl     
            return subject
            
        def getSeries (self): 
            subject, exam, series = self._NodeObjTupl 
            return series 
        
        def getFilesystemROIDataPath (self) : #returns the data path used to store the ROIDatafile on disk
            return self.getTreePath ()+ ".nii.gz"            
            
        def getNIfTIDatasetFile (self) :            
                if (self.isSubject () or self.isExam ()):
                    return PesscaraDF (self._pesscaraInterface, None)                    
                else:
                    #subject, exam, series = self._NodeObjTupl                                        
                    return PesscaraDF (self._pesscaraInterface, self)
                    
        def getSubjectName (self) :
            subject, exam, series = self._NodeObjTupl 
            return subject.name
        
        def getSubjectID (self) :
            subject, exam, series = self._NodeObjTupl 
            return subject.subjectid
        
        def getExamName (self) :
            subject, exam, series = self._NodeObjTupl 
            return exam.name            

        def getSeriesName (self) :
            subject, exam, series = self._NodeObjTupl 
            return series.name                        
            
        def textUndefined (self) :
            return self._text == ""
        
        def getText (self) :
            return self._text
        
        def setText (self, txt) :
            self._text = txt
        
        def hasParent (self) :
            return (self._parent is not None) 
                                
        def getParent (self) :
            return self._parent
        
        def getParentRow (self) :
            if (self._parent is not None):
                return self._parent._row
                         
        def getChild (self, row):                       
            if (row >= 0 and row < len (self._childrenList)):                
               return self._childrenList[row]    
                              
        def getChildernLst (self) :
            return self._childrenList
            
        def getChildCount (self) :
            return len (self._childrenList)
        
        def hasOnlySingleChild (self) :            
            node = self
            while True :
                if len (node._childrenList) == 0 :
                    break
                elif len (node._childrenList) > 1:
                    return False
                else:
                    node = node._childrenList[0]                        
            return True                    

        def setChildernLst (self, lst):
            if (self._childrenList != lst) :                
                    self._childrenList =  lst                       
                    
        def setScanPhaseTxt (self, scanPhase, UpdateTree = True) :            
            self.setDescription (scanPhase)                                            
               
        def setMLPredictedScanPhaseTxt (self, predictedScanPhase, UpdateTree = True) :
            if (self.getROIDatasetMLDescriptionPrediction ()!= predictedScanPhase) :
                self.setROIDatasetMLDescriptionPrediction (predictedScanPhase)           
                if (self.getDescription () == "Unknown") :
                    self.fireTreeNodeChangedEvent ()
                
        def getScanPhaseTxt (self) :
            if (self.getDescription () == "Unknown") :
                if (self.getROIDatasetMLDescriptionPrediction ()!= "Unknown") :
                    return ("Predicted: " + self.getROIDatasetMLDescriptionPrediction ()) 
                return ""
            else:
                return self.getDescription ()
        
        def isScanPhasePredicted (self) :
            if (self.getDescription () == "Unknown") :
                if (self.getROIDatasetMLDescriptionPrediction ()!= "Unknown") :
                    return True
            return False       
        
        def isSeries (self) :
            subject, exam, series = self._NodeObjTupl 
            return (subject != None and exam != None and series != None)
            
        def isExam (self) :
            subject, exam, series = self._NodeObjTupl 
            return (subject != None and exam != None and series == None)
            
        def isSubject (self) :
            subject, exam, series = self._NodeObjTupl 
            return (subject != None and exam == None and series == None)
        
        def getRow (self) :
            return self._row
        
        def getNodeIDTupl (self) :
            return self._NodeIDTupl
             
        def nodeDataUpdated (self, childernList, childLoadingThread):
                self._childLoadingThread = childLoadingThread    
                childHasData = False
                for child in childernList :
                    if child.isThreadLoading () :
                       child.nodeDataUpdated (child._childrenList, None)
                    childHasData = childHasData or child.getROIDatasetIndicator ()                    
                self.setChildernLst (childernList)            
                self._finishedThreadLoad ()                
                if len (childernList) > 0 :
                    self._childNodeROIIndicatorChanged (childHasData)
                    self._childNodeChangedPLockStatus ()             
                    self._childNodeDescriptionChanged ()
                self.fireTreeNodeChangedEvent ()
                                
                 
        def getPesscaraContex (self) :
            return self._tree.getPesscaraContex ()
                                   
        def loadAllChildTreeNodes (self) :            
            self._tree.loadAllChildTreeNodes (self)            
                
        def getQModelIndex (self):
            return self._tree.createIndex (self.getRow (), 0, self)                          
        
        def _initalizeThreadLoadingState (self) :
            self._ThreadLoading = True
            self._Tags = DatasetTagManager  ()
            self._AntsLog = None
            self._NodeLocked = "unlocked"
            self._PLock = None
            self._Description = "Unknown"
            self._HasData = False
            self._CurrentUserPLock = "Unlocked"

        def isThreadLoading (self) :
            return self._ThreadLoading
        
        def _finishedThreadLoad (self):
            if self._ThreadLoading :
                self._ThreadLoading = False                          
                self.initCache (self._Tags, self._AntsLog, self._NodeLocked, self._PLock, self._Description, self._HasData)                
                del self._CurrentUserPLock
                del self._Tags
                del self._AntsLog
                del self._NodeLocked
                del self._PLock                
                del self._Description
                del self._HasData
            
            
            
        def getTags (self, AquireFileLock = True)  :
            if self._ThreadLoading : 
                return self._Tags
            else:
                return super(PesscaraTreeWidgetNode, self).getTags (AquireFileLock = AquireFileLock)
       
        def setTags (self, tags, FireTreeNodeChangedEvent = True, AquireFileLock = True, NodePLockedState = None) :   
            if self._ThreadLoading :
               tagCopy = tags.copy () 
               self._addInternalTags (tagCopy, AquireFileLock = AquireFileLock, NodePLockedState = NodePLockedState)
               self._Tags = tagCopy
               if FireTreeNodeChangedEvent :
                   self.fireTreeNodeChangedEvent ()                    
            else:
               super(PesscaraTreeWidgetNode, self).setTags (tags, FireTreeNodeChangedEvent = FireTreeNodeChangedEvent, AquireFileLock = AquireFileLock)               
             
        def isNodeAntsRegistered (self, AquireFileLock = True) :
            if  self._ThreadLoading :
                return self._AntsLog is not None 
            else:
                return super(PesscaraTreeWidgetNode, self).isNodeAntsRegistered (AquireFileLock = AquireFileLock)               
                 
        def setANTSRegisteredLog (self, log, AquireFileLock = True) :
            if  self._ThreadLoading :
                self._AntsLog = log.copy (None)
            else:
                super(PesscaraTreeWidgetNode, self).setANTSRegisteredLog (log, AquireFileLock = AquireFileLock)                           
        
        def getAntsRegisteredDate (self,AquireFileLock = True) :
            if  self._ThreadLoading :
                if self._AntsLog is None :
                    return None
                year, month, day = self._AntsLog.getLastRegisterDate ()
                return str (month) + "/" + str (day) + "/" + str (year)
            else:
                return super(PesscaraTreeWidgetNode, self).getAntsRegisteredDate (AquireFileLock = AquireFileLock)                           
                        
        def getAntsRegisteredDateTuple (self, AquireFileLock = True) :
            if  self._ThreadLoading :
                if self._AntsLog is None :
                    return (None,None,None)
                year, month, day = self._AntsLog.getLastRegisterDate ()
                return (month,day,year)
            else:
                return super(PesscaraTreeWidgetNode, self).getAntsRegisteredDateTuple ()                           
        
        def setNodeROIFileLock  (self, nodeLock, AquireFileLock = True) :
            if  self._ThreadLoading :
                self._NodeLocked = nodeLock
            else:
                super(PesscaraTreeWidgetNode, self).setNodeROIFileLock (nodeLock, AquireFileLock = AquireFileLock)                           
         
        def getROIFileLock  (self, nodeLock, AquireFileLock = True) :
            if  self._ThreadLoading :
                return self._NodeLocked
            else:
                return super(PesscaraTreeWidgetNode, self).getROIFileLock (AquireFileLock = AquireFileLock)                           
        
        def _setCurrentUserPLockStatus (self, PLockStatus, FireTreeNodeChangedEvent = True, AquireFileLock = True) :
            if  self._ThreadLoading :
                self._CurrentUserPLock = PLockStatus
                self._setPLockStateTags (NodePLockedState = PLockStatus)                
            else:
                super(PesscaraTreeWidgetNode, self)._setCurrentUserPLockStatus (PLockStatus, FireTreeNodeChangedEvent = FireTreeNodeChangedEvent, AquireFileLock = AquireFileLock, NodePLockedState = PLockStatus)                           
            
                
        def setNodePersistentFileLock  (self, nodeLock, FireChildNodeChangeEvent= True, FireTreeNodeChangeEvent = True, AquireFileLock = True, UserGroupList = None) :
            if  self._ThreadLoading :
                self._PLock = nodeLock
                tree = self._tree
                if (tree is not None) :
                    PLockStatus = PersistentFSLock.getLockStatusForCurrentUser (nodeLock, tree._ProjectDataset.getProjectFileInterface ())               
                    self._setCurrentUserPLockStatus(PLockStatus, FireTreeNodeChangedEvent = False, AquireFileLock =AquireFileLock)                    
                self.callNodeChangedPLockStatus (AquireFileLock = AquireFileLock)                 
                self.fireTreeNodeChangedEvent ()
            else:
                super(PesscaraTreeWidgetNode, self).setNodePersistentFileLock (nodeLock, FireChildNodeChangeEvent = FireChildNodeChangeEvent, FireTreeNodeChangeEvent = FireTreeNodeChangeEvent, AquireFileLock = AquireFileLock, UserGroupList = UserGroupList)                           
   
        def getNodePersistentFileLock  (self, AquireFileLock = True) :
            if  self._ThreadLoading :
                return self._PLock
            else:
                return super(PesscaraTreeWidgetNode, self).getNodePersistentFileLock (AquireFileLock = AquireFileLock)                                                   
            
        def getCurrentUserPLockStatus (self, AquireFileLock = True) :
            if  self._ThreadLoading :
                return self._CurrentUserPLock
            else:
                return super(PesscaraTreeWidgetNode, self).getCurrentUserPLockStatus (AquireFileLock = AquireFileLock)                                       

        def setDescription (self, description, AquireFileLock = True, FireTreeNodeChangedEvent = True, FireTreeNodeDescriptionChangeEvent = True):
            if  self._ThreadLoading :
                if self._Description != description :
                    self._Description = description
                    if FireTreeNodeDescriptionChangeEvent :
                        self.fireTreeNodeDescriptionChangedEvent (AquireFileLock)
                    if FireTreeNodeChangedEvent :
                        self.fireTreeNodeChangedEvent ()
                        return False
                    return True
                return False
            else:
                return super(PesscaraTreeWidgetNode, self).setDescription (description, AquireFileLock = AquireFileLock, FireTreeNodeChangedEvent =FireTreeNodeChangedEvent, FireTreeNodeDescriptionChangeEvent = FireTreeNodeDescriptionChangeEvent)           
                   
        def getDescription (self, AquireFileLock = True):
            if  self._ThreadLoading :
                return self._Description
            else:
                return super(PesscaraTreeWidgetNode, self).getDescription (AquireFileLock = AquireFileLock)                                       
            
        def setROIDatasetIndicator (self, val, FireChildNodeChangedEvent = True, FireTreeNodeChangedEvent = True, AquireFileLock = True) :         
            if  self._ThreadLoading :
                self._HasData = val
                self.callChildNodeROIIndicatorChanged (val, AquireFileLock = AquireFileLock)
                self.fireTreeNodeChangedEvent ()
            else:
                super(PesscaraTreeWidgetNode, self).setROIDatasetIndicator (val, FireChildNodeChangedEvent = FireChildNodeChangedEvent, FireTreeNodeChangedEvent = FireTreeNodeChangedEvent, AquireFileLock = AquireFileLock)
                                   
        def getROIDatasetIndicator (self, AquireFileLock = True) :
            if  self._ThreadLoading :
                return self._HasData
            else:
                return super(PesscaraTreeWidgetNode, self).getROIDatasetIndicator (AquireFileLock = AquireFileLock)           
                     
          
        
        
                
        

                
class PesscaraDataLoader (QThread):
        
        #updateNodeData = pyqtSignal(['PyQt_PyObject','PyQt_PyObject','PyQt_PyObject'])        
        
        def __init__(self, TreeNodeList, globalSeriesWithTagDictionary, ROIDatasetInterface, globalSeriesDescriptionDictionary, LoadEntireTree = False, ProjectDataset = None):            
            QThread.__init__(self, None)            
            self._ProjectDataset = ProjectDataset
            self._TreeNodeList = TreeNodeList
            self.exiting = False                                
            self._globalSeriesWithTagDictionary = globalSeriesWithTagDictionary
            self._ROIDatasetInterface = ROIDatasetInterface
            self._globalSeriesDescription = globalSeriesDescriptionDictionary
            
            #connectNodeDataUpdate = True
            #if (len (self._TreeNodeList) > 0) :
            #    first = self._TreeNodeList[0]
            #    if first.isExam () and LoadEntireTree :
            #      connectNodeDataUpdate = False  
             
            #if (connectNodeDataUpdate) :
            #    for nodes in TreeNodeList:
            #        self.updateNodeData.connect (nodes.nodeDataUpdated)
            self._threadResults = []
            self._LoadEntireTree = LoadEntireTree
        
        def ProcessThreadResultsSync (self, UpdateTree = True) :
            if (self._threadResults != None) :
                for node, childlist, childloadingThread in self._threadResults :
                    node.setChildernLst (childlist)        
                    if UpdateTree :
                        node.fireTreeNodeChangedEvent ()                        
                    node._childLoadingThread = childloadingThread
                self._threadResults = None
                                         
        
        def run(self):
            if (len (self._TreeNodeList) > 0) :
                first = self._TreeNodeList[0]
                if first.isExam () :
                    #print ("loading exam")
                    subjectCode = first.getSubject().code
                    combinedSeriesList = first.getPesscaraInterace ().getAllSubjectsExamsSeriesQuery (subjectCode)                    
                    if (self.exiting) :
                            return 
                                                
                    examDict = {}      
                    if (self._ROIDatasetInterface.isPesscaraInterface ()) :    
                        examTagDic = {}                       
                    else:
                        examTagDic = None
                        
                    for series in combinedSeriesList :                        
                        if (series.exam_code not in examDict) :
                            examDict[series.exam_code] = []
                        examDict[series.exam_code].append(series)
                        
                        if (self._ROIDatasetInterface.isPesscaraInterface ()) :    
                            if (series.code in self._globalSeriesWithTagDictionary) :
                                if (series.exam_code not in examTagDic) :
                                    examTagDic[series.exam_code] = []
                                examTagDic[series.exam_code].append (series.code)
                                            
                        if (self.exiting) :
                            return 
                                                        
                    for node in  self._TreeNodeList :
                        if (self.exiting) :
                            return        
                        #print ("processing node")
                        examSeriesTreeNodeList = []
                        examCode = node.getExam ().code
                        if (examCode not in examDict) :
                            print ("Error Exam Code " + examCode  + " unexpectedly not found.")
                            print ("Subject Name " + node.getSubject ().name)
                            print ("Exam Name " + node.getExam ().name)
                            print (examDict.keys ())
                        else:
                            examSeriesList = examDict[examCode]                            
                            if (self._ROIDatasetInterface.isPesscaraInterface ()) :    
                                if (examCode in examTagDic) :
                                    examSeriesTagList = examTagDic[examCode]                                                                             
                                else:
                                    examSeriesTagList = []
                                try:
                                    currentProjectDescriptionList = self._ProjectDataset.getScanPhaseList ()
                                except:
                                    print ("Could not get projects description list")  # calling it scanphase description is legacy.
                                    
                                for series in examSeriesList :                                    
                                    seriesTreeNode = PesscaraTreeWidgetNode (node.getPesscaraInterace(), node._tree, node, examSeriesTagList, len (examSeriesList), node.getSubject (), node.getExam (), series, Siblings=examSeriesTreeNodeList)
                                    
                                    treePath = seriesTreeNode.getTreePath ()
                                    DatasetCache = self._ProjectDataset.getProjectFileInterface().getProjectDatasetCache (treePath)
                                    if DatasetCache is not None and RCC_NiftiFileSystemDataInterface.isValidCache (DatasetCache) :                                        
                                        contoursExist = DatasetCache["ContoursExist"]
                                        #antsLog  = DatasetCache["AntsLog"]
                                        scanPhase  = DatasetCache["ScanPhase"]
                                        tags  = DatasetCache["Tags"]
                                        fileobj  = DatasetCache["FileObj"]                                
                    
                                        if (tags is not None) :
                                            node.setTags (tags)                                            
                                            if (tags.hasInternalTag ("ML_DatasetDescription")):
                                                node.setMLPredictedScanPhaseTxt (tags.getInternalTag ("ML_DatasetDescription"))                            
                                        if (contoursExist or scanPhase != "Unknown"):
                                            node.setROIDatasetIndicator (True)
                                            node.setScanPhaseTxt (scanPhase)                                                                                            
                                            if (fileobj is not None) :
                                                if (fileobj.hasParameter ("PESSCARA_SubjectCode") and fileobj.hasParameter ("PESSCARA_ExamCode") and fileobj.hasParameter ("PESSCARA_SeriesCode") and fileobj.hasFileObject ("RCC_PesscaraInterface")) : 
                                                    pescaraInterfaceDescription = fileobj.getFileObject ("RCC_PesscaraInterface")
                                                    node.setPesscaraDataSetSource (pescaraInterfaceDescription, fileobj.getParameter ("PESSCARA_SubjectCode"), fileobj.getParameter ("PESSCARA_ExamCode"), fileobj.getParameter ("PESSCARA_SeriesCode"))                                                                                    
                                                        
                                    
                                    if (series.code in self._globalSeriesDescription) :
                                        scanDescriptionTag = "RILC_"+ self._ProjectDataset.getDescriptionContext () +"_Description"
                                        if scanDescriptionTag in series.tags :                                            
                                            descriptonTplStr = series.tags[scanDescriptionTag]                                            
                                            descriptonMap = json.loads (descriptonTplStr)
                                            scanPhase = descriptonMap["D"]
                                            seriesTreeNode.setScanPhaseTxt (scanPhase, UpdateTree = False)                                              
                                                                                        
                                            try :
                                                if (scanPhase not in currentProjectDescriptionList) :
                                                    currentProjectDescriptionList.append (scanPhase)
                                            except:
                                                print ("Could not added description to project description list")
                                        
                                            # Add Tags Saved In Tactic
                                            try :
                                                DatasetUIHeaderManager = self._ProjectDataset.getNIfTIDatasetInterface().getUIHeader ()
                                                for HeaderObjIndex in range (DatasetUIHeaderManager.headerObjectCount ()) :
                                                    if (DatasetUIHeaderManager.getHeaderType (HeaderObjIndex) == "Tag") :
                                                            tagIdentifier = DatasetUIHeaderManager.getHeaderIdentifier (HeaderObjIndex)
                                                            tagfound = self._ProjectDataset.getProjectTagManger ().getTagByIdentifer (tagIdentifier)
                                                            if (tagfound is not None) :
                                                                if tagfound.storeTagInTatic () :
                                                                    name = tagfound.getName ()
                                                                    if (tagfound.shouldMangleTagNameWithTaticContext ()) :
                                                                        tacticContext = "RILC_"+tagfound.getTagTacticContext (self._ProjectDataset.getProjectName()) + "_"
                                                                        tactic_tag = tacticContext  + name
                                                                        mangleTagNameWithTaticContext = True
                                                                    else:
                                                                        tactic_tag =  name
                                                                        mangleTagNameWithTaticContext = False
                                                                    if tactic_tag in series.tags :
                                                                        tagvalue = series.tags[tactic_tag]
                                                                        storeInTactic = True
                                                                        internalTag = tagfound.isInternalTag ()
                                                                        Override_Tactic_Context = tagfound.getTagTacticContext (self._ProjectDataset.getProjectName())
                                                                        seriesTreeNode.getTags ().createTagFromTacticValues (name, tagvalue, internalTag, storeInTactic, mangleTagNameWithTaticContext, Override_Tactic_Context = Override_Tactic_Context) 
                                            except:
                                                print ("ERROR: A error occured loading tags from tactic.")
                                            
                                    examSeriesTreeNodeList.append (seriesTreeNode) 
                                try :
                                    if (len (currentProjectDescriptionList) > len (self._ProjectDataset.getScanPhaseList ())) :
                                        self._ProjectDataset.setScanPhaseList (currentProjectDescriptionList)
                                except:
                                    print ("A error occured updating the project's description list.")
                                    
                                if (not self._LoadEntireTree) :
                                    node.nodeDataUpdated (examSeriesTreeNodeList, None)                                    
                                self._threadResults.append ((node, examSeriesTreeNodeList, None))
                            else:
                                # Loading from ROI Data disk requires loading tags and associated data from filesystem 
                                examSeriesTagList = None                                
                                for series in examSeriesList :
                                    seriesTreeNode = PesscaraTreeWidgetNode (node.getPesscaraInterace(), node._tree, node, examSeriesTagList, len (examSeriesList), node.getSubject (), node.getExam (), series, Siblings=examSeriesTreeNodeList)                                    
                                    examSeriesTreeNodeList.append (seriesTreeNode)                                                             
                                    
                                    fileHandle = self._ROIDatasetInterface.getROIDataForDatasetPath (seriesTreeNode, IgnoreLock = True, Verbose = False)                                    
                                    if (fileHandle is not None) :
                                        contoursExist, fileheader = ROIDictionary.contoursExistForDataSet_ReturnHeader (fileHandle.getPath ())
                                        scanPhase = ROIDictionary.getScanPhaseFromHeader (fileheader)
                                        
                                        tags = DatasetTagManager ()                                        
                                        tags.loadTagDatasetFromFile (fileHandle, FileHeader = fileheader)
                                        fileHandle.updateTagsFromDataSource (tags, RemoveExistingInternalTags = True)
                                        seriesTreeNode.setTags (tags)
                                        if (tags.hasInternalTag ("ML_DatasetDescription")):
                                            seriesTreeNode.setMLPredictedScanPhaseTxt (tags.getInternalTag ("ML_DatasetDescription"))
                                            
                                        if (contoursExist or scanPhase != "Unknown"):                               
                                            seriesTreeNode.setROIDatasetIndicator (True)     
                                            seriesTreeNode.setScanPhaseTxt (scanPhase, UpdateTree = False)
                                        fileHandle.closeUnsaved ()
                                    del fileHandle
                                if (not self._LoadEntireTree) :
                                    node.nodeDataUpdated (examSeriesTreeNodeList, None)                                  
                                self._threadResults.append ((node, examSeriesTreeNodeList, None))
                            #print ((node.getNodeIDTupl (), examSeriesTreeNodeList))
                else:  #Patients
                    for node in  self._TreeNodeList :
                        if (self.exiting) :
                            return                
                        subject, exam, series = node._NodeObjTupl
                        childernLst = subject.getExams ()                            
                        testTag =  node.getPesscaraContex ()
                        if (self._globalSeriesWithTagDictionary != None) :
                            codeList = []
                            if node.getNodeROIDatasetIndicator () :  
                                codeList = node.getPesscaraInterace().getExamsWithTag (testTag, subject.code )
                        else:
                            codeList = None
                            
                        ReturnList = []                 
                        for row, child in enumerate(childernLst) :                
                            exam =  child                
                            ReturnList.append (PesscaraTreeWidgetNode (node.getPesscaraInterace(), node._tree, node, codeList, row, subject, exam, series, Siblings=ReturnList))                                                        
                        
                        childLoadingThread = None
                        if (self._LoadEntireTree) :
                            #print (subject.name)
                            #print ("start loading exam tree child tree")
                            childLoadingThread = PesscaraDataLoader (ReturnList, self._globalSeriesWithTagDictionary, self._ROIDatasetInterface, self._globalSeriesDescription, LoadEntireTree = True, ProjectDataset = self._ProjectDataset)
                            #print ("starting thread")
                            childLoadingThread.start ()
                            childLoadingThread.wait ()
                            #print ("processing thread results")
                            childLoadingThread.ProcessThreadResultsSync (UpdateTree = False)
                            #print ("end loading exam tree child tree")
                        node.nodeDataUpdated (ReturnList, childLoadingThread)                        
                        self._threadResults.append ((node, ReturnList, childLoadingThread))
                    
               
                                        
                                
                                
class RCC_NiftiPesscaraDataInterface  (QtCore.QAbstractItemModel):
               
    #NIfTI_DatasetInterface = RCC_NiftiPesscaraDataInterface (self._ROI_DatasetInterface, NewPesscaraSession =(pesscaraNiftiInterface, pesscaraNiftiSubjectList))    
    #def __init__(self, PesscaraInterface, SubjectLst, parent = None, *args):                
    def __init__(self, ROIDatasetInterface, FObject = None, NewPesscaraSession = None, parent = None,  ProjectDataset = None, UIFObjectHeader = None, SplashScreen = None, *args):                        
        QtCore.QAbstractItemModel.__init__(self, parent, *args)
        if (UIFObjectHeader is None) :
            self._DatasetUIHeader = DatasetUIHeader (FObject=FObject)   
        else:
            self._DatasetUIHeader = DatasetUIHeader (FObject=UIFObjectHeader)   
        self._treeViewWidget = None
        self._ProjectDataset = ProjectDataset
        self.rootThread = None
        self._ROIDatasetInterface = ROIDatasetInterface
        self._projectName = self._ROIDatasetInterface.getProjectName ()
        
       
                
        if (NewPesscaraSession != None)        :
            PesscaraInterface, SubjectLst = NewPesscaraSession        
        else:                
            PesscaraInterface = RCC_PesscaraInterface (FObject = FObject.getFileObject ("RCC_PesscaraInterface"))
            subjectObj = FObject.getFileObject ("SubjectList")
            count = subjectObj.getParameter ("SubjectCount")
            SubjectLst = []
            if (count > 0) :
                if SplashScreen is None :
                    progdialog = RCC_SplashScreenDlg (ProjectDataset , parent) 
                else:
                    progdialog = SplashScreen
                progdialog.setMaximum (count)
                for index in range (count) :                    
                    subjectCode = subjectObj.getParameter ("S_" + str (index))
                    subject = PesscaraInterface.getSubjectFromSubjectCode (subjectCode)
                    SubjectLst.append (subject)
                    progdialog.setValue (index)  
                    progdialog.processSystemEvents ()
                    if  (progdialog.wasCanceled ()) :
                        break
                progdialog.close() 
                del progdialog
                
        self._pesscaraInterface = PesscaraInterface                    
        self._rootMemory = []             
        self._finalizer = None
        if PythonVersionTest.IsPython3 () :
            self._finalizer = weakref.finalize (self, RCC_NiftiPesscaraDataInterface._finalizeObj, self.rootThread)
            
        if (ROIDatasetInterface.isPesscaraInterface ()) :    
            
            self._globalSeriesWithDescriptionDictionary = {}    
            
            seriesList = PesscaraInterface.getAllSeriesWithTag ("RILC_" + self._ProjectDataset.getDescriptionContext () +"_Description")         
            for series in seriesList :
                self._globalSeriesWithDescriptionDictionary[series.code] = True                    
            
            seriesList = PesscaraInterface.getAllSeriesWithTag (self.getPesscaraContex ())                 
            self._globalSeriesWithTagDictionary = {}
            seriesList = PesscaraInterface.getAllSeriesWithTag (self.getPesscaraContex ())         
            for series in seriesList :
                self._globalSeriesWithTagDictionary[series.code] = True        
            subjectCodeList = PesscaraInterface.getSubjectsWithTag (self.getPesscaraContex ())        
            for row, subjects in enumerate (SubjectLst) :            
                self._rootMemory.append (PesscaraTreeWidgetNode(PesscaraInterface, self, None, subjectCodeList, row, subjects, None, None))                     
            self.rootThread = PesscaraDataLoader (self._rootMemory, self._globalSeriesWithTagDictionary, self._ROIDatasetInterface, self._globalSeriesWithDescriptionDictionary, ProjectDataset = self._ProjectDataset)
            self.rootThread.start ()                            
        else: # loading data from file system
            self._globalSeriesWithTagDictionary = None
            self._globalSeriesWithDescriptionDictionary = None
            subjectCodeList = None
            for row, subjects in enumerate (SubjectLst) :            
                self._rootMemory.append (PesscaraTreeWidgetNode(PesscaraInterface, self, None, subjectCodeList, row, subjects, None, None))                     
            self.rootThread = PesscaraDataLoader (self._rootMemory, self._globalSeriesWithTagDictionary, self._ROIDatasetInterface, self._globalSeriesWithDescriptionDictionary, LoadEntireTree = True, ProjectDataset = self._ProjectDataset)
            self.rootThread.start ()                            
    
    if PythonVersionTest.IsPython2 () :
        def __del__ (self) :
            RCC_NiftiPesscaraDataInterface._finalizeObj (self.rootThread)
   
    def getPesscaraInterface (self) :
        return self._pesscaraInterface
         
    def setTreeViewWidget (self, widget) :
        self._treeViewWidget = widget
        
    def getTreeViewWidget (self) :
        return self._treeViewWidget
    
    def getFileSysetmEventHandler (self) :
        return None
    
    def treeNodeChanged (self, node) :
        self.layoutAboutToBeChanged.emit ()            
        nodechanged = self.createIndex(node.getRow (), 0, node)                
        self.dataChanged.emit (nodechanged, nodechanged)
        self.changePersistentIndex(nodechanged, nodechanged)
        self.layoutChanged.emit ()           
            
    def _removeFinalizer (self) :
            if self._finalizer is not None :            
                self._finalizer.detach()
                self._finalizer = None 
                
    def findFileInDataset (self, path) :
        return None 
    
    def rebuildTree (self) :
        self.layoutAboutToBeChanged.emit ()  
        self.layoutChanged.emit ()   
        
    @staticmethod
    def _finalizeObj (rootThread):
        if (rootThread is not None) :
            rootThread.exiting = True       
    
    def close (self) :
        RCC_NiftiPesscaraDataInterface._finalizeObj (self.rootThread)
        self._removeFinalizer ()
        
    def getInterfaceName (self) :
        return "RCC_NiftiPesscaraDataInterface"
    
    def setRegistrationDayFilter (self, datetuple = (None,None,None)):
       return # do nothing, only supported for filesystem interface
       
    def setTempDir (self, path) : 
        self._pesscaraInterface.setTempDir (path)
   
    def loadAllChildTreeNodes (self, node) :
        self.rootThread.wait ()         
        self.rootThread.ProcessThreadResultsSync ()            
        nodeSubjectCode  = node.getSubject().code
        for subject in self._rootMemory :                                    
            if (subject.getSubject ().code == nodeSubjectCode ) :                                        
                if (subject._childLoadingThread == None) :                        
                    examList = subject.getChildernLst ()                         
                    subject._childLoadingThread = PesscaraDataLoader (examList, self._globalSeriesWithTagDictionary, self._ROIDatasetInterface, self._globalSeriesWithDescriptionDictionary, ProjectDataset = self._ProjectDataset)
                    subject._childLoadingThread.start ()
                subject._childLoadingThread.wait ()       
                subject._childLoadingThread.ProcessThreadResultsSync ()                    
                break
         
    def getAllTreeNodes(self)    :
        TreeNodeList = []
        self.rootThread.wait ()  
        self.rootThread.ProcessThreadResultsSync ()                              
        for subject in self._rootMemory :                                    
                if (subject._childLoadingThread == None) :
                   examList = subject.getChildernLst () 
                   subject._childLoadingThread = PesscaraDataLoader (examList, self._globalSeriesWithTagDictionary, self._ROIDatasetInterface, self._globalSeriesWithDescriptionDictionary, ProjectDataset = self._ProjectDataset)
                   subject._childLoadingThread.start ()
                subject._childLoadingThread.wait ()                
                subject._childLoadingThread.ProcessThreadResultsSync ()                              
                subjectExamList = []
                for exams in subject.getChildernLst () :
                    
                    examsSeriesList = []
                    for series in exams.getChildernLst () :                                                    
                        examsSeriesList.append ((series, []))
                                                        
                    subjectExamList.append ((exams, examsSeriesList))
                    
                TreeNodeList.append ((subject, subjectExamList))
        return TreeNodeList               
 
  
        
    def getNiftiTreeNodeList (self, Memory = None, AllFiles = False) :        
        TreeNodeList = set ()
        self.rootThread.wait ()            
        self.rootThread.ProcessThreadResultsSync ()  
        if Memory is None :
            Memory = self._rootMemory
        for subject in Memory :                         
            if (AllFiles or subject.getNodeROIDatasetIndicator ()) :
                print ("Processing: " + subject.getText ())
                if (subject._childLoadingThread is None) :
                   examList = subject.getChildernLst () 
                   subject._childLoadingThread = PesscaraDataLoader (examList, self._globalSeriesWithTagDictionary, self._ROIDatasetInterface, self._globalSeriesWithDescriptionDictionary, ProjectDataset = self._ProjectDataset)
                   subject._childLoadingThread.start ()
                subject._childLoadingThread.wait ()    
                subject._childLoadingThread.ProcessThreadResultsSync ()           
                for exams in subject.getChildernLst () :                    
                    if (AllFiles or exams.getNodeROIDatasetIndicator ()) :                        
                        for series in exams.getChildernLst () :                        
                            if (AllFiles or series.getNodeROIDatasetIndicator ()) :
                                TreeNodeList.add (series)
                                print ("  Parent: " +subject.getText () + "   Exam: " + exams.getText () + "   Series: " + series.getText ())
        return list (TreeNodeList)       
        
    def getPesscaraContex (self) :
            return "RILC_" + self.getProjectName ()
            
    def getProjectName (self) : 
        return  self._projectName
        
    def getFileObject (self) :
        fobj = FileObject ("RCC_NiftiPesscaraDataInterface")        
        fobj.addInnerFileObject (self._pesscaraInterface.getFileObject ())
        subjectObj = FileObject ("SubjectList")
        subjectObj.setParameter ("SubjectCount", len (self._rootMemory))
        for row, pesscaraTreeWidgetNode in enumerate (self._rootMemory) :
            subject = pesscaraTreeWidgetNode.getSubject ()
            subjectObj.setParameter ("S_" + str (row), subject.code)
        fobj.addInnerFileObject (subjectObj)
        fobj.addInnerFileObject (self._DatasetUIHeader.getFileObject ())
        return (fobj)

                
    def index (self, row, column, parent):
        if parent is None or not parent.isValid():            
            if row >= 0 and row < len (self._rootMemory) :
                return self.createIndex(row, column, self._rootMemory[row])            
            else:
                return QModelIndex()
        else :
            parentNode = parent.internalPointer ()        
            childNode = parentNode.getChild (row)
            if (childNode is not None) :
                return self.createIndex(row, column, childNode)            
            else:
                return QModelIndex()
                    
    def data(self, index, role):
        if not index.isValid():
            return None
        node = index.internalPointer()
        if role == QtCore.Qt.DisplayRole :
            if self._DatasetUIHeader.getVisibleHeaderType (index.column()) == "Dataset" :
                if node.textUndefined () :            
                    if (node.isSubject ()):                                            
                        SubjectTxt =  str(node.getSubjectID ())
                        lName = str(node.getSubjectName ())
                        if (lName is not None and lName != "") :
                            lName = node.getSubjectName ().lower()
                            if (lName != "unknown") :
                                if (lName != SubjectTxt.lower ()):
                                    SubjectTxt += " (name: %s)" % node.getSubjectName ()
                        node.setText(SubjectTxt)
                    elif (node.isExam ()):                    
                        node.setText (node.getExamName ())
                    else:                    
                        node.setText (node.getSeriesName ())
                return node.getText ()
            elif (self._DatasetUIHeader.getVisibleHeaderType (index.column()) == "Description") :
                return node.getScanPhaseTxt ()
            elif (self._DatasetUIHeader.getVisibleHeaderType (index.column()) == "Tag") :
                
                #if tag found return it!
                tagidentifer  = self._DatasetUIHeader.getVisibleHeaderIdentifierByIndex (index.column())                

                if self._ProjectDataset is not None :                
                    datasetTagManager = self._ProjectDataset.getProjectTagManger ()                
                    if datasetTagManager is not None and datasetTagManager.getTagByIdentifer (tagidentifer) is None :
                        return ""                
                
                tagtextfound = node.getUICacheTag (tagidentifer)   
                if (tagtextfound is not None) :
                    return tagtextfound
                
                #if loading data from pesscara and not the filesystem lazy load dataset tags otherwise dataset tags loaded when data as the data is accessed
                if (self._ROIDatasetInterface.isPesscaraInterface ()) :    
                    series = node.getSeries ()
                    if (series is not None) :
                        if (len (tagidentifer) == 4) :
                            Name, internalTag, storeInTactic, mangleTagNameWithTaticContext = tagidentifer
                            if storeInTactic :
                                tagToLoad = self.getTags ().getTagByIdentifer (tagidentifer)
                                if tagToLoad is not None :
                                    tagContext = tagToLoad.getTagTacticContext (self.getProjectName())
                                else:
                                    tagContext = self.getProjectName()
                                if mangleTagNameWithTaticContext :
                                    SearchName = "RILC_" + tagContext + "_" + Name
                                if SearchName in series.tags :
                                    TagValue = series.tags[SearchName]                                
                                    node.getTags ().createTagFromTacticValues (Name, TagValue, internalTag, storeInTactic, mangleTagNameWithTaticContext, Override_Tactic_Context = tagContext)                        
                                    tagfound = node.getTags().getTagByIdentifer (tagidentifer)
                                    if (tagfound is not None) :
                                        return tagfound.getValue ()
                
                return ""
                                    
            
        elif role == QtCore.Qt.ForegroundRole :
            if (self._DatasetUIHeader.getVisibleHeaderType (index.column()) == "Description" and node.isScanPhasePredicted ()):
                return QtGui.QColor (255,0,0)
            if (node.isSelected ()) :
                return QtGui.QColor (15,130,255)
            if (node.isDatasetOpen ()) :
                return QtGui.QColor (12,63,119)            
            #if not node.hasOnlySingleChild () :
            #    return  QtGui.QColor (139,142,142)
            return  QtGui.QColor (0,0,0)
        elif role == QtCore.Qt.FontRole :
            if (self._DatasetUIHeader.getVisibleHeaderType (index.column()) == "Dataset") :
                font = QtGui.QFont ()
                font.setBold (node.getNodeROIDatasetIndicator ())
                return  font
            else:
                font = QtGui.QFont ()
                font.setBold (False)
                return  font
        else:            
            return None       
    
    def doesTreeContainANTSRegisteredDataset (self) :
        return False
    
    def hasIndex (self, row, column, parent) :
        if (row < 0) :
            return False
        if not parent.isValid():
            return row < len (self._rootMemory)
        parentNode = parent.internalPointer ()        
        if (parentNode.isSeries ()):                                
            return False
        elif (parentNode.isSubject ()):                                
            count = parentNode.getChildCount ()
            return (row < count) 
        else: #parent node is exam
            parentSeries = parentNode.getParent()
            if (parentSeries._childLoadingThread is None) :
                examList = parentSeries.getChildernLst () 
                parentSeries._childLoadingThread = PesscaraDataLoader (examList, self._globalSeriesWithTagDictionary, self._ROIDatasetInterface, self._globalSeriesWithDescriptionDictionary, ProjectDataset = self._ProjectDataset)
                parentSeries._childLoadingThread.start ()
            count = parentNode.getChildCount ()
            return (row < count) 
    
    def rowCount(self, parent):
      if not parent.isValid():
           return len (self._rootMemory)
      else:
          parentNode = parent.internalPointer ()
          if (parentNode.isSeries ()):                                                
             return 0
          elif (parentNode.isSubject ()):                                                
              return parentNode.getChildCount ()
          else: # exam
             parentSeries = parentNode.getParent()
             if (parentSeries._childLoadingThread is None) : 
                examList = parentSeries.getChildernLst () # get list of subjects Exams
                parentSeries._childLoadingThread = PesscaraDataLoader (examList, self._globalSeriesWithTagDictionary, self._ROIDatasetInterface, self._globalSeriesWithDescriptionDictionary, ProjectDataset = self._ProjectDataset)
                parentSeries._childLoadingThread.start ()                
             return parentNode.getChildCount () 
    
    def parent (self, index) :
        if not index.isValid():
            return QModelIndex()
        node =  index.internalPointer()
        if (node is None or not node.hasParent ()) :
            return QModelIndex()
        else:
            return self.createIndex(node.getParentRow (), 0, node.getParent ())                
            
    def flags (self, index) :
        return  QtCore.Qt.ItemIsEnabled |  QtCore.Qt.ItemIsSelectable
        
    def columnCount(self, parent):
        return self._DatasetUIHeader.headerVisibleObjectCount ()
    
    def headerData (self, section, orientation, role):
        if role != QtCore.Qt.DisplayRole  :
            return(None)
        if (orientation == QtCore.Qt.Horizontal):
            return self._DatasetUIHeader.getVisibleHeaderName (section)            
        else:
            return str (section)
    
    def setUIHeader (self, newHeader) :
            self._DatasetUIHeader = newHeader            
            self.headerDataChanged.emit (QtCore.Qt.Horizontal, 0, self._DatasetUIHeader.headerVisibleObjectCount () -1)
    
    def getUIHeader (self) :
        return self._DatasetUIHeader
    
    def getNiftiDatasetRootPath (self)  :       
        return ("Pesscara:")
    
    #code for matching a file path list with the tree
    def _getHeadNode (self, rootNodelist, fileList) :
        index = 0
        rootFileList = []
        for node in rootNodelist :
            rootFileList.append (node.getFilename ())        
        while (index < len (fileList)) :
            if (fileList[index] in rootFileList) :
                nodeindex = rootFileList.index (fileList[index])
                return (rootNodelist[nodeindex], index)
            else:
                index += 1
        return (None, None)
        
    def _getChildTreeNode (self, treeNode, fileName) :        
        for node in treeNode.getChildernLst () :
            if fileName == node.getFilename () :
                return node
        return None            
    
    
    def getLeafNIfTINodes (self, treeNode = None):
        if treeNode is None :
            returnLst = []
            for node in self._rootMemory :
                returnLst += self.getLeafNIfTINodes (node) 
            return returnLst
        else:
            childernLst = treeNode.getChildernLst ()
            if len (childernLst) == 0 :        
                try:
                    fname = treeNode.getFilename ()
                    lowerfilename = fname.lower ()                
                    if lowerfilename.endswith (".nii.gz") or lowerfilename.endswith (".nii") :
                        return [(treeNode)]                
                except:
                    return []            
            returnLst = []
            for node in childernLst :
                returnLst += self.getLeafNIfTINodes (node)
            return returnLst
        
    def _getChildTreeNodesWithNiftiFiles (self, treeNode):
        returnLst = [] 
        for node in treeNode.getChildernLst () :
            if node.getFilename ().lower ().endswith (".nii.gz") or node.getFilename ().lower().endswith (".nii"):
                returnLst.append (node)
        return returnLst
    
    def _treeNodeHasParentIn (self, node, parentList) :
       while node is not None :
           if node in parentList :
               return True
           node = node.getParent ()
       return False
           
    def getTreeNodeSubset (self, TreeNodeList, subsetList) :
       if subsetList is None :
           return TreeNodeList
       else:
           selection = []
           for treeNode in TreeNodeList :
               if self._treeNodeHasParentIn (treeNode, subsetList) :
                   selection.append (treeNode)
           return selection
    
    
    def getNiftiTreeNodeListForFileList (self, fileList, filename, LeafFileCache = None) :             
        if (len (fileList) == 0) :
            treeNode = self._rootMemory
        else:
            root =  self._rootMemory
            while (len (root) == 1) :                
                if (root[0].isExam ()) :
                    parentNode = root[0]
                    parentSeries = parentNode.getParent()
                    if (parentSeries._childLoadingThread is None) : 
                        examList = parentSeries.getChildernLst () # get list of subjects Exams
                        parentSeries._childLoadingThread = PesscaraDataLoader (examList, self._globalSeriesWithTagDictionary, self._ROIDatasetInterface, self._globalSeriesWithDescriptionDictionary, ProjectDataset = self._ProjectDataset)
                        parentSeries._childLoadingThread.start ()
                        parentSeries._childLoadingThread.join ()                    
                if (root[0].getChildCount () == 0):
                    break
                else:                
                    root = root[0].getChildernLst()                    
            
            treeNode, index = self._getHeadNode (root, fileList)
            if (treeNode is None) :
                #Tree node could not be found that matched the file list 
                #see if matching file names are in present in root.
                rootnodelist = []
                for node in root :
                    if node.getFilename ().lower ().endswith (".nii.gz") or node.getFilename ().lower ().endswith (".nii"):
                        if (filename in node.getFilename ()) :
                            rootnodelist.append (node)
                return rootnodelist
            index += 1
            while index < len (fileList) :                        
                if (treeNode.isExam ()):
                    parentNode = treeNode
                    parentSeries = parentNode.getParent()
                    if (parentSeries._childLoadingThread is None) : 
                        examList = parentSeries.getChildernLst () # get list of subjects Exams
                        parentSeries._childLoadingThread = PesscaraDataLoader (examList, self._globalSeriesWithTagDictionary, self._ROIDatasetInterface, self._globalSeriesWithDescriptionDictionary, ProjectDataset = self._ProjectDataset)
                        parentSeries._childLoadingThread.start ()
                        parentSeries._childLoadingThread.join ()                    
                        
                treeNode = self._getChildTreeNode (treeNode, fileList[index])
                index += 1
                if (treeNode is None) :
                   return []
            if index < len (fileList) :                        
                return []
        return self._getChildTreeNodesWithNiftiFiles (treeNode)

class ColumnSort :
    def __init__ (self) :
        self._primarySort = 1
        self._secondaryColumnSort = {}
        self._lastSecondaryColumnNameSet = None
    
    def setSecondaryColumnSort (self, columnName, direction, TagIdentifer = None) :
        if (self._lastSecondaryColumnNameSet is None or self._lastSecondaryColumnNameSet[0] != columnName or self._lastSecondaryColumnNameSet[1] != direction or (self._lastSecondaryColumnNameSet[0] == columnName and self._lastSecondaryColumnNameSet[2] != TagIdentifer)) :
            self._lastSecondaryColumnNameSet = (columnName, direction, TagIdentifer)
            if (columnName != "Tags") : 
                self._secondaryColumnSort[columnName] = direction
            else:
                if columnName not in self._secondaryColumnSort :
                    self._secondaryColumnSort[columnName] = {}
                self._secondaryColumnSort[columnName][TagIdentifer] = direction
            return True
        return False
    
    def getSecondaryColumnSortDirection (self, columnName, TagIdentifer = None) :
        if columnName not in self._secondaryColumnSort :
            return None
        if columnName != "Tags" :
            return self._secondaryColumnSort[columnName]
        if TagIdentifer not in self._secondaryColumnSort[columnName] :
            return None
        return self._secondaryColumnSort[columnName][TagIdentifer]
        
 
    def getSecondaryColumnSortColumnName (self) :
        if self._lastSecondaryColumnNameSet is None :
            return None
        return  self._lastSecondaryColumnNameSet[0]
    
    def getSecondaryColumnTagIdentifier (self) :
        if self._lastSecondaryColumnNameSet is None :
            return None
        return  self._lastSecondaryColumnNameSet[2]

    
    def getPrimaryColumnSortDirection (self) :
        return self._primarySort
     
    def setPrimaryColumnSortDirection (self, direction) :
        if (self._primarySort != direction) :
            self._primarySort = direction
            self._lastSecondaryColumnNameSet = None
            return True
        return False

class FileSystemTreeWidgetNode (AbstractTreeWidgetNode) :
        def __init__ (self, tree, parent, row, path, AquireFileLock = True, DirCache = None):       
            super(FileSystemTreeWidgetNode, self).__init__ ()
            self._TagsInitalized = False
            self._treePathFSCache = None
            self._datasetOpened = False
            self._DelayedCacheLoading = (None, None)
            self._tree = tree    
            if tree is not None :
                self.treeNodeChanged.connect (tree.treeNodeChanged)    
            self._initalized = False
            self._created  = False
            self._nodePath = path                    
            self._text = ""                    
            self._row = row            
            self._childrenList = []
            self._parent = parent            
            if parent is not None :
                self.childNodeChangedROIDatasetIndicator.connect (parent._childNodeROIIndicatorChanged)
                self.childNodeChangedPLockStatus.connect (parent._childNodeChangedPLockStatus)
                self.treeNodeDescriptionChanged.connect (parent._childNodeDescriptionChanged)                
            self.setText (self.getLongFileName (self.getFilename () ))            
            self._longFileNameAPI = None                
            if self._tree is not None :
                LongFileNameCache = self._tree.getLongFileNameCache ()
            else:
                LongFileNameCache = None
            if DirCache is not None :
                if LongFileNameAPI.hasLongFileNameDefinitions (self, CreateLongFileNamePathIfMissing = False, DirCache = DirCache):
                    self._longFileNameAPI = LongFileNameAPI (self, DirCache = DirCache, LongFileNameCache = LongFileNameCache)
                else:
                    if path in DirCache and not isinstance (DirCache[path], str) : #files are cached pointed to themselves.  directories are lists.
                        if parent is not None and parent.isLongFileAPILoaded () :
                            self._longFileNameAPI = LongFileNameAPI (self, DirCache = DirCache, CreateIfMissing = True, LongFileNameCache = LongFileNameCache)
            elif os.path.isdir (path):
                if LongFileNameAPI.hasLongFileNameDefinitions (self, CreateLongFileNamePathIfMissing = False):
                    self._longFileNameAPI = LongFileNameAPI (self, LongFileNameCache = LongFileNameCache)
                elif parent is not None and parent.isLongFileAPILoaded () :
                    self._longFileNameAPI = LongFileNameAPI (self, DirCache = DirCache, CreateIfMissing = True,  LongFileNameCache = LongFileNameCache)
                                    
            self._pesscaraDatasourceInterfaceFileObject = None # initalized to a fileobject
            self._pesscaraNodeID = None            
            self._selected = False
            self._ColumnSort = ColumnSort ()
            self._isVisible = True
        
        
        
        def _setTagsInitalized (self) :
            try :
                self._TagsInitalized = True
            except:
                pass
            return 
        
        def _initTagsIfNecessary (self, tags, AquireFileLock = True) :
            try :                                
                if not self._TagsInitalized :
                    try :
                        if AquireFileLock :
                            self._tree._ProjectDataset.getProjectFileInterface().aquireFileLock ()                                        
                        self.setTags(tags, FireTreeNodeChangedEvent = False, AquireFileLock = False)                 
                        return self.getTags(AquireFileLock = False) 
                    finally:
                        if AquireFileLock :
                            self._tree._ProjectDataset.getProjectFileInterface().releaseFileLock ()        
                return tags
                
            except:
                pass
            return tags

        
        def renameLongfileName (self, newName, progdialog, app) :
                            
            def _updateChildernNodes (childnode, app, progdialog, LongFileNameCache) :                
                if childnode is not None :                    
                    path = childnode.getPath ()
                    if progdialog is not None :
                        progdialog.setLabelText ("Updating child node: " + path)                 
                    if os.path.isdir (path) :                
                        childnode._longFileNameAPI = LongFileNameAPI (childnode, CreateIfMissing = True, LongFileNameCache = LongFileNameCache)                
                    for child in childnode.getChildernLst () :
                        app.processEvents ()
                        if not child.describesNiftiDataFile () :
                            _updateChildernNodes (child, app, progdialog, LongFileNameCache) 
                    
            if self._tree is not None :
                LongFileNameCache = self._tree.getLongFileNameCache()
            else:
                LongFileNameCache = None
                
            parent = self.getParent ()                    
            if parent is None :
                parentLongFileNameAPI = self._tree.getLongFileNameAPI (CreateIfMissing = True)
            else:
                parentLongFileNameAPI = parent.getLongFileNameAPI (CreateIfMissing = True)
                
            if not parentLongFileNameAPI.rename (self.getFilename (), newName) :
                return "File name allready exists"
            
            if progdialog is not None :
                progdialog.show ()
            app.processEvents ()
                                
            self.setText(newName)                        
            _updateChildernNodes (self, app, progdialog, LongFileNameCache)            
            if LongFileNameCache is not None :
                LongFileNameCache.UnloadLongFileNameCache ()
            self.fireTreeNodeChangedEvent ()
            return True
          
                
            
        def getDisplayText (self) :
            if self.textUndefined () :            
                self.setText(self.getLongFileName (self.getFilename ()))
            if self._tree is not None and self._tree.showShortFileName () :
                return self.getFilename ()
            return self.getText ()
                
        def isLongFileAPILoaded (self) :
            return self._longFileNameAPI is not None 
        
        def getLongFileNameAPI (self, CreateIfMissing = False) :
            if self._longFileNameAPI is None and CreateIfMissing : 
                if self._tree is not None :
                    LongFileNameCache = self._tree.getLongFileNameCache ()
                else:
                    LongFileNameCache = None
                self._longFileNameAPI = LongFileNameAPI (self, CreateIfMissing = True, LongFileNameCache = LongFileNameCache)
            return self._longFileNameAPI
        
        def getLongFileName (self, shortFileName) :
            if self.getParent () is not None :
                if self.getParent ().isLongFileAPILoaded () :
                    lfnapi = self.getParent ().getLongFileNameAPI ()
                else:
                    lfnapi = None
            else:
                lfnapi = self._tree.getLongFileNameAPI ()
            if lfnapi is not None :
                longfilename = lfnapi.getLongFileName (shortFileName)
                if longfilename is not None :
                    return longfilename
            return shortFileName
        
        def meetsAdvancedFilterOptions (self, option) :            
            if option == "EditiableImageFiles" or option == "ImageFilesAssignedToMe" :                            
                PLockStatus = self.getCurrentUserPLockStatus ()
                if "UserLocked" == PLockStatus :
                    return True                
                elif (PLockStatus == "OtherUserLocked" or option == "ImageFilesAssignedToMe"):        
                    return False                
                elif (PLockStatus == "UserGroupLocked"):        
                    return True
                else:
                    fileSharing = self._tree._ProjectDataset.getProjectFileInterface().getMultiUserFileSharingManager ()                    
                    if fileSharing is None :
                        return True
                    UserName = self._tree._ProjectDataset.getUserName ()
                    return fileSharing.canLockUnlockedFiles (UserName)
            else:
                return True
        
        def setVisible (self, setV, UpdateParent = True) :
            if setV == self._isVisible :
                return False  
            if not setV :
                self.setRow (-1)
            if not UpdateParent or self._tree is None:              
                self._isVisible = setV  
            else:
                self._tree.layoutAboutToBeChanged.emit ()                                   
                nodeAddList = []
                nodeRemoveList = []
                nodeChangeList = []                
                expandedNodeList = []                
                TreeViewWidget = self._tree.getTreeViewWidget ()
                child = self                
                while True :                                       
                    parent = child.getParent ()                                                
                    if parent is None :
                        childlist = self._tree.getRootMemory ()
                    else:                        
                        childlist = parent.getChildernLst ()                         
                    VRowCount = 0
                    for node in childlist :
                        if node.isVisible () :
                            VRowCount += 1
                    
                    if child._isVisible != setV  :
                        child._isVisible = setV 
                        
                    childNodesChanged = False                                            
                    NewVisRowCount = 0
                    for node in childlist :                                       
                        if node.isVisible () :    
                             oldRow = node.getRow () 
                             if oldRow != -1 :                    
                                nodeexpanded = TreeViewWidget.isExpanded (self.createIndex(oldRow, 0, node))
                             else:
                                nodeexpanded = False 
                             if node.setRow (NewVisRowCount) :
                                 if NewVisRowCount < VRowCount :
                                     nodeChangeList.append (node)                        
                                     childNodesChanged = True                                                                    
                                 expandedNodeList.append ((node, nodeexpanded))            
                             NewVisRowCount += 1                                                                     
                    if NewVisRowCount > VRowCount :
                        nodeAddList.append ((child, VRowCount, NewVisRowCount))
                        childNodesChanged = True
                    elif NewVisRowCount < VRowCount:
                        nodeRemoveList.append ((child, VRowCount, NewVisRowCount))
                        childNodesChanged = True
                                    
                    if parent is None :
                        break
                    if not childNodesChanged :
                        break
                    child = parent
                    if NewVisRowCount == 0 :
                        setV = False
                    else:
                        setV = True
                                                                
                for tpl in nodeAddList :                                
                    node,VRowCount, NewVisRowCount = tpl
                    start = VRowCount
                    end   = NewVisRowCount - 1
                    if node is not None :
                        nodechanged = self._tree.createIndex(node.getRow (), 0, node)                
                    else:
                        nodechanged = QModelIndex ()
                    self._tree.rowsAboutToBeInserted.emit (nodechanged, start, end)
                    self._tree.rowsInserted.emit (nodechanged, start, end)
        
                for tpl in nodeRemoveList :                                
                    node,VRowCount, NewVisRowCount = tpl
                    start = NewVisRowCount 
                    end   = VRowCount - 1
                    if node is not None :
                        nodechanged = self._tree.createIndex(node.getRow (), 0, node)
                    else:
                        nodechanged = QModelIndex ()
                    self._tree.rowsAboutToBeRemoved.emit (nodechanged, start, end)
                    self._tree.rowsRemoved.emit (nodechanged, start, end)
                    
                for node in nodeChangeList :                                
                    nodechanged = self._tree.createIndex(node.getRow (), 0, node)
                    self._tree.dataChanged.emit (nodechanged, nodechanged)
                    self._tree.changePersistentIndex(nodechanged, nodechanged)
                self._tree.layoutChanged.emit ()      
                for node_tuple in expandedNodeList :
                    node, expanded = node_tuple            
                    nodechanged = self.createIndex(node.getRow (), 0, node)
                    TreeViewWidget.setExpanded (nodechanged, expanded)
            return True    

        def isVisible (self) :
            return self._isVisible
        
        @staticmethod 
        def getNodeCacheData (treePath, dataset, UserUID, ProjectFileInterface, AquireFileLock = True) :
            roiDatasetPath, niftiDatasetPath, pathForMissingROI = dataset
            contoursExist = False
            scanPhase = "Unknown"
            antsLog = None
            tags = None
            fileobj = None        
            isLocked = False
            pLock = None
            if roiDatasetPath is None : 
                roiDatasetPath = pathForMissingROI
            tempROIDatasource = None
            
            try :
                if (roiDatasetPath is not None) :             
                        try:              
                            cachedResult = ProjFileInterface.loadDatasetCompressedCache (roiDatasetPath)
                            if cachedResult is not None and RCC_NiftiFileSystemDataInterface.isValidCache(cachedResult) :
                                cachedResult["NodeLocked"] = RCC_NiftiFileSystemDataInterface.getLockFileState (NonPersistentFSLock.getFileLockPathForROIFilePath (roiDatasetPath), UserUID)
                                cachedResult["PLock"] = PersistentFSLock.getCurrentPLockFromPath (PersistentFSLock.getPersistentLockPathForROIFilePath (roiDatasetPath))
                                cachedResult["PLockInit"] = True
                                #micro hack to fix bug in some caches which did not correctly store contour exists.
                                if not cachedResult["ContoursExist"] :
                                    cachedResult["ContoursExist"],_ = ROIDictionary.contoursExistForDataSet_ReturnHeader (roiDatasetPath)                  
                                ProjectFileInterface.setProjectDatasetTagCache (treePath, cachedResult, AquireFileLock = AquireFileLock)
                                return cachedResult
                        except:
                            pass
                        
                        if os.path.exists (roiDatasetPath) :
                            try :
                                tempROIDatasource = RCC_ROIDataset_FileSystemObject (roiDatasetPath, NIfTIPath = niftiDatasetPath, ProjectDataset = None, ProjectFileInterface = ProjectFileInterface, SilentIgnoreOnLockFail = True, Verbose = False) # ProjectData set is None, don't use file save queueing inside of processes.                                            
                            except:
                                tempROIDatasource = None
                            try:
                                contoursExist, fileheader = ROIDictionary.contoursExistForDataSet_ReturnHeader (roiDatasetPath)                               
                            except:
                                contoursExist = False
                                fileheader = None
                        else:
                            contoursExist = False
                            fileheader = None
                            tempROIDatasource = None
                        if (fileheader is not None) :
                            try:
                                scanPhase = ROIDictionary.getScanPhaseFromHeader (fileheader)
                            except:
                                scanPhase = "Unknown"
                            try :                
                                antsLog = ANTSRegistrationLog (None)
                                antsLog.loadFromFile (FileHeader = fileheader) 
                            except:
                                antsLog = None                                
                            try :
                                tags = DatasetTagManager ()                                                                                    
                                tags.loadTagDatasetFromFile (tempROIDatasource, FileHeader = fileheader)
                                if tempROIDatasource is not None :
                                    tempROIDatasource.updateTagsFromDataSource (tags, RemoveExistingInternalTags = True)                            
                            except:
                                tags = None 
                            try:
                                if (fileheader.hasFileObject ("NIfTI_Data_Source_Description")) :
                                    fileobj = fileheader.getFileObject ("NIfTI_Data_Source_Description")  
                            except:
                                fileobj = None                    
                        try:
                            LockFP = NonPersistentFSLock.getFileLockPathForROIFilePath (roiDatasetPath)
                            if os.path.exists  (LockFP) :
                                isLocked = RCC_NiftiFileSystemDataInterface.getLockFileState (LockFP, UserUID)
                            else:
                                isLocked = "unlocked"  
                        except:
                            isLocked = "unlocked"  
                        try:                    
                            LockFP = PersistentFSLock.getPersistentLockPathForROIFilePath (roiDatasetPath)
                            if os.path.exists  (LockFP) :
                                pLock = PersistentFSLock.getCurrentPLockFromPath (LockFP)
                            else:
                                pLock = None
                        except:
                            pLock = None
                cachedResult = {}
                cachedResult["ContoursExist"] = contoursExist
                cachedResult["AntsLog"] = antsLog
                cachedResult["ScanPhase"] = scanPhase
                cachedResult["Tags"] = tags
                cachedResult["FileObj"] = fileobj
                cachedResult["NodeLocked"] = isLocked
                cachedResult["PLock"] = pLock
                cachedResult["PLockInit"] = True
                ProjectFileInterface.setProjectDatasetTagCache (treePath, cachedResult, AquireFileLock = AquireFileLock)
            finally:
                if tempROIDatasource is not None :
                    try:
                        if not tempROIDatasource.isReadOnly () :
                            ProjectFileInterface.saveDatasetCompressedCache (roiDatasetPath, cachedResult)
                            print ("Creating dataset cache for: " + roiDatasetPath)
                    except:
                        print ("Error occured trying to save dataset cache for: " + roiDatasetPath )
                    tempROIDatasource.close ()                                 
            return cachedResult
        
        def isDelayedCacheLoadingSet (self) :
            return self._DelayedCacheLoading is not None
        
        def clearDelayedCacheLoading (self) :
            self._DelayedCacheLoading = None
        
        def setDelayedCacheLoading (self, tpl) :
            self._DelayedCacheLoading = tpl   
            if tpl is not None :
                self.clearROIDatasetIndicatorCache ()
            
        
        def getNoneDataChildernCount (self) :
            node = self
            while node is not None :
                childernlen = len (node.getChildernLst ())
                if childernlen == 1 :
                    node = node.getChildernLst ()[0]
                else:
                    return childernlen
            return 0

        def delayedCacheLoading (self, AquireFileLock = True, InitalizeChildNodes = True, SetParentLocked = True, RecursionCount = 0, ForceLoadChildern = False) :        
            
            def _clearChildern (node) :
                try:
                    nodeQueue = [node] 
                    while len (nodeQueue) > 0 :
                        node = nodeQueue.pop ()
                        node.clearDelayedCacheLoading ()
                        if len (node._childrenList) > 0 :
                            nodeQueue += node._childrenList
                except:
                    pass
                    
            if self._DelayedCacheLoading is None :
                return 
            if AquireFileLock :
                self._tree._ProjectDataset.getProjectFileInterface().aquireFileLock ()            
            try:       
                if len (self._childrenList) > 0 :                    
                    parent = self.getParent ()
                    self.clearDelayedCacheLoading ()
                    if ForceLoadChildern :
                        loadChildern = True
                    else:
                        if self._tree is None :
                            MAXChildernCount = 300
                        elif self._tree.isRootDirNode (self) or (parent is not None and self._tree.isRootDirNode (parent)) :
                            MAXChildernCount = 500
                        else:
                            MAXChildernCount = 100
                        loadChildern = InitalizeChildNodes
                        if loadChildern :
                            if self.getNoneDataChildernCount () > 0 and self.hasParent () :
                                if len (self.getParent ().getChildernLst ()) > MAXChildernCount :
                                    loadChildern = False
                    if loadChildern :                        
                        if self._tree is not None :
                            filePath = self._tree.getROIFilePathForNode (self)
                            if filePath is not None :
                                postfixlen = len ("_ROI.txt")
                                if len (filePath) >= postfixlen :
                                    filePath = filePath[:-postfixlen]
                                    if not os.path.exists (filePath) or not os.path.isdir (filePath) :
                                        loadChildern = False  
                                        for child in self._childrenList :
                                            _clearChildern (child) 
                                        
                        if loadChildern :    
                            NextRecursionCount = RecursionCount + 1
                            for child in self._childrenList :
                                child.delayedCacheLoading (AquireFileLock = False, InitalizeChildNodes = InitalizeChildNodes, SetParentLocked = False, RecursionCount = NextRecursionCount, ForceLoadChildern = ForceLoadChildern)
                            if InitalizeChildNodes and RecursionCount == 0:
                                RCC_NiftiFileSystemDataInterface.initalizeLoadedAllNodesPLockStatusAndDescription  ([self], AquireFileLock = False)                                        
                    self.fireTreeNodeChangedEvent ()
                else:
                    datasetPath, userUID = self._DelayedCacheLoading
                    if datasetPath is not None :
                        self.clearDelayedCacheLoading ()
                        treePath = self.getTreePath ()
                        if self.describesNiftiDataFile () :
                            projectFi = self._tree._ProjectDataset.getProjectFileInterface()
                            cacheResult = FileSystemTreeWidgetNode.getNodeCacheData (treePath, datasetPath, userUID, projectFi, AquireFileLock = False)
                            self._tree._updateTreeNodeStateForCacheResult (self, cacheResult, FireChildNodeChangeEvent= InitalizeChildNodes, FireTreeNodeChangeEvent = InitalizeChildNodes, AquireFileLock = False, SetParentNodeLocked = SetParentLocked)                        
                            self.fireTreeNodeChangedEvent ()
            finally:
                if AquireFileLock :
                    self._tree._ProjectDataset.getProjectFileInterface().releaseFileLock ()

            
        def setROIFileLocked (self, nodeLockedState, setParent = True, AquireFileLock = True, FireTreeNodeChangeEvent = True, FireChildTreeNodeChangeEvent = True) :
            CallTreeNodeChanged = False
            CallChildTreeNodeChanged = False
            if AquireFileLock :
                self._tree._ProjectDataset.getProjectFileInterface().aquireFileLock ()
            try:
                if (nodeLockedState != self.getROIFileLock (AquireFileLock = False))  :               
                    if (self._tree == None or not FireTreeNodeChangeEvent) :                             
                        self.setNodeROIFileLock (nodeLockedState, AquireFileLock = False)                                           
                        CallTreeNodeChanged = True
                    else:                    
                        if self.isVisible () :
                            tree = self._tree
                            tree.layoutAboutToBeChanged.emit ()            
                        self.setNodeROIFileLock (nodeLockedState, AquireFileLock = False)                                            
                        if self.isVisible () :
                            nodechanged = tree.createIndex(self.getRow (), 0, self)                
                            tree.dataChanged.emit (nodechanged, nodechanged)
                            tree.changePersistentIndex(nodechanged, nodechanged)
                            tree.layoutChanged.emit ()
                if (setParent and self.hasParent ()):
                    if not self.getParent ().isDelayedCacheLoadingSet () :
                        if (nodeLockedState != self.getParent ().getROIFileLock (AquireFileLock = False)) :
                            found = True
                            for child in self.getParent ().getChildernLst () :
                                if child is not self :
                                    if child.isDelayedCacheLoadingSet () :
                                        child.delayedCacheLoading (AquireFileLock = False, InitalizeChildNodes = False, SetParentLocked = False)
                                    if (nodeLockedState != child.getROIFileLock (AquireFileLock = False)) :
                                        found = False
                                        break
                            if found :
                                self.getParent ().setROIFileLocked (nodeLockedState, setParent = True, AquireFileLock = False, FireTreeNodeChangeEvent = FireChildTreeNodeChangeEvent, FireChildTreeNodeChangeEvent = FireChildTreeNodeChangeEvent)
                                if not FireChildTreeNodeChangeEvent :
                                    CallChildTreeNodeChanged = True
                return CallChildTreeNodeChanged, CallTreeNodeChanged
            finally:                 
                if AquireFileLock : 
                    self._tree._ProjectDataset.getProjectFileInterface().releaseFileLock ()
                        
        @staticmethod
        def getPrimarySortKey (node) :
            if len (node[0].getChildernLst ()) == 0 :
                return "a"+node[0].getDisplayText ()
            return "b"+node[0].getDisplayText ()
        
        @staticmethod
        def getSecondaryColumnTupleSortKey (node) :
            if  node[1] is not None :
                return node[1]
            return ""
            
        def getSecondaryColumnSortKey (self, columnName, tagidentifer = None):
            if self.isDelayedCacheLoadingSet () :
                return "Cache_Delayed"            
            if columnName == "Description" :
                return self.getDescription ()
            elif columnName == "Registration" :
                if (self.isNodeAntsRegistered ()) :
                    return self.getAntsRegisteredDate ()
                else:
                    return ""
            elif columnName == "Tag" :
                if tagidentifer is None :
                    return ""
                datasetTags = self.getTags ()
                if datasetTags is None :
                    return ""
                tagfound = datasetTags.getTagByIdentifer (tagidentifer)
                if (tagfound is None) :
                    return ""
                else:
                    value = str(tagfound.getValue ())
                    if tagfound.isInternalTag () :
                        name = tagfound.getName ()
                        if name == "File owner"  :
                            if self._tree is not None :
                                if self._tree._ProjectDataset is not None:
                                    UserName = self._tree._ProjectDataset.getUserName ()
                                    if value == UserName:
                                        return "1"+value
                                    else:
                                        fileSharing = self._tree._ProjectDataset.getProjectFileInterface().getMultiUserFileSharingManager ()
                                        if fileSharing is not None :
                                            userGroupList = fileSharing.getUserGroups (UserName)
                                            if value in userGroupList :
                                                return "2"+value
                                        if value == "" :
                                            return "3"
                                        return "4"+value
                            return value
                        elif name == "File status"  :
                            if value == "Acquired" :
                                return "1"+value
                            elif value == "Group" :
                                return "2"+value
                            elif value == "" :
                                return "3"
                            elif value == "Other user" :
                                return "4"+value
                        elif name == "Date acquired"  :
                             if isinstance (value, str) :
                                 return "99999" + value
                             return DateUtil.getSortableString (value)
                        elif name == "Time acquired"  :
                            if isinstance (value, str) :
                                 return "99999" + value
                            return TimeUtil.timeToSortableString (value)
                    return value
            
        def setPrimaryColumnSortDirection (self, direction, ForceSort = False) :
            if (self._ColumnSort.setPrimaryColumnSortDirection (direction) or ForceSort) :
                tree = self._tree
                sortList = []
                TreeViewWidget = tree.getTreeViewWidget ()
                for child in self._childrenList :                    
                        if (tree is not None) :
                            if child.isVisible () :
                                nodechanged = tree.createIndex(child.getRow (), 0, child)
                                nodeexpanded = TreeViewWidget.isExpanded (nodechanged)
                            else:
                                nodeexpanded = False
                        else:
                            nodeexpanded = False
                        sortList.append ((child, nodeexpanded))
                    
                sortList = sorted (sortList, key = FileSystemTreeWidgetNode.getPrimarySortKey)
                if direction < 0 :
                    sortList.reverse ()
                    
                self._childrenList = []
                visibleRow = 0
                for node_tuple in sortList :
                    node, expanded = node_tuple
                    node.setPrimaryColumnSortDirection (direction, ForceSort = ForceSort) 
                    self._childrenList.append (node)                    
                    if node.isVisible () :
                        if node.setRow (visibleRow) :        
                            if (tree is not None) :
                                tree.layoutAboutToBeChanged.emit ()   
                                nodechanged = tree.createIndex(visibleRow, 0, node)
                                tree.dataChanged.emit (nodechanged, nodechanged)
                                tree.changePersistentIndex(nodechanged, nodechanged)
                                tree.layoutChanged.emit () 
                        visibleRow += 1
                if (tree is not None) :
                    for node_tuple in sortList :
                        node, expanded = node_tuple
                        if node.isVisible () :
                            nodechanged = tree.createIndex(node.getRow (), 0, node)
                            TreeViewWidget.setExpanded (nodechanged, expanded)
                
                
        def setSecondaryColumnSortDirection (self, columnName, direction, tagIdentifier, IgnoreBlank = "", ForceSort = False) :
            if (self._ColumnSort.setSecondaryColumnSort (columnName, direction, tagIdentifier) or ForceSort) :
                SortableDataList = []
                tree = self._tree
                TreeViewWidget = tree.getTreeViewWidget ()
                for child in self._childrenList :
                    if tree is not None :
                       if child.isVisible () :
                           nodechanged = tree.createIndex(child.getRow (), 0, child)
                           nodeexpanded = TreeViewWidget.isExpanded (nodechanged)
                       else:
                           nodeexpanded = False
                    else:
                        nodeexpanded = False
                    if not child.describesNiftiDataFile () :
                        sortKey = child.setSecondaryColumnSortDirection (columnName, direction, tagIdentifier, IgnoreBlank = IgnoreBlank, ForceSort = ForceSort)
                        SortableDataList.append ((child, sortKey, nodeexpanded))
                    else:
                        SortableDataList.append ((child, child.getSecondaryColumnSortKey (columnName, tagIdentifier), nodeexpanded))
                if len (SortableDataList) > 0 :
                    sortKey = SortableDataList[0][1]
                    sortRequired = False
                    for item in SortableDataList :
                        if item[1] != sortKey :
                            sortRequired = True
                            break
                    if not sortRequired :
                        return sortKey
                    sortedChildernList = sorted (SortableDataList,key = FileSystemTreeWidgetNode.getSecondaryColumnTupleSortKey)
                    if len (sortedChildernList) > 0 :
                        if (direction < 0) :
                            sortedChildernList.reverse ()
                        newLst = []
                       
                        visibleRow = 0
                        for child in sortedChildernList :
                            childnode = child[0]
                            newLst.append (childnode)
                            if childnode.isVisible () :
                                if childnode.setRow (visibleRow) :
                                    if (tree is not None) :
                                        tree.layoutAboutToBeChanged.emit ()   
                                        nodechanged = tree.createIndex(visibleRow, 0, childnode)                
                                        tree.dataChanged.emit (nodechanged, nodechanged)
                                        tree.changePersistentIndex(nodechanged, nodechanged)
                                        tree.layoutChanged.emit ()  
                                visibleRow += 1
                        self._childrenList = newLst
                        if (tree is not None) :
                            for node_tuple in sortedChildernList :                                
                                node, expanded = node_tuple[0], node_tuple[2]
                                if node.isVisible () :
                                    nodechanged = tree.createIndex(node.getRow (), 0, node)
                                    TreeViewWidget.setExpanded (nodechanged, expanded)
                        for index in range (len (sortedChildernList)):
                            val = sortedChildernList[index][1]
                            if val != IgnoreBlank :
                                return val
                        return sortedChildernList[0][1]
            return None
               
                
        
           
                     
        def isDatasetOpen (self) :
            return self._datasetOpened
            
        def setDatasetOpen (self, val) :
            if (self._datasetOpened != val) :            
                tree = self._tree
                if (tree is not None and self.isVisible ()) :
                    tree.layoutAboutToBeChanged.emit ()            
                self._datasetOpened = val    
                if (tree is not None and self.isVisible ()) :
                    nodechanged = tree.createIndex(self.getRow (), 0, self)                
                    tree.dataChanged.emit (nodechanged, nodechanged)
                    tree.changePersistentIndex(nodechanged, nodechanged)
                    tree.layoutChanged.emit ()                          
                if (self.hasParent ()):
                    self.getParent ().setDatasetOpen (val)                        
                    
        def setSelected (self, val) :
            if (self._selected != val) :            
                tree = self._tree
                if (tree is not None and self.isVisible ()) :
                    tree.layoutAboutToBeChanged.emit ()            
                self._selected = val    
                if (tree is not None and self.isVisible ()) :
                    nodechanged = tree.createIndex(self.getRow (), 0, self)                
                    tree.dataChanged.emit (nodechanged, nodechanged)
                    tree.changePersistentIndex(nodechanged, nodechanged)
                    tree.layoutChanged.emit ()                          
                if (self.hasParent ()):
                    self.getParent ().setSelected (val)                        
        
        def isSelected (self) :
            return self._selected 
        
        
        
        def setPesscaraDataSetSource (self, pesscaraInterfaceFileObj, SubjectCode, ExamCode, SeriesCode) :
            self._pesscaraDatasourceInterfaceFileObject = pesscaraInterfaceFileObj
            self._pesscaraNodeID = (SubjectCode, ExamCode, SeriesCode)
            
        def isInitalized (self) :
            return self._initalized
        
        def setInitalized (self) :
            self._initalized = True
        
        def getNodeType (self) :
            return "FileSystemTreeWidgetNode"
            
        def equals (self, testTreeNode):
            try :
                return self._nodePath == testTreeNode._nodePath
            except :
                return False
                
        def getExportMaskFilesystemPath (self) :
            path, filename = os.path.split (self.getPath  ())
            return path
        
        def getPesscaraInterace (self) :
            return None
                
            
        def clearTree (self) :
            self._tree = None
            
        def getSubjectName (self):
            if (self.hasParent ()) :
                parent = self.getParent ()
                if (parent.hasParent ()) :
                    return parent.getParent ().getFilename ()
            return ""
        
        def getSaveFileNifitFileSourceFileObject (self)  :
            self.delayedCacheLoading ()
            f = FileObject ("NIfTI_Data_Source_Description")
            f.setParameter ("DataLocation","Filesystem")
            f.setParameter ("Filename",self.getFilename  ())
            f.setParameter ("full_path",self.getPath  ())
            f.setParameter ("roitree_path",self.getTreePath  ())        
            if (self._pesscaraDatasourceInterfaceFileObject is not None and self._pesscaraNodeID is not None) : # used as a mecahmism to save pesscara info if dataset origionated from pesscara 
                f.addInnerFileObject (self._pesscaraDatasourceInterfaceFileObject)
                f.setParameter ("PESSCARA_SubjectCode", self._pesscaraNodeID[0])
                f.setParameter ("PESSCARA_ExamCode", self._pesscaraNodeID[1])
                f.setParameter ("PESSCARA_SeriesCode", self._pesscaraNodeID[2])
                f.setParameter ("PESSCARA_Context", self._tree.getPesscaraContex ())
            return f
            
        def getExamName (self) :
            if (self.hasParent ()) :
                return self.getParent().getFilename ()
            return ""
        
        def getSeriesName (self) :
            if (self.hasParent ()) :
                return self.getFilename ()
            return ""    
            
        
        def getFilesystemROIDataPath (self, ReturnFileSystemPath = True) : #returns the data path used to store the ROIDatafile on disk    
           return  self.getTreePath (ReturnFileSystemPath=ReturnFileSystemPath)            
            
        def getNodeROIDatasetIndicator (self, AquireFileLock = True):
            return self.getROIDatasetIndicator (AquireFileLock= AquireFileLock)
        
        def getTreePathList (self, ReturnFileSystemPath = True):
            if ReturnFileSystemPath :
                path = [self.getFilename  ()]
            else:
                path = [self.getText  ()]
            parent = self.getParent ()
            if ReturnFileSystemPath :
                while (parent is not None) :
                    path.append ( parent.getFilename ())             
                    parent = parent.getParent ()  
            else:
                while (parent is not None) :
                    path.append (parent.getText ())             
                    parent = parent.getParent ()  
            path.reverse()
            return path      
        
        def getTreePath (self, ReturnFileSystemPath = True):
            if ReturnFileSystemPath :
                if self._treePathFSCache is not None :
                    return self._treePathFSCache
                path = self.getFilename  ()
            else:
                path = self.getText  ()
            parent = self.getParent ()
            if ReturnFileSystemPath :
                while (parent is not None) :
                    path = os.path.join (parent.getFilename  (), path)                    
                    parent = parent.getParent ()
            else:
                while (parent is not None) :
                    path = os.path.join (parent.getText  (), path)                    
                    parent = parent.getParent ()
            if ReturnFileSystemPath :
                self._treePathFSCache = path
            return path
                    
        def describesNiftiDataFile (self) :
            lPath = self._nodePath.lower()
            return  lPath.endswith (".nii.gz") or lPath.endswith (".nii")
            
        def getNIfTIDatasetFile (self) :                        
            node = self
            if (len (node._childrenList) > 0) :
                return FileSystemDF ()            
            lPath = node._nodePath.lower()
            if (lPath.endswith (".nii.gz") or lPath.endswith (".nii")) :                        
                return FileSystemDF (self)
            return FileSystemDF () 
                
        def isCreated (self) :
            return self._created
        
        def setCreated (self) :
            self._created = True
        
        def getPath (self) :
            return self._nodePath
            
        def getFilename (self, LongFileName = False) :    
            if LongFileName :
                return self.getText ()
            path, filename = os.path.split (self._nodePath)
            return filename
            
        def textUndefined (self) :
            return self._text == ""
        
        def getText (self) :
            return self._text
                
        def setText (self, txt) :
            self._text = txt            

        def setScanPhaseTxt (self, scanPhase, AquireFileLock = True, FireChildNodeChangeEvent = True, FireTreeNodeDescriptionChangeEvent = True) :
            if AquireFileLock :
                self._tree._ProjectDataset.getProjectFileInterface().aquireFileLock ()
            try:                
                return self.setDescription (scanPhase, AquireFileLock = False, FireTreeNodeChangedEvent = FireChildNodeChangeEvent, FireTreeNodeDescriptionChangeEvent = FireTreeNodeDescriptionChangeEvent)                           
            finally:
                if AquireFileLock :
                    self._tree._ProjectDataset.getProjectFileInterface().releaseFileLock ()
                
        def setMLPredictedScanPhaseTxt (self, predictedScanPhase, AquireFileLock=True, FireChildNodeChangeEvent = True) :
            if AquireFileLock :
                self._tree._ProjectDataset.getProjectFileInterface().aquireFileLock ()
            try:
                if (self.getROIDatasetMLDescriptionPrediction (AquireFileLock=False)!= predictedScanPhase) :
                    self.setROIDatasetMLDescriptionPrediction (predictedScanPhase, AquireFileLock =False)         
                    if (FireChildNodeChangeEvent and self.getDescription (AquireFileLock=False) == "Unknown") :
                       self.fireTreeNodeChangedEvent ()   
                       return False
                    return True
                return False
            finally:
                if AquireFileLock :
                    self._tree._ProjectDataset.getProjectFileInterface().releaseFileLock ()
                    
        def isScanPhasePredicted (self, AquireFileLock=True) :
            if AquireFileLock :
                self._tree._ProjectDataset.getProjectFileInterface().aquireFileLock ()
            try:
                if (self.getDescription (AquireFileLock=False) == "Unknown") :
                    if (self.getROIDatasetMLDescriptionPrediction (AquireFileLock=False)!= "Unknown") :
                        return True
                return False        
            finally:
                if AquireFileLock :
                    self._tree._ProjectDataset.getProjectFileInterface().releaseFileLock ()
                
        def getScanPhaseTxt (self, AquireFileLock = True) :
            if AquireFileLock :
                self._tree._ProjectDataset.getProjectFileInterface().aquireFileLock ()
            try:
                if (self.getDescription (AquireFileLock=False) == "Unknown") :
                    if (self.getROIDatasetMLDescriptionPrediction (AquireFileLock=False)!= "Unknown") :
                        return ("Predicted: " + self.getROIDatasetMLDescriptionPrediction (AquireFileLock=False)) 
                    return ""
                else:
                    return self.getDescription ()
            finally:
                if AquireFileLock :
                    self._tree._ProjectDataset.getProjectFileInterface().releaseFileLock ()
        
        def hasParent (self) :
            return (self._parent is not None) 
                                
        def getParent (self) :
            return self._parent
        
        def getParentRow (self) :
            if (self._parent is not None):
                return self._parent._row
                         
        def getChild (self, row):                       
            if (row >= 0 and row < len (self._childrenList)):                
               return self._childrenList[row]    
                              
        def getChildernLst (self) :
            return self._childrenList
            
        def hasOnlySingleChild (self) :            
            node = self
            while True :
                if len (node._childrenList) == 0 :
                    break
                elif len (node._childrenList) > 1:
                    return False
                else:
                    node = node._childrenList[0]                        
            return True                    
                
        def getChildCount (self) :
            return len (self._childrenList)                    
        
        def addChild (self, node):
            self._childrenList.append (node)    
            return len (self._childrenList) -1
            
        def getRow (self) :
            return self._row
               
        def setCreatedAndRowFast (self, rownum) :
            self._row = rownum 
            self._created = True
            
        def setRow (self, rownum) :
            if rownum != self._row :
                self._row = rownum 
                return True
            return False
         
        def loadAllChildTreeNodes (self) :            
            return
      
        def getQModelIndex (self):
            NodeList = []
            if not self.isVisible () :
                self.setVisible (True)
                NodeList.append (self)
                parent = self.getParent ()
                while parent is not None :                
                    NodeList.append (parent)
                    parent = parent.getParent  ()
                NodeList.reverse()
                self._tree.layoutAboutToBeChanged.emit ()   
                for node in NodeList :                             
                    nodechanged = self.createIndex(node.getRow (), 0, node)
                    self._tree.dataChanged.emit (nodechanged, nodechanged)
                    self._tree.changePersistentIndex(nodechanged, nodechanged)
                self._tree.layoutChanged.emit ()                             
            return self._tree.createIndex (self.getRow (), 0, self)        
                
            

            

        
class FileSystemLoader :        
        
        #updateProgressThreadCallback = pyqtSignal(['PyQt_PyObject','PyQt_PyObject'])  
        
        def __init__(self, tree, rootListofFilestoLoad, DataSetDirectoryCount = None):      
            #super(FileSystemLoader, self).__init__ ()
            self.exiting = False
            self._rootListFilestoLoad = rootListofFilestoLoad            
            self._tree = tree
            self._progress = 0
            self._lastProgressUpdate = -1
            self._filesToProcessCount = 0
            print ("File system loader initalized")    
            #self._results = []        
            self._dataSetDirectoryCount = DataSetDirectoryCount
            self._ShowDICOM2NiftiErrorMsg = True            
            
                 
        def setCallBacks (self, updateNodeTreeNode, updateProgress):
            self.updateTreeNode = updateNodeTreeNode      
            self.updateProgress = updateProgress       
            #self.updateProgressThreadCallback.connect (updateProgress)
      
        def _CreateParentDirs (self, TreeNodePath):
            for node in TreeNodePath :
                if (not node.isCreated ()) :
                    node.setCreated ()
                    if (node.hasParent ()):
                       parent = node.getParent ()
                       row = parent.addChild (node)
                       node.setRow (row)                       
                       self.updateTreeNode ([parent,node])  
                       #self._results.append ([parent,node])
                    else:
                        self.updateTreeNode ([node])                    
                        #self._results.append ([node])
        
        def _tryCountDirectory (self,  filePath):
            if (self.exiting) :
                    return                        
            self._readNoProgress (filePath) 
            try:
                fileDirectoriesIterator = ScanDir.scandir(filePath) 
                for fileDescription in fileDirectoriesIterator :                    
                    self._filesToProcessCount += 1            
                    if (self.exiting) :
                        return                    
                    if (fileDescription.is_dir ()):                    
                        self._tryCountDirectory (fileDescription.path)     
            finally:
                try:
                    fileDirectoriesIterator.close ()
                except:
                     pass
        
            
        
        @staticmethod
        def _getSortedFileDir (path) :         
            def _byName (lfAPI, fobj) :
                try :
                    if lfAPI is not None :
                        try :                        
                            lfongName = lfAPI.getLongFileName (fobj.name)
                            if lfongName is not None:
                                return  lfongName
                        except:
                            pass                                    
                    return fobj.name
                except:
                    return None
            
            try:
                lfNameAPI = None
                if LongFileNameAPI.doesLFDataFileExistForPath (path, TryRead = False) :
                    try :
                        lfNameAPI = LongFileNameAPI (path, LoadLongFileNameFileDescriptorTreePathList = True)
                    except:
                        lfNameAPI = None
                iterator = ScanDir.scandir (path)
                sortFunc = partial (_byName, lfNameAPI)
                return sorted (list (iterator),key = sortFunc)
            finally:
                try :
                    iterator.close ()
                except:
                    pass #python 2
        
        @staticmethod 
        def _isFileNameInFileObjList (filename, fileObjList) :
            for fobj in fileObjList :
                if fobj.name == filename :
                    return True
            return False
            
        @staticmethod 
        def MP_getDirList (path):
            try:
                DirList = FileSystemLoader._getSortedFileDir (path)
                returnTree = {}
                returnTree[path] = DirList  
                for entry in DirList :
                    if entry.is_file () :                        
                        returnTree[entry.path] = entry.path
                return returnTree
            except:
                print ("Returning None")
                return None
            
        
                
                
        def _BuildDirectory (self, filePath, ParentDirectoryLst, parent,  AquireFileLock = True, DirCache = None):                 
            if (self.exiting) :
                return             
                        
            niftifilefound = False
            triedToProcessedOtherFiles = False            
            try:
                fileDirectories = DirCache[filePath]
            except:
                DirCache = FileSystemLoader.MP_getDirList (filePath)                
                if filePath not in DirCache :
                    fileDirectories = None
                else:
                    fileDirectories = DirCache[filePath]
            if fileDirectories is None :
                if not FileUtil.testCanReadFromDirectory (filePath) :                    
                    title, msg = "File Permission Error", "Unable to read from the NIfTI data directory '"+ filePath + "'. You likely do not have file privlages to read from this location."                    
                else:
                    title, msg = "File Error", "Unknown error occured trying to read path: %s" % filePath
                MessageBoxUtil.showMessage (title, msg)
                print (msg)
                self.exiting = True
                return 
            
            
            ChildernDir = OrderedDict ()
            NodeLeaf = OrderedDict ()
            try :
                App = self._tree._ProjectDataset.getApp ()
            except: 
                App = None
            while fileDirectories is not None : 
                otherfilesfound = False 
                for fileObj in fileDirectories :            
                    if fileObj.is_file ():
                        lfilename = fileObj.name.lower ()
                        if lfilename.endswith (".nii.gz") or lfilename.endswith (".nii") :
                            niftifilefound = True                        
                            addfile = False
                            if not fileObj.name.startswith ("o") :         
                                addfile = True
                            else:                                
                                if not FileSystemLoader._isFileNameInFileObjList (fileObj.name[1:], fileDirectories) :
                                    addfile = True                                
                            if (addfile) :                                                                  
                                NodeLeaf[fileObj.path] = True                                                  
                        elif not triedToProcessedOtherFiles and lfilename not in [".ds_store","lf.dat","skipdicoms.dat","convdicoms.dat"] and not lfilename.endswith (".dicomtags") and not lfilename.endswith (".xml") and not lfilename.endswith (".n4b")and not lfilename.endswith (".ndt") and not lfilename.endswith ("_info.txt") and not lfilename.endswith (".prj") and not lfilename.endswith (".cache") and not lfilename.endswith ("_dscache.bin") and not lfilename.endswith ("_roi.txt") and not lfilename.endswith (".ver")  :
                            otherfilesfound = True
                    else:      
                        ChildernDir[fileObj.path] = True
                                
                fileDirectories = None
                if not niftifilefound and otherfilesfound :    
                    triedToProcessedOtherFiles = True
                    if (DicomVolume.doesDirectoryContainDicomFiles (filePath, IgnoreProcessedDicoms = True, App = App)) :
                        if not FileUtil.testCanReadWriteFromDirectory (filePath) :
                            MessageBoxUtil.showMessage ("File Permission Error", "Unable to write to the NIfTI data directory. You likely do not have file privlages to write to this location. RIL-Contour requires users level write privlages to convert found DICOM files to NIfTI.")
                        else:
                            self._incProgress ("Converting DICOM files to NIfTI")                    
                            niftifilename, self._ShowDICOM2NiftiErrorMsg = DicomVolume.convertDicomFilesToNIfTI (filePath, ShowDICOM2NiftiErrorMsg = self._ShowDICOM2NiftiErrorMsg, App = App)                                                            
                            if (niftifilename is not None and len (niftifilename) > 0) :
                                fileDirectories = FileSystemLoader._getSortedFileDir (filePath)
                                DirCache[filePath] = fileDirectories  
                                
                    if (not niftifilefound and DicomVolume.doesDirectoryContainImageFiles (filePath, IgnoreProcessedDicoms = True, App = App)) :
                        if not FileUtil.testCanReadWriteFromDirectory (filePath) :
                            MessageBoxUtil.showMessage ("File Permission Error", "Unable to write to the NIfTI data directory. You likely do not have file privlages to write to this location. RIL-Contour requires users level write privlages to convert found Image files to NIfTI.")
                        else:
                            self._incProgress ("Converting image files to NIfTI")
                            niftifilename = DicomVolume.convertRGBFilesToNIfTI (filePath, App = App)
                            if (niftifilename is not None and len (niftifilename) > 0) :
                                fileDirectories = FileSystemLoader._getSortedFileDir (filePath)
                                DirCache[filePath] = fileDirectories             
            try :                   
                if AquireFileLock :
                    self._tree._ProjectDataset.getProjectFileInterface().aquireFileLock ()                
                ParentDirectoryLstCopy = copy.copy (ParentDirectoryLst)                
                self._incProgress (filePath)
                parent = FileSystemTreeWidgetNode (self._tree, parent, -1, filePath, AquireFileLock = False, DirCache = DirCache)                
                ParentDirectoryLstCopy.append (parent) 
                #update parent node ROI DataIndocaton
                
                """if parent.getROIDatasetIndicator (AquireFileLock = False, InitalizeChildNodes = False) :
                    parentNode = parent.getParent ()
                    while parentNode is not None :
                        if parentNode.getROIDatasetIndicator (AquireFileLock = False, InitalizeChildNodes = False) :
                            break
                        print (parentNode.getText ())
                        parentNode.setROIDatasetIndicator (True, FireChildNodeChangedEvent = False, FireTreeNodeChangedEvent = False, AquireFileLock = False)
                        parentNode = parentNode.getParent ()"""
                
                if len (NodeLeaf) > 0 :                                            
                    self._CreateParentDirs (ParentDirectoryLstCopy)
                    for leaffilePath in NodeLeaf.keys () :                            
                        node = FileSystemTreeWidgetNode (self._tree, parent, -1, leaffilePath, AquireFileLock = False, DirCache = DirCache)
                        row = parent.addChild (node)                      
                        node.setCreatedAndRowFast (row)                       
                        self.updateTreeNode ([parent,node])  
                        #self._results.append ([parent,node])
                                                  
                subDirList = list (ChildernDir.keys ())
                DirCache = {}
                for index, subTree in enumerate (PlatformThreading.ThreadMap (FileSystemLoader.MP_getDirList, subDirList, App = App, SleepTime = 0.05, Debug = False, ThreadMax = 32)) :
                    if subTree is None :
                        sd = subDirList[index]
                        DirCache[sd] = None
                    else:
                        for key, value in subTree.items () :
                            DirCache[key] = value                                                                        
                for dirPath in subDirList :                    
                    self._BuildDirectory ( dirPath, ParentDirectoryLstCopy, parent,  AquireFileLock = False, DirCache = DirCache)                                       
    
            finally :    
                if AquireFileLock : 
                    self._tree._ProjectDataset.getProjectFileInterface().releaseFileLock ()
                    
                      
           
                    
        
                
        def _readNoProgress (self, path) :
            self.updateProgress (path, 0)            
            
        def _incProgress (self, path) :                                       
            self._progress += 1            
            value = int (100.0 * self._progress / self._filesToProcessCount)
            if (value >= self._lastProgressUpdate + 5) :
                self._lastProgressUpdate = value
                if (value > 100):                     
                    self.updateProgress (path,100)
                else:                        
                    self.updateProgress (path,value)            
            
        
        def stoploading (self) :
            self.exiting = True
                    
        def start(self):   
            print ("Starting loading FS")
            if (self._rootListFilestoLoad is not None)  :
                if (self._dataSetDirectoryCount is None) :
                    self._filesToProcessCount += 1
                    for filePath in self._rootListFilestoLoad :      
                        if (filePath != ""):                                
                            if (self.exiting) :
                                return                                                                            
                            if os.path.isdir (filePath) :                        
                                self._tryCountDirectory (filePath)                        
                else:
                    self._filesToProcessCount = self._dataSetDirectoryCount
                    
                self._incProgress ("")
                try :
                    self._tree._ProjectDataset.getProjectFileInterface().aquireFileLock ()
                    for filePath in self._rootListFilestoLoad :      
                        if (filePath != ""):                                
                            if (self.exiting) :
                                return
                            print (("Processing", filePath))    
                            if os.path.isfile (filePath) :                      
                                lFilePath = filePath.lower()
                                if lFilePath.endswith (".nii.gz") or lFilePath.endswith (".nii") :         
                                     if not FileUtil.testCanReadFromFile (filePath):
                                         MessageBoxUtil.showMessage ("File Permission Error", "Unable to read from the file '"+ filePath + "'. You likely do not have file privlages to read from this location.")
                                         break
                                     else:
                                         print ("Found File: " + filePath)
                                         rootNode = FileSystemTreeWidgetNode (self._tree, None, -1, filePath, AquireFileLock = False)
                                         rootNode.setCreated ()
                                         self.updateTreeNode ([rootNode])
                                         #self._results.append ([rootNode])                                 
                            elif os.path.isdir (filePath) :        
                                DirCache = FileSystemLoader.MP_getDirList (filePath)
                                self._BuildDirectory (filePath, [], None,  AquireFileLock = False, DirCache=DirCache)
                                   
                finally:                    
                    self._tree._ProjectDataset.getProjectFileInterface().releaseFileLock ()
                    
            print ("Done loading FS")
            return self._progress
             
  
            

    
           

def isValidCache (cacheObj) :
       try :        
            return "ContoursExist" in cacheObj and "AntsLog" in cacheObj and "ScanPhase" in cacheObj and "Tags" in cacheObj and "FileObj" in cacheObj         
       except:
            return False            


#class FileSystemEventHandler (watchdog.events.FileSystemEventHandler, QObject) :
class FileSystemEventHandler (QObject) :
    
    pathEvent = pyqtSignal(['PyQt_PyObject','PyQt_PyObject','PyQt_PyObject'])      
        
    def _hasFileChanged (self, path) :
        try:
            fileMonitor = self._fileChangeMonitor
        except:
            self._fileChangeMonitor = {}
            fileMonitor = self._fileChangeMonitor
        if path not in fileMonitor :
            fileMonitor[path] = FileChangeMonitor (path = path)
            return True
        elif fileMonitor[path].hasFileChanged (path) :
            fileMonitor[path].monitorFile (path = path)
            return True
        else:
            return False
        
    def processEvent (self,pathList, treeNode = None) :        
        if not isinstance (pathList, list) :
            pathList = [pathList]
        processedPathList = []
        processedType = []
        for path in pathList :
            try:           
                if NonPersistentFSLock.isFilePathALockFile (path) :    
                    if self._hasFileChanged (path) :
                        processedPathList.append (path)
                        processedType.append ("NonPersistentFSLock")
                elif PersistentFSLock.isFilePathAPLockFile (path) :
                    if self._hasFileChanged (path) :
                        processedPathList.append (path)
                        processedType.append ("PersistentFSLock")
                elif ProjFileInterface.isFileCompressedCache (path) :
                    if self._hasFileChanged (path) :
                        processedPathList.append (path)
                        processedType.append ("DataCache")        
            except  :
                print ("\nA error occured responding to a watchdog file system event.\n")
        if len (processedType) > 0 :
            self.pathEvent.emit (processedPathList, processedType, treeNode)                       
    
    def on_any_event (self, event) :
        try:  
            path = str(event.src_path)
            self.processEvent (path)                       
        except:
            print ("\nA error occured responding to a watchdog file system event.\n")
     

                
class RCC_NiftiFileSystemDataInterface  (QtCore.QAbstractItemModel):
        
                         
    def __init__(self,ROI_Dataset, RootSearchPath = None, FObject = None, parent = None, ProjectDataset = None, ProjectDirPath = None, RILContourCommandLine = None, UIFObjectHeader = None, CreatingSaveCopy = False, SplashScreen = None, *args):                                        
        try :         
            self.resetCachedNodeCount  (AquireFileLock = False)                 
            ResetCache = False
            self._setDisableNodeUpdateLayoutChange (False)
            if ProjectDataset is not None :
                try:
                    ResetCache = ProjectDataset.isProjectReadOnly () and  ProjectDataset.isInNoCacheCommandLineMode()
                except:
                    ResetCache = False
            self._LongFileNameCache = LongFileNameCache (os.path.join (ProjectDirPath,"lf.cache"), ResetCache = ResetCache)                    
            self._showShortFileName = False
            self._timeTreeLastUpdated = 0
            self._treeViewWidget = None
            self._ProjectDataset = ProjectDataset
            self._ColumnTextFilter = ""
            self._OptionsFilter = "AllFiles"
            self._watchDogEventHandler = None 
            #self._TreeWatchDogTimeout = time.time ()
            self._nodeWatchDogTimeout = {}
            self.filesystemLoader = None    
            if os.path.join (ProjectDataset.getHomeFilePath (),"ROIDatasets") == ROI_Dataset.getROIDatasetRootPath () :  #loading default blank project.
                self._watchDogEventHandler = None
            else:
                if not CreatingSaveCopy :
                    self._watchDogEventHandler = FileSystemEventHandler ()
                    self._watchDogEventHandler.pathEvent.connect (self.setPathLocked)                    
            """self._finalizer = None
            self._observedWatchDogWatch = None
            self._watchDogEventHandler = None
            if os.path.join (ProjectDataset.getHomeFilePath (),"ROIDatasets") == ROI_Dataset.getROIDatasetRootPath () :  #loading default blank project.
                self._watchDogEventHandler = None
            else:
                if not CreatingSaveCopy :
                    self._watchDogEventHandler = FileSystemEventHandler ()
                    self._watchDogEventHandler.pathEvent.connect (self.setPathLocked)
                    wd_observer = self._ProjectDataset.getWatchDogDirectoryObserver ()
                    if wd_observer is None :
                        self._observedWatchDogWatch = None
                    else:
                        self._observedWatchDogWatch = wd_observer.schedule (self._watchDogEventHandler, ROI_Dataset.getROIDatasetRootPath (),recursive=True)
                    if PythonVersionTest.IsPython3 () :
                        self._finalizer = weakref.finalize (self, RCC_NiftiFileSystemDataInterface._finalizeObj, self._ProjectDataset.getWatchDogDirectoryObserver (), self._observedWatchDogWatch)"""
            self._DatasetUIHeader = DatasetUIHeader (FObject = UIFObjectHeader)        
            self._datasetDirectoryCount = 32768 # A large number. Inital runs will have progress bars which are innaccurate.
            self._registrationUIFilterDate = (None,None,None)
            self._FirstRegistrationUIFilterDate = (None,None,None)
            self._treeContainsANTSRegisteredDatasets = False
            QtCore.QAbstractItemModel.__init__(self, parent, *args)      
            self._rootMemory = []                
            self._RootSearchPath = RootSearchPath
            self._RootSearchPath = FileUtil.correctOSPath (self._RootSearchPath)                
            self._relativeRootPathList = None
            self._ColumnSort = ColumnSort ()
            self._RILContourCommandLine = RILContourCommandLine
            if RILContourCommandLine is not None and "niftisearchpath" in RILContourCommandLine and not CreatingSaveCopy :
               self._RootSearchPath = RILContourCommandLine["niftisearchpath"]
               self._datasetDirectoryCount = 1
            else:
                if (FObject is not None) :
                    if (FObject.hasParameter("RootSearchPath")) :
                        self._RootSearchPath = FObject.getParameter("RootSearchPath")     
                        self._RootSearchPath = FileUtil.correctOSPath (self._RootSearchPath)        
                    else:
                        self._RootSearchPath = None
                        MessageBoxUtil.showMessage ("Root NIfTI Search Path Not Found", "The loaded project file is misssing the NIfTI RootSearchPath entry or has a malformed NIfTI RootSearchPath entry.")
                    
                    
                    if (FObject.hasParameter("DatasetDirectoryCount")) :
                        self._datasetDirectoryCount = FObject.getParameter("DatasetDirectoryCount")
                    
                    if (FObject.hasParameter("RelativeRootSearchPathList")) :
                        self._relativeRootPathList = RelativePathUtil.relativePathListCopy (FObject.getParameter("RelativeRootSearchPathList"))
                
                if (self._RootSearchPath is not None and len (self._RootSearchPath) == 1 and os.path.exists (self._RootSearchPath[0]) and os.path.isdir (self._RootSearchPath[0])) :
                       relativePathList = RelativePathUtil.getRelatePathListFromFullPath (self._RootSearchPath[0], ProjectDirPath)
                       if not RelativePathUtil.doRelativePathListMatch (relativePathList, self._relativeRootPathList) :
                           self._relativeRootPathList = relativePathList  
                           print ("Updating NIfTI dataset relative path")
                elif (self._relativeRootPathList is not None) :
                     fullPath = RelativePathUtil.getFullPathFromRelativePathList (self._relativeRootPathList, ProjectDirPath)
                     if (fullPath is not None and os.path.exists (fullPath) and os.path.isdir (fullPath)) :
                           self._RootSearchPath = [fullPath]
                           print ("Updating NIfTI dataset path from relative path: " + fullPath)
                RetryFindROIPath = True
                while not os.path.exists (self._RootSearchPath[0]) and RetryFindROIPath :
                    dlg= QFileDialog()
                    dlg.setWindowTitle('NIfTI datasource directory was not found; Select a select the directory to load NIfTI data from' )
                    dlg.setViewMode( QFileDialog.Detail )    
                    dlg.setFileMode (QFileDialog.Directory)
                    dlg.setNameFilters( [self.tr('All Files (*)')] )
                    dlg.setDefaultSuffix( '' )
                    dlg.setAcceptMode (QFileDialog.AcceptOpen)
                    result = dlg.exec_()
                    if result == QDialog.Rejected :
                        RetryFindROIPath = False
                    elif result == QDialog.Accepted and (len (dlg.selectedFiles())== 1):
                        path = dlg.selectedFiles()[0]                                                    
                        if os.path.exists (path) :
                            try:
                                _, NewNiftRoot = os.path.split (path)
                                _, OldNiftRoot = os.path.split (self._RootSearchPath[0])
                                if (NewNiftRoot != OldNiftRoot) :
                                    MessageBoxUtil.showMessage ("Warrning", "Existing ROI Data will not be detected. Root ROI data folder may need to be renamed from %s to %s." % (OldNiftRoot, NewNiftRoot))
                            except:
                                pass
                            self._RootSearchPath = [path]        
            if len (self._RootSearchPath) > 0 and self._RootSearchPath[0] is not None and os.path.isdir (self._RootSearchPath[0]) and LongFileNameAPI.hasLongFileNameDefinitions (self._RootSearchPath[0], CreateLongFileNamePathIfMissing = False):                
                try:
                    _, rootDirName = os.path.split (self._RootSearchPath[0])
                    TreePathList = [rootDirName]
                except:
                    TreePathList = None
                self._longFileNameAPI = LongFileNameAPI (self._RootSearchPath[0], TreePathList = TreePathList, LongFileNameCache = self._LongFileNameCache)                
            else:
                self._longFileNameAPI = None          
            if not CreatingSaveCopy :            
                cacheSize = self._ProjectDataset.getProjectFileInterface().getCacheSize () # self._ProjectDataset.getProjectFileInterface().loadProjectDatasetCache ()
                self.filesystemLoader = FileSystemLoader (self, self._RootSearchPath, DataSetDirectoryCount = max (cacheSize, self._datasetDirectoryCount))
                self._ROI_Dataset = ROI_Dataset
                              
                self._NodeUpdateQue = []
                
                
                self.filesystemLoader.setCallBacks (self.treeChanged, self.updateLoadingProgress)
                if SplashScreen is None :
                    self._progdialog = RCC_SplashScreenDlg (ProjectDataset , parent) 
                else:
                    self._progdialog = SplashScreen
                try:    
                    self._progdialog.setProgressRange (0,100)
                    self._progdialog.setMessage ("Loading Dataset")
                    self._progdialog.setWindowModality(QtCore.Qt.ApplicationModal)                
                    self._progdialog.canceled.connect (self._loadDatasetCanceled)
                   
                    self._datasetDirectoryCount = self.filesystemLoader.start ()  # not really a thread. code used to be refactored to remove threading 
                    print ("start Run_MP_UpdateTree")
                    self.Run_MP_UpdateTree (self._progdialog)                     
                finally:                            
                    print ("done")                
                    self._progdialog.close()  
                    del self._progdialog
                    del self._NodeUpdateQue # clean up only used for starting.  
                    del self.filesystemLoader        
        finally:
            self._LongFileNameCache.UnloadLongFileNameCache ()
    
    """def importNiftiProject (self, NiftiDir, ROIDir, ForceCompressedPathNames= True) :
        
        # to do import 
        # copy Nifti ->
        # copy ROI   ->
        # copy lock file
        # copy plock file
        # copy version file
        # copy cache files
        
        
        if len (self._RootSearchPath) > 0 and self._RootSearchPath[0] is not None and os.path.isdir (self._RootSearchPath[0]) and not LongFileNameAPI.hasLongFileNameDefinitions (self._RootSearchPath[0]):                
            self._longFileNameAPI = LongFileNameAPI (self._RootSearchPath[0])
            for node in self._rootMemory :
                fileName = node.getFileName ()
                shortFileName = self._longFileNameAPI.createShortFileName (fileName)
                try :
                    os.rename ()
                    node.
                    node.setFileName (shortFileName)
                except:"""
    

    
        
    def getLongFileNameCache (self) :
        return self._LongFileNameCache
    
                              
    def importNifti (self, path, SourceROIDataDir = None, ROICopyOptions = None, TryOrganizeDataBySubjectAccessionExam = False, PatientIDFilter = "", PreferredFileFormat = ".nii.gz") :         
        
      
        def _getFileDir (path) :         
            try:
                iterator = ScanDir.scandir (path[0])
                return list (iterator)
            except :
                return []
            finally:
                try :
                    iterator.close ()
                except:
                    pass #python 2
        
        def MP_UpdateDirs (lst) :
            if len (lst) == 0 :
                 return 
            try:
                 App = self._ProjectDataset.getApp ()
            except:
                 App = None 
            for index, dirList in enumerate (PlatformThreading.ThreadMap (_getFileDir, lst , App = App, SleepTime = 0.05, Debug = False, ThreadMax = 32)): 
                 lst[index][2] += dirList   
            
             
        def _initNode (node, destinationROIPath) :              
            def _setNodeParentDelayedCacheLoading (ProjectFileInterface, node) :
                    parent = node.getParent ()
                    while (parent is not None) :
                        parent.setDelayedCacheLoading ((None, None))
                        parent = parent.getParent ()
        
            userUID = self._ProjectDataset.getUserUID ()         
            projectFI = self._ProjectDataset.getProjectFileInterface ()
            node.setDelayedCacheLoading (((destinationROIPath, node.getNIfTIDatasetFile ().getFilePath (), None), userUID))
            node.delayedCacheLoading (AquireFileLock = False)
            _setNodeParentDelayedCacheLoading (projectFI, node) 
            
        def _addTreeNode (ParentNode, NodePath, SkipNodeInitalization = False):
               
            
                newTreeNode = FileSystemTreeWidgetNode (self, ParentNode, -1, NodePath, AquireFileLock = False)    
                if ParentNode is not None :
                    row = ParentNode.addChild (newTreeNode)                      
                else:
                    row = len(self._rootMemory)
                    self._rootMemory.append (newTreeNode)
                    try :
                        self._rootNodes.append (newTreeNode)
                    except:
                        self._rootNodes = [newTreeNode]
                newTreeNode.setCreatedAndRowFast (row)                       
                #if (ParentNode is not None) :
                #    self.treeChanged ([ParentNode,newTreeNode])
                #else:                
                self.treeChanged ([newTreeNode])
            
                treePath = newTreeNode.getTreePath ()          
                projectFI = self._ProjectDataset.getProjectFileInterface ()
                projectFI.removeProjectDatasetCacheEntry (treePath, AquireFileLock = False)     
                if not SkipNodeInitalization  :
                    _initNode (newTreeNode, None)                 
                return newTreeNode
            
        try:    
            ImagingNotImported = []
            ImagingImported = []
            self._ProjectDataset.getProjectFileInterface().aquireFileLock ()        
            self.resetCachedNodeCount  (AquireFileLock = False)     
            if SourceROIDataDir is not None  :    
                if not SourceROIDataDir.endswith (os.sep) :
                    SourceROIDataDir += os.sep                                        
                
                
            self.layoutAboutToBeChanged.emit ()           
            
            self._NodeUpdateQue = None
            self._progdialog = QProgressDialog("Loading Imaging", "Cancel", int (0), int(0), self._ProjectDataset.getNiftiDatasetDlg())  
            self._progdialog.setMinimumDuration (4)        
            self._progdialog.setWindowFlags(self._progdialog.windowFlags () | QtCore.Qt.Dialog | QtCore.Qt.WindowStaysOnTopHint)          
            self._progdialog.setWindowModality(QtCore.Qt.ApplicationModal)       
            self._progdialog.setLabelText ("Discovering NIfTI imaging")   
            self._progdialog.setRange (0,0)
            self._progdialog.setAutoClose (False)
            h = self._progdialog.height ()
            self._progdialog.resize (300, h)    
            self._progdialog.show ()
    
            pathqueue = [[path, [], []]]
            MP_UpdateDirs (pathqueue)
            
            NodeLeaf = {}
                        
            ProcessOtherDirPath = []
            while len (pathqueue) > 0 :
                if  	self._progdialog.wasCanceled()  :
                    break
                dirpath, ReadDirPath, fileDirectories = pathqueue.pop ()                
             
                
                if dirpath not in NodeLeaf :              
                    initalDirectoryFiles = set ()
                    for entry in fileDirectories :
                        initalDirectoryFiles.add (entry.path)               
                    NodeLeaf[dirpath] = (copy.copy (ReadDirPath), [], initalDirectoryFiles)
                
                niftifilefound = False   
                
                if  	self._progdialog.wasCanceled()  :
                    break
                otherfilesfound = False 
                newDirList = []
                for fileObj in fileDirectories :     
                    if  	self._progdialog.wasCanceled()  :
                        break
                    self._ProjectDataset.getApp ().processEvents ()
                    if fileObj.is_dir () :
                        newDirList.append ([fileObj.path, ReadDirPath + [fileObj.name], []])
                    else:
                         lfilename = fileObj.name.lower()
                         if lfilename.endswith (".nii.gz") or lfilename.endswith (".nii") :
                            niftifilefound = True                        
                            addfile = False
                            if not fileObj.name.startswith ("o") :         
                                addfile = True
                            else:                                
                                if not FileSystemLoader._isFileNameInFileObjList (fileObj.name[1:], fileDirectories) :
                                    addfile = True                                
                            if (addfile) :          
                                NodeLeaf[dirpath][1].append (fileObj.name) 
                         elif lfilename not in [".ds_store","lf.dat","skipdicoms.dat","convdicoms.dat"] and not lfilename.endswith (".dicomtags") and not lfilename.endswith (".xml") and not lfilename.endswith (".n4b")and not lfilename.endswith (".ndt") and not lfilename.endswith ("_info.txt") and not lfilename.endswith (".prj") and not lfilename.endswith (".cache") and not lfilename.endswith ("_dscache.bin") and not lfilename.endswith ("_roi.txt") and not lfilename.endswith (".ver") :
                            otherfilesfound = True   
                
                if len (newDirList) > 0 :
                    MP_UpdateDirs (newDirList)
                    pathqueue += newDirList
                del newDirList
                
                if not niftifilefound and otherfilesfound and ProcessOtherDirPath is not None:     
                    if not FileUtil.testCanReadWriteFromDirectory (dirpath) :
                        MessageBoxUtil.showMessage ("File Permission Error", "Unable to write to the NIfTI data directory. You likely do not have file privlages to write to this location. RIL-Contour requires users level write privlages to convert found Image files to NIfTI.")
                    else:
                        ProcessOtherDirPath.append ([dirpath, ReadDirPath, []])
                        
                if len (pathqueue) == 0 and ProcessOtherDirPath is not None and len (ProcessOtherDirPath) > 0 :  
                    self._progdialog.setLabelText ("Converting imaging to NIfTI")                        
                    self._progdialog.setRange (0, len (ProcessOtherDirPath))
                    for indx, fileTuple in enumerate (ProcessOtherDirPath) :
                         self._progdialog.setValue (indx)
                         dirpath = fileTuple[0]
                         try :
                             if (DicomVolume.doesDirectoryContainDicomFiles (dirpath, IgnoreProcessedDicoms = True, App = self._ProjectDataset.getApp ())) :               
                                DicomVolume.convertDicomFilesToNIfTI (dirpath, App = self._ProjectDataset.getApp (), PreferredFileFormat = PreferredFileFormat)                                                            
                             if (DicomVolume.doesDirectoryContainImageFiles (dirpath, IgnoreProcessedDicoms = True, App = self._ProjectDataset.getApp ())) : 
                                DicomVolume.convertRGBFilesToNIfTI (dirpath, App = self._ProjectDataset.getApp (), PreferredFileFormat = PreferredFileFormat)
                         except:
                            print ()
                            print ("ERROR ERROR ERROR")
                            print ()
                    self._progdialog.setRange (0, 0)
                    self._progdialog.setLabelText ("Discovering NIfTI imaging")
                    MP_UpdateDirs (ProcessOtherDirPath)
                    pathqueue += ProcessOtherDirPath
                    ProcessOtherDirPath = None        
                                            
            _, treeRootName = os.path.split (self._RootSearchPath[0])
            self._progdialog.setRange (0,len (NodeLeaf))
            count = 0
            if len (NodeLeaf) > 0 :
                if len (self._rootMemory) == 0 :                      
                    _addTreeNode (None, self._RootSearchPath[0])
            destROIDatasetInterface = self._ProjectDataset.getROIDatasetInterface()            
            for dirpath, NodeTpl in NodeLeaf.items () :                                 
                ReadDirPath, ReadDirNodeList, initalDirectoryFiles = NodeTpl                
                if len (ReadDirNodeList) <= 0 :
                    continue
                if  	not self._progdialog.wasCanceled()  :
                    self._progdialog.setLabelText ("Copying: %s" % dirpath)                   
                    self._progdialog.setValue (count)                 
                    count += 1
                    self._ProjectDataset.getApp ().processEvents ()                   
                    lfnAPI = LongFileNameAPI (dirpath, LoadLongFileNameFileDescriptorTreePathList = True)
                    LongFileNameDatasetTreePath = None
                    if not TryOrganizeDataBySubjectAccessionExam and LongFileNameAPI.doesLFDataFileExistForPath (dirpath) :
                        LongFileNameDatasetTreePath = lfnAPI.getTreePathList ()  
                        if len (LongFileNameDatasetTreePath) > 0 :
                            del LongFileNameDatasetTreePath[0]
              
                    if LongFileNameDatasetTreePath is not None :
                        DatasetNodeList = [(LongFileNameDatasetTreePath, ReadDirNodeList)]
                    elif TryOrganizeDataBySubjectAccessionExam :
                        subjectAccessionDescriptionCatalog = DicomVolume.DicomSubjectAccessionDescriptionCatalog (ReadDirPath)
                        for niftiFile in ReadDirNodeList :
                            subject, accession, description = DicomVolume.getNIfTIDicomTagSubjectAccessionExamTags (os.path.join (dirpath, niftiFile))
                            if subject is not None and PatientIDFilter != "" and PatientIDFilter is not None :
                                subject = DicomVolume.formatSubjectID (subject, PatientIDFilter)                                 
                            
                            subject = FileUtil.makeSafeName (subject)
                            accession = FileUtil.makeSafeName (accession)
                            description = FileUtil.makeSafeName (description)
                            
                            subjectAccessionDescriptionCatalog.add (subject, accession, description, niftiFile)
                        DatasetNodeList, MissingPatientAccessionExamDicomTags = subjectAccessionDescriptionCatalog.getOrganizedDatasetList ()
                        ImagingNotImported += MissingPatientAccessionExamDicomTags
                    else:
                        DatasetNodeList = [(ReadDirPath, ReadDirNodeList)]
                    
                    for treePath, NodeList in DatasetNodeList : 
                        if len (treePath) > 0 :
                            node, residualTreePath = self.getNodeAtTreePath (treePath, ReturnPartialPath = True, FileSystemName = False)  
                        else:
                            node = None
                     
                        pathparts = path.split (os.sep)
                        endIndex = len (pathparts) - 1
                        while endIndex >= 0 and node is None:
                            while endIndex >= 0 and pathparts[endIndex] != treeRootName :
                                endIndex -= 1    
                            if endIndex >= 0 :
                                dirnames = pathparts[endIndex:]
                                node, residualTreePath = self.getNodeAtTreePath (dirnames + treePath, ReturnPartialPath = True, FileSystemName = False)  
                                endIndex -= 1
                        
                        if node is None :
                            _, RootDirName = os.path.split (self._RootSearchPath[0])
                            node, residualTreePath = self.getNodeAtTreePath ([RootDirName] + treePath, ReturnPartialPath = True, FileSystemName = False) 
                            
                        if node is  None :                        
                            node = self._rootMemory[0]
                            residualTreePath = treePath
                            
                        basePath = node.getPath ()
                        baseTreePath = node.getTreePathList (ReturnFileSystemPath = False)
                        
                        ParentNode = node
                        if len (residualTreePath) == 0 :
                            if node is None :
                                fullDirPath = self._RootSearchPath[0]
                                importDirlfnAPI = self.getLongFileNameAPI (CreateIfMissing = True)
                            else:
                                importDirlfnAPI = node.getLongFileNameAPI (CreateIfMissing = True)
                                fullDirPath = node.getPath ()
                        else:
                            if not basePath.endswith (os.sep) :
                                fullDirPath = basePath + os.sep
                            else:
                                fullDirPath = basePath
                            if len (fullDirPath + os.sep.join (residualTreePath)) <= 180 :
                                MaxLength = 180
                            else:
                                MaxLength = None
                            
                            if ParentNode is None : 
                                importDirlfnAPI = self.getLongFileNameAPI (CreateIfMissing = True)
                            else:
                                importDirlfnAPI = ParentNode.getLongFileNameAPI (CreateIfMissing = True)
                            for item in residualTreePath :
                                newShortFileName = importDirlfnAPI.createShortFileName (item, MaxLength = MaxLength)
                                fullDirPath = os.path.join (fullDirPath, newShortFileName)
                                if not os.path.isdir (fullDirPath) :
                                    os.mkdir (fullDirPath)
                                baseTreePath.append (item)                            
                                ParentNode = _addTreeNode (ParentNode, fullDirPath) 
                                importDirlfnAPI = ParentNode.getLongFileNameAPI (CreateIfMissing = True)
                                    
                        if not os.path.isdir (fullDirPath) :
                             print ("Error missing import file dir. " + fullDirPath)
                        elif not FileUtil.testCanReadWriteFromDirectory (fullDirPath) : 
                            print ("Error cannot read/write from dir. " + fullDirPath)
                        else:
                            ConvertedImageFiles = {}                        
                            for shortfilename in NodeList :
                              
                                longfilenameImported = lfnAPI.getLongFileName (shortfilename)
                                if longfilenameImported is None :
                                    print ("Error file not found.")
                                    continue
                                if  importDirlfnAPI.hasFileName (longfilenameImported) :
                                    testfile = os.path.join (fullDirPath, longfilenameImported)
                                    print ("File Allready Exists. %s" % testfile)
                                else:
                                    
                                    try :
                                        AdditionalExclusionFileNames = set ()
                                        parentDirPath = destROIDatasetInterface.getROIDataFilePathForDatasetPath (ParentNode)
                                        parentDirPath = parentDirPath[:-len ("_ROI.txt")]
                                        if os.path.isdir (parentDirPath) :
                                            for filename in  os.listdir (parentDirPath) :
                                                if filename.lower().endswith ("_roi.txt") :
                                                    AdditionalExclusionFileNames.add (filename[:-len ("_ROI.txt")])
                                                else:
                                                    filelst = filename.split (".")
                                                    if len (filelst) > 1 :
                                                        del filelst[-1]
                                                    AdditionalExclusionFileNames.add (".".join (filelst))
                                                    
                                    except:
                                        AdditionalExclusionFileNames = set ()
                                    
                                    longfilename = importDirlfnAPI.createShortFileName (longfilenameImported, MaxLength = 20, shortfilename = shortfilename, ExcludeFileNames =AdditionalExclusionFileNames)
                                    idealLongFileName = os.path.join (fullDirPath, longfilename)
                                    sourceFile = os.path.join (dirpath, shortfilename)
                                    try:
                                        shutil.copy (sourceFile,idealLongFileName) 
                                        ImagingImported.append (idealLongFileName)
                                        print ("Importing ->  %s" % os.path.join (fullDirPath, longfilenameImported))
                                        testname = sourceFile.lower ()
                                        if testname.endswith (".nii.gz") :
                                            matchextension = ".nii.gz"
                                        elif testname.endswith (".nii") :
                                            matchextension = ".nii"
                                        else:
                                            matchextension = None
                                        if matchextension is not None :
                                                                                    
                                            for n4biasExtensions in [".n4b", ".ndt"] :
                                                n4biasSourceFile = sourceFile + n4biasExtensions
                                                try:
                                                    if os.path.isfile (n4biasSourceFile) :
                                                        shutil.copy (n4biasSourceFile,idealLongFileName + n4biasExtensions) 
                                                except:
                                                    pass 
                                            try:
                                                MIRConvertDataFile =  sourceFile[:-len(matchextension)] + "_info.txt"
                                                if os.path.isfile (MIRConvertDataFile) :
                                                    DicomTagIdealLongFileName = idealLongFileName[:-len (matchextension)] + "_info.txt"
                                                    if not os.path.isfile (DicomTagIdealLongFileName) :
                                                        shutil.copy (MIRConvertDataFile,DicomTagIdealLongFileName) 
                                            except:
                                                pass
                                            dicomTagSourceFile = sourceFile[:-len(matchextension)]
                                            dicomTagSourceFile += ".DicomTags"
                                            if os.path.isfile (dicomTagSourceFile) :
                                                try :
                                                    DicomTagIdealLongFileName = idealLongFileName[:-len (matchextension)] + ".DicomTags"                                        
                                                    fileDicomDataTags = pd.read_csv (dicomTagSourceFile, header = 0,index_col=0)
                                                    foundIndex = fileDicomDataTags.loc[fileDicomDataTags['TagName']== "OrigionalImageFileName"]                                        
                                                    if len (foundIndex.index) != 1 :
                                                         shutil.copy (dicomTagSourceFile,DicomTagIdealLongFileName) 
                                                    else:
                                                         index = int (foundIndex.index[0])
                                                         origionalPath = fileDicomDataTags["TagValue"].values[index]
                                                         origionalPath = FileUtil.correctOSPath (origionalPath)
                                                         _, imageFileName = os.path.split (origionalPath)
                                                         sourceImageFileName = os.path.join (dirpath, imageFileName)
                                                         if os.path.exists (sourceImageFileName) :
                                                             destImageFileName = importDirlfnAPI.createShortFileName (imageFileName, MaxLength = 20)
                                                             destImageFileName = os.path.join (fullDirPath, destImageFileName)
                                                             shutil.copy (sourceImageFileName,destImageFileName) 
                                                             fileDicomDataTags["TagValue"].values[index] = destImageFileName
                                                             df = pd.DataFrame(fileDicomDataTags)
                                                             df.to_csv (DicomTagIdealLongFileName, index=False, index_label=False)
                                                             FileUtil.setFilePermissions (DicomTagIdealLongFileName)
                                                             ConvertedImageFiles[destImageFileName] = True
                                                except:
                                                    print ("Error occured trying to copy dicom tags")
                                        SourceROIPath = None
                                        try :
                                             if SourceROIDataDir is not None :
                                                SourceROIPathDir = SourceROIDataDir + os.sep.join (ReadDirPath)
                                                sourceROIFileName = shortfilename
                                                sourceROIFileName,_ = FileUtil.removeExtension (sourceROIFileName, [".nii",".nii.gz"])
                                                SourceROIPath = os.path.join (SourceROIPathDir,sourceROIFileName + "_ROI.txt")
                                                if not os.path.isfile (SourceROIPath) :
                                                    SourceROIPath = None          
                                        except:
                                           SourceROIPath = None
                                      
                                        SkipNodeInitalization = SourceROIPath is not None
                                        childDataNode = _addTreeNode (ParentNode, idealLongFileName, SkipNodeInitalization = SkipNodeInitalization)
                                        
                                        destinationROIPath = None
                                        try :                                    
                                            if SourceROIPath is not None and childDataNode is not None :                                        
                                                if not os.path.isfile (destROIDatasetInterface.getROIDataFilePathForDatasetPath (childDataNode)):
                                                    try:
                                                        DestinationROIDataFileHandle = destROIDatasetInterface.getROIDataForDatasetPath (childDataNode, IgnoreLock = False)                                                
                                                        if DestinationROIDataFileHandle is not None and not os.path.exists (DestinationROIDataFileHandle.getPath ()) and DestinationROIDataFileHandle.isFileHandleLocked () :
                                                               
                                                            destinationROIPath = DestinationROIDataFileHandle.getPath ()
                                                            shutil.copy (SourceROIPath, destinationROIPath)
                                                                                                                                                                                    
                                                            if ROIDictionary.contoursExistForDataSet (destinationROIPath) :
                                                                childDataNode.setROIDatasetIndicator (True, FireChildNodeChangedEvent = True, FireTreeNodeChangedEvent = True, AquireFileLock = False)
                                                    finally:
                                                        DestinationROIDataFileHandle.close ()
                                                            
                                        except:
                                            destinationROIPath = None
                                            print ("A error occured trying to copy ROI Data")
                                        
                                        if SkipNodeInitalization and childDataNode is not None :
                                            _initNode (childDataNode, destinationROIPath) 
                                        
                                    except:
                                        print ("Error occured trying to copy data")
                            if len (ConvertedImageFiles) > 0 :
                                try :
                                    convertedPaths = os.path.join (dirpath,"ConvDicoms.dat")                  
                                    try :
                                        with open (convertedPaths,"rt") as infile :
                                            previousData = json.load (infile)
                                    except:
                                        previousData = {}
                                    for key, value in ConvertedImageFiles.items () :
                                        previousData[key] = value
                                   
                                    with open (convertedPaths,"wt") as outfile :
                                        json.dump (previousData, outfile)
                                    FileUtil.setFilePermissions (convertedPaths)
                                except:
                                    print ("Error occured updating ConvDicoms.dat")
                try :
                    fileDirectories = ScanDir.scandir (dirpath)
                    for fp in fileDirectories :
                        if fp.path not in initalDirectoryFiles :
                            try:
                                os.remove (fp.path)
                            except:
                                pass
                finally:
                    try :
                        fileDirectories.close ()
                    except:
                        pass
            primarySortDirection = self.getPrimaryColumnSortDirection ()
            if primarySortDirection is not None :
                self.setPrimaryColumnSortDirection (primarySortDirection, ForceSort = True)
            secondaryColumnName = self._ColumnSort.getSecondaryColumnSortColumnName ()
            if secondaryColumnName is not None :
                tagidentifer = self._ColumnSort.getSecondaryColumnTagIdentifier ()            
                secondarySortDirection = self.getSecondaryColumnSortDirection (secondaryColumnName, tagidentifer)                        
                if secondarySortDirection is not None : 
                    self.setSecondaryColumnSortDirection (secondaryColumnName, secondarySortDirection, tagidentifer, ForceSort = True) 
                                                    
            self._rootNodes = []
            for node in self._rootMemory :
                RCC_NiftiFileSystemDataInterface._addRootNodes (node, self._rootNodes)
            for nodes in self._rootNodes :
                nodes.delayedCacheLoading (AquireFileLock = False)
                        
        finally:
            self.layoutChanged.emit ()            
            self._progdialog.close()  
            del self._progdialog
            del self._NodeUpdateQue # clean up only used for starting.                  
            self.getLongFileNameCache().UnloadLongFileNameCache ()                
            self._ProjectDataset.getProjectFileInterface().releaseFileLock ()           
            print ("done")                
            return ImagingImported, ImagingNotImported
            
            
                            
    
        
        
        
        
    #End import Nifti
        
    def getLongFileNameAPI (self, CreateIfMissing = False) :
        if self._longFileNameAPI is None and CreateIfMissing : 
            rootNode = self._RootSearchPath[0]            
            self._longFileNameAPI = LongFileNameAPI (rootNode, TreePathList = rootNode.self.getTreePathList (ReturnFileSystemPath = False), CreateIfMissing = True,LongfileNameCache = self._LongFileNameCache)
        return self._longFileNameAPI
    
    def getFileSysetmEventHandler (self) :
         return self._watchDogEventHandler
    
    #def _removeFinalizer (self) :
    #    if self._finalizer is not None :            
    #        self._finalizer.detach()
    #        self._finalizer = None 
    
    def _setDisableNodeUpdateLayoutChange (self, val) :
        self._DisableNodeUpdateLayoutChange = val
        
    def _isNodeUpdateLayoutChangeDisable (self):
        return self._DisableNodeUpdateLayoutChange 
    
    def treeNodeChanged (self, node) :
        if node.isVisible () :
            if (not self._isNodeUpdateLayoutChangeDisable ()) :
                self.layoutAboutToBeChanged.emit ()            
                nodechanged = self.createIndex(node.getRow (), 0, node)                
                self.dataChanged.emit (nodechanged, nodechanged)
                self.changePersistentIndex(nodechanged, nodechanged)
                self.layoutChanged.emit ()  
          
        
    @staticmethod 
    def getLockFileState (path, UserUID) :
        if path is None or not os.path.exists (path) :
            return "unlocked"
        try :
            lockSignature = NonPersistentFSLock.getLockFile (path)
            if lockSignature is not None and "UserUIDCreatedLock" in lockSignature and lockSignature["UserUIDCreatedLock"] == UserUID :
                return "locked_self"
            return "locked_other"
        except:
            return "locked_other"
    
    def close (self) :
        #RCC_NiftiFileSystemDataInterface._finalizeObj (self._ProjectDataset.getWatchDogDirectoryObserver (), self._observedWatchDogWatch)
        #self._observedWatchDogWatch = None
        #self._removeFinalizer ()
        pass
        
    @staticmethod
    def _finalizeObj (WatchDogDirectoryObserver, observedWatchDogWatch) :
         try:
             if observedWatchDogWatch is not None and WatchDogDirectoryObserver is not None :
                 WatchDogDirectoryObserver.unschedule (observedWatchDogWatch)
         except Exception as exp :
             print (str(exp))
             print ("")
             import traceback
             traceback.print_stack ()
             print ("A error occured removing the watchdog observer")
         print ("Directory monitor unloaded.") 
        
    """if PythonVersionTest.IsPython2 () :
        def __del__ (self) :
           RCC_NiftiFileSystemDataInterface._finalizeObj (self._ProjectDataset.getWatchDogDirectoryObserver (), self._observedWatchDogWatch)"""
        
        
    def setPathLocked (self, LockFileNameList, FileTypeList, treeNode = None):                
        if isinstance (LockFileNameList, str) :
            LockFileNameList = [LockFileNameList]
        if isinstance (FileTypeList, str) :
            FileTypeList = [FileTypeList]
        length = len (FileTypeList)        
        if length > 0 :
            for index in range (length) :
                try:
                    LockFileName = LockFileNameList[index]
                    FileType = FileTypeList[index]
                    if treeNode is None :
                        rootPath = self._ROI_Dataset.getROIDatasetRootPath ()  
                        rootPath = FileUtil.getPathList (rootPath)                
                        pathLocked = FileUtil.getPathList (LockFileName) 
                        pathLocked = pathLocked[len (rootPath):]
                        if len (pathLocked) > 0 :
                            treeNode = self.getPathTreeNode (pathLocked)
                    if treeNode is not None :
                         if FileType == "NonPersistentFSLock" :                     
                             lockState = RCC_NiftiFileSystemDataInterface.getLockFileState (LockFileName, self._ProjectDataset.getUserUID ())
                             treeNode.setROIFileLocked (lockState, setParent= True)
        
                         elif FileType == "PersistentFSLock" :
                             pLock = PersistentFSLock.getCurrentPLockFromPath (LockFileName)
                             treeNode.setNodePersistentFileLock (pLock)                           
                             
                         elif FileType == "DataCache" :                                                                  
                             roiDatasetPath = ProjFileInterface.getROIFilePathFromFileCompressedCache (LockFileName)
                             treepath = treeNode.getTreePath ()
                             DataCacheFileChanged = False
                             projectFileIFace = self._ProjectDataset.getProjectFileInterface()
                             projectFileIFace.aquireFileLock ()
                             try :
                                 HasDatasetCacheChanged = projectFileIFace.getProjectDatasetCache (treepath, Key = "DataCacheChange", Default="None", AquireFileLock = False) 
                                 if HasDatasetCacheChanged == "None" or HasDatasetCacheChanged is None or  HasDatasetCacheChanged.hasFileChanged ( LockFileName,IgnoreFileName = True) :
                                     projectFileIFace.setProjectDatasetCache ( treepath, FileChangeMonitor (LockFileName), Key = "DataCacheChange", AquireFileLock = False) 
                                     cachedResult ,compressedCachedResult = ProjFileInterface.loadDatasetCompressedCache (roiDatasetPath, ReturnCompressedAlso = True)
                                     if cachedResult is not None and projectFileIFace.isCacheDifferent (treepath, compressedCachedResult, AquireFileLock = False) :
                                          if cachedResult is not None and RCC_NiftiFileSystemDataInterface.isValidCache(cachedResult) :
                                               nLPLockPath = NonPersistentFSLock.getFileLockPathForROIFilePath (roiDatasetPath)
                                               cachedResult["NodeLocked"] = RCC_NiftiFileSystemDataInterface.getLockFileState (nLPLockPath, projectFileIFace.getUserUID())
                                               cachedResult["PLock"] = PersistentFSLock.getCurrentPLockFromPath (PersistentFSLock.getPersistentLockPathForROIFilePath (roiDatasetPath))                                               
                                               projectFileIFace.setProjectDatasetTagCache (treepath, cachedResult, AquireFileLock = False)
                                               self._updateTreeNodeStateForCacheResult (treeNode, cachedResult, AquireFileLock = False)
                                               DataCacheFileChanged = True
                                 if not DataCacheFileChanged :
                                       nLPLockPath = NonPersistentFSLock.getFileLockPathForROIFilePath (roiDatasetPath)
                                       lockedState = RCC_NiftiFileSystemDataInterface.getLockFileState (nLPLockPath, projectFileIFace.getUserUID())
                                       treeNode.setROIFileLocked (lockedState, setParent = True)      
                                       pLock = PersistentFSLock.getCurrentPLockFromPath (PersistentFSLock.getPersistentLockPathForROIFilePath (roiDatasetPath))                            
                                       treeNode.setNodePersistentFileLock (pLock) 
                             finally:
                                projectFileIFace.releaseFileLock ()
                                
                         #FireChildNodeChangeEvent= True, FireTreeNodeChangeEvent = True, AquireFileLock = True
                    
                         #nodechanged = self.createIndex(treeNode.getRow (), 0, treeNode)
                         #self.dataChanged.emit (nodechanged, nodechanged)
                         #self.changePersistentIndex(nodechanged, nodechanged)                     
                except :
                    try :
                        print ("A exception occured trying to lock path: " + LockFileName)            
                    except:
                        print ("A exception occured trying to lock path")            
            if self._ProjectDataset is not None :
                dlg = self._ProjectDataset.getNiftiDatasetDlg ()
                if dlg is not None :
                    dlg.update ()
            
        
    def rebuildTree (self) :
        self.layoutAboutToBeChanged.emit ()  
        self.layoutChanged.emit ()         

    
    def getInterfaceName (self) :
        return "RCC_NiftiFileSystemDataInterface"
    
    def getRootNodeList (self) :
        txtLst = []
        for node in self._rootMemory :
           txtLst.append (node.getText ()) 
        return txtLst
    
    def getPathTreeNode (self, nodePath) :
        root = self._rootMemory 
        testName = None
        for index, findName in enumerate (nodePath) :
            if NonPersistentFSLock.isFilePathALockFile (findName) and index == len (nodePath) -1 :  #small hack to enable finding lock files
                testName = findName [:-len ("lock")]
            elif PersistentFSLock.isFilePathAPLockFile (findName) and index == len (nodePath) -1 :  #small hack to enable finding lock files
                testName = findName [:-len ("plock")]
            elif ProjFileInterface.isFileCompressedCache (findName) and index == len (nodePath) -1 :  #small hack to enable finding lock files
                testName = findName [:-len ("_DSCache.bin")] + "."
            for item in root :
                if testName is not  None :
                    suffix = ""
                    itemName = item.getFilename ()
                    litemName = itemName.lower()
                    if litemName.endswith (".nii.gz") :
                        suffix = itemName[len (itemName) - 6:]
                    elif litemName.endswith (".nii") :
                        suffix = itemName[len (itemName) - 3:]
                    if itemName == testName + suffix :
                        return item
                else:
                    if item.getFilename () == findName :
                        if index == len (nodePath) - 1 :
                            return item
                        else:
                            root = item.getChildernLst ()
        return None
    
    def getPathQModelIndex (self, nodePath) :
        root = self._rootMemory 
        for index in range (len (nodePath)) :
            findName = nodePath[index]
            for item in root :
                if item.getFilename () == findName :
                    if index == len (nodePath) - 1 :
                        if not item.isVisible () :                            
                            NodeList = []
                            item.setVisible (True)
                            NodeList.append (item)
                            parent = item.getParent ()
                            while parent is not None :           
                                NodeList.append (parent)
                                parent = parent.getParent  ()
                            NodeList.reverse()
                            self.layoutAboutToBeChanged.emit ()   
                            for node in NodeList :                             
                                nodechanged = self.createIndex(node.getRow (), 0, node)
                                self.dataChanged.emit (nodechanged, nodechanged)
                                self.changePersistentIndex(nodechanged, nodechanged)
                            self.layoutChanged.emit ()                             
                        return self.createIndex (item.getRow (), 0, item)
                    else:
                        root = item.getChildernLst ()
        return None
            
    def getRootMemory (self) :
        return self._rootMemory  
    
    @staticmethod
    def getRootMemorySortKey (node) :
        if len(node[0].getChildernLst ()) == 0 :
            return "a" + node[0].getDisplayText ()
        return "b" + node[0].getDisplayText ()
    
    def getSecondaryColumnSortDirection (self, ColumnTypeName, TagIndicator) :
        return self._ColumnSort.getSecondaryColumnSortDirection (ColumnTypeName, TagIndicator)
    
    def getPrimaryColumnSortDirection (self) :
        return self._ColumnSort.getPrimaryColumnSortDirection ()
    
    def setPrimaryColumnSortDirection (self, direction, ForceSort = False) :
        if (self._ColumnSort.setPrimaryColumnSortDirection (direction) or ForceSort) :
            sortList = []
            TreeViewWidget = self.getTreeViewWidget ()
            for child in self._rootMemory :                
                if child.isVisible () :
                    nodechanged = self.createIndex(child.getRow (), 0, child)
                    nodeexpanded = TreeViewWidget.isExpanded (nodechanged)
                else:
                    nodeexpanded = False
                sortList.append ((child, nodeexpanded))
            sortList = sorted (sortList, key = RCC_NiftiFileSystemDataInterface.getRootMemorySortKey)
            if direction < 0 :
                 sortList.reverse ()  
            self._rootMemory = []
            visibleIndex = 0
            for node_tuple in sortList :
                node, expanded = node_tuple
                self._rootMemory.append (node)
                node.setPrimaryColumnSortDirection (direction, ForceSort = ForceSort) 
                if node.isVisible () :
                    if node.setRow (visibleIndex) :             
                        self.layoutAboutToBeChanged.emit ()                       
                        nodechanged = self.createIndex(visibleIndex, 0, node)
                        self.dataChanged.emit (nodechanged, nodechanged)
                        self.changePersistentIndex(nodechanged, nodechanged)
                        self.layoutChanged.emit () 
                        visibleIndex +=  1
            for node_tuple in sortList :
                node, expanded = node_tuple
                if node.isVisible () :
                    nodechanged = self.createIndex(node.getRow (), 0, node)
                    TreeViewWidget.setExpanded (nodechanged, expanded)


        
    def _filterNodeText (self, child, childlist, filterText, nodeChangeList, nodeRemoveList, nodeAddList, expandedNodeList, ForceVisible = False, AdvancedFilterOptions = "AllImageFiles") :                
        ChildNodeContainsFilteredText = False
        if child is not None :
            lowerChildText = child.getDisplayText ().lower()
            if isinstance (filterText, str) :
                ChildNodeContainsFilteredText = filterText in lowerChildText
            elif isinstance (filterText, list) :
                for subText in filterText :
                    if subText in lowerChildText :
                        ChildNodeContainsFilteredText = True
                        break
        isChildVisible = ForceVisible or ChildNodeContainsFilteredText
        isVisible = isChildVisible
        if (child is not None and child.describesNiftiDataFile () and not child.meetsAdvancedFilterOptions (AdvancedFilterOptions)) :
            isVisible = False            
        VRowCount = 0
        for ch in childlist :                               
            if ch.isVisible () :
                VRowCount += 1                
        
        NewVisRowCount = 0
        if len (childlist) > 0 : 
            TreeViewWidget = self.getTreeViewWidget()
            isVisible = False            
            for ch in childlist :                               
                if ch.isVisible () :                    
                    nodeexpanded = TreeViewWidget.isExpanded (self.createIndex( ch.getRow (), 0, ch))
                else:
                    nodeexpanded = False 
                
                self._filterNodeText (ch, ch.getChildernLst () , filterText, nodeChangeList, nodeRemoveList, nodeAddList, expandedNodeList, ForceVisible = isChildVisible, AdvancedFilterOptions = AdvancedFilterOptions)
                if ch.isVisible () :                    
                     isVisible = True                        
                     if ch.setRow (NewVisRowCount) : 
                        if NewVisRowCount < VRowCount :                        
                            nodeChangeList.append (ch)                           
                        expandedNodeList.append ((ch, nodeexpanded))                
                     NewVisRowCount += 1 
            
        if NewVisRowCount > VRowCount :
            nodeAddList.append ((child, VRowCount, NewVisRowCount))
        elif NewVisRowCount < VRowCount:
            nodeRemoveList.append ((child, VRowCount, NewVisRowCount))
        
        if child is not None : 
            child.setVisible (isVisible, UpdateParent = False)        
        
    def setTreeViewWidget (self, widget) :
        self._treeViewWidget = widget
        
    def getTreeViewWidget (self) :
        return self._treeViewWidget
    
    def setFilterText (self, filterText, advancedFilterOptions) :        
        filterText = filterText.strip ().lower()
        if self._ColumnTextFilter == filterText and self._OptionsFilter == advancedFilterOptions :
            return False         
        self._ColumnTextFilter = filterText  
        self._OptionsFilter = advancedFilterOptions
        
        nodeChangeList = []
        nodeRemoveList = []
        nodeAddList = []
        expandedNodeList = []        
                
        try:
            testList = []
            currentWord = None
            testsplit = shlex.split (filterText, posix=False)
            for word in testsplit :                
                if word in ["or",","] :
                    if currentWord is None :
                        currentWord = word 
                    else:
                        testList.append (currentWord)
                        currentWord = None
                else:            
                    if len (word) > 1 :
                        for delim in ["'", '"'] :
                            if word.startswith (delim) and word.endswith (delim) :
                                word = word[1:-1]
                                break
                    if len (word) > 0 :
                        if currentWord is None :
                            currentWord = word 
                        else:                        
                            currentWord += " " + word
            if currentWord is not None :
                testList.append (currentWord)
            if len (testList) > 0 :
                filterText = testList
        except:
            pass
        
        self._filterNodeText (None, self._rootMemory, filterText, nodeChangeList, nodeRemoveList, nodeAddList, expandedNodeList, ForceVisible = False, AdvancedFilterOptions = advancedFilterOptions)
                

        self.layoutAboutToBeChanged.emit ()                                   
        for tpl in nodeAddList :                                
            node,VRowCount, NewVisRowCount = tpl
            start = VRowCount
            end   = NewVisRowCount - 1
            if node is not None :
                nodechanged = self.createIndex(node.getRow (), 0, node)                
            else:
                nodechanged = QModelIndex ()
            self.rowsAboutToBeInserted.emit (nodechanged, start, end)
            self.rowsInserted.emit (nodechanged, start, end)

        for tpl in nodeRemoveList :                                
            node,VRowCount, NewVisRowCount = tpl
            start = NewVisRowCount 
            end   = VRowCount - 1
            if node is not None :
                nodechanged = self.createIndex(node.getRow (), 0, node)
            else:
                nodechanged = QModelIndex ()
            self.rowsAboutToBeRemoved.emit (nodechanged, start, end)
            self.rowsRemoved.emit (nodechanged, start, end)
            
        for node in nodeChangeList :                                
            nodechanged = self.createIndex(node.getRow (), 0, node)
            self.dataChanged.emit (nodechanged, nodechanged)
            self.changePersistentIndex(nodechanged, nodechanged)
        self.layoutChanged.emit ()         
        
        if len (expandedNodeList) > 0 :
            TreeViewWidget = self.getTreeViewWidget ()
            for node_tuple in expandedNodeList :
                node, expanded = node_tuple            
                nodechanged = self.createIndex(node.getRow (), 0, node)
                TreeViewWidget.setExpanded (nodechanged, expanded)
        
            
    @staticmethod
    def getSecondaryColumnTupleSortKey (node) :
        if node[1] is not None :
            return node[1]
        return ""
    
    def setSecondaryColumnSortDirection (self, columnName, direction, tagTagIdentifer, ForceSort = False) :
        if (self._ColumnSort.setSecondaryColumnSort (columnName, direction, tagTagIdentifer) or ForceSort) :
            #newLst = []
            IgnoreBlank = ""
            sortParent = False        
            if columnName == "Tag" :
                if self._ProjectDataset is not None :
                    tagfound = self._ProjectDataset.getProjectTagManger ().getTagByIdentifer (tagTagIdentifer)
                    if tagfound is not None and tagfound.isInternalTag () :
                        name = tagfound.getName ()
                        if name in ["File owner","File status"] :
                            sortParent = True     
                            IgnoreBlank = "3"

            # for single level projects enable model sorting if no child nodes.
            try :
                if not sortParent  :
                    setSortParentTrue = True
                    for root in self._rootMemory :                                        
                        for node in root.getChildernLst () :
                            if len (node.getChildernLst ()) > 0 :
                                setSortParentTrue = False
                                break     
                        sortParent = setSortParentTrue
            except:
                pass
            
                      
            if sortParent :
                for rootnode in self._rootMemory :
                    rootnode.setSecondaryColumnSortDirection (columnName, direction, tagTagIdentifer, IgnoreBlank = IgnoreBlank, ForceSort = ForceSort) 
            else:
                for rootnode in self._rootMemory :
                    if len (rootnode.getChildernLst ()) > 0 :
                        for node in rootnode.getChildernLst () :
                            node.setSecondaryColumnSortDirection (columnName, direction, tagTagIdentifer, IgnoreBlank = IgnoreBlank, ForceSort = ForceSort) 
                    else:
                        rootnode.setSecondaryColumnSortDirection (columnName, direction, tagTagIdentifer, IgnoreBlank = IgnoreBlank, ForceSort = ForceSort) 
            
                        

    
    def _loadDatasetCanceled (self) :
        try :
            if (self.filesystemLoader is not None) :
                self.filesystemLoader.stoploading ()                
        except :
            print ("A error occured trying to cancel the filesystem NIfTI loading thread.")
        
    def updateLoadingProgress (self, label, progressvalue) :
        if label == "" :            
            self._progdialog.setLabelText ("Reading directory")        
        else:
            if (progressvalue == 0) :
                self._progdialog.setLabelText ("Reading directory: " + label)        
            else:
                self._progdialog.setLabelText ("Loading files in: " + label)        
                print  ("Loading files in: " + label)
        self._progdialog.setValue (progressvalue)      
    
    def doneLoading (self, finalDirCount) :
        self._datasetDirectoryCount = finalDirCount
  
    def setTempDir (self, path) : # do nothing
        return 
    
    def getRegistrationDayFilter (self):
       return self._registrationUIFilterDate 
   
    def setRegistrationDayFilter (self, datetuple = (None,None,None)):
       self._registrationUIFilterDate = datetuple       
   
    def isRegistrationDateOnOrAfter (self, date) :
        filterMonth, filterDay, filterYear =self.getRegistrationDayFilter ()
        #if nothing filtered
        if (filterMonth is None or filterDay is None or filterYear is None) :
            return True
        testMonth, testDay, testYear = date
        if (testMonth is None or testDay is None or testYear is None) :
            return False
        if (filterYear > testYear) :            
            return False
        if (filterMonth > testMonth) :            
            return False
        if (filterDay > testDay) :            
            return False
        return True
    
    def _setFirstRegistrationDate (self, date):
        if (date[0] is None) :
            return
        if (self._FirstRegistrationUIFilterDate[0] == None) :
            self._FirstRegistrationUIFilterDate = date
            return
        else:
            filterMonth, filterDay, filterYear =  self._FirstRegistrationUIFilterDate
            testMonth, testDay, testYear = date            
            if (filterYear < testYear) :            
                return 
            if (filterMonth < testMonth) :            
                return 
            if (filterDay < testDay) :            
                return
            self._FirstRegistrationUIFilterDate = date
        
    def getFirstRegistrationDate (self) :
       return self._FirstRegistrationUIFilterDate
   
    def doesTreeContainANTSRegisteredDataset (self) :
        return self._treeContainsANTSRegisteredDatasets
    
    def getAllTreeNodes(self, Memory = None)    :
        TreeNodeList = []
        if Memory == None :        
            Memory = self._rootMemory        
        for interalData in Memory :            
            if (interalData.getChildCount () == 0):                
                TreeNodeList.append ((interalData, []))
            else:
                TreeNodeList.append ((interalData, self.getAllTreeNodes (Memory = interalData.getChildernLst () ) ))            
        return (TreeNodeList)
        
        
    def getNiftiTreeNodeList (self, Memory = None, AllFiles = False, AquireFileLock = True, TreeNodeSet = None) :                
        if AquireFileLock :
            self._ProjectDataset.getProjectFileInterface().aquireFileLock ()
        try :            
            if TreeNodeSet is None :
                TreeNodeList = set ()        
                RemoveDuplicateNodes = True
            else:
                TreeNodeList = TreeNodeSet      
                RemoveDuplicateNodes = False
            if Memory is None :        
                Memory = self._rootMemory
            elif RemoveDuplicateNodes :
                NewList = []
                if len (Memory) >  1 :
                    NodeSet = set (Memory)
                    for node in NodeSet :
                        parent = node.getParent ()
                        while parent is not None and parent not in NodeSet :
                            parent = parent.getParent ()
                        if parent is None :
                            NewList.append (node)
                    Memory = NewList 
            for interalData in Memory :     
                #print ("Processing: " + interalData.getText ())
                if (AllFiles or interalData.getNodeROIDatasetIndicator (AquireFileLock = False)) :
                    if (interalData.describesNiftiDataFile ()):                
                        TreeNodeList.add (interalData)
                    elif (interalData.getChildCount () > 0):                
                        self.getNiftiTreeNodeList (Memory = interalData.getChildernLst (), AllFiles =AllFiles, AquireFileLock = False, TreeNodeSet = TreeNodeList)                                        
            if RemoveDuplicateNodes :
                return list (TreeNodeList)
        finally:                 
            if AquireFileLock :
                self._ProjectDataset.getProjectFileInterface().releaseFileLock ()
    
    def getNodeAtTreePath (self, treePath, ReturnPartialPath = False, FileSystemName = True):
        treePath = copy.copy (treePath)
        treePath.reverse()
        childMemory = self._rootMemory
        parentNode = None
        while len (treePath) > 0 :
            name = treePath.pop ()
            nodeFound = False
            for node in childMemory :
                if FileSystemName :
                    nodeName = node.getFilename ()
                else:
                    nodeName =  node.getText ()
                if (nodeName == name) :
                    if len (treePath) > 0 :
                        parentNode = node
                        childMemory = node.getChildernLst ()
                        nodeFound = True
                        break
                    else:
                        if ReturnPartialPath :
                             return node, []
                        return node
               
            if not nodeFound :
                if ReturnPartialPath :
                   treePath.append (name)
                   treePath.reverse ()
                   return parentNode, treePath   
                return None
       
                
    def findFileInDataset (self, path) :
        self._ProjectDataset.getProjectFileInterface().aquireFileLock ()
        try :
            filePath = FileUtil.getPathList (path)
            rootSearchPath = FileUtil.getPathList (self._RootSearchPath[0])
            for index in range (len (rootSearchPath)) :
                if rootSearchPath[index] != filePath[index] :
                    return None 
            filePath = filePath[max (len (rootSearchPath)-1, 0):]
            node = self.getNodeAtTreePath (filePath)
            if (node is not None and node.getPath () == path) :
                return node
            return None
        except:
            return None
        finally:                         
            self._ProjectDataset.getProjectFileInterface().releaseFileLock ()
      
    def getRootSearchPath (self):
        try:
            return self._RootSearchPath[0]
        except:
            return None
        
    def getPesscaraContex (self) :
        return "RILC_" + self._ROI_Dataset.getProjectName ()
        
    def getFileObject (self) :
        fobj = FileObject ("RCC_NiftiFileSystemDataInterface")
        fobj.setParameter ("RootSearchPath", self._RootSearchPath)  
        fobj.setParameter ("DatasetDirectoryCount", self._datasetDirectoryCount)  
        fobj.setParameter ("RelativeRootSearchPathList", RelativePathUtil.relativePathListCopy (self._relativeRootPathList)) 
        fobj.addInnerFileObject (self._DatasetUIHeader.getFileObject ())
        return (fobj)
    
        
    def getNiftiDatasetRootPath (self)  :
        if (len (self._rootMemory) > 0) :
            filename, path = os.path.split (self._rootMemory[0].getPath ())
            return (path)
        return ("No Nifiti Files Loaded From File System")
        
    #MP_UpdateTree = staticmethod (MP_UpdateTree)
     
    isValidCache = staticmethod (isValidCache)
    
    def _updateTreeNodeStateForCacheResult (self, node, blockResult, FireChildNodeChangeEvent= True, FireTreeNodeChangeEvent = True, AquireFileLock = True, SetParentNodeLocked = True) :
        contoursExist, antsLog, scanPhase, tags, fileobj = blockResult["ContoursExist"], blockResult["AntsLog"], blockResult["ScanPhase"], blockResult["Tags"], blockResult["FileObj"]
        if AquireFileLock :
             self._ProjectDataset.getProjectFileInterface().aquireFileLock ()
        try :
            CallFireChildNodeChangeEvent = False
            CallFireNodeChangeEvent = False            
            if (antsLog is not None) :
                if (antsLog.hasRegisteredROI ()) :                        
                    node.setANTSRegisteredLog (antsLog, AquireFileLock= False)
                    self._treeContainsANTSRegisteredDatasets = True
                    self._setFirstRegistrationDate(node.getAntsRegisteredDateTuple(AquireFileLock= False))
            if (tags is not None) :
                    NChangeEvent = node.setTags (tags, FireTreeNodeChangedEvent = False, AquireFileLock = False)
                    if NChangeEvent :
                        CallFireNodeChangeEvent = True
                    if (tags.hasInternalTag ("ML_DatasetDescription")):
                        CNodeChangeEvent = node.setMLPredictedScanPhaseTxt (tags.getInternalTag ("ML_DatasetDescription"))                           
                        if CNodeChangeEvent :
                            CallFireChildNodeChangeEvent = True
            
            if (contoursExist or  scanPhase != "Unknown"):
                CNodeChangeEvent, NChangeEvent = node.setROIDatasetIndicator (True, FireChildNodeChangedEvent = FireTreeNodeChangeEvent, FireTreeNodeChangedEvent = False, AquireFileLock = False)
                if CNodeChangeEvent :
                    CallFireChildNodeChangeEvent = True
                if NChangeEvent :
                    CallFireNodeChangeEvent = True
                NChangeEvent = node.setScanPhaseTxt (scanPhase, AquireFileLock = False, FireChildNodeChangeEvent =False, FireTreeNodeDescriptionChangeEvent = False)                 
                if NChangeEvent :
                    CallFireNodeChangeEvent = True
    
            if "NodeLocked" in blockResult :       
                lockedState = blockResult["NodeLocked"]
                CNodeChangeEvent, NChangeEvent = node.setROIFileLocked (lockedState, setParent = SetParentNodeLocked, AquireFileLock = False, FireTreeNodeChangeEvent = False)  
                if CNodeChangeEvent :
                    CallFireChildNodeChangeEvent = True
                if NChangeEvent :
                    CallFireNodeChangeEvent = True
            
            if "PLock" in blockResult :
                plockedState = blockResult["PLock"]                
                CNodeChangeEvent, NChangeEvent = node.setNodePersistentFileLock (plockedState, FireChildNodeChangeEvent= False, FireTreeNodeChangeEvent = False, AquireFileLock = False)
                if CNodeChangeEvent :
                    CallFireChildNodeChangeEvent = True
                if NChangeEvent :
                    CallFireNodeChangeEvent = True            
            if (CallFireNodeChangeEvent or CallFireChildNodeChangeEvent) and FireTreeNodeChangeEvent :
                node.fireTreeNodeChangedEvent ()                
            if CallFireChildNodeChangeEvent and FireChildNodeChangeEvent :                
                node.callNodeChangedPLockStatus (AquireFileLock = False)                             
        finally:             
            if AquireFileLock :
                self._ProjectDataset.getProjectFileInterface().releaseFileLock ()
        if (fileobj is not None) :
            if (fileobj.hasParameter ("PESSCARA_SubjectCode") and fileobj.hasParameter ("PESSCARA_ExamCode") and fileobj.hasParameter ("PESSCARA_SeriesCode") and fileobj.hasFileObject ("RCC_PesscaraInterface")) : 
                pescaraInterfaceDescription = fileobj.getFileObject ("RCC_PesscaraInterface")
                node.setPesscaraDataSetSource (pescaraInterfaceDescription, fileobj.getParameter ("PESSCARA_SubjectCode"), fileobj.getParameter ("PESSCARA_ExamCode"), fileobj.getParameter ("PESSCARA_SeriesCode"))                                                                                    
    
    @staticmethod
    def initalizeLoadedAllNodesPLockStatusAndDescription (layerList, AquireFileLock = True, ProgressDialog = None) :                  
        nodePLockStateSet = set ()
        nodeDescription = "Unknown"
        for node in layerList :
            nodeChildern = node.getChildernLst ()
            if len (nodeChildern)  > 0 :
                childPLockState, childDescription = RCC_NiftiFileSystemDataInterface.initalizeLoadedAllNodesPLockStatusAndDescription (nodeChildern , AquireFileLock = AquireFileLock, ProgressDialog = ProgressDialog)
                if len (childPLockState) == 1 : # All nodes are same
                    nodeLockedState = childPLockState.pop ()
                elif "UserLocked" in childPLockState :
                    nodeLockedState = "UserLocked"
                elif "UserGroupLocked" in childPLockState :
                    nodeLockedState = "UserGroupLocked"
                elif "Unlocked" in childPLockState :
                    nodeLockedState = "Unlocked"
                else:
                    nodeLockedState = "OtherUserLocked"
                nodePLockStateSet.add (nodeLockedState)
                
                if nodeDescription != "Multiple" and childDescription != "Unknown":
                    if nodeDescription == "Unknown" :
                        nodeDescription = childDescription 
                    elif nodeDescription != childDescription :
                        nodeDescription = "Multiple"
                
                if not node.isDelayedCacheLoadingSet () :
                    node.setDescription (childDescription, AquireFileLock = AquireFileLock, FireTreeNodeChangedEvent = False, FireTreeNodeDescriptionChangeEvent = False) 
                    if (nodeLockedState != node.getCurrentUserPLockStatus (AquireFileLock = AquireFileLock, UseDecompressionCache = False)) :                    
                        node._setCurrentUserPLockStatus (nodeLockedState, FireTreeNodeChangedEvent = True, AquireFileLock = AquireFileLock)                     
            else:    
                if nodeDescription != "Multiple" :
                    nodePLockStateSet.add (node.getCurrentUserPLockStatus (AquireFileLock = AquireFileLock))       
                    nD = node.getDescription (AquireFileLock = AquireFileLock)                    
                    if nodeDescription != "Multiple" and nD != "Unknown":
                        if nodeDescription == "Unknown" :
                            nodeDescription = nD 
                        elif nodeDescription != nD :
                            nodeDescription = "Multiple"                        
                else:
                    nodePLockStateSet.add (node.getCurrentUserPLockStatus (AquireFileLock = AquireFileLock, UseDecompressionCache = False))                           
                if ProgressDialog is not None :
                    val = ProgressDialog.getValue ()
                    ProgressDialog.setValue (val + 1)
        return nodePLockStateSet, nodeDescription
     
        
    

    
    @staticmethod       
    def MP_hasContours ( path) :
        try :
            return ROIDictionary.contoursExistForDataSet_ReturnHeader (path)[0]
        except:
            return False
    
   
       


    def Run_MP_UpdateTree (self, progdialog) :
        
        def _getFileState (tpl) :
            if tpl is None :
                return None 
            ROIFilePath, compressedFilePath, DatasetCache = tpl                        
            ROIExists = os.path.exists (ROIFilePath)            
            DatasetChanged = True
            if ROIExists and "DataCacheChange" in DatasetCache :                                 
                datasetChange = DatasetCache["DataCacheChange"]                                
                if DatasetCache["DataCacheChange"] is not None :                                       
                    try :
                        DatasetChanged = datasetChange.hasFileChanged (compressedFilePath, IgnoreFileName = True) 
                    except:
                        pass
            return ROIExists, DatasetChanged
        
        def getPLockStatus (ProjectFI, treePath, DatasetCache) :
            pLock = None
            if "PLock" not in DatasetCache or DatasetCache["PLock"] is None or ProjectFI.forceReloadFileSystemCache() :   
                if "PLockInit" not in DatasetCache or ProjectFI.forceReloadFileSystemCache() :
                    try:
                        lockFilePath = PersistentFSLock.getPersistentLockPathForROIFilePath (ROIFilePath)
                        if os.path.exists (lockFilePath) :
                            pLock = PersistentFSLock.getCurrentPLockFromPath (lockFilePath)
                        ProjectFI.setProjectDatasetCache (treePath, True, Key="PLockInit", AquireFileLock = False)
                    except:
                        pLock = None   
            else:
                pLock = DatasetCache["PLock"]
            return pLock
                    
        if (len (self._NodeUpdateQue) <= 0) :
            return
        cachedResultList = []        
        progdialog.setLabelText ("Building file tree")        
        progdialog.setMaximum (len (self._NodeUpdateQue) * 4)   
        progdialog.setValue (0)                  
        
        #allocate FSUID before loading DS
        ProjectFI = self._ProjectDataset.getProjectFileInterface()
        FSUID = ProjectFI.getFileSystemUID ()
        print ("Started FSUID: " + FSUID)        
        treeList = []
        userUID = self._ProjectDataset.getUserUID ()  
        ProjectFI.aquireFileLock ()
        self._rootNodes = []
        try:
            App = self._ProjectDataset.getApp ()
            for tupIndex, tupl in enumerate (self._NodeUpdateQue)  :
                if (progdialog.wasCanceled ()) :
                    break
                node, datasetPath = tupl
                progdialog.setValue (tupIndex)  
                treePath = node.getTreePath ()                        
                treeList.append (treePath)            
                App.processEvents ()
                            
            
            for node in self._rootMemory :
                RCC_NiftiFileSystemDataInterface._addRootNodes (node, self._rootNodes)
            baseLen = len (self._NodeUpdateQue)
            ProgressBaseLength = baseLen
            DatasetCacheList = ProjectFI.getProjectDatasetCacheForList (treeList, AquireFileLock = False, ProgressDialog = progdialog, BaseIndex = ProgressBaseLength)            
            self._setDisableNodeUpdateLayoutChange (True)
                        
            GetFileExistAndChangeDate = []            
            for tupIndex, tupl in enumerate (self._NodeUpdateQue)  :              
                node, datasetPath = tupl                
                ROIFilePath = self._ROI_Dataset.getROIDataFilePathForNiftiDataset (node)
                DatasetCache = DatasetCacheList[tupIndex]    
                if DatasetCache is not None  and RCC_NiftiFileSystemDataInterface.isValidCache (DatasetCache) :                                             
                    compressedFilePath = ProjFileInterface.getFileCompressedCachePathFromROIFilePath (ROIFilePath)
                    GetFileExistAndChangeDate.append ((ROIFilePath, compressedFilePath, DatasetCache))
                else:
                    GetFileExistAndChangeDate.append (None)
            progdialog.setLabelText ("Updating file cache")        
            ProgressBaseLength += baseLen
            MPFileState = PlatformThreading.ThreadMap (_getFileState,GetFileExistAndChangeDate,App = App, ThreadMax = 32, ProgressDialog = progdialog, BaseProgressBarIndex = ProgressBaseLength)        
            del GetFileExistAndChangeDate
            
            MultiUserFileSharingCurrentUSerGroupList = PersistentFSLock.getCurrentUserGroupList (ProjectFI)
            self.layoutAboutToBeChanged.emit ()
            ProgressBaseLength += baseLen
            for tupIndex, tupl in enumerate (self._NodeUpdateQue)  :
                if (progdialog.wasCanceled ()) :
                    break
                node, datasetPath = tupl
                progdialog.setValue (tupIndex + ProgressBaseLength)  
                treePath = node.getTreePath ()
                progdialog.setLabelText ("Decoding Cache: " + treePath)                              
                mpNodeFileState = MPFileState[tupIndex]
                if mpNodeFileState is not None :
                    DatasetCache = DatasetCacheList[tupIndex]    
                    try :       
                        ROIFileExists, DatasetCacheChanged = mpNodeFileState                                                                                                                                                         
                        ROIFilePath, Exists = self._ROI_Dataset.getROIDataFilePathForExistingFile (node, Exists = ROIFileExists) 
                        if not Exists :
                            if DatasetCache['ContoursExist'] :
                                ProjectFI.removeProjectDatasetCacheEntry (treePath, AquireFileLock = False)   
                                DatasetCache["NodeLocked"] = None
                                DatasetCache["ContoursExist"] = None
                                DatasetCache["ScanPhase"] = None
                            node.clearDelayedCacheLoading ()
                            if DatasetCache["NodeLocked"] != False :
                                node.setROIFileLocked (False, setParent = False, AquireFileLock = False, FireTreeNodeChangeEvent = False)                  
                            if DatasetCache["ContoursExist"] != False :
                                node.setROIDatasetIndicator (False, FireChildNodeChangedEvent = False, FireTreeNodeChangedEvent = False, AquireFileLock = False)        
                            if DatasetCache["ScanPhase"] != "Unknown" :                            
                                node.setDescription ("Unknown", AquireFileLock = False, FireTreeNodeChangedEvent = False, FireTreeNodeDescriptionChangeEvent = False)                                                                    
                            pLock = getPLockStatus (ProjectFI, treePath, DatasetCache)                            
                            node.setNodePersistentFileLock (pLock, FireChildNodeChangeEvent= False, FireTreeNodeChangeEvent = False, AquireFileLock = False, UserGroupList = MultiUserFileSharingCurrentUSerGroupList)   
                            continue
                        elif not DatasetCacheChanged :
                            cachedResultList.append ((DatasetCache, node, datasetPath))  
                            continue                                                                      
                    except:
                        pass
                DatasetCache = FileSystemTreeWidgetNode.getNodeCacheData (treePath, datasetPath, userUID, ProjectFI, AquireFileLock = False)
                cachedResultList.append ((DatasetCache, node, datasetPath))     
                   
            del treePath
            del DatasetCache
            del DatasetCacheList        
            del MPFileState 
            
            progdialog.setLabelText ("Updating UI")
            progdialog.setProgressRange (0,0)   
            progdialog.setValue (0)   
            
            try :
                userUID = self._ProjectDataset.getUserUID ()  
                VerifyEmptyContourPathList = []
                VerifyEmptyContourNodeList = []
                
                progdialog.setMaximum (len (cachedResultList)) 
                progdialog.setValue (0)
                App = self._ProjectDataset.getApp ()
                for blockIndex, blockResultTpl in enumerate(cachedResultList) :
                    progdialog.processSystemEvents ()
                    blockResult, node, roiDataFilePaths  = blockResultTpl
                    try :                                                                                            
                        node.clearDelayedCacheLoading ()
                        if "FileObj" in blockResult :
                            fileobj = blockResult["FileObj"]
                            if (fileobj is not None) :
                                if (fileobj.hasParameter ("PESSCARA_SubjectCode") and fileobj.hasParameter ("PESSCARA_ExamCode") and fileobj.hasParameter ("PESSCARA_SeriesCode") and fileobj.hasFileObject ("RCC_PesscaraInterface")) : 
                                    pescaraInterfaceDescription = fileobj.getFileObject ("RCC_PesscaraInterface")
                                    node.setPesscaraDataSetSource (pescaraInterfaceDescription, fileobj.getParameter ("PESSCARA_SubjectCode"), fileobj.getParameter ("PESSCARA_ExamCode"), fileobj.getParameter ("PESSCARA_SeriesCode"))                                                                                    
                    
                        roiDatasetPath = roiDataFilePaths[0]
                        if roiDatasetPath is None :
                            roiDatasetPath = roiDataFilePaths[2]
                        if (roiDatasetPath is None) :
                            node.setNodePersistentFileLock  (blockResult["PLock"], FireChildNodeChangeEvent= False, FireTreeNodeChangeEvent = False, AquireFileLock = False, UserGroupList = MultiUserFileSharingCurrentUSerGroupList)
                        else:
                            lockFilePath = NonPersistentFSLock.getFileLockPathForROIFilePath (roiDatasetPath)
                            LockFileExists = os.path.exists (lockFilePath) 
                            lockedState = None
                            if LockFileExists :
                                try :
                                   lockedState = RCC_NiftiFileSystemDataInterface.getLockFileState (lockFilePath, userUID)
                                except:
                                   lockedState = None
                                   LockFileExists = False
                            
                            pLock = getPLockStatus (ProjectFI, node.getTreePath (), blockResult)
                          
                            try :
                                if LockFileExists :
                                     node.setROIFileLocked (lockedState, setParent = True, AquireFileLock = False, FireTreeNodeChangeEvent = False)                  
                                node.setNodePersistentFileLock (pLock, FireChildNodeChangeEvent= False, FireTreeNodeChangeEvent = False, AquireFileLock = False, UserGroupList = MultiUserFileSharingCurrentUSerGroupList)   
                                if blockResult['ContoursExist'] :                                
                                    node.setROIDatasetIndicator (True, FireChildNodeChangedEvent = True, FireTreeNodeChangedEvent = False, AquireFileLock = False)                                
                                else:
                                    VerifyEmptyContourPathList.append (roiDatasetPath)
                                    VerifyEmptyContourNodeList.append (node)            
                            except:
                                print ("An Error occured Loading the cache block data")
                        progdialog.setValue (blockIndex)
                        App.processEvents ()
                    except :
                        print ("An Error occured Loading the cache block data")
                   
                if len (VerifyEmptyContourPathList) > 0 :
                    progdialog.setLabelText ("Verifying cache")        
                    progdialog.setProgressRange (0,len (VerifyEmptyContourPathList))   
                    ProcessDialogTextList = []
                    for path in  VerifyEmptyContourPathList : 
                        ProcessDialogTextList.append ("Verifying cache: " + path)
                    if  PlatformMP.isMPDisabled () :
                        hasContours = PlatformThreading.ThreadMap (RCC_NiftiFileSystemDataInterface.MP_hasContours,VerifyEmptyContourPathList,App = App, ThreadMax = 10, ProgressDialog = progdialog, ProgressDialogTxtLst = ProcessDialogTextList)  
                    else:
                        hasContours = PlatformMP.processMap (RCC_NiftiFileSystemDataInterface.MP_hasContours, VerifyEmptyContourPathList, FileSaveThreadManager = self._ProjectDataset.getROISaveThread (), PoolSize = None, UseProcessBasedMap = False, QApp = App, Debug = False, ProgressDialog = progdialog, ProcessDialogTextList = ProcessDialogTextList, ProcessName = None, ProcessTimingHintPath = None, ProjectFileInterface = ProjectFI) 
                    for index, hasCT in enumerate(hasContours) :
                        if hasCT :
                            node = VerifyEmptyContourNodeList[index]
                            node.setROIDatasetIndicator (True, FireChildNodeChangedEvent = True, FireTreeNodeChangedEvent = False, AquireFileLock = False)
                            print ("Correcting Node. " + node.getText())                    
            finally:             
                rootMem = self.getRootMemory ()
                if rootMem is not None :
                    for rootNode in rootMem :
                        rootNode.delayedCacheLoading (InitalizeChildNodes = True, AquireFileLock = False)                
                self.layoutChanged.emit ()   
                self._setDisableNodeUpdateLayoutChange (False)
                progdialog.setProgressRange (0,0)
                RCC_NiftiFileSystemDataInterface.initalizeLoadedAllNodesPLockStatusAndDescription  (rootMem, AquireFileLock = False, ProgressDialog = progdialog)     
        except Exception as exp :
            print (exp)
            raise
        finally:            
            
            self._ProjectDataset.getProjectFileInterface().releaseFileLock ()           
            for nodes in self._rootNodes :
                nodes.clearDelayedCacheLoading ()
            if ProjectFI.forceReloadFileSystemCache() :
                progdialog.setMaximum (len (treeList))                 
                for nodeIndex, node_tuple in enumerate (self._NodeUpdateQue) :
                    node = node_tuple[0]
                    roiPath = self._ROI_Dataset.getROIDataFilePathForNiftiDataset (node) 
                    if os.path.isfile (roiPath) :
                        self.updateNodeStateFromPath (roiPath, node, BackgroundUpdate = True)  
                        progdialog.setLabelText ("Reloading Cache: " + node.getTreePath ())        
                    progdialog.setValue (nodeIndex)
                ProjectFI.saveProjectDatasetCache ()
                
    @staticmethod 
    def _addRootNodes (node, nodelist ):
        nodeList = []
        while not node.describesNiftiDataFile () :
            nodeList.append (node)
            if node.getChildCount () == 1 :
                node = node.getChild (0)
            else:
                break
        if not node.describesNiftiDataFile () :
            nodelist += nodeList
        
        
    def treeChanged (self, nodelist):              
        currentTime = time.time ()
        try:
            if (self._progdialog is not None) :   
                if currentTime - self._timeTreeLastUpdated >= 2.0 :
                    self._timeTreeLastUpdated = currentTime                
                    self._progdialog.processSystemEvents()                
        except:
            pass #silent faiialog cannot be updated
        for node in nodelist :            
            if (node.isInitalized ()) :            
                continue            
            if (node.describesNiftiDataFile()) :                  
                roiPath = self._ROI_Dataset.getROIDataFilePathForNiftiDataset (node)  
                #print ("Adding " + roiPath)
                self._nodeWatchDogTimeout[roiPath] =  currentTime + (60.0 * (5.0 + (float (random.randint(0,30))/10.0))) 
                if self._NodeUpdateQue is not None :
                    if self._ROI_Dataset.hasROIDatafileForNiftiDataset (node) :
                        datasetPath = self._ROI_Dataset.getROIDataFilePathForDatasetPath (node)   
                        if (datasetPath is not None) :
                            self._NodeUpdateQue.append ((node, (datasetPath, node.getNIfTIDatasetFile ().getFilePath (), None)))                   
                    else: # try to load tags directly
                        self._NodeUpdateQue.append ((node, (None, node.getNIfTIDatasetFile ().getFilePath (), roiPath)))
            else:
                node.setDelayedCacheLoading ((None, None))                

            if not node.hasParent () :                
                if (node.getRow () == -1) :
                    if node.isVisible () :
                        node.setRow (len (self._rootMemory))
                    self._rootMemory.append (node)          

            #if node.isVisible () :
            #    self.layoutAboutToBeChanged.emit ()                    
            #    nodechanged = self.createIndex(node.getRow (), 0, node)            
            #    self.dataChanged.emit (nodechanged, nodechanged)
            #    self.changePersistentIndex(nodechanged, nodechanged)                    
            #    self.layoutChanged.emit ()
            node.setInitalized ()
        
            
    def index (self, row, column, parent):        
        if parent is None or not parent.isValid():                   
            if row >= 0 and row < len (self._rootMemory) :
                child = RCC_NiftiFileSystemDataInterface._getVisibleChild (self._rootMemory, row)                
                if child is not None :
                    return self.createIndex(row, column, child)                                    
        else :
            parentNode = parent.internalPointer ()        
            childList = parentNode.getChildernLst ()
            if row >= 0 and row < len (childList) :
                childNode = RCC_NiftiFileSystemDataInterface._getVisibleChild (childList, row)                            
                if (childNode is not None) :
                    return self.createIndex(row, column, childNode)                        
        return QModelIndex()
    
    
    
    def _tryGetMultipleChildANTSRegistration (self, node, AquireFileLock = True):
        nodeLst =  []
        nodestack = [node]        
        while (len (nodestack) > 0) :
            node = nodestack.pop ()             
            if ( node.isNodeAntsRegistered (AquireFileLock = AquireFileLock)) :
                nodeLst.append (node)                
            for childindex in range (node.getChildCount ()) :
                nodestack.append (node.getChild (childindex))
        return nodeLst
    
    @staticmethod
    def _getChildenOneMajorLevel (node, nodestack, MultipleChildern):
        childCount = node.getChildCount ()
        if (childCount > 0) :
            if childCount == 1 :
                for childindex in range (childCount) :
                    nodestack.append (node.getChild (childindex))
        elif not MultipleChildern :
            MultipleChildern = True
            for childindex in range (childCount) :
                nodestack.append (node.getChild (childindex))     
        return MultipleChildern 

    def _tryGetMultipleChildNodeTag (self, node, tagidentifer, AquireFileLock = True):
        MultipleChildern = False
        nodestack = [node]
        nodeDescription = None
        while (len (nodestack) > 0) :            
            node = nodestack.pop ()
            if node.getChildCount () == 0 :
                tag_textvalue = node.getUICacheTag (tagidentifer, AquireFileLock = AquireFileLock)   
                if (tag_textvalue is not None) :
                    description = tag_textvalue
                    if (description != "") :
                        if (nodeDescription is None) :
                            nodeDescription = description
                        elif (nodeDescription != description) :
                            return "Multiple"                                
            else:
                MultipleChildern = RCC_NiftiFileSystemDataInterface._getChildenOneMajorLevel (node, nodestack, MultipleChildern)
        if (nodeDescription is None) :
            return ""
        return nodeDescription
    
    
    
    def _doesNodeContainPredictedScanPhase (self, node, AquireFileLock = True) :        
        MultipleChildern = False
        nodestack = [node]        
        while (len (nodestack) > 0) :
            node = nodestack.pop ()  
            childCount = node.getChildCount ()
            if (childCount > 0) :
                MultipleChildern = RCC_NiftiFileSystemDataInterface._getChildenOneMajorLevel (node, nodestack, MultipleChildern)
            elif ( node.isScanPhasePredicted (AquireFileLock = AquireFileLock)) :
                return True
        return False
        
    def updateNodeStateFromPath (self, roiPath, node, BackgroundUpdate = False) :
        try :
            FileChangeEventHandler = self.getFileSysetmEventHandler ()
            if (FileChangeEventHandler is not None) :                
                self._nodeWatchDogTimeout[roiPath] = time.time () + (60.0 * (5.0 + float (random.randint(0,3)))) + float (random.randint(0,60))                  
                if (BackgroundUpdate) :
                    cachPath = self._ProjectDataset.getProjectFileInterface ().getFileCompressedCachePathFromROIFilePath (roiPath)
                    FileChangeEventHandler.processEvent (cachPath, treeNode = node)
                else:
                    cachPath = self._ProjectDataset.getProjectFileInterface ().getFileCompressedCachePathFromROIFilePath (roiPath)
                    nonpersistantLockPath = NonPersistentFSLock.getFileLockPathForROIFilePath (roiPath)               
                    pLockPath = PersistentFSLock.getPersistentLockPathForROIFilePath (roiPath)                               
                    FileChangeEventHandler.processEvent ([cachPath, nonpersistantLockPath, pLockPath], treeNode = node)                                        
        except:
            pass
        
    def isRootDirNode (self, node) :
        return node in self._rootNodes
        
    def doesNodeHaveDelayedState (self, node) :
        if node.isDelayedCacheLoadingSet () :
            return True        
        for child in node.getChildernLst () :
            if child.isDelayedCacheLoadingSet ():
                return True
        return False
        
    def getROIFilePathForNode (self, node) :
        try :
            return self._ROI_Dataset.getROIDataFilePathForNiftiDataset (node)         
        except:
            return None 
    
    def setShowShortFileName (self, val) :
        if val != self._showShortFileName :
            self.layoutAboutToBeChanged.emit ()
            self._showShortFileName = val
            self.layoutChanged.emit ()
    
    def showShortFileName (self) :
        return self._showShortFileName
    
    def data(self, index, role):
        if not index.isValid():
            return None
        elif role not in [QtCore.Qt.FontRole, QtCore.Qt.DisplayRole,  QtCore.Qt.ForegroundRole] :                
            return None
        else:
            node = index.internalPointer()  
            #print ("node Tree: "+ node.getTreePath())
                 
            if role == QtCore.Qt.DisplayRole :       
                if node.describesNiftiDataFile () and node.getChildCount () == 0 :
                    FileChangeEventHandler = self.getFileSysetmEventHandler ()
                    if (FileChangeEventHandler is not None) :
                        if node.getNodeROIDatasetIndicator () :
                            roiPath = self._ROI_Dataset.getROIDataFilePathForNiftiDataset (node)                          
                            currentTime = time.time ()
                            if roiPath not in self._nodeWatchDogTimeout or self._nodeWatchDogTimeout[roiPath] - currentTime < 0 :                         
                                self.updateNodeStateFromPath (roiPath, node, BackgroundUpdate = True)
                   
                           
                if (self._DatasetUIHeader.getVisibleHeaderType (index.column()) == "Dataset") :  
                    return  node.getDisplayText ()
                   
                elif (self._DatasetUIHeader.getVisibleHeaderType (index.column())== "Description") :
                    if self.isRootDirNode (node) : 
                        return ""                    
                    self._ProjectDataset.getProjectFileInterface().aquireFileLock ()                    
                    try:    
                        return node.getScanPhaseTxt (AquireFileLock = False)                        
                    finally:                        
                        self._ProjectDataset.getProjectFileInterface().releaseFileLock ()
                elif (self._DatasetUIHeader.getVisibleHeaderType (index.column())== "Registration") :  
                    if self.isRootDirNode (node) : 
                        return ""
                    if self.doesNodeHaveDelayedState (node) :
                        return "Click to load"
                    self._ProjectDataset.getProjectFileInterface().aquireFileLock ()
                    try:    
                        if node.isNodeAntsRegistered (AquireFileLock = False) :                                    
                            if (self.isRegistrationDateOnOrAfter (node.getAntsRegisteredDateTuple (AquireFileLock= False))) :
                                return "Reg: " + node.getAntsRegisteredDate (AquireFileLock = False)
                            return ""
                        else:
                            nodeLst = self._tryGetMultipleChildANTSRegistration (node, AquireFileLock = False)  
                            
                            filteredNodeList = []
                            for testnode in nodeLst :
                                if (self.isRegistrationDateOnOrAfter (testnode.getAntsRegisteredDateTuple (AquireFileLock= False))) :
                                    filteredNodeList.append (testnode)
                            nodeLst =  filteredNodeList
                            
                            length = len (nodeLst)
                            if (length < 1) :
                                return ""                    
                            testdate = nodeLst[0].getAntsRegisteredDate (AquireFileLock = False)
                            for i in range (1, length, 1):
                                if (testdate != nodeLst[i].getAntsRegisteredDate (AquireFileLock = False)) :                    
                                    return "Registered"                    
                            return "Reg: " + testdate
                    finally:
                        self._ProjectDataset.getProjectFileInterface().releaseFileLock ()
                elif (self._DatasetUIHeader.getVisibleHeaderType (index.column())== "Tag") :                          
                    if self.isRootDirNode (node) : 
                        return ""           
                    if self.doesNodeHaveDelayedState (node) :                        
                        return "Click to load"
                    tagidentifer  = self._DatasetUIHeader.getVisibleHeaderIdentifierByIndex (index.column())                
                    """if self._ProjectDataset is not None :
                        datasetTagManager = self._ProjectDataset.getProjectTagManger ()
                        if datasetTagManager is not None and datasetTagManager.getTagByIdentifer (tagidentifer) is None :
                            return ""                """
                    self._ProjectDataset.getProjectFileInterface().aquireFileLock ()
                    try:
                        tag_text_value = node.getUICacheTag (tagidentifer, AquireFileLock = False)           
                        if (tag_text_value is None) :
                            return self._tryGetMultipleChildNodeTag (node, tagidentifer, AquireFileLock = False) #return self._trygetChildNodeDescription (node)
                        else:
                            return tag_text_value                        
                    finally:
                        self._ProjectDataset.getProjectFileInterface().releaseFileLock ()                    
                else:
                    return None
            elif role == QtCore.Qt.ForegroundRole :
                self._ProjectDataset.getProjectFileInterface().aquireFileLock ()
                try:
                    if self.isRootDirNode (node) :
                        return QtGui.QColor (0,0,0)
                    if (self._DatasetUIHeader.getVisibleHeaderType (index.column())== "Description" and self._doesNodeContainPredictedScanPhase (node, AquireFileLock = False)):
                        return QtGui.QColor (255,0,0)
                    if (node.isSelected ()) :
                        return QtGui.QColor (15,130,255)
                    if (node.isDatasetOpen ()) :
                        return QtGui.QColor (12,63,119)   
                    fileLock = node.getROIFileLock (AquireFileLock = False)
                    
                    if (fileLock == "locked_other"):        
                        return QtGui.QColor (128,0,0)   
                    if (fileLock == "locked_self"):        
                        return QtGui.QColor (218,105,75)
                    PLockStatus = node.getCurrentUserPLockStatus (AquireFileLock = False)
                    if (PLockStatus == "OtherUserLocked"):        
                        return QtGui.QColor (128,0,0)   
                    if (PLockStatus == "UserGroupLocked"):        
                        return QtGui.QColor (118,0,255)
                    if (PLockStatus == "UserLocked"):        
                        return QtGui.QColor (100,173,70)
                    #if self.doesNodeHaveDelayedState (node) :
                    #    return QtGui.QColor (166,176,179)
                    #if not node.hasOnlySingleChild () :
                    #    return  QtGui.QColor (139,142,142)
                    return QtGui.QColor (0,0,0)
                finally:
                    self._ProjectDataset.getProjectFileInterface().releaseFileLock ()
            elif role == QtCore.Qt.FontRole :
                if (self._DatasetUIHeader.getVisibleHeaderType (index.column()) == "Dataset") :
                    font = QtGui.QFont ()
                    font.setBold (node.getNodeROIDatasetIndicator ())                                    
                    return  font
                else:
                    font = QtGui.QFont ()
                    font.setBold (False)
                    return  font        
                        
    
    @staticmethod
    def _getVisibleChildCount (vec):
        count = 0
        for index in range(len(vec)) :
            if vec[index].isVisible () :
                count += 1
        return count
        
    @staticmethod
    def _getVisibleChild (vec, index):
        count = 0
        for idx in range(len(vec)) :
            if vec[idx].isVisible () :
                if count == index :
                    return vec[idx]
                count += 1
        return None                    
    
    def hasIndex (self, row, column, parent) :
        if (row < 0) :
            return False                
        return row < self.rowCount (parent)        
    
    def rowCount(self, parent):
      if not parent.isValid():           
           return RCC_NiftiFileSystemDataInterface._getVisibleChildCount (self._rootMemory)
      else:
          parentNode = parent.internalPointer ()
          return RCC_NiftiFileSystemDataInterface._getVisibleChildCount (parentNode.getChildernLst ())

    
    def parent (self, index) :
        if not index.isValid():
            return QModelIndex()
        node =  index.internalPointer()
        if (node is None or not node.hasParent ()) :
            return QModelIndex()
        else:
            return self.createIndex(node.getParentRow (), 0, node.getParent ())                
            
    def flags (self, index) :
        try:
            node = index.internalPointer ()        
            if (node.getChildCount () == 0) :
                return  QtCore.Qt.ItemIsEnabled |  QtCore.Qt.ItemIsSelectable |  QtCore.Qt.ItemNeverHasChildren
            else: #elif (node.hasOnlySingleChild ()) :                        
                return  QtCore.Qt.ItemIsEnabled |  QtCore.Qt.ItemIsSelectable            
        except:
            return  QtCore.Qt.NoItemFlags
        
    def columnCount(self, parent):
        #if (self._treeContainsANTSRegisteredDatasets) :
        #    return 3
        return self._DatasetUIHeader.headerVisibleObjectCount ()
            

    def setUIHeader (self, newHeader) :
        self._DatasetUIHeader = newHeader
        self.headerDataChanged.emit (QtCore.Qt.Horizontal, 0, self._DatasetUIHeader.headerVisibleObjectCount () -1)
    
    def getUIHeader (self) :
        return self._DatasetUIHeader
        
    def headerData (self, section, orientation, role):
        if role != QtCore.Qt.DisplayRole  :
            return(None)
        if (orientation == QtCore.Qt.Horizontal):
            return self._DatasetUIHeader.getVisibleHeaderName (section)            
        else:
            return str (section)
    
    
    #code for matching a file path list with the tree
    def _getHeadNode (self, rootNodelist, fileList) :
        rootShortFileList = []
        rootLongFileList= []
        for node in rootNodelist :
            rootShortFileList.append (node.getFilename ())       
            rootLongFileList.append (node.getFilename (LongFileName = True)) 
        for searchList in [rootLongFileList, rootShortFileList] :            
            searchSet = set (searchList)
            for index in range (len (fileList)) :
                if (fileList[index] in searchSet) :
                    nodeindex = searchList.index (fileList[index])
                    return (rootNodelist[nodeindex], index)        
        return (None, None)
        
    def _getChildTreeNode (self, treeNode, fileName) :        
        for LongFileName in [True, False] :
            for node in treeNode.getChildernLst () :
                if fileName == node.getFilename (LongFileName = LongFileName) :
                    return node
        return None            
    
    def _getChildTreeNodesWithNiftiFiles (self, treeNode, filename):
       
        filename, _ = FileUtil.removeExtension (filename, [".nii.gz", ".nii"])
        
        returnLst = [] 
        for LongFileName in [True, False] :
            for node in treeNode.getChildernLst () :
                nFilename = node.getFilename (LongFileName = LongFileName)            
                nFilename, extension = FileUtil.removeExtension (nFilename, [".nii.gz", ".nii"])
                if extension is not None :
                    if (filename in nFilename) :
                        returnLst.append (node)                
            if len (returnLst) > 0 :
                break
        return returnLst
    
    def getDatasetStatistics (self, Node = None) :
        def _getAllDatasetStatistics (childernLst) :
            nodeCount = 0
            nodeWithData = 0
            for node in childernLst :
                nodeChildList = node.getChildernLst ()
                if len (nodeChildList) == 0 :
                    if node.describesNiftiDataFile ()  :
                        nodeCount += 1
                        if node.getROIDatasetIndicator () :
                            nodeWithData += 1
                else:
                    nC, nD = _getAllDatasetStatistics (nodeChildList) 
                    nodeCount += nC
                    nodeWithData += nD
            return nodeCount, nodeWithData   
        
        def _getHasDataDatasetStatistics (childernLst) :
            nodeWithData = 0
            for node in childernLst :
                if node.getROIDatasetIndicator () :
                    nodeChildList = node.getChildernLst ()
                    if len (nodeChildList) == 0 :
                        if node.describesNiftiDataFile ()  :
                            nodeWithData += 1
                    else:
                        nodeWithData += _getHasDataDatasetStatistics (nodeChildList) 
            return nodeWithData   
        
        try:    
            self._ProjectDataset.getProjectFileInterface().aquireFileLock ()    
            if Node is None or Node.isRootNode () : 
                if len (self._rootMemory) == 0 :
                    nodeCount = 0
                    nodeWithData = 0
                    return nodeCount, nodeWithData   
                
                try :
                    if self._CachedNodeCount is None :
                        self._CachedNodeCount, NodesWithData = _getAllDatasetStatistics (self._rootMemory)
                    else:
                        NodesWithData = _getHasDataDatasetStatistics (self._rootMemory)
                    return self._CachedNodeCount, NodesWithData
                except:
                    return -1, -1
            else:
                return _getAllDatasetStatistics ([Node])
        finally:
            self._ProjectDataset.getProjectFileInterface().releaseFileLock ()     
        
    def resetCachedNodeCount (self, AquireFileLock = True) :
        try:    
            if AquireFileLock :
                self._ProjectDataset.getProjectFileInterface().aquireFileLock () 
            self._CachedNodeCount = None
        finally:
            if AquireFileLock :
                self._ProjectDataset.getProjectFileInterface().releaseFileLock ()     
    
    def getLeafNIfTINodes (self, treeNode = None, Memory = None):        
        if Memory is None :
            returnLst = [] 
        else:
            returnLst = Memory            
            
        if treeNode is None :            
            for node in self._rootMemory :
                self.getLeafNIfTINodes (node, Memory = returnLst) 
        else:
            childernLst = treeNode.getChildernLst ()
            if len (childernLst) == 0 :        
                try:
                    fname = treeNode.getFilename ()
                    lowerfilename = fname.lower ()                
                    if lowerfilename.endswith (".nii.gz") or lowerfilename.endswith (".nii") :
                        returnLst.append ((treeNode))                    
                except:
                    pass
            else:            
                for node in childernLst :
                    self.getLeafNIfTINodes (node, Memory = returnLst)        
        return returnLst
    
    def _treeNodeHasParentIn (self, node, parentList) :
       while node is not None :
           if node in parentList :
               return True
           node = node.getParent ()
       return False
           
    def getTreeNodeSubset (self, TreeNodeList, subsetList) :
       if subsetList is None :
           return TreeNodeList
       else:
           selection = []
           subsetList = set (subsetList)
           for treeNode in TreeNodeList :
               if self._treeNodeHasParentIn (treeNode, subsetList) :
                   selection.append (treeNode)
           return selection
       
    def getNiftiTreeNodeListForFileList (self, fileList, filename, LeafFileCache = None, ExactMatch = False) :             
        if (len (fileList) == 0) :
            treeNode = self._rootMemory
        else:
            root =  self._rootMemory
            while (len (root) == 1) :
                if (root[0].getChildCount () == 0):
                    break
                else:
                    root = root[0].getChildernLst()
            
            treeNode, index = self._getHeadNode (root, fileList)
            if (treeNode is None) :
                if LeafFileCache is None :
                    LeafFileCache = self.getLeafNIfTINodes ()
                #Tree node could not be found that matched the file list 
                #see if matching file names are in present in root.
                rootnodelist = []   
                for LongFileName in [True, False] :                          
                    for node in LeafFileCache :
                        nFilename = node.getFilename (LongFileName = LongFileName)
                        lowerNodeFilename = nFilename.lower()
                        if (lowerNodeFilename.endswith (".nii.gz") or lowerNodeFilename.endswith (".nii")) :                        
                            if ExactMatch :
                                if (filename == nFilename) :
                                    rootnodelist.append (node)
                            else:
                                if (filename in nFilename) :
                                    rootnodelist.append (node)
                    if len (rootnodelist) > 0 :
                        break
                return rootnodelist
    
            index += 1
            while index < len (fileList) :                        
                treeNode = self._getChildTreeNode (treeNode, fileList[index])
                index += 1
                if (treeNode is None) :
                   return []
            if index < len (fileList) :                        
                return []
        return self._getChildTreeNodesWithNiftiFiles (treeNode, filename)
