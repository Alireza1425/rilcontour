#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan  7 00:39:52 2019

@author: KennethPhilbrick
"""
import sys
import multiprocessing
from uuid import uuid4
import time 
from rilcontourlib.util.FileUtil import FileUtil
from rilcontourlib.util.rcc_util import  FileInterface, PlatformMP, FileChangeMonitor
import zlib
import pickle
import os
from uuid import uuid1 
from rilcontourlib.util.DateUtil import TimeUtil, DateUtil
import json
import getpass
import shutil
try:
    from pathlib import Path
except:
    from pathlib2 import Path
    

class innerProjectCacheInterface :
    def __init__ (self) :
        self._isProjectReadOnly = False
        self._MPMode = False
        self._RLock = None
        self._projectDatasetTagDecompressionCacheOrder = []
        self._projectDatasetTagDecompressionCache = {}
        self._projectDatasetTagCache = {}
        self._ThreadingRLock = multiprocessing.RLock()
        self._multiUserFileSharingManager = None
        self._projectDatasetUICache = {}
       
    
    def getProjectDatasetUICache (self) :
        return self._projectDatasetUICache 
        
    def getDatasetCacheDictionary (self) :
        if self._MPMode == False :
            return self._projectDatasetTagCache
        else:
            out = {}
            for key, value in self._projectDatasetTagCache.items () :
                out[key] = value 
            return out
    
    def setDatasetCacheDictionary (self, indict) :  
        if self._MPMode == False :
            self._projectDatasetTagCache = indict
        else:
            for key in list (self._projectDatasetTagCache.keys ()) :
                del self._projectDatasetTagCache[key]
            for key, value in indict.items ():
                if isinstance (value, dict) :
                    self._projectDatasetTagCache[key] = ProjFileInterface.getCompressedTag (value)
                else:
                    self._projectDatasetTagCache[key] = value
                
    def isInMPMode (self):
        return self._MPMode
       
    @staticmethod 
    def _copyDictCacheObj (cacheObj, newCacheObj):
        for key, value in cacheObj.items () :
               newCacheObj[key] = value
        return newCacheObj
    
    
    @staticmethod 
    def _copyListCacheObj (cacheObj, newCacheObj):
        for value in cacheObj :
               newCacheObj.append(value)
        return newCacheObj
    
    def setMPMode (self, manager) :
        if self._MPMode == False:
           self._MPMode = True
           if self._RLock is None :
               self._RLock = manager.RLock()
               
           self._projectDatasetTagDecompressionCacheOrder = innerProjectCacheInterface._copyListCacheObj (self._projectDatasetTagDecompressionCacheOrder, manager.list ())
           self._projectDatasetTagDecompressionCache = innerProjectCacheInterface._copyDictCacheObj (self._projectDatasetTagDecompressionCache, manager.dict () )
           self._projectDatasetTagCache = innerProjectCacheInterface._copyDictCacheObj (self._projectDatasetTagCache, manager.dict () )
           self._projectDatasetUICache = innerProjectCacheInterface._copyDictCacheObj (self._projectDatasetUICache, manager.dict () )
        
        
    def setSingeThreadMode (self) :
        if self._MPMode == True:
           self._MPMode = False
          
           self._projectDatasetTagDecompressionCacheOrder = innerProjectCacheInterface._copyListCacheObj (self._projectDatasetTagDecompressionCacheOrder, [])
           self._projectDatasetTagDecompressionCache = innerProjectCacheInterface._copyDictCacheObj (self._projectDatasetTagDecompressionCache, {} )
           self._projectDatasetTagCache = innerProjectCacheInterface._copyDictCacheObj (self._projectDatasetTagCache, {} )
           self._projectDatasetUICache = innerProjectCacheInterface._copyDictCacheObj (self._projectDatasetUICache, {} )
    
    def getDecompressionCacheOrder (self) :
        return self._projectDatasetTagDecompressionCacheOrder
    
    def getDecompressionCache (self) :
        return self._projectDatasetTagDecompressionCache
    
    def getDatasetCache (self) :
        return self._projectDatasetTagCache

    def aquireFileLock (self) :
        if self._MPMode :
             self._RLock.acquire()
        else:
            self._ThreadingRLock.acquire()

    def releaseFileLock (self) :
        if self._MPMode :
             self._RLock.release()
        else:
            self._ThreadingRLock.release()
    
    def isProjectReadOnly (self) :
        return self._isProjectReadOnly 
    
    def setProjectReadOnly (self, val) :
        if self._isProjectReadOnly == False :
            self._isProjectReadOnly = val
    
    def debugGetRLocks (self) :
        return self._RLock, self._ThreadingRLock
    
    def debugSetRLocks (self,  rlock, threadlock) :
        self._RLock, self._ThreadingRLock = rlock, threadlock
    
class ProjFileInterface :
    def __init__ (self,  CacheInterface = None, RILContourCommandLine = None, FileSystemUID = None, MultiUserFileSharingManager = None, UserUID = None) :        
        self._projectPath = None        
        self._projectUID  = None
        self._UserUID = None
        self._FileSystemUID = FileSystemUID
        self._projectName = "Project Name Undefined"
        self._userName = getpass.getuser ()
        self._RILContourCommandLine = RILContourCommandLine
        self._MPEnabled = False                
        self._projectDatasetTagCacheLoaded = False
        self._projectDatasetTagCacheFileName = None
        self._ProjectDatasetTagCacheManager = None 
        self._MultiUserProjectPath = None
        self.clearFilePermissions ()
        if CacheInterface is not None :
            self._CacheInterface = CacheInterface
        else:
            self._CacheInterface = innerProjectCacheInterface ()
        self.setMultiUserFileSharingManager (MultiUserFileSharingManager)
    
    def setMutliUserProjectPath (self, path) :
        self._MultiUserProjectPath = path
        
    def debugGetRLocks (self) :
        if self._CacheInterface is not None :
            return self._CacheInterface.debugGetRLocks ()
        else:
            return None, None
    
    def debugSetRLocks (self,rlock, threadlock) :
        if self._CacheInterface is not None :
            return self._CacheInterface.debugSetRLocks (rlock, threadlock)
       
    
    def weakLocksCommandLineParameterEnabled (self) :
        return self._RILContourCommandLine is not None and "weaklocks" in self._RILContourCommandLine
    
    def isInSuperUserCommandLineMode (self) :
        return self._RILContourCommandLine is not None and "superuser" in self._RILContourCommandLine
  
    def isRunningInDemoMode (self):
         return (self._RILContourCommandLine is not None and "demo" in self._RILContourCommandLine)
   
    def isFileUnlockingCommandLineModeEnabled (self) :
       return self._RILContourCommandLine is not None and ("superuser" in self._RILContourCommandLine or "fileunlock" in self._RILContourCommandLine)
      
    def forceReloadFileSystemCache (self) :
       return self._RILContourCommandLine is not None and ("ReloadFileSystemCache" in self._RILContourCommandLine)
      
    def getFileWritePermissions (self) :
        return self._FileWritePermissions
        
    def clearProjectPath (self):
        self._projectPath = None 
        
    def clearFilePermissions (self) :        
        self._FileWritePermissions = 0o775 #None            
        
        
    def setFilePermissions (self, permissions) :
        #self._FileWritePermissions = permissions
        self.clearFilePermissions () 
        
        
    def setProjectPath (self, path) :
        if self._projectPath != path  :
            self._projectPath = path                        
        if self._projectUID is None :
            self._initProjectUID ()
            
    def getProjectPath(self):
        return self._projectPath
        
    def setProjectName (self, name) :
        self._projectName = name
    
    def getProjectName (self) :
        return self._projectName
    
    def setProjectReadOnly (self, val) :        
        self._CacheInterface.setProjectReadOnly (val)
    
    def getMultiUserProjectPath (self) :
        return self._MultiUserProjectPath
    
    def isProjectReadOnly (self):
        return self._CacheInterface.isProjectReadOnly () or self.isRunningInDemoMode ()
        
    def getRILContourCommandLine (self) :
        return self._RILContourCommandLine
    
    def setMultiUserFileSharingManager (self,mgr):
        self._CacheInterface._multiUserFileSharingManager = mgr
        
    def getMultiUserFileSharingManager (self) :
        return self._CacheInterface._multiUserFileSharingManager 
    
    def getProjectDatasetUICache (self, AquireFileLock = True) :
        if AquireFileLock :
            self.aquireFileLock ()
        try:
            return self._CacheInterface.getProjectDatasetUICache()
        finally:
           if AquireFileLock :  
               self.releaseFileLock()
               
    def getMultuserFileSharingAssignments (self, IncludeSelf = False) :
        results = []        
        fileshareManager = self.getMultiUserFileSharingManager ()
        if fileshareManager is not None and fileshareManager.isInitalized () :
            results = fileshareManager.getUserAssignments (self.getUserName (), IncludeSelf = IncludeSelf)
            if results is None :
                return []
        return results
    
    def getFileSystemUID (self) :
        if (self._FileSystemUID is None) :            
            timestr = TimeUtil.timeToString (TimeUtil.getTimeNow ())
            datestr = DateUtil.dateToString (DateUtil.today())
            self._FileSystemUID = "FILESYSTEMUID_" + str(uuid1()) + "_" + datestr + "_" + timestr
        return self._FileSystemUID    
    
    def clearUserName (self) :
        self._userName = "Unknown"
    
    def setUserName (self, name) :
        if name != self._userName :
           if name is None :
               self._userName = "Unknown"
           else:
               self._userName = name   
               multiUserFileSharingManager = self.getMultiUserFileSharingManager ()
               if multiUserFileSharingManager is not None and "Unknown" != self._userName :
                  if self._RILContourCommandLine is not None :
                      if  "user" not in self._RILContourCommandLine  :
                          multiUserFileSharingManager.setUserNameUID (self._userName, self.getUserUID ())
                      elif  "user" in self._RILContourCommandLine and "userhomepath" in self._RILContourCommandLine :
                          multiUserFileSharingManager.setUserNameUID (self._RILContourCommandLine["user"], self.getUserUID ())
           return True
        return False
        
    def getUserName (self, Actual = False):
        if not Actual and self._RILContourCommandLine is not None and "user" in self._RILContourCommandLine :
            return self._RILContourCommandLine["user"]
        return self._userName

    @staticmethod    
    def getRootHomePath (RILContourCommandLine) :
        if RILContourCommandLine is not None and "userhomepath" in RILContourCommandLine and os.path.isdir (RILContourCommandLine["userhomepath"]) :
            try :
                return (RILContourCommandLine["userhomepath"])                            
            except:                
                return (str(Path.home()))
        else:
            return (str(Path.home()))
            
    def getUserUID (self) :
        if (self._UserUID is not None and self._UserUID != "ErrorOccuredWhileGeneratingUserUID") :
            return self._UserUID
        try :            
            home = ProjFileInterface.getRootHomePath (self.getRILContourCommandLine ())
            homepath = os.path.join (home,".rilcontour")
            if (not os.path.isdir (homepath) and not os.path.exists (homepath)):
                os.mkdir (homepath)
            fpath = os.path.join (homepath,"useruid.txt")
        except:
            fpath = None
            print ("A exception occured creating .rilcontour in home dir")
        try :
            if fpath is not None and os.path.exists (fpath) :
                with open (fpath,"rt") as infile :
                    fobj = json.loads (infile.read ())
                self._UserUID = fobj["UserUID"]
                return self._UserUID
        except:
            pass
        try :
            timestr = TimeUtil.timeToString (TimeUtil.getTimeNow ())
            datestr = DateUtil.dateToString (DateUtil.today())
            name = self.getUserName()
            self._UserUID = name + "_" + str(uuid1()) + "_" + datestr + "_" + timestr 
            if fpath is not None :
                try :
                    data = {}
                    data["UserName"] = name
                    data["UserUID"] = self._UserUID
                    with open (fpath,"wt") as outfile :
                        outfile.write (json.dumps (data))
                except:
                    print ("Error: A error occured saving: " + fpath)
        except:
            print ("Could not generate user UID.")
            self._UserUID = "ErrorOccuredWhileGeneratingUserUID"
        return self._UserUID
    
    def isInMultiUserMode (self) :
        mannager = self.getMultiUserFileSharingManager ()
        return mannager is not None and mannager.isInitalized ()        
    
    def setMPEnabled (self, val) :
        if PlatformMP.isMPDisabled () :
            val = False
        if self._MPEnabled != val :
            self._MPEnabled = val
            if val  :
                if self._ProjectDatasetTagCacheManager is None :
                    self._ProjectDatasetTagCacheManager = multiprocessing.Manager ()
                self._CacheInterface.setMPMode (self._ProjectDatasetTagCacheManager)
            else:
                self._CacheInterface.setSingeThreadMode ()
            
    def isMPEnabled (self) :
        return self._MPEnabled
        
    def _initProjectUID (self) :
        if self._projectPath is None :
            self._projectUID = None
        else:  
            if not os.path.exists (self._projectPath) :
                self._projectUID = None
            else:
                try:
                    self.aquireFileLock  ()
                    fobj = FileInterface ()  
                    fobj.readDataFile (self._projectPath)
                    header = fobj.getFileHeader ()  
                    if header.hasParameter ("ProjectUID"):
                        self._projectUID = header.getParameter ("ProjectUID") 
                        if self._projectUID is None and not self.isProjectReadOnly ():
                            self._projectUID = self._generateProjectUID()
                            header.setParameter ("ProjectUID", self._projectUID )
                            fobj.writeDataFile (self._projectPath)     
                    elif not self.isProjectReadOnly () :
                        self._projectUID = self._generateProjectUID()
                        header.setParameter ("ProjectUID", self._projectUID ) 
                        fobj.writeDataFile (self._projectPath)     
                except:
                    self._projectPath = None
                    self._projectUID = None
                finally:
                       self.releaseFileLock ()
                   
    def genDocumentUID (self) :
       uid =  str (uuid4())
       uid = "DocumentUID_"+ self._projectName + "_" + self._userName + "_" + uid + "_" + str (time.time ())
       return uid
   
    def aquireFileLock (self) :
        self._CacheInterface.aquireFileLock ()  
    
    def releaseFileLock (self) :
        self._CacheInterface.releaseFileLock ()
  
        
    def copy (self) :
        newObj = ProjFileInterface (CacheInterface = self._CacheInterface, RILContourCommandLine = self._RILContourCommandLine, FileSystemUID = self._FileSystemUID, MultiUserFileSharingManager = self.getMultiUserFileSharingManager(), UserUID = self._UserUID)  #ProjFileInterface
        newObj._projectPath = self._projectPath
        newObj._projectUID =  self._projectUID
        newObj._projectName = self._projectName 
        newObj._userName =  self._userName        
        newObj._FileWritePermissions = self._FileWritePermissions
        newObj._projectDatasetTagCacheLoaded = self._projectDatasetTagCacheLoaded        
        newObj._projectDatasetTagCacheFileName = self._projectDatasetTagCacheFileName        
        newObj._MultiUserProjectPath = self._MultiUserProjectPath
        return newObj
    

    
    def _generateProjectUID (self) :
       uid =  str (uuid4())
       uid = "ProjectUID_"+ self._projectName + "_" + self._userName + "_" + uid + "_" + str (time.time ())
       return uid
        
     
    def setProjectUID (self, uid) :
         self._projectUID  = uid
         
    def clearProjectUID (self) :
        self._projectUID  = None
        
    def getProjectUID (self) :
        return self._projectUID 
    
    def _getTagCachePath (self) :
        tagPath = self._projectPath
        if (tagPath.lower ().endswith (".prj")) :
            tagPath = tagPath[:-4]
        tagPath += "_tag.cache"
        return tagPath
    
    def _getUserTagCachePath (self) :
        tagPath = self.getMultiUserProjectPath()        
        if tagPath is None :
            return None
        if (tagPath.lower ().endswith (".prj")) :
            tagPath = tagPath[:-4]            
        tagPath += "_tag.cache"
        return tagPath
        
    def _getTagCache (self, path, UseDecompressionCache = True):
        try :
            if UseDecompressionCache :
                if path in self._CacheInterface.getDecompressionCache()  :
                    try :
                        decompressionCacheOrder = self._CacheInterface.getDecompressionCacheOrder()
                        indx = decompressionCacheOrder.index (path)
                        if indx != len (decompressionCacheOrder ) -1 :
                            del decompressionCacheOrder[indx]
                            decompressionCacheOrder.append (path)
                    except:
                        pass
                    return self._CacheInterface.getDecompressionCache()[path] 
            rawDataCache = self._CacheInterface.getDatasetCache()
            if path not in rawDataCache :
                rawDataCache[path] = {}
            found = rawDataCache[path]                        
            if not isinstance (found, dict) :
                found =  pickle.loads (zlib.decompress (found))   
            if not UseDecompressionCache :
                return found
            else:
                decompressionCacheOrder = self._CacheInterface.getDecompressionCacheOrder()
                decompressionCache = self._CacheInterface.getDecompressionCache()
                if (len (decompressionCacheOrder ) > 200) : # max decompressed Tags
                    removePath = decompressionCacheOrder[0]
                    rawDataCache[removePath] = ProjFileInterface.getCompressedTag (decompressionCache[removePath])
                    del decompressionCacheOrder[0]
                    del decompressionCache[removePath]
                decompressionCache[path] = found
                decompressionCacheOrder.append (path)
                return found
        except:                        
            return {}
        
        
    def _setTagCache (self, path, rawValue) :
        decompressionCache = self._CacheInterface.getDecompressionCache()
        if path in decompressionCache :
            decompressionCache[path]  = rawValue            
        else:
            self._CacheInterface.getDatasetCache()[path] = ProjFileInterface.getCompressedTag  (rawValue)
        
    @staticmethod
    def getCompressedTag (value):
        try :
            return zlib.compress (pickle.dumps (value, protocol=2), 9)            
        except:
            raise
        
    def hasProjectDatasetTagCacheFlagChanged (self, path, flagname, flagvalue) :
        try :
            self.aquireFileLock ()                         
            if path not in self._CacheInterface.getDatasetCache() :
                return True        
            FlagDir = self._getTagCache(path) 
            if flagname not in FlagDir or FlagDir[flagname] != flagvalue:
                FlagDir[flagname] = flagvalue
                return True        
            return False
        except:
            return True
        finally: 
            self.releaseFileLock ()
    
    def _isProjectTagFileLoaded (self) :
        return self._projectDatasetTagCacheLoaded         
    
    def _setProjectTagFileLoaded (self) :
        self._projectDatasetTagCacheLoaded = True
        
    def _InitProjectTagFileLoaded (self) :
        self._projectDatasetTagCacheLoaded = False
    
    def saveProjectDatasetCache (self) : 
        if (self._isProjectTagFileLoaded ()) :               
            try:
                self.aquireFileLock ()
                if self._projectDatasetTagCacheFileName is not None :
                    path = self._projectDatasetTagCacheFileName
                    if path is not None :
                        if path == self._getUserTagCachePath () or (path == self._getTagCachePath () and not self.isProjectReadOnly ()) :
                            for key in self._CacheInterface.getDecompressionCache().keys () :
                                self._CacheInterface.getDatasetCache()[key] = ProjFileInterface.getCompressedTag (self._CacheInterface.getDecompressionCache()[key])                            
                            with open (path,"wb") as pickleFile :
                                out = self._CacheInterface.getDatasetCacheDictionary ()
                                pickle.dump (out, pickleFile, protocol=2)
                                del out
                            print ("Saving project dataset cache to: " + path)
                            FileUtil.setFilePermissions (path, self.getFileWritePermissions ())
               
            finally: 
                self.releaseFileLock ()
    
    def getCacheSize (self, AquireFileLock = True) :
        if (self._isProjectTagFileLoaded ()) :   
            if AquireFileLock :             
                self.aquireFileLock ()
            try:                
                return len (self._CacheInterface.getDatasetCache())
            finally: 
                if AquireFileLock :             
                    self.releaseFileLock ()
        return 0
    
    
    def _loadProjectDatasetTagPath (self, tagPath, userPath) :
       if (self._isProjectTagFileLoaded ()) :   
          return 
       LoadTagPathList = []       
       try:
           if self.isInSuperUserCommandLineMode () :
               userPath = None
           if self.isProjectReadOnly () :
               if userPath is not None :
                   CopyMasterTagPathOverUserPath = False
                   if os.path.isfile (tagPath) :
                       if not os.path.isfile (userPath)  :
                           CopyMasterTagPathOverUserPath = True
                       else:
                           try :
                               userPathStat = os.stat (userPath) 
                               tagPathStat = os.stat (tagPath) 
                               if userPathStat.st_mtime  < tagPathStat.st_mtime :
                                    CopyMasterTagPathOverUserPath = True
                           except:
                               pass                     
                   if (CopyMasterTagPathOverUserPath) :
                       try :
                           shutil.copyfile (tagPath, userPath)
                           FileUtil.setFilePermissions (userPath, self.getFileWritePermissions ())
                       except:                             
                           for key in list (self._CacheInterface.getDatasetCache().keys ()) :
                               del self._CacheInterface.getDatasetCache()[key]
                           out = {}
                           with open (userPath,"wb") as pickleFile :
                                pickle.dump (out, pickleFile, protocol=2)
                           FileUtil.setFilePermissions (userPath, self.getFileWritePermissions ())                                                             
               LoadTagPathList = [userPath]
           else:
               if userPath is not None and os.path.isfile (userPath) and os.path.isfile (tagPath) :
                   userPathStat = os.stat (userPath) 
                   tagPathStat = os.stat (tagPath) 
                   if userPathStat.st_mtime  < tagPathStat.st_mtime :
                       try :                           
                           shutil.copyfile (tagPath, userPath)
                           FileUtil.setFilePermissions (userPath, self.getFileWritePermissions ())
                           LoadTagPathList = [userPath]
                       except:                             
                           os.remove (userPath)
                           LoadTagPathList = [tagPath]
                   else:
                       LoadTagPathList = [userPath, tagPath]
               elif userPath is not None and os.path.isfile (userPath) :
                   LoadTagPathList = [userPath]
               elif os.path.isfile (tagPath) :
                   LoadTagPathList = [tagPath]
            
           LoadedTags = False
           for path in LoadTagPathList :          
               try : 
                   if path is not None and os.path.exists (path) :                   
                       with open (path,"rb")  as pickleFile :
                           indict = pickle.load(pickleFile)  
                       self._CacheInterface.setDatasetCacheDictionary (indict)    
                       self._projectDatasetTagCacheFileName = path
                       print ("Dataset cache loaded from: " + path)
                       LoadedTags = True
                       break
               except:
                   pass
           if not LoadedTags :                   
               if self.isProjectReadOnly () and userPath is not None:
                  LoadTagPathList = [userPath]
               elif (userPath is None or not os.path.isfile (userPath)) and os.path.isfile (tagPath) :
                  LoadTagPathList = [tagPath]
               elif userPath is not None and os.path.isfile (userPath) and not os.path.isfile (tagPath) :
                  LoadTagPathList = [userPath]
               else:
                  try :
                      os.remove (userPath)
                  except: 
                      pass
                  if userPath is None :
                      LoadTagPathList = [tagPath]           
                  else:
                      LoadTagPathList = [tagPath, userPath]           
               for key in list (self._CacheInterface.getDatasetCache().keys ()) :
                   del self._CacheInterface.getDatasetCache()[key]
               out = {}
               SavedTags = False
               for path in LoadTagPathList           :
                   try :
                       with open (path,"wb") as pickleFile :
                           pickle.dump (out, pickleFile, protocol=2)
                       FileUtil.setFilePermissions (path, self.getFileWritePermissions ())
                       SavedTags = True
                       self._projectDatasetTagCacheFileName = path
                       break
                   except:
                       try:
                           os.remove (path)
                       except:
                           pass
               if not SavedTags :               
                  self._projectDatasetTagCacheFileName = None     
       except:
           for key in list (self._CacheInterface.getDatasetCache().keys ()) :
                 del self._CacheInterface.getDatasetCache()[key]
           self._projectDatasetTagCacheFileName = None     
       for key in list (self._CacheInterface.getDecompressionCache().keys ()) :
           del self._CacheInterface.getDecompressionCache()[key]
       while len (self._CacheInterface.getDecompressionCacheOrder() ) > 0 :
           del self._CacheInterface.getDecompressionCacheOrder() [0]
       self._setProjectTagFileLoaded ()
       return 
   
    def clearProjectDatasetCache (self) :
        self._projectDatasetTagCacheFileName = None
        for key in list (self._CacheInterface.getDatasetCache().keys ()) :
            del self._CacheInterface.getDatasetCache()[key]
        for key in list (self._CacheInterface.getDecompressionCache().keys ()) :
            del self._CacheInterface.getDecompressionCache()[key]
        while len (self._CacheInterface.getDecompressionCacheOrder() ) > 0 :
           del self._CacheInterface.getDecompressionCacheOrder() [0]
     
    def initalizeEmptyProjectDatasetCache (self) :
        try:
            self.aquireFileLock ()
            self.clearProjectDatasetCache ()
            self._projectDatasetTagCacheFileName = self._getTagCachePath ()
            try :
                os.remove (self._projectDatasetTagCacheFileName)
            except:
                pass
        finally: 
            self.releaseFileLock () 
            
    def loadProjectDatasetCache (self) :
        try:
            self.aquireFileLock ()
            self._InitProjectTagFileLoaded ()
            self._loadProjectDatasetTagPath  (self._getTagCachePath (), self._getUserTagCachePath ())     
            return self.getCacheSize (AquireFileLock = False)
        finally: 
            self.releaseFileLock ()  
    

    def hasProjectDatasetCache (self, treePath, AquireFileLock = True) :
        try:
            if AquireFileLock :
                self.aquireFileLock ()            
            return treePath in self._CacheInterface.getDecompressionCache() or treePath in self._CacheInterface.getDatasetCache()   
        except:
            return False
        finally: 
            if AquireFileLock :
                self.releaseFileLock ()
        
    
    def getProjectDatasetCache (self, treePath, Key = None, Default=None, AquireFileLock = True, UseDecompressionCache = True) :
        try:
            if AquireFileLock :
                self.aquireFileLock ()            
            if treePath in self._CacheInterface.getDecompressionCache() or treePath in self._CacheInterface.getDatasetCache() :
                if Key is None :
                    return self._getTagCache (treePath, UseDecompressionCache = UseDecompressionCache)
                cache = self._getTagCache (treePath, UseDecompressionCache = UseDecompressionCache)
                if Key in cache :
                    return cache[Key]
                elif Default is not None :
                    return Default
                else:
                    print ("getProjectDatasetCache")
                    print ("TreePath: " + str (treePath))
                    print ("Unexpected Key: " + str (Key)) 
                    import traceback
                    traceback.print_stack ()
                    sys.exit ()
            return None
        except :
            return None
        finally: 
            if AquireFileLock :
                self.releaseFileLock ()
        
    
    def renameProjectDatasetCacheTreePath (self, oldTreePath, newTreePath,AquireFileLock = True) :
        try:
            if AquireFileLock :
                self.aquireFileLock ()                    
            decompressionCache = self._CacheInterface.getDecompressionCache() 
            if oldTreePath in decompressionCache :
                decompressionCache[newTreePath] = decompressionCache[oldTreePath]
                del decompressionCache[oldTreePath]
            compressedCache = self._CacheInterface.getDatasetCache()     
            if oldTreePath in compressedCache :
                compressedCache[newTreePath] = compressedCache[oldTreePath]
                del compressedCache[oldTreePath]                    
            cacheOrder = self._CacheInterface.getDecompressionCacheOrder()
            cacheOrder[cacheOrder.index (oldTreePath)] = newTreePath        
        finally: 
            if AquireFileLock :
                self.releaseFileLock ()
            
        
    def setProjectDatasetCache (self, treePath, value, Key = None, AquireFileLock = True, DictSet = False) :
        try:
            Change = False
            if AquireFileLock :
                self.aquireFileLock ()            
            if DictSet :
                tag = self._getTagCache (treePath)                  
                for vkey, val in value.items () :
                    if vkey not in tag or tag[vkey] != val :
                        Change = True
                        tag[vkey] = val
                if (Change) :
                    self._setTagCache (treePath, tag)
            elif Key is not None :
                tag = self._getTagCache (treePath)                  
                if Key not in tag or tag[Key] != value :
                    Change = True
                    tag[Key] = value
                    self._setTagCache (treePath, tag)
            elif Key is None and isinstance (value, dict) :  
                try:
                    tag = self._getTagCache (treePath)                           
                    for key, val in value.items () :
                        if key not in tag or tag[key] != val :
                            Change = True
                            break                        
                    if not Change :
                        for key in tag.keys () :
                            if key not in value :
                                Change = True
                                break
                except:
                    Change = True
                if Change :
                    self._setTagCache (treePath, value)                
            else:
                print ("ERROR: unexpected value")
                print ("TreePath: " + str (treePath))
                print ("Value: " + str (value))
                import traceback
                traceback.print_stack ()
                sys.exit ()
        finally: 
            if AquireFileLock :
                self.releaseFileLock ()
            return Change
   
    def removeProjectDatasetCacheEntry(self, TreePath, AquireFileLock = True) :
        try:
            if AquireFileLock :
                self.aquireFileLock ()               
            
            decompressionCache = self._CacheInterface.getDecompressionCache()
            if TreePath in decompressionCache :
                del decompressionCache[TreePath]
                cacheOrder = self._CacheInterface.getDecompressionCacheOrder()
                del cacheOrder[cacheOrder.index (TreePath)]                
            dsCache = self._CacheInterface.getDatasetCache()
            if TreePath in dsCache:
                del dsCache[TreePath]                  
        finally: 
            if AquireFileLock :
                self.releaseFileLock ()
                
    def getProjectDatasetCacheForList (self, TreePathList, AquireFileLock = True, ProgressDialog = None, BaseIndex = 0) :
        try:
            if AquireFileLock :
                self.aquireFileLock ()               
            returnList = []
            for index, treePath in enumerate(TreePathList) :
                if treePath in self._CacheInterface.getDatasetCache() :
                    returnList.append (self._getTagCache (treePath, UseDecompressionCache = False))
                else:
                    returnList.append (None)
                if (ProgressDialog is not None) :
                    ProgressDialog.setValue (index + BaseIndex)
                    ProgressDialog.setLabelText ("Uncompressing: " + treePath)                
            return returnList
        finally: 
            if AquireFileLock :
                self.releaseFileLock ()
    
    def setProjectDatasetCacheFromBlockList (self, encodedCacheList, progdialog) :
        if (self._isProjectTagFileLoaded ()) :
            try :
                self.aquireFileLock ()
                progdialog.setLabelText ("Updating Cache")        
                progdialog.setMaximum (len (encodedCacheList))        
                
                tagPath = None
                if self._projectDatasetTagCacheFileName is not None :
                    tagPath = self._projectDatasetTagCacheFileName 
                else:
                    tagPath = self._getTagCachePath ()
                    if os.path.exists (tagPath) :
                        self._projectDatasetTagCacheFileName  = tagPath
                    else:
                        tagPath = None                        
                if (tagPath is not None) :                    
                    for blockIndex, blockResult in enumerate(encodedCacheList) :
                        try:
                            treePath, tagDataset = blockResult                                                 
                            self._setTagCache (treePath, tagDataset)
                            progdialog.processSystemEvents ()
                        except:
                            pass
                        progdialog.setValue (blockIndex)                        
            finally:
                self.releaseFileLock ()
        
    def setProjectDatasetTagCache (self, treePath, tagDataset, AquireFileLock = True) :
        if (self._isProjectTagFileLoaded ()) :
           try :
                if AquireFileLock :
                    self.aquireFileLock ()
                tagPath = None
                if self._projectDatasetTagCacheFileName is not None :
                    tagPath = self._projectDatasetTagCacheFileName 
                else:
                    tagPath = self._getTagCachePath ()
                    if os.path.exists (tagPath) :
                        self._projectDatasetTagCacheFileName  = tagPath
                    else:
                        tagPath = None                                                    
                if (tagPath is not None) :                                        
                    self.setProjectDatasetCache (treePath, tagDataset, Key = None, AquireFileLock = False, DictSet = True)                    
           finally:
                if AquireFileLock :
                    self.releaseFileLock ()
    
    def isCacheDifferent (self, treePath, compressedCachedResult, AquireFileLock = True) :
        try :
            if AquireFileLock :
                self.aquireFileLock ()
            if treePath in self._CacheInterface.getDecompressionCache() :
                tagDataset = self._CacheInterface.getDecompressionCache()[treePath] 
                compressedCache = ProjFileInterface.getCompressedTag (tagDataset)
                self._CacheInterface.getDatasetCache()[treePath] = compressedCache
                return compressedCache != compressedCachedResult
            elif treePath in self._CacheInterface.getDatasetCache() :
                return self._CacheInterface.getDatasetCache()[treePath]  != compressedCachedResult
            return True
        finally:
            if AquireFileLock :
                self.releaseFileLock ()
        
    @staticmethod
    def isFileCompressedCache (Path) :
        return Path.endswith ("_DSCache.bin")
    
    @staticmethod
    def getROIFilePathFromFileCompressedCache (Path) :
        return FileUtil.changeExtention (Path, "_DSCache.bin","_ROI.txt")
    
    @staticmethod
    def getFileCompressedCachePathFromROIFilePath (Path) :
        return FileUtil.changeExtention (Path, "_ROI.txt", "_DSCache.bin")
            
    def saveDatasetCompressedCache (self,  datasetPath, tagDataset, ProjectDataset = None) :
            print ("Dataset Cache Saved: " + datasetPath)
            try:
                self.aquireFileLock ()
                filename = FileUtil.changeExtention (datasetPath, "_ROI.txt", "_DSCache.bin")            
                if isinstance (tagDataset, dict) :
                    compressedDataset = ProjFileInterface.getCompressedTag (tagDataset)
                else:
                    compressedDataset = tagDataset
                with open (filename, "wb") as outfile :
                    outfile.write (compressedDataset)    
                FileUtil.setFilePermissions (filename, self.getFileWritePermissions ())      
            finally:
                self.releaseFileLock ()
            if ProjectDataset is not None :
               try :
                    niftiInterface = ProjectDataset.getNIfTIDatasetInterface ()
                    if niftiInterface is not None :
                        eventHandler = niftiInterface.getFileSysetmEventHandler ()
                        if eventHandler is not None :
                            eventHandler.processEvent (filename)    
               except:
                    print ("Error occured in saveDatasetCompressedCache calling lockfile path process event")
           
    def getDatasetCompressedCache (self, treePath) :
        try :
            self.aquireFileLock ()
            if treePath in self._CacheInterface.getDecompressionCache() :
                decompressedCache = self._CacheInterface.getDecompressionCache()[treePath] 
                compressedCache = ProjFileInterface.getCompressedTag (decompressedCache)
                self._CacheInterface.getDatasetCache()[treePath] = compressedCache
                return compressedCache
            if treePath in self._CacheInterface.getDatasetCache() :                
                return self._CacheInterface.getDatasetCache()[treePath]
            print ("Error tree Path Not found")
            return None 
        finally:
            self.releaseFileLock ()
        
    
    @staticmethod
    def loadDatasetCompressedCache (datasetPath, ReturnCompressedAlso = False) :
           filename = ProjFileInterface.getFileCompressedCachePathFromROIFilePath (datasetPath)
           try :
               if os.path.isfile (filename) :
                    with open (filename, "rb") as infile :
                        decompressedData = pickle.loads (zlib.decompress (infile.read ()))                    
                        decompressedData["DataCacheChange"] = FileChangeMonitor (filename)
                        if ReturnCompressedAlso :         
                            compresseddata = ProjFileInterface.getCompressedTag (decompressedData)
                            return (decompressedData, compresseddata)                    
                        return decompressedData                      
           except:
               try :
                   os.remove (filename)
               except:
                   pass
           if ReturnCompressedAlso :
               return (None, None)  
           return (None) 