import os
from six import *
from rilcontourlib.util.rcc_util import FileChangeMonitor
import time
import yaml
from rilcontourlib.util.FileUtil import FileUtil

class User :
    def __init__ (self) :
        self._userName = None  # users must have unique names and groups must have unique names
        self._userUID  = None
        self._assignTo = []
        self._assignToExclusions = []
        self._AccessUnAssignedData = True
        self._nickname = None  # users must have unique names and groups must have unique names
        self._defaultCheckin = None 
        self._getUserGroups  = None
        
    def getUserYAMLStruct (self) :
        if self._userName is None :
            return None
        ret = {}
        if self._userUID is not None :
            ret["userid"] = self._userUID
        if len (self._assignTo) > 0  :
            lst = []
            for item in self._assignTo :
                lst.append (item)
            ret["assign"] = lst
        if len (self._assignToExclusions) > 0  :
            lst = []
            for item in self._assignToExclusions :
                lst.append (item)
            ret["exclude"] = lst
        ret["read_none"] = self._AccessUnAssignedData
        if self._defaultCheckin is None : 
            ret["defaultcheckin"] = "none"
        else:
            ret["defaultcheckin"] = self._defaultCheckin
        if self._nickname is not None :
            ret["nickname"] = self._nickname
        return ret
    
    def setUserGroups (self, grouplist) :
        self._getUserGroups = grouplist
        
    def getUserGroups (self) :
        return self._getUserGroups
    
    def setNickName (self, name) :
        self._nickname = name
    
    def getNickName (self):
        return self._nickname 
        
    def getDefaultCheckin (self) :
        return self._defaultCheckin
            
    def setDefaultCheckin (self, name) :
        self._defaultCheckin = MultiUserManager._lowerStr(name)
        
    def setName (self, userName) :
        self._userName = MultiUserManager._lowerStr(userName)
    
    def setAccessToUnassignedData (self, val) :
        self._AccessUnAssignedData = val
        
    def setUID (self, userid):
        self._userUID  = userid
    
    def setAssignTo (self, assignTo):
        self._assignTo = assignTo
    
    def setExclude (self, exlcusion):
        self._assignToExclusions = exlcusion
    
    def canAccessUnAssignedData (self) :
        return self._AccessUnAssignedData
    
    def getName (self):
        return self._userName
    
    def getExclusionList (self) :
        return self._assignToExclusions
        
    def getAssignToList (self) :
        return self._assignTo
    
    def getUserID (self) :
        return self._userUID
    
    def isUserChanged (self, testUser, IgnoreUserID = False) :
        if self._userName != testUser._userName :
            return True
        if self._AccessUnAssignedData != testUser._AccessUnAssignedData :
            return True
        if self._userUID != testUser._userUID and not IgnoreUserID :
            return True
        if len (self._assignTo) != len (testUser._assignTo) :
            return True
        if len (self._assignToExclusions) != len (testUser._assignToExclusions) :
            return True
        for item in self._assignTo :
            if item not in testUser._assignTo :
                return True
        for item in self._assignToExclusions :
            if item not in testUser._assignToExclusions :
                return True
        if self.getDefaultCheckin () != testUser.getDefaultCheckin () :
            return True
        if self.getNickName () != testUser.getNickName () :
            return True
        return False

        
        
class MultiUserManager :
    def __init__ (self, yamlPath) :
        try :
            self._users = {}
            self._groups = {}
            self._yamlFileMonitor = None
            self._FileLoadFailed = False
            self._InternalText = None
            self._FileSavePermissions = 0o775
            if yamlPath is None :
                self._YAMLPath = None
            elif not os.path.exists (yamlPath) :
                self._YAMLPath = None
                self._FileLoadFailed = True
            else:
                try :
                    self._YAMLPath = yamlPath
                    self._yamlFileMonitor = FileChangeMonitor (yamlPath)
                    result = MultiUserManager.loadFile (yamlPath)
                    if result is not None:
                        self._users = result._users
                        self._groups = result._groups
                except:
                    self._users = {}
                    self._groups = {}
                    self._yamlFileMonitor = None
                    self._YAMLPath = None
                    self._FileLoadFailed = True
        finally:
            self._genNicknameLookup ()
            
    def _genNicknameLookup (self) :
        self._nicknames = {}
        self._reverseUserNameLookup = {}
        for userName, userData in self._users.items () :
           if userData.getNickName () is not None :
                lnNAme = MultiUserManager._lowerStr (userData.getNickName ())
                if lnNAme not in self._users and lnNAme not in self._groups and lnNAme != "none" :
                    nickName = MultiUserManager._lowerStr (lnNAme)
                    self._nicknames[userName] = nickName
                    self._reverseUserNameLookup[nickName] = userName
        for userName, userData in self._users.items () : 
            if userData.getDefaultCheckin () is None :
                groupList = self.getUserGroups (userName)
                if len (groupList) == 1 :
                    userData.setDefaultCheckin (groupList[0])
                else:
                    userData.setDefaultCheckin ("none")
           
    @staticmethod 
    def _lowerStr (text) :
        if text is None :
            return None
        return (str(text)).lower ()
    
    @staticmethod
    def validate (mum1, mum2) :
        try:
            if len (mum1._groups) != len (mum2._groups) or len (mum1._users) != len (mum2._users):
                return False
            for userName in mum1._users.keys ():
                if userName not in mum2._users :
                    return False
                if mum1._users[userName].isUserChanged (mum2._users[userName], IgnoreUserID = True) :
                    return False
            for groupName in mum1._groups.keys ():
                if groupName not in mum2._groups :
                    return False
                if len (mum1._groups[groupName]) != len (mum2._groups[groupName]) :
                    return False
                for item in mum1._groups[groupName] :
                    if item not in mum2._groups[groupName] :
                        return False
            return True   
        except:
            return False
    
    def saveInternalYAMLToFile (self, path) :
        if self._InternalText is not None :
            try:
                with open (path, "wt") as outfile :
                    outfile.write (self._InternalText)
                FileUtil.setFilePermissions (path, self._FileSavePermissions)
                return True
            except:
                pass
        return False
            
    def removeInternalYAMLFile (self) :
        self._users = {}
        self._groups = {}
        self._yamlFileMonitor = None
        self._YAMLPath = None
        self._InternalText = None
        self._genNicknameLookup ()
        
    def setInternalYAMLFile (self, yamlPath, projectFilePath, YAMLText = None) :
        try :
             try :
                 #validate that text c
                 if YAMLText is None :
                     with open (yamlPath,"rt") as inputfile :
                         text = inputfile.read ()
                 else:
                     text = YAMLText
                 manager = MultiUserManager.parseText (text) 
                 if manager is not None :
                     self._InternalText = text
                     self._users = manager._users
                     self._groups = manager._groups
                     
                     MultiUserManagerPath = FileUtil.changeExtention (projectFilePath,".prj",".tmum")
                     for _ in range (5) :
                            try:    
                                with open (MultiUserManagerPath,"wt") as outfile :
                                    outfile.write (text)
                                    self._yamlFileMonitor = FileChangeMonitor (MultiUserManagerPath)
                                    self._YAMLPath = MultiUserManagerPath
                                FileUtil.setFilePermissions (MultiUserManagerPath, self._FileSavePermissions)
                                return True
                            except:
                                time.sleep (5)
                                self._yamlFileMonitor = None
                                self._YAMLPath = None
                     return True
             except:
                 pass
             self._users = {}
             self._groups = {}
             self._yamlFileMonitor = None
             self._YAMLPath = None
             self._InternalText = None
             return False
        finally:
             self._genNicknameLookup ()
             
    def initFromText (self, text, projectFilePath) :
        try:
            self._users = {}
            self._groups = {}
            self._yamlFileMonitor = None
            self._FileLoadFailed = False
            self._YAMLPath = None
            self._yamlFileMonitor = None
            self._InternalText = text
            """try :
                internalData = MultiUserManager.parseText (text)
            except:
                internalData = None
                self._FileLoadFailed = True
                return"""
          
            externalData = None    
            MultiUserManagerPath = FileUtil.changeExtention (projectFilePath,".prj",".tmum")
            if os.path.exists (MultiUserManagerPath) :
                try :
                    externalData = MultiUserManager.loadFile (MultiUserManagerPath)
                except:
                    externalData = None
                    try :
                        os.remove (MultiUserManagerPath)
                    except:
                        pass
            if externalData is not None :        
                #internalData = MultiUserManager.parseText (text)
                #if MultiUserManager.validate (externalData, internalData) :
                    self._users = externalData._users
                    self._groups = externalData._groups
                    self._yamlFileMonitor = FileChangeMonitor (MultiUserManagerPath)
                    self._YAMLPath = MultiUserManagerPath
                    return True
            return self.setInternalYAMLFile (None, projectFilePath, YAMLText = text)
        finally:
             self._genNicknameLookup ()
    
    def didLoadFromFile (self)  :
        return self._YAMLPath is not None
    
    def hasEmbededYAML (self) :
        return self._InternalText is not None
    
    def didLoadFromText (self)  :
        return self._InternalText is not None
    
    def didLoadFromFileFail (self) :
        return self._FileLoadFailed
    
    def isInitalized (self) :
        return (self.didLoadFromFile () or self.didLoadFromText ()) and not self.didLoadFromFileFail ()
    
    def _buildAssignToUserList (self, assignTo, SelfUserName = None) :
        groupNames = []
        userNames = []
        addNone = False
        for item in assignTo :
            if item == "none" :
                addNone = True
            elif item in self._groups :
                groupNames.append (item)
                userNames += self._groups[item]
            elif item in self._users :
                userNames.append (item)
        if SelfUserName is not None :
            if SelfUserName not in userNames :
                userNames.append (SelfUserName)
        groupNames = sorted (list (set (groupNames)))
        nickNameList = []
        for un in userNames :
            if un in self._nicknames :
                nickNameList.append (self._nicknames[un])
        userNames = sorted (list (set (userNames)))
        nickNameList = sorted (list (set(nickNameList)))
        if addNone :
            return ["none"] + groupNames + nickNameList + userNames
        else:
            return groupNames + nickNameList + userNames
   
    def isManagerChanged (self, testmgr):
        if len (self._users) != len (testmgr._users) :
            return True
        if len (self._groups) != len (testmgr._groups) : 
            return True
        for key, value in self._groups.items () :
            if key not in testmgr or testmgr._groups[key] != value :
                return True
        for userName, value in self.users.items () :
            if userName not in testmgr.users or value.isUserChanged (testmgr.users[userName]) :
                return True
        return False
    
    @staticmethod
    def _decodeUser (user):
        if user is None or not isinstance (user, dict) :
            return None
        userName = None
        userid = None
        nickname = None
        defaultcheckin = None
        assignTo = []
        exlcusion = []
        unassigned = True
        for key in user.keys () :
            lkey = MultiUserManager._lowerStr(key)
            if lkey.startswith ("name") or  lkey.startswith ("username") :
                    userName = MultiUserManager._lowerStr (user[key])
            elif lkey.startswith ("nickname") or  lkey.startswith ("screenname") :
                    nickname = MultiUserManager._lowerStr (user[key])
            elif lkey.startswith ("defaultcheckin") or  lkey.startswith ("default") :
                    defaultcheckin = MultiUserManager._lowerStr (user[key])
            elif lkey.startswith ("userid") or  lkey.startswith ("id") :
                    userid = str(user[key])
            elif lkey.startswith ("read_none") or lkey.startswith ("none")  :
                try :
                    unassigned = MultiUserManager._lowerStr (user[key])
                    unassigned = unassigned == "true"
                except:
                     unassigned = True
            elif lkey.startswith ("assignto") or  lkey.startswith ("assign") :
                if isinstance (user[key], list) or isinstance (user[key], set) :
                    for assignment in user[key] :
                        assignTo.append (MultiUserManager._lowerStr (assignment))
                else:
                    assignTo.append (MultiUserManager._lowerStr(user[key]))
            elif lkey.startswith ("exclusion") or  lkey.startswith ("exclude") :
                if isinstance (user[key].value, list) or isinstance (user[key].value, set) :
                    for exclude in user[key] :
                        exlcusion.append (MultiUserManager._lowerStr(exclude))
                else:
                    exlcusion.append (MultiUserManager._lowerStr(user[key]))
        exlcusion = set (exlcusion)
        assignTo = set (assignTo)
        uinstance = User ()
        uinstance.setName (userName)
        uinstance.setNickName (nickname)
        uinstance.setDefaultCheckin (defaultcheckin)
        uinstance.setUID (userid)
        uinstance.setAssignTo (assignTo)
        uinstance.setExclude (exlcusion)
        uinstance.setAccessToUnassignedData (unassigned)
        return uinstance
        
    
    @staticmethod
    def _loadUsers (user):
        if not isinstance (user, dict) and not isinstance (user, list) and not isinstance (user, set)   :
            return None
        userDict = {}
        if isinstance (user, list) or isinstance (user, set) :
            for block in user :
                usrInstance = MultiUserManager._decodeUser (block)
                if usrInstance is not None and usrInstance.getName () is not None :
                    userDict[MultiUserManager._lowerStr(usrInstance.getName ())] = usrInstance
        else:
            userDict = {}
            for userName, values in user.items () :
                lname = MultiUserManager._lowerStr (userName)
                if lname != "none" :
                    usrInstance = MultiUserManager._decodeUser (values)
                    if usrInstance is None :
                        usrInstance = User ()
                        usrInstance.setName (lname)
                        userDict[lname] = usrInstance
                    elif usrInstance.getName () is None :
                        usrInstance.setName (lname)
                        userDict[lname] = usrInstance
                    elif usrInstance.getName ()  ==lname:
                        userDict[lname] = usrInstance
        if len (user) != len (userDict) :
            userDict = None
        return userDict
    
    @staticmethod 
    def _loadGroups (group):
        if not isinstance (group, dict)  :
            return None
        groupDict = {}
        for groupName, values in group.items () :
            lgName = MultiUserManager._lowerStr (groupName)
            if lgName != "none" :
                if (isinstance (values, list) or isinstance (values, set)) :
                    lst = []
                    for item in values :
                        lst.append (MultiUserManager._lowerStr (item))
                    groupDict[lgName] = lst
                else :
                    groupDict[lgName] = [MultiUserManager._lowerStr (values)]
        return groupDict
    
    @staticmethod
    def parseText (fileText) :
        try:
            users = None
            group = None
            try:
               output = yaml.load (fileText, Loader=yaml.FullLoader)
            except:
               output = yaml.load (fileText)
            if isinstance (output, dict) :
                for key in output.keys ():
                    value = MultiUserManager._lowerStr(key)
                    if value.startswith ("user") :
                        users  = MultiUserManager._loadUsers  (output[key])
                    if value.startswith ("group") :
                        group  = MultiUserManager._loadGroups  (output[key])
        except Exception as exp:
            print ("YAML file parser error: " + str (exp))
            users = None
            group = None
        if users is None or group is None :
            return None
        else:
            mum = MultiUserManager (None)
            mum._users = users
            mum._groups = group
            mum._genNicknameLookup ()
            return mum
    
    @staticmethod 
    def loadFile (filepath) :
        mum = None
        try:
            with open (filepath,"rt") as infile :
                inputData = infile.read ()
            mum = MultiUserManager.parseText (inputData)
        except Exception as exp:
            print ("YAML file read error: " + str (exp))
        if mum is not None :
            mum._YAMLPath = filepath
            mum._yamlFileMonitor = FileChangeMonitor (filepath)
        return mum
           
    
    def getYAMLText (self) :
        obj = {}
        obj["groups"] = {}
        for key, value in self._groups.items () :
            lst = []
            for exclude in value : 
                lst.append (exclude)
            obj["groups"][key] = lst
        obj["users"] = {}
        for key, value in self._users.items () :
            userYaml = value.getUserYAMLStruct ()
            if userYaml is not None :
                obj["users"][key] = userYaml
        return yaml.dump (obj)
        
    def saveFile (self, filepath) :
        txt = self.getYAMLText ()
        with open (filepath, "wt") as outfile :
            outfile.write (txt)
        FileUtil.setFilePermissions (filepath, self._FileSavePermissions)
        
    
    def _reloadDef (self) :
        if self._YAMLPath is not None and self._yamlFileMonitor is not None:
            yamlPath = self._YAMLPath
            if self._yamlFileMonitor.hasFileChanged (yamlPath) :
                self._yamlFileMonitor.monitorFile (yamlPath)
                result = MultiUserManager.loadFile (yamlPath)
                if result is None :
                  if self._yamlFileMonitor is not None :
                      self._yamlFileMonitor.clear ()
                elif result is not None:
                    self._users = result._users
                    self._groups = result._groups
                    self._genNicknameLookup ()
    
    
    def correctNickNameIfNecessary (self, userName):
        userName = MultiUserManager._lowerStr (userName)
        if userName is None :
            return "none"
        if userName == "none" :
            return userName
        if userName in  self._users or userName  in self._groups :
            return userName
        if userName in self._reverseUserNameLookup :
            return self._reverseUserNameLookup[userName]
        return userName
     
    def _updateMangerUserUID (self, yamlPath, username, uid) :
        try:
            errorCount = 0
            while (True) :
                if errorCount > 0 :
                    time.sleep (5)
                if self._yamlFileMonitor is not None :
                    self._yamlFileMonitor.monitorFile (yamlPath)
                result = MultiUserManager.loadFile (yamlPath)
                if result is None :
                    return
                elif result is not None:
                    self._users = result._users
                    self._groups = result._groups
                    self._genNicknameLookup ()
                if username in self._users :
                    if self._users[username].getUserID() == uid:
                        return
                    self._users[username].setUID (uid)
                self.saveFile (yamlPath)
                errorCount+= 1
                if errorCount > 10 :
                    print ("Could not update user UID in YAML file will try again later.")
                    return
        except:
            print ("A error occured trying to update user UID in the user file sharing YAML (_updateMangerUserUID)")    
    
    def setUserNameUID (self, userName, UID) :
        userName = self.correctNickNameIfNecessary (userName)
        if userName in self._users :
            currentUID = self._users[userName].getUserID ()
            if currentUID is None or currentUID != UID :
                if self._YAMLPath is not None :
                    self._updateMangerUserUID (self._YAMLPath, userName, UID)            
            return True 
        return False # name not in users
    
    def isUserInGroup (self, userName, groupName) :
        self._reloadDef ()
        groupName = MultiUserManager._lowerStr(groupName)        
        if groupName not in self._groups :
            return False
        userName = self.correctNickNameIfNecessary (userName)
        return userName in self._groups[groupName]
         
    def getUserGroups (self, userName) :
        self._reloadDef ()
        userName = self.correctNickNameIfNecessary (userName)
        if userName in self._users :
            grouplist = self._users[userName].getUserGroups ()
            if grouplist is not None :
                return grouplist
        groupList = []
        for groupName, userNameList in self._groups.items () :
            if userName in userNameList :
                groupList.append (groupName)
        if userName in self._users :
             self._users[userName].setUserGroups (grouplist)
        return groupList  
    
    def getNameUID (self, userName) :
        self._reloadDef ()
        userName = self.correctNickNameIfNecessary (userName)
        if userName in self._users :
            return self._users[userName].getUserID ()
        return None
    
    def getUIDName (self, userUID) :
        self._reloadDef ()
        for name, value in self._users.items ():
            if value.getUserID () == userUID :
                return name
        return None
    
    def isGroupDefined (self, groupName) :
        self._reloadDef ()
        return MultiUserManager._lowerStr(groupName) in self._groups
    
        
    def isUserDefined (self, userName) :
         self._reloadDef ()
         userName = self.correctNickNameIfNecessary (userName)
         return userName in self._users 
         
    def canLockUnlockedFiles (self, userName) :
        userName = self.correctNickNameIfNecessary (userName)
        if userName not in  self._users :
            return False
        return self._users[userName].canAccessUnAssignedData ()
    
    def getUserNickname (self, userName) :
        self._reloadDef ()
        userName = MultiUserManager._lowerStr (userName)
        if userName in self._nicknames :
            return self._nicknames[userName]
        return userName
    
    def getUserDefaultCheckin (self, userName) :
        userName = self.correctNickNameIfNecessary (userName)
        if userName in self._users :
            return self.correctNickNameIfNecessary (self._users[userName].getDefaultCheckin ())
        return None
        
    def getUserAssignments (self, userName, IncludeSelf = False) :
        self._reloadDef ()       
        userName = self.correctNickNameIfNecessary (userName)
        if userName in self._users :
            userDef = self._users[userName]
            if IncludeSelf :
                SelfUserName = userName
            else:
                SelfUserName = None
            assignTo = self._buildAssignToUserList (userDef.getAssignToList (), SelfUserName = SelfUserName)
            exclude  = self._buildAssignToUserList (userDef.getExclusionList ())
            for item in exclude :
                if item in assignTo :
                    del assignTo[assignTo.index(item)]
            if not IncludeSelf :
                if userName in assignTo :
                    del assignTo[assignTo.index(userName)]
                userNickname = self.getUserNickname (userName)
                if userNickname in assignTo :
                    del assignTo[assignTo.index(userNickname)]
            return assignTo
        return None
    
    def getUserName (self, name) :
        if name in self._users :
            return name
        if name in self._groups :
            return name
        
            
    def validateYaml (self) :
        Errors = []
        for groupname, groupvalues in self._groups.items () :
            for groupUserAssignment in groupvalues :
                if groupUserAssignment not in self._users :
                    Errors.append ("Group '%s' contains undefined user '%s'" % (groupname, groupUserAssignment))
        for user in self._users.keys () :
            assignmentList = self.getUserAssignments (user)            
            for assignment in assignmentList :
                assignName = self.correctNickNameIfNecessary (assignment)
                if assignName is not None and assignName != "none" and assignName not in self._groups and assignName not in self._users :
                    Errors.append ("User '%s' contains unidentified assignment '%s'" % (user, assignment))
            defaultCheckin = self._users[user].getDefaultCheckin ()
            if defaultCheckin is not None and defaultCheckin != "none" and defaultCheckin not in self._groups and defaultCheckin not in self._users :
                Errors.append ("User '%s' contains unidentified default-checkin '%s'" % (user, defaultCheckin))
            nickname = self._users[user].getNickName ()
            if nickname is not None :
                if nickname in self._groups :
                    Errors.append ("User '%s' has nickname, '%s', that matches a group name. The nickname will be ignored" % (user, nickname))
                elif nickname in self._users :
                    Errors.append ("User '%s' has nickname, '%s', that matches a users name. The nickname will be ignored" % (user, nickname))
                else:
                    for inner_user in self._users.keys () :
                        if inner_user != user :
                            if self._users[inner_user].getNickName () == nickname :                            
                                Errors.append ("User '%s' and user '%s' have matching nicknames. The behavior will be undefined. One of them will be ignored." % (user, inner_user))
        return Errors