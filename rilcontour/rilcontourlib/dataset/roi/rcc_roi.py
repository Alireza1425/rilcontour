"""
Created on Fri Oct 14 13:25:37 2016

@author: Philbrick
"""
import datetime
import os
import copy
import gc
import math
import sys
import numpy as np
from PyQt5 import  QtGui
from six import *
from rilcontourlib.dataset.roi.rcc_roiobjects import MultiROIAreaObject, ROIPointObject
from rilcontourlib.util.rcc_util import FileInterface, FileObject, HounsfieldVisSettings, VisSettings, ANTSRegistrationLog, DatasetTagManager, NiftiVolumeData, PlatformMP, FileChangeMonitor, FindContours, Coordinate, ActivityTimer, TimeLog,MessageBoxUtil, FastUnique
from rilcontourlib.dataset.roi.rcc_roiobjects import  Contour
from rilcontourlib.dataset.roi.rcc_ROIUIDManager import ROIUIDManager
from scipy import ndimage    
import nibabel as nib
from rilcontourlib.util.DateUtil import TimeUtil, DateUtil
import time
import pkg_resources
#from numba import jit

"""
  class ROIDEF
  
  Defines a region of intrest (ROI). ROI names are keyed by name (unique).
  Color defines the color the ROI object is drawn in.
  Type defines the ROI as being a "Point" or "Area" object.
"""

    
class ROIDef :
    def __init__ (self, roiIDNumber = None, name = "", color = (0,0,0), roitype = "Point", mask = 1, Hidden=False, RadlexID = "unknown", Locked = False, ROIDefUIDObj = None): 
        if ROIDefUIDObj is None : 
            self._ROIDefUIDObj = ROIUIDManager ()
        else:
            self._ROIDefUIDObj = ROIDefUIDObj.copy ()
        self._idNumber   = roiIDNumber
        self._name = name
        self._maskValue = mask
        self._color = color
        color = color[1] # test that color is not a QColor
        self._roitype = roitype
        self._Hidden = Hidden
        self._locked = Locked
        if RadlexID is None or len (RadlexID) == 0 :
            self._RadlexID = "unknown"
        else:
            self._RadlexID = RadlexID
        return

    def _setIdNumber (self, num) :
        self._idNumber  = num 
        
    def getDefinedUID (self) :
        return self._ROIDefUIDObj
    
    def getNameUID (self, name) :
        return self._ROIDefUIDObj.getNameUID (name)
    
    def getUIDName (self, ROI_UID) :
        return self._ROIDefUIDObj.getUIDName (ROI_UID)
    
    def hasUIDDefined (self) :
        return self._ROIDefUIDObj.hasUIDDefined ()
    
    def getFirstDefinedUID  (self) :
        return self._ROIDefUIDObj.getFirstDefinedUID ()
    
    def isUIDDefined (self, UID):
        return self._ROIDefUIDObj.hasUIDKey (UID)
        
    def getUIDList (self) :
        return self._ROIDefUIDObj.getUIDList ()
    
    def defineUIDKey (self,key, project, name):
        self._ROIDefUIDObj.defineUIDKey (key, project, name)
        
    def setNameForUIDKey (self, UID) :
        name = self._ROIDefUIDObj.getUIDName (UID)
        if name is not None :
            self._name = name
            return True
        return False
        
    def isLocked (self) :
        return self._locked
    
    def setLocked (self, val) :
        self._locked = val
        
    def equals (self, testdef) :
        if self._idNumber != testdef._idNumber :
            return False
        elif self._name != testdef._name :
            return False
        elif self._maskValue != testdef._maskValue :
            return False
        elif self._color != testdef._color :
            return False
        elif self._roitype != testdef._roitype :
            return False
        elif self._Hidden != testdef._Hidden :
            return False
        elif self._RadlexID != testdef._RadlexID :
            return False
        elif self._locked != testdef._locked :
            return False
        elif not self._ROIDefUIDObj.equals (testdef._ROIDefUIDObj) :
            return False
        else:
            return True
    
    def copy (self) :
        return ROIDef (roiIDNumber = self._idNumber, name= self._name, color= self._color,roitype = self._roitype, mask = self._maskValue, Hidden= self._Hidden, RadlexID= self._RadlexID, Locked= self._locked, ROIDefUIDObj = self._ROIDefUIDObj)
        
    def getName (self) :
        return self._name
        
    def getType (self) :
        return self._roitype
    
    def setMask (self, mask):
        self._maskValue = mask
    
    def getMask (self) :
        return self._maskValue
    
    def getRadlexID (self) :
        return self._RadlexID
    
    def setDef (self, roidef) :        
        self._name = roidef[0]  
        self._color = (int (roidef[1]) ,int (roidef[2]) ,int (roidef[3]))      
        self._roitype = roidef[4]                                
        self._maskValue = 1
        self._locked = False
        if (len (roidef) >= 6) :
            self._maskValue = roidef[5]                                
        self._Hidden = False
        if (len (roidef) >= 7) :
            self._Hidden = roidef[6]                                
        if (len (roidef) >= 8) :
            self._RadlexID = roidef[7]                                
            if (self._RadlexID is None) :
                self._RadlexID = "unknown"
            else:
                self._RadlexID = self._RadlexID.strip ()
                if (len (self._RadlexID) == 0) :
                    self._RadlexID = "unknown"
        else:
            self._RadlexID = "unknown"
        if (len (roidef) >= 9) :
            try :
                if roidef[8] :
                    self._locked = True
                else:
                    self._locked = False
            except:
                self._locked = False
        self._ROIDefUIDObj = ROIUIDManager ()
        self._idNumber = None
        if (len (roidef) >= 10) :
            try :
                self._ROIDefUIDObj.initFromTuple (roidef[9])
            except:
                self._ROIDefUIDObj = ROIUIDManager ()
            self._idNumber = self._ROIDefUIDObj.getNameUID (self._name)
        self._Hidden = False # don't actually set always load un hidden
              
            
    def getDef  (self) :
        return ((self._name, self._color[0], self._color[1], self._color[2], self._roitype, self._maskValue, self._Hidden, self._RadlexID, self._locked, self._ROIDefUIDObj.getTuple ()))        

    def getColor (self) :        
        return self._color
    
    def getQColor (self) :
        return QtGui.QColor (self._color[0], self._color[1], self._color[2])
    
    def getIDNumber (self) :
        return self._idNumber

    def setNameAndColor (self, name = None, color = None):
        change = False
        if (name != None) :
            self._name = name
            change = True
        if (color != None) :
            self._color = color
            color = color[1] # test that color is not a QColor
            change = True
        return change
    
    def setHidden (self, value) :
        self._Hidden = value
    
    def isHidden (self) :
        return self._Hidden
        
"""class ROIDefIDManger :
    def __init__ (self) :
        self._idNumber = 0
        self._recycleList = []
        self._allocatedIDList = []        

    def copy (self) :
        cm = ROIDefIDManger ()
        cm._idNumber = self._idNumber
        cm._recycleList = copy.copy (self._recycleList)
        cm._allocatedIDList = copy.copy (self._allocatedIDList)
        return cm
    
    def getNewName (self, newName, requiredType, requiredRadLexID) :        
        if (not self.hasROI (newName)) : 
            return newName
        NameCounter = 2
        while True :
            testName = newName + "(%d)" % NameCounter
            if (not self.hasROI (testName)) : 
                return testName            
            elif (self.getROIType (testName) == requiredType and self.getROINameRadlexID (testName) == requiredRadLexID) :
                return testName                        
            NameCounter += 1                        
        
    
    def getNewID (self) :
        if (len (self._recycleList) > 0) :        
            newID = self._recycleList.pop ()
        else :
            self._idNumber += 1
            newID = self._idNumber
        self._allocatedIDList.append (newID)
        return newID
           
    def deleteID (self, contourID) :        
        self._recycleList.append (contourID)
        self._allocatedIDList.remove (contourID)    """
                        
"""
  class ROIDefinitions
  
  Defines and manages a collection of ROIDef definitions.
  
  Key variables:
          _ROI_Dictionary : python dictionary keyed by ROI Name used to store ROIDef objects
          _changeListener : contains a list of call backs to be notified by changes occure to the ROIDefs
          _selectionListener : called when selected ROI changes          
"""
class ROIDefinitions :
    def __init__ (self, projectFileInterface) :
        self._filename = None
        self._ROI_Dictionary = {} 
        self._changeListener = []
        self._selectionListener = []               
        self._selectedROIDict = {}        
        self._singleROIPerVoxel = True
        self._ROIIDOrder = []
        self.clearSelectedROI ()       
        self._projectFileChangeMonitor = FileChangeMonitor ()                 
        self._ProjectFileInterface = projectFileInterface         
        
    """def setTopDrawROI (self, obj) :
        if obj is None :
            self._drawROIOnTopROIID = None
        else:
            self._drawROIOnTopROIID = obj.getROIDefIDNumber ()"""
    
    def setProjectFilePath (self, path) :
        self._filename = path
        
    def areAllROISelected (self) :
        for roiDef in self._ROI_Dictionary.values () :
            if roiDef.getIDNumber () not in self._selectedROIDict :
                return False
        return True
            
    def getProjectFileInterface (self) :
        return self._ProjectFileInterface  
        
    def reloadROIDefsIfFileChanged (self, PreserveSelection = True) :
        if (self._projectFileChangeMonitor.hasFileChanged (self._filename) == True):
            self.reloadROIDefs (PreserveSelection = PreserveSelection)   
            return True
        return False
            
    def arePointROIDefined (self) :
       for roi in self._ROI_Dictionary.values () :
           if roi.getType () == "Point" :
               return True
       return False
       
    def getUniqueName (self, basename, MatchingROI = None) :
        roiNameSet = set ()
        for roi in self._ROI_Dictionary.values () :
            roiNameSet.add (roi.getName ())
        if (basename not in roiNameSet) :
            return basename
        if (MatchingROI is not None) :
            matchingROIradLexID = MatchingROI.getRadlexID ()
            matchingROIroiType = MatchingROI.getType ()
            matchingName = self._findBestMatchingROIName (basename, matchingROIradLexID, matchingROIroiType)
            if (matchingName != "None"):
                return matchingName
            
        counter = 2 
        basename += "_"
        testName = basename + str (counter)
        while testName in roiNameSet :
            if (MatchingROI is not None) :                
                matchingName = self._findBestMatchingROIName (testName, matchingROIradLexID, matchingROIroiType)
                if (matchingName != "None"):
                    return matchingName
            counter += 1
            testName = basename + str (counter)
        return testName
     
    def _findBestMatchingROIName (self, name, radlex, roiType) :        
        radLexNameMatch = "None"
        for roiValues in  self._ROI_Dictionary.values () :
            testType = roiValues.getType ()
            if (testType == roiType):
                 testName = roiValues.getName ()
                 testRadLexID = roiValues.getRadlexID ()
                 if (testName == name and radlex == testRadLexID) :
                     return testName #perfect_match
                 if (radlex == "unknown" and name == testName) :
                     return testName
                 if (radlex != "unknown" and radlex == testRadLexID) :
                     radLexNameMatch = testName
        if (radLexNameMatch != "None") :
            return radLexNameMatch        

        FoundROIUID = self.tryToGetUIDKeyForOrigionalName (name)
        if (FoundROIUID is not None):
            testName = self.getROINameFromUID (FoundROIUID)
            testType = self.getROIType (testName)
            if (testType == roiType):
                return testName
        return "None"
    
    def findBestMatchingROIName (self, roiDef) :
        name = roiDef.getName ()
        radlex = roiDef.getRadlexID ()
        roiType = roiDef.getType ()
        ROIDefUIDObj = roiDef.getDefinedUID ()
        
        if (ROIDefUIDObj is not None) :
            projectUID = self._ProjectFileInterface.getProjectUID ()
            if (projectUID is not None) :
                 if ROIDefUIDObj.hasUIDDefined () :
                     ROIUID = ROIDefUIDObj.getUIDfromUID (projectUID)
                     if (ROIUID is not None) :
                         testName = self.getROINameFromUID (ROIUID)
                         if testName is not None :
                             return testName
        return self._findBestMatchingROIName (name, radlex, roiType)
        
        
        
    @staticmethod
    def _copy (source, dest, CopyFilename) :
        selectionChanged = False
        roiDictionaryChanged = False
        if (CopyFilename) :
            if (dest._filename != source._filename) :
               dest._filename = source._filename   
               roiDictionaryChanged = True
        if (dest._ProjectFileInterface != source._ProjectFileInterface) :
            dest._ProjectFileInterface = source._ProjectFileInterface
            roiDictionaryChanged = True
        dest._projectFileChangeMonitor = source._projectFileChangeMonitor.copy ()
        if dest._singleROIPerVoxel != source._singleROIPerVoxel :
            dest._singleROIPerVoxel = source._singleROIPerVoxel    
            roiDictionaryChanged = True
        
        ROIOrderChanged = False 
        if len(dest._ROIIDOrder) != len (source._ROIIDOrder):
            ROIOrderChanged = True
        else:
            for destIndex ,item in enumerate (dest._ROIIDOrder) :
                if item not in source._ROIIDOrder :
                    ROIOrderChanged = True
                    break
                if destIndex != source._ROIIDOrder.index (item) :
                    ROIOrderChanged = True
                    break
        if (ROIOrderChanged) :
            roiDictionaryChanged = True
            dest._ROIIDOrder = copy.copy (source._ROIIDOrder)  
        
        oldSelectionDictionary = dest._selectedROIDict
        dest._selectedROIDict = {}
        for key in oldSelectionDictionary.keys () :
            if key not in source._selectedROIDict :
                selectionChanged = True
        for key, value in source._selectedROIDict.items () :
            if (key not in oldSelectionDictionary) :
                selectionChanged = True
            elif (oldSelectionDictionary[key] != value) :
                selectionChanged = True
            dest._selectedROIDict[key] = value
        del oldSelectionDictionary
        
        oldROIDictionary = dest._ROI_Dictionary
        dest._ROI_Dictionary = {}
        for key in oldROIDictionary.keys () :
            if key not in source._ROI_Dictionary :
                roiDictionaryChanged = True
        for key, value in source._ROI_Dictionary.items () :
            if key not in oldROIDictionary :
                roiDictionaryChanged = True
            elif not oldROIDictionary[key].equals (value) :
                roiDictionaryChanged = True
                
            dest._ROI_Dictionary[key] = value.copy ()
        del oldROIDictionary
        return selectionChanged, roiDictionaryChanged
    
    def copy (self, CopyFilename = False) :
        newDefs = ROIDefinitions (None)
        selectionChanged, roiDictionaryChanged = ROIDefinitions._copy (self, newDefs, CopyFilename = CopyFilename)
        return newDefs
    
    def setDefinitions (self, newDefs, CallChangeListener = True, CallDictionaryChangeListener = True) :
        selectionChanged, roiDictionaryChanged = ROIDefinitions._copy (newDefs, self, CopyFilename = True)    
        if (selectionChanged and CallChangeListener) :
            self._callSelectionChangedListners ()
        if (roiDictionaryChanged and CallDictionaryChangeListener) :
            self.ROIDefsChanged ()
        
    def delete (self) :
        self._filename = None
        self._ROI_Dictionary = {}  #map by ROI Name -> ROIDef
        self._changeListener = []
        self._selectionListener = []               
        self._selectedROIDict = {}        
        self._ROIIDOrder = []
        
    def hasSingleROIPerVoxel (self) :
        return self._singleROIPerVoxel 
    
    def setSingleROIPerVoxel (self, val) :
        if (val != self._singleROIPerVoxel) :
            self._singleROIPerVoxel = val
            self.ROIDefsChanged ()
            
    def updateROIDefNameAndColor (self, oldname, newname, color):
        idNumber = self.getROINameIDNumber (oldname)
        if idNumber in self._ROI_Dictionary :
            self._ROI_Dictionary[idNumber].setNameAndColor (name = newname, color = color)                
    
    def getROINameRadlexID (self, name, ignoreNotFound = False):
        if (name == "None") :
            return "unknown"            
        for values in self._ROI_Dictionary.values () :   
            if (values.getName () == name) :
                return values.getRadlexID ()
        if not ignoreNotFound :
                print ("Error Name Not Found, " + name)
        return "unknown"
    
    
    
    def getROINameIDNumber (self, name, ignoreNotFound = False):
        if (name == "None") :
            return 0            
        for key, values in self._ROI_Dictionary.items () :   
            if (values.getName () == name) :
                return key
        if not ignoreNotFound :
                print ("Error Name Not Found, " + name)
        return 0
                    
    def getROIIDNumberName (self, idNum):
        if (idNum == 0) :
            return "None"
        if idNum in self._ROI_Dictionary :
            return self._ROI_Dictionary[idNum].getName ()
        print ("Error ID Num Not found, " + str (idNum))
        return "None"
        
                    
    def count (self) :
        return (len (self._ROI_Dictionary))
        
    # method: addSelectionChangedListener  (callback : method)   adds call back to selection listner call back
    def addSelectionChangedListener (self, callback):
        if (callback not in self._selectionListener) :
            self._selectionListener.append (callback)
    
    def removeSelectionChangedListener (self, callback):
        if (callback in self._selectionListener) :
            del self._selectionListener[self._selectionListener.index (callback)]
            
    def reloadROIDefs (self,  PreserveSelection = False) :    
        if (self._filename is not None):
           if (PreserveSelection) :
               SelectedROIUIDList = []
               for roiID in self._selectedROIDict.keys ():
                   if roiID != 0 and roiID in self._ROI_Dictionary :
                       name = self._ROI_Dictionary[roiID].getName ()
                       if name is not None :
                           UID = self._ROI_Dictionary[roiID].getNameUID (name)
                           if UID is not None :
                              SelectedROIUIDList.append (UID)
           self.loadROIDefsFromProjectFile (False) 
           if (PreserveSelection) :
               clearSelection = True
               for uid in SelectedROIUIDList :
                   numid = self.getROIIDFromUID (uid)
                   if numid is not None and numid in self._ROI_Dictionary :
                       name = self._ROI_Dictionary[numid].getName ()
                       color = self._ROI_Dictionary[numid].getQColor ()
                       self.setSelectedROI (name, color, clearSelection)
                       clearSelection = False
               
    def _callSelectionChangedListners (self) :
        for i in self._selectionListener :
            i ()
    
    def _eraseROISelectionDict (self) :
        self._selectedROIDict = {}        
        
    def isOneROISelected (self) :
        return len (self._selectedROIDict) == 1

    def getSelectedROICount (self):
        return len (self._selectedROIDict)
    
    def isROINameSelected (self, name) :
        roiID = self.getROINameIDNumber (name)
        return (roiID in self._selectedROIDict)
            
     # method: setSelectedROI (name : string, roicolor : QTColor)  sets selected ROI
    def setSelectedROI (self, name, roicolor = None, clearSelection = False, CallChangeListener = True) :            
        if (clearSelection) :
            self._selectedROIDict = {}                    
        roiID = self.getROINameIDNumber (name)
        if (roiID not in self._selectedROIDict):
           if (name == "None") :
               self._selectedROIDict = {}        
               roicolor = QtGui.QColor (0,0,0)        
           if (roicolor is not None) :
               if isinstance (roicolor, tuple) :
                   self._selectedROIDict[roiID] =  QtGui.QColor (roicolor[0],roicolor[1],roicolor[2])                   
               else:
                   self._selectedROIDict[roiID] = roicolor               
           else:
               self._selectedROIDict[roiID] = self.getROIColor (name)
                                 
           if (CallChangeListener) :
               self._callSelectionChangedListners ()
         
    # method:  getSelectedROIColor ()  returns selected ROI color    
    def getSelectedROIColor (self) :
        if (len (self._selectedROIDict) > 0) :            
            selectedROI = self.getSelectedROI ()     
            idNumber = self.getROINameIDNumber (selectedROI [0])
            return self._selectedROIDict[idNumber]
        return QtGui.QColor (0,0,0)    
        
    # method:  hasROI (name : string)  returns true if ROI name is defined
    def hasROI (self, name) :    
       idNumber = self.getROINameIDNumber (name, ignoreNotFound = True)
       if (idNumber == 0) :
           return False
       return idNumber in self._ROI_Dictionary
       
    # method: getROIColor (name : string)  returns the color of a defined ROI   
    def getROIColor (self, name) :        
        idNumber = self.getROINameIDNumber (name)
        if (idNumber in self._ROI_Dictionary) :
            color = self._ROI_Dictionary[idNumber].getColor ()
            return QtGui.QColor (color[0], color[1], color[2])
        return QtGui.QColor (0,0,0)            

    def getROIColorRGBTupleFromIDNumber (self, idNumber) :                
        if (idNumber in self._ROI_Dictionary) :
            color = self._ROI_Dictionary[idNumber].getColor ()
            return (color[0], color[1], color[2])
        return (0,0,0)               
    
    def getROIDefUIDObjByName (self, name):
        idNumber = self.getROINameIDNumber (name)
        if (idNumber in self._ROI_Dictionary) :
            return self._ROI_Dictionary[idNumber].getDefinedUID ()
        return None
        
        
    def getROIMaskValue (self, name):
        idNumber = self.getROINameIDNumber (name)
        if (idNumber in self._ROI_Dictionary) :
            return self._ROI_Dictionary[idNumber].getMask ()
        return 1
    
    def setROIMaskValue (self, name, value):
        idNumber = self.getROINameIDNumber (name)
        if (idNumber in self._ROI_Dictionary) :
            self._ROI_Dictionary[idNumber].setMask (value)             
    
    def isROIHidden (self, name):        
        idNumber = self.getROINameIDNumber (name)
        if (idNumber in self._ROI_Dictionary) :
            return self._ROI_Dictionary[idNumber].isHidden ()       
        return False
    
    def setROIHidden (self, name, value, AutoLock = False):
        idNumber = self.getROINameIDNumber (name)
        if (idNumber in self._ROI_Dictionary) :
            self._ROI_Dictionary[idNumber].setHidden (value)
            if AutoLock :             
                self._ROI_Dictionary[idNumber].setLocked (value)
                
    def isROILocked (self, name):        
        idNumber = self.getROINameIDNumber (name)
        if (idNumber in self._ROI_Dictionary) :
            return self._ROI_Dictionary[idNumber].isLocked ()       
        return False
    
    def setROILocked (self, name, value):
        idNumber = self.getROINameIDNumber (name)
        if (idNumber in self._ROI_Dictionary) :
            self._ROI_Dictionary[idNumber].setLocked (value)                
            
    def getROIType (self, name) :        
        idNumber = self.getROINameIDNumber (name)
        if (idNumber in self._ROI_Dictionary) :
            return self._ROI_Dictionary[idNumber].getType ()
        return None            

    def getROIIDNumber (self, name) :        
        idNumber = self.getROINameIDNumber (name)
        return idNumber
        """if (idNumber in self._ROI_Dictionary) :
            return self._ROI_Dictionary[idNumber].getIDNumber ()
        return None            """

    # method: getSelectedROI  returns the selected ROI        
    def getSelectedROI (self, ReturnSet = False):                 
        if (len (self._selectedROIDict) > 0) :
            if ReturnSet :
                returnLst = set ()
                for roiID in self._selectedROIDict.keys () :
                   returnLst.add (self.getROIIDNumberName (roiID))
                return returnLst
            else:
                returnLst = []
                for roiID in self._selectedROIDict.keys () :
                   returnLst.append (self.getROIIDNumberName (roiID))
                return returnLst
        return ["None"]
                    
    # method: isROISelected  returns true if ROI is selected     
    def isROISelected (self):
        if (len (self._selectedROIDict) <= 0) :
            return False        
        return self.getROINameIDNumber ("None") not in self._selectedROIDict 
    
    # method: clearSelectedROI ()  clears selected ROI and sets selected ROI to "None"
    def clearSelectedROI (self, CallSelectionChangeListener = False) :
        self._selectedROIDict = {}
        self._selectedROIDict[0] = QtGui.QColor (0,0,0)  
        if CallSelectionChangeListener :
            self._callSelectionChangedListners () # calling selection change lister here caused problems with loading dataset ui disableing.
    
    def selectAll (self) :
        self._selectedROIDict = {}
        for roiDef in self._ROI_Dictionary.values () :
            idNumber = roiDef.getIDNumber ()            
            self._selectedROIDict[idNumber] = roiDef.getQColor ()
        self._callSelectionChangedListners () # calling selection change lister here caused problems with loading dataset ui disableing.
        
    def toggleSelection (self) :
        keyList = set(list (self._selectedROIDict.keys ()))
        self._selectedROIDict = {}
        for roiDef in self._ROI_Dictionary.values () :
            idNumber = roiDef.getIDNumber ()         
            if idNumber not in keyList :
                self._selectedROIDict[idNumber] = roiDef.getQColor ()
        if len (self._selectedROIDict) == 0 :
             self._selectedROIDict[0] = QtGui.QColor (0,0,0)  
        self._callSelectionChangedListners () # calling selection change lister here caused problems with loading dataset ui disableing.
        
        
        
    def getROINameFromUID (self, ROI_UID) :
        for rDef in self._ROI_Dictionary.values () :
            name  = rDef.getUIDName (ROI_UID)
            if name is not None :
                return name
        return None

    def getROIIDFromUID (self, ROI_UID) :
        for rDef in self._ROI_Dictionary.values () :
            name  = rDef.getUIDName (ROI_UID)
            if name is not None :
                return rDef.getIDNumber ()
        return None
    
    def getROINameUID (self, name) :
        for rDef in self._ROI_Dictionary.values () :
            uid  = rDef.getNameUID (name)
            if uid is not None :
                return uid
        return None
    
    def tryToGetUIDKeyForOrigionalName (self, origionalName) :
        for rDef in self._ROI_Dictionary.values () :
            uid  = rDef.getDefinedUID().tryToGetUIDKeyForOrigionalName (origionalName)
            if uid is not None :
                return uid
        return None
    
    # method: safeToFile (filename (path) : string)  saves ROI definitions to a file
    def saveROIDefsToProjectFile (self):        
        try:
            self._ProjectFileInterface.aquireFileLock ()
            if self._ProjectFileInterface.isProjectReadOnly () :
                return
            filename = self._filename
            if filename is None :
                return
            f = FileInterface ()      
            try:            
                f.readDataFile (filename)        
                header = f.getFileHeader ()         
                if (header.hasFileObject ("ROIDefinitions")) :
                    header.removeFileObject ("ROIDefinitions")            
            except :
                f = FileInterface ()      
                header = f.getFileHeader ()         
            ROIDefFileObj = FileObject ("ROIDefinitions")
            ROIDefFileObj.setParameter ("Version", 2)                
            ROIDefFileObj.setParameter ("ROI_Count", len (self._ROI_Dictionary))                
            count = 1        
            for roi in self._ROI_Dictionary.values () :
                ROIDefFileObj.setParameter ("ROI_Def_" + str (count), roi.getDef ())
                count += 1                        
            ROIDefFileObj.setParameter ("SingleROIPerVoxel", self._singleROIPerVoxel)              
            
            nameOrder = []
            for idNum in self._ROIIDOrder :
                if (idNum in self._ROI_Dictionary) :
                    name = self._ROI_Dictionary[idNum].getName ()
                    nameOrder.append (name)            
            ROIDefFileObj.setParameter ("ROIListOrder", nameOrder)                      
              
            header.addInnerFileObject (ROIDefFileObj)        
            f.writeDataFile (filename)
            self._projectFileChangeMonitor.monitorFile (self._filename)
        finally:
            self._ProjectFileInterface.releaseFileLock ()
            
    # method: isROIArea (name : string)  returns True if name is defined (ROI) and is defined as a "Area" object
    def isROIArea (self, name):
        idNumber = self.getROINameIDNumber (name)
        if idNumber not in self._ROI_Dictionary :
            return False
        return self._ROI_Dictionary[idNumber].getType () == "Area"
        
    # method: isROIPoint (name : string)  returns True if name is defined (ROI) and is defined as a "Point" object
    def isROIPoint (self, name):
        idNumber = self.getROINameIDNumber (name)
        if idNumber not in self._ROI_Dictionary :
            return False
        return self._ROI_Dictionary[idNumber].getType () == "Point"
    
    def getROIDefCopyByUID (self, uid) :        
        idNum = self.getROIIDFromUID (uid)
        if idNum is None :
            return None
        return self._ROI_Dictionary[idNum].copy ()
    
    def getROIDefCopyByName (self, name) :
        idNumber = self.getROINameIDNumber (name)
        if idNumber not in self._ROI_Dictionary :
            return None
        return self._ROI_Dictionary[idNumber].copy ()
                           
    # method: loadFromFile (filename : string) Loads ROI definitions from a file
    def loadROIDefsFromProjectFile (self,  Verbose = True):                
        try:
            print ("Load ROI Defs From Project")
            ROIDefsUpdated = False
            self._ProjectFileInterface.aquireFileLock ()
            filename = self._ProjectFileInterface.getProjectPath ()
            self.clearSelectedROI () 
            self._singleROIPerVoxel = False
            self._ROIIDOrder = []
            self._ROI_Dictionary = {}                     
            f = FileInterface ()      
            f.readDataFile (filename)
            header = f.getFileHeader ()  
            if (header.hasFileObject ("ROIDefinitions")):        
                header = header.getFileObject ("ROIDefinitions")                                
                if (header.hasParameter ("SingleROIPerVoxel")) :
                    self._singleROIPerVoxel = header.getParameter ("SingleROIPerVoxel")                        
                if (header.hasParameter ("ROI_Count")) :
                    count = header.getParameter ("ROI_Count")        
                    for i in range (count) :
                        ROIparameter = "ROI_Def_" + str (i + 1)
                        if (header.hasParameter (ROIparameter)) :
                            roi = header.getParameter (ROIparameter)                    
                            newROI = ROIDef ()                        
                            newROI.setDef (roi)
                            newROIName = newROI.getName ()                            
                            if (newROIName != "None"):
                                if not newROI.hasUIDDefined () :
                                    DefineOrigionalName = True
                                    ROIDefsUpdated = True
                                else:
                                    DefineOrigionalName = False
                                self.appendROIDef (newROI, SkipReloadAndSave = True, DefineOrigionalName = DefineOrigionalName, ProjectFileLocked = True)
                                if (Verbose):
                                    print (newROI.getName())         
                if header.hasParameter ("ROIListOrder") :
                    namelist = header.getParameter ("ROIListOrder")
                else:
                    namelist =  self.getSortedNameLst ()
                if len (namelist) > 0 :
                    currentIDList = copy.copy (self._ROIIDOrder)
                    newIDList = []
                    for name in namelist :
                        if (self.hasROI (name)) :
                            idnum = self.getROIIDNumber (name)
                            newIDList.append (idnum)
                            if (idnum in currentIDList) :
                                del currentIDList[currentIDList.index (idnum)]
                    for idnum in currentIDList :
                        newIDList.append (idnum)
                    self._ROIIDOrder = newIDList                    
            self._filename = filename
            if (ROIDefsUpdated) :           #Format changed resave to capture ROI UUID
                self.saveROIDefsToProjectFile ()
            self._projectFileChangeMonitor.monitorFile (self._filename)
        finally:
            self._ProjectFileInterface.releaseFileLock ()

    def moveROIUp (self, name) :
        idnum = self.getROIIDNumber (name)
        if idnum in self._ROIIDOrder :
            index = self._ROIIDOrder.index (idnum)
            if (index != 0) :
                del self._ROIIDOrder[index]
                self._ROIIDOrder.insert (index - 1, idnum)
                self.ROIDefsChanged ()
                
    def moveROIDown (self, name) :
        idnum = self.getROIIDNumber (name)
        if idnum in self._ROIIDOrder :
            index = self._ROIIDOrder.index (idnum)
            if (index < len (self._ROIIDOrder) -1) :
                del self._ROIIDOrder[index]
                self._ROIIDOrder.insert (index + 1, idnum)
                self.ROIDefsChanged ()
                
    # method: clear ()   Clears ROI Dictionary
    def clear (self):              
         self._ROI_Dictionary = {}         
         self._ROIIDOrder = []
         self.clearSelectedROI ()
           
    # method: appendROIDef (roi)   Adds a ROI to the ROI dictionary
    def appendROIDef (self, roi, FireChangedEvent = False, SkipReloadAndSave = False, DefineOrigionalName = False, ProjectFileLocked = False):
         #print (("Adding ROI ",roi.getIDNumber ()))         
         try:
             if (not ProjectFileLocked) :
                 self._ProjectFileInterface.aquireFileLock ()
             if (not SkipReloadAndSave) :   
                 self.reloadROIDefsIfFileChanged (PreserveSelection = True)  
             num = roi.getIDNumber ()
             if num is None :
                num = ROIUIDManager.generateUID (self._ProjectFileInterface.getProjectName (), self._ProjectFileInterface.getUserName (), roi.getName ())
             if (DefineOrigionalName) :
                 origionalName = roi.getName ()
             else:
                 origionalName = None
             roi.getDefinedUID().defineUIDKey (num, self._ProjectFileInterface.getProjectName (), roi.getName (), origionalName = origionalName)
             roi._setIdNumber (num)
             self._ROI_Dictionary[num] = roi     
             if num not in self._ROIIDOrder :
                 self._ROIIDOrder.append (num)
             if (not SkipReloadAndSave) :   
                 self.saveROIDefsToProjectFile ()
         finally:
            if (not ProjectFileLocked) :
                self._ProjectFileInterface.releaseFileLock ()     
         if (FireChangedEvent):
            self.ROIDefsChanged ()
                  
             
    def getROIIndex (self, name):
        for position, index in enumerate(self._ROIIDOrder) :            
            if (self._ROI_Dictionary[index].getName () == name):
                return position
        return -1
     
        
    def renameROI (self, index, roiNameToRename, roi, PreserveROIUID = False) :
        try:
            self._ProjectFileInterface.aquireFileLock ()
            self.reloadROIDefsIfFileChanged (PreserveSelection = True)  
            selectionChanged = False
            idNumber = self.getROINameIDNumber (roiNameToRename)
            if idNumber != 0 and idNumber in self._ROI_Dictionary :
                del self._ROI_Dictionary[idNumber]
                
                if idNumber in self._ROIIDOrder :
                   index = self._ROIIDOrder.index (idNumber)
                   del self._ROIIDOrder[index]            
            if not PreserveROIUID :
                idNumber = None
                        
            if idNumber is None :
                idNumber = ROIUIDManager.generateUID (self._ProjectFileInterface.getProjectName (), self._ProjectFileInterface.getUserName (), roi.getName ())            
            roi._setIdNumber (idNumber)
            roi.getDefinedUID().defineUIDKey (idNumber, self._ProjectFileInterface.getProjectName (), roi.getName ())
            self._ROI_Dictionary[idNumber] = roi                 
            if idNumber not in self._ROIIDOrder :
                 self._ROIIDOrder.insert (index, idNumber)
            self.saveROIDefsToProjectFile ()            
        finally:
            self._ProjectFileInterface.releaseFileLock ()            
        if (selectionChanged):
            self._callSelectionChangedListners ()
        return idNumber
    
    
    def deleteROI (self, name, PreserveROIUID = False):        
        try:
            self._ProjectFileInterface.aquireFileLock ()
            self.reloadROIDefsIfFileChanged (PreserveSelection = True)  
            selectionChanged = False
            idNumber = self.getROINameIDNumber (name)
            if idNumber != 0 and idNumber in self._ROI_Dictionary :
                del self._ROI_Dictionary[idNumber]
                
                if idNumber in self._ROIIDOrder :
                   index = self._ROIIDOrder.index (idNumber)
                   del self._ROIIDOrder[index]
                self.saveROIDefsToProjectFile ()
        finally:
            self._ProjectFileInterface.releaseFileLock ()            
        if (selectionChanged):
            self._callSelectionChangedListners ()
        return idNumber
            
    # method: addChangeListener (callback)   Adds a callback to the ROI Change Listener
    def addChangeListener (self, listener) :
        self._changeListener.append (listener)
            
    def ROIDefsChanged (self) :
        for i in self._changeListener :
            i ()
   
    def getSortedNameLst (self):
        namelst = []
        for roidef in self._ROI_Dictionary.values () :
            namelst.append (roidef.getName ())
        return sorted (namelst)
        
    def getNameLst (self):
        namelst = []
        #testList = list (self._ROI_Dictionary.keys ())
        for index in self._ROIIDOrder :            
            namelst.append (self._ROI_Dictionary[index].getName ())
            #del testList[testList.index (index)]
        """if (len (testList) > 0) :
            print ("Error index list not syncronized")
            sys.exit ()"""
        return namelst
    
    def _getObjNumberSimpleOrder (self, obj):
        number = obj.getROIDefIDNumber ()
        return self._ROIIDOrder.index (number)
    
    def _getObjNumberROIOnTop (self, obj):
        number = obj.getROIDefIDNumber ()
                
        drawROIOnTopROIID = None
        if len (self._selectedROIDict) == 1 :
            if self.getROINameIDNumber ("None") not in self._selectedROIDict :            
                drawROIOnTopROIID = list(self._selectedROIDict.keys())[0]
                
        if drawROIOnTopROIID == number :
            return 0
        return self._ROIIDOrder.index (number) + 1
    
    def getROIIdNumberOrder (self, idNumber) :
        return self._ROIIDOrder.index (idNumber)
    
    def getObjectLstInROIOrder (self, objectList, UseDrawTopOverride = False) :
        try :  
            if UseDrawTopOverride and self.hasSingleROIPerVoxel () :
                return sorted (objectList, key= self._getObjNumberROIOnTop)
            return sorted (objectList, key= self._getObjNumberSimpleOrder)
        except:
            return []

    def isFirstROISelected (self):
        if (len (self._ROIIDOrder) <= 0) :
            return False        
        firstROIID = self._ROIIDOrder[0]
        return firstROIID in self._selectedROIDict
    
    def isLastROISelected (self):
        if (len (self._ROIIDOrder) <= 0) :
            return False        
        lastROIID = self._ROIIDOrder[len (self._ROIIDOrder)-1]
        return lastROIID in self._selectedROIDict
            

class ROIDictionaryProjectUIDOpenLog :
    def __init__ (self, projectLog = None) :
        if projectLog is None :
            projectLog =  {}        
        self._RILContourProjectFileOpenedLog =  ROIDictionaryProjectUIDOpenLog._copyTree (projectLog)        
    
    def copy (self):
        return ROIDictionaryProjectUIDOpenLog (self._RILContourProjectFileOpenedLog)
        
    def isDocumentNew (self, PrjFileInterface) :
        ProjectDescription = {}
        ProjectDescription["LastOpenedProjectPath"] = PrjFileInterface.getProjectPath ()
        ProjectDescription["LastOpenedByUser"]      = PrjFileInterface.getUserName ()
        ProjectDescription["LastOpenedProjectName"] = PrjFileInterface.getProjectName ()
        ProjectDescription["LastOpenedDate"]        = DateUtil.dateToString (DateUtil.today ())
        ProjectDescription["LastOpenedTime"]        = TimeUtil.timeToString(TimeUtil.getTimeNow ())
        ProjectUID = PrjFileInterface.getProjectUID()
        if ProjectUID not in self._RILContourProjectFileOpenedLog :
            isDocumentNew = True
        else:
            isDocumentNew = False
        self._RILContourProjectFileOpenedLog[ProjectUID] = ProjectDescription        
        return isDocumentNew    
    
    @staticmethod
    def _copyTree (tree):
        rTree = {}
        for key, value in tree.items ():
           innerDescriptionTree = {}
           for iK, iV in value.items () :
               innerDescriptionTree[iK] = iV
           rTree[key] = innerDescriptionTree
        return rTree
        
    def getFileParamater (self) :        
        return ROIDictionaryProjectUIDOpenLog._copyTree (self._RILContourProjectFileOpenedLog)
        
    
        
"""
  class ROIDictionary      
    
  Key variables:
      self._ROIDefs : reference to application defined ROIDefinitions class
      self._ROI_Dictionary : Core container dictionary which holds ROIobjects defined in the current dictionary
                             objects are keyed by ROI name.
      self._changeListener : Python list, holds callbacks called when Dictionary is changed
     
      self._oldSelectedObj : internal variable holds the currently selected object.
      
      Refactor, self._oldSelectedObj hold last selected object, slightly different than the value held by self._ROIDefs.getSelectedROI() 
      self._ROIDefs.getSelectedROI ()  holds name of last selected object name.  self._oldSelectedObj holds last selected object when object has been defined.
      Names can exist in self._ROIDefs.getSelectedROI() but not in self._oldSelectedObj when a object has been selected but no contours / points have been added      
      
  Internal Variables used to support undo Redo:
      self._redoList : Python list of ROIDictionaries (Redo)
      self._undoList : Python list of ROIDictionaries (Undo)
      
      Undo/Redo mechanism works by "Saving" copies of the the current ROIDictionary state to the undo/redo list and restoring saved state to from the 
      requisit list.  Core undo/redo mechanism is based on defining ".copy" method for ROI objects stored in _ROI_Dictionary
      
      self._filename  : filename used to save and load ROIDictionary
      self._scanPhase : variable holds current scanphase text (bit of a archectural hack to put this here, but it works)      
"""                    
def MP_generateSolidObjectContourEdge (Tupe) :                        
            objectContoursToReturn  = []
        
            MaskedVolume, featureindex, BaseName, structure, XOffset, YOffset, ZOffset, sliceShape  = Tupe                
            print ("Generating Solid Contour Edge: " + BaseName)
            if (featureindex != 0) :
                MaskedVolume = MaskedVolume == featureindex                
                
            #print (labeledObjects.shape)            
            #print (MaskedVolume.shape)
            x, y, z = MaskedVolume.nonzero ()            
            firstX = np.min(x)
            firstY = np.min(y)
            firstZ = np.min(z)            
            MaskedVolume = MaskedVolume[ firstX : np.max(x) + 1, firstY : np.max(y) + 1, firstZ : np.max(z) + 1]           

            filledMaskVolume = np.copy (MaskedVolume)                           
            TwoDimStructure = structure[1,:,:]
            nonBranchingSliceRanges = []            
            previousSliceCount = -1
            for siceNumber in range (filledMaskVolume.shape[2]) :
                sliceData = MaskedVolume[:,:, siceNumber]
                sliceData = ndimage.binary_fill_holes (sliceData).astype (np.uint8)                     
                filledMaskVolume[:,:, siceNumber] = sliceData                
                objects, count = ndimage.measurements.label (sliceData, TwoDimStructure) 
                if (count != previousSliceCount) :
                    nonBranchingSliceRanges.append ([siceNumber, siceNumber, count])
                    previousSliceCount = count
                else:
                    lastobjectIndex = len (nonBranchingSliceRanges) - 1
                    nonBranchingSliceRanges[lastobjectIndex][1] = siceNumber
            
            sliceContouringMemory = np.zeros ((filledMaskVolume.shape[0] + 2,filledMaskVolume.shape[1] + 2), dtype = filledMaskVolume.dtype)
            for sliceRangeIndex, sliceRange in enumerate (nonBranchingSliceRanges) :
                sliceRangeName = BaseName
                if (len (nonBranchingSliceRanges) > 1) :
                    sliceRangeName += "Pt" + str (sliceRangeIndex)                                
                                
                firstSlice, lastSlice, objCount = sliceRange
                subVolume = filledMaskVolume[:,:,firstSlice:lastSlice + 1]                
                subVolume, SubVolumePartCount = ndimage.measurements.label (subVolume, structure) 
                for subvolumeindex in range (1, SubVolumePartCount + 1) :
                    ContourName = sliceRangeName
                    if (SubVolumePartCount > 1) :
                        ContourName += "_" + str (subvolumeindex)
                                                            
                    labeledSubVolume = subVolume == subvolumeindex
                    for ZSlice in range (labeledSubVolume.shape[2]) :
                        labeledSlice = labeledSubVolume[:,:,ZSlice]                                                
                        # Create, object contours using for Contur Named: ContourName using contours in NonBranchingSliceRangeObjContours using Contours specfied in edgeType                                                
                        contoursFound = ROIDictionary.MP_createSliceContour (sliceShape, firstSlice + ZSlice, ContourName, labeledSlice, sliceContouringMemory)                        
                        if (contoursFound is not None) :
                            #addjust contour offsests, to global offset, subtract 1 to account for dialated contour mask used in call to find contours
                            ROIDictionary.MP_addOffsetToContours (contoursFound, XOffset + firstX-1, YOffset + firstY-1,  ZOffset + firstZ)
                            objectContoursToReturn += contoursFound
                        
            #insideEdge = np.copy (MaskedVolume)
            insideEdge = MaskedVolume
            insideEdge[filledMaskVolume == 0] = 1
            insideEdge = (insideEdge == 0).astype (np.uint8)
            if np.any (insideEdge) :
                subVolume, SubVolumePartCount = ndimage.measurements.label (insideEdge, structure) 
                for insideobjindex in range (1, SubVolumePartCount + 1) :
                    insideobj = (subVolume  == insideobjindex).astype (np.uint8)
                    if (SubVolumePartCount > 1) :
                        strIndex = str (insideobjindex)
                    else:
                        strIndex = ""
                    prams = (insideobj, 0, BaseName + "_inside" + strIndex, structure, XOffset + firstX , YOffset + firstY,  ZOffset + firstZ, sliceShape)                    
                    
                    objectContoursToReturn += ROIDictionary.MP_generateSolidObjectContourEdge (prams)                    
            
            return objectContoursToReturn

class SharedVolumeCache :
    def __init__ (self, Parent) :                
    
        self._ObjectIDToCacheIDMap = {}
        self._VolumeMem = None
        self._IdNum = 0        
        self._Parent = Parent
        self._ColorTable = None
        self._clipMaskCache = {}                    
        #self._ObjectsIDWithMissingData = set ()
    
    """def setObjectsWithMissingData (self, objID) :
        self._ObjectsIDWithMissingData.add (objID)"""
        
    
    """def removeObjectsWithMissingData (self, objID) :
        if objID in self._ObjectsIDWithMissingData :
            self._ObjectsIDWithMissingData.remove (objID)"""
            
        
    """def initalizeVolumeCacheSlicesWithMissingData (self) :
        if self._Parent is not None and len (self._ObjectsIDWithMissingData) > 0 :
            ObjIDList = list (self._ObjectsIDWithMissingData)
            for objID in ObjIDList  :
                if objID in self._Parent._ROI_Dictionary :
                    self._Parent._ROI_Dictionary[objID].initalizeVolumeCacheSlicesWithMissingData ()
            self._ObjectsIDWithMissingData = set ()"""
        
        
    def changeROIOID (self, oldID, newID) :
        if oldID in self._ObjectIDToCacheIDMap and newID not in self._ObjectIDToCacheIDMap :
            self._ObjectIDToCacheIDMap[newID] = self._ObjectIDToCacheIDMap[oldID]
            del self._ObjectIDToCacheIDMap[oldID]
            """if oldID in self._ObjectsIDWithMissingData :
                self._ObjectsIDWithMissingData.remove (oldID)
                self._ObjectsIDWithMissingData.add (newID)"""
        
    def _getObjectMemID (self, roiDefUID) :      
        if (roiDefUID is None) :
            return None
        if roiDefUID in self._ObjectIDToCacheIDMap :
            return self._ObjectIDToCacheIDMap[roiDefUID]            
        else:            
            self._IdNum += 1
            num = int (self._IdNum)
            self._ObjectIDToCacheIDMap[roiDefUID] = num

            if self._Parent._ROIDefs is None :
                self._ColorTable = None
            else:
                ColorTable = np.zeros ((4,len (self._ObjectIDToCacheIDMap) + 1),dtype =np.int)
                for objectID, cacheID in self._ObjectIDToCacheIDMap.items ():                
                    red, green, blue = self._Parent._ROIDefs.getROIColorRGBTupleFromIDNumber (objectID)
                    ColorTable[0,cacheID] = blue
                    ColorTable[1,cacheID] = green
                    ColorTable[2,cacheID] = red
                    ColorTable[3,cacheID] = 255                
                ColorTable = ColorTable.T
                self._ColorTable = ColorTable            
            return num            
    
    def initalizeVolume (self, objID, shape) :
        if self._VolumeMem is None or tuple (self._VolumeMem.shape) != tuple (shape) :            
            self._ObjectIDToCacheIDMap = {}        
            self._IdNum = 0
            self._VolumeMem = np.zeros (shape, dtype = np.int)    
            #self._ObjectsIDWithMissingData = set ()                            
        elif (objID is not None):
            self.zeroVolume (objID)  
            #self.setObjectsWithMissingData (objID)
        
    def clearVolume (self) :        
        self._ObjectIDToCacheIDMap = {}
        self._VolumeMem = None
        self._IdNum = 0
        #self._ObjectsIDWithMissingData = set ()   
        
    def isInitalized (self) :        
        return self._VolumeMem is not None 
    
    def zeroVolume (self, objID) :        
        if (self._VolumeMem is  not None) :            
            if objID not in self._ObjectIDToCacheIDMap :
                return
            memID = self._getObjectMemID (objID)
            if memID is None :
                 return None
            self._VolumeMem[self._VolumeMem == memID] = 0    
            #self.setObjectsWithMissingData (objID)                
        
    def zeroSlice (self, objID, height) :        
        if (self._VolumeMem is  not None) : 
            if objID not in self._ObjectIDToCacheIDMap :
                return
            memID = self._getObjectMemID (objID)
            #self.setObjectsWithMissingData (objID)   
            if memID is None :
                return None
            mem = self._VolumeMem[:,:, height]
            sliceData = self._VolumeMem[:,:, height]
            sliceData [mem == memID] = 0        
            self._VolumeMem[:,:, height] = sliceData
        
    def setSlice (self, objID, height, data) :    
        if (self._VolumeMem is  not None) :
            memID = self._getObjectMemID (objID)
            if memID is None :
                 return None
            sliceData = self._VolumeMem[:,:, height]
            sliceData[sliceData == memID] = 0
            sliceData[data > 0] = memID
            self._VolumeMem[:,:, height] = sliceData                        
    
    def initializeSlice (self, objID, height, data) :    
        if (self._VolumeMem is  not None) :
            memID = self._getObjectMemID (objID)
            if memID is None :
                 return None
            sliceData = self._VolumeMem[:,:, height]            
            sliceData[data > 0] = memID
            self._VolumeMem[:,:, height] = sliceData                        
            
    def getSlice (self, objID, height) :
        if (self._VolumeMem is  not None) :            
            memID = self._getObjectMemID (objID)
            if memID is None :
                 return None            
            return (self._VolumeMem[:,:, height] == memID).astype (np.uint8)
            
        return None
    
    def getROIVolume (self, objID):
        if (self._VolumeMem is  not None) :            
            memID = self._getObjectMemID (objID)
            if memID is None :
                 return None            
            return (self._VolumeMem == memID).astype (np.uint8)            
        return None
        
    def getROISubVolume (self, objID, FirstSlice, LastSlice):
        if (self._VolumeMem is  not None) :            
            memID = self._getObjectMemID (objID)
            if memID is None :
                 return None            
            return (self._VolumeMem[:,:,FirstSlice: LastSlice + 1] == memID).astype (np.uint8)            
        return None
    
    def getObjectIDIndexAtPoint (self, xIndex, yIndex, height) :
        if (self._VolumeMem is  not None) :  
            #self.initalizeVolumeCacheSlicesWithMissingData ()
            try :
                idNum = self._VolumeMem[xIndex,yIndex, height]
                if idNum == 0 :
                    return None
                for key, value in self._ObjectIDToCacheIDMap.items ():
                    if value == idNum :
                        return key                
                return None
            except :
                return None            
        return None
    
    #@jit (cache=True)
    def getRawROIMaskVolume (self) :
        if self._ColorTable is None :
            return None, None, None
        return self._VolumeMem, self._ColorTable, self._ObjectIDToCacheIDMap
     
    def getRawROIinAxis (self,index, axis, ROIDefs, SelectedObjects) :              
        if self._ColorTable is None :
            return None, None, None
        if axis == "X" :
            if index < 0 or index >= self._VolumeMem.shape[0] :
                return np.zeros ((self._VolumeMem.shape[1], self._VolumeMem.shape[2]),dtype = np.int), self._ColorTable, self._ObjectIDToCacheIDMap
            sliceColorMap = self._VolumeMem[index,:, :]
        elif axis == "Y" :
            if index < 0 or index >= self._VolumeMem.shape[1] :
                return np.zeros ((self._VolumeMem.shape[0], self._VolumeMem.shape[2]),dtype = np.int), self._ColorTable, self._ObjectIDToCacheIDMap
            sliceColorMap = self._VolumeMem[:,index, :]
        else:
            if index < 0 or index >= self._VolumeMem.shape[2] :
                return np.zeros ((self._VolumeMem.shape[0], self._VolumeMem.shape[1]),dtype = np.int), self._ColorTable, self._ObjectIDToCacheIDMap

            sliceColorMap = self._VolumeMem[:,:, index]            
        
        if not np.any (sliceColorMap) :   
            return sliceColorMap, self._ColorTable, self._ObjectIDToCacheIDMap
        
        if axis not in self._clipMaskCache :
            self._clipMaskCache[axis] = {}        
        axisCache = self._clipMaskCache[axis]
        
        memShape = (sliceColorMap.shape[0], sliceColorMap.shape[1], len (self._ObjectIDToCacheIDMap) + 1)
        if "shape" not in axisCache or axisCache["shape"] != sliceColorMap.shape or axisCache["mem"].shape != memShape :
            axisCache["shape"] = sliceColorMap.shape            
            axisCache["mem"] = np.zeros (memShape, dtype=np.int)                        
            axisCache["cacheIDState"] = {}
        
        #Cache the objects clip state, only regenerate clipstate if object changes
        sliceColorMap = np.copy (sliceColorMap)
       
        clipMask = axisCache["mem"]
        cacheIDState = axisCache["cacheIDState"]
        for objectID, cacheID in self._ObjectIDToCacheIDMap.items ():                                               
            if objectID not in self._Parent._ROI_Dictionary :
                Hidden = True
                Shape = (None, None,None, None)                
            else:                
                obj = self._Parent._ROI_Dictionary[objectID]
                if obj in SelectedObjects or ROIDefs.isROIHidden (obj.getName (ROIDefs)) :
                    Hidden = True
                    Shape = (None, None,None, None)     
                else:
                    Hidden = False   
                    if axis == "Z" :
                        top, bottom = obj.getZBoundingBoxCoordinate ()
                        if index < top or index > bottom :
                            Hidden = True
                            Shape = (None, None,None, None)     
                        
                    if not Hidden :
                        xrange,yrange = obj._getAxisBoundingBox (sliceAxis = axis)   
                        Shape = (xrange[0], xrange[1], yrange[0], yrange[1])                                    
            testTuple = (Hidden, Shape)
            
            if cacheID not in cacheIDState or cacheIDState[cacheID] != testTuple :
                cacheIDState[cacheID] = testTuple           
                clipMask[:,:, cacheID] = -cacheID
                if not Hidden : 
                    clipMask[Shape[0]:Shape[1], Shape[2]:Shape[3], cacheID] = 0                            
    
        index = sliceColorMap != 0
        idVals = sliceColorMap[index]
        msks = clipMask[index]
        sliceColorMap[index] += msks[np.arange (idVals.shape[0]),idVals] #ndimage.map_coordinates(msks, [np.arange (idVals.shape[0]),idVals], order=0, mode='constant', cval=0.0, prefilter=False)
        
        #msks[np.arange (idVals.shape[0]),idVals]
        #xIndex, yIndex = sliceColorMap.nonzero ()
        #sliceColorMap[xIndex,yIndex] += clipMask[xIndex,yIndex, sliceColorMap[xIndex,yIndex]]        
        return sliceColorMap, self._ColorTable, self._ObjectIDToCacheIDMap
    
    
    def getROIinYAxis (self, yIndex) :
        FoundKeys = []
        if (self._VolumeMem is  not None) :
           #self.initalizeVolumeCacheSlicesWithMissingData ()           
           mem = FastUnique.unique (self._VolumeMem[:,yIndex, :], MaxInt = self._IdNum)
           for key, value in  self._ObjectIDToCacheIDMap.items ():
               if value in mem :
                  FoundKeys.append (key)
        return FoundKeys
    
    def getROIinXAxis (self, XIndex) :
        FoundKeys = []
        if (self._VolumeMem is  not None) :
           #self.initalizeVolumeCacheSlicesWithMissingData ()           
           mem = FastUnique.unique (self._VolumeMem[XIndex,:, :], MaxInt = self._IdNum)
           for key, value in  self._ObjectIDToCacheIDMap.items ():
               if value in mem :
                  FoundKeys.append (key)
        return FoundKeys
    
    def getROIinZAxis (self, height) :
        FoundKeys = []        
        if (self._VolumeMem is  not None) :
           #self.initalizeVolumeCacheSlicesWithMissingData ()           
           mem = FastUnique.unique (self._VolumeMem[:,:, height], MaxInt = self._IdNum)
           for key, value in  self._ObjectIDToCacheIDMap.items ():
               if value in mem :
                  FoundKeys.append (key)
        return FoundKeys
    
    def getYAxisSlice (self, objID, yIndex) :
        if (self._VolumeMem is  not None) :
           memID = self._getObjectMemID (objID)                 
           if memID is None :
                return None           
           return (self._VolumeMem[:,yIndex, :] == memID).astype (np.uint8)           
        return None
        
    def getXAxisSlice (self, objID, xIndex) :
        if (self._VolumeMem is  not None) : 
           memID = self._getObjectMemID (objID)       
           if memID is None :
                return None           
           return (self._VolumeMem[xIndex,:, :] == memID).astype (np.uint8)                      
        return None
                
        
class ROIDictionary :
    def __init__ (self, ROIDefs, Hounsfieldsettings, VisSettings_ = None, LowMemory = False) :    
        self._readOnlyMode = "False"
        self._AxisProjection = None
        self._LowMemory = LowMemory
        self._resetReadOnlyMode   ()       
        self._FileChangePreviouslyDetected = False
        self._ProjectFileLockInterface  = None       
        self._ActivityTimer = ActivityTimer ()                
        self._currentUISlice = None
        self._excludedFileObjectList = []
        self._DocumentUID = None
        self._FileChangeTimeLog = TimeLog ()
        self._objClipMaskVolumeCache = {}
        self.clearClipMaskVolumeCache ()
        self._undoRedoStackSize = 5
        self._DisableFileSave = False
        self._cachedNiftiShape = None
        self._ROIMode = "Area"
        self._KeepFileHandleOpen = False
        self._setHeaderChangeLog (())
        self._setHeaderCache ("")
        self._ProjectDataSet = None
        self._file_modified = False
        self._ROIDefs = ROIDefs        
        try :
            self._Debug_RILContourVersion = pkg_resources.get_distribution("rilcontour").version
        except :
            self._Debug_RILContourVersion = "Could not detect Ril-Contour version."    
        try:
            self._Debug_PythonVersion = sys.version
        except:
            self._Debug_PythonVersion = "Could not detect Python version."
      
        #if nothing was passed in just create temporary objects to hold loaded parameters
        if (Hounsfieldsettings == None) : 
            Hounsfieldsettings = HounsfieldVisSettings ()
        if (VisSettings_ == None) :
            VisSettings_ = VisSettings ()
            
        self._Hounsfieldsettings = Hounsfieldsettings        
        self._Hounsfieldsettings.addChangeListener (self._hounsfieldchanged)
        self._VisSettings = VisSettings_        
        self._VisSettings.addListener (self._vischanged)                
            
        self._ROI_Dictionary = {}
        self._changeListener = []
        self._undoRedoChangeListener = []
        self._readOnlyChangeListener = []
        self._oldSelectedObj = None
        self._niftiDataSourceDescription = None
        
        self._redoList = []
        self._undoList = []        
        self._filHandle = None
        self._scanPhase = None
        self._NIfTIOrientation = None
        self._ANTSRegistrationLog = ANTSRegistrationLog (None)        
        
        self._DataFileTagManager = DatasetTagManager ()
        self._ROIDictionaryProjectUIDOpenedLog = ROIDictionaryProjectUIDOpenLog ()            
        # start at present time series features are not implemented.
        self._UnloadedTimeSeriesDatasets = {}
        self._TimeDim  = 0
        self._timeDimensions = np.ones ((1),dtype = np.uint8)
        self._sharedVolumeCache = None
        # end at present time series features are not implemented.
    
    def setAxisProjection (self, projection) :
        self._AxisProjection = projection
        
    def isRunningInLowMemoryMode (self) :
        return self._LowMemory
    
    def _isSharedVolumeCacheEnabled (self) :
        if self._ROIDefs is not None and self._ROIDefs.hasSingleROIPerVoxel() and not self.isRunningInLowMemoryMode () :
            return True
        else:
            self._sharedVolumeCache = None
            return False

                
    def getSharedVolumeCacheMem (self):
        if (not self._isSharedVolumeCacheEnabled ()) :
            self._sharedVolumeCache = None
        elif (self._sharedVolumeCache is None) :
            self._sharedVolumeCache  = SharedVolumeCache (self)
        return self._sharedVolumeCache        
        
    def getDocumentUID (self) :
        return self._DocumentUID
        
    def getFileChangeTimeLog (self) : 
        return self._FileChangeTimeLog
    
    def getDataFileTagManager (self) :
        return self._DataFileTagManager
    
    # start at present time series features are not implemented.    
    def getDefinedTimeSeries (self):
        dim = self._timeDimensions.nonzero ()[0]
        return dim.tolist ()
        
    def getTimeSeriesCount (self) :
        return len (self._timeDimensions)
    
    def getDatasetTimeDimension (self):
        return self._TimeDim                
    """
    Currently only 3-Dimensional datasets are supported, time dimension = 0
    to support 4-D datasets, ROI objects will need to be saved with references to a time dimension and the names of the ROI objects will need to be 
    manged to support multiple objects with the same name but at different time dimensions
    finally UI support will need to be added to support specifiying a Time dimension and and exporting dataset masks with a time dimension & csv files in reference to the time 
    specified time dimension.  Given that 4-D datasets are relatively uncommon this is a low priority
    """
    # end at present time series features are not implemented.
    
       
    def getSelectedROIObjectLst (self) :
        objLst = []
        try :
            roiNameLst = self._ROIDefs.getSelectedROI() 
            for name in roiNameLst :
                if (name != "None" and self.isROIDefined (name)) :            
                    obj = self.getROIObject (name)
                    if (obj != None) :
                        objLst.append (obj)
            return objLst
        except:
            return []
        
    def getNiftiDataSourceDescription (self) :
        return self._niftiDataSourceDescription
    
    def getLoadedROIFilename (self) :
        if (self._filHandle == None) :
            return None
        return self._filHandle.getPath ()
    
  
    
    #def setXYZAxisOrientation (self, orientation):
    def setNIfTIVolumeOrientationDescription (self, orientation, ProgDialog = None, App = None):
        self._clearObjInProjectionCache ()
        self.clearClipMaskVolumeCache ()
        self._NIfTIOrientation = orientation
        volumeDimensions = self.getNIfTIDim ()
        if (volumeDimensions is not None) :
            if (ProgDialog is not None) :
                ProgDialog.setRange (0,len (self._ROI_Dictionary))
                self.getSharedVolumeCacheMem ()
            memcache = self.getSharedVolumeCacheMem ()
            if memcache is not None :
                memcache.initalizeVolume (None, volumeDimensions) 
                        
            for index, obj in enumerate(self._getDictionaryObjectsInReverseDrawOrder ()) :
                if (ProgDialog is not None) :
                    if (self._ROIDefs is not None) :
                        ProgDialog.setLabelText ("Initializing " +obj.getName (self._ROIDefs) + " volume")
                    else:
                         ProgDialog.setLabelText ("Initializing ROI volume")
                    ProgDialog.setValue (index)
                if obj.isROIArea () :
                    obj.setVolumeShape (volumeDimensions, self.getSharedVolumeCacheMem (), QApp = App, ClipSliceMask = False)                                        
            if (ProgDialog is not None) :
                ProgDialog.setRange (0, 0)
                    
    def getNIfTIDim (self) :
        if (self._NIfTIOrientation is not None and self._NIfTIOrientation.hasParameter ("VolumeDimension")) :    
            dimensions = self._NIfTIOrientation.getParameter ("VolumeDimension")
            try :
                shape = tuple (dimensions.tolist ())
            except:
                try :
                    shape = tuple (dimensions)
                except :
                    return None
            try:
                if len (shape) == 3 :
                    return shape
            except:
                return None
        return None 
    
    def getNIfTIVolumeOrientationDescription (self):
        return self._NIfTIOrientation
        
    def delete (self, KeepFileHandleOpen = False):                         
         if (not KeepFileHandleOpen) :  
             self._tryCloseOldFileHandle (None) 
             self._filHandle = None         
         else:
             self._tryCloseOldFileHandle (self._filHandle) 
         self._AxisProjection = None
         self._LowMemory = False
         self._sharedVolumeCache = None         
         self._currentUISlice = None
         self.clearClipMaskVolumeCache ()
         self._DocumentUID = None
         self._cachedNiftiShape = None
         self._KeepFileHandleOpen = KeepFileHandleOpen
         self._UnloadedTimeSeriesDatasets = {}
         self._TimeDim  = 0
         self._timeDimensions = np.zeros ((1),dtype = np.uint8)         
         self._ProjectDataSet = None         
         if (self._Hounsfieldsettings != None) :
            self._Hounsfieldsettings.removeChangeListener (self._hounsfieldchanged)
            self._Hounsfieldsettings = None
         if (self._VisSettings != None) :
            self._VisSettings.removeListener (self._vischanged)
            self._VisSettings = None
         self._ROI_Dictionary = {}
         self._excludedFileObjectList = []
         self._changeListener = []
         self._undoRedoChangeListener = []
         self._readOnlyChangeListener = []
         self.setSelected (None)         
         self._niftiDataSourceDescription = None        
         self._redoList = []
         self._undoList = []         
         self._scanPhase = None
         self._ROIDefs = None
         self._NIfTIOrientation = None
         self._ANTSRegistrationLog = None
         self._setHeaderChangeLog (())
         self._setHeaderCache ("")
         self._DataFileTagManager = None
         self._ROIDictionaryProjectUIDOpenedLog = ROIDictionaryProjectUIDOpenLog ()  
         self._ProjectFileLockInterface  = None   
         self._FileChangePreviouslyDetected = False     
         self._resetReadOnlyMode   ()       
         gc.collect ()
            
            
    def getHounsfieldSettings (self) :
        return self._Hounsfieldsettings
        
    def getVisSettings (self) :
        return self._VisSettings
        
    # used to update ROI defs when ROI defs change
    def setROIDefs (self, newdefs, CallChangeListener = True, ClearUndoRedo = True) :
        """if (ClearUndoRedo) :
            self._redoList = []
            self._undoList = []"""
        self._clearObjInProjectionCache ()
        self.clearClipMaskVolumeCache ()
        self._ROIDefs = newdefs
        if (CallChangeListener):
            self._CallDictionaryChangedListner ()
                
    def getROIDefs (self) :
        return self._ROIDefs        
        
    # method: getScanPhaseTxt  returns current scanPhase string
    def getScanPhaseTxt  (self):
        if self._scanPhase == None :
            return "Unknown"
        return self._scanPhase
        
    def setNiftiDataSourceDescription (self, description) : #description is a rcc_util.FileObject describing the datasource
        self._niftiDataSourceDescription = description
        
    # method: setScanPhase  (phase:string) sets the current scan phase and saves dictionary to a file
    def setScanPhase (self, phase):
         if (self._scanPhase != phase) :
             #print (("Setting Scan Phase", phase))
             self._scanPhase = phase
             self._CallDictionaryChangedListner ()
    
    def shutdownExceptionCloseFileHandle (self) :
       self._tryCloseOldFileHandle (None)
       self._filHandle = None  #force file handle to none, if file is kept open then it won't be closed. This behavior is purposeful.
    
    # method: clear ()  clears the ROIDictionary object
    def clear (self, KeepFileHandleOpen = None) :   
       self._AxisProjection = None
       self._currentUISlice = None
       self._LowMemory = False
       self._sharedVolumeCache = None
       self._clearObjInProjectionCache ()
       self.clearClipMaskVolumeCache ()
       self._DocumentUID = None
       self._tryCloseOldFileHandle (KeepFileHandleOpen)
       self._filHandle = None  #force file handle to none, if file is kept open then it won't be closed. This behavior is purposeful.
       
       self._cachedNiftiShape = None
       self._DataFileTagManager = DatasetTagManager ()
       self._ROIDictionaryProjectUIDOpenedLog = ROIDictionaryProjectUIDOpenLog ()            
       self._UnloadedTimeSeriesDatasets = {}
       self._TimeDim  = 0
       self._timeDimensions = np.zeros ((1),dtype = np.uint8) 

       self._setHeaderChangeLog (())
       self._setHeaderCache ("")

       self._ROI_Dictionary = {} 
       self._excludedFileObjectList = []
       self._ProjectDataSet = None
       self._redoList = []
       self._undoList = []
      
       self.setSelected (None)
       self._scanPhase = None       
       self._niftiDataSourceDescription = None       
       self._ANTSRegistrationLog = ANTSRegistrationLog (None)
       self._NIfTIOrientation = None
       self._ProjectFileLockInterface  = None       
       self._FileChangePreviouslyDetected = False
       self._resetReadOnlyMode   (CallChangeListener = True)       
       gc.collect ()
    
    def pauseActivityTimer (self) :
        try :
            self._ActivityTimer.pauseTimer ()
        except:
            pass
    
    def resumeActivityTimer (self) :
        try :
            self._ActivityTimer.resumeTimer ()
        except:
            pass
        
    @staticmethod
    def _SavePoint (lst, dictionary, ROIDefs, ROIMode, ResetUndoChange = True) :
        tempDictionary = {}
        for key, value in dictionary.items () :         
            if (value.isROIArea()):
                obj = value.copy (MoveSliceMaskVolumeCache = True)  # Core undo/redo mechanism is based on defining custom .copy ()      
                obj.clearTemporaryObjectSliceMask ()
            else:
                obj = value.copy ()            
            tempDictionary[key] = obj  # Core undo/redo mechanism is based on defining custom .copy ()]
            if ResetUndoChange :
               value.resetObjectUndoChanged ()
        if (ROIDefs is None) :
            roiDefCopy = None
        else:
            roiDefCopy = ROIDefs.copy (CopyFilename = True)
        lst.append ((tempDictionary, roiDefCopy, ROIMode))
        return lst

    def setUndoRedoStackSize (self, stackSize) :
        if (self._undoRedoStackSize != stackSize) :
            self._undoRedoStackSize = stackSize
            while (len (self._undoList) > self._undoRedoStackSize) :
                self._undoList.pop (0)                                
            gc.collect ()

    
        
    def getSelectedObjectSlice (self) :        
        obj = self.getSelectedObject ()
        if obj is not None :
            cord = obj.getCoordinate ()
            if (cord is not None) :  
                zcord = cord.getZCoordinate ()   
                return zcord                  
        return None
    
    def setCurrentUISlice (self, sliceIndex) :
        self._currentUISlice = sliceIndex
        
    def getCurrentUISlice (self) :                
        return self._currentUISlice 
    
    
    @staticmethod
    def _getClosestChangedSlice (CurrentSlice, newDictionary, oldDictionary) :
        SliceList = []
        for roiID in newDictionary.keys () :
            if roiID not in oldDictionary :
                SliceList += newDictionary[roiID].getSliceNumberList ()
                                
        for roiID in oldDictionary.keys () :
            if roiID not in newDictionary :
                SliceList += oldDictionary[roiID].getSliceNumberList ()
            else:
                SliceList += newDictionary[roiID].getChangedSlices (oldDictionary[roiID])
        if len (SliceList) <= 0 :
            return CurrentSlice        
        if CurrentSlice is None :
            return SliceList[0]        
        sliceNumber = int (CurrentSlice)
        if len (SliceList) == 1 :
            return SliceList[0]
        SliceList = (np.array (SliceList)).astype (np.int)        
        index = np.argmin (np.abs (SliceList -  sliceNumber))
        return SliceList[index]
            
            
    # method: _SaveUndoPoint () saves the current dictionary state to the undo list to add a undo point       
    def _SaveUndoPoint (self, CallDictionaryChangedListner = True) :                
        #import traceback
        #traceback.print_stack()    
        #print ("SaveUndoPoint")        
        self.clearClipMaskVolumeCache ()        
        self._redoList = []                                
        self._undoList = ROIDictionary._SavePoint (self._undoList, self._ROI_Dictionary, self.getROIDefs (), self._ROIMode)                    
        #print ("(Saving Undo Point) Undo: %d, Redo: %d" % (len (self._undoList), len (self._redoList)))
        if (len (self._undoList) > self._undoRedoStackSize) :
            self._undoList.pop (0)                                
            gc.collect ()
        if (CallDictionaryChangedListner) :
            self._CallDictionaryChangedListner ()
        
    # method: _UpdateSelection () updates current selection info and saves current selection info in the ROIDefs
    def _UpdateSelection(self, ClearOldSelection = True) :
        for roi in self._ROI_Dictionary.values () :
            if  roi._isSelected () :
                name = roi.getName (self._ROIDefs)
                self._ROIDefs.setSelectedROI (name, clearSelection = True, CallChangeListener = False) 
                self.setSelected (roi, ClearOldSelection = ClearOldSelection)
    
    # method: undo () executes a undo action.  returns length of remaining undo list
    # core undo/redo mechanism is based on defining ".copy" method for ROI objects stored in _ROI_Dictionary
    def undo (self, All = False, ClearRedo = False) :                
        if len (self._undoList) > 0 :
            try:
                self.pauseActivityTimer ()
                self._clearObjInProjectionCache ()
                self.clearClipMaskVolumeCache ()                
                self._redoList = ROIDictionary._SavePoint (self._redoList, self._ROI_Dictionary, self.getROIDefs (), self._ROIMode, ResetUndoChange = False)                
                
                """objIsChanged = False
                for roi in self._ROI_Dictionary.values () :
                    if roi.isObjectUndoChanged () :
                        objIsChanged = True
                        break                    """
                selectedSlice = self.getCurrentUISlice ()
                oldDictionary = self._ROI_Dictionary
                
                if All :
                    self._undoList = [self._undoList[0]]
                
                changedSlice = ROIDictionary._getClosestChangedSlice(selectedSlice, self._ROI_Dictionary, self._undoList[-1][0]) 
                if changedSlice != selectedSlice :
                    selectedSlice = changedSlice
                    self._CallUndoRedoListener (SetSelectedSlice = selectedSlice)   
                                    
                self._ROI_Dictionary, savedROIDef, self._ROIMode = self._undoList.pop () 
                volumeDimensions = self.getNIfTIDim ()
                if (volumeDimensions is not None) :
                    for obj in self._getDictionaryObjectsInReverseDrawOrder () :
                        key = obj.getROIDefIDNumber ()
                        if obj.isROIArea () :
                            if key not in oldDictionary :
                                 obj.setVolumeShape (volumeDimensions, self.getSharedVolumeCacheMem (), ClipSliceMask = False)
                                 obj.setObjectChanged ()   
                            else:
                                oldObj = oldDictionary[key]
                                objectIsUndoChanged = oldObj.isObjectUndoChanged ()
                                obj.swapSliceVolumeCache (oldObj)                 
                                if objectIsUndoChanged :
                                    changedSlices = obj.getChangedSlices (oldObj)
                                    #print (("Undo",key,changedSlices))
                                    if len (changedSlices) > 0 :
                                        obj.setVolumeShape (volumeDimensions, self.getSharedVolumeCacheMem (), ClearSelectSliceList = changedSlices, ClipSliceMask = False) 
                                        obj.setObjectChanged ()  
                    if self._isSharedVolumeCacheEnabled () :
                        sharedMem = self.getSharedVolumeCacheMem ()
                        if sharedMem is not None and sharedMem.isInitalized ():
                            for key in oldDictionary.keys () :
                                if key not in self._ROI_Dictionary :
                                    sharedMem.zeroVolume (key)
                                
                self._ROIDefs.setDefinitions (savedROIDef, CallChangeListener = True, CallDictionaryChangeListener = True)
                if ClearRedo :
                    self._redoList = []
                #self._UpdateSelection(ClearOldSelection = False)                       
                self._CallDictionaryChangedListner ()
                self._CallUndoRedoListener (SetSelectedSlice = selectedSlice)
                #print ("(Call Undo) Undo: %d, Redo: %d" % (len (self._undoList), len (self._redoList)))
                return len (self._undoList)
            finally:
                self.resumeActivityTimer ()
        return 0
        
    # method: redo () executes a redo action returns the length of the remaining redo list
    # core undo/redo mechanism is based on defining ".copy" method for ROI objects stored in _ROI_Dictionary
    def redo (self) :        
        if len (self._redoList) > 0 :
            try:
                self.pauseActivityTimer ()
                self._clearObjInProjectionCache ()
                self.clearClipMaskVolumeCache ()                                
                self._undoList = ROIDictionary._SavePoint (self._undoList, self._ROI_Dictionary, self.getROIDefs (), self._ROIMode, ResetUndoChange = False)                
                if (len (self._undoList) > self._undoRedoStackSize) :
                    self._undoList.pop (0)                        
                    gc.collect ()
                
                selectedSlice = self.getCurrentUISlice ()
                changedSlice = ROIDictionary._getClosestChangedSlice(selectedSlice, self._redoList[-1][0], self._ROI_Dictionary)                            
                if changedSlice != selectedSlice :
                    selectedSlice = changedSlice                                    
                    self._CallUndoRedoListener (SetSelectedSlice = selectedSlice)   
                
                oldDictionary = self._ROI_Dictionary
                self._ROI_Dictionary, savedROIDef, self._ROIMode = self._redoList.pop ()  
                
                volumeDimensions = self.getNIfTIDim ()
                if (volumeDimensions is not None) :
                    for obj in self._getDictionaryObjectsInReverseDrawOrder () :
                        key = obj.getROIDefIDNumber ()
                        if obj.isROIArea () :
                            if key not in oldDictionary :
                                obj.setVolumeShape (volumeDimensions, self.getSharedVolumeCacheMem (), ClipSliceMask = False)                      
                                obj.setObjectChanged ()    
                            else:
                                oldObj = oldDictionary[key]
                                objectIsUndoChanged = obj.isObjectUndoChanged ()
                                obj.swapSliceVolumeCache (oldObj)                                
                                if objectIsUndoChanged :
                                    changedSlices = obj.getChangedSlices (oldObj)
                                    #print (("Redo",key,changedSlices))
                                    if len (changedSlices) > 0 :
                                        obj.setVolumeShape (volumeDimensions, self.getSharedVolumeCacheMem (), ClearSelectSliceList = changedSlices, ClipSliceMask = False)                     
                                        obj.setObjectChanged ()  
                    if self._isSharedVolumeCacheEnabled () :
                        sharedMem = self.getSharedVolumeCacheMem ()
                        if sharedMem is not None and sharedMem.isInitalized ():
                            for key in oldDictionary.keys () :
                                if key not in self._ROI_Dictionary :
                                    sharedMem.zeroVolume (key)
                                    
                self._ROIDefs.setDefinitions (savedROIDef, CallChangeListener = True, CallDictionaryChangeListener = True)
                #print ("(Call Redo) Undo: %d, Redo: %d" % (len (self._undoList), len (self._redoList)))   
                #self._UpdateSelection(ClearOldSelection = False)   
                self._CallDictionaryChangedListner ()
                self._CallUndoRedoListener (SetSelectedSlice = selectedSlice)
            finally:
                self.resumeActivityTimer ()
        return len (self._redoList)        
            
    # method: getUndoStackSize   returns the length of the undo stack
    def getUndoStackSize (self) :
        return len (self._undoList)
        
    # method: getRedoStackSize   returns the length of the redo stack    
    def getRedoStackSize (self) :
        return len (self._redoList)
     

    @staticmethod 
    def getScanPhaseFromHeader (header) :
       scanPhase = "Unknown"
       if (header != None and header.hasParameter ("Scan Phase")):
           scanPhase = header.getParameter ("Scan Phase")
       return scanPhase                
   
    # method: contoursExistForDataSet (filename: string)   returns true if data file contains defined ROI (contours or Points)
    @staticmethod
    def contoursExistForDataSet (filename) :    
        contoursExist, fileheader = ROIDictionary.contoursExistForDataSet_ReturnHeader (filename)
        if contoursExist :
            return True
        if (ROIDictionary.getScanPhaseFromHeader (fileheader) != "Unknown") :
            return True
        return contoursExist
    
    # method: contoursExistForDataSet (filename: string)   returns true if data file contains defined ROI (contours or Points) and returns scan phase text
    @staticmethod
    def contoursExistForDataSet_ReturnHeader (filename) :    
        if (not os.path.isfile (filename)) :   
            return False, None
        else:                                    
            return FileInterface.doesFileHaveROIorScanPhase_ReturnHeader (filename) 
            
    """def contoursExistForSelectedDataset (self) :
        if (self._filHandle != None) :
            return ROIDictionary.contoursExistForDataSet (self._filHandle.getPath ()) 
        return False"""
    
    def contoursExistForSelectedDataset (self) :
        if self.getDescription () != "Unknown" :
            return True 
        return self.ROICountDefined () > 0        
                
    
    def getROIFileHandle (self):
        return self._filHandle 
    
    
    @staticmethod
    def _filepath_fixName (path) :
        if (os.path.sep  == "\\") :
            return path.replace ("/", os.path.sep )
        elif (os.path.sep  == "/") :
            return path.replace ("\\", os.path.sep)
        else:
            print ("Warrning: unidentifed OS Path divider")
            return path
        
    @staticmethod  
    def _fixSourceDescriptionPath  (SourceDescription) :
        if (SourceDescription != None) :
            if (SourceDescription.hasParameter ("Filename")):
                fixedFilename = ROIDictionary._filepath_fixName (SourceDescription.getParameter ("Filename"))
                SourceDescription.setParameter ("Filename", fixedFilename)                
            if (SourceDescription.hasParameter ("full_path")): 
                fixedFullPath = ROIDictionary._filepath_fixName (SourceDescription.getParameter ("full_path"))
                SourceDescription.setParameter ("full_path", fixedFullPath)
            if (SourceDescription.hasParameter ("roitree_path") ):
                fixedTreePath = ROIDictionary._filepath_fixName (SourceDescription.getParameter ("roitree_path"))
                SourceDescription.setParameter ("roitree_path", fixedTreePath)        
    
    
    def _getDictionaryObjectsInReverseDrawOrder (self) :
            ObjList = list (self._ROI_Dictionary.values ())
            ObjList = self._ROIDefs.getObjectLstInROIOrder (ObjList, UseDrawTopOverride = False)
            ObjList.reverse ()
            return ObjList
    
    def getObjectsAtPoint (self, cZ, cX , cY, niftiSliceWidgetSelection, NIfTIVolume, ProjectDataset, ObjList = None) :
        if ObjList is None :
            ObjList = list (self._ROI_Dictionary.values ())
        foundROIs = [] 
        ObjList = self._ROIDefs.getObjectLstInROIOrder (ObjList, UseDrawTopOverride = True)
        if (self.isROIinAreaMode ()) :
            sharedMem = self.getSharedVolumeCacheMem ()            
            try :
                if sharedMem is not None and sharedMem.isInitalized ():
                    searchTypes = ["Point"]
                    roiID = sharedMem.getObjectIDIndexAtPoint (cX, cY, cZ)
                    if roiID is not None and roiID in self._ROI_Dictionary :
                        obj = self._ROI_Dictionary[roiID]
                        if obj in ObjList :
                            IDList = obj.getContourIDManger ().getAllocatedIDList ()
                            if len (IDList) == 1 :                        
                                contourID = IDList[0]                            
                                foundROIs.append ((obj.getName (self._ROIDefs), [contourID]))
                            elif len (IDList) > 1 :                     
                                searchTypes = ["Area", "Point"] #error occured, contour ID count is not what is expected
                else:
                    searchTypes = ["Area", "Point"]
            except:
                searchTypes = ["Area", "Point"]
            
            if len (searchTypes) == 1 :
                foundROIName = None
                if len (foundROIs) > 0 :
                    foundROIName = foundROIs[0][0]
                for obj in ObjList :
                    if obj.isROIArea () :
                        roiName = obj.getName (self._ROIDefs)
                        if roiName == foundROIName :
                            break
                        annotation = obj.getDeepGrowAnnotation () 
                        ROIUID = self._ROIDefs.getROINameUID (roiName)
                        if annotation.hasAnnotationsForSlice  (cZ) and annotation.hasAnnotationOnXAxis  (NIfTIVolume, ProjectDataset, ROIUID, cX) and annotation.hasAnnotationOnYAxis  (NIfTIVolume, ProjectDataset, ROIUID, cY) :
                            predictionMask = annotation.getDeepGrowMaskPrediction (cZ,"Z", NIfTIVolume, ProjectDataset, ROIUID) 
                            if predictionMask is not None :
                                if predictionMask[cX , cY] == 1 :
                                    foundROIs = [(roiName, [])]
                                    break
                for obj in ObjList :  
                    if obj.getType () in searchTypes :                        
                        roiContourList = obj.getSliceContoursAtPoint (cZ, cX, cY, niftiSliceWidgetSelection)
                        if (len (roiContourList) > 0) :
                            roiName = obj.getName (self._ROIDefs)
                            foundROIs.append ((roiName, roiContourList))
            else:                    
                for obj in ObjList :  
                    if obj.getType () in searchTypes :
                        roiContourList = obj.getSliceContoursAtPoint (cZ, cX, cY, niftiSliceWidgetSelection)
                        roiName = obj.getName (self._ROIDefs)
                        if (len (roiContourList) > 0) :
                            foundROIs.append ((roiName, roiContourList))
                        else:
                            if obj.isROIArea () :
                                annotation = obj.getDeepGrowAnnotation ()
                                ROIUID = self._ROIDefs.getROINameUID (roiName)
                                if annotation.hasAnnotationsForSlice  (cZ) and annotation.hasAnnotationOnXAxis  (NIfTIVolume, ProjectDataset, ROIUID, cX) and annotation.hasAnnotationOnYAxis  (NIfTIVolume, ProjectDataset, ROIUID, cY) :
                                    predictionMask = annotation.getDeepGrowMaskPrediction (cZ,"Z", NIfTIVolume, ProjectDataset, self._ROIDefs.getROINameUID (roiName)) 
                                    if predictionMask is not None :
                                        if predictionMask[cX , cY] == 1 :
                                            foundROIs.append ((roiName, []))
        else:
            for obj in ObjList :            
                roiContourList = obj.getSliceContoursAtPoint (cZ, cX, cY, niftiSliceWidgetSelection)                                                    
                if (len (roiContourList) > 0) :
                    foundROIs.append ((obj.getName (self._ROIDefs), roiContourList))
        return foundROIs 
         
    def getBlankDictionary (self) :
        newDictionary = ROIDictionary (self._ROIDefs, self._Hounsfieldsettings, self._VisSettings)
        for item in self._changeListener :
            newDictionary._changeListener.append (item)
        for item in self._undoRedoChangeListener :
            newDictionary._undoRedoChangeListener.append (item)   
        for item in self._readOnlyChangeListener :
            newDictionary._readOnlyChangeListener.append (item)   
        newDictionary._ROIMode = self._ROIMode
        return newDictionary
    
   
    @staticmethod
    def _getCPUCount (requestSize):
        requestSize = max (requestSize, 1)
        return min (PlatformMP.getAvailableCPU (), requestSize)
    
    def removeTagListener (self):
        self._DataFileTagManager.removeParameterChangeListener (self._tagsChanged)
        
    @staticmethod
    def getDocumentUIDFromFile (filename, ProjectFileLockInterface):
        try :
            if filename is None or not os.path.exists (filename) :
                return None
            try:                
                ProjectFileLockInterface.aquireFileLock ()                
                finterface = FileInterface ()                                   
                finterface.readDataFile (filename)                
                header = finterface.getFileHeader ()
                if (not header.hasParameter ("DocumentUID")) :
                    return None
                return header.getParameter ("DocumentUID")                
            finally:
                ProjectFileLockInterface.releaseFileLock ()
        except :
            return None
            
    # method: loadFromFile (filename: string)   Loads a Dictionary object from a datafile
    def loadROIDictionaryFromFile (self, fileHandle, ProjectFileLockInterface, TimeDimension = 0, CallDictionaryChangedListener = True,  ProjectDataset = None, ProgDialog = None, ReloadROIDefs = True) :
        QApp = None
        if ProjectDataset is not None :
            QApp = ProjectDataset.getApp()
            
        self._AxisProjection = None
        self._DisableFileSave = True
        self._DocumentUID =  None
        ForceFileSaveAfterLoading = False
        try:
            ProjectFileLockInterface.aquireFileLock ()                        
            try :
                self._Debug_PythonPath = ProjectDataset.getPythonProjectBasePath ()
            except:
                self._Debug_PythonPath = "Could not determin Python path."
            try:
                self._Debug_ProjectPath = ProjectDataset.getPath ()
            except:
                self._Debug_ProjectPath = "Could not determine project path"
            
            self._tryCloseOldFileHandle (fileHandle)
            self._filHandle = fileHandle
            
            self._resetReadOnlyMode (fileHandle.getFileHandleReadOnlyStatus (), CallChangeListener = True)
            self._FileChangePreviouslyDetected = False
            self._ProjectFileLockInterface = ProjectFileLockInterface
            self._clearObjInProjectionCache ()
            self.clearClipMaskVolumeCache ()
            self._redoList = []
            self._ProjectDataSet = None
            self._undoList = []
            self.setSelected (None)
            self._ROI_Dictionary = {} 
            self._excludedFileObjectList = []
            self._niftiDataSourceDescription = None
            self._NIfTIOrientation = None
            self._setHeaderChangeLog (())
            self._setHeaderCache ("")
            filename = fileHandle.getPath ()
            if (ProgDialog is not None) :
                ProgDialog.setLabelText ("Loading Header: " + filename)
            if (QApp is not None) :
                QApp.processEvents ()
            self._DataFileTagManager = DatasetTagManager ()             
            isProjectReadOnly = ProjectFileLockInterface.isProjectReadOnly ()
            self._ROIDictionaryProjectUIDOpenedLog = ROIDictionaryProjectUIDOpenLog ()            
        
            try :
                #scan phase needs to be set if it is initalized in tactic to avoid overwriting
                self._scanPhase = fileHandle.getDescriptionFromDataSource ()
            except:
                pass
            
            if (not fileHandle.isFileHandleLocked ()) :
                self.setHardReadOnlyMode (CallChangeListener = True)
            elif fileHandle.getHandleType () == "FileSystem" :
                if not fileHandle.ownsPersistentFileLock () and not fileHandle.canGetPersistentFileLock ()  :
                    self.setHardReadOnlyMode (CallChangeListener = True)
                
            if (os.path.isfile (filename)) :
                FC = None 
            else:
                self._DisableFileSave = not ProjectFileLockInterface.isRunningInDemoMode ()              
                if not fileHandle.isReadOnly () and fileHandle.isFileHandleLocked() and fileHandle.canVersionFile () :
                    try :
                        FC = self.saveROIDictionaryToFile (FileHandle = fileHandle, QApp = QApp, SaveForFileCreationVersioning = True)      
                    except:
                        FC = None
                        self.setHardReadOnlyMode ()
                        fileHandle.setOpenFileReadOnlyStatus ("HardReadOnly")
                    if (FC is not None) :
                        try:
                            print ("Creating baseline file version")
                            fileHandle.versionFile ("Baseline file version.")
                        except:
                            pass
                else:
                    FC = self.saveROIDictionaryToFile (FileHandle = fileHandle, QApp = QApp, ReturnFileContentAsStringForFileCreation = True)      
                self._DisableFileSave = True
            
            if (not os.path.isfile (filename) and FC is None) :                                    
                self.setProjectDataset (ProjectDataset, CallParamaterChangeListener = False)
                fileHandle.updateTagsFromDataSource (self._DataFileTagManager, RemoveExistingInternalTags = True)
            else:       
                    finterface = FileInterface ()   
                    _, fname = os.path.split (filename)
                    print ("Reading File Start: " + fname)
                    starttime = time.time ()
                    finterface.readDataFile (filename, QApp = QApp, FileContent = FC)
                    del FC
                    header = finterface.getFileHeader ()
                    if (not header.hasParameter ("DocumentUID")) :
                        header.setParameter ("DocumentUID", ProjectFileLockInterface.genDocumentUID())                    
                    self._DocumentUID = header.getParameter ("DocumentUID")                    
                    fileHandle.setDocumentUID (self._DocumentUID)
                    
                    #Test if the project_UID had been logged in the document.  determines if the project path                     
                    if (header.hasParameter ("PrjUIDOpenedLog")) :                        
                        self._ROIDictionaryProjectUIDOpenedLog = ROIDictionaryProjectUIDOpenLog (header.getParameter ("PrjUIDOpenedLog"))                    
                    isDocumentNew = self._ROIDictionaryProjectUIDOpenedLog.isDocumentNew (ProjectFileLockInterface)                                      
                    if (isDocumentNew) :
                        print ("Document opened in project for the first time.")
                    header.setParameter ("PrjUIDOpenedLog", self._ROIDictionaryProjectUIDOpenedLog.getFileParamater ())
                    
                    self._ANTSRegistrationLog.loadFromFile (FileHeader = header)                                 
                    if (not header.hasParameter ("Selected Object")) :
                        header.setParameter ("Selected Object", "None")
                    if (header.hasFileObject ("NIfTIVolumeOrientationDescription")):
                        self._NIfTIOrientation = header.getFileObject ("NIfTIVolumeOrientationDescription")
                                        
                    self._FileChangeTimeLog = TimeLog ()                    
                    if (header.hasFileObject ("FileChangeTimeLog")):
                        file_timeLog = header.getFileObject ("FileChangeTimeLog")                        
                        self._FileChangeTimeLog.loadFromFileObject (file_timeLog)
                                        
                    if (header.hasFileObject ("NIfTI_Data_Source_Description")):                    
                        self._niftiDataSourceDescription = header.getFileObject ("NIfTI_Data_Source_Description")
                        ROIDictionary._fixSourceDescriptionPath (self._niftiDataSourceDescription)                                            
                        if ProjectFileLockInterface is not None and fileHandle is not None:
                            treePath = fileHandle.getFileTreePath ()
                            if treePath is not None :
                                ProjectFileLockInterface.setProjectDatasetCache (treePath, copy.deepcopy(self._niftiDataSourceDescription), Key = "FileObj") 
                    
                    
                    self._DataFileTagManager.loadTagDatasetFromFile (fileHandle , FileHeader =header)                      
                    if (isDocumentNew and ProjectDataset is not None and not isProjectReadOnly) :
                           ProjectDataset.addNewTagsToProject (self._DataFileTagManager)                                           
                    self.setProjectDataset (ProjectDataset, CallParamaterChangeListener = False)
                    fileHandle.updateTagsFromDataSource (self._DataFileTagManager, RemoveExistingInternalTags = True)                    
                    
                    if (ProjectDataset is not None and ProjectDataset.getProjectTagManger().areAnyTagsDefined ()) :                                                
                        self._DataFileTagManager.saveToFile (header)
                        
                    self._DataFileTagManager.addParameterChangeListener (self._tagsChanged)
                    if (self._Hounsfieldsettings != None) :
                        if (not self._Hounsfieldsettings.isInitalized ()) :
                            self._Hounsfieldsettings.loadSettingsFromFile (header) 
                        else:
                            tempHounsfieldSettings = HounsfieldVisSettings ()
                            tempHounsfieldSettings.loadSettingsFromFile (header)
                            minMaxValue = tempHounsfieldSettings.getDatasetMinMaxValue ()
                            self._Hounsfieldsettings.setDataSetMaxRange (minMaxValue)
                            tempHounsfieldSettings = 0
                            
                    if (self._VisSettings != None) :
                        if (not self._VisSettings.isInitalized ()) :
                            self._VisSettings.loadSettingsFromFile (FileHeader = header) 
                    
                    #self._scanPhase = fileHandle.getDescriptionFromDataSource () if scan phase is non don't reload.
                    if (self._scanPhase is None) :
                        try:
                            self._scanPhase = header.getParameter ("Scan Phase")
                        except:
                            self._scanPhase = None
                    #print ("ScanPhase Loaded: %s" % (self._scanPhase))
                    
                    if (not header.hasParameter ("ROIAreaMode")) :
                        header.setParameter ("ROIAreaMode", "Perimeter" )
                    self._ROIMode = header.getParameter ("ROIAreaMode")
                    
                    if (not header.hasParameter ("UserChangeLog")) :
                        header.setParameter ("UserChangeLog", () )
                    self._setHeaderChangeLog (header.getParameter ("UserChangeLog"))                
                                                        
                    
                    objList = finterface.getFileObjects ()
                    newobjList = []
                    if (ProgDialog is not None) :
                        ProgDialog.setRange (0,len (objList))
                    if (QApp is not None) :
                         QApp.processEvents ()
                         
                    for fileObjIndex, fileobj in enumerate (objList) :                    
                        newObjName = fileobj.getParameter ("Name")  
                        if (newObjName == "None" or newObjName is None) :       
                            newobjList.append (None)
                        try :
                              if (fileobj.getFileObjectName () == "ROIPointObject") :
                                  newobj = ROIPointObject (finterface = fileobj)  
                                  if (QApp is not None) :
                                      QApp.processEvents ()
                              elif (fileobj.getFileObjectName () == "MultiROIAreaObject") :
                                  newobj = MultiROIAreaObject (finterface = fileobj, QApp = QApp)                              
                              newobjList.append (newobj)
                        except:
                              newobjList.append (None)
                        if (ProgDialog is not None) :
                            ProgDialog.setLabelText ("Decoding file object: " + newObjName)   
                            ProgDialog.setValue (fileObjIndex)   
                        if (QApp is not None) :
                            QApp.processEvents ()     
                        
                    projectUID = ProjectFileLockInterface.getProjectUID ()
                    
                    if (ReloadROIDefs) :
                        self._ROIDefs.reloadROIDefs (PreserveSelection = True)  
                    print ("Done Reading Header: " + str (time.time () - starttime))
                    if (ProgDialog is not None) :
                        ProgDialog.setRange (0,len (objList))
                    if (QApp is not None) :
                         QApp.processEvents ()
                    for fileObjIndex, fileobj in enumerate (objList) :
                        starttime = time.time ()
                        if (not fileobj.hasParameter ("UserChangeLog")):
                            fileobj.setParameter ("UserChangeLog", () )
                        newObjName = fileobj.getParameter ("Name")
                        if (fileobj.hasParameter ("ROIDefUIDObj")) :
                               ROIDefUIDObj = ROIUIDManager ()
                               ROIDefUIDObj.initFromTuple (fileobj.getParameter ("ROIDefUIDObj"))
                        else:                               
                               ForceFileSaveAfterLoading = True
                               ROIDefUIDObj = ROIUIDManager ()
                               FoundROIUID = self._ROIDefs.tryToGetUIDKeyForOrigionalName (newObjName)
                               if FoundROIUID is None :
                                   FoundROIUID = self._ROIDefs.getROINameUID  (newObjName)
                               if FoundROIUID is not None and projectUID is not None  :                                                                      
                                    projName = ProjectFileLockInterface.getProjectName ()
                                    ROIDefUIDObj.defineUIDKey (projectUID, projName, FoundROIUID, newObjName)
                                    
                        if ROIDefUIDObj.hasUIDDefined () :
                            ROIUID = ROIDefUIDObj.getUIDfromUID (projectUID)
                            if (ROIUID is not None) :
                                testName = self._ROIDefs.getROINameFromUID (ROIUID)
                                if testName is not None :
                                    newObjName = testName
                            
                            
                        if ((not isDocumentNew or isProjectReadOnly) and not self._ROIDefs.hasROI (newObjName)) : # if object is not new and ROIDefs not defined     
                            if ROIDefUIDObj.hasUIDDefined () :
                                ROIDefUIDObj.removeProjectUID (projectUID)
                                fileobj.setParameter ("ROIDefUIDObj", ROIDefUIDObj.getTuple ())
                                if (ROIDefUIDObj.count () > 0) : # base name is never removed
                                    self._excludedFileObjectList.append (fileobj)
                                else:
                                    newObjName = "None" #Don't load object
                        elif (newObjName != "None") :  #if the document is new in the project then import ROIDefs defined in the document into the project
                            roiColor = QtGui.QColor (0,0,0, 255)      
                            if (self._ROIDefs.hasROI (newObjName)) : # ROIName is  in the project
                                roiColor = self._ROIDefs.getROIColor (newObjName)
                            elif fileobj.hasParameter ("Color") :
                                R, G, B, A = fileobj.getParameter ("Color")
                                roiColor =  QtGui.QColor (R,G,B, A)  
                            
                            if (self._ROIDefs.hasROI (newObjName)) :
                                maskvalue = self._ROIDefs.getROIMaskValue (newObjName)               
                            elif (fileobj.hasParameter ("MaskValue")) :  # otherwise, if file has a ROI mask defined use it
                                maskvalue = fileobj.getParameter ("MaskValue")                        
                            else:                                       # Otherwise no ROI mask is defined set ROI mask to 1 and save it to avoid uncessary write.
                                maskvalue = 1
                                
                         
                            if (fileobj.getFileObjectName () == "ROIPointObject") :
                                roitype = "Point"
                            elif (fileobj.getFileObjectName () == "MultiROIAreaObject") :
                                roitype = "Area"
                            ForceRename = False
                            if (self._ROIDefs.hasROI (newObjName)) :
                                if self._ROIDefs.getROIType (newObjName) != roitype :
                                    ForceRename = True
                            
                            # Check here if Object Name With different Radlex ID Allready exists.  If it does rename object to new undefined name.                                                        
                            fobj_radlexID = "unknown"
                            if (fileobj.hasParameter ("RadlexID")) :
                                fobj_radlexID = fileobj.getParameter ("RadlexID")
                                if (fobj_radlexID is None or len(fobj_radlexID.strip ()) == 0) :
                                    fobj_radlexID = "unknown"                                                                                    
                            if (not self._ROIDefs.hasROI (newObjName)) :
                                NewROIDefRadlexID = fobj_radlexID
                            else:
                                existing_ROIRadlexID = self._ROIDefs.getROINameRadlexID (newObjName)
                                if (existing_ROIRadlexID == "unknown") :
                                    NewROIDefRadlexID = fobj_radlexID
                                elif existing_ROIRadlexID == fobj_radlexID :
                                    NewROIDefRadlexID = fobj_radlexID
                                elif fobj_radlexID == "unknown" :
                                    NewROIDefRadlexID = existing_ROIRadlexID
                                elif (not isDocumentNew) : #document is not new take projects radlex definintion
                                    NewROIDefRadlexID = existing_ROIRadlexID
                                else:                                
                                    NewROIDefRadlexID = fobj_radlexID
                                    #conflicting Radlex ID Found rename loaded ROI to avoid radlex conflict. 
                                    newObjName = self._ROIDefs.getUniqueName (newObjName + "_" +str (NewROIDefRadlexID) + "_" + roitype)
                                    ForceRename = False
                            if ForceRename :
                                newObjName = self._ROIDefs.getUniqueName (newObjName + "_" +str (NewROIDefRadlexID) + "_" + roitype)
                            
                            if (newObjName != "None") : 
                                if isProjectReadOnly and not self._ROIDefs.hasROI (newObjName) :  
                                    if ROIDefUIDObj.hasUIDDefined () :
                                        ROIDefUIDObj.removeProjectUID (projectUID)
                                        fileobj.setParameter ("ROIDefUIDObj", ROIDefUIDObj.getTuple ())
                                        if (ROIDefUIDObj.count () > 0) : # base name is never removed
                                            self._excludedFileObjectList.append (fileobj)
                                        else:
                                            newObjName = "None" #Don't load object
                                else:
                                    fileobj.setParameter ("RadlexID", NewROIDefRadlexID)
                                    fileobj.setParameter ("MaskValue", maskvalue)     
                                    fileobj.setParameter ("Name", newObjName)  
                                    if not self._ROIDefs.hasROI (newObjName) :                       
                                        Hidden = False               
                                        Locked = False                                                                
                                        self._ROIDefs.appendROIDef (ROIDef (roiIDNumber = None,  name = newObjName, color = (roiColor.red (), roiColor.green (), roiColor.blue() ), roitype = roitype, mask= maskvalue, Hidden =Hidden, RadlexID= NewROIDefRadlexID, Locked = Locked, ROIDefUIDObj = ROIDefUIDObj))
                                    idNumber = self._ROIDefs.getROINameIDNumber (newObjName, ignoreNotFound = True)                            
                                    newobj = newobjList[fileObjIndex]
                                    if (newobj is None) :
                                        continue                
                                    ROIUID = self._ROIDefs.getROINameUID (newObjName)
                                    ROIDefUIDObj.defineUIDtoUIDKey (projectUID, ROIUID)
                                    fileobj.setParameter ("ROIDefUIDObj", ROIDefUIDObj.getTuple ())
                                    newobj.setROIDefUIDManager (ROIDefUIDObj)    
                                    if (newobj.isROIArea ()) :
                                        #check that area slice shape dimensions match volume 
                                        if self._NIfTIOrientation != None and self._NIfTIOrientation.hasParameter ("VolumeDimension") :
                                            newobj.checkSliceShape (self._NIfTIOrientation.getParameter ("VolumeDimension"))
                                    newobj.setROIIDNumber (idNumber)    
                                    newobj.setObjectClipDictionary (self)                    
                                    self._ROI_Dictionary[idNumber] = newobj   
                        if (ProgDialog is not None) :
                            ProgDialog.setLabelText ("Loading object: " + newObjName)   
                            ProgDialog.setValue (fileObjIndex)   
                        if (QApp is not None) :
                            QApp.processEvents ()                                     
                        print ("Done Reading " + newObjName +" : " + str (time.time () - starttime))
                        starttime = time.time ()  
                    if (ProgDialog is not None) :
                        ProgDialog.setRange (0,0)
                        ProgDialog.setLabelText ("Finishing Loading: " + filename)   
                    if (QApp is not None) :
                         QApp.processEvents ()
                    #avoid uncessary header updates if ROI object list is not present create it.
                    if (not header.hasParameter ("ROIDeletionDetectionList")):
                        RoiList = []
                        for obj in list (self._ROI_Dictionary.values ()) :                                                                                        
                            name = obj.getName (self._ROIDefs)            
                            RoiList.append (name)
                        header.setParameter ("ROIDeletionDetectionList", RoiList)    
                    
                    skipHeaderObjectList = ["PrjUIDOpenedLog", "NIfTI_Data_Source_Description", "ROI_DataFile_SaveLocation", "Selected Object", "NIfTIVolumeOrientationDescription", "NIfTIDataFileHeader", "FileChangeTimeLog", "HeaderFileHeaderFILEOBJECTCOUNT"]
                    if (self._Hounsfieldsettings != None) :
                        skipHeaderObjectList = skipHeaderObjectList + self._Hounsfieldsettings.getParameterList ()
                    if (self._VisSettings != None) :
                        skipHeaderObjectList = skipHeaderObjectList + self._VisSettings.getParameterList ()
                    
                    if ForceFileSaveAfterLoading :
                        partialtxt = ""
                        print ("Project ROI IDNumbers are being updated to UUID.")
                        print ("Please wait loading will take a bit longer.")
                    else:
                        _, partialtxt = header.getFileObjectTxt ( indent = "", skipobjectlst = skipHeaderObjectList, partialOutputSkipFileHeader = True, QApp = QApp)
                    self._setHeaderCache (partialtxt)                    
                    print ("Done Reading File: " + fname + " " + str (time.time () - starttime))
                    self._ActivityTimer = ActivityTimer () # start a new activity timer        
        finally:
            ProjectFileLockInterface.releaseFileLock ()
            self._DisableFileSave = ProjectFileLockInterface.isRunningInDemoMode ()          
        if (self._filHandle is not None) :
            mode = self._getReadOnlyMode () 
            if mode == "False" :
                self.setSoftReadOnlyMode (CallChangeListener = True)
            try :
                self.saveROIDictionaryToFile (QApp = QApp)
                self._resetReadOnlyMode (mode, CallChangeListener = True) 
            except:
                self.setHardReadOnlyMode ()
                fileHandle.setOpenFileReadOnlyStatus ("HardReadOnly")     
        self._file_modified = False
        self._FileChangePreviouslyDetected = False       
        if (CallDictionaryChangedListener) :
            self._CallDictionaryChangedListner (SkipFileSave = True)    
        print ("Done Loading")
            
    def _tryCloseOldFileHandle (self, newFileHandle = None) :        
        if (self._filHandle is not None and self._filHandle != newFileHandle) :
            try :    
                if (self._file_modified) :
                    self._filHandle.close ()
                else:
                    filePath = self._filHandle.getPath ()
                    if self._ProjectDataSet is not None :
                        self._ProjectDataSet.getROISaveThread ().getGlobalFileSaveThread ().blockingFileClose (filePath)
                    self._filHandle.closeUnsaved ()
                
                self._filHandle = None
            except :
                print ("Unexpectedly could not close file handle")

    
    def setProjectDataset (self, ds, CallParamaterChangeListener = True) :
        if (self._ProjectDataSet != ds) :
            self._ProjectDataSet = ds
            if (ds is not None) :
                self.getDataFileTagManager().mergeDatsetFileTagsWithProjectTags (ds.getProjectTagManger(), CallParamaterChangeListener = CallParamaterChangeListener) 
                
    def _setHeaderCache (self, val):
        self._headerCache = val
        
    def _getHeaderCache (self):
        return self._headerCache
        
    def _setHeaderChangeLog (self, val) :
        self._headerChangeLog = val
    
    def _getHeaderChangeLog (self) :
        return self._headerChangeLog
        
    def _tryGetUserName (self, UserName = None):
        if (UserName != None) :
            return UserName
        try:
            if (self._ProjectDataSet != None):
                return self._ProjectDataSet.getUserName ()
            else:
                return "Unknown"
        except:
            return "Unknown"        
    
    @staticmethod
    def _getNewObjectChangeLog (UserName = "Unknown") :
       now = datetime.datetime.now ()
       date = (now.month, now.day, now.year)
       time = (now.hour, now.minute, now.second, now.microsecond)
       return ((UserName, date, time))
   
        
    def getDescription (self) :
        return self.getScanPhaseTxt ()
   
    def _getReadOnlyMode (self) :
        return self._readOnlyMode
    
    def _resetReadOnlyMode (self, initState = "False", CallChangeListener = False) :
        if self._readOnlyMode != initState :
            self._readOnlyMode = initState
            if (CallChangeListener) :
                self._callReadOnlyModeChangeListener ()

    def trySetOffReadOnlyMode (self, CallChangeListener = False) :
        if self._readOnlyMode == "SoftReadOnly"  :
            self._readOnlyMode = "False"
            if (CallChangeListener) :
               self._callReadOnlyModeChangeListener ()
    
    def setHardReadOnlyMode (self, CallChangeListener = False) :
        if self._readOnlyMode != "HardReadOnly" :
            self._readOnlyMode = "HardReadOnly"
            if (CallChangeListener) :
                self._callReadOnlyModeChangeListener ()
    
    def setSoftReadOnlyMode (self, CallChangeListener = False) :        
        if self._readOnlyMode == "False" :
            if self._filHandle is not None :
                self._filHandle.flushFileToDisk ()
            self._readOnlyMode = "SoftReadOnly"
            if (CallChangeListener) :
                self._callReadOnlyModeChangeListener ()
    
    def canChangeReadOnlyMode (self) :
        return  self._readOnlyMode != "HardReadOnly"
    
    def isReadOnly (self) :
        return self._readOnlyMode != "False"
    
    def isHardReadOnly (self) :
        return self._readOnlyMode == "HardReadOnly"
        
    def setFileChangedDetected (self):
        self._FileChangePreviouslyDetected = True 
        
    # method: saveROIDictionaryToFile (filename : string)    Saves current dictionary to a datafile    
    def saveROIDictionaryToFile (self, FileHandle = None, QApp = None, ReturnFileContentAsStringForFileCreation = False, SaveForFileCreationVersioning = False, ForceWrite = False) :
        UserName = None
        ProjectFileLockInterface = self._ProjectFileLockInterface
        if self._ProjectDataSet is not None :
            if QApp is None :
                QApp = self._ProjectDataSet.getApp ()
            if ProjectFileLockInterface is None :
                ProjectFileLockInterface = self._ProjectDataSet.getProjectFileInterface ()
        if ProjectFileLockInterface is not None :
            UserName = ProjectFileLockInterface.getUserName ()
            if ProjectFileLockInterface.isRunningInDemoMode () :
                return
        if (self._DisableFileSave):
            return   
        if (FileHandle is None) :
            FileHandle = self._filHandle
        if (FileHandle is not None) :
            if FileHandle.isClosed () :
                return
            filename = FileHandle.getPath ()
        else:
            return
        
        if not ReturnFileContentAsStringForFileCreation :
           if (not FileHandle.isFileHandleLocked ()) :
                return
           if (FileHandle.isReadOnly ()) :
                return
           if (self.isHardReadOnly () and not ForceWrite) :               
               return
        try:
            self.pauseActivityTimer ()
            self._ActivityTimer.keepAlive ()        
            finterface = FileInterface ()
            header = finterface.getFileHeader ()
            header.setParameter ("Name", "Header")
            header.setParameter ("File_Format_Version", 2)                        
            header.setParameter ("ROI_DataFile_SaveLocation", filename)        
            self._DataFileTagManager.saveToFile (header)
            if (self._NIfTIOrientation != None) :
                 header.addInnerFileObject (self._NIfTIOrientation)                
            documentUID = self.getDocumentUID ()
            if documentUID is None : 
                documentUID = ProjectFileLockInterface.genDocumentUID()
                self._DocumentUID = documentUID
            header.setParameter ("DocumentUID", documentUID)
            FileHandle.setDocumentUID (documentUID)
            
            header.setParameter ("Scan Phase", self.getScanPhaseTxt ())  
            header.setParameter ("ROIAreaMode", self.getROIMode ()) 
            
            #write out project open log.
            header.setParameter("PrjUIDOpenedLog", self._ROIDictionaryProjectUIDOpenedLog.getFileParamater ())
            
            self._ANTSRegistrationLog.saveToFile (FileHeader = header) 
            if (self._oldSelectedObj != None) :
                header.setParameter ("Selected Object", self._oldSelectedObj.getName (self._ROIDefs))
            else:
                header.setParameter ("Selected Object", "None")
            if (self._Hounsfieldsettings != None) :
                self._Hounsfieldsettings._saveSettingsToFile (header) 
            if (self._VisSettings != None) :
                self._VisSettings._saveSettingsToFile (FileHeader = header) 
            if (self._niftiDataSourceDescription != None) :
                header.addInnerFileObject (self._niftiDataSourceDescription)
                    
            header.addInnerFileObject (self._FileChangeTimeLog.getFileObject (self._tryGetUserName (UserName = UserName), self._ActivityTimer))
       
            #store roi list in file allows ROI removals to be detected in the header
            RoiList = []
            for obj in self._ROI_Dictionary.values () :                                                                                        
                name = obj.getName (self._ROIDefs)            
                RoiList.append (name)
            for fobj in self._excludedFileObjectList :
                name = fobj.getParameter ("Name")  
                RoiList.append (name)
            header.setParameter ("ROIDeletionDetectionList", RoiList)    
            header.setParameter ("UserChangeLog", self._getHeaderChangeLog ())                          
            
            skipHeaderObjectList = ["PrjUIDOpenedLog", "NIfTI_Data_Source_Description", "ROI_DataFile_SaveLocation", "Selected Object", "NIfTIVolumeOrientationDescription", "NIfTIDataFileHeader", "FileChangeTimeLog", "HeaderFileHeaderFILEOBJECTCOUNT"]
            if (self._Hounsfieldsettings != None) :
                skipHeaderObjectList = skipHeaderObjectList + self._Hounsfieldsettings.getParameterList ()
            if (self._VisSettings != None) :
                skipHeaderObjectList = skipHeaderObjectList + self._VisSettings.getParameterList ()
    
            # header cache fileobject cache so objects are written
            #header.clearWriteCache (ignoreobjects = ["NIfTI_Data_Source_Description", "NIfTIVolumeOrientationDescription"])                
            fileChanged = self._FileChangePreviouslyDetected
            fulltxt, partialtxt = header.getFileObjectTxt (indent = "", skipobjectlst = skipHeaderObjectList, partialOutputSkipFileHeader = True, QApp = QApp)
            if (self._getHeaderCache() != partialtxt):           
                 #print ("Header changed!")self
                 #print (("old header log", self._getHeaderChangeLog ()))
                 self._setHeaderChangeLog (ROIDictionary._getNewObjectChangeLog (UserName = self._tryGetUserName (UserName = UserName)))
                 header.setParameter ("UserChangeLog", self._getHeaderChangeLog ())                                                  
                 fulltxt, partialtxt = header.getFileObjectTxt (indent = "", skipobjectlst = skipHeaderObjectList, partialOutputSkipFileHeader = True, QApp = QApp)
                 self._setHeaderCache (partialtxt)        
                 #print (("new header change log", self._getHeaderChangeLog ()))
                 fileChanged = True
                 #print ("Header Changed")
            header.setWriteCache (fulltxt)
            
            """if ProjectFileLockInterface is not None and FileHandle is not None:
                if (header.hasFileObject ("NIfTI_Data_Source_Description")) :
                    fileobj = header.getFileObject ("NIfTI_Data_Source_Description")  
                    treePath = FileHandle.getFileTreePath ()
                    if treePath is not None :
                        ProjectFileLockInterface.setProjectDatasetCache (treePath, copy.deepcopy (fileobj), Key = "FileObj")"""
                
            if (len (self._ROI_Dictionary) > 0)  :
                ROIDictionaryObjectList = []
                UserName = self._tryGetUserName (UserName = UserName)
                memoryCompressed = False
                for roiObj in self._ROI_Dictionary.values () :
                    ROIDictionaryObjectList.append ((self._ROIDefs, UserName, roiObj))                
                    if (roiObj.isChanged ()) :
                        print ("ROI changed: %s " % roiObj.getName (self._ROIDefs))
                        fileChanged = True
                        roiObj.resetCompressionWaitIndicator ()
                    else:
                        if (roiObj.ifPossibleCompressROIMemory ()):
                            print ("ROI %s memory compressed " % roiObj.getName (self._ROIDefs))                        
                            memoryCompressed = True
                if (memoryCompressed) :
                    gc.collect ()
                if (not fileChanged) :
                    return
                for fobj in map (ROIDictionary._getFileObjectForROIObj, ROIDictionaryObjectList) :            
                    finterface.addFileObjects (fobj)  
                for fobj in self._excludedFileObjectList :
                    finterface.addFileObjects (fobj)                       
                        
            self._FileChangePreviouslyDetected = fileChanged            
            if fileChanged :      
                _, lastSavedText, newFileModify = finterface.writeDataFile (None, QApp = QApp) 
                
                if ReturnFileContentAsStringForFileCreation :
                    self._file_modified = False
                    self._FileChangePreviouslyDetected = False 
                    return lastSavedText
                else:
                    if not SaveForFileCreationVersioning :
                        if (ProjectFileLockInterface.isInMultiUserMode ()) :  
                            if FileHandle.getHandleType () == "FileSystem" :
                                if not FileHandle.ownsPersistentFileLock () and not self.isReadOnly ():                                    
                                    unlockDataset = MessageBoxUtil.showWarningMessageBox ("Resource Unlocked", "The file being modifed is currently unlocked. Do you wish to lock the file and save changes to it?") 
                                    if (not unlockDataset) :
                                        self.setSoftReadOnlyMode (CallChangeListener = True)
                                        self.undo (All=True, ClearRedo = True)
                                        return None
                                    if not FileHandle.getPersistentFileLock () :
                                        print ("Exception:  A error occured trying to acquire a persistent file lock")
                                        #debug = FileHandle.getPersistentFileLock ()
                                        return None
                                    self.trySetOffReadOnlyMode (CallChangeListener = True)
                    if (self.isReadOnly () and not ForceWrite) :
                        return None
                    if not SaveForFileCreationVersioning :
                        #if loading and saving from file system write out datafile cache to speed subsquent file loading.            
                        if (self._niftiDataSourceDescription is not None and (self._ProjectDataSet is not None or ProjectFileLockInterface is not None)) :
                            if self._niftiDataSourceDescription.hasParameter ("roitree_path"):
                                CachePath = self._niftiDataSourceDescription.getParameter ("roitree_path")
                                if (CachePath is not None) :
                                    scanPhaseCache = self.getScanPhaseTxt ()                      
                                    contoursExistFlag = (scanPhaseCache != "Unknown") or self.ROICountDefined () > 0
                                    fileCacheInterface = {}
                                    fileCacheInterface["ContoursExist"] = contoursExistFlag
                                    fileCacheInterface["AntsLog"] = self._ANTSRegistrationLog.copy (None)
                                    fileCacheInterface["ScanPhase"] = scanPhaseCache
                                    fileCacheInterface["Tags"] = self._DataFileTagManager.copy ()
                                    fileCacheInterface["FileObj"] = copy.deepcopy (self._niftiDataSourceDescription)                                    
                                    if ProjectFileLockInterface is not None :
                                        ProjectFileLockInterface.setProjectDatasetTagCache (CachePath, fileCacheInterface)
                                    elif self._ProjectDataSet is not None :
                                        self._ProjectDataSet.getProjectFileInterface ().setProjectDatasetTagCache (CachePath, fileCacheInterface)
                                    FileHandle.setSaveCacheOnCloseCachePath (filename, CachePath)
                    fileSaveQueued = False                
                    if not SaveForFileCreationVersioning :
                        try :
                            if (FileHandle is not None) :
                                FileHandle.queueFileSave (lastSavedText)                                                                
                                fileSaveQueued = True
                                self._file_modified = True
                        except:
                            print ("Warrning: A error occured tying to save async saving file using slower blocking methods.")
                            fileSaveQueued = False
                    if (not fileSaveQueued) :
                        finterface.writeDataFile (filename, QApp = QApp, FileSavePermissions = FileHandle.getFilePermissions ())
                        if (FileHandle is not None and FileHandle.getHandleType () == "Tactic" and FileHandle.getPath () == filename) :
                            FileHandle.fileSaveThreadCallback (filename, ReturnRunningAsyncProcess = False)
                        self._file_modified = True
                    self._FileChangePreviouslyDetected = False  
                    return lastSavedText
        finally:
           self.resumeActivityTimer ()
        #print ("Saving File Done")

    @staticmethod
    def _getFileObjectForROIObj (tpl) :    
        ROIDefs, UserName, obj = tpl
        objectName = obj.getName (ROIDefs)
        if obj.isChanged () :
            obj.setObjectChangeLog (ROIDictionary._getNewObjectChangeLog (UserName = UserName))
            print ("Object Changed: ", objectName)
        return obj.getFileObject (ROIDefs, Name = objectName)                
    
    def getANTSRegistrationLog (self) :
        return self._ANTSRegistrationLog.getANTSRegistrationLog ()
    
    def hasANTSRegisteredROI (self, ROIName = None) : 
        return self._ANTSRegistrationLog.hasRegisteredROI (ROIName = ROIName)
    
    def logANTSRegistration (self, ROIName, ROIType, SourceDictionary, RegistrationParams, Log) :
        self._ANTSRegistrationLog.logANTSRegistration (ROIName, ROIType, SourceDictionary, RegistrationParams, Log)
        self.saveROIDictionaryToFile ()
        
    def isScanPhaseDefined (self):
        return self.getScanPhaseTxt () != "Unknown"
    
    # method: getSelectedObject ():  returns currently selected object
    def getSelectedObject (self) :
        return  self._oldSelectedObj  
    
    def _getObjListOrder (self, tup):
        _, ROIObject = tup
        return self._ROIDefs.getROIIdNumberOrder (ROIObject.getROIDefIDNumber ())
        
    def addROIList (self, NameObjList, SaveUndoPt = True) :
        self.clearClipMaskVolumeCache ()
        selectedObject = None
        
        NameObjList = sorted(NameObjList, key=self._getObjListOrder)         
        for index, tup in enumerate(NameObjList) :
            Name, ROIObject = tup
            idNumber = self._ROIDefs.getROINameIDNumber (Name)       
            
            volumeDimensions = self.getNIfTIDim ()            
            if ROIObject.isROIArea () :
                if (volumeDimensions != ROIObject.getSliceVolumeShape ()) :
                    ROIObject.setVolumeShape (volumeDimensions, self.getSharedVolumeCacheMem ())                    
                    
            ROIObject.setROIIDNumber (idNumber) 
            ROIObject.setObjectClipDictionary (self)
            
            projFileInterface = self._ROIDefs.getProjectFileInterface ()
            if (projFileInterface is not None) :
                projectUID = projFileInterface.getProjectUID ()
                objROIDMgr = ROIObject.getROIUIDManager ()
                if (projectUID is not None and objROIDMgr is not None) :
                    projName = projFileInterface.getProjectName ()
                    objROIDMgr.defineUIDKey (projectUID, projName, idNumber)
                
            if ROIObject.hasSlices () : 
                if (SaveUndoPt) :
                     self._SaveUndoPoint ( CallDictionaryChangedListner = False)
                self._ROI_Dictionary[idNumber] = ROIObject 
                SaveUndoPt = False
                if (index == 0):
                    selectedObject = ROIObject
            elif idNumber in self._ROI_Dictionary :
                if (SaveUndoPt) :
                     self._SaveUndoPoint ( CallDictionaryChangedListner = False)
                self.removeROIEntry (Name,  first = False, callChangeListener = False) 
                SaveUndoPt = False
        for t_obj in self._ROI_Dictionary.values () : # make sure temporary objects are cleared for all ROI in the dictionary.
           t_obj.clearTemporaryObjectSliceMask ()
        self.setSelected (selectedObject)  
        self._CallDictionaryChangedListner  ()  
        
    # method: addROI (name, roiObject)  Adds an ROI to the current ROIDictionary and calls the updatelistner
    def addROI (self, Name, ROIObject, SaveUndoPt = True, SkipDictionaryChangeListener = False) :
        self.clearClipMaskVolumeCache ()
        idNumber = self._ROIDefs.getROINameIDNumber (Name)       
        ROIObject.setROIIDNumber (idNumber) 
        ROIObject.setObjectClipDictionary (self)
        
        projFileInterface = self._ROIDefs.getProjectFileInterface ()
        if (projFileInterface is not None) :
            projectUID = projFileInterface.getProjectUID ()
            objROIDMgr = ROIObject.getROIUIDManager ()
            if (projectUID is not None and objROIDMgr is not None) :
                projName = projFileInterface.getProjectName ()
                objROIDMgr.defineUIDKey (projectUID, projName, idNumber)
                
        volumeDimensions = self.getNIfTIDim ()
        if ROIObject.isROIArea () :                
            if (volumeDimensions != ROIObject.getSliceVolumeShape ()) :
                ROIObject.setVolumeShape (volumeDimensions, self.getSharedVolumeCacheMem ())                
                            
        if (ROIObject.hasSlices () or ( ROIObject.isROIArea () and ROIObject.hasDeepGrowAnnotations ())) :
            if (SaveUndoPt) :
                self._SaveUndoPoint ( CallDictionaryChangedListner = False)
            self._ROI_Dictionary[idNumber] = ROIObject
            self.setSelected (ROIObject)   
        elif idNumber in self._ROI_Dictionary :
                if (SaveUndoPt) :
                     self._SaveUndoPoint ( CallDictionaryChangedListner = False)
                self.removeROIEntry (Name,  first = False, callChangeListener = False) 
                SaveUndoPt = False
        else:
            self.setSelected (None)   
        for t_obj in self._ROI_Dictionary.values () : # make sure temporary objects are cleared for all ROI in the dictionary.
            t_obj.clearTemporaryObjectSliceMask ()
        if (not SkipDictionaryChangeListener) :
            self._CallDictionaryChangedListner  ()  
    
    def clearClipMaskVolumeCache (self, Axis = None, SliceNumbers = None, SliceNumber = None): 
      try :
           if Axis is None or "Axis" not in self._objClipMaskVolumeCache or self._objClipMaskVolumeCache["Axis"] != Axis or (SliceNumbers is None and SliceNumber is None):
               self._objClipMaskVolumeCache["Axis"] = None
               self._objClipMaskVolumeCache["SliceCache"] = {}
               return            
           sliceCache = self._objClipMaskVolumeCache["SliceCache"]
           if SliceNumber is not None :
              if SliceNumber in sliceCache :
                   del sliceCache[SliceNumber]
           else: 
               for sn in SliceNumbers :
                   if sn in sliceCache :
                       del sliceCache[sn]
           return                   
      except:
          self._objClipMaskVolumeCache["Axis"] = None
          self._objClipMaskVolumeCache["SliceCache"] = {}
          
    def getObjectClipMask (self,Axis, coordinate, obj) :
       if (self._cachedNiftiShape is None) :
           orientation = self.getNIfTIVolumeOrientationDescription()
           if (orientation is None) :
               return None
           if not orientation.hasParameter ("VolumeDimension") :
               return None
           niftishape = orientation.getParameter ("VolumeDimension")
           
           if orientation.hasParameter ("NativeNiftiFileOrientation") and orientation.hasParameter ("FinalROINiftiOrientation") :
               nativeOrientation = orientation.getParameter ("NativeNiftiFileOrientation")
               finalOrientation = orientation.getParameter ("FinalROINiftiOrientation")
               if (nativeOrientation != finalOrientation) :
                   _, niftishape = NiftiVolumeData.transformPoint (nativeOrientation, finalOrientation, (0,0,0), niftishape) 
           self._cachedNiftiShape = niftishape
       else:
           niftishape = self._cachedNiftiShape
       try :
           if (self._objClipMaskVolumeCache is None) :
               self._objClipMaskVolumeCache = {}
               self._objClipMaskVolumeCache["Axis"] = None
               self._objClipMaskVolumeCache["SliceCache"] = {}
       except:
           self._objClipMaskVolumeCache = {}
           self._objClipMaskVolumeCache["Axis"] = None
           self._objClipMaskVolumeCache["SliceCache"] = {}
       if (self._objClipMaskVolumeCache["Axis"] != Axis):
           self._objClipMaskVolumeCache["Axis"] = Axis
           self._objClipMaskVolumeCache["SliceCache"] = {}
           
       if coordinate in self._objClipMaskVolumeCache["SliceCache"] : #not yet initalized
           sliceData = self._objClipMaskVolumeCache["SliceCache"][coordinate]  
       else:
           #print ("Rebuilding Cache For Slice")
           sliceData = None
           nameList = self._ROIDefs.getNameLst ()       
           nameList.reverse ()
           for name in nameList :
               idNumber = self._ROIDefs.getROINameIDNumber (name)
               orderNumber = self._ROIDefs.getROIIdNumberOrder (idNumber)
               if (idNumber in self._ROI_Dictionary) :
                   clipobj = self._ROI_Dictionary[idNumber]
                   mask = clipobj.getObjClipMaskDictionaryHelper (niftishape, Axis, coordinate, SharedROIMask = self.getSharedVolumeCacheMem ())
                   if mask is not None :
                       if sliceData is None :
                           sliceData = np.zeros (mask.shape, dtype=np.int)
                           sliceData[...] = np.iinfo (np.int).max
                       sliceData[mask > 0] = orderNumber
           self._objClipMaskVolumeCache["SliceCache"][coordinate] = sliceData
       if (sliceData is None) :
           return None
       targetObjID = obj.getROIDefIDNumber ()
       targetObjOrderNumber = self._ROIDefs.getROIIdNumberOrder (targetObjID)
       returnMask = np.zeros (sliceData.shape, dtype=np.uint8)
       returnMask[sliceData < targetObjOrderNumber] = 1
       return returnMask
           
    
    def _mergeROI (self, Name, ROIObjectToMerge, VolumeShape, SaveUndoPoint = True):
        self._clearObjInProjectionCache ()
        self.clearClipMaskVolumeCache ()
        if (SaveUndoPoint) :
            self._SaveUndoPoint (CallDictionaryChangedListner = False)
        idNumber = self._ROIDefs.getROINameIDNumber (Name)  
        if (idNumber not in self._ROI_Dictionary) :
            obj = MultiROIAreaObject (VolumeShape = VolumeShape)
           
            obj.setROIIDNumber (idNumber)
            self._ROI_Dictionary[idNumber] = obj
            #hack to fix bug z bounding box not initalized correctly, correcting here.
            obj._ZBoundingBox = np.copy (ROIObjectToMerge._ZBoundingBox)
        else:
            obj = self._ROI_Dictionary[idNumber]

        if (obj.isROIArea () and ROIObjectToMerge.isROIArea ()) :
            NewContourIDList, FirstSlice, LastSlice = obj.mergeROI (ROIObjectToMerge, VolumeShape, PerimeterAreaObjectMerge = self.isROIinPerimeterMode())            
            return (NewContourIDList, FirstSlice, LastSlice)
        else:
            return ([], 0, 0)

        
    # method: getROIObject (name : string)  Returns the ROIobject defined for name
    def getROIObject (self, name) :
        idNumber = self._ROIDefs.getROINameIDNumber (name) 
        if (idNumber == None or idNumber not in self._ROI_Dictionary) :
            print ("Error Name->ID mapping not found! Name: " + name)
            return None
        return self._ROI_Dictionary[idNumber]
    
    # method: ROICountDefined ()  Returns the number of ROIobjects stored in the dictionary
    def ROICountDefined(self) :
        return len(self._ROI_Dictionary)
    
    # method: isROIDefined (Name : string)  Returns True if Name (ROI) is defined in the _ROIDictionary
    def isROIDefined (self, Name):
        if (Name == "None") :
            return False
            
        idNumber = self._ROIDefs.getROINameIDNumber (Name) 
        if (idNumber == 0) :
            print ("Error invalid object ROI name: " + Name)
            return False
        return idNumber in self._ROI_Dictionary

    # method: getROIAxisList  (Coordinate class, axis : string, ROIdefs object)  returns ROIObjects are visible in the coordinate and axis "string"     
    def convertAxisProjectiontoROIDictionary (self, projectionDictionary, ObjList = None) :
         ObjectDict = {}
         if projectionDictionary is not None :
             for zCoord in projectionDictionary.keys () :             
                 ROIIDSet = projectionDictionary[zCoord]
                 returnlist = []
                 for roiID in ROIIDSet :
                     if roiID in self._ROI_Dictionary : 
                         roiObject = self._ROI_Dictionary[roiID]
                         if (ObjList is None or roiObject in ObjList) :
                             returnlist.append (roiObject)      
                 if len (returnlist) > 0 :
                    ObjectDict[int (zCoord)] =  returnlist
         return ObjectDict
                        
    def getROIAxisList (self,coordinate, axis, ObjList = None, niftivolume = None, AxisProjectionControls = None) :
        returnlist = []                            
        roidefs = self.getROIDefs ()        
        if axis == "Z" and AxisProjectionControls is not None and AxisProjectionControls.isAxisRotated ()  :            
            #zCoord = AxisProjectionControls.getProjectedSliceForWorldCoordinate (coordinate, niftivolume)
            #print ((zCoord, [coordinate.getXCoordinate (), coordinate.getYCoordinate (), coordinate.getZCoordinate ()]))
            zCoord = coordinate.getZCoordinate ()
            projectionDictionary = AxisProjectionControls.getROIProjectionSliceAxis (niftivolume, self, coordinate)            
            if projectionDictionary is not None  and zCoord in projectionDictionary :
                ROIIDSet = projectionDictionary[zCoord]
                for roiID in ROIIDSet :
                    if roiID in self._ROI_Dictionary : 
                        roiObject = self._ROI_Dictionary[roiID]
                        if (ObjList is None or roiObject in ObjList) :
                            returnlist.append (roiObject)         
        else:
            for roiObject in self._ROI_Dictionary.values () :                        
                if roidefs.hasROI (roiObject.getName (roidefs)) : 
                    if roiObject.isInAxisList  (coordinate,axis, SharedROIMask = self.getSharedVolumeCacheMem ()):     
                        if ObjList is None or roiObject in ObjList :
                            returnlist.append (roiObject)
        return returnlist
       
    def getROIXYSliceDictionary (self, NIfTIVolume, ProjectDataset, IncludeDeepGrowAnnotatedSlices = False, ObjList = None) :
        X_ROI_SliceDictionary = {}  
        Y_ROI_SliceDictionary = {}  
        roidefs = self.getROIDefs ()        
        if ObjList is None :
            ObjList = list (self._ROI_Dictionary.values ())
        for roiObject in ObjList :            
            ROIName = roiObject.getName (roidefs)
            if roidefs.hasROI (ROIName) :          
                if IncludeDeepGrowAnnotatedSlices and roiObject.isROIArea () and roiObject.hasDeepGrowAnnotations () :                               
                    ROIUID = roidefs.getROINameUID (ROIName)
                    sliceDim = NIfTIVolume.getSliceDimensions ()
                    XAxisShape = np.zeros ((sliceDim[0],), dtype=np.bool)
                    YAxisShape = np.zeros ((sliceDim[1],), dtype=np.bool)
                    roiObject.getDeepGrowAnnotation ().getDeepGrowSliceXYAxisSliceNumberList (NIfTIVolume, ProjectDataset, ROIUID, XAxisShape, YAxisShape)                
                    roiObject.getXAxisSliceNumberList (SliceMem = XAxisShape, SharedROIMask = self.getSharedVolumeCacheMem ())
                    roiObject.getYAxisSliceNumberList (SliceMem = YAxisShape, SharedROIMask = self.getSharedVolumeCacheMem ())
                    xlist = XAxisShape.nonzero ()[0]
                    ylist = YAxisShape.nonzero ()[0]
                else:                
                    xlist = roiObject.getXAxisSliceNumberList (SharedROIMask = self.getSharedVolumeCacheMem ())
                    ylist = roiObject.getYAxisSliceNumberList (SharedROIMask = self.getSharedVolumeCacheMem ())

                if len (X_ROI_SliceDictionary) == 0 :
                    for index in xlist :
                        X_ROI_SliceDictionary[index] = [roiObject]
                else:
                    for index in xlist :
                        if index not in X_ROI_SliceDictionary :
                            X_ROI_SliceDictionary[index] = [roiObject]
                        else:
                            X_ROI_SliceDictionary[index].append (roiObject)  
                if len (Y_ROI_SliceDictionary) == 0 :
                    for index in ylist :
                        Y_ROI_SliceDictionary[index] = [roiObject]
                else:
                    for index in ylist :
                        if index not in Y_ROI_SliceDictionary :
                            Y_ROI_SliceDictionary[index] = [roiObject]
                        else:
                            Y_ROI_SliceDictionary[index].append (roiObject)                
        return X_ROI_SliceDictionary, Y_ROI_SliceDictionary
        
    # method: getROISliceDictionary  (ROIdefs object)  returns a python dictionary object, keyed by slice number, which contains tuples contianing the roiobject defined for the slice and a bool indicating if the slice contains human edited contours
    def getROISliceDictionary (self, SliceIndexList = None, IncludeDeepGrowAnnotatedSlices = False, ObjList = None) :
        ROI_SliceDictionary = {}  
        roidefs = self.getROIDefs ()    
        if ObjList is None :
            ObjList = list (self._ROI_Dictionary.values ())
        for roiObject in ObjList :   
            if roidefs.hasROI (roiObject.getName (roidefs)) :
                if SliceIndexList is None :
                    lst = roiObject.getSliceNumberList ()
                    if IncludeDeepGrowAnnotatedSlices and roiObject.isROIArea () and roiObject.hasDeepGrowAnnotations ():
                        for item in roiObject.getDeepGrowAnnotation ().getSliceList () :
                            if item not in lst :
                                lst.append (item)   
                    for index in lst :
                        if index not in ROI_SliceDictionary :
                            ROI_SliceDictionary[index] = [roiObject]
                        else:
                            ROI_SliceDictionary[index].append (roiObject)
                else:
                    if IncludeDeepGrowAnnotatedSlices and roiObject.isROIArea () and roiObject.hasDeepGrowAnnotations () :
                        DeepGrowSliceList =  roiObject.getDeepGrowAnnotation ().getSliceList () 
                    else: 
                        DeepGrowSliceList = []
                    for index in  SliceIndexList :
                        if roiObject.hasSlice (index) or index in DeepGrowSliceList :
                            if index not in ROI_SliceDictionary :
                                ROI_SliceDictionary[index] = [roiObject]
                            else:
                                ROI_SliceDictionary[index].append (roiObject)
        return ROI_SliceDictionary
    
    def _clearObjInProjectionCache (self, roiID = None, SliceIndex = None) :        
        if self._AxisProjection is not None and self._AxisProjection.isAxisTransformationEnabled () and self._AxisProjection.isAxisRotated ():
            self._AxisProjection.invalidateObjectSliceCache (ROIIDNumber = roiID, SliceIndex = SliceIndex)
    
    def _delROIDictionaryEntry (self, idNumber) :
        self._clearObjInProjectionCache (idNumber)     
        del self._ROI_Dictionary[idNumber]                 
        if (self._oldSelectedObj is not None and self._oldSelectedObj.getROIDefIDNumber () == idNumber) :
            self.setSelected (None)
        sharedVolumeCache = self.getSharedVolumeCacheMem ()
        if sharedVolumeCache is not None :
            sharedVolumeCache.zeroVolume (idNumber)   
           
    # method: removeROIEntry (name of ROIobject to remove, slice index (int) to remove object from, first flag to indicate if dictionary should be saved
    def removeROIEntry (self, name, sliceindex = None, first = True, callChangeListener = True, RemoveAllContours = False, Axis="Z", lastIndex = None, RemoveProjectedSlices = False, SliceView = None) :
        SliceDeleted = False
        if not self.isROIinAreaMode () and Axis != "Z" :
            return SliceDeleted
                
        roidefs = self.getROIDefs () 
        idNumber = roidefs.getROINameIDNumber (name)    
        if (idNumber in self._ROI_Dictionary) :
            if (sliceindex is None) :                    
                self.clearClipMaskVolumeCache ()
                if (first) :
                    self._SaveUndoPoint (CallDictionaryChangedListner = False) 
                SliceDeleted = True
                self._delROIDictionaryEntry (idNumber)
                if (callChangeListener) :
                    self._CallDictionaryChangedListner ()
                return SliceDeleted
            else:
                firstIndex = sliceindex
                if lastIndex is None :
                    lastIndex = sliceindex
                if Axis == "Z" :                    
                    if self._ROI_Dictionary[idNumber].hasDeleteableSliceInRange (firstIndex, lastIndex, RemoveProjectedSlices = RemoveProjectedSlices, SliceView = SliceView, ROIDictionary = self) :
                        self.clearClipMaskVolumeCache ()
                        if (first) :
                            self._SaveUndoPoint (CallDictionaryChangedListner = False)                                                         
                        SliceDeleted = True     
                    if SliceDeleted :
                        if (0 == self._ROI_Dictionary[idNumber].removeSlice (sliceindex, RemoveAllSliceContours = RemoveAllContours, Axis = Axis, SharedROIMask = self.getSharedVolumeCacheMem (), LastIndex = lastIndex, RemoveProjectedSlices = RemoveProjectedSlices, SliceView = SliceView)):
                            self._delROIDictionaryEntry (idNumber)
                        if (callChangeListener) :
                            self._CallDictionaryChangedListner ()
                    return SliceDeleted
                else:
                    if Axis == "X" :
                        axisMask = self._ROI_Dictionary[idNumber].getXAxisSliceNumberList (SharedROIMask = self.getSharedVolumeCacheMem ())
                    elif Axis == "Y" :
                        axisMask = self._ROI_Dictionary[idNumber].getYAxisSliceNumberList (SharedROIMask = self.getSharedVolumeCacheMem ())
                    axisIndex   = np.bitwise_and (axisMask <= lastIndex, axisMask >= firstIndex)
                    if np.any (axisIndex) :                        
                        self.clearClipMaskVolumeCache ()
                        if (first) :
                            self._SaveUndoPoint (CallDictionaryChangedListner = False)                             
                            first = False
                        SliceDeleted = True
                        SliceViewObjectsChanged = set ()
                        self._ROI_Dictionary[idNumber].removeSlice (firstIndex, RemoveAllSliceContours = RemoveAllContours, Axis = Axis, SharedROIMask = self.getSharedVolumeCacheMem (), LastIndex = lastIndex, SliceViewObjectsChanged = SliceViewObjectsChanged)                      
                        slicesRemaining = 1
                        for obj in SliceViewObjectsChanged :
                           slicesRemaining = obj.endCrossSliceChange ()
                        if (slicesRemaining == 0):
                            self._delROIDictionaryEntry (idNumber)
                        if (callChangeListener) :
                            self._CallDictionaryChangedListner ()
                        return SliceDeleted
        return SliceDeleted
                    
    def addDictionaryChangedListner (self, event):
        if (event not in self._changeListener) :
            self._changeListener.append (event)
       
    def _tagsChanged (self) :
        #print ("Dataset Tags Changed.  Saving DS")
        self._CallDictionaryChangedListner (SkipFileSave = False)
        
    def _CallDictionaryChangedListner (self, SkipFileSave = False):        
        if (self._filHandle != None and not SkipFileSave) :
            self.saveROIDictionaryToFile ()
        for events in self._changeListener :
            events ()
    
    def addReadOnlyModeChangeListener (self, event) :
        if (event not in self._readOnlyChangeListener) :
            self._readOnlyChangeListener.append (event)
       
    def _callReadOnlyModeChangeListener (self):
        for events in self._readOnlyChangeListener :
            events ()
            
    def addUndoRedoDictionaryChangedListner (self, event):
        if (event not in self._undoRedoChangeListener) :
            self._undoRedoChangeListener.append (event)
            
    def _CallUndoRedoListener (self, SetSelectedSlice = None):
        for events in self._undoRedoChangeListener :
            events (SetSelectedSlice)
            
    def setSelected (self, obj, ClearOldSelection = True) :        
        if (ClearOldSelection and self._oldSelectedObj is not None):
            self._oldSelectedObj.setSelected (False)
        if (obj is not None) :
            obj.setSelected (True)
        self._oldSelectedObj = obj
        
    def versionDocument (self, Comment = None) :
        if self._filHandle is not None and self._filHandle.canVersionFile () and not self.isReadOnly () and not self._filHandle.isReadOnly () and self._filHandle.isFileHandleLocked () :
            return self._filHandle.versionFile (Comment)
        return False
            
    def setROISelectedContours (self, name, contours) :                              
        roidefs = self.getROIDefs () 
        idNumber = roidefs.getROINameIDNumber (name)
        if idNumber in self._ROI_Dictionary :
          obj = self._ROI_Dictionary[idNumber]
          obj.setSelectedContours (contours)                        
          
    def getROISelectedContours (self, name) :                              
        roidefs = self.getROIDefs () 
        idNumber = roidefs.getROINameIDNumber (name)
        if idNumber in self._ROI_Dictionary :
          obj = self._ROI_Dictionary[idNumber]
          return obj.getSelectedContours ()              
        return []   
       
    def getROISelectedContourCount (self, name) :
        return len (self.getROISelectedContours (name))
        
    def getROIContourManger (self, name) :
        roidefs = self.getROIDefs () 
        idNumber = roidefs.getROINameIDNumber (name)
        if idNumber in self._ROI_Dictionary :
          obj = self._ROI_Dictionary[idNumber]             
          return obj.getContourIDManger ()
        return None
        
    # method: get defined ROI returns a list of ROI objects defined in the current dictionary
    def getDefinedROINames (self) :        
        roidefs = self.getROIDefs () 
        namelst = []
        for obj in self._ROI_Dictionary.values ():
            namelst.append (obj.getName (roidefs))
        return sorted (namelst)        

    def getFilename (self):
        return self._filHandle.getPath()


    def _ifCanQuietWrite (self) :
             if (self.isReadOnly ()) :
                 return False
             FileHandle = self._filHandle
             if FileHandle is None :
                 return False
             ProjectFileLockInterface = self._ProjectFileLockInterface
             if (ProjectFileLockInterface is not None) :  
                 if  FileHandle.getHandleType () == "FileSystem" :
                     if not FileHandle.ownsPersistentFileLock () and not self.isReadOnly () and ProjectFileLockInterface.isInMultiUserMode ():
                         return False
             return True
       
                                
    def _hounsfieldchanged (self):
        if (self._ifCanQuietWrite ()) :
            self.saveROIDictionaryToFile ()
        
    def _vischanged (self) :
       if (self._ifCanQuietWrite ()) :
            self.saveROIDictionaryToFile ()
    
    
    # Core code to convert masks into objects add UI to contour window to test.    
    @staticmethod 
    def MP_createSliceContour (shape, zSlice,  contourName, image, sliceContouringMemory) :
        #contour name may not exist if contour name has not been used yet if not used create it 
        #contour name should not exist in specified slice for ROIDef
        if np.any (image) :                                    
            sliceContouringMemory[1:sliceContouringMemory.shape[0]-1,1:sliceContouringMemory.shape[1]-1] = image
            area = FindContours.findContours (sliceContouringMemory)   
            if (len (area) > 0) :
                #print ("Area Len: " + str (len (area)))                
                contour = Contour ( 0, area[0], shape, "Human")          
                if (contour.getArea() > 0) :                                        
                    return [[contourName, zSlice, area[0]]]
        return None
      
    @staticmethod 
    def MP_addOffsetToContours (contoursFound, xoffset, yoffset, zoffset) :
          contoursFound[0][1] += zoffset
          offset = np.array ([[xoffset, yoffset]], dtype = contoursFound[0][2].dtype)
          contoursFound[0][2] +=  offset
                
  
    #generate contours across an individual object
    #determine objects outer perimeter by filling each slice
    #Algorith, detect the non-branching slice ranges of the object, then generate contours across each of these non-branching ranges.    
    MP_generateSolidObjectContourEdge = staticmethod (MP_generateSolidObjectContourEdge)
   

    def _AreaAreaObjectExctraction (self, MaskedVolume,  RoiDef, sliceDescription = None, IsSliceHumanEdited = False,sliceIndex = None, QApp = None)   :          
        if (QApp is not None) :
            QApp.processEvents()
        roiName = RoiDef.getName ()
        contourID = None
        sliceShape = (MaskedVolume.shape[0], MaskedVolume.shape[1])
        SliceAdded = []
        if (not self.isROIDefined (roiName)) :
            obj = MultiROIAreaObject ( )       
            obj.setROIIDNumber (RoiDef.getIDNumber ())                    
        else:
            obj = self.getROIObject (roiName)
        if (sliceIndex is not None) :
            sliceData = MaskedVolume[:,:]
            if np.any (sliceData) :                                    
                if (contourID is None) :
                    IDList = obj.getContourIDManger ().getAllocatedIDList ()
                    if len (IDList) > 0 :
                        contourID = IDList[0]
                    else:
                        contourID = obj.getContourIDManger ().allocateID()

                isSliceHumanEditited = IsSliceHumanEdited
                HumanEditSnapCount = 0
                if (sliceDescription is not None):
                    if sliceIndex in sliceDescription :
                        isSliceHumanEditited, HumanEditSnapCount = sliceDescription[sliceIndex]
                if (isSliceHumanEditited):
                    editedby = "Human"
                else:
                    editedby = "Computer"
                obj.setSliceROIAreaMask ( contourID, sliceIndex, sliceData, sliceShape, ContourType = editedby, SnappedHumanEditedSliceCount = HumanEditSnapCount,DelayInitLineCross = True)
                SliceAdded.append (sliceIndex)
        else:
            for ZSlice in range (MaskedVolume.shape[2]) :
                if (QApp is not None) :
                    QApp.processEvents()
                sliceData = MaskedVolume[:,:, ZSlice]
                if np.any (sliceData) :                                    
                    if (contourID is None) :
                        IDList = obj.getContourIDManger ().getAllocatedIDList ()
                        if len (IDList) > 0 :
                            contourID = IDList[0]
                        else:
                            contourID = obj.getContourIDManger ().allocateID()

                    isSliceHumanEditited = IsSliceHumanEdited
                    HumanEditSnapCount = 0
                    if (sliceDescription is not None):
                        if ZSlice in sliceDescription :
                            isSliceHumanEditited, HumanEditSnapCount = sliceDescription[ZSlice]
                    if (isSliceHumanEditited):
                        editedby = "Human"
                    else:
                        editedby = "Computer"
                    obj.setSliceROIAreaMask ( contourID, ZSlice, sliceData, sliceShape, ContourType = editedby, SnappedHumanEditedSliceCount = HumanEditSnapCount,DelayInitLineCross = True)
                    SliceAdded.append (ZSlice)
        if (contourID is not None) :
            obj.initAreaObjectLineCrossForSlicesList (SliceAdded)
            obj.setSelectedContours ([contourID])            
            self.addROI (roiName, obj,SaveUndoPt = False, SkipDictionaryChangeListener = True)                      


    #
    #  walk through the volume extract all non touching objects .    
    def _AreaPerimeterObjectExctraction (self, MaskedVolume, BaseName,  RoiDef, sliceDescription = None, IsSliceHumanEdited = False,sliceIndex = None, FileSaveThreadManger = None, ProjectDataset = None) :        
        #if Masked Voxels Found in Data
        if ProjectDataset is not None :
            QApp = ProjectDataset.getApp ()
        else:
            QApp = None
        if np.any (MaskedVolume) :   
            if (QApp is not None) :
                QApp.processEvents()
            if (sliceIndex is not None and len (MaskedVolume.shape) == 2) :
                newMem = np.zeros ((MaskedVolume.shape[0],MaskedVolume.shape[1], 3), dtype=MaskedVolume.dtype)
                newMem[:,:,1] = MaskedVolume[:,:]
                MaskedVolume = newMem

            structure = structure  = np.zeros ((3,3,3),dtype = np.uint8)            
            structure[0,1,1] = 1
            structure[1,0,1] = 1
            structure[1,1,0] = 1
            structure[1,1,1] = 1
            structure[1,1,2] = 1
            structure[1,2,1] = 1
            structure[2,1,1] = 1   
                     
            labeledObjects, num_features  = ndimage.measurements.label (MaskedVolume, structure)    
            if (num_features > 100) :
                print ("Too Many features detected.")
                print ("Num Featuers Detected: %d" % num_features)
                print ("Skipping area extraction")
                return
                
            sliceShape = (labeledObjects.shape[0], labeledObjects.shape[1])            
            PoolSize = PlatformMP.getAvailableCPU ()
            
            procQueue = PlatformMP.processQueue (ROIDictionary.MP_generateSolidObjectContourEdge, None, PoolSize = PoolSize,  SleepTime = 5, ProjectDataset=ProjectDataset)
            for featureindex in range (1, num_features+1) :                
                if (QApp is not None) :
                    QApp.processEvents()
                featureName = BaseName
                if (num_features > 1) :
                    featureName += str (featureindex)                                                                                             
                    
                processParameters = (labeledObjects, featureindex, featureName, structure, 0, 0, 0, sliceShape)
                cleanupParameters = None
                procQueue.queueJob (FileSaveThreadManger, processParameters, cleanupParameters)
            
            resultList = procQueue.join ()
             
            if (len (resultList) > 0) :
                roiName = RoiDef.getName ()
                if (not self.isROIDefined (roiName)) :
                    obj = MultiROIAreaObject ( ) 
                else:
                    obj = self.getROIObject (roiName)
                    
                contourIDLookup = {}                    
                contourID = None
                SliceAdded = set ()
                for result in resultList                 :                    
                    for contour in result :
                        if (QApp is not None) :
                             QApp.processEvents() 
                        contourName, ZSlice, Area = contour
                        if sliceIndex is not None :
                            ZSlice = sliceIndex
                        testContour = Contour (contourID = -1, area = Area, shape = sliceShape)
                        if (obj.doesContourDefineNewPerimeter (ZSlice, testContour)):
                            if (contourName in contourIDLookup) :
                                contourID = contourIDLookup[contourName]
                            else:                    
                                contourID = obj.addROIAreaContour (contourName)
                                contourIDLookup[contourName] = contourID
                            
                            isSliceHumanEditited = IsSliceHumanEdited
                            HumanEditSnapCount = 0
                            if (sliceDescription is not None):
                                if ZSlice in sliceDescription :
                                    isSliceHumanEditited, HumanEditSnapCount = sliceDescription[ZSlice]
                            if (isSliceHumanEditited):
                                editedby = "Human"
                            else:
                                editedby = "Computer"
                            obj.setSliceROIPerimeter (contourID, ZSlice, Area, sliceShape, ContourType = editedby, SnappedHumanEditedSliceCount = HumanEditSnapCount, DelayInitLineCross = True)   
                            SliceAdded.add (ZSlice)
                if (contourID is not None) :
                    obj.initAreaObjectLineCrossForSlicesList (SliceAdded)
                    obj.setSelectedContours ([contourID])            
                    self.addROI (roiName, obj,SaveUndoPt = False)                      

            
            
    @staticmethod         
    def MP_getPoint (labeledObjects, featureindex, sliceIndex, SliceAxis="Z") :         
        labledObj       = labeledObjects == featureindex
        if (sliceIndex is None) : 
            x,y,z = labledObj.nonzero ()
            x = np.mean (x)
            y = np.mean (y)
            z = np.mean (z)
        else:
            x,y = labledObj.nonzero ()
            x = np.mean (x)
            y = np.mean (y)
            z = sliceIndex
        position = Coordinate ()
        if SliceAxis == "Z" :
            position.setCoordinate ([x,y,z])        
        elif SliceAxis == "X" :
            position.setCoordinate ([z,x,y])        
        else:
            position.setCoordinate ([x,z,y])        
        return ( position)
        
    def _pointObjectExctraction (self, MaskedVolume, BaseName,  RoiDef, sliceIndex = None, QApp = None, SliceAxis="Z", HumanEditedPoint = True) :
        # find point locations at the centoids of labeled objects.
        if np.any (MaskedVolume) :                                    
            
            if (len (MaskedVolume.shape) == 3) :
                structure = structure  = np.zeros ((3,3,3),dtype = np.uint8)            
                structure[0,1,1] = 1
                structure[1,0,1] = 1
                structure[1,1,0] = 1
                structure[1,1,1] = 1
                structure[1,1,2] = 1
                structure[1,2,1] = 1
                structure[2,1,1] = 1        
                
            elif (len (MaskedVolume.shape) == 2) :
                structure = structure  = np.zeros ((3,3),dtype = np.uint8)            
                structure[0,1] = 1
                structure[1,0] = 1
                structure[1,1] = 1
                structure[1,2] = 1
                structure[2,1] = 1   
            labeledObjects, num_features  = ndimage.measurements.label (MaskedVolume, structure)    
            pointList = []
            for featureindex in range (1, num_features + 1) :                                
                pointList.append (ROIDictionary.MP_getPoint (labeledObjects, featureindex, sliceIndex, SliceAxis))
                if (QApp is not None) :
                    QApp.processEvents() 
            name = RoiDef.getName ()        
            if (self.isROIDefined (name)) :
                obj = self.getROIObject (name)
            else:
                obj = ROIPointObject  ()
            
            pointID = None
            featureindex = 0
            for point in pointList :
                if (QApp is not None) :
                    QApp.processEvents() 
                px, py, pz = point.getCoordinate ()
                if not obj.isPointPositionDefined (px, py, pz) :
                    featureindex += 1
                    if (featureindex == 1) :
                        PtName = BaseName
                    else:
                        PtName = BaseName + str (featureindex)                    
                    pointID = obj.addPoint (PtName, point, humanEditedPoint = HumanEditedPoint)                                                             
                
            if (obj.getPointCount () > 0) :
                obj.setSelectedContours ([pointID])                                                                                      
                self.addROI (RoiDef.getName (), obj, SaveUndoPt = False)                        
    
  
  
                    
    def createROISliceFromBinarySliceMaskForSliceView (self,sliceView, sliceIndex, roiName, segementSlice, ChangeMap , IsSliceHumanEdited =True, SkipFileSave = True, SliceViewObjectsChanged = None, CallDictionaryChangeListener = True) :
        try:            
            maskedVolumeAdded = False
            self.pauseActivityTimer ()            
            if (self.getROIDefs ().hasROI (roiName)):                
                if (np.any (segementSlice)) :                            
                    roiDef = self.getROIDefs ().getROIDefCopyByName (roiName)
                    self._clearObjInProjectionCache (roiID = roiDef.getIDNumber (),SliceIndex = sliceIndex)
                    self.clearClipMaskVolumeCache ()                                                            
                    if roiDef.getType () == "Area" :
                        if (not self.isROIinPerimeterMode()):                                                                                                                                                                        
                            if (not self.isROIDefined (roiName)) :
                                obj = MultiROIAreaObject ( )       
                                obj.setROIIDNumber (roiDef.getIDNumber ())                    
                                ObjCreated = True
                            else:
                                obj = self.getROIObject (roiName)
                                ObjCreated = False
                            IDList = obj.getContourIDManger ().getAllocatedIDList ()
                            if len (IDList) > 0 :
                                contourID = IDList[0]
                            else:
                                contourID = obj.getContourIDManger ().allocateID()
                            obj.setSliceViewObjectMask (sliceView,  contourID, segementSlice, sliceIndex, ChangeMap, SliceViewObjectsChanged, ContourType = "Computer")
                            obj.setSelectedContours ([contourID])            
                            if ObjCreated :
                                self.addROI (roiName, obj,SaveUndoPt = False, SkipDictionaryChangeListener = True)                      
                            if sliceView.getSliceAxis () == "Z" :
                                 sliceDim = sliceView.getNIfTIVolume().getSliceDimensions()
                                 obj.adjustBoundingBoxToIncludeSliceList ([sliceIndex],sliceDim)            
                    elif roiDef.getType () == "Point" :
                        self._pointObjectExctraction (segementSlice, roiDef.getName (),  roiDef, sliceIndex = sliceIndex, SliceAxis = sliceView.getSliceAxis (), HumanEditedPoint = False)
                    else:
                        print ("Unrecognized object type")
                        sys.exit ()                            
                    maskedVolumeAdded = True                    
            if CallDictionaryChangeListener :
                self._CallDictionaryChangedListner (SkipFileSave = SkipFileSave)
            return maskedVolumeAdded    
        finally:
            self.resumeActivityTimer ()


    def createROISliceFromBinarySliceMask (self, sliceIndex, roiName, segementSlice , IsSliceHumanEdited =True, FileSaveThreadManger = None, ProjectDataset = None, VolumeShape = None) :
        try:
            if ProjectDataset is not None :
                QApp = ProjectDataset.getApp ()
            else:
                QApp = None
            self.pauseActivityTimer ()            
            maskedVolumeAdded = False                    
            if (self.getROIDefs ().hasROI (roiName)):                
                if (np.any (segementSlice)) :                            
                    roiDef = self.getROIDefs ().getROIDefCopyByName (roiName)
                    self._clearObjInProjectionCache (roiID = roiDef.getIDNumber (),SliceIndex = sliceIndex)
                    self.clearClipMaskVolumeCache ()            
                    if roiDef.getType () == "Area" :
                        if (self.isROIinPerimeterMode()):                            
                            self._AreaPerimeterObjectExctraction (segementSlice, roiDef.getName (),  roiDef, IsSliceHumanEdited = IsSliceHumanEdited, sliceIndex = sliceIndex, FileSaveThreadManger = FileSaveThreadManger, ProjectDataset = ProjectDataset)  
                        else:
                            self._AreaAreaObjectExctraction (segementSlice,  roiDef, IsSliceHumanEdited = IsSliceHumanEdited, sliceIndex = sliceIndex, QApp = QApp)  
                        if (self.isROIDefined (roiDef.getName ())):
                            obj = self.getROIObject (roiDef.getName ())
                            if (obj is not None and obj.isSliceContoured (sliceIndex)) :
                                obj.adjustBoundingBoxToIncludeSliceList ([sliceIndex], VolumeShape)            
                    elif roiDef.getType () == "Point" :
                        self._pointObjectExctraction (segementSlice, roiDef.getName (),  roiDef, sliceIndex = sliceIndex, QApp = QApp)
                    else:
                        print ("Unrecognized object type")
                        sys.exit ()                            
                    maskedVolumeAdded = True                    
                
            return maskedVolumeAdded    
        finally:
            self.resumeActivityTimer ()

    def createROIFromMask (self, roiMaskMapping, MaskedVolume, method, DoNotOverwriteExistingROI, ExcludeExistingMaskedAreaFromImport = False, RemoveExistingMaskedAreaFromOverlapping = False, ProgressDialog = None, FileSaveThreadManger = None, ProjectDataset=None, SaveUndoPoint = True, ProgressDialogValueRange = None) :
        try:
            if ProjectDataset is not None :
                QApp = ProjectDataset.getApp ()
            else:
                QApp = None
            self.pauseActivityTimer ()
            self._clearObjInProjectionCache ()
            self.clearClipMaskVolumeCache ()
            if (method != "AndMask" and method != "OrMask") :
                print ("Invalid Method")
            #PtObjName name should not exist in specified slice for ROIDef
                sys.exit ()                
            
            ROIAdded = False
            
            newMaskMapping = {}
            for key, value in roiMaskMapping.items () :
                if value not in newMaskMapping :
                    newMaskMapping[value] = [key]
                else:
                    newMaskMapping[value].append(key)
            roiMaskMapping = newMaskMapping # maps ROI name to list of Mask Values
            if SaveUndoPoint :
                self._SaveUndoPoint (CallDictionaryChangedListner = False)
            maskedVolumeAdded = False
            
            
            
            #Code is currently disabled but when when enabled will allow 
            #Imported mask area to be ignored if it is allready defined      ExcludeExistingMaskedAreaFromImport = True      
            #
            #or or the existing masks to be editied if the space they occupy is being re-defiend
            DictionaryRoiDefs = self.getROIDefs ()
            PreserveLockedOrHiddenROI = False
            for roi in self._ROI_Dictionary.values () :
                roiName = roi.getName (DictionaryRoiDefs)
                if DictionaryRoiDefs.isROILocked (roiName) :
                    PreserveLockedOrHiddenROI = True
                    break
                
            ExistingAreaMask = None
            if (DoNotOverwriteExistingROI) : # make sure if existing ROI are to be un modified they are not changed
               RemoveExistingMaskedAreaFromOverlapping = False  
            if (ExcludeExistingMaskedAreaFromImport or RemoveExistingMaskedAreaFromOverlapping or PreserveLockedOrHiddenROI) :
                if (ExcludeExistingMaskedAreaFromImport or PreserveLockedOrHiddenROI) :
                    ExistingAreaMask = np.zeros (MaskedVolume.shape, dtype = np.bool)
                    ExistingAreaMask[:] = False
                if (RemoveExistingMaskedAreaFromOverlapping) :                        
                    maskedIndexs = MaskedVolume > 0
                for roi in self._ROI_Dictionary.values () :
                    if roi.isROIArea () :                    
                        sliceDescription = {}
                        sliceList = roi.getSliceNumberList ()
                        for slicenumber in sliceList :
                            sliceDescription[slicenumber] = (roi.doesSliceContainHumanEditedContour (slicenumber), roi.getSliceMaxHumanEditedContourSnapCount (slicenumber))                                                     
                        if (QApp is not None) :
                            QApp.processEvents() 
                        
                        objectMaskedMem = None
                        roiName = roi.getName (DictionaryRoiDefs)
                        if DictionaryRoiDefs.isROILocked (roiName) :
                             objectMaskedMem = roi.getMaskedVolume (MaskedVolume.shape)      
                             ExistingAreaMask[objectMaskedMem > 0] = True        
                             if (ProgressDialog is not None) :
                                 ProgressDialog.setLabelText ("Protecting locked ROI %s data from import." % roiName)
                        else:
                            if (ExcludeExistingMaskedAreaFromImport) :
                                objectMaskedMem = roi.getMaskedVolume (MaskedVolume.shape)      
                                ExistingAreaMask[objectMaskedMem > 0] = True        
                                if (ProgressDialog is not None) :
                                    ProgressDialog.setLabelText ("Protecting existing ROI %s data from import." % roiName)
                            if (RemoveExistingMaskedAreaFromOverlapping) : 
                                if (objectMaskedMem is None) :
                                    objectMaskedMem = roi.getMaskedVolume (MaskedVolume.shape)      
                                if (ProgressDialog is not None) :
                                    ProgressDialog.setLabelText ("Removing existing overlapping ROI %s data." % roiName)
                                if np.any (objectMaskedMem[maskedIndexs] > 0) :
                                    objectMaskedMem[maskedIndexs] = 0                            
                                    roiDef = DictionaryRoiDefs.getROIDefCopyByName (roiName)                            
                                    if (self.isROIinPerimeterMode()):
                                        self._AreaPerimeterObjectExctraction (objectMaskedMem, roiName,  roiDef, sliceDescription = sliceDescription,  FileSaveThreadManger=FileSaveThreadManger, ProjectDataset =ProjectDataset) 
                                    else:
                                        self._AreaAreaObjectExctraction (objectMaskedMem,  roiDef, sliceDescription = sliceDescription, QApp = QApp) 
            
                
            segementVolume = np.zeros (MaskedVolume.shape, dtype = np.uint8)
            keylist = list (roiMaskMapping.keys ())
            for keyIndex, roiName in enumerate(keylist) :
                if (DictionaryRoiDefs.hasROI (roiName)):
                    if (ProgressDialog is not None) :
                        ProgressDialog.setLabelText ("Generating %s ROI." % roiName)
                        if (ProgressDialogValueRange is not None) :
                            startV, endV = ProgressDialogValueRange
                            value = int ((float (endV - startV) * (float (keyIndex) / float (len (keylist)))) + startV)
                            value = min(max (int (startV), value), int (endV))
                            ProgressDialog.setValue (value)                            
                            
                    if (DoNotOverwriteExistingROI and self.isROIDefined (roiName)) :
                        continue
                    if DictionaryRoiDefs.isROILocked (roiName)  : # Do not import locked ROIs
                        continue
                    maskValueList = roiMaskMapping[roiName]
                    
                    segementVolume[:] = 0
                    for MaskValue in maskValueList : 
                        if (QApp is not None) :
                            QApp.processEvents() 
                        convertTypeToMatchingType = np.array ([MaskValue],dtype = MaskedVolume.dtype)
                        matchingBinaryValue = convertTypeToMatchingType[0]
                        if (method == "OrMask") :
                            TestVolume = np.bitwise_and (MaskedVolume, matchingBinaryValue) 
                            segementVolume[TestVolume != 0] = 1
                            del TestVolume
                        else:                                                       
                            segementVolume[MaskedVolume == matchingBinaryValue] = 1
                       
                    
                    if ((ExcludeExistingMaskedAreaFromImport or PreserveLockedOrHiddenROI) and ExistingAreaMask is not None) :
                        segementVolume[ExistingAreaMask] = 0
                        
                    if (np.any (segementVolume)) :                            
                        roiDef = DictionaryRoiDefs.getROIDefCopyByName (roiName)
                        if roiDef.getType () == "Area" :
                            if (self.isROIinPerimeterMode()):                            
                                self._AreaPerimeterObjectExctraction (segementVolume, roiDef.getName (),  roiDef, IsSliceHumanEdited = True, FileSaveThreadManger=FileSaveThreadManger, ProjectDataset =ProjectDataset)  
                            else:
                                self._AreaAreaObjectExctraction (segementVolume,  roiDef, IsSliceHumanEdited = True, QApp = QApp)  
                            if (self.isROIDefined (roiDef.getName ())):
                                obj = self.getROIObject (roiDef.getName ())
                                if (obj is not None) :
                                    obj.adjustBoundingBoxToFitContours (volumeShape=MaskedVolume.shape)            
                        elif roiDef.getType () == "Point" :
                            self._pointObjectExctraction (segementVolume, roiDef.getName (),  roiDef, QApp = QApp)
                        else:
                            print ("Unrecognized object type")
                            sys.exit ()                            
                        maskedVolumeAdded = True                    
                
            return maskedVolumeAdded, ROIAdded   
        finally:           
            self.resumeActivityTimer ()
    

                    
    # ROI def to add from mask
    # Path to mask file
    # method to use to extract ROI from mask
    # Nifti volume to match orientation of (can be none)
    def CreateDatasetROIFromMaskForDictionary (self, roiMaskMapping, maskfilepath, method, DoNotOverwriteExistingROI, niftiVolumeToMatchOrientation = None, ExcludeExistingMaskedAreaFromImport = False, RemoveExistingMaskedAreaFromOverlapping = False, ProgressDialog = None, FileSaveThreadManger = None, ProjectDataset = None, SaveUndoPoint = True, ProgressDialogValueRange = None) :                
        try:  
            self.pauseActivityTimer ()
            self._clearObjInProjectionCache ()
            self.clearClipMaskVolumeCache ()
            MaskedVolume = NiftiVolumeData (nib.load (maskfilepath) , MatchOrientation = niftiVolumeToMatchOrientation, Verbose = False)
            MaskedVolume = MaskedVolume.getImageData ()
            if (niftiVolumeToMatchOrientation is not None and tuple (MaskedVolume.shape) != tuple (niftiVolumeToMatchOrientation.getImageData ().shape)) :
                print ("Invalid Nifti Mask shape mask must match dimensions of the datafile.")
            else:
                MaskedVolume = np.round(MaskedVolume).astype (np.int)        
                self.createROIFromMask (roiMaskMapping, MaskedVolume, method, DoNotOverwriteExistingROI, ExcludeExistingMaskedAreaFromImport = ExcludeExistingMaskedAreaFromImport, RemoveExistingMaskedAreaFromOverlapping = RemoveExistingMaskedAreaFromOverlapping, ProgressDialog= ProgressDialog, FileSaveThreadManger = FileSaveThreadManger, ProjectDataset = ProjectDataset, SaveUndoPoint = SaveUndoPoint, ProgressDialogValueRange= ProgressDialogValueRange)        
                self._CallDictionaryChangedListner ()      
        finally:
             self.resumeActivityTimer ()
    
    def covertObjToPerimeter (self, NiftiVolumeShape, roiAreaObject, roiDef,  ProgressDialog = None, FileSaveThreadManger = None, volumeMem = None, ProjectDataset = None) :
        try:
            self.pauseActivityTimer ()
            if (volumeMem is None)  :
                volumeMem = np.zeros (NiftiVolumeShape, dtype=np.int)         
            if (ProgressDialog is not None) :
                    ProgressDialog.setLabelText ("Convert ROI " + roiAreaObject.getName (roiDef) + " to perimeter mask.")                
            boundingBox = roiAreaObject.getBoundingBox ()
            sliceDescription = {}
            sliceList = roiAreaObject.getSliceNumberList ()
            for slicenumber in sliceList :
                sliceDescription[slicenumber] = (roiAreaObject.doesSliceContainHumanEditedContour (slicenumber), roiAreaObject.getSliceMaxHumanEditedContourSnapCount (slicenumber))
            
            ROIName = roiAreaObject.getName (roiDef)
            Rdef = roiDef.getROIDefCopyByName (ROIName)
            #print ("Extracting " + ROIName)
            self._AreaPerimeterObjectExctraction (roiAreaObject.getNonPerimeterMaskVolume (NiftiVolumeShape, volumeMem= volumeMem), "Contour",  Rdef, sliceDescription = sliceDescription, FileSaveThreadManger = FileSaveThreadManger, ProjectDataset = ProjectDataset) 
            roiAreaObject.removeNonPerimeterMasks ()        
            roiAreaObject.adjustBoundingBoxToFitContours ()   
            roiAreaObject.setBoundingBoxCoordinates (boundingBox) 
        finally:
            self.resumeActivityTimer ()
            
    #Core functions to convert between Area and Perimeter ROI Definitions
    def convertROIToPerimeter (self, NiftiVolumeShape, roiDef,  ProgressDialog = None, FileSaveThreadManger = None, ProjectDataset= None) :
        try:
            self.pauseActivityTimer ()
            #remove all ROI Area objects from the dictionary
            areaObjList = []
            for key, roiObject in self._ROI_Dictionary.items () :
                if (roiObject.getType () == "Area" and roiObject.hasContoursWithOnlyAreaDefinitions()) :
                    areaObjList.append (roiObject)
            if len (areaObjList) > 0 :
                self._ROIMode = "Area"  
                self._SaveUndoPoint (CallDictionaryChangedListner = False)  
                self._ROIMode = "Perimeter"              
                volumeMem = np.zeros (NiftiVolumeShape, dtype=np.int) 
                for roiAreaObject in areaObjList :
                        self.covertObjToPerimeter ( NiftiVolumeShape, roiAreaObject, roiDef,  ProgressDialog = ProgressDialog, FileSaveThreadManger = FileSaveThreadManger, volumeMem =volumeMem, ProjectDataset =ProjectDataset)
                self._CallDictionaryChangedListner ()
                return True
            return False
        finally:
            self.resumeActivityTimer ()
            
    def verrifyDictionaryIsConsistentWithPerimeterAreaSettings (self, NiftiVolumeShape, roiDef,  ProgressDialog = None, FileSaveThreadManger = None, ProjectDataset = None) :
        if (self.isROIinPerimeterMode ()) :
            self.convertROIToPerimeter (NiftiVolumeShape, roiDef, ProgressDialog = ProgressDialog, FileSaveThreadManger = FileSaveThreadManger, ProjectDataset =ProjectDataset)
        elif (self.isROIinAreaMode ()):
            if ProjectDataset is not None :
                QApp = ProjectDataset.getApp ()
            else:
                QApp = None
            self.convertROIToAreaMask (ProgressDialog = ProgressDialog, QApp = QApp)
            
    def convertROIToAreaMask (self, ProgressDialog = None, QApp = None) :
        try:
            self.pauseActivityTimer ()
            ROIConverted = False
            for key, roiObject in self._ROI_Dictionary.items () :            
                if (roiObject.getType () == "Area" and roiObject.hasContourWithPerimeterDefinitions()) :
                    if (not ROIConverted) :
                        self._ROIMode = "Perimeter" # Hack to correctly save old ROI Mode
                        self._SaveUndoPoint (CallDictionaryChangedListner = False)    
                        self._ROIMode = "Area"                    
                    if (ProgressDialog is not None) :
                        ProgressDialog.setLabelText ("Convert ROI " + roiObject.getName (self.getROIDefs ()) + " to area mask.")
                    roiObject.convertPerimeterToAreaMask (QApp = QApp) 
                    ROIConverted = True
            if (ROIConverted) :
                """self._redoList = []
                self._undoList = []"""
                self._CallDictionaryChangedListner ()
                return True
            return False
        finally:
            self.resumeActivityTimer ()
    
    def getROIMode (self) :
        return self._ROIMode
    
    def isROIinAreaMode (self) :
        return self._ROIMode == "Area"
    
    def isROIinPerimeterMode (self) :
        return self._ROIMode == "Perimeter"
                    
    def setROIPerimeterMode (self,NiftiVolumeShape, roiDef,  ProgressDialog = None, FileSaveThreadManger= None, ProjectDataset = None) :
        if (self._ROIMode != "Perimeter") :
            self._ROIMode = "Perimeter"
            self.convertROIToPerimeter (NiftiVolumeShape, roiDef, ProgressDialog = ProgressDialog, FileSaveThreadManger = FileSaveThreadManger, ProjectDataset = ProjectDataset)
            
    def setROIAreaMode (self,  ProgressDialog = None, QApp = None) :
        if (self._ROIMode != "Area") :
            self._ROIMode = "Area"            
            self.convertROIToAreaMask (ProgressDialog = ProgressDialog, QApp = QApp)   
    
  
        
    def reorientContourVolumeToMatchNiftiVolume (self, niftiVolume, FileSaveThreadManger = None, ProjectDataset = None) :
        if ProjectDataset is not None :
            QApp = ProjectDataset.getApp ()
        else:
            QApp = None
        try:
            self.pauseActivityTimer ()
            self._clearObjInProjectionCache ()
            self.clearClipMaskVolumeCache ()
            self._redoList = []
            self._undoList = []        
            volumeDescription = self.getNIfTIVolumeOrientationDescription ()                            
            if (volumeDescription is not None and volumeDescription.hasParameter ("SliceAxis") and self.ROICountDefined () > 0 and self.isROIinAreaMode ()) :
                sourceSliceAxis = volumeDescription.getParameter ("SliceAxis")            
                destinationSliceAxis = niftiVolume.getSliceAxis ()
                if (sourceSliceAxis != destinationSliceAxis) :
                   dictionaryObjDrawList = self._getDictionaryObjectsInReverseDrawOrder ()                   
                   self._ROI_Dictionary = {}
                                      
                   for roiobj in dictionaryObjDrawList  :
                       key = roiobj.getROIDefIDNumber ()
                       if (roiobj.getType () == "Point") or (roiobj.getType () == "Area") :
                           roiobj.reorientObjectToMatchVolume (niftiVolume, sourceSliceAxis, QApp = QApp, ClipSliceMask = False)                           
                       else:
                          print ("Unrecoginzed Type")
                          sys.exit ()
                       self._ROI_Dictionary[key] = roiobj
                   
                   
                   volumeDescription.setParameter ("SliceAxis", destinationSliceAxis) 
        finally:
             self.resumeActivityTimer ()
        return
    
    
    def hasDeepGrowAnnotations (self, ROIName = None) :        
        if ROIName is not None :
            roiDefs = self.getROIDefs ()
            idNumber = roiDefs.getROINameIDNumber (ROIName, ignoreNotFound = True)
            if idNumber == 0 or idNumber not in self._ROI_Dictionary :
                return False
            obj = self._ROI_Dictionary[idNumber]
            if not obj.isROIArea () : 
                return False
            deepGrowAnnotation = obj.getDeepGrowAnnotation ()
            return deepGrowAnnotation.hasAnnotations ()
        else:
            for obj in self._ROI_Dictionary.values () :
                if obj.isROIArea () : 
                    deepGrowAnnotation = obj.getDeepGrowAnnotation ()
                    if deepGrowAnnotation.hasAnnotations () :
                        return True
            return False
    
    def hasOtherDeepGrowAnnotations (self, ROIName) :
        roiDefs = self.getROIDefs ()
        for obj in self._ROI_Dictionary.values () :
            if obj.isROIArea () : 
                if obj.getName (roiDefs) != ROIName :
                    deepGrowAnnotation = obj.getDeepGrowAnnotation ()
                    if deepGrowAnnotation.hasAnnotations () :
                        return True
        return False
        
    def getROIListWithDeepGrowAnnotations (self, SliceNumber = None) :
        objLst = []
        roiDefs = self.getROIDefs ()
        for obj in self._ROI_Dictionary.values () :
            if obj.isROIArea () : 
                deepGrowAnnotation = obj.getDeepGrowAnnotation ()
                if SliceNumber is not None and deepGrowAnnotation.hasAnnotationsForSlice (SliceNumber) :
                    objLst.append (obj.getName (roiDefs))
                elif deepGrowAnnotation.hasAnnotations () :
                    objLst.append (obj.getName (roiDefs))
        return objLst
            
    def clearDeepGrowAnnotations (self, ROIName, SaveUndoPoint = True, CallDictionaryChangeListener = True) :
        roiDefs = self.getROIDefs ()
        if not roiDefs.hasROI (ROIName) :
            return False
        idNumber = roiDefs.getROINameIDNumber (ROIName)
        if idNumber not in self._ROI_Dictionary :
            return False
        obj = self._ROI_Dictionary[idNumber]
        if not obj.isROIArea () :
            return False        
        deepGrowAnnotation = obj.getDeepGrowAnnotation ()
        if not deepGrowAnnotation.hasAnnotations () :
            return False 
        if SaveUndoPoint :
            self._SaveUndoPoint (CallDictionaryChangedListner = False) 
        deepGrowAnnotation.clearAllSlices ()
        if not obj.hasSlices () :
            self.removeROIEntry (ROIName, sliceindex = None, first = False, callChangeListener = False, RemoveAllContours = True)
        if CallDictionaryChangeListener :
            self._CallDictionaryChangedListner ()
        return True
    
    def updateDeepGrowLockedObjectClipMaskForROI (self, ROIWithChangedHiddenLockedStateChanged, sliceShape) :
        roiDefs = self.getROIDefs ()
        selectedROIIDNumber = roiDefs.getROINameIDNumber (ROIWithChangedHiddenLockedStateChanged)
        if selectedROIIDNumber in self._ROI_Dictionary :
            if self._ROI_Dictionary[selectedROIIDNumber].isROIArea () : 
                listofROISlicesToUpdate = self._ROI_Dictionary[selectedROIIDNumber].getSliceNumberList ()
                if len (listofROISlicesToUpdate) > 0 :
                    for obj in self._ROI_Dictionary.values () :
                        if obj.isROIArea () : 
                            objName = obj.getName (roiDefs)
                            if objName != ROIWithChangedHiddenLockedStateChanged :
                                deepGrowAnnotation = obj.getDeepGrowAnnotation ()
                                if deepGrowAnnotation.hasAnnotations () :
                                    deepGrowAnnotatedSlices = deepGrowAnnotation.getSliceList ()
                                    for sliceIndex in listofROISlicesToUpdate :
                                        if sliceIndex in deepGrowAnnotatedSlices :
                                            clipMask = self.getLockedObjectClipMask (objName, sliceIndex, sliceShape)
                                            obj.setDeepGrowSlicePredictionClipSlice (sliceIndex, clipMask) 
                                            deepGrowAnnotation.invalidatePredictionCache (sliceIndex, 'Z')
                                        
                                    
    
    def getLockedObjectClipMask (self, selectedObjectName, sliceNumber, SliceShape) :
        roiDefs = self.getROIDefs ()
        if not roiDefs.hasSingleROIPerVoxel () :
            return None 
        SliceClipMask = None
        for name in roiDefs.getNameLst () :
            if selectedObjectName != name :
                if roiDefs.isROILocked (name) :
                    idNumber = roiDefs.getROINameIDNumber (name)
                    if idNumber in self._ROI_Dictionary :
                        sliceObj = self._ROI_Dictionary[idNumber]
                        if sliceObj.isROIArea() and sliceObj.hasSlice (sliceNumber) :
                            sliceMap = sliceObj.getSliceMap  (sliceNumber, SliceShape, NonPerimeterMasksOnly = False, ClipOverlayedObjects = roiDefs.hasSingleROIPerVoxel ()) 
                            if SliceClipMask is None :
                                SliceClipMask = sliceMap
                            else:
                                SliceClipMask[sliceMap > 0] = 1              
        return SliceClipMask
        
    def _getDeepGrowObjectChangeOrder (self, ROIObject):        
        return self._ROIDefs.getROIIdNumberOrder (ROIObject.getROIDefIDNumber ())
    
    def flattenDeepGrowAnnotations (self, ROIName, NIfTIVolume, ProjectDataset, SaveUndoPoint = True, CallDictionaryChangeListener = True) :
        roiDefs = self.getROIDefs ()
        if  roiDefs is None or not roiDefs.hasROI (ROIName) :
            return False
        idNumber = roiDefs.getROINameIDNumber (ROIName)
        if idNumber not in self._ROI_Dictionary :
            return False
        obj = self._ROI_Dictionary[idNumber]
        if not obj.isROIArea () :
            return False        
        deepGrowAnnotation = obj.getDeepGrowAnnotation ()
        sliceList = deepGrowAnnotation.getSliceList ()
        if len (sliceList) <= 0 :
            return False
        isSingleROIPerVoxelMode = roiDefs.hasSingleROIPerVoxel ()
        
        bbox = obj.getBoundingBox ()        
        xbox, ybox, zbox = bbox
        contourID = None
        
        ROINameList = roiDefs.getNameLst ()
        DeepGrowObjFlattened = False
        xDim, yDim, zDim = NIfTIVolume.getSliceDimensions ()
        SliceShape = (xDim, yDim)
        SlicesChanged = []
        SecondaryObjectChangeDictionary =  {}
        ROIUID = self._ROIDefs.getROINameUID (ROIName)
        for sliceNumber in sliceList :            
            Prediction = deepGrowAnnotation.getDeepGrowMaskPrediction (sliceNumber, "Z", NIfTIVolume, ProjectDataset, ROIUID)
            Prediction = Prediction == 1
            if np.any (Prediction) :                
                existingmask = obj.getSliceMap (sliceNumber, SliceShape, NonPerimeterMasksOnly = False, ClipOverlayedObjects = roiDefs.hasSingleROIPerVoxel ()) 
                if existingmask is None :                    
                    existingmask = np.zeros (SliceShape,dtype = np.uint8)
                    
                Prediction[existingmask > 0] = 0
                if np.any (existingmask[Prediction] == 0) :
                    ClipOverLayList = []
                    if isSingleROIPerVoxelMode :
                        for name in ROINameList :
                            if name != ROIName :
                                idNumber = roiDefs.getROINameIDNumber (name)
                                if idNumber in self._ROI_Dictionary :
                                   sliceObj = self._ROI_Dictionary[idNumber]
                                   if sliceObj.isROIArea() and sliceObj.hasSlice (sliceNumber) :
                                       if not roiDefs.isROILocked (name) :
                                           sliceMap = sliceObj.getSliceMap  (sliceNumber, SliceShape, NonPerimeterMasksOnly = False, ClipOverlayedObjects = roiDefs.hasSingleROIPerVoxel ()) 
                                           ClipOverLayList.append ((sliceObj, sliceMap))
                    
                    if SaveUndoPoint :
                        self._SaveUndoPoint (CallDictionaryChangedListner = False) 
                        SaveUndoPoint = False
                    DeepGrowObjFlattened = True
                    
                    xRange, yRange = Prediction.nonzero ()
                    xmin, xmax, ymin,ymax = np.min (xRange), np.max (xRange), np.min (yRange), np.max (yRange)
                    if xbox[0] == -1 and ybox[0] == -1 :
                        xbox[0] = int (max (xmin-5, 0))
                        xbox[1] = int (min (xmax+5, xDim - 1))              
                        ybox[0] = int (max (ymin-5, 0))
                        ybox[1] = int (min (ymax+5, yDim - 1))
                        zbox[0] = int (max (sliceNumber - 5, 0))
                        zbox[1] = int (min (sliceNumber + 5, zDim - 1))
                    else:
                        xbox[0] = int (min (max (xmin-5, 0), xbox[0]))
                        xbox[1] = int (max (min (xmax+5, xDim - 1), xbox[1]))
                        ybox[0] = int (min (max (ymin-5, 0), ybox[0]))
                        ybox[1] = int (max (min (ymax+5, yDim - 1), ybox[1]))
                        zbox[0] = int (min (max (sliceNumber - 5, 0), zbox[0]))
                        zbox[1] = int (max (min (sliceNumber + 5, zDim - 1), zbox[1]))
                    existingmask[Prediction] = 1
                    obj.setBoundingBoxCoordinates ((xbox, ybox, zbox))
                    if (contourID is None) : 
                        IDList = obj.getContourIDManger ().getAllocatedIDList ()
                        if len (IDList) > 0 :
                            contourID = IDList[0]
                        else:
                            contourID = obj.getContourIDManger ().allocateID()
                    if obj.doesSliceContainHumanEditedContour (sliceNumber) :
                        ContourType = "Human"
                    else:
                        ContourType = "Computer"
                    obj.setSliceROIAreaMask (contourID, sliceNumber, existingmask, SliceShape, ContourType, SnappedHumanEditedSliceCount = 0,DelayInitLineCross = False, createSliceIfUndefined = True)
                    SlicesChanged.append (sliceNumber)
                    for clipmasktpl in ClipOverLayList :
                        sliceObj, sliceMap = clipmasktpl 
                        if np.any (sliceMap[Prediction] == 1) :
                            sliceMap[Prediction] = 0
                            if sliceObj not in SecondaryObjectChangeDictionary :
                                SecondaryObjectChangeDictionary[sliceObj] = [sliceNumber]
                            else:
                               SecondaryObjectChangeDictionary[sliceObj].append (sliceNumber)
                            if np.any (sliceMap) :
                                 IDList = sliceObj.getContourIDManger ().getAllocatedIDList ()
                                 if len (IDList) > 0 :
                                     cID = IDList[0]
                                 else:
                                     cID = sliceObj.getContourIDManger ().allocateID()
                                 if sliceObj.doesSliceContainHumanEditedContour (sliceNumber) :
                                      ContourType = "Human"
                                 else:
                                      ContourType = "Computer"
                                 sliceObj.setSliceROIAreaMask (cID, sliceNumber, sliceMap, SliceShape, ContourType, SnappedHumanEditedSliceCount = 0,DelayInitLineCross = False, createSliceIfUndefined = True)
                            else:
                                self.removeROIEntry (sliceObj.getName (roiDefs), sliceindex = sliceNumber, first = False, callChangeListener = False, RemoveAllContours = False)    
        if DeepGrowObjFlattened :
            volumeDim = NIfTIVolume.getSliceDimensions ()
            sharedVolumeMemCache = self.getSharedVolumeCacheMem ()
            
            if (len (SlicesChanged) > 0) :
                SecondaryObjectChangeDictionary[obj] = SlicesChanged
            if len (SecondaryObjectChangeDictionary) > 0 :
                objlist = sorted (list (SecondaryObjectChangeDictionary.keys ()), key = self._getDeepGrowObjectChangeOrder)                            
                for obj in objlist :
                    SlicesChanged = SecondaryObjectChangeDictionary[obj]           
                    obj.setVolumeShape (volumeDim, sharedVolumeMemCache, SlicesChanged)   
            deepGrowAnnotation.clearAllSlices ()
            if CallDictionaryChangeListener :
                self._CallDictionaryChangedListner ()
        return DeepGrowObjFlattened
    
    
    
        
    
    def canTrainDeepGrowAnnotationModel (self, ROIName, NIfTIVolume, ProjectDataset) :
       try:
            roiDefs = self.getROIDefs ()
            if roiDefs is None or not roiDefs.hasROI (ROIName) :
                return False
                        
            idNumber = roiDefs.getROINameIDNumber (ROIName)
            obj = self._ROI_Dictionary[idNumber]
            if not obj.isROIArea () :
                return False       
           
            sliceCount = len (obj.getSliceNumberList ())
            if sliceCount <= 0 :
                deepGrowAnnotation = obj.getDeepGrowAnnotation ()
                if deepGrowAnnotation is not None :
                    sliceCount = len (deepGrowAnnotation.getSliceList ()) 
                
            if sliceCount  <= 0 :
                return False            
            MLInterface = ProjectDataset.getKerasModelLoader ()
            if MLInterface is None :
                return False            
            UID = roiDefs.getROINameUID (ROIName)        
            dgROIModelInterface = ProjectDataset.getDeepGrowROIModelInterface ()
            if dgROIModelInterface is None :
                return False                
            ROIDGModel =dgROIModelInterface.getROIModel (UID)
            if ROIDGModel is None :
                return False        
            return True
       except:
            return False
        
    def trainDeepGrowAnnotationModel (self, ROIName, NIfTIVolume, ProjectDataset, ProgressDialog = None) :       
       try:
            roiDefs = self.getROIDefs ()
            if roiDefs is None or not roiDefs.hasROI (ROIName) :
                return False
                        
            idNumber = roiDefs.getROINameIDNumber (ROIName)
            obj = self._ROI_Dictionary[idNumber]
            if not obj.isROIArea () :
                return False       
            
            TrainingData = []
            volumeDim = NIfTIVolume.getSliceDimensions ()
            shape = (volumeDim[0], volumeDim[1])
            
             
            deepGrowSliceList = []
            ObjectSliceList = obj.getSliceNumberList ()
            CombinedSliceList = copy.copy (ObjectSliceList)
            deepGrowAnnotation = obj.getDeepGrowAnnotation ()
            if deepGrowAnnotation is not None :
                deepGrowSliceList = deepGrowAnnotation.getSliceList ()
                for sliceNumber in deepGrowSliceList :
                    if sliceNumber not in CombinedSliceList :
                        CombinedSliceList.append (sliceNumber)
        
            ROIUID = self._ROIDefs.getROINameUID (ROIName)
            for sliceNumber in CombinedSliceList :
                #if obj.doesSliceContainHumanEditedContour (sliceNumber) :                    
                sliceMap = None
                if sliceNumber in ObjectSliceList :
                    sliceMap = obj.getSliceMap  (sliceNumber, shape, ClipOverlayedObjects = roiDefs.hasSingleROIPerVoxel ())
                if sliceNumber in deepGrowSliceList:                    
                    Prediction = deepGrowAnnotation.getDeepGrowMaskPrediction (sliceNumber, "Z", NIfTIVolume, ProjectDataset, ROIUID)
                    if sliceMap is None :                
                        sliceMap = Prediction
                    else:
                        sliceMap[Prediction > 0] = 1                        
                TrainingData.append ((np.copy (NIfTIVolume.getImageData(Z=sliceNumber)), sliceMap))                   
            if len (TrainingData)  <= 0 :
                return False            
            MLInterface = ProjectDataset.getKerasModelLoader ()
            if MLInterface is None :
                return False            
            UID = roiDefs.getROINameUID (ROIName)
            dgROIModelInterface = ProjectDataset.getDeepGrowROIModelInterface ()
            if dgROIModelInterface is None :
                return False                
            ROIDGModel =dgROIModelInterface.getROIModel (UID)
            if ROIDGModel is None :
                return False
            return dgROIModelInterface.trainModel (UID, TrainingData, MLInterface, BatchSize = 8, ProgressDialog = ProgressDialog, App = ProjectDataset.getApp ())
       except:
            return False
        
    