#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  6 09:30:33 2020

@author: KennethPhilbrick
"""
import numpy as np 
import math


#import matplotlib.pyplot as plt
#from skimage.draw import polygon
try:
    from cupyx.scipy.ndimage import map_coordinates as cp_map
    import cupy as cp
    CUPYXLoaded = True
    print ("cupy loaded")
except:    
    CUPYXLoaded = False

from scipy.ndimage import map_coordinates
    
from rilcontourlib.util.rcc_util import FastUnique

#from  matplotlib import cm
#from mpl_toolkits.mplot3d import Axes3D
#from mpl_toolkits.mplot3d.art3d import Poly3DCollection

def InitMapCoordMem (inputmem):
    if CUPYXLoaded :        
        return cp.array (inputmem)        
    else:
        return inputmem

def MapCoordinateWrapper (inputmem, coordinates,  order=None, mode='constant', cval=0.0, prefilter=True, AppyAny = False):
    if CUPYXLoaded :        
        GPUMem = cp_map (inputmem, coordinates, order = order, mode =mode, cval=cval, prefilter = prefilter)
        if AppyAny :
            return (cp.any (GPUMem)).get()
        return GPUMem.get()
    else:
        result = map_coordinates (inputmem, coordinates, order = order, mode =mode, cval=cval, prefilter = prefilter)
        if AppyAny :
            return np.any (result)
        return result

class  MarkerList :    
    def __init__ (self) :
          self._angle = 0
          self._isSelected = False
          self._selectionBaseAngle = None
          self._lastMouseSelection = None
    
    def  getSlope (self) :
        return self._angle
    
    
    def setProjectionVector (self, slope) :
        if self._angle != slope :
            self._angle = slope
            return True
        return False
                 
        
    def moveMarker (self, cX, cY, sliceView):        
        cX, cY = sliceView.transformPointFromImageToScreen (cX, cY)        
        pt = np.array ([cX, cY],dtype=np.float)

        coordinate = sliceView.getCoordinate ()
        if sliceView.getSliceAxis () == 'Y' :
            sX = coordinate.getXCoordinate ()
        else:
            sX = coordinate.getYCoordinate ()              
        sY = coordinate.getZCoordinate ()       
        sX, sY = sliceView.transformPointFromImageToScreen (sX, sY)
        center = np.array ([sX, sY],dtype=np.float) 
        
        #center = np.array ([int (sliceView.width ()/2), int (sliceView.height ()/2)],dtype=np.float)
        pt -= center
        mag = np.sqrt(np.sum (pt * pt))
        angleChanged = False
        if mag > 0 :
            pt /= mag
            
            newAngle = math.atan2(pt[1], pt[0]) 
            selectionStartRotation, baseAngle = self._selectionBaseAngle 
            angleDelta = newAngle - selectionStartRotation
          
            MaxAngle = math.pi * 2.0
            finalAngle = angleDelta + baseAngle
            clip = float (math.floor (abs(finalAngle / MaxAngle)))
            if MaxAngle > 0 :
                finalAngle -= (clip * MaxAngle)
            else:
                finalAngle += (clip * MaxAngle)
            
            #if finalAngle < 0 :
            #    finalAngle += MaxAngle
            #finalAngle = max (min (finalAngle, MaxAngle), 0.0)
            if finalAngle < 0.03 and finalAngle > -0.03:
                finalAngle = 0.0
            else:
                clipPosAngle1 = (math.pi * 45.0 / 180.0)
                clipPosAngle2 = 2.0 * math.pi - clipPosAngle1
                
                if not ((finalAngle <= clipPosAngle1 and finalAngle >= -clipPosAngle1) or (finalAngle >= -2.0 * math.pi and finalAngle <=  -clipPosAngle2) or (finalAngle <= 2.0 * math.pi and finalAngle >=  clipPosAngle2)) :
                    oldSlope = self.getSlope () 
                    if (oldSlope <= clipPosAngle1 and oldSlope >= -clipPosAngle1) :
                        finalAngle = max (min (finalAngle, clipPosAngle1), -clipPosAngle1)
                    elif (oldSlope >= -2.0 * math.pi and oldSlope <=  -clipPosAngle2) :
                        finalAngle = -clipPosAngle2
                    elif (oldSlope <= 2.0 * math.pi and oldSlope >=  clipPosAngle2) :
                        finalAngle = clipPosAngle2
            
            angleChanged = self.setProjectionVector (finalAngle)
            
            lastMouse = sliceView.getLastSystemMouseCoordinate ()
            mX, mY  = lastMouse
            self._lastMouseSelection = (mX, mY)
        return angleChanged
             
        
    def testAndSetSelectionMarkerSelection (self, sliceView) :
        lastMouse = sliceView.getLastSystemMouseCoordinate ()
        mX, mY  = lastMouse
       
        coordinate = sliceView.getCoordinate ()
        if sliceView.getSliceAxis () == 'Y' :
            sX = coordinate.getXCoordinate ()
        else:
            sX = coordinate.getYCoordinate ()              
        sY = coordinate.getZCoordinate ()        
        sX, sY = sliceView.transformPointFromImageToScreen (sX, sY)
            
        center = np.array ([sX, sY],dtype=np.float)
        mouse = np.array ([mX, mY],dtype=np.float)
        
        mouseV = (mouse - center)
        mag =  np.sqrt (np.sum(mouseV * mouseV))
        if mag <= 0 :
            self._isSelected = False
            self._selectionBaseAngle = None
            self._lastMouseSelection = None
        else:
            mouseV /= mag
            radians = self._angle 
            angleVector = np.array ([math.cos (radians), math.sin (radians)], dtype=np.float)
            cos = abs (np.sum (angleVector * mouseV))
            self._isSelected = cos <= 0.05 or cos >= 0.95
            if not self._isSelected :
                self._selectionBaseAngle = None
                self._lastMouseSelection = None
            else:
                self._selectionBaseAngle = math.atan2(mouseV[1], mouseV[0])
                self._selectionBaseAngle = (self._selectionBaseAngle, float (self._angle))
                self._lastMouseSelection = (mX, mY)
        return self._isSelected
        
    
    def clearMarkerSelection (self) :
        self._isSelected = False
        self._selectionBaseAngle = None
        self._lastMouseSelection = None
                
    def isMarkerSelected (self) :
        return self._isSelected
       
    def getLastMouseSelection (self) :
        return self._lastMouseSelection




class Projection :

    def __init__ (self, AxisNormal = None, InPlane1Vec = None, InPlane2Vec = None, VoxelDim = None) :
        if AxisNormal is not None :
            self.setSliceAxisNormal (AxisNormal, InPlane1Vec = InPlane1Vec, InPlane2Vec = InPlane2Vec, VoxelDim = VoxelDim)
        else:
            self.setSliceAxisNormal ([0,0,1])

    @staticmethod
    def _makeUnitVec(vec):
        mag = np.sqrt(np.sum(vec * vec))
        if mag != 1.0 :
            vec[:] /= mag

    def _setXVec (self, vec) :
        self._XVec = np.copy (vec)
        Projection._makeUnitVec (self._XVec)

    def _setYVec (self, vec) :
        self._YVec = np.copy (vec)
        Projection._makeUnitVec (self._YVec)

    def _setTransformation (self, transform):
          self._Transformation = transform

    def isTransformed (self) :
        return  self._Transformation

    def getXVec (self):
        return self._XVec

    def getYVec (self) :
        return self._YVec

    def getNormal (self) :
        return np.copy (self._ZVec)
    
    def _setSliceNormal  (self, vec) :
        self._ZVec = np.zeros((3,), dtype=np.float)
        self._ZVec[0] = vec[0]
        self._ZVec[1] = vec[1]
        self._ZVec[2] = vec[2]
        Projection._makeUnitVec(self._ZVec)

    def initTransformMatrix (self, VoxelDim = None) :
        if not self.isTransformed () :
            self._TransformMatrix = np.eye (3)
            #self._InvTransformMatrix = np.eye (3)
        else:
            self._TransformMatrix = np.zeros ((3,3), dtype=np.float)
            self._TransformMatrix[:, 0] = self._XVec
            self._TransformMatrix[:, 1] = self._YVec
            self._TransformMatrix[:, 2] = self._ZVec
            if VoxelDim is not None :
                NormVoxelDim = VoxelDim / np.min (VoxelDim)
                NormVoxelDim = np.expand_dims (NormVoxelDim, axis = -1)
                self._TransformMatrix = self._TransformMatrix / NormVoxelDim
            #self._InvTransformMatrix = np.linalg.inv(self._TransformMatrix) 


    def setSliceAxisNormal (self, vec, InPlane1Vec= None, InPlane2Vec = None, VoxelDim = None) :
        self._setSliceNormal (vec)
        WorldZ = (np.array ([0.0,0.0,1.0])).astype (np.float)
        if not np.any (np.abs(self._ZVec) - WorldZ) :
            self._setXVec (np.array ([1.0,0.0,0.0]))
            self._setYVec(np.array([0.0, 1.0, 0.0]))
            self._setTransformation (False)
        else:
            if InPlane1Vec is not None and InPlane2Vec is not None :
                XVec = np.copy (InPlane1Vec)
                YVec = np.copy (InPlane2Vec)
            else:
                XVec = np.cross (self._ZVec, WorldZ)
                YVec = np.cross (self._ZVec, XVec)
            self._setXVec (XVec)
            self._setYVec (YVec)
            if np.any (self.getXVec() - np.array ([0.0,1.0,0.0]))  or np.any (self.getYVec() - np.array ([1.0,0.0,0.0]))  :
                self._setTransformation(True)
            else:
                self._setTransformation(False)
                self._setSliceNormal(WorldZ)
        self.initTransformMatrix (VoxelDim = VoxelDim)

    def transformLocalToWorldCoordinate (self, center, pt):
        pt = np.copy(pt)
        if len (pt.shape) == 2 :
           if center is not None :
               pt[:] -= center
           expandedMatrix = np.expand_dims(self._TransformMatrix, axis=0)
           pX = np.sum(expandedMatrix[:,0,:] * pt, axis = -1)
           pY = np.sum(expandedMatrix[:,1,:] * pt, axis = -1)
           pZ = np.sum(expandedMatrix[:,2,:] * pt, axis = -1)
           pt[:,0] = pX# + center[0]
           pt[:,1] = pY# + center[1]
           pt[:,2] = pZ# + center[2]
        else:
            if center is not None :
                pt -= center
            pX = np.sum(self._TransformMatrix[0,:] * pt)
            pY = np.sum(self._TransformMatrix[1,:] * pt)
            pZ = np.sum(self._TransformMatrix[2,:] * pt)
            pt[0] = pX# + center[0]
            pt[1] = pY# + center[1]
            pt[2] = pZ# + center[2]
        return pt

    
    def  getSliceProjection (self):
        slicePoints = np.array ([[1.0,0.0, 0.0], [0.0,1.0, 0.0]], dtype = np.float) 
        slicePointProjection = self.transformLocalToWorldCoordinate (None, slicePoints)
        return slicePointProjection 
        





    #volume Dimensions
    def getTransformProjection (self, VolumeDim) :
        xdim, ydim, zdim = VolumeDim[0], VolumeDim[1], VolumeDim[2]
        slicePoints = np.array ([[0., 0., 0.], [xdim, 0.0, 0.0], [0.0, ydim, 0.0], [xdim, ydim, 0.0],
                                [0., 0., zdim], [xdim, 0.0, zdim], [0.0, ydim, zdim], [xdim, ydim,zdim]], dtype=np.float)
        center = np.array ([int (xdim/2), int (ydim/2), int (zdim/2)], dtype=np.float)
        slicePointProjection = self.transformLocalToWorldCoordinate(center, slicePoints)
        minX = np.min(slicePointProjection[:,0])
        maxX = np.max(slicePointProjection[:, 0])
        minY = np.min(slicePointProjection[:, 1])
        maxY = np.max(slicePointProjection[:, 1])
        
        return math.ceil (maxX - minX + 1), math.ceil (maxY - minY + 1)



    def getProjectionOffsetMatrix (self, sliceDim, VoxelSize) : 
       sliceProj = self.getSliceProjection ()
       dc = sliceProj[0] 
       dr = sliceProj[1]
       dc /= np.sqrt (np.sum (dc * dc))
       dr /= np.sqrt (np.sum (dr * dr))
       
       dz = self.getNormal ()
       isVertical = abs(dz[2]) > 0.50
       """if dz[2] < 0 :
           centerSign = 1
       else:
           centerSign = -1"""
       
       width, height = sliceDim 
       
       matrix = np.zeros ((2,2), dtype=np.float) 
       matrix[:,0] =  dc[0:2]
       matrix[:,1] =  dr[0:2]
       matrix = np.linalg.inv(matrix)         
       dim = np.array ([float (width), float (height)])
              
       x1 = np.sum (matrix[0,:] * dim) 
       x2 = np.sum (matrix[1,:] * dim) 
       
        #print ((x1, x2))
       
       """if  (dc[0] == 0 and dr[0] != 0) or (dc[1] == 0 and dr[1] != 0) :
           if dc[0] == 0 :
               x2 = width /dr[0]
           else:
               x2 = height/dr[1]
           
       elif  (dc[0] != 0 and dr[0] == 0) or (dc[1] != 0 and dr[1] == 0) :
           if dr[0] == 0 :
               x1 = width /dc[0]
           else:
               x1 = height/dc[1]
               
           x1 = (height - x2 * dr[1]) / dc[1]
       elif  dr[0] == 0 and dc[0] != 0 and dr[1] != 0:
           x1 = width / dc[0]
           x2 = (height - x1 * dc[0]) / dr[1]
       elif  dc[1] == 0 and dc[0] != 0 and dr[1] != 0:
           x1 = width / dc[0]
           x2 = (height - x1 * dc[0]) / dr[1]
       else:
           x2 = (height * dc[0] - width*dc[0]) / (dr[1]*dc[0] - dr[0] *dc[1])
           x1 = (width - x2 * dr[0]) / dc[0]"""
       
       dcV = dc * x1
       drV = dr * x2
       
       xdim, ydim = np.sqrt (np.sum (dcV ** 2)), np.sqrt (np.sum (drV ** 2))
       xdim = int (math.ceil ((xdim-1) / 2.0))
       ydim = int (math.ceil ((ydim-1) / 2.0))
       
       column = np.arange(-xdim,xdim+1, dtype=np.float)
       row = np.arange(-ydim,ydim+1, dtype=np.float)
       row = np.expand_dims (row,axis=-1)
       column = np.expand_dims (column,axis=-1)
       
       columnVecCenterOffsetCorection =  (xdim * np.abs (dc))
       columnVecCenterOffsetCorection[2] = 0
       rowVecCenterOffsetCorection =  (ydim * np.abs(dr))
       rowVecCenterOffsetCorection[2] = 0
       columnVec = column * dc + columnVecCenterOffsetCorection
       rowVec = row * dr + rowVecCenterOffsetCorection       
       
       columnVecAlone = np.copy (columnVec)
       rowVecAlone = np.copy (rowVec)
       
       if isVertical :
           columnVec = np.expand_dims (columnVec, 0)
           rowVec = np.expand_dims (rowVec, 1)
       else:
           columnVec = np.expand_dims (columnVec, 1)
           rowVec = np.expand_dims (rowVec, 0)
       rowPoints = rowVec + columnVec 

       xSize, ySize, zSize = VoxelSize
       voxelDim = np.array ([xSize, ySize, zSize], dtype=np.float)
       
       InPlaneXDim, InPlaneYDim = float (rowPoints.shape[0]), float (rowPoints.shape[1])
       
       xres = InPlaneXDim / np.sqrt(np.sum((drV * voxelDim)**2))
       yres = InPlaneYDim / np.sqrt(np.sum((dcV * voxelDim)**2))
       voxelProjection = np.array ([xres,yres],dtype=np.float)

       return rowPoints, voxelProjection,  columnVecAlone, rowVecAlone
    
    

    
    
    
    
    
class AxialAxisProjectionControls :
     def __init__ (self, projectedSliceView) :
         self._sliceView = projectedSliceView
         self._xAxis = MarkerList ()
         self._yAxis = MarkerList ()
         self._AxisRotationEnabled = True
         self._projectionMatrix = None
         self._projectedVoxels = None
         self._LoadedNiftiVolumeMem = None
         self._ZAxisPadding = [0,0]
         self._axisprojectionstate = int(0)
         self._MapCordMemCacheObj = None
         self._initAxisCache ()         
         
     def getProjectionState (self) :
         return self._axisprojectionstate
         
     def setAxisTransformationEnabled (self, enabled) :
         if not enabled :
             self.resetAxisRotation ()
         self._AxisRotationEnabled = enabled
         
     def isAxisTransformationEnabled (self) :
        return self._AxisRotationEnabled
    
     def _getMarkerLst (self, sliceView) :
         if sliceView.getSliceAxis () == 'X' :
             return self._xAxis
         elif sliceView.getSliceAxis () == 'Y' :
             return self._yAxis
         else:
            return None
    
     def resetAxisRotation (self) :
         self._xAxis.setProjectionVector (0.0)
         self._yAxis.setProjectionVector (0.0)
         self._xAxis.clearMarkerSelection ()
         self._yAxis.clearMarkerSelection ()
         self._projectionMatrix = None
         self._LoadedNiftiVolumeMem  = None
         self._projectedVoxels = None
         self._ZAxisPadding = [0,0]
         self._initAxisCache ()
         
     def moveMarker  (self, mx, my, mz, sliceView) :
         self._projectionMatrix = None
         self._projectedVoxels = None
         self._LoadedNiftiVolumeMem  = None
         self._ZAxisPadding = [0,0]
         self._initAxisCache ()
         if self.isAxisTransformationEnabled () :
             mList = self._getMarkerLst(sliceView)
             if mList is not None :
                sliceAxis = sliceView.getSliceAxis ()
                if sliceAxis == 'X' :
                    if mList.moveMarker (my, mz, sliceView):
                        self._initAxisCache ()
                        return True
                    return False
                elif sliceAxis == 'Y' :
                    if mList.moveMarker (mx, mz, sliceView) :
                        self._initAxisCache ()
                        return True
                    return False
         return None
       
     
     def isXAxisRotated (self) :
         if self.isAxisTransformationEnabled () : 
             slope = self._xAxis.getSlope ()             
             return (slope != 0 and not np.isnan (slope))                 
         return False
     
     def isYAxisRotated (self) :
         if self.isAxisTransformationEnabled () : 
             slope = self._yAxis.getSlope ()             
             return (slope != 0 and not np.isnan (slope))                 
         return False
     
     def isAxisRotated (self) :
         if self.isAxisTransformationEnabled () : 
             for slope in [self._xAxis.getSlope (), self._yAxis.getSlope ()] :
                if slope != 0 and not np.isnan (slope) :
                    return True
         return False
        
     def getAxisSlope (self, sliceView) :
         if self.isAxisTransformationEnabled () : 
             mList = self._getMarkerLst(sliceView)
             if mList is not None :
                return mList.getSlope ()
         return 0.0
     
     def isEnabledForAxis (self, sliceView) :
         if self.isAxisTransformationEnabled () : 
             mList = self._getMarkerLst(sliceView)
             if mList is not None :
                 return True
         return False
        
     def testAndSetMarkerSelection (self, sliceView) :
         self._projectionMatrix = None
         self._projectedVoxels = None
         self._LoadedNiftiVolumeMem  = None
         self._ZAxisPadding = [0,0]
         if self.isAxisTransformationEnabled () :
             mList = self._getMarkerLst(sliceView)
             if mList is not None :
                 return mList.testAndSetSelectionMarkerSelection (sliceView)
         return False     
                 
     def isMarkerSelected (self, sliceView) :
         if self.isAxisTransformationEnabled () :
             mList = self._getMarkerLst(sliceView)
             if mList is not None :
                 return mList.isMarkerSelected ()
         return False
     
     def clearMarkerSelection (self, sliceView) :
         if self.isAxisTransformationEnabled () :
             mList = self._getMarkerLst(sliceView)
             if mList is not None :
                 mList.clearMarkerSelection ()
         return
     
     def getLastMouseSelection (self, sliceView) :
         if self.isAxisTransformationEnabled () :
             mList = self._getMarkerLst(sliceView)
             if mList is not None :
                 return mList.getLastMouseSelection ()
         return None 
     
    
     def getAxialViewProjection (self, NIfTIVolume) :
         if NIfTIVolume is None :             
             self.resetAxisRotation ()
             return self._projectionMatrix,  self._projectedVoxels
         if self._projectionMatrix  is not None and self._projectedVoxels is not None and self._LoadedNiftiVolumeMem is NIfTIVolume:
             return self._projectionMatrix,  self._projectedVoxels
         try :
             if not self.isAxisTransformationEnabled () :
                 self._projectionMatrix = None
                 self._projectedVoxels = None
             else:
                 #print ("ReComputing Projection")
                 xAxisSlope = self._xAxis.getSlope ()
                 yAxisSlope = self._yAxis.getSlope ()
                 if xAxisSlope == 0 and yAxisSlope == 0 :
                     self._projectionMatrix = None
                     self._projectedVoxels = None
                     self._LoadedNiftiVolumeMem  = None
                 else: 
                     self._projectionMatrix = None
                     self._projectedVoxels = None
                     self._LoadedNiftiVolumeMem  = None
                     dx1, dy1 = self._sliceView.transformShapeImageToScreen (math.cos (xAxisSlope), math.sin (xAxisSlope))                      
                     d1 = np.array ([0,dx1,dy1], dtype=np.float)
                     
                     dx2, dy2 = self._sliceView.transformShapeImageToScreen (math.cos (yAxisSlope), math.sin (yAxisSlope))                                           
                     #dx2 = math.cos (yAxisSlope)
                     #dy2 = math.sin (yAxisSlope)
                     d2 = np.array ([dx2,0,dy2], dtype=np.float)
                     normal = np.cross (d1, d2)
                     projection = Projection (normal,d1,d2, NIfTIVolume.getVoxelSize ())
                     
                     dimX, dimY, dimZ = NIfTIVolume.getSliceDimensions ()
                     
                     """slicePoints = np.array ([ [1.0, 0.0, 0.0], [0.0, 1.0, 0.0]], dtype=np.float)
                     center = np.array ([0.0, 0.0, 0.0], dtype=np.float)
                     slicePointProjection = projection.transformLocalToWorldCoordinate(center, slicePoints)
                     dx = slicePointProjection[0] 
                     dy = slicePointProjection[1] 
                     InPlaneXDim = float (math.ceil(float(dimX)/float (dy[0])))
                     InPlaneYDim = float (math.ceil(float(dimY)/float (dx[1])))"""
                    
                     projectionMatrix, voxelProjection, columnProjection, rowProjection = projection.getProjectionOffsetMatrix (( dimX, dimY), NIfTIVolume.getVoxelSize ())
                     self._projectedVoxels = voxelProjection
                    
                     def addSliceData (mask, xPos, yPos, zPos, SliceData, RoundVals = False, cY=None, cZ=None, cMask=None, rX=None, rZ = None, rMask = None) :
                         
                         def fixJaggedEdge (xpos, zpos) :
                              xmin = np.min (xpos)  
                              xmax = np.max (xpos)                              
                              unset = np.min (zpos) - 1
                              xmem = np.full ((int (xmax - xmin+1),),unset, dtype=zpos.dtype)                              
                              SkippedDuplicateCleanup = False
                              adjustedXIndex = (xpos + xmin).astype (np.int)                              
                              for i in range (xpos.shape[0]) :
                                  xIndex = adjustedXIndex[i]
                                  currentValue = xmem[xIndex]
                                  iZpos = zpos[i]
                                  if currentValue == unset :
                                      xmem[xIndex] = iZpos
                                  elif iZpos < currentValue :                                          
                                      zpos[i] = currentValue
                                  elif iZpos > currentValue :                                          
                                      xmem[xIndex] = iZpos
                                      SkippedDuplicateCleanup = True                                                                                   
                              if SkippedDuplicateCleanup :
                                  Length = xpos.shape[0]
                                  zpos[0:Length] = xmem[adjustedXIndex[0:Length]]
                                                               
                         def fixJaggedEdge2D (xpos, ypos, zpos) :
                              xmin = np.min (xpos)  
                              xmax = np.max (xpos)                              
                              ymin = np.min (ypos)  
                              ymax = np.max (ypos)                              
                              unset = np.min (zpos) - 1
                              xymem = np.full ((int (xmax - xmin+1), int (ymax - ymin+1)),unset, dtype=zpos.dtype)                              
                              SkippedDuplicateCleanup = False
                              adjustedXIndex = (xpos + xmin).astype (np.int)                              
                              adjustedYIndex = (ypos + ymin).astype (np.int)                              
                              for i in range (xpos.shape[0]) :
                                  xIndex = adjustedXIndex[i]
                                  yIndex = adjustedYIndex[i]
                                  iZpos = zpos[i]
                                  currentValue = xymem[xIndex,yIndex]
                                  if currentValue == unset :
                                      xymem[xIndex,yIndex] = iZpos
                                  elif iZpos < currentValue :
                                      zpos[i] = currentValue
                                  elif iZpos > currentValue :                                          
                                      xymem[xIndex,yIndex] = iZpos
                                      SkippedDuplicateCleanup = True                                                                                                                        
                              if SkippedDuplicateCleanup :                                                                    
                                  Length = xpos.shape[0]
                                  zpos[0:Length] = xymem[adjustedXIndex[0:Length],adjustedYIndex[0:Length]]
                              
                                                                      
                         sd = SliceData
                         sd["Mask"]    = np.copy (mask)
                         xp = xPos[mask]
                         zVal = zPos[mask]
                         yp = yPos[mask]
                         if RoundVals :
                             xp = np.round (xp)
                             yp = np.round (yp)
                             zVal = np.round (zVal)
                             cY = np.round (cY[cMask])
                             rX = np.round (rX[rMask])
                             cZ = np.round (cZ[cMask])
                             rZ = np.round (rZ[rMask])
                             
                             fixJaggedEdge (cY,cZ)
                             fixJaggedEdge (rX,rZ)
                             fixJaggedEdge2D (xp,yp,zVal)
                             
                             sd["ColumnPos"] = np.zeros ((2,cY.shape[0]), dtype = np.int16)
                             sd["RowPos"] = np.zeros ((2,rX.shape[0]), dtype = np.int16)
                             sd["ColumnPos"][0,:] = cY
                             sd["ColumnPos"][1,:] = cZ
                             sd["RowPos"][0,:] = rX
                             sd["RowPos"][1,:] =  rZ
                             sd["MaskSelMem"] = np.zeros (mask.shape, dtype = np.uint)
                             sd["MaskSelMem"][mask] = (mask[mask]).nonzero ()[0]
                         sd["XYZPos"] = np.zeros ((3,xp.shape[0]), dtype = np.float32)
                         sd["XYZPos"][0,:] = xp 
                         sd["XYZPos"][1,:] = yp
                         sd["XYZPos"][2,:] = zVal
                         SliceData["ZProjection"] = (np.min (zVal), np.max (zVal))
                     
                     def clipProjection (projectionMatrix) :
                          ZMatrix = projectionMatrix[...,2]
                          XMatrix = projectionMatrix[...,0]
                          YMatrix = projectionMatrix[...,1]
                          slice2DMask = np.logical_and ((XMatrix >= 0),(XMatrix <= dimX-1)) 
                          np.logical_and ((YMatrix >= 0),slice2DMask, out = slice2DMask) 
                          np.logical_and ((YMatrix <= dimY-1),slice2DMask, out = slice2DMask) 
                          return XMatrix, YMatrix, ZMatrix, slice2DMask
                      
                     NiftiProjection = {}
                     ROIProjection = {}                     
                     XMatrix, YMatrix, ZMatrix, slice2DMask = clipProjection (projectionMatrix)
                     
                     _, cYMatrix, cZMatrix, cSlice2DMask = clipProjection (columnProjection)
                     rXMatrix, _, rZMatrix, rSlice2DMask = clipProjection (rowProjection)
                     
                     if np.any (slice2DMask)  :           
                          addSliceData (slice2DMask, XMatrix, YMatrix, ZMatrix, ROIProjection, RoundVals = True, cY= cYMatrix, cZ=cZMatrix, cMask=cSlice2DMask, rX=rXMatrix, rZ=rZMatrix, rMask=rSlice2DMask) 
                          addSliceData (slice2DMask, XMatrix, YMatrix, ZMatrix, NiftiProjection)
                     
                        
                     self._projectedVoxels = voxelProjection
                     self._projectionMatrix = {}
                     self._projectionMatrix["Projection"] = projection
                     self._projectionMatrix["NIfTI"] = NiftiProjection #ComputeProjection (NiftiProjection)
                     self._projectionMatrix["ROI"] = ROIProjection
                     offset = int (ROIProjection["ZProjection"][0])
                     if offset < 0 :
                        offset = -offset
                     maxoffset = int (ROIProjection["ZProjection"][1])                     
                     self._LoadedNiftiVolumeMem = NIfTIVolume
                     self._ZAxisPadding = [offset,maxoffset]        
                     self._initAxisCache ()
                       
                     
         except:             
             self.resetAxisRotation ()
         finally: 
            return self._projectionMatrix,  self._projectedVoxels
    

     
     def getProjectionAxisZeroOffset (self) :
         return self._ZAxisPadding[0]
     
     def getProjectionAxisPadding (self) :
         return self._ZAxisPadding[1]
     
       
     def _initAxisCache (self) :
         self.clearNiftiSliceAxisCache ()         
         NIfTIVolume = self._LoadedNiftiVolumeMem
         if NIfTIVolume is None :
             self._roiSliceAxisCacheValid  = None
         else:
             _,_, zDim = NIfTIVolume.getSliceDimensions ()
             zDim += self.getProjectionAxisZeroOffset () + self.getProjectionAxisPadding ()
             try :
                 if self._roiSliceAxisCacheValid is not None and self._roiSliceAxisCacheValid.shape[0] == zDim :
                     self._roiSliceAxisCacheValid[:] = False
                 else:
                     self._roiSliceAxisCacheValid = np.zeros ((zDim,), dtype=np.bool)
             except:
                 self._roiSliceAxisCacheValid = np.zeros ((zDim,), dtype=np.bool)
         self._coordinateCache = None
         self._roiSliceAxisCache = None
         self._roiSliceAxisObjectROIIDtoAxisCacheIDConversion = {}
         self._individualROIProjectionCache = {}
         self._roiSliceAxisCacheColorTable = None
         self._ROIProjectionAxis = None         
         self._InvalidateROIProjectionAxis = set ()
         self._axisprojectionstate += 1
         
     def _setTransformedNiftiAxisCache (self, Cache, sliceIndex, SliceData) :
          sliceIndex += self.getProjectionAxisZeroOffset ()
          Cache[sliceIndex] = SliceData
         
     def _getTransformedNiftiAxisCache (self, Cache, sliceIndex) :
         try :
             sliceIndex += self.getProjectionAxisZeroOffset ()
             if sliceIndex in Cache :
                 return Cache[sliceIndex]
             return None
         except:
             return None
       
     def clearNiftiSliceAxisCache (self) :
         self._niftiSliceAxisCache = {}
         self._maskSliceAxisCache = {}         
         
     @staticmethod
     def getProjectionSliceList (sliceList, upperB, lowerB, ZDim,ProjectionAxisOffset) :
             ProjectionSliceList = []
             lastSliceIndex = -1
             ZDim -= 1 
             for key in sliceList  :
                 bottom = min (key + upperB, ZDim)
                 if bottom <= lastSliceIndex :
                     continue
                 top    = key + lowerB + ProjectionAxisOffset    
                 top = max (top, lastSliceIndex + 1)                 
                 lastSliceIndex = bottom                                
                 for si in range (int (top), int (bottom+1)) :
                     ProjectionSliceList.append (si)
             return ProjectionSliceList
         
     def invalidateObjectSliceCache (self,  ROIIDNumber = None,  SliceIndex = None) :         
         if ROIIDNumber is None :
            self.invalidateSliceCache () 
         else:
            if SliceIndex is not None :
                SliceIndex += self.getProjectionAxisZeroOffset ()                
            if ROIIDNumber in self._roiSliceAxisObjectROIIDtoAxisCacheIDConversion and self._roiSliceAxisCacheValid is not None and self._roiSliceAxisCache is not None:
                chval = self._roiSliceAxisObjectROIIDtoAxisCacheIDConversion[ROIIDNumber]
                if SliceIndex is not None :
                    if (np.any (self._roiSliceAxisCache[SliceIndex,...]==chval)) :
                        self._InvalidateROIProjectionAxis.add (SliceIndex)
                else:
                    invalidSlices = np.any (self._roiSliceAxisCache == chval, axis = (1,2))
                    invalidSlices = invalidSlices.nonzero ()[0]
                    self._roiSliceAxisCacheValid[invalidSlices] = False
                    for ind in invalidSlices :
                        self._InvalidateROIProjectionAxis.add (ind)
                    #self._roiSliceAxisCache[invalidSlices,...] = 0
            if ROIIDNumber in self._individualROIProjectionCache :
                if self._roiSliceAxisCacheValid is not None :
                    objCache = self._individualROIProjectionCache[ROIIDNumber]
                    validSlices = objCache["ValidSlices"]         
                    if SliceIndex is not None :
                        if validSlices[SliceIndex] :
                            if np.any (objCache["Volume"][SliceIndex,...]) :
                                self._InvalidateROIProjectionAxis.add (SliceIndex)
                            validSlices[SliceIndex] = False
                    else:
                        nonzeroVolume = np.any (objCache["Volume"], axis = (1,2))
                        invalidSlices = (np.logical_and (validSlices, nonzeroVolume)).nonzero ()[0]
                        for ind in invalidSlices :
                            self._InvalidateROIProjectionAxis.add (ind)
                        validSlices[...] = False
                        #objCache["Volume"][invalidSlices,...] = 0
        
     
     def invalidateSliceCache (self) :         
            if self._roiSliceAxisCacheValid is not None :
                self._roiSliceAxisCacheValid[...] = False
                #self._roiSliceAxisCache[...] = 0
            self._individualROIProjectionCache = {}
            self._ROIProjectionAxis = None
             
     def invalidateSliceAxisCacheProjectedSlices (self, InvalidateSliceList, ROIIDNumber = None) :                 
         invalidateIndex = np.array (InvalidateSliceList) + self.getProjectionAxisZeroOffset ()
         if self._roiSliceAxisCacheValid is not None :
            self._roiSliceAxisCacheValid[invalidateIndex] = False
         if ROIIDNumber is None :
             for roiobj in self._individualROIProjectionCache.values () :
                 roiobj["ValidSlices"][invalidateIndex] = False
         elif ROIIDNumber in self._individualROIProjectionCache :
             self._individualROIProjectionCache[ROIIDNumber]["ValidSlices"][invalidateIndex] = False                     
         for index in invalidateIndex :
             self._InvalidateROIProjectionAxis.add (index)
     
                                   
     
     def _markOnInAxisSliceCache (self,SliceNumberPlusOffset, ROIID) :         
         if self._ROIProjectionAxis is None :
             self._ROIProjectionAxis = {}
             self._ROIProjectionAxis[SliceNumberPlusOffset] = set ()
         elif SliceNumberPlusOffset not in self._ROIProjectionAxis :
              self._ROIProjectionAxis[SliceNumberPlusOffset] = set ()
         self._ROIProjectionAxis[SliceNumberPlusOffset].add(ROIID)
         
     def _cacheProjectedSlice (self, SliceNumberPlusOffset, ReturnMem, ROIIDNumber = None, ROIIDColorMaskIDConversionDict = None, ColorTable = None, MaxInt = None, ZeroMem = None) :
         if ROIIDNumber is None :
             self._roiSliceAxisObjectROIIDtoAxisCacheIDConversion = ROIIDColorMaskIDConversionDict
             self._roiSliceAxisCacheColorTable = ColorTable 
             if self._roiSliceAxisCache is None :
                 CacheShape = list (self._roiSliceAxisCacheValid.shape)+ list (ReturnMem.shape)
                 self._roiSliceAxisCache = np.zeros (CacheShape, dtype = ReturnMem.dtype)            
             self._roiSliceAxisCache[SliceNumberPlusOffset,...] = ReturnMem
             self._roiSliceAxisCacheValid[SliceNumberPlusOffset] = True
             self._individualROIProjectionCache = {}
             unqiueColorMasks = FastUnique.unique (ReturnMem, MaxInt= MaxInt, ZeroMem=ZeroMem)
             for key, value in ROIIDColorMaskIDConversionDict.items ():
                 if value in unqiueColorMasks :
                     self._markOnInAxisSliceCache (SliceNumberPlusOffset, key)
                      
         elif not self._roiSliceAxisCacheValid[SliceNumberPlusOffset] :
             CacheShape = tuple (list (self._roiSliceAxisCacheValid.shape)+ list (ReturnMem.shape))
             if ROIIDNumber not in self._individualROIProjectionCache or self._individualROIProjectionCache[ROIIDNumber]["Volume"].shape != CacheShape :
                self._individualROIProjectionCache[ROIIDNumber] = {}
                self._individualROIProjectionCache[ROIIDNumber]["ValidSlices"]  = np.zeros (self._roiSliceAxisCacheValid.shape, dtype=np.bool)
                self._individualROIProjectionCache[ROIIDNumber]["Volume"] = np.zeros (CacheShape, dtype=np.uint8)
             roiobj = self._individualROIProjectionCache[ROIIDNumber]
             roiobj["ValidSlices"][SliceNumberPlusOffset] = True
             roiobj["Volume"][SliceNumberPlusOffset,...] =  ReturnMem             
             if np.any (ReturnMem) :
                 self._markOnInAxisSliceCache (SliceNumberPlusOffset, ROIIDNumber)
       
     def getROIProjectionMask (self, niftiMem) :
         SliceData, _ = self.getAxialViewProjection (niftiMem)
         SliceData = SliceData["ROI"]
         projectionMask = SliceData["Mask"] 
         return projectionMask
     
        
     def _getXYZProj (self, coordinate, niftiMem, ZOffsetOnly = False, xc1 = None, yc1 = None):
         xc, yc, _ = coordinate.getCoordinate ()
         xc, yc = float (xc), float (yc) 

         testcache = (xc, yc)
         if self._coordinateCache is not None and testcache in self._coordinateCache  :
             coordinateCacheValue = self._coordinateCache[testcache]
             if ZOffsetOnly :
                 return coordinateCacheValue[0]
             return coordinateCacheValue[1], coordinateCacheValue[2]

         SliceData, _ = self.getAxialViewProjection (niftiMem)                
         roiSliceData = SliceData["ROI"]
         tempMem = roiSliceData["MaskSelMem"]
         if xc1 is None or yc1 is None :             
             #Projection = SliceData["Projection"]            
             #matrix =  Projection._TransformMatrix             
             
             xdim, ydim, _ = niftiMem.getSliceDimensions ()
             center =  np.array ([float (ydim), float (xdim)])    
             center -= 1.0
             center /= 2.0
             pt  = np.array ([float (yc), float (xc)])     
             pt = (pt - center) / center
             #reverse = np.linalg.inv (matrix[0:2,0:2])
             #xc1 = np.sum (reverse[0,:] * pt)
             #yc1 = np.sum (reverse[1,:] * pt)
             
             """xdim, ydim, _ = niftiMem.getSliceDimensions ()
             pt  = np.array ([yc  - float (ydim-1) / 2.0,xc - float (xdim-1) / 2.0, 0.0])              
             X = matrix[0, :]
             Y = matrix[1, :]
             X = X / np.sqrt(np.sum (X**2))
             Y = Y / np.sqrt(np.sum (Y**2))
             Normal = np.cross (X, Y)
             Zoffset = (Normal[0] * pt[0] + Normal[1]*pt[1]) / -Normal[2]
             pt[2] = Zoffset         
             xc1 = np.sum (X * pt)
             yc1 = np.sum (Y * pt)
                    """             
             maskShape = tempMem.shape
             maskCenter = np.array ([ float (maskShape[1]), float (maskShape[0])])
             maskCenter -= 1.0
             maskCenter /= 2.0
             maskPoint = np.round ((pt * maskCenter) + maskCenter)
             yc1,xc1 =  maskPoint[0], maskPoint[1]
             #xc1, yc1 = int (np.round (xc1+xCenter)), int (np.round (yc1+ yCenter) )
             xc1 = int (max(min (xc1, maskShape[0]-1), 0))
             yc1 = int (max(min (yc1, maskShape[1]-1), 0))
         
         sel = tempMem[xc1,yc1]
         positionData = roiSliceData["XYZPos"]
         Zoffset = positionData[2,sel]
         if self._coordinateCache is None :
             self._coordinateCache = {}
         #else:
         #    print (("Setting Coord Cache", testcache, (Zoffset, xc1, yc1)))
         self._coordinateCache[testcache] = (Zoffset, xc1, yc1)     
         
       
         if ZOffsetOnly :
             return Zoffset
         else:
             return xc1, yc1
         
       
     
     def getWorldCoordinateForPlaneCoordinate (self, pX, pY, PlaneSliceNumber, NiftiMem):        
         SliceData, _ = self.getAxialViewProjection (NiftiMem)                
         SliceData = SliceData["ROI"]
         tempMem = SliceData["MaskSelMem"]
         sel = tempMem[pX,pY]
         positionData = SliceData["XYZPos"]
         cordMem = np.zeros ((3,), dtype= np.int)
         cordMem[0] = positionData[0,sel]
         cordMem[1] = positionData[1,sel]
         SOffset = positionData[2,sel]
         cordMem[2] = SOffset+ PlaneSliceNumber
         #print (("GetWorldCordSetting Coord Cache", (cordMem[0], cordMem[1]),  (SOffset, pX, pY)  ))
         if self._coordinateCache is None :
             self._coordinateCache = {}
         self._coordinateCache[(cordMem[0], cordMem[1])] = (SOffset, pX, pY)         
         #temp = Coordinate ()
         #temp.setCoordinate (cordMem)
         #newprojectedSlice = self.getProjectedSliceForWorldCoordinate (temp, NiftiMem, xc1 = pX, yc1 = pY)
         #cordMem[2]  += PlaneSliceNumber -newprojectedSlice
         return cordMem
         
     def getProjectedPlaneXYCoordinateFromWC  (self, coordinate, niftiMem):        
        if not self.isAxisRotated () or niftiMem is None :
            self._coordinateCache = None
            xc, yc, _ = coordinate.getCoordinate ()
            xc, yc = int (xc), int (yc)         
            return xc, yc            
        xc1, yc1 = self._getXYZProj (coordinate, niftiMem, ZOffsetOnly = False)                    
        return xc1, yc1
    
     def getProjectedSliceForWorldCoordinate (self, coordinate, niftiMem, xc1 = None, yc1 = None):         
        zc = int (coordinate.getZCoordinate ())
        if not self.isAxisRotated () or niftiMem is None :
            self._coordinateCache = None
            return zc                
        Zoffset = self._getXYZProj (coordinate, niftiMem, ZOffsetOnly = True, xc1 = xc1, yc1 = yc1)
        return int (zc  - Zoffset)             
        
     def _ClipROIProjection (self, roiProjectionAxis, Coordinate = None) :
         if  roiProjectionAxis is None or len (roiProjectionAxis) == 0:
             return roiProjectionAxis
         clippedDict =  {}
         MaxDim = None         
         if self._LoadedNiftiVolumeMem is not None :
             try :
                 MaxDim = self._LoadedNiftiVolumeMem.getSliceDimensions()[2]                          
             except:
                 MaxDim = None
         zc = Coordinate.getZCoordinate ()
         zc_corrected = self.getProjectedSliceForWorldCoordinate (Coordinate,self._LoadedNiftiVolumeMem) 
         adjustment = zc - zc_corrected
         #print (("adjustment: %d" %adjustment, list (roiProjectionAxis.keys ())))
         #print ("Clip ROI Projection")
         #print ((Coordinate.getCoordinate (), zc_corrected, adjustment))
         #print (list (roiProjectionAxis.keys ()))
         #print ("")
         zeroAxisOffset = self.getProjectionAxisZeroOffset ()
         if MaxDim is not None :
             for sliceIndex, sliceObjSet in roiProjectionAxis.items () :   
                 sliceIndex -= zeroAxisOffset 
                 sliceIndex += adjustment
                 if sliceIndex >= 0 and  sliceIndex < MaxDim :                 
                     clippedDict[sliceIndex] = sliceObjSet
         else:
             for sliceIndex, sliceObjSet in roiProjectionAxis.items () : 
                 sliceIndex -= zeroAxisOffset 
                 sliceIndex += adjustment 
                 if sliceIndex >= 0 :                 
                     clippedDict[sliceIndex] = sliceObjSet
         return clippedDict
          
     def getROIProjectionSliceAxis (self, niftiMem, ROIDictionary, Coordinate) :                                        
         if self._ROIProjectionAxis is not None and len (self._InvalidateROIProjectionAxis) <= 0 :
             return self._ClipROIProjection (self._ROIProjectionAxis, Coordinate=Coordinate)         
         xAxisSlope = self._xAxis.getSlope ()
         yAxisSlope = self._yAxis.getSlope ()
         sliceDictionary = ROIDictionary.getROISliceDictionary ()             
         if xAxisSlope == 0 and yAxisSlope == 0 :
             self._ROIProjectionAxis = {}             
             for index, objlist in sliceDictionary.items ():
                 self._ROIProjectionAxis[index] = set ()
                 for obj in objlist :
                     self._ROIProjectionAxis[index].add (obj.getROIDefIDNumber ()) 
             if len (self._InvalidateROIProjectionAxis) > 0 :
                 self._InvalidateROIProjectionAxis = set ()        
             return self._ROIProjectionAxis
         SliceData, _ = self.getAxialViewProjection (niftiMem)
         SliceData = SliceData["ROI"]       
         projectionMask = SliceData["Mask"] 
         xyzPositions = SliceData["XYZPos"]
         lowerB, upperB = SliceData["ZProjection"]        
         ColorTable = None 
         ZPos = xyzPositions[2,:] 
         XPos = xyzPositions[0,:]
         YPos = xyzPositions[1,:]
         ZDim = niftiMem.getSliceDimensions()[2]                          
         sharedMem = ROIDictionary.getSharedVolumeCacheMem ()        
         
         ROISliceList = None
         SingleAxisProjection= (xAxisSlope == 0 or yAxisSlope == 0) and (xAxisSlope != 0 or yAxisSlope != 0)
         if self._ROIProjectionAxis is None :
             self._InvalidateROIProjectionAxis = None
             self._ROIProjectionAxis = {}
             ROISliceList = sorted(list (sliceDictionary.keys ()))
         
         GlobalVolumeMem = None
         if sharedMem is not None and not SingleAxisProjection :                         
             GlobalVolumeMem, ColorTable, ROIIDColorMaskIDConversionDict = sharedMem.getRawROIMaskVolume () 
             ROIIDNumber = None
             if ROISliceList is None :
                 ROISliceList = sorted(list (sliceDictionary.keys ()))
             ReturnMem = np.zeros (projectionMask.shape, dtype = np.int)           
             if ColorTable is not None :
                 MaxInt = ColorTable.shape[1]
                 ZeroMem = np.zeros ((MaxInt,), dtype=np.bool)                                 
                
         def isAnyContored (sliceIndex, lowerB, upperB, ZDim) :
             ZDim -= 1
             li = sliceIndex + lowerB
             if li >= 0 and li <= ZDim :
                 return True
             ui = sliceIndex + upperB
             if ui >= 0 and ui <= ZDim :
                 return True
             if li < 0 and ui > ZDim :
                 return True
             return False
             
             
         if SingleAxisProjection :
             if xAxisSlope == 0 :
                planePos = SliceData["RowPos"]
             else:
                planePos = SliceData["ColumnPos"]
             ZPos = planePos[1,:] 
             XPos = planePos[0,:]
             NameList = ROIDictionary.getDefinedROINames ()
             ZClip = None
             InitalizedSlice = set ()
             projectionAxisZOffset  =self.getProjectionAxisZeroOffset ()
             for roiName in NameList :
                 obj = ROIDictionary.getROIObject (roiName)  
                 if obj.isROIArea () :
                    ObjProjectionCache = None
                    for si in AxialAxisProjectionControls.getProjectionSliceList (obj.getSliceNumberList (), upperB, lowerB, ZDim, projectionAxisZOffset) :
                        if self._InvalidateROIProjectionAxis is None or si in self._InvalidateROIProjectionAxis :
                            if si not in InitalizedSlice :
                                InitalizedSlice.add (si)
                                if si in self._ROIProjectionAxis :
                                    del self._ROIProjectionAxis[si]
                            
                            drawIndex = si - projectionAxisZOffset
                            if isAnyContored (drawIndex, lowerB, upperB, ZDim) :
                                ZCord = ZPos + drawIndex
                                if drawIndex + lowerB >= 0 and drawIndex + upperB < ZDim :
                                    Coords = [XPos, ZCord]
                                else:
                                    ZClip = np.logical_and (ZCord >= 0, ZCord < ZDim, out = ZClip)    
                                    Coords = [XPos[ZClip], ZCord[ZClip]]
                                if ObjProjectionCache is None :
                                    objVol = obj.getMaskedVolumeFromMemCache (SharedROIMask=sharedMem)
                                    if xAxisSlope == 0 :
                                         ObjProjection = np.any(objVol,axis= 1) 
                                    else:
                                         ObjProjection = np.any(objVol,axis= 0) 
                                    ROIIDNumber = obj.getROIDefIDNumber ()   
                                    ObjProjectionCache = InitMapCoordMem (ObjProjection)
                                if MapCoordinateWrapper(ObjProjectionCache, InitMapCoordMem (Coords), order=0, mode='constant', cval=0.0, prefilter=False, AppyAny = True) :
                                    self._markOnInAxisSliceCache (si, ROIIDNumber)                                
                                    #print (("On axis slice: ", si))
                    del ObjProjectionCache
         else:
             projectionAxisZOffset  =self.getProjectionAxisZeroOffset ()
             ZClip = None                 
             if GlobalVolumeMem is not None :                     
                 GlobalVolumeMemCache = None
                 for si in AxialAxisProjectionControls.getProjectionSliceList (ROISliceList, upperB, lowerB, ZDim, projectionAxisZOffset)  :                           
                     if self._InvalidateROIProjectionAxis is None or si in self._InvalidateROIProjectionAxis :
                         if si in self._ROIProjectionAxis :
                             del self._ROIProjectionAxis[si]
                         drawIndex = si - projectionAxisZOffset
                         if isAnyContored (drawIndex, lowerB, upperB, ZDim) :
                             ZCord = ZPos + drawIndex
                             if drawIndex + lowerB >= 0 and drawIndex + upperB < ZDim :
                                 Coords = [XPos, YPos, ZCord]
                                 ClipMask = projectionMask
                             else:
                                 ZClip = np.logical_and (ZCord >= 0, ZCord < ZDim, out = ZClip)    
                                 Coords = [XPos[ZClip], YPos[ZClip], ZCord[ZClip]]
                                 ClipMask = np.copy(projectionMask)                 
                                 ClipMask[ClipMask == 1] = ZClip                                            
                             if GlobalVolumeMemCache is None :
                                GlobalVolumeMemCache = InitMapCoordMem (GlobalVolumeMem)
                             ReturnMem[ClipMask] = MapCoordinateWrapper(GlobalVolumeMemCache, InitMapCoordMem(Coords), order=0, mode='constant', cval=0.0, prefilter=False)                                                              
                             self._cacheProjectedSlice (si, ReturnMem, ROIIDNumber, ROIIDColorMaskIDConversionDict, ColorTable, MaxInt = MaxInt, ZeroMem = ZeroMem)
                             ReturnMem[...] = 0   
                 del GlobalVolumeMemCache                                       
             else:   
                InitalizedSlice = set ()
                NameList = ROIDictionary.getDefinedROINames ()
                for roiName in NameList :
                    obj = ROIDictionary.getROIObject (roiName)  
                    if obj.isROIArea () :
                        objVol = obj.getMaskedVolumeFromMemCache (SharedROIMask=sharedMem)
                        objVolCache = None
                        ROIIDNumber = obj.getROIDefIDNumber ()                            
                        for si in AxialAxisProjectionControls.getProjectionSliceList (obj.getSliceNumberList (), upperB, lowerB, ZDim, projectionAxisZOffset) :
                            if self._InvalidateROIProjectionAxis is None or si in self._InvalidateROIProjectionAxis :
                                if si not in InitalizedSlice :
                                    InitalizedSlice.add (si)
                                    if si in self._ROIProjectionAxis :
                                        del self._ROIProjectionAxis[si]
                                drawIndex = si - projectionAxisZOffset
                                if isAnyContored (drawIndex, lowerB, upperB, ZDim) :
                                    ZCord = ZPos + drawIndex
                                    if drawIndex + lowerB >= 0 and drawIndex + upperB < ZDim :
                                         Coords = [XPos, YPos, ZCord]
                                         ClipMask = projectionMask
                                    else:
                                         ZClip = np.logical_and (ZCord >= 0, ZCord < ZDim, out = ZClip)    
                                         Coords = [XPos[ZClip], YPos[ZClip], ZCord[ZClip]]
                                         ClipMask = np.copy(projectionMask)                 
                                         ClipMask[ClipMask == 1] = ZClip         
                                    if objVolCache is None :
                                        objVolCache = InitMapCoordMem (objVol)
                                    ReturnMem[ClipMask] = MapCoordinateWrapper(objVolCache, InitMapCoordMem(Coords), order=0, mode='constant', cval=0.0, prefilter=False)                                                                                  
                                    self._cacheProjectedSlice (si, ReturnMem, ROIIDNumber, ROIIDColorMaskIDConversionDict, ColorTable)
                                    ReturnMem[...] = 0       
                        del objVolCache    
         self._InvalidateROIProjectionAxis = set ()                                                         
         return self._ClipROIProjection (self._ROIProjectionAxis, Coordinate=Coordinate)         
             
     
        
     def getNIfTIProjection (self, coordinate, niftiMem, Mask = None, UseCache = True, SliceIndex = None, ArrayMem = None) :
         def getRAWSlice (coordinate, niftiMem, Mask, SliceIndex, ArrayMem):
             if Mask is None :
                 volume = niftiMem 
             else:
                 volume = Mask
             if SliceIndex is None :
                 SliceIndex = coordinate.getZCoordinate ()
                 
             SliceIndex = int (max(min (SliceIndex, volume.getZDimSliceCount () - 1),0))
             if ArrayMem is not None :
                 return ArrayMem[..., SliceIndex]
             return  volume.getImageData (Z=SliceIndex)
         
         if not self.isAxisTransformationEnabled () or not self.isAxisRotated ():
             return getRAWSlice (coordinate, niftiMem, Mask, SliceIndex, ArrayMem), None
         SliceData, _ = self.getAxialViewProjection (niftiMem)
         if SliceData is None or len (SliceData["NIfTI"]) == 0 :
             return getRAWSlice (coordinate, niftiMem, Mask, SliceIndex, ArrayMem), None
         else:
            if SliceIndex is not None :
                coordinate = coordinate.copy ()
                coordinate.setZCoordinate (SliceIndex)
            selectedZ = self.getProjectedSliceForWorldCoordinate (coordinate, niftiMem)
            if selectedZ is None :
                return None, self.getROIProjectionMask (niftiMem)
         if UseCache :                         
             if Mask is not None :
                 if "img" not in self._maskSliceAxisCache or self._maskSliceAxisCache["img"] is not Mask :
                     self._maskSliceAxisCache = {}
                     self._maskSliceAxisCache["img"] = Mask                 
                 SliceAxisNiftiCache = self._maskSliceAxisCache             
             else:
                 if "img" not in self._niftiSliceAxisCache or self._niftiSliceAxisCache["img"] is not niftiMem :
                     self._niftiSliceAxisCache = {}
                     self._niftiSliceAxisCache["img"] = niftiMem                 
             SliceAxisNiftiCache = self._niftiSliceAxisCache             
             cacheResult = self._getTransformedNiftiAxisCache (SliceAxisNiftiCache, selectedZ)
             if cacheResult is not None :                 
                 ReturnMem, ClipMask = cacheResult
                 return ReturnMem, ClipMask         
         if Mask is not None :
             SliceData = SliceData["ROI"]
             order = 0
         else:
             SliceData = SliceData["NIfTI"]
             order = 1
         projectionMask = SliceData["Mask"] 
         xyzPositions = SliceData["XYZPos"] 
        
         if ArrayMem is not None :
            SliceMem = ArrayMem
         elif Mask is not None :
            SliceMem = Mask.getImageData ()
         else: 
            SliceMem = niftiMem.getImageData ()
         if self._MapCordMemCacheObj is not  SliceMem :
             self._MapCordMemCache = InitMapCoordMem(SliceMem)
             self._MapCordMemCacheObj = SliceMem
         ReturnMem = np.zeros (projectionMask.shape, dtype = SliceMem.dtype)
         ZPos = xyzPositions[2,:] + selectedZ
         lowerB, upperB = SliceData["ZProjection"]    
         ZDim = SliceMem.shape[2]-1
         if selectedZ + lowerB >= 0 and selectedZ + upperB <= ZDim :
            ZClip = None
         else:
            ZClip = np.logical_and (ZPos >= 0, ZPos <= ZDim)
            ZPos = ZPos[ZClip]        
            
         ClipMask = np.copy(projectionMask)
         if ZClip is not None :
            XPos = xyzPositions[0][ZClip]
            YPos = xyzPositions[1][ZClip]                        
            ClipMask[ClipMask == 1] = ZClip
         else:
            XPos = xyzPositions[0]
            YPos = xyzPositions[1]              
         ReturnMem[ClipMask] = MapCoordinateWrapper( self._MapCordMemCache, InitMapCoordMem([XPos,YPos, ZPos]), order=order, mode='constant', cval=0.0, prefilter=False)
         if UseCache :
             self._setTransformedNiftiAxisCache (SliceAxisNiftiCache, selectedZ, (ReturnMem, ClipMask))
         return ReturnMem, ClipMask
     
     def setNIfTISliceChangeMasks (self, sliceView, ChangeMask, crossSliceMap, SliceIndex) :
         niftiMem = sliceView.getNIfTIVolume()
         SliceData, _ = self.getAxialViewProjection (niftiMem)
         SliceData = SliceData["ROI"]
         projectionMask = SliceData["Mask"] 
         lowB, uperB = SliceData["ZProjection"]     
         xyzPositions = SliceData["XYZPos"] 
         zSelected = ChangeMask[projectionMask]
         zSliceValues=crossSliceMap[projectionMask]
         zSelected = zSelected != 0
         zSliceValues = zSliceValues[zSelected]
         #slicesChanged = np.expand_dims(zSelected,axis=0)
         #slicesChanged = np.stack ([zSelected,zSelected,zSelected])
         slicesChanged = (zSelected).nonzero()[0]
         #pos = (xyzPositions[slicesChanged]).astype (np.int)
         XCord = (xyzPositions[0,slicesChanged]).astype (np.int)
         YCord = (xyzPositions[1,slicesChanged]).astype (np.int)
         ZCord = (xyzPositions[2,slicesChanged]).astype (np.int)
         
         #import matplotlib.pyplot as plt
         
         #def showimg (img):
         #   plt.imshow (img); plt.show ()
         
         baseSliceIndex = sliceView.getAxisProjectionSliceNumber (SliceNumber = SliceIndex)
         #print ("Shown Index: %d" %  baseSliceIndex)
         
         selectedIndices = ZCord - lowB
         ZSlicesChanged = FastUnique.unique (selectedIndices, MaxInt= int(uperB - lowB + 1))
         ZSlicesChanged = ZSlicesChanged.astype(np.int)
         ZSlicesChanged += int (lowB) + int (baseSliceIndex)
         VolumeSliceDims = niftiMem.getSliceDimensions ()
         ZDim = VolumeSliceDims[2]
         validChangedIndex = np.logical_and (ZSlicesChanged >= 0, ZSlicesChanged < ZDim)
         sliceList = ZSlicesChanged[validChangedIndex]
         oldMem = None
         for zChangeSliceIndex in sliceList :
                ZSel = ZCord == int (zChangeSliceIndex - baseSliceIndex)
                xc = XCord[ZSel]
                yc = YCord[ZSel]                
                SliceMem = niftiMem.getImageData (Z = zChangeSliceIndex)
                if oldMem is None :
                    oldMem = np.copy (SliceMem)
                else:
                    oldMem[...] = SliceMem[...]
                SliceMem[xc,yc] = zSliceValues[ZSel]
                SetMask = oldMem != SliceMem
                if np.any (SetMask) :
                    niftiMem.setSliceData (zChangeSliceIndex, SliceMem, SetMask = SetMask, Axis="Z") 
         baseSliceIndex += self.getProjectionAxisZeroOffset ()
         if baseSliceIndex in self._niftiSliceAxisCache :
            del self._niftiSliceAxisCache[baseSliceIndex]
         
                
     def setROISliceChangeMasks (self, sliceView, ChangeMask, crossSliceMap, obj, contourID, ContourType, SliceIndex = None, Coordinate = None) :
         niftiMem = sliceView.getNIfTIVolume()
         SliceData, _ = self.getAxialViewProjection (niftiMem)
         SliceData = SliceData["ROI"]
         projectionMask = SliceData["Mask"] 
         lowB, uperB = SliceData["ZProjection"]     
         xyzPositions = SliceData["XYZPos"] 
         zSelected = ChangeMask[projectionMask]
         zSliceValues=crossSliceMap[projectionMask]
         zSelected = zSelected != 0
         zSliceValues = zSliceValues[zSelected]
         #slicesChanged = np.expand_dims(zSelected,axis=0)
         #slicesChanged = np.stack ([zSelected,zSelected,zSelected])
         slicesChanged = (zSelected).nonzero()[0]
         #pos = (xyzPositions[slicesChanged]).astype (np.int)
         XCord = (xyzPositions[0,slicesChanged]).astype (np.int)
         YCord = (xyzPositions[1,slicesChanged]).astype (np.int)
         ZCord = (xyzPositions[2,slicesChanged]).astype (np.int)
         
         #import matplotlib.pyplot as plt
         
         #def showimg (img):
         #   plt.imshow (img); plt.show ()
         
         baseSliceIndex = sliceView.getAxisProjectionSliceNumber (SliceNumber = SliceIndex, Coordinate = Coordinate)
         #print ("Shown Index: %d" %  baseSliceIndex)
         
         selectedIndices = ZCord - lowB
         ZSlicesChanged = FastUnique.unique (selectedIndices, MaxInt= int(uperB - lowB + 1))
         ZSlicesChanged = ZSlicesChanged.astype(np.int)
         ZSlicesChanged += int (lowB) + int (baseSliceIndex)
         VolumeSliceDims = niftiMem.getSliceDimensions ()
         ZDim = VolumeSliceDims[2]
         validChangedIndex = np.logical_and (ZSlicesChanged >= 0, ZSlicesChanged < ZDim)
         ROIIDNumber = obj.getROIDefIDNumber ()
         sliceList = ZSlicesChanged[validChangedIndex]
         oldMem = None
         for zChangeSliceIndex in sliceList :
                ZSel = ZCord == int (zChangeSliceIndex - baseSliceIndex)
                xc = XCord[ZSel]
                yc = YCord[ZSel]                
                SliceMem = obj.getSliceViewObjectMask (sliceView, SliceNumber = zChangeSliceIndex, ClipOverlayingROI = False, DisableAxisProjection = True)
                if oldMem is None :
                    oldMem = np.copy (SliceMem)
                else:
                    oldMem[...] = SliceMem[...]
                SliceMem[xc,yc] = zSliceValues[ZSel]
                if np.any (oldMem !=  SliceMem) :
                    obj.setSliceDataForCrossSliceChange (SliceMem.shape, contourID, zChangeSliceIndex, SliceMem, X = None, Y=None, ContourType =ContourType)
         obj.adjustBoundingBoxToIncludeSliceList (sliceList, VolumeSliceDims)
         self.invalidateSliceAxisCacheProjectedSlices ([baseSliceIndex], ROIIDNumber = ROIIDNumber)
                
    
     def clearROISliceChangeMasks (self, sliceView, obj, ContourType, SliceIndexList) :
         ObjectChanged = False                 
         ROIIDNumber = obj.getROIDefIDNumber ()
         #sliceIndexChanged = set ()
         deletedSlices = []
         validChangedIndex = None
         niftiMem = sliceView.getNIfTIVolume()
         SliceData, _ = self.getAxialViewProjection (niftiMem)
         SliceData = SliceData["ROI"]
         lowB, uperB = SliceData["ZProjection"]     
         xyzPositions = SliceData["XYZPos"] 
         XCord = (xyzPositions[0,:]).astype (np.int)
         YCord = (xyzPositions[1,:]).astype (np.int)
         ZCord = (xyzPositions[2,:]).astype (np.int)
         selectedIndices = ZCord - lowB
         UniqueZSlices = FastUnique.unique (selectedIndices, MaxInt= int(uperB - lowB + 1))
         UniqueZSlices = UniqueZSlices.astype(np.int)
         VolumeSliceDims = niftiMem.getSliceDimensions ()
         ZDim = VolumeSliceDims[2]
         for SliceIndex in SliceIndexList :
             baseSliceIndexAdded = False
             baseSliceIndex = sliceView.getAxisProjectionSliceNumber (SliceNumber = SliceIndex)             
             ZSlicesChanged = UniqueZSlices + int (lowB) + int (baseSliceIndex)    
             validChangedIndex = np.logical_and (ZSlicesChanged >= 0, ZSlicesChanged < ZDim, out = validChangedIndex)
             sliceList = ZSlicesChanged[validChangedIndex]
             for zChangeSliceIndex in sliceList :
                    if obj.hasSlice (zChangeSliceIndex) :                        
                        contourIDList = obj.getSliceContourIDList (zChangeSliceIndex)
                        if contourIDList is not None and len (contourIDList) > 0:
                            ZSel = ZCord == int (zChangeSliceIndex - baseSliceIndex)
                            xc = XCord[ZSel]
                            yc = YCord[ZSel]                
                            for contourID in contourIDList :                                                            
                                SliceMem = obj.getSliceViewObjectMask (sliceView, SliceNumber = zChangeSliceIndex, ClipOverlayingROI = False, DisableAxisProjection = True)
                                if np.any (SliceMem[xc,yc]) :
                                    if not baseSliceIndexAdded :
                                        deletedSlices.append (baseSliceIndex)
                                        baseSliceIndexAdded = True
                                    SliceMem[xc,yc] = 0
                                    obj.setSliceDataForCrossSliceChange (SliceMem.shape, contourID, zChangeSliceIndex, SliceMem, X = None, Y=None, ContourType =ContourType)
                                    #sliceIndexChanged.add (zChangeSliceIndex)
                                    ObjectChanged = True
         if len (deletedSlices) > 0 :
             self.invalidateSliceAxisCacheProjectedSlices (deletedSlices, ROIIDNumber = ROIIDNumber)
         return ObjectChanged
     
    
             
         
     def transformROIDataFromVolumeToProjection (self, niftiMem, SliceNumber, GetVolumeFunc,  ROIIDNumber = None, HideObjects = None, ClipROIObjectList = None, DisableProjectionSliceCache = False) :
        
        def clipReturnMem (ReturnMem, ClipROIObjectList, niftiMem, ObjectOutputMaskKey) :
            if ClipROIObjectList is None or len (ClipROIObjectList) <= 0 :
                return ReturnMem 
            clipObjectMasks =  {}
            SliceData, _ = self.getAxialViewProjection (niftiMem)
            SliceData = SliceData["ROI"]
            lowB, uperB = SliceData["ZProjection"]        
            volumeSliceDims = niftiMem.getSliceDimensions ()
            XDim, YDim, ZDim = volumeSliceDims
            AxisMem = np.zeros ((int (max (XDim,YDim)),),dtype=np.bool)
            for obj in ClipROIObjectList :
                if obj.isROIArea () :
                    if obj.isSliceRangeContainClippedSlices (volumeSliceDims, lowB + SliceNumber, uperB + SliceNumber, AxisMem = AxisMem) :
                        objectVoxelsDrawn = ReturnMem == ObjectOutputMaskKey[obj.getROIDefIDNumber () ]
                        if np.any (objectVoxelsDrawn) :
                            clipObjectMasks[obj] = objectVoxelsDrawn
            if len (clipObjectMasks) <= 0 :
                return ReturnMem
            
            projectionMask = SliceData["Mask"] 
            xyzPositions = SliceData["XYZPos"] 
            overlap = np.zeros (projectionMask.shape,dtype=np.bool)
            for obj, objmask in clipObjectMasks.items () :
                np.logical_and (projectionMask, objmask, out = overlap)
                XBox, YBox, ZBox = obj.getBoundingBox ()    
                ZClipLower, ZClipMax = ZBox
                XClipLower, XClipMax = XBox
                YClipLower, YClipMax = YBox
                maskedIndex = overlap[projectionMask]
                ZPosTest = xyzPositions[2][maskedIndex] + SliceNumber
                XPosTest = xyzPositions[0][maskedIndex]
                YPosTest = xyzPositions[1][maskedIndex]
                
                ClipMask = np.logical_or (ZPosTest < ZClipLower, ZPosTest > ZClipMax)
                np.logical_or (ClipMask, XPosTest < XClipLower, out = ClipMask)
                np.logical_or (ClipMask, XPosTest > XClipMax, out = ClipMask)
                np.logical_or (ClipMask, YPosTest < YClipLower, out = ClipMask)
                np.logical_or (ClipMask, YPosTest > YClipMax, out = ClipMask)
                if np.any (ClipMask) :
                    innermask = ReturnMem[overlap]
                    innermask[ClipMask] = 0
                    ReturnMem[overlap] = innermask
            return ReturnMem
        
        def hideObject (HideObjects, sliceData):
            for hideIDNum in HideObjects :
                if hideIDNum in self._roiSliceAxisObjectROIIDtoAxisCacheIDConversion :
                    sliceData[sliceData==self._roiSliceAxisObjectROIIDtoAxisCacheIDConversion[hideIDNum]] = 0
            return sliceData
        
        
        if HideObjects is not  None :
            HideObjects = [ obj.getROIDefIDNumber () for obj in HideObjects]
        
       
        SliceNumberPlusOffset = SliceNumber + self.getProjectionAxisZeroOffset ()       
        if not DisableProjectionSliceCache :             
            if self._roiSliceAxisCacheValid[SliceNumberPlusOffset] :
                if self._roiSliceAxisCache is not None :
                    if ROIIDNumber is None :
                        sliceData = self._roiSliceAxisCache[SliceNumberPlusOffset,...]
                        ObjectOutputMaskKey = self._roiSliceAxisObjectROIIDtoAxisCacheIDConversion
                        sliceData = np.copy (sliceData)
                        if HideObjects is not None and len (HideObjects) > 0 :
                            sliceData = hideObject (HideObjects, sliceData)
                        sliceData = clipReturnMem (sliceData, ClipROIObjectList, niftiMem, ObjectOutputMaskKey)
                    else:
                        sliceData = self._roiSliceAxisCache[SliceNumberPlusOffset,...]
                        if ROIIDNumber in self._roiSliceAxisObjectROIIDtoAxisCacheIDConversion and (HideObjects is None or ROIIDNumber not in HideObjects) :
                            cacheID = self._roiSliceAxisObjectROIIDtoAxisCacheIDConversion[ROIIDNumber]
                            sliceData = (sliceData == cacheID).astype (np.uint8)
                            for obj in ClipROIObjectList :
                                if obj.getROIDefIDNumber () == ROIIDNumber :
                                    ObjectOutputMaskKey = {}
                                    ObjectOutputMaskKey[ROIIDNumber] = 1
                                    sliceData = clipReturnMem (sliceData, [obj], niftiMem, ObjectOutputMaskKey)
                                    break
                        else:
                            sliceData = np.zeros (sliceData.shape,dtype=np.uint8)
                    return sliceData, self._roiSliceAxisCacheColorTable
                
            elif ROIIDNumber is not None and ROIIDNumber in self._individualROIProjectionCache :
                roiCache = self._individualROIProjectionCache[ROIIDNumber]
                if  roiCache["ValidSlices"][SliceNumberPlusOffset]  :
                    sliceData = roiCache["Volume"][SliceNumberPlusOffset,...]
                    if (HideObjects is not None and ROIIDNumber in HideObjects) :
                        return np.zeros (sliceData.shape, dtype=np.uint8), self._roiSliceAxisCacheColorTable
                    sliceData = np.copy (sliceData)
                    for obj in ClipROIObjectList :
                        if obj.getROIDefIDNumber () == ROIIDNumber :
                            ObjectOutputMaskKey = {}
                            ObjectOutputMaskKey[ROIIDNumber] = 1
                            sliceData = clipReturnMem (sliceData, [obj], niftiMem, ObjectOutputMaskKey)
                            break                
                    return sliceData, self._roiSliceAxisCacheColorTable     
        SliceData, _ = self.getAxialViewProjection (niftiMem)
        SliceData = SliceData["ROI"]
       
        projectionMask = SliceData["Mask"] 
        
        if (HideObjects is not None and ROIIDNumber in HideObjects) :
            return np.zeros (projectionMask.shape, dtype = np.uint8), None
        
        xyzPositions = SliceData["XYZPos"] 
        lowerB, upperB = SliceData["ZProjection"]    
        
        ColorTable = None 
        ROIIDColorMaskIDConversionDict = None
        ZPos = xyzPositions[2,:] + SliceNumber
        
        ZDim = niftiMem.getSliceDimensions ()[2] - 1
        if SliceNumber + lowerB >= 0 and SliceNumber + upperB <= ZDim :
            ZClip = None
        else:
            ZClip = np.logical_and (ZPos >= 0, ZPos <= ZDim)
            ZPos = ZPos[ZClip]        
        if ROIIDNumber is None :
            ReturnMem = np.zeros (projectionMask.shape, dtype = np.int)
            results = GetVolumeFunc (None, None)
        else:            
            zMin = np.min (ZPos)
            ReturnMem = np.zeros (projectionMask.shape, dtype = np.uint8)
            results = GetVolumeFunc (int (zMin), int(np.max (ZPos)))
        if results is None :
            return ReturnMem, ColorTable
        
        if ROIIDNumber is None :
            SliceMem, ColorTable, ROIIDColorMaskIDConversionDict = results[0], results[1], results[2]
        else:
            SliceMem = results
            ZPos -= zMin            
        
        if ZClip is not None :
            XPos = xyzPositions[0][ZClip]
            YPos = xyzPositions[1][ZClip]
            ClipMask = np.copy(projectionMask)
            ClipMask[ClipMask == 1] = ZClip
        else:
            XPos = xyzPositions[0]
            YPos = xyzPositions[1]
            ClipMask = projectionMask                    
        
        if SliceMem is not None :
            ReturnMem[ClipMask] = map_coordinates(SliceMem, [XPos,YPos, ZPos], order=0, mode='constant', cval=0.0, prefilter=False)
            
            if not DisableProjectionSliceCache :
                self._cacheProjectedSlice (SliceNumber + self.getProjectionAxisZeroOffset (), ReturnMem, ROIIDNumber = ROIIDNumber, ROIIDColorMaskIDConversionDict = ROIIDColorMaskIDConversionDict, ColorTable = ColorTable)
            
            ReturnMem = np.copy (ReturnMem)
           
            if ROIIDNumber is None :
                if HideObjects is not None and len (HideObjects) > 0 :     
                    ReturnMem = hideObject (HideObjects, ReturnMem)
            
            if ROIIDNumber is None :
                ObjectOutputMaskKey = self._roiSliceAxisObjectROIIDtoAxisCacheIDConversion
                ReturnMem = clipReturnMem (ReturnMem, ClipROIObjectList, niftiMem, ObjectOutputMaskKey)
            else:
                for obj in ClipROIObjectList :
                    if obj.getROIDefIDNumber () == ROIIDNumber :
                        ObjectOutputMaskKey = {}
                        ObjectOutputMaskKey[ROIIDNumber] = 1
                        ReturnMem = clipReturnMem (ReturnMem, [obj], niftiMem, ObjectOutputMaskKey)
                        break
        return ReturnMem, ColorTable