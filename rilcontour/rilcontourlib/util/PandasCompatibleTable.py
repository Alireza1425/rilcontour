#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 25 15:48:14 2018

@author: m160897
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  3 21:52:42 2018

@author: KennethPhilbrick
"""

import math
import copy
import numpy as np
from scipy import stats 
import pandas as pd
from PyQt5.QtWidgets import QApplication

class PandasCompatibleTable :
    def __init__ (self):
        self._columnVector = []
        self._columnHeader = []
    
    def insertTableRight (self, table) :        
        self_row_count = self.getRowCount ()
        table_row_count = table.getRowCount ()        
        if self_row_count <  table_row_count :
            self._Resize (table_row_count, self.getColumnCount ())
        elif self_row_count >  table_row_count :
            table._Resize (self_row_count, table.getColumnCount ())        
        self._columnHeader += copy.copy (table._columnHeader)
        self._columnVector += copy.copy (table._columnVector)
            
    def getHeaderIndexList (self, find) :
        indexLst = []
        for index, item in enumerate (self._columnHeader) :
            if find == item :
                indexLst.append (index)
        return indexLst

    def inserHeaderColumnAt (self, insertion_index, columnHeaderVector) :        
        blankRowVector = []
        QApplication.processEvents ()                 
        if len (self._columnVector) > 0 :
            for index in range (len (self._columnVector[0])) :
                blankRowVector.append ("")        
        for index in range (len (columnHeaderVector)) :
            self._columnHeader.insert(insertion_index + index, columnHeaderVector[index])        
        for index in range (len (columnHeaderVector)) :
            self._columnVector.insert (insertion_index, copy.copy (blankRowVector))            
    
    def hasDataInColumn (self, column, data, OrEmpty = False, IgnoreCase = False) :
        if column < len ( self._columnVector) :
            if not IgnoreCase :
                if data in self._columnVector[column] :
                    return True
            else:
                lowerdata = str(data).lower ()
                for item in self._columnVector[column] :
                    if str(item).lower () == lowerdata :
                        return True
            if OrEmpty :
                for item in self._columnVector[column] :
                    if len (item) > 0 :
                        return False
                return True
        return False
    
    @staticmethod 
    def getCellText (param, default = "NotSet")  :
        text = param.strip()
        if (len (text) == 0) :
            return default
        return text
    
    def convertTableToPandasDataFrame (self, IncludeHeader = False) :
        return pd.DataFrame(self.getTable (IncludeHeader = IncludeHeader)).T
    
    def getHeaderColumnCount (self) :
        return len (self._columnHeader)
    
    def getColumnCount (self) :
        return len (self._columnVector)
    
    def getRowCount (self) :
        if len ( self._columnVector) == 0 :
            return 0
        return len (self._columnVector[0])
    
    def insertHeaderColumnsRight (self, columnHeaderVector) :
        blankColumnVector = [] 
        blankRowVector = []
        QApplication.processEvents ()
        if len (self._columnVector) > 0 :
            for index in range (len (self._columnVector[0])) :
                blankRowVector.append ("")
        for index in range (len (columnHeaderVector)) :
            blankColumnVector.append (copy.copy (blankRowVector))            
        self._columnVector +=  blankColumnVector
        self._columnHeader +=  columnHeaderVector
        
    def insertHeaderColumnsLeft (self, columnHeaderVector) :
        blankColumnVector = [] 
        blankRowVector = []
        QApplication.processEvents ()
        if len (self._columnVector) > 0 :
            for index in range (len (self._columnVector[0])) :
                blankRowVector.append ("")
        for index in range (len (columnHeaderVector)) :
            blankColumnVector.append (copy.copy (blankRowVector))            
        self._columnVector = blankColumnVector + self._columnVector
        self._columnHeader = columnHeaderVector + self._columnHeader
        
    def _Resize (self, row, column) :
        rS, cS = self.getSize ()
        if cS < column :
            QApplication.processEvents ()
            columnsToAdd = column - cS
            rowlist = []
            rowCount = max (rS, row)
            for ri in range (rowCount) :
                rowlist.append ("")                
            for index in range (columnsToAdd) :
                self._columnVector.append (copy.copy (rowlist))
                self._columnHeader.append ("")                
        if rS < row :
            QApplication.processEvents ()
            rowsToAdd = row - rS
            rowlist = []
            for index in range (rowsToAdd) :
               rowlist.append ("")               
            for columIndex in range (len (self._columnVector)) :                
                if len (self._columnVector[columIndex]) < row:
                     self._columnVector[columIndex] += rowlist    
    
    def insertDataVector (self, row, column, datavector) :
        QApplication.processEvents ()
        for index, item in enumerate (datavector) :
            self.insertData (row, column + index, item)
            
        
    def setColumnHeaderList (self, baseindex, lst) :
        QApplication.processEvents ()
        for index, columnName in enumerate (lst) :
            self.setColumnHeader (baseindex + index, columnName)            
            
    def setColumnHeader (self, columnIndex, text):
        rS, cS = self.getSize ()
        if columnIndex >= cS :
             self._Resize (rS, columnIndex + 1)
        self._columnHeader[columnIndex] = text
        
    def insertData (self, row, column, data) :
        rS, cS = self.getSize ()
        newRow = max (rS, row + 1)
        newColumn = max (cS, column + 1)
        if (newRow > rS or newColumn > cS) :
            self._Resize (newRow, newColumn)
        self._columnVector[column][row] = data             
    
    def getColumnByRef (self, column) :
        return self._columnVector[column]
    
    def getData (self, column, row) :
        return self._columnVector[column][row]
    
    def getSize (self) :
        rows = 0
        columns = len (self._columnVector)
        if (columns > 0) :
            rows = len (self._columnVector[0])
        return (rows, columns)
        
    def appendBlankRow (self) :
        if  len (self._columnVector) <= 0 :
            self._columnVector = [[""]]
            self._columnHeader = [""]
        else:
            QApplication.processEvents ()
            for columIndex in range (len (self._columnVector)) :
                self._columnVector[columIndex] += [""]
                
    
    def appendRowTitleList (self, titlelst) :
        if  len (self._columnVector) <= 0 :
            self._columnVector = []
            self._columnHeader = []
            QApplication.processEvents ()
            for titleName in titlelst :
                self._columnVector.append ([titleName])
                self._columnHeader.append ("")                
        else:
            if len (titlelst) > len (self._columnVector) :
                row, column = self.getSize ()
                self._Resize (row, len (titlelst))
            QApplication.processEvents ()
            for titleIndex, titleName in enumerate(titlelst):
                self._columnVector[titleIndex] += [titleName]            
            for columIndex in range (titleIndex + 1, len (self._columnVector)) :
                self._columnVector[columIndex] += [""]

    
    def removeColumn (self, index) :
        del self._columnVector[index]
        del self._columnHeader[index]
    
    def removeRow (self, index) :
        QApplication.processEvents ()
        for columnindex in range (len ( self._columnVector)) :
            vec = self._columnVector[columnindex]
            del vec[index]
            self._columnVector[columnindex] = vec
            
     
    def appendTableRows (self, table) :
        self.appendTable(table, InsertColumnHeader = False)
        
    def appendTable (self, table, InsertColumnHeader = True) :
        r1, c1 = table.getSize ()
        r2, c2 = self.getSize ()
        newC = max(c1, c2)
        if InsertColumnHeader :
            newR = r1 + r2 + 1
            headeroffset = 1
        else:
            newR = r1 + r2
            headeroffset = 0
        self._Resize (newR, newC)
        QApplication.processEvents ()
        for cIndex in range (c1) :
            if (InsertColumnHeader) :
                self._columnVector[cIndex][r2] = table._columnHeader[cIndex]
            for rIndex in range (r1) :
                self._columnVector[cIndex][rIndex + r2 + headeroffset] = table._columnVector[cIndex][rIndex]
                
     
    def getColumnHeader (self) :
         return copy.copy (self._columnHeader)
     
    def getTable (self, IncludeHeader= True) :          
         returnColumnVector = [] 
         if (IncludeHeader) :      
             for columnIndex in range (len (self._columnVector)):
                 returnColumnVector.append ([self._columnHeader[columnIndex]] + self._columnVector[columnIndex])
         else:
             QApplication.processEvents ()
             for columnIndex in range (len (self._columnVector)):
                 returnColumnVector.append ( copy.copy (self._columnVector[columnIndex]))                 
         """if RemoveSpaces:
            for cI in range (len (returnColumnVector)) :
                column = returnColumnVector[cI]
                for rI in range (len (column)) :
                    try :
                        column[rI] = column[rI].strip ()
                    except:
                        pass"""
         return returnColumnVector
    
    def appendColumnSummaryStatistics (self, StatsSummaryList, HeaderColumn = -1, DataColumns = None, ):
        if len (self._columnVector) == 0 :
            return
        rowcount = len (self._columnVector[0])
        if (rowcount <= 1) :
            return
       
        arrayList = []
        QApplication.processEvents ()
        for columnIndex in range (len (self._columnVector)):            
            if DataColumns is None or columnIndex in DataColumns :
                try:
                    ary = np.array (self._columnVector[columnIndex], dtype=np.float)
                except :
                    ary = None
                arrayList.append (ary)    
            else:
                 arrayList.append (None)
                 
        rowsToAdd = len (StatsSummaryList)
        rows, columns = self.getSize ()
        self._Resize (rows + rowsToAdd, columns)
        QApplication.processEvents ()
        for columnIndex in range (len (self._columnVector)) :
            for statindex, stat in enumerate(StatsSummaryList) :                
                if columnIndex == HeaderColumn :
                    self._columnVector[columnIndex][rowcount + statindex]  = stat
                elif (arrayList[columnIndex] is not None) :
                    if (stat == "Mean") :
                        self._columnVector[columnIndex][rowcount + statindex] = "%0.3f" % np.mean (arrayList[columnIndex])
                    elif (stat == "SD") :
                        self._columnVector[columnIndex][rowcount + statindex] = "%0.3f" % math.sqrt (np.var (arrayList[columnIndex], ddof=1))
                    elif (stat == "Count") :
                        self._columnVector[columnIndex][rowcount + statindex] = "%d" % arrayList[columnIndex].shape[0]
                    elif (stat == "Median") :
                        self._columnVector[columnIndex][rowcount + statindex] = "%0.3f" % np.median (arrayList[columnIndex])
                    elif (stat == "Min") :
                        self._columnVector[columnIndex][rowcount + statindex] = "%0.3f" % np.min (arrayList[columnIndex])
                    elif (stat == "Max") :
                        self._columnVector[columnIndex][rowcount + statindex] = "%0.3f" % np.max (arrayList[columnIndex])
                   
    
    def appendColumnANOVAStatistics (self, StatsSummaryList, HeaderColumn = -1, DataColumns = None, RawDataVectors = None):
        if len (self._columnVector) == 0 :
            return
        rowcount = len (self._columnVector[0])
        if (rowcount <= 1) :
            return
        if (RawDataVectors is  None):
            return
        
        arrayList = RawDataVectors
        
        rowsToAdd = len (StatsSummaryList)
        rows, columns = self.getSize ()
        self._Resize (rows + rowsToAdd, columns)
        QApplication.processEvents ()
        for columnIndex in range (len (self._columnVector)) :
            if (columnIndex != HeaderColumn and columnIndex not in DataColumns) :
                continue
            for statindex, stat in enumerate(StatsSummaryList) :                
                if columnIndex == HeaderColumn :
                    self._columnVector[columnIndex][rowcount + statindex]  = stat
                elif (columnIndex in DataColumns) :
                    if (stat == "ANOVA(P)") :
                        
                        _, PValue = stats.f_oneway (*arrayList)
                        self._columnVector[columnIndex][rowcount + statindex] = "%0.3f" % PValue
                    elif (stat == "ANOVA(F)") :
                        FStat, _ = stats.f_oneway (*arrayList)
                        self._columnVector[columnIndex][rowcount + statindex] = "%0.3f" % FStat
    