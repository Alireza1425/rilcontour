#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 18 10:53:35 2019

@author: m160897
"""
from PyQt5 import QtCore
from PyQt5.QtWidgets import  QDialog
from rilcontourlib.ui.qt_ui_autogen.rcc_listdlgautogen import Ui_ErrorListDlg
from rilcontourlib.util.rcc_util import ResizeWidgetHelper

class ErrorListDlg (QDialog) :
    def __init__ (self, parent, title, msg, textlst) :        
        QDialog.__init__ (self, parent)        
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)         
        self.ui = Ui_ErrorListDlg ()        
        self.ui.setupUi (self)          
        self.setWindowTitle (title)
        self.setWindowFlags(QtCore.Qt.Dialog)          
        self.ui.CloseBtn.clicked.connect (self.accept)
        self.ui.MsgLabel.setText (msg)
        for text in textlst :
            self.ui.listWidget.addItem (text)        
        self._resizeWidgetHelper = ResizeWidgetHelper ()

    def _internalWindowResize (self, newsize):                            
        if (self._resizeWidgetHelper != None) :
            self._resizeWidgetHelper.setWidth  (self.ui.MsgLabel, newsize.width () - 19)         
            self._resizeWidgetHelper.setWidth  (self.ui.listWidget, newsize.width () - 19)            
            self._resizeWidgetHelper.setHeight  (self.ui.listWidget, newsize.height () - 100)                   
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.CloseBtn, newsize.width () - 130)                     
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.CloseBtn, newsize.height () - 41)    
            
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)                 
        self._internalWindowResize ( qresizeEvent.size () ) 
    
    @staticmethod
    def showErrorListDlg (parent, title, msg, textlst):
        dlg = ErrorListDlg (parent, title, msg, textlst)
        dlg.exec_()
        del dlg
