#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 30 12:04:09 2020

@author: m160897
"""
import sys
import os
import json
import pickle
import copy
from rilcontourlib.util.FileUtil import ScanDir, FileUtil
import weakref

class PythonVersionTest :    
    @staticmethod    
    def IsPython2 () :
        return sys.version_info[0]  == 2
    
    @staticmethod    
    def IsPython3 () :
        return sys.version_info[0]  >= 3
    
class LongFileNameCache :
    def __init__ (self, path, ResetCache = False) :
        if ResetCache :
            try :
                with open (path, "wb") as outfile :
                    pickle.dump ({}, outfile)
                FileUtil.setFilePermissions (path)
                print ("Reseting Long file name cache.")
            except:
                print ("A error occured reseting the long file name cache")
                
        self._finalizer = None
        self._cacheFilePath = path        
        self._NodesChanged = {}        
        self._LFPathCache = None
        self._loadLongFileNameCache ()   
            
    if PythonVersionTest.IsPython2 () :
        def __del__ (self) :
            self.saveIfChanged ()          
    
    def _queueFinalizer (self) :
        if PythonVersionTest.IsPython3 () :                 
            if self._finalizer is not None :
                self._finalizer.detach()
                self._finalizer = None 
            self._finalizer = weakref.finalize (self, LongFileNameCache._emergancySaveChanges, self._cacheFilePath, self._NodesChanged)
        return 
    
    def getLFCache (self, lf) :
        path = lf.getPath ()      
        if path in self._NodesChanged :
            return self._NodesChanged[path]
        if self._LFPathCache is not None :
            if path in self._LFPathCache :
                return self._LFPathCache[path]
        return (None, None)
                    
    def getPath (self) :
        return self._cacheFilePath
    
    def isChanged (self) :
        return len (self._NodesChanged) > 0
    
    def signalLongFileNameChange (self, lf) :
        self._NodesChanged[lf.getPath ()] = (copy.copy (lf.getTreePathList ()), copy.copy (lf.getLongfileNameMap ()))        
        self._queueFinalizer ()
        
    def UnloadLongFileNameCache (self) :
        if self.isChanged () :
            self.saveIfChanged ()
        self._LFPathCache = None
        
    def _loadLongFileNameCache (self) :
        try :            
            path = self.getPath ()
            if path is None :
                self._LFPathCache = {}                
            else:
                if os.path.isfile (path) :
                    with open (path, "rb") as infile :
                        self._LFPathCache = pickle.load (infile)   
                    if self._LFPathCache is None :
                        self._LFPathCache = {}
                else:
                    self._LFPathCache = {}
        except:
            self._LFPathCache = {}

    @staticmethod 
    def _emergancySaveChanges (path, changes):
        if len (changes) > 0 :            
            try :
                with open (path, "rb") as infile :
                    cache = pickle.load (infile)
                if cache is None :
                    cache = {}
            except:
                cache = {}
            for key, value in changes.items ():
                cache[key] = value
            try :
                with open (path, "wb") as outfile :
                    pickle.dump (cache, outfile)
                FileUtil.setFilePermissions (path)
            except:
                print ("A error occured saving the long file name cache")
    
    def saveIfChanged (self) :
        if self.isChanged () :
            print ("Saving Changes")
            path = self.getPath ()
            if path is not None :                      
                if self._LFPathCache is not None :
                    UnloadCache = False
                else:
                    UnloadCache = True
                    self._loadLongFileNameCache ()            
                for key, value in  self._NodesChanged.items () :
                    self._LFPathCache[key] = value           
                try :
                    with open (path, "wb") as outfile :
                        pickle.dump (self._LFPathCache, outfile)
                    FileUtil.setFilePermissions (self._cacheFilePath)
                    self._NodesChanged = {}
                    self._queueFinalizer ()
                except:
                    try :
                        os.remove (path)
                    except:
                        pass
                if UnloadCache :
                    self._LFPathCache = None
                    

class LongFileNameAPI :    
    
    def __init__ (self, filesystemtreeNode, LoadLongFileNameFileDescriptorTreePathList = False, TreePathList = None, CreateIfMissing = False, DirCache = None, LongFileNameCache = None) :        
        self._LongFileNameCache = LongFileNameCache
        self._FileNameMap = {}
        self._addedLFDatToSkipDicomFile = False
        self._TreePathList = TreePathList
        if isinstance (filesystemtreeNode, str) :
            if filesystemtreeNode.endswith (os.sep) :
                self._filesystemtreeNode = filesystemtreeNode[:-len (os.sep)]
            else:
                self._filesystemtreeNode = filesystemtreeNode        
            if TreePathList is None :
                LoadLongFileNameFileDescriptorTreePathList = True     
        else:
            self._filesystemtreeNode = filesystemtreeNode   
        
        if CreateIfMissing :
            self._saveLongFileNameDescriptionData (SaveOnlyIfMissing = True)                
            
        self._buildDirFileList (DirCache = DirCache)
        self._loadLongFileNameDescriptionData (LoadLongFileNameFileDescriptorTreePathList)    
        
    @staticmethod
    def getLongFileNamePath (shortpath) :
        fileDir, filename = os.path.split (shortpath)
        lfnameAPI = LongFileNameAPI (fileDir, LoadLongFileNameFileDescriptorTreePathList = True)
        return lfnameAPI.getLongFileNameRelativePath (filename)
     
    def getLongfileNameMap (self) :
        return self._FileNameMap
    
    def rename (self, shortfileName, longFileName) :        
        if shortfileName != longFileName and self.hasFileName (longFileName, IgnoreFileName = shortfileName) :
            return False
        else:
            self.createLongFileNameMap (shortfileName, longFileName)
            return True
        
    def getLongFileNameRelativePath (self, shortName) :
        fname = self.getLongFileName (shortName) 
        if fname is None :
            fname = shortName
        return os.path.join (self.getLongFileRelativeBasePath (), fname)
    
    def getLongFileRelativeBasePath (self) :
        try :
            return self._longfilepath
        except:    
            basePath = self.getPath ()
            pathlst = self.getTreePathList ()       
            for filename in pathlst :
                basePath, _ = os.path.split (basePath)
            for filename in pathlst :
                basePath = os.path.join (basePath, filename)
            self._longfilepath = basePath
            return self._longfilepath
         
          
    def getPath (self) :        
        if isinstance (self._filesystemtreeNode, str) :
            return self._filesystemtreeNode
        else:
            return self._filesystemtreeNode.getPath ()
     
    def getTreePathList (self) :
        if self._TreePathList is None :
            if isinstance (self._filesystemtreeNode, str) :
                return [] 
            else:
                return self._filesystemtreeNode.getTreePathList (ReturnFileSystemPath = False)
        else:
            return self._TreePathList 
    
    def _buildDirFileList (self, DirCache = None):
        self._filesInDir = set ()    
        path = self.getPath ()
        if DirCache is not None :
            if path in DirCache :
                fileList = DirCache[path]
                if isinstance (fileList, list) :
                    for entry in fileList :
                        self._filesInDir.add (entry.name)
                    return   
        if os.path.isdir (path) :
            try :
                try :
                    dirIterator = ScanDir.scandir (path) 
                    for entry in dirIterator :
                        self._filesInDir.add (entry.name)
                finally:
                    try :
                      dirIterator.close ()
                    except:
                        pass
            except:
                FileList = os.path.listdir (path)
                for fname in FileList :
                     self._filesInDir.add (fname)
    
    @staticmethod
    def doesLFDataFileExistForPath (path, TryRead = True) :
        try:
            path = os.path.join (path,"LF.dat")
            if os.path.isfile (path) :
                if TryRead :
                    with open  (path,"rt") as infile:
                        _, FileNameMap = json.load (infile)
                return True
            return False
        except:
            return False    
        
    @staticmethod
    def hasLongFileNameDefinitions (treeNode, CreateLongFileNamePathIfMissing = True, DirCache = None) :        
        if isinstance (treeNode, str) : 
            basePath = treeNode
        else:
            basePath = treeNode.getPath ()                        
        path = os.path.join (basePath,"LF.dat")
        
        if DirCache is not None :
            if not CreateLongFileNamePathIfMissing :
                return path in DirCache
            elif path in DirCache :
                return True
            
        if os.path.isfile (path) :
            return True
        
        if (CreateLongFileNamePathIfMissing) :
            try :
                if isinstance (treeNode, str) :
                    _, fname = os.path.split (treeNode)
                    treePathList = [fname]
                else:
                    treePathList = treeNode.getTreePathList (ReturnFileSystemPath = False)
                with open  (path,"wt") as infile:
                    json.dump ((treePathList, {}), infile)       
                FileUtil.setFilePermissions (path)
                LongFileNameAPI._addLFdatFileToSkipDicoms (basePath)
                return True
            except:
                pass            
        return False
    
    @staticmethod 
    def _TreePathsMatch (l1, l2) :
        if l1 is None and l2 is not None :
            return False
        if l2 is None and l1 is not None :
            return False
        if len (l1) != len (l2) :
            return False
        for index in range (len (l1)) :
            if l1[index] != l2[index] :
                return False
        return True
            
            
    def _loadLongFileNameDescriptionData (self, LoadDirectoryTreePathList = False):
        self._FileNameMap = {} 
        try :
            lfDirPath = self.getPath ()
            LoadedCachedFileMap = False
            TryToLoadFromCache = self._LongFileNameCache is not None and not LoadDirectoryTreePathList
            if TryToLoadFromCache :
                fileTreePathList, self._FileNameMap = self._LongFileNameCache.getLFCache (self) 
                if self._FileNameMap is not None :
                    LoadedCachedFileMap = True
            if not LoadedCachedFileMap :
                path = os.path.join (lfDirPath,"LF.dat")            
                with open  (path,"rt") as infile:
                    fileTreePathList, self._FileNameMap = json.load (infile)  
                if TryToLoadFromCache :
                   self._LongFileNameCache.signalLongFileNameChange (self)
            if LoadDirectoryTreePathList :
                self._TreePathList = fileTreePathList            
            
            FileMapChanged = False
            for fname in list (self._FileNameMap.keys ()):
                    if not fname.endswith (".") :
                        if fname not in self._filesInDir : 
                            del self._FileNameMap[fname]
                            FileMapChanged = True
                    else:
                        if fname not in self._filesInDir : 
                            correctedFName = fname[:-1]
                            if correctedFName not in self._filesInDir :
                                del self._FileNameMap[fname]
                                FileMapChanged = True
                            elif correctedFName not in self._FileNameMap :
                                self._FileNameMap[correctedFName] = self._FileNameMap[fname]
                                del self._FileNameMap[fname]
                                FileMapChanged = True                                
            if FileMapChanged or not LoadDirectoryTreePathList and not LongFileNameAPI._TreePathsMatch (self.getTreePathList (), fileTreePathList) :
                self._saveLongFileNameDescriptionData ()
        except:
            self._TreePathList = None
            self._FileNameMap = {}        
      
    def createLongFileNameMap (self, short, long) :
        Change = False        
        if short not in self._FileNameMap :            
            Change = True                    
        elif self._FileNameMap[short] != long :
            Change = True                            
        self._filesInDir.add (short)            
        if Change :
            self._FileNameMap[short] = long        
            self._saveLongFileNameDescriptionData ()
        
    
    def _removeShortFileNameMap (self, name) :
        if name in self._FileNameMap :
            del self._FileNameMap[name]
            self._saveLongFileNameDescriptionData ()
                    
    def hasFileName (self, name, RemoveMissingLongFiles = True, IgnoreFileName = None) :
        if name in self._FileNameMap :
            return True
        if name in self._filesInDir :
            return True        
        RemoveDangelingShortFileNames = []
        try :            
            for shortfilename, longfilename in self._FileNameMap.items () :                
                    if name == longfilename :
                        if IgnoreFileName is not None and shortfilename == IgnoreFileName :
                            return False
                        elif not RemoveMissingLongFiles :
                            return True
                        else:
                            try :                        
                                filenamepath = os.path.join (self.getPath (), shortfilename)
                                if os.path.isfile (filenamepath) :
                                    return True
                                else:
                                    RemoveDangelingShortFileNames.append (shortfilename)
                            except:
                                #if can not determine if file exists assume it does.
                                return True    
            return False 
        finally:
            FileNameMapChanged = False
            for shortfilename in RemoveDangelingShortFileNames :
               try :
                   del self._FileNameMap[shortfilename] 
                   FileNameMapChanged = True
               except:
                   pass
               try :
                   self._filesInDir.remove (shortfilename)
               except:
                   pass
            if FileNameMapChanged :
                self._saveLongFileNameDescriptionData ()
                
            
    def isShortFileName (self, name) :
        return name in self._FileNameMap 
        
    def isLongFileName (self, name) :
        for value in self._FileNameMap.values ():
            if value == name :
                return True
        return False
    
    @staticmethod
    def _addLFdatFileToSkipDicoms (path) :        
        skipDicoms = {}
        path = os.path.join (path,"SkipDicoms.dat")             
        try :                        
            if os.path.isfile (path) :
                with open  (path,"rt") as infile:
                    skipDicoms = json.load (infile)        
        except:
            skipDicoms = {}
        if "LF.dat" in skipDicoms :
            return True
        skipDicoms["LF.dat"] = True
        try :
            with open  (path,"wt") as outfile:
                json.dump (skipDicoms, outfile)    
            FileUtil.setFilePermissions (path)
            return True
        except:
            print ("Error occured saving adding LF.dat to skip dicoms")
        return False
        
            
    def _saveLongFileNameDescriptionData (self, SaveOnlyIfMissing = False):        
        try :            
            basePath = self.getPath ()
            path = os.path.join (basePath,"LF.dat") 
            if SaveOnlyIfMissing and os.path.isfile (path) :
                return True
            with open  (path,"wt") as infile:
                json.dump ((self.getTreePathList (), self._FileNameMap), infile)            
            FileUtil.setFilePermissions (path)
            if not self._addedLFDatToSkipDicomFile :
                self._addedLFDatToSkipDicomFile = LongFileNameAPI._addLFdatFileToSkipDicoms (basePath)
            if self._LongFileNameCache is not None :
                self._LongFileNameCache.signalLongFileNameChange (self)
            return True
        except:
            return False
            
    def getShortFileName (self, longName, ReturnShortMappings = True) :
        for key ,value in self._FileNameMap.items ():
            if value == longName :
                return key
        if ReturnShortMappings :
            if longName in self._FileNameMap or self.isShortFileName (longName) :
                return longName
        return None
        
    def getLongFileName (self, shortName) :
        if shortName in self._FileNameMap :
            return self._FileNameMap[shortName]
        if shortName in self._filesInDir :
            return shortName 
        return None

    def isDir (self, longdirname):
        shortFileName = self.getShortFileName (longdirname)
        if shortFileName is None :
            return False        
        return os.path.isdir (os.path.join (self.getPath (),shortFileName))
    
    def exists (self, longdirname):
        return self.getShortFileFileName (longdirname) is not None            
                
    def isFile (self, longfileName):
        shortFileName = self.getShortFileName (longfileName)
        if shortFileName is None :
            return False        
        return not os.path.isdir (os.path.join (self.getPath (),shortFileName))
        
    def isFileInDir (self, shortfileName) :
        return shortfileName in self._filesInDir
    
    def renameFileInDir (self, oldName, newName) :
        if oldName != newName :
            self._filesInDir.remove (oldName)
            self._filesInDir.add (newName)
            self._FileNameMap[newName] = self._FileNameMap[oldName]
            del self._FileNameMap[oldName]
                
    def createShortFileName (self, LongFileName, MaxLength = None, shortfilename = None, ExcludeFileNames = None) :
        
        def _getExtension (fname) :
            lowerName = fname.lower ()
            if lowerName.endswith (".nii.gz") :
                return "nii.gz"
            else:
                lowerPath = lowerName.split (".") 
                if len (lowerPath) == 1 :
                    return ""
                else:
                    extension = lowerPath[-1]
                    return fname[-len (extension):].strip ()            
        
        shortFileName = self.getShortFileName (LongFileName, ReturnShortMappings = False)        
        if shortFileName is not None  :
            return shortFileName
        
        path = self.getPath ()
        fullName = os.path.join (path, LongFileName)
        if MaxLength is not None and len (fullName) <= MaxLength :
            if not os.path.exists (fullName) :
                self.createLongFileNameMap (LongFileName, LongFileName)
                return LongFileName
            
        
        if shortfilename is not None :
            extension = _getExtension (shortfilename)
        else:
            extension = _getExtension (LongFileName)
                    
        ShortName = "S~%d"
        count = 1
        
        while True :
            TestName = ShortName % count            
            InvalidFileName = False
            if ExcludeFileNames is not None :
                InvalidFileName = TestName in ExcludeFileNames                    
            if not InvalidFileName :
                if len (extension)  > 0 :
                    TestName += "." + extension
                if TestName not in self._filesInDir :
                    if not self.hasFileName(TestName, RemoveMissingLongFiles = False) :
                        TestPath = os.path.join (path, TestName)
                        if not os.path.exists (TestPath) :
                            self.createLongFileNameMap (TestName, LongFileName)
                            return TestName                        
            count += 1
                
    def removeLongFile (self, LongFileName) :
        shortFileName = self.getShortFileName (LongFileName)        
        self.removeShortFile (shortFileName)
    
    def removeShortFile (self, shortFileName) :  
        self.removeShortFileNameMap (shortFileName)
        if shortFileName in self._filesInDir :
            self._filesInDir.remove(shortFileName)
    