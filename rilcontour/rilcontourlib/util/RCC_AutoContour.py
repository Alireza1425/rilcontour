#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 13 16:40:01 2017

@author: m160897
"""
from six import *
import rilcontourlib.util.morphsnakes as morphsnakes
import numpy as np
from skimage.morphology import binary_erosion, binary_dilation, watershed
from PyQt5 import QtCore
from PyQt5.QtWidgets import QProgressDialog, QApplication
import copy
from rilcontourlib.util.rcc_util import FindContours, PlatformMP
from scipy.ndimage import filters as filt
from scipy.ndimage import sobel

def _threadGac (tpl) :    
        index, img, initialSeg = tpl
        if (index == 0) :
            gI = morphsnakes.gborders(img,alpha=1E3,sigma=1.0) # increasing sigma allows more changes in contour
            mgac = morphsnakes.MorphGAC(gI,smoothing=1,threshold=0.01,balloon=-1) #was 2.5
        elif (index == 1) :
            gI = morphsnakes.gborders(img,alpha=1E5,sigma=3.0) # increasing sigma allows more changes in contour
            mgac = morphsnakes.MorphGAC(gI,smoothing=3,threshold=0.01,balloon=0) #was 2.5setSliceViewObjectMask
        elif (index == 2) :
            gI = morphsnakes.gborders(img,alpha=1E7,sigma=3.0) # increasing sigma allows more changes in contour
            mgac = morphsnakes.MorphGAC(gI,smoothing=3,threshold=0.0001,balloon=-1) #was 2.5
        elif (index == 3) :
            gI = morphsnakes.gborders(img,alpha=1E3,sigma=3.0) # increasing sigma allows more changes in contour
            mgac = morphsnakes.MorphGAC(gI,smoothing=7,threshold=10000,balloon=1) #was 2.5
        elif (index == 4) :
            gI = morphsnakes.gborders(img,alpha=1E5,sigma=3.0) # increasing sigma allows more changes in contour
            mgac = morphsnakes.MorphGAC(gI,smoothing=7,threshold=10000,balloon=0) #was 2.5                
            
        mgac.levelset = initialSeg
        for ijk123 in range(15):            
            prev = np.copy (mgac.levelset)
            mgac.step()            
            if ijk123 != 14 and not np.any (prev - mgac.levelset) :
                break
        result = np.copy (mgac.levelset)
        del img
        del initialSeg
        del gI
        del mgac        
        return (result)  

def _GACContourSlice (img, hounsfieldVisSettings, inputArea, minVal = None, maxVal = None, FileSaveThreadManger = None):                               
        if (minVal is None or maxVal is None) :
            if (hounsfieldVisSettings.maximizeWithinSliceContrast ()) :
                    minVal = np.min (img)            
                    maxVal = np.max (img)
                    if (minVal < -1024) :
                       minVal = -1024
            else:  
                minVal, maxVal  = hounsfieldVisSettings.getCustomHounsfieldRange ()
        img = img.clip (minVal, maxVal )                    
        dv = maxVal - minVal        
        img = (img.astype (float) - float (minVal)) / float(dv)
                    
        # Dilate input Area
        initialSeg = RCC_AutoContourROIArea._DialationInputArea (inputArea,iterations = 1)
        # Now do gaussian blur of polygon to smooth
        initialSeg = ((filt.gaussian_filter(initialSeg * 255.0,sigma=[3,3])) > 100).astype (np.float)
        if (not np.any (initialSeg)) :
            return initialSeg
                
        #Begin active contour method... 
        procQueue = PlatformMP.processQueue (RCC_AutoContourROIArea._threadGac, None, PoolSize = None, SleepTime = 0, ProjectDataset = None)
        for pindex in range (5) :
            processParameters = (pindex, img, initialSeg)
            cleanupParameters = None
            procQueue.queueJob (FileSaveThreadManger, processParameters, cleanupParameters)
        ProcessResultList = procQueue.join ()
        
        
        sobelFilt = sobel(img)
        
        initalized = False
        for index, result in enumerate (ProcessResultList) :
            try :
                if (not initalized) :
                    fs1 = result
                    initalized = True
                else:
                    fs1 += result        
            except:
                print ("A error occured geting result from Autocontouring child process")
        del ProcessResultList
        
        finalSeg = (fs1 >2).astype (np.float)
        # Now do gaussian blur and threshold to finalize segmentation...
        finalSeg = (filt.gaussian_filter(finalSeg*255.0,sigma=[3,3])) > 100
        #using this helps with single slice errors of the active contour
        # Now narrow band sobel/watershed technique.                            
        if not np.any (finalSeg) :        
            return finalSeg.astype (np.float)
        
        erodeimg = RCC_AutoContourROIArea._ErosionInputArea(finalSeg,iterations=1)
        dilateimg = RCC_AutoContourROIArea._DialationInputArea  (finalSeg,iterations=1)                
        seeds = dilateimg
        seeds[erodeimg>0] = 2.0                
        
        finalSeg = (watershed(sobelFilt,seeds)>1.0).astype (np.float)
        return finalSeg

def _threadSliceGac (tpl) :
        niftiSliceData, index, ContourList,  minValue, maxValue = tpl
        sliceGACContourResultList = []
        for contour in ContourList :
            result = RCC_AutoContourROIArea._GACContourSlice (niftiSliceData, None, contour, minVal = minValue, maxVal = maxValue, FileSaveThreadManger = None) #File save closed by outer thread
            sliceGACContourResultList.append (result)        
        return (index, sliceGACContourResultList)                         
    
class RCC_AutoContourROIArea :
    
    @staticmethod    
    def _ErosionInputArea (previousArea, iterations = 5):          
       for count in range (iterations) :
           previousArea = binary_erosion (previousArea)
       return previousArea.astype (float)
                     
    @staticmethod    
    def _DialationInputArea (previousArea, iterations = 5):          
       for count in range (iterations) :
           previousArea = binary_dilation (previousArea)
       return previousArea.astype (float)
        
    _threadGac = staticmethod (_threadGac)
    
    # GAC contour opperation for a single slice       
    _GACContourSlice = staticmethod (_GACContourSlice)
    
  
    @staticmethod
    def _andArea (a1, a2):         
         new = np.bitwise_and (a1.astype(bool), a2.astype(bool))
         return new.astype (float) # * 2 -1
   
    @staticmethod
    def _setSliceContour (shape, obj, contourID, slicenumber, contour, Type, humaneiditdContoursnapcount):       
        if (len (contour) <= 0) : 
            return False        
        obj.addLargestAreaAtSlicePos(contourID, shape, contour,  slicenumber, Type)         
        obj.setHumanEditedContourSnapCount ( contourID, slicenumber, humaneiditdContoursnapcount)        
        return True        
    
    @staticmethod
    def _setSliceArea (roiSliceDictionary, roiDictionary, shape, obj, contourID, slicenumber, area, Type, humaneiditdContoursnapcount, SetSlicIndexCallBack = None):               

        if (roiDictionary.isROIinPerimeterMode ()) :
           contour = FindContours.findContours (area)        
           result = RCC_AutoContourROIArea._setSliceContour (shape, obj, contourID, slicenumber, contour, Type, humaneiditdContoursnapcount)
        else:
           origional_contour = obj.getContourMap (contourID, slicenumber, shape) 
           if np.any (area.astype(np.int8) - origional_contour.astype (np.int8) ) :                      
               ROIDefs = roiDictionary.getROIDefs ()
               RCC_AutoContourROIArea.applyChangeMaskToROIAndOverlappingObjectsAtSelectedSlice (slicenumber, obj, contourID, area.astype (np.uint8), origional_contour, roiSliceDictionary, roiDictionary, ROIDefs, Type = Type, SnappedHumanEditedSliceCount = humaneiditdContoursnapcount)               
               result = True
           else:
               result = False
             
             
        QApplication.processEvents ()    
        if (SetSlicIndexCallBack is not None and slicenumber is not None) :
            SetSlicIndexCallBack (slicenumber, Axis="Z")                      
        return result
            
    @staticmethod
    def isNpyArray (v):
        return type(v).__module__ == np.__name__    
   
    
                       
    # Called to update contouring for manually contoured sections    
    @staticmethod
    def contourSingleSlice (niftiVolume, ROIDictionary, hounsfieldVisSettings, itemName, selectionIndexList, StartSliceIndex, ShowProgressDialogParentWindow = None, SetSlicIndexCallBack = None, ContourIDList = None, SaveDictionaryChanges = True, Verbose = True, CliptoROIXYBoundingBox = False, FileSaveThreadManger = None, ProjectDataset = None) :                                
                ROIDefs = ROIDictionary.getROIDefs ()
                if ROIDefs.isROIArea (itemName) and ROIDictionary.isROIDefined(itemName) :
                   if (SaveDictionaryChanges) :
                        ROIDictionary._SaveUndoPoint ()
                   obj = ROIDictionary.getROIObject (itemName)
                                      
                   if (CliptoROIXYBoundingBox) :
                       clipXYMask = obj.getXYBoundingBoxMask () == 0
                       if not np.any (clipXYMask) :
                           CliptoROIXYBoundingBox = False
                           
                   sliceData = niftiVolume.getImageData (Z = StartSliceIndex)
                   imageShape = sliceData.shape  
                   ShowProgressDialog = ShowProgressDialogParentWindow != None 
                   # progress UI dialog 
                   if (ShowProgressDialog) :
                           progdialog = QProgressDialog("", "Cancel", 0, 100, ShowProgressDialogParentWindow)
                           progdialog.setMinimumDuration (0)
                           progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)
                           progdialog.setWindowTitle("Re-contouring Slices")
                           progdialog.setWindowModality(QtCore.Qt.ApplicationModal)
                           progdialog.setMaximum (100)                           
                   try:                     
                       if (ShowProgressDialog) :
                           progdialog.forceShow()                         
                           progdialog.setValue (0)
                           progdialog.update ()   
                           if ProjectDataset is not None :
                               ProjectDataset.getApp ().processEvents ()
                           
                       if (ContourIDList == None) :
                           ContourIDList = obj.getSelectedContours ()                           
                       # walk across current slice selection
                       roiSliceDictionary = ROIDictionary.getROISliceDictionary ()
                       for i, index in enumerate (selectionIndexList) :
                           if (ShowProgressDialog) :
                               progdialog.setValue (100 * i / len (selectionIndexList))                       
                           for contourID in ContourIDList :                                                      
                               if  (ShowProgressDialog) :
                                   QApplication.processEvents ()  
                                   if (progdialog.wasCanceled ()) :
                                       break
                               if (obj.contourDefinedForSlice (contourID, index)) :        # if contours are defined for slice, then attempt to recontour the slice
                                   if obj.isHumanEditedContour (contourID, index) :          
                                       if (Verbose) :
                                           print ("Contouring Human Edit Slice: %d" % (index))
                                       SliceContourType = "Human"
                                       Setting = 1                                                                  
                                   else:
                                       if (Verbose) :
                                           print ("Contouring Computer Edit Slice: %d" % (index))
                                       SliceContourType = "Computer"
                                       Setting = 0                                                                  
                                   origional_contour = obj.getContourMap (contourID, index, imageShape) 
                                   contour = np.copy (origional_contour)
                                   contour = RCC_AutoContourROIArea._GACContourSlice (niftiVolume.getImageData (Z = index), hounsfieldVisSettings, contour, FileSaveThreadManger = FileSaveThreadManger)
                                   if (CliptoROIXYBoundingBox) :
                                       contour[clipXYMask] = 0                                       
                                   if np.any (contour.astype (np.int8) - origional_contour.astype (np.int8)) :
                                       RCC_AutoContourROIArea._setSliceArea (roiSliceDictionary, ROIDictionary, imageShape, obj, contourID, index, contour, SliceContourType, Setting, SetSlicIndexCallBack = SetSlicIndexCallBack)                                                                    
                           if (SetSlicIndexCallBack != None) :
                               SetSlicIndexCallBack (index)             
                       if (SetSlicIndexCallBack != None) :
                            SetSlicIndexCallBack (StartSliceIndex)                   
                   finally:
                       if (ShowProgressDialog) :
                           if (progdialog.wasCanceled ()):
                               ROIDictionary.undo ()                   
                           progdialog.close()
                           if ProjectDataset is not None :
                               ProjectDataset.getApp ().processEvents ()
                   if (SaveDictionaryChanges) :
                       ROIDictionary.saveROIDictionaryToFile ()                                     
  
    # Called to update contouring for manually contoured sections    
    @staticmethod
    def contourSingleSlice_SliceView (sliceView, niftiVolume, ROIDictionary, hounsfieldVisSettings, itemName, selectionIndexList, StartSliceIndex, ShowProgressDialogParentWindow = None, SetSlicIndexCallBack = None, SaveDictionaryChanges = True, Verbose = True, CliptoROIXYBoundingBox = False, FileSaveThreadManger = None, ProjectDataset = None) :                                
                Axis = sliceView.getSliceAxis ()
                ROIDefs = ROIDictionary.getROIDefs ()
                if ROIDefs.isROIArea (itemName) and ROIDictionary.isROIDefined(itemName) :                   
                   if (SaveDictionaryChanges) :
                        ROIDictionary._SaveUndoPoint ()
                   obj = ROIDictionary.getROIObject (itemName)
                   
                   #volumeDimensions = niftiVolume.getSliceDimensions ()
                   #XYSliceShape = (volumeDimensions[0],volumeDimensions[1])
                   isAxixProjectionEnabled = sliceView.areAxisProjectionControlsEnabledForAxis ()                       
                                                         
                   if (CliptoROIXYBoundingBox) :
                       sliceData = niftiVolume.getImageData (StartSliceIndex, Axis = Axis,Coordinate = sliceView.getCoordinate (),AxisProjectionControls = sliceView.getAxisProjectionControls ())                                                                                                                   
                       imageShape = sliceData.shape 
                       if Axis == "Z" :                           
                           clipMask = obj.getXYBoundingBoxMask (imageShape, ReturnAxisProjectedMask = isAxixProjectionEnabled, niftislicewidget = sliceView) == 0                           
                       elif Axis == "X" :
                           clipMask = obj.getYZBoundingBoxMask (imageShape) == 0
                       else:                        
                           clipMask = obj.getXZBoundingBoxMask (imageShape) == 0
                       if not np.any (clipMask) :
                           CliptoROIXYBoundingBox = False
                                                         
                   ShowProgressDialog = ShowProgressDialogParentWindow != None 
                   # progress UI dialog 
                   if (ShowProgressDialog) :
                           progdialog = QProgressDialog("", "Cancel", 0, 100, ShowProgressDialogParentWindow)
                           progdialog.setMinimumDuration (0)
                           progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)
                           progdialog.setWindowTitle("Re-contouring Slices")
                           progdialog.setWindowModality(QtCore.Qt.ApplicationModal)
                           progdialog.setMaximum (100)                           
                   try:                     
                       if (ShowProgressDialog) :
                           progdialog.forceShow()                         
                           progdialog.setValue (0)
                           progdialog.update ()   
                           if ProjectDataset is not None :
                               ProjectDataset.getApp ().processEvents ()
                                                  
                       # walk across current slice selection                       
                       IDList = obj.getContourIDManger ().getAllocatedIDList ()
                       if len (IDList) > 0 :
                           contourID = IDList[0]
                       else:
                           contourID = obj.getContourIDManger ().allocateID()
                       roiSliceDictionary = ROIDictionary.getROISliceDictionary ()
                       SliceViewObjectsChanged = set ()
                       
                       
                       if isAxixProjectionEnabled :
                           roiID = obj.getROIDefIDNumber()
                           axisProjection = sliceView.getAxisProjectionControls ()
                           coordinate = sliceView.getCoordinate ()
                           projectionAxis = axisProjection.getROIProjectionSliceAxis (niftiVolume, ROIDictionary,  coordinate) 
                           projectedSlicesIndexs = set ()
                           for sliceIndex in projectionAxis.keys () :
                               if roiID in projectionAxis[sliceIndex] :
                                   projectedSlicesIndexs.add (sliceIndex)
                                                                    
                       for i, index in enumerate (selectionIndexList) :
                           if (ShowProgressDialog) :
                               progdialog.setValue (100 * i / len (selectionIndexList))                       
                           
                           if  (ShowProgressDialog) :
                               QApplication.processEvents ()  
                               if (progdialog.wasCanceled ()) :
                                   break                               
                           if Axis == "Z" :
                               if not isAxixProjectionEnabled :
                                   hasSlice = obj.hasSlice (index)                                  
                               else:
                                   hasSlice = index in projectedSlicesIndexs
                           elif Axis == "X" :
                               hasSlice = obj.hasAnnotationOnXAxis (index)   
                           else:
                               hasSlice =  obj.hasAnnotationOnYAxis (index)   
                           if (hasSlice) :        # if contours are defined for slice, then attempt to recontour the slice                                                                                                                                                                                                   
                               origional_contour = obj.getSliceViewObjectMask (sliceView, SliceNumber = index, ClipOverlayingROI = ROIDefs.hasSingleROIPerVoxel ())                                   
                               contour = np.copy (origional_contour)
                               sliceData = niftiVolume.getImageData (index, Axis = Axis, Coordinate = sliceView.getCoordinate (),AxisProjectionControls = sliceView.getAxisProjectionControls ())
                               contour = RCC_AutoContourROIArea._GACContourSlice (sliceData, hounsfieldVisSettings, contour, FileSaveThreadManger = FileSaveThreadManger)
                               if (CliptoROIXYBoundingBox) :
                                   contour[clipMask] = 0    
                               if Axis == "Z" and not isAxixProjectionEnabled :  
                                   changeMask = contour.astype(np.int8) - origional_contour.astype (np.int8)                                    
                                   if np.any (changeMask) :                                          
                                       RCC_AutoContourROIArea.applyChangeMaskToROIAndOverlappingObjectsAtSelectedSlice (index, obj, contourID, contour, origional_contour, roiSliceDictionary, ROIDictionary, ROIDefs)
                               else:
                                   changeMask = contour.astype(np.int8) - origional_contour.astype (np.int8)                                                                       
                                   obj.setSliceViewObjectMask (sliceView,  contourID, contour, index, changeMask, SliceViewObjectsChanged, ContourType = "Computer")
                               QApplication.processEvents ()                                   
                           if (SetSlicIndexCallBack is not None) :
                               SetSlicIndexCallBack (index, Axis=Axis)             
                       for obj in SliceViewObjectsChanged :
                           obj.endCrossSliceChange ()
                       if (SetSlicIndexCallBack is not None) :
                            SetSlicIndexCallBack (StartSliceIndex, Axis=Axis)                   
                   finally:
                       if (ShowProgressDialog) :
                           if (progdialog.wasCanceled ()):
                               ROIDictionary.undo ()                   
                           progdialog.close()
                           if ProjectDataset is not None :
                               ProjectDataset.getApp ().processEvents ()
                   if (SaveDictionaryChanges) :
                       ROIDictionary.saveROIDictionaryToFile ()    
                       
                       
    @staticmethod 
    def getROIIDOrder (tpl) :
        return tpl[0]
            
    @staticmethod 
    def applyChangeMaskToROIAndOverlappingObjectsAtSelectedSlice (index, obj, contourID, contour, origional_contour, roiSliceDictionary, ROIDictionary, ROIDefs, Type = "Human", SnappedHumanEditedSliceCount = 1) :        
        if ROIDictionary.isROIinAreaMode () and ROIDefs.hasSingleROIPerVoxel () :
            objName = obj.getName (ROIDefs)
            unlockedROISiblings = []
            if index in roiSliceDictionary :
                for sibling_obj in roiSliceDictionary[index] :
                    if sibling_obj != obj :
                        siblingName = sibling_obj.getName (ROIDefs)
                        if siblingName != objName :                    
                            if ROIDefs.isROILocked (siblingName) :                                
                                siblingAnnotation = sibling_obj.getSliceMap (index, contour.shape, ClipOverlayedObjects = ROIDefs.hasSingleROIPerVoxel ())                 
                                contour[siblingAnnotation > 0] = 0
                            else:
                                unlockedROISiblings.append (sibling_obj)
            changeMask = contour.astype(np.int8) - origional_contour.astype (np.int8)                                            
            if np.any (changeMask) :   
                changedObjList  = [(ROIDefs.getROIIdNumberOrder (obj.getROIDefIDNumber()), obj, contour)]
                for sibling_obj in unlockedROISiblings  :
                    siblingAnnotation = sibling_obj.getSliceMap (index, contour.shape, ClipOverlayedObjects = ROIDefs.hasSingleROIPerVoxel ())                 
                    newSiblingAnnotation = np.copy (siblingAnnotation)
                    newSiblingAnnotation[changeMask > 0] = 0
                    changeMask = newSiblingAnnotation.astype(np.int8) - siblingAnnotation.astype (np.int8)                                    
                    if np.any (changeMask) :
                        changedObjList.append ((ROIDefs.getROIIdNumberOrder (sibling_obj.getROIDefIDNumber()), sibling_obj, newSiblingAnnotation))
                changedObjList = sorted (changedObjList, key = RCC_AutoContourROIArea.getROIIDOrder)
                for obj_tpl in changedObjList :
                    order, ob, mask = obj_tpl
                    if ob is obj :                        
                        ob.setSliceROIAreaMask (contourID, index , mask.astype (np.uint8), mask.shape, Type, SnappedHumanEditedSliceCount = SnappedHumanEditedSliceCount)       
                    else:
                        if ob.isHumanEditedContour (contourID, index) :
                            contourtype = "Human"
                        else:
                            contourtype = "Computer"
                        SnappedHumanEditedSliceCount = ob.getHumanEditedContourSnapCount (contourID, index)
                        ob.setSliceROIAreaMask (contourID, index , mask.astype (np.uint8), mask.shape, contourtype, SnappedHumanEditedSliceCount = SnappedHumanEditedSliceCount)       
        else:
            obj.setSliceROIAreaMask (contourID, index , contour.astype (np.uint8), contour.shape, "Human", SnappedHumanEditedSliceCount = SnappedHumanEditedSliceCount)      
                                                   
    # Called to automatically generate contoured sections based on manually contoured sections 
    """
    @staticmethod
    def autoGrowVolume (ROIDictionary, niftiVolume, obj, hounsfieldVisSettings, GrowVolume = 1, FileSaveThreadManger = None) :                                                      
           selContours = obj.getContourIDManger ().getAllocatedIDList () 
           contourCount = len (selContours)
           if (contourCount == 0):
               return           
           
           roiSliceDictionary = ROIDictionary.getROISliceDictionary ()
           
           sliceData =  niftiVolume.getImageData (Z = 0)
           imageShape = sliceData.shape                                       
           
           sliceList = obj.getSliceNumberList ()
                                  
           selectionIndexList = []
           LastSliceIndex = niftiVolume.getZDimSliceCount () -1
           lastadded = -1
           for sliceindex in sliceList : 
               fslice = max (0, sliceindex - GrowVolume)
               lslice = min (sliceindex + GrowVolume + 1, LastSliceIndex)                              
               fslice = max (lastadded, fslice)              
               for index in range (fslice, lslice) :                             
                  lastadded = index          
                  selectionIndexList.append (index) 
                                                                      
           firstslice = selectionIndexList[0]
           lastslice = selectionIndexList[len (selectionIndexList) - 1]    
                          
           for contourID in selContours:                                          
               sliceIndexList = []
               changedSize = False
               processedSliceDict = {}
    
               for x in range (firstslice, lastslice + 1) :                                                                                                         
                   if obj.contourDefinedForSlice (contourID, x) : 
                      changedSize = True   
                      processedSliceDict[x] = (0, obj.getContourMap (contourID, x ,imageShape))   
                   else:                               
                      sliceIndexList.append (x)
                      if (obj.contourDefinedForSlice (contourID, x)) :
                           obj.removeSliceContour (contourID, x)                                       
                   
               DistanceFromHumanContour = -1                       
               while (changedSize and len (sliceIndexList) > 0) :                                                      
                   DistanceFromHumanContour += 1
                   changedSize = False                   
                   TempList = copy.copy (sliceIndexList)
                   
                   for index in TempList :                                     
                         #print ("testing %d" % (index))
                         lowerContour = None
                         upperContour = None
                         if (obj.contourDefinedForSlice (contourID, index)) :
                             if (index + 1 in processedSliceDict or index - 1 in processedSliceDict) :                                      
                                 processedSliceDict[index] = (DistanceFromHumanContour + 1, obj.getContourMap (contourID, index, imageShape))
                                 sliceIndexList.remove (index) 
                         else:    
                             if (index + 1 in processedSliceDict) :  
                                     #print ("found")
                                     distance, contour = processedSliceDict [index + 1]
                                     if (distance == DistanceFromHumanContour) :                                     
                                         lowerContour = RCC_AutoContourROIArea._GACContourSlice (niftiVolume.getImageData (Z = index), hounsfieldVisSettings, contour, FileSaveThreadManger = FileSaveThreadManger)
                                     
                             if (index - 1 in processedSliceDict) :
                                     distance, contour = processedSliceDict [index - 1]
                                     if (distance == DistanceFromHumanContour) :  
                                         upperContour = RCC_AutoContourROIArea._GACContourSlice (niftiVolume.getImageData (Z = index), hounsfieldVisSettings, contour, FileSaveThreadManger = FileSaveThreadManger)
                                     
                             if (RCC_AutoContourROIArea.isNpyArray(lowerContour) or RCC_AutoContourROIArea.isNpyArray(upperContour)) :
                                 if (not RCC_AutoContourROIArea.isNpyArray(lowerContour)) :         
                                     conn = upperContour
                                 elif (not RCC_AutoContourROIArea.isNpyArray(upperContour)):
                                     conn =  lowerContour
                                 else: 
                                     conn = RCC_AutoContourROIArea._andArea (lowerContour,upperContour)
                                     
                                 if (RCC_AutoContourROIArea._setSliceArea (roiSliceDictionary, ROIDictionary, imageShape, obj, contourID, index, conn, "Computer", 0)) : 
                                     changedSize = True                                 
                                     sliceIndexList.remove (index) 
                                     processedSliceDict[index] = (DistanceFromHumanContour + 1, conn)         """                                                   
           
           

    @staticmethod 
    def clipToXYMask (area, clipXYMask) :
        return np.bitwise_and (area.astype(bool), clipXYMask.astype(bool)).astype (float)
        
    _threadSliceGac = staticmethod (_threadSliceGac)
    
    
    # Called to automatically generate contoured sections based on manually contoured sections 
    @staticmethod
    def autoContour (niftiVolume, ROIDictionary, hounsfieldVisSettings, itemName, selContours, firstslice, lastslice, StartSliceIndex, selectionIndexList,  SetSlicIndexCallBack = None, ShowProgressDialogParentWindow = None, CliptoROIXYBoundingBox = False, AutoContourHumanSlicesOnce = True, ProjectDataset = None) :                        
               
               if ProjectDataset is None :
                   FileSaveThreadManger = None
               else:
                   FileSaveThreadManger = ProjectDataset.getROISaveThread ()
                   
               ShowProgressDialog =  ShowProgressDialogParentWindow != None
               ROIDefs = ROIDictionary.getROIDefs ()                       
               if ROIDefs.isROIArea (itemName) and ROIDictionary.isROIDefined(itemName) :
                   roiSliceDictionary = ROIDictionary.getROISliceDictionary ()
                   
                   obj = ROIDictionary.getROIObject (itemName)  
                                      
                   if (CliptoROIXYBoundingBox) :
                       clipXYMask = obj.getXYBoundingBoxMask ()
                   
                   
                   contourCount = len (selContours)
                   if (contourCount == 0):
                       return
                   ROIDictionary._SaveUndoPoint ()
                                          
                   sliceData =  niftiVolume.getImageData (Z = StartSliceIndex)
                   imageShape = sliceData.shape                                       
                   
                   if (ShowProgressDialog):
                       progdialog = QProgressDialog("", "Cancel", 0, 100, ShowProgressDialogParentWindow)
                       progdialog.setMinimumDuration (0)
                       progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)
                       progdialog.setWindowTitle("Auto-contouring Slices")
                       progdialog.setWindowModality(QtCore.Qt.ApplicationModal)
                       progdialog.setMaximum (100)                       
                   try:
                       if (ShowProgressDialog):
                           progdialog.forceShow()                   
                           progdialog.setValue (0)
                           progdialog.update()    
                           if ProjectDataset is not None :
                               ProjectDataset.getApp ().processEvents ()
                           
                       contourBlockSize = int (100 / contourCount)
                       baseProgress = -contourBlockSize 
                       for contourID in selContours:                           
                           #initalize clear all contours that are not human and contour human slices which haven't been processed at least once
                           sliceIndexList = []
                           changedSize = False
                           processedSliceDict = {}
    
                           baseProgress += contourBlockSize                       
                           
                           sliceList = obj.getSliceNumberList ()
                           for index in range (firstslice, lastslice + 1) :
                              if index not in sliceList :
                                  sliceList.append (index)
                           sliceList = sorted (sliceList)       
                           
                           # remove slices to recontour
                           for index, x in enumerate (sliceList) :                           
                               if (ShowProgressDialog) :
                                   precent = 0.25 * (float (index) / float (len (sliceList)))
                                   progdialog.setValue (baseProgress + float (contourBlockSize) * precent)
                                   QApplication.processEvents ()  
                                   if progdialog.wasCanceled () :
                                       break
                               
                               if (x >= firstslice and x <= lastslice) :
                                   if (len (selectionIndexList) <= 1 or x in selectionIndexList) :
                                       if not obj.isHumanEditedContour (contourID, x) :                                
                                           sliceIndexList.append (x)
                                           if (obj.contourDefinedForSlice (contourID, x)) :
                                               obj.removeSliceContour (contourID, x)
                                               if (SetSlicIndexCallBack != None) :
                                                   SetSlicIndexCallBack (x)                                                                              
                                   else:                                  
                                          changedSize = True 
                                          processedSliceDict[x] = (0, obj.getContourMap (contourID, x, imageShape))   
                               elif not obj.isHumanEditedContour (contourID, x) : 
                                   if (obj.contourDefinedForSlice (contourID, x)) :
                                               obj.removeSliceContour (contourID, x)
                                               if (SetSlicIndexCallBack != None) :
                                                   SetSlicIndexCallBack (x)
                                                                                          
                           #Recontour remaining uncontoured human contoured slices
                           sliceListToProcess = []
                           for index, x in enumerate (sliceList) :                           
                               if (ShowProgressDialog) :                                                                                            
                                   precent = 0.25 + (0.12 * (float (index) / float (len (sliceList))))
                                   progdialog.setValue (baseProgress + float (contourBlockSize) * precent)
                                   QApplication.processEvents ()  
                                   if progdialog.wasCanceled () :
                                       break
                               
                               if (x >= firstslice and x <= lastslice) :
                                   if (len (selectionIndexList) <= 1 or x in selectionIndexList) :
                                       if obj.isHumanEditedContour (contourID, x) : 
                                            changedSize = True   
                                            if (obj.getHumanEditedContourSnapCount (contourID, x) == 0) :                                                                                                          
                                                contour = obj.getContourMap (contourID, x, imageShape) 
                                                if (AutoContourHumanSlicesOnce) :
                                                    sliceListToProcess.append ((x, [contour]))                                                
                                                else :                                            
                                                    if (CliptoROIXYBoundingBox):
                                                        contour = RCC_AutoContourROIArea.clipToXYMask (contour, clipXYMask)
                                                    RCC_AutoContourROIArea._setSliceArea (roiSliceDictionary, ROIDictionary, imageShape, obj, contourID, x, contour, "Human", 1, SetSlicIndexCallBack = SetSlicIndexCallBack) 
                                                    processedSliceDict[x] = (0, obj.getContourMap (contourID, x ,imageShape))   
                                            else:
                                                print ("Skipping human edit slice, allready contoured: %d" % (x))
                                                processedSliceDict[x] = (0, obj.getContourMap (contourID, x ,imageShape))   
    
                           totalSliceToProcess = float (len (sliceListToProcess))
                           
                               
                           while (len (sliceListToProcess) > 0) :                        
                               if (ShowProgressDialog) :                                        
                                   precent = 0.37 + (0.13 * ((totalSliceToProcess - float (len (sliceListToProcess))) / totalSliceToProcess))
                                   progdialog.setValue (baseProgress + float (contourBlockSize) * precent)
                                   QApplication.processEvents ()  
                                   if progdialog.wasCanceled () :
                                       break
                            
                               JobsQueued = 0
                               procQueue = PlatformMP.processQueue (RCC_AutoContourROIArea._threadSliceGac, None, PoolSize = 3, SleepTime = 0, ProjectDataset = ProjectDataset)
                               while (len (sliceListToProcess) > 0 and JobsQueued < 2) :
                                   NiftiSliceIndexToContour1, ContourList = sliceListToProcess.pop () 
                                   img = niftiVolume.getImageData (Z = NiftiSliceIndexToContour1)
                                   if (hounsfieldVisSettings.maximizeWithinSliceContrast ()) :
                                                minHValue = np.min (img)            
                                                maxHValue = np.max (img)
                                                if (minHValue < -1024) :
                                                   minHValue = -1024
                                   else:  
                                                minHValue, maxHValue  = hounsfieldVisSettings.getCustomHounsfieldRange () 
                                  
                                   processParameters = (img, NiftiSliceIndexToContour1, ContourList, minHValue, maxHValue)
                                   cleanupParameters = None
                                   procQueue.queueJob (FileSaveThreadManger, processParameters, cleanupParameters)
                                   JobsQueued += 1
                               processResultList = procQueue.join ()                                  
                               for processResults in processResultList : 
                                   if processResults is not None :
                                       index, gacContourListResults = processResults
                                       if (CliptoROIXYBoundingBox):
                                          gacContourListResults[0] = RCC_AutoContourROIArea.clipToXYMask (gacContourListResults[0], clipXYMask)
                                       
                                       RCC_AutoContourROIArea._setSliceArea (roiSliceDictionary, ROIDictionary, imageShape, obj, contourID, index, gacContourListResults[0], "Human", 1, SetSlicIndexCallBack = SetSlicIndexCallBack)                                   
                                       processedSliceDict[index] = (0, obj.getContourMap (contourID, index ,imageShape))                                   
                                                                                                                            
                                                   
                           print (sliceIndexList)           
                           DistanceFromHumanContour = -1
                           initalSliceListSize = len (sliceIndexList)
                           while (changedSize and len (sliceIndexList) > 0) :                           
                               if (ShowProgressDialog) :
                                  if (progdialog.wasCanceled ()) : 
                                      break                                                             
                                  
                               DistanceFromHumanContour += 1
                               changedSize = False                   
                               TempList = copy.copy (sliceIndexList)
                               
                               sliceListToProcess = []
                               for index in TempList :             
                                     if (ShowProgressDialog) :
                                           if (progdialog.wasCanceled ()) : 
                                               break                                                             
                                         
                                     if (obj.contourDefinedForSlice (contourID, index)) :
                                         if (index + 1 in processedSliceDict or index - 1 in processedSliceDict) :                                      
                                             processedSliceDict[index] = (DistanceFromHumanContour + 1, obj.getContourMap (contourID, index, imageShape))
                                             sliceIndexList.remove (index) 
                                     else:    
                                         found = False
                                         if (((index + 1) in processedSliceDict) and ((index - 1) in processedSliceDict)) :  
                                             distance1, contour1 = processedSliceDict [index - 1]
                                             distance2, contour2 = processedSliceDict [index + 1]
                                             if (distance1 == DistanceFromHumanContour and distance2 == DistanceFromHumanContour) :  
                                                 sliceListToProcess.append ((index , [contour1, contour2]))
                                                 found = True
                                         if (not found)  :
                                             if (index - 1 in processedSliceDict) :
                                                 distance, contour = processedSliceDict [index - 1]
                                                 if (distance == DistanceFromHumanContour) :  
                                                     sliceListToProcess.append ((index, [contour]))
                                             elif (index + 1 in processedSliceDict) :
                                                 distance, contour = processedSliceDict [index + 1]
                                                 if (distance == DistanceFromHumanContour) :  
                                                     sliceListToProcess.append ((index, [contour]))
                                                 
                               while (len (sliceListToProcess) > 0) :
                                   if (ShowProgressDialog) :
                                       if (progdialog.wasCanceled ()) : 
                                           break                                                             
                                       precent = 0.5 + (0.5 * (float (initalSliceListSize - len (sliceIndexList)) / float (initalSliceListSize)))
                                       progdialog.setValue (baseProgress + float (contourBlockSize) * precent)
                                       QApplication.processEvents ()                                   
                                   
                                   JobsQueued = 0
                                   procQueue = PlatformMP.processQueue (RCC_AutoContourROIArea._threadSliceGac, None, PoolSize = 3,  SleepTime = 0, ProjectDataset = ProjectDataset)
                                   while (len (sliceListToProcess) > 0 and JobsQueued < 2) :
                                       NiftiSliceIndexToContour1, ContourList = sliceListToProcess.pop ()        
                                       img = niftiVolume.getImageData (Z = NiftiSliceIndexToContour1)
                                       #img = niftiVolume.getImageData (Z = NiftiSliceIndexToContour1)
                                       if (hounsfieldVisSettings.maximizeWithinSliceContrast ()) :
                                                minHValue = np.min (img)            
                                                maxHValue = np.max (img)
                                                if (minHValue < -1024) :
                                                   minHValue = -1024
                                       else:  
                                            minHValue, maxHValue  = hounsfieldVisSettings.getCustomHounsfieldRange ()
                                     
                                       processParameters = (img, NiftiSliceIndexToContour1, ContourList, minHValue, maxHValue)
                                       cleanupParameters = None
                                       procQueue.queueJob (FileSaveThreadManger, processParameters, cleanupParameters)
                                       JobsQueued += 1
                                   processResultList = procQueue.join ()  
                               
                                   for processResults in processResultList : 
                                       if (processResults is not None) :
                                           index, gacContourListResults = processResults
                                           if (len (gacContourListResults) == 1) :
                                                if (CliptoROIXYBoundingBox):
                                                    gacContourListResults[0] = RCC_AutoContourROIArea.clipToXYMask (gacContourListResults[0], clipXYMask)
                                           else:
                                               if (CliptoROIXYBoundingBox):
                                                    gacContourListResults[0] = RCC_AutoContourROIArea.clipToXYMask (gacContourListResults[0], clipXYMask)
                                                    gacContourListResults[1] = RCC_AutoContourROIArea.clipToXYMask (gacContourListResults[1], clipXYMask)
                                               gacContourListResults[0] = RCC_AutoContourROIArea._andArea (gacContourListResults[0], gacContourListResults[1])                                                                              
                                           
                                           if (RCC_AutoContourROIArea._setSliceArea (roiSliceDictionary, ROIDictionary, imageShape, obj, contourID, index, gacContourListResults[0], "Computer", 0, SetSlicIndexCallBack = SetSlicIndexCallBack)) : 
                                                     changedSize = True                                 
                                                     sliceIndexList.remove (index) 
                                                     processedSliceDict[index] = (DistanceFromHumanContour + 1, gacContourListResults[0])                                         
                                                                            
                       if (SetSlicIndexCallBack != None) :
                           SetSlicIndexCallBack (StartSliceIndex)
                       
                       print ("Auto Contour Area")
                       canceled = False
                   finally:
                       if (ShowProgressDialog) :
                           if (progdialog.wasCanceled ()):
                               ROIDictionary.undo ()
                               canceled = True
                           progdialog.close()    
                           if ProjectDataset is not None :
                               ProjectDataset.getApp ().processEvents ()
                               
                   ROIDictionary.saveROIDictionaryToFile ()                 
                   return canceled
