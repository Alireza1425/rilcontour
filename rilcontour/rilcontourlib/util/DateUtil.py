#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  9 10:06:53 2018

@author: m160897
"""
from PyQt5 import QtCore
from datetime import datetime

class TimeUtil :
    @staticmethod 
    def getTimeNow () :
        hour = datetime.now ().hour 
        minute = datetime.now ().minute 
        second = datetime.now ().second 
        return (hour, minute, second)
    
    @staticmethod     
    def timeToString (time) :
        return ("%d:%d:%d" % tuple (time))
    
    @staticmethod     
    def timeToSortableInt (time) :
        hour, minute, second =  tuple (time)
        return (hour *10000) + (minute *100) + second

    @staticmethod     
    def timeToSortableString (time) :
        hour, minute, second =  tuple (time)
        return str((hour *10000) + (minute *100) + second)
    
class DateUtil:
    
    @staticmethod
    def getSortableString (date) :
        date = tuple (date)
        qDate = DateUtil.dateToQDate (date)
        return qDate.toString("yyyyMMdd")
        
    @staticmethod
    def QDateToDate (qDate) :
        return (qDate.month (), qDate.day (), qDate.year ())
    
    @staticmethod
    def isDateGreaterThan (date1, date2) :
        date1 = tuple (date1)
        date2 = tuple (date2)
        testdate1 = DateUtil.dateToQDate (date1)
        testdate2 = DateUtil.dateToQDate (date2)
        return testdate1 > testdate2
    
    @staticmethod
    def isDateInFuture (date) :
        date = tuple (date)
        testdate = DateUtil.dateToQDate (date)
        today = QtCore.QDate.currentDate()
        return testdate > today
    
    @staticmethod
    def today () :
        date = QtCore.QDate.currentDate()
        return DateUtil.QDateToDate (date)
        
    @staticmethod
    def dateToQDate (date) :
        date = tuple (date)
        month, day,  year = date
        return QtCore.QDate (year, month, day)
        
    @staticmethod
    def dateToString (date) :
        date = tuple (date)
        dob = DateUtil.dateToQDate (date)
        return dob.toString("ddd MMMM d, yyyy")
    
    @staticmethod 
    def getAgeYears (date) :
         date = tuple (date)
         dob = DateUtil.dateToQDate (date)
         today = QtCore.QDate.currentDate()
         age_years = today.year () - dob.year ()
         if (today.month ()< dob.month ()) :
             age_years -= 1
         elif (today.month ()== dob.month () and today.day () < dob.day ()) :
             age_years -= 1
         return age_years
    
    @staticmethod 
    def getAgeYearsAtDate (date,AtDate) :
         date = tuple (date)
         AtDate = tuple (AtDate)
         dob = DateUtil.dateToQDate (date)
         today = DateUtil.dateToQDate (AtDate)
         age_years = today.year () - dob.year ()
         if (today.month ()< dob.month ()) :
             age_years -= 1
         elif (today.month ()== dob.month () and today.day () < dob.day ()) :
             age_years -= 1
         return age_years
