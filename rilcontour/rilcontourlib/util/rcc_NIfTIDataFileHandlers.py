#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jan  5 17:50:11 2017

@author: m160897
"""
import os
import weakref
from rilcontourlib.util.rcc_util import PythonVersionTest
        
    
class PesscaraDF (object) :
    def __init__ (self, pesscarainterface, treewidgetNode, context = None, cachedFile = False, EnableDestructor = True) :
           self._closeEnabled = True
           self._closeCalled = False
           self._datafile = None
           self._pesscarainterface = pesscarainterface
           self._treewidgetNode = treewidgetNode         
           self._context = None           
           self._finalizer = None
           if PythonVersionTest.IsPython3 () :
               if EnableDestructor :
                   self._finalizer = weakref.finalize (self, PesscaraDF._finalizeObj, self._datafile, self._context)
           
           if (treewidgetNode is None or treewidgetNode.getSeries ()  is None) :
               self._datafile = None
           else:
               if (context is not None and context != "image") :
                   self._context = context
                   self._datafile = self._pesscarainterface.getSeriesNIfTIDataFile (treewidgetNode.getSeries (), self._context)
               else:
                   self._datafile = None
                   self._context = "image"
                   self._datafile = self._pesscarainterface.getSeriesNIfTIDataFile (treewidgetNode.getSeries (), self._context)
                   # dataset file caching code added but has bugs get error with datasets not being found.  the performance bonus to adding this isn't huge so.. Not adding at the moment
                   """if (not cachedFile) :
                       self._datafile = self._pesscarainterface.getNIfTIDataSourceCache ().getNIfTI (treewidgetNode.getSeries ())
                       #self._datafile = self._pesscarainterface.getSeriesNIfTIDataFile (treewidgetNode.getSeries (), self._context)
                   else:
                        self._pesscarainterface.getNIfTIDataSourceCache ().cacheNIfTI (treewidgetNode.getSeries ())"""
        
    if PythonVersionTest.IsPython2 () :
        def __del__ (self) :
            PesscaraDF._finalizeObj (self._datafile, self._context)
        
    def copyForChildProcessCopyDisableFinalizer (self) :
        self._closeEnabled = False
        copy = PesscaraDF (self._pesscarainterface, None, self._context, EnableDestructor = False)
        copy._closeEnabled = False
        copy._treewidgetNode = self._treewidgetNode
        copy._context = self._context
        copy._datafile = self._datafile
        if copy._finalizer is not None :            
            copy._finalizer.detach()
            copy._finalizer = None 
        return copy
    
    def enableClose (self) :
        self._closeEnabled = True
        if self._closeCalled :
            self.closeDataFile ()
    
    @staticmethod
    def _finalizeObj (datafile, context) :
         if (datafile is not None) :
             if (context == "image") :                 
                 # dataset file caching code added but has bugs get error with datasets not being found.  the performance bonus to adding this isn't huge so.. Not adding at the moment
                 #self._pesscarainterface.getNIfTIDataSourceCache ().closeNIfTI (self._datafile)             
                 try :
                     os.remove (datafile)
                     print ("Deleting temporary file " + datafile)
                 except:
                     print ("Error deleting temporary file " + datafile)
             else:
                 try :
                     os.remove (datafile)
                     print ("Deleting temporary file " + datafile)
                 except:
                     print ("Error deleting temporary file " + datafile)       
      
          
    def hasDataFile (self) :
         return self._datafile != None
             
    def getFilePath(self) : 
           return self._datafile
           
    def closeDataFile (self) : 
         self._closeCalled = True
         if self._closeEnabled :
             if (self._datafile is not None) :
                 if (self._context == "image") :
                     
                     # dataset file caching code added but has bugs get error with datasets not being found.  the performance bonus to adding this isn't huge so.. Not adding at the moment
                     #self._pesscarainterface.getNIfTIDataSourceCache ().closeNIfTI (self._datafile)             
                     try :
                         os.remove (self._datafile)
                         print ("Deleting temporary file " + self._datafile)
                     except:
                         print ("Error deleting temporary file " + self._datafile)
                 else:
                     try :
                         os.remove (self._datafile)
                         print ("Deleting temporary file " + self._datafile)
                     except:
                         print ("Error deleting temporary file " + self._datafile)
                 self._datafile = None
                                                              
             
class FileSystemDF (object) :
    def __init__ (self,  treewidgetNode = None, NIfTIFilePath = None, EnableDestructor = True) :
        self._closeCalled = False
        self._closeEnabled = True
        self._treewidgetNode = treewidgetNode                
        self._NIfTIFilePath = NIfTIFilePath                
        
    def getFilePath (self) : 
           if self._treewidgetNode is not None :
               return self._treewidgetNode.getPath ()      
           else:
               return self._NIfTIFilePath           
           
    def copyForChildProcessCopyDisableFinalizer (self) :
          self._closeEnabled = False
          copy =   FileSystemDF (self._treewidgetNode, self._NIfTIFilePath, EnableDestructor = False)
          copy._closeEnabled = True
          return copy
    
    def enableClose (self) :
        self._closeEnabled = True
        if self._closeCalled :
            self.closeDataFile ()
            
    def hasDataFile (self) :
        return self._treewidgetNode is not None or self._NIfTIFilePath is not None 
    
        
    def closeDataFile (self) : 
        self._closeCalled = True
        if self._closeEnabled :
            self._treewidgetNode = None
            self._NIfTIFilePath = None