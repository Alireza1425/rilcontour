#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 26 16:10:23 2018

@author: m160897
"""
import json    
import os
import shutil
import tempfile
from rcc_util import PlatformMP

def _singleCopy (roiFileMaskTupl) :        
        niftiFilePath, roiFilePath, datasetIndex, nifti_outputdir, roi_outputdir  = roiFileMaskTupl                               
        extention = LoadDatasetFromRilContourExportPath._getExtention (niftiFilePath)        
        if not LoadDatasetFromRilContourExportPath._copyFile (niftiFilePath, os.path.join (nifti_outputdir,"Nifti_%d.%s" % (datasetIndex, extention))) :
           return False
       
        extention = LoadDatasetFromRilContourExportPath._getExtention (roiFilePath)
        if not LoadDatasetFromRilContourExportPath._copyFile (roiFilePath, os.path.join (roi_outputdir,"ROIMask_%d.%s" % (datasetIndex, extention) )) :
           return False
        return True
    
class LoadDatasetFromRilContourExportPath  :    
        
    @staticmethod
    def LoadRilContourDataset (dsDir) :
        
        if (dsDir.endswith (os.sep)) :
            dsDir = dsDir[:-len (os.sep)]
            
        timestamp = "" 
        try :
            dataset = open (dsDir + "/MaskDescriptionFileLst.txt","rt")
        except:
            print ("Error: Could not open MaskDescriptionFileLst.txt for reading.")
            return None
        
        datasetList = []
    
        try :
            for path in dataset :    
                if (timestamp == "") :
                  timestamp = path.strip ()
                  continue            
                
                if os.sep == "/" :
                    relativeFilePath = path.replace ("\\","/").strip ()       
                elif os.sep == "\\" :                
                    relativeFilePath = path.replace ("/","\\").strip ()       
                else:
                    print ("Error: %s is a expected directory seperator." % os.sep)
                    return None    
                niftiDSDescriptionPath = dsDir + relativeFilePath[1:]
                datasetList.append (niftiDSDescriptionPath)
            dataset.close ()
        except:
            print ("A error occured reading MaskDescriptionFileLst.txt.")
            return None
              
        Dataset_Nifti_And_Mask_File_List = []
        for datafile in datasetList :
            try:
                subDS = open (datafile,"rt")            
                header = ""    
                directory, _ = os.path.split (datafile)        
                for line in subDS :
                    if (header == "") :
                        header = line.strip ()
                        continue
                
                    description = json.loads (line.strip ())                        
                    try :                    
                        if (type (description) == dict) :
                            maskfile = description["NIfTI_Mask_File"]
                            contour_sliceAxis = description["Contour_Slice_Axis"]      
                            ROI_List = description["ROI_Mask_List"]     
                            BackgroundValueMask = description["Background_Voxel_Value"]   
                            DefaultMaskValue = description["Default_Mask_Value"]   
                            InSlicePointSize = description["In_slice_point_size"]     
                            CrossSlicePointSize = description["Cross_slice_point_size"]     
                            BinaryOrMaskValue = description["Binary_OR_mask_value"]      
                            exportedNiftiFilename = description["exported_nifti_file_name"]
                            sourceFileName = description["source_nifti_file"] 
                            roiFileName  = description["ROIFileName"]    
                            RoiDataFilePath  = description["ROI_data_file_path"]   
                            DicomTagFileName  = description["Dicom_TagFileName"]      
                            LogTime   = description["log_time(h:m:s:ms)"]
                            LogDate  = description["log_date(m:d:y)"]                                                                                
                        elif len (description) == 15 : # new ROI description format
                            maskfile, contour_sliceAxis , ROI_List, BackgroundValueMask, DefaultMaskValue, InSlicePointSize, CrossSlicePointSize, BinaryOrMaskValue, exportedNiftiFilename, sourceFileName, roiFileName, RoiDataFilePath, DicomTagFileName,LogTime, LogDate  = description                    
                        elif len (description) == 14 : # old ROI description format does not have tag file
                            maskfile, contour_sliceAxis , ROI_List, BackgroundValueMask, DefaultMaskValue, InSlicePointSize, CrossSlicePointSize, BinaryOrMaskValue, exportedNiftiFilename, sourceFileName, roiFileName, RoiDataFilePath, LogTime, LogDate  = description                    
                        elif len (description) == 13 : # old ROI description format does not have tag file
                            maskfile,  ROI_List, BackgroundValueMask, DefaultMaskValue, InSlicePointSize, CrossSlicePointSize, BinaryOrMaskValue, exportedNiftiFilename, sourceFileName, roiFileName, RoiDataFilePath, LogTime, LogDate  = description                    
                        else:
                            print ("A error occured decoding rilcontour exported description.  Rilcontour export format may have changed.")
                            return None
                        
                        maskSourcePath = os.path.join (directory, maskfile)
                        niftiSourcePath = os.path.join (directory, exportedNiftiFilename)            
                        #ROI_List = [["ROI_name", "ROI_type", "ROI_mask_value"],  ]
                        #BinaryOrMaskValue = True/False how to interpret ROI mask 
                        if (os.path.isfile (niftiSourcePath) and os.path.isfile (maskSourcePath)) :
                            Dataset_Nifti_And_Mask_File_List.append ([niftiSourcePath, maskSourcePath])
                        else:
                            if not os.path.isfile (niftiSourcePath) :
                                print ("Error: Nifti file source not found: " + niftiSourcePath)
                            if not os.path.isfile (maskSourcePath) :
                                print ("Error: Mask file source not found: " + maskSourcePath)
                    except:
                        print ("A error occured decoding rilcontour exported description.  Rilcontour export format may have changed.")
                        return None
                    
                subDS.close ()
            except:
                print ("A error occured reading: " + datafile)
                
        return Dataset_Nifti_And_Mask_File_List

    @staticmethod
    def _createDir  (path) :        
        if not os.path.exists (path) :
            try:
                os.mkdir (path)
                return True
            except:
                print ("A error occured trying to create: " + path)
                return False
        if os.path.isfile (path) :
            print ("Error " + path + " is a file.")
            return False
        return True
    
    @staticmethod
    def _copyFile (sourcePath, destPath) :        
        try :
            shutil.copyfile (sourcePath, destPath)
            print ("Copied: %s to %s" % (sourcePath, destPath))
            return True
        except:
            print ("A error occured trying to copy: \nFrom: %s\nTo: %s" % (sourcePath, destPath))
            return False
        
    @staticmethod
    def _getExtention (filename)    :
        if (filename.lower().endswith (".nii.gz")) :
            return "nii.gz"
        else:
            fileparts = filename.split (".")
            return fileparts[len (fileparts) -1]
    
    _singleCopy = staticmethod (_singleCopy)        
                
    @staticmethod
    def FlattenDataset (RilContourDirSource, nifti_outputdir = None, roi_outputdir = None, MPWorkers = 1) :        
        
        if (nifti_outputdir is None) :
            nifti_outputdir = tempfile.mkdtemp ()
            
        if (roi_outputdir is None) :
            roi_outputdir = nifti_outputdir
        
        if (not LoadDatasetFromRilContourExportPath._createDir (nifti_outputdir)) :
            return None
        if (not LoadDatasetFromRilContourExportPath._createDir (roi_outputdir)) :
            return None
                
        niftiROIMaskList = LoadDatasetFromRilContourExportPath.LoadRilContourDataset (RilContourDirSource)
        if niftiROIMaskList is None :
            return None
        
        if len (niftiROIMaskList) <= 0 :
            print ("No datasets exported")        
                
        for datasetIndex in range (len (niftiROIMaskList)) :        
            niftiROIMaskList[datasetIndex] +=  [datasetIndex, nifti_outputdir, roi_outputdir]
        FileSaveManager = None
        result = PlatformMP.processMap (LoadDatasetFromRilContourExportPath._singleCopy, niftiROIMaskList, FileSaveManager, PoolSize = int(MPWorkers))  
            
        if (False in result) :
            return None
        
        print ("%d Files Pairs Exported" % len (niftiROIMaskList))
        
        if (nifti_outputdir == roi_outputdir) :
            return (nifti_outputdir,)
        return (nifti_outputdir, roi_outputdir)
        
if __name__ == "__main__":            
    
    #To use call either 
    #
    # pass rilcontour export directory to import data from
    #
    # returns a list of nifti and mask file name pairs, 
    # returns None if a error occured. Error Message is written to standard out
    #
    datasetList = LoadDatasetFromRilContourExportPath.LoadRilContourDataset ("/research/projects/RCC_WorkingDatasets/RCCDataExport")
        
    #To get flattend version 
    #
    # pass rilcontour export directory to import data from First Parameter
    #
    # nifti_outputdir = optional output directory to export nifti data to. if not supplied will export to a temp directory.
    #
    # mask_outputdir = optional mask output directory to export mask data to. if not supplied will export to nifti_outputdir
    #
    # MPWorkers = integer number of workers processes to use to copy dataset
    #
    # returns None if method fails, returns a tuple containing (nifti_outputdir) if nifti_outputdir == mask_outputdir otherwise returns (nifti_outputdir, mask_outputdir)
    LoadDatasetFromRilContourExportPath.FlattenDataset ("/research/projects/RCC_WorkingDatasets/RCCDataExport", nifti_outputdir = "/fastIOlocal/test", MPWorkers = 16)    
    