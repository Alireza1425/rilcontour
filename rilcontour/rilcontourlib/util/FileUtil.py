#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb  3 12:44:30 2019

@author: KennethPhilbrick
"""
import os
from six import *
import tempfile
try:
    from pathlib import Path
    print ("Importing: pathlib")
except:
    from pathlib2 import Path
    print ("Importing: pathlib2")


class ScanDir :
  @staticmethod 
  def scandir (path) :
      try :
          return os.scandir (path)
      except:
          import scandir
          iterator =  scandir.scandir (path)
          return iterator

class FileUtil : 
     
     @staticmethod 
     def makeSafeName (filename) :
         unsafeCharList = [chr(num) for num in range (32)]
         unsafeCharList = '<>:"/\|?*' + "'" + "".join (unsafeCharList)         
                          
         fileNameSet = set (filename)
         if len (fileNameSet) > len (unsafeCharList) :
             SearchList = unsafeCharList
             SearchSet = fileNameSet
         else:
             SearchList = fileNameSet
             SearchSet = set (unsafeCharList)         
         HasUnsafeChar = False          
         for ch in SearchList :
             if ch in SearchSet :
                 HasUnsafeChar = True
                 break         
         if not HasUnsafeChar :
             return filename
         
         newName = []
         for ch in filename :
             if ch in unsafeCharList :
                 newName.append ("_")
             else:
                 newName.append (ch)
         return "".join (newName)
     
     @staticmethod
     def correctOSPath (path) :
        if path is None :
            return
        if isinstance (path, list) :
            adjustedList = []
            for item in path :
                adjustedList.append (FileUtil.correctOSPath (item))
            return adjustedList
        else:                
            if os.path.sep != "\\" and "\\" in path :
                return path.replace ("\\", os.path.sep)
            elif os.path.sep != "/" and "/" in path :
                return path.replace ("/", os.path.sep)
            return path
    
     @staticmethod
     def testCanReadFromFile (path) :
         try :             
             return os.path.isfile (path) and os.access (path,os.F_OK | os.R_OK)                 
         except: 
             return False
    
     @staticmethod
     def testCanReadFromDirectory (path) :
         try :             
             return os.path.isdir (path) and os.access (path,os.F_OK | os.R_OK)                 
         except: 
             return False
     
     @staticmethod
     def testCanReadWriteFromDirectory (path) :
         try :             
             if os.path.isdir (path) and os.access (path,os.F_OK | os.W_OK | os.R_OK):
                 tf = tempfile.TemporaryFile (dir = path)
                 tf.close ()
                 del tf
                 testdir = tempfile.mkdtemp (dir = path)
                 tf = tempfile.TemporaryFile (dir = testdir)
                 tf.close ()
                 del tf
                 os.rmdir (testdir)
                 return True
             return False
         except: 
             return False
    
     @staticmethod 
     def removeExtension (path, extensionLst) :
         lPath = path.lower () 
         for extension in extensionLst :
             if lPath.endswith (extension.lower ()):
                 length = len(extension)
                 if length > 0 :
                     return path[:-length], extension                 
                 return path, extension
         return path, None 
         
     @staticmethod 
     def setPermissionForFilesUnderDirectory (path) :
         dirqueue = [path]
         while len (dirqueue) > 0 :
             dirpath = dirqueue.pop ()
             try:
                 fileIterator = ScanDir.scandir(dirpath)
                 for fobj in fileIterator :                                          
                     if not fobj.name.startswith (".") :                         
                         if fobj.is_dir ()  :
                             dirqueue.append (fobj.path)
                         else:
                             FileUtil.setFilePermissions (fobj.path, FileSavePermissions = 0o775) 
             finally:
                 try :
                     fileIterator.close ()
                 except:
                     pass
         
     @staticmethod 
     def setFilePermissions (path, FileSavePermissions= 0o775): 
        FileSavePermissions = 0o775
        if FileSavePermissions is not None and os.path.exists (path) :
            try :
               os.chmod (path, FileSavePermissions)
               print ("Setting File Permissions: %s to %s" % (path, str (FileSavePermissions)))
            except:
               print ("")
               print ("A error occured trying to set file permissions")
               print (path)
               print ("Check file system privlages for the data file.")
               print ("")
               pass
     
     @staticmethod 
     def getFilePermissions (path): 
        try :
            filestats = os.stat (path)
            print ("Getting File Permissions: %s" % (path))
            return filestats.st_mode
        except:
            return None
    
     @staticmethod
     def resolvePath (pathstr, ReplaceTokens = {}, TryMakeTerminalDir = False):
        if os.path.exists (pathstr) :
            return pathstr
        try:
            pathList = FileUtil.getPathList (pathstr)
            while "" in pathList :
                del pathList[pathList.index ("")]
            
            for key, value in ReplaceTokens.items () :
               while (key in pathList) :
                   pathList[pathList.index (key)] = value
                   
            if pathList[0] == os.path.sep and len (pathList) > 1:
               pathList[0] = pathList[0] + pathList[1]
               del pathList[1]
               
            if pathList[0] == "~" :
                homePath = FileUtil.getPathList (str(Path.home()))
                while "" in homePath :
                    del homePath[homePath.index ("")]
                
                for key, value in ReplaceTokens.items () :
                   while (key in homePath) :
                       homePath[homePath.index (key)] = value
                
                if homePath[0] == os.path.sep and len (homePath) > 1:
                    homePath[0] = homePath[0] + homePath[1]
                    del homePath[1]
                del pathList[0]
                pathList = homePath + pathList
            while len (pathList) > 0:
                if ".." in pathList :
                    index = pathList.index ("..")
                    if index > 0 :
                        del pathList[index-1]
                        del pathList[index-1]
                else:
                    break
            path = os.path.sep.join (pathList)
            if os.path.exists (path) :
                return path
            if TryMakeTerminalDir :
                try :
                    os.mkdir (path)
                    if os.path.exists (path) :
                        return path
                except:
                    pass
            return pathstr
        except:
           return pathstr
   
     @staticmethod
     def changeExtention (filename, oldextention, newextention) :
         if (filename.lower().endswith (oldextention.lower ())) :
             length = len (oldextention)
             if length > 0 :
                 return filename[:-length] + newextention             
         return filename + newextention
    
     @staticmethod
     def getPathList (path) :
         result = []
         while True :
             dirname, fname = os.path.split (path)
             if len (fname) > 0:
                 result.append (fname)
                 path = dirname
             else:
                 result.append (path)
                 break
         result.reverse ()
         return result
                 
     
     @staticmethod
     def createPath (path, hasfilename = False) :
       if (os.path.isfile (path) or os.path.isdir (path)):
           return
       if (hasfilename):      
           path, filename = os.path.split (path)
       pathstack = []
       while (path != "" and not os.path.isdir (path)) :
           path, dirname = os.path.split (path)
           pathstack.append (dirname)
       UndoMkDirs = []
       ExceptionOccured = False
       while (len (pathstack) > 0) :
           dirname = pathstack.pop ()
           path = os.path.join (path, dirname)
           if (not os.path.isdir (path)) :
               try :
                   os.makedirs(path)
                   UndoMkDirs.append (path)
               except:
                  if not os.isdir (path) :
                      ExceptionOccured = True
                      break         
       if ExceptionOccured :
           while len (UndoMkDirs) > 0 :
               try :
                   os.rmdir (UndoMkDirs.pop ())
               except :
                   pass
                   
        