#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  4 16:59:58 2017

@author: m160897
"""
from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import  QDialog
from rilcontourlib.util.rcc_util  import ResizeWidgetHelper
from rilcontourlib.ui.qt_ui_autogen.rcc_ScanPhaseEditDlgAutoGen import Ui_ScanPhaseEditorDlg


class RCC_ScanPhaseEditDlg (QDialog) :
    
    def __init__ (self, parent, scanphaselines) :        
        QDialog.__init__ (self, parent)                
                    
        scanphasetxt = ""
        self._Result = []
        for lines in scanphaselines :            
            self._Result.append (lines)
            if (lines != "Unknown") :
                if (len (scanphasetxt) == 0) :
                    scanphasetxt = lines                
                else:
                    scanphasetxt += "\n"  + lines                
        
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)         
        self.ui = Ui_ScanPhaseEditorDlg ()        
        self.ui.setupUi (self)           
        self._resizeWidgetHelper = ResizeWidgetHelper ()        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.scanPhaseTextEdit)        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.OkBtn)        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.CancelBtn)                    


        self.ui.scanPhaseTextEdit.setLineWrapMode ( self.ui.scanPhaseTextEdit.NoWrap)
        self.ui.scanPhaseTextEdit.setText (scanphasetxt)
        self.ui.CancelBtn.clicked.connect (self.cancelBtnClk )    
        self.ui.OkBtn.clicked.connect (self.okBtnClk )   
        self.setMinimumSize (self.size ())
    
    def _internalWindowResize (self, newsize):                            
        if (self._resizeWidgetHelper != None) :                                                                       
            self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.scanPhaseTextEdit, newsize)                    
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.scanPhaseTextEdit, newsize)                            
            self._resizeWidgetHelper.setWidgetYPos     (self.ui.CancelBtn, self.height () - 27)            
            self._resizeWidgetHelper.setWidgetYPos     (self.ui.OkBtn, self.height () - 27)            
            self._resizeWidgetHelper.setWidgetXPos     (self.ui.CancelBtn, self.width () - 100)            
            self._resizeWidgetHelper.setWidgetXPos     (self.ui.OkBtn, self.width () - 190)            
                        
    
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)         
        self._internalWindowResize ( qresizeEvent.size () ) 
        
    def cancelBtnClk (self) :
        self.reject ()
        self.close ()
    
    def okBtnClk (self) :
        result = []
        result.append ("Unknown")
        
        txt = self.ui.scanPhaseTextEdit.toPlainText()
        lines = txt.split ("\n")
        
        for line in lines :
            text = line.strip ()
            if ((len (text) > 0) and (text not in result)) :
                result.append (text)
        
        self._Result = result
        self.accept ()
        self.close ()
