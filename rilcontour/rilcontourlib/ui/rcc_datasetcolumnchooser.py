#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 19 13:07:00 2017

@author: m160897
"""

from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import   QDialog, QComboBox, QItemDelegate,  QAbstractItemView
from PyQt5.QtGui import QStandardItemModel
from PyQt5.QtCore import QModelIndex#, QAbstractTableModel
from six import *
from rilcontourlib.ui.qt_ui_autogen.RCC_DatasetColumnChooserAutogen import Ui_RCC_DatasetColumnChooser
import os
from rilcontourlib.util.rcc_util import ResizeWidgetHelper, HeaderObject, Tag

class QListDelegate (QItemDelegate) :    
    def __init__ (self, columnchooserlistorder, datasetTagManager, datasetUIHeader, OldHeaderObjList, RegistrationTag = False, parent = None) :         
         QItemDelegate.__init__ (self, parent)                  
         self._registrationTag = RegistrationTag
         self._parameterList = columnchooserlistorder         
         self._DatasetUIHeader = datasetUIHeader
         self._DatasetTagManager = datasetTagManager
         self._OldHeaderObjList = OldHeaderObjList
         
    def createEditor(self, parent, options, qModelIndex) :       
        editor = QComboBox (parent)
        editor.setAutoFillBackground(True)
        for x in self._parameterList :
            editor.addItem (str (x))         
        index = int (qModelIndex.model().data(qModelIndex, QtCore.Qt.UserRole))                
        editor.setCurrentIndex(index) 
        return editor
        
    def setEditorData(self, qWidgetEditor, qModelIndex) :
        # Get the value via index of the Model        
        index = int (qModelIndex.model().data(qModelIndex, QtCore.Qt.UserRole))        
        qWidgetEditor.setCurrentIndex(index) 
        
        
    def setModelData(self,  qWidgetEditor, qAbstractModel, qModelIndex) :        
        currentIndex = qWidgetEditor.currentIndex()                
        currentText = qWidgetEditor.currentText() 
        qAbstractModel.setData(qModelIndex, str (currentIndex), QtCore.Qt.UserRole)        
        qAbstractModel.setData(qModelIndex, currentText, QtCore.Qt.EditRole)        
        rowindex = qModelIndex.row ()
        
        if (currentIndex == 0) :
            self._DatasetUIHeader.setHeaderByIndex (rowindex, HeaderObject ("Description","Description"))
        elif (currentIndex == 1 and self._registrationTag) :
            self._DatasetUIHeader.setHeaderByIndex (rowindex, HeaderObject ("Registration","Registration"))
        else:
            if (self._registrationTag) :
                tagIndex = currentIndex - 2
            else:
                tagIndex = currentIndex - 1        
            if (tagIndex < self._DatasetTagManager.getCustomTagCount ()) :
                tag = self._DatasetTagManager.getCustomTagByIndex (tagIndex)
            else:
                tagIndex = tagIndex - self._DatasetTagManager.getCustomTagCount ()
                if (tagIndex < self._DatasetTagManager.getInternalTagCount ()) :
                    tag = self._DatasetTagManager.getInternalTagByIndex (tagIndex)                
                else: 
                    tag = None
                    tagIndex = tagIndex - self._DatasetTagManager.getInternalTagCount ()
            
            if (tag is not None) : # tag was found
                self._DatasetUIHeader.setHeaderByIndex (rowindex, HeaderObject ("Tag",tag.getName (), tag.getIdentifer ()))
            else:
                # tag was not found old header value chosen
                self._DatasetUIHeader.setHeaderByIndex (rowindex, self._OldHeaderObjList[tagIndex])
        
    def updateEditorGeometry(self, qWidgetEditor, qStyleOptionViewItem, qModelIndex) :
        qWidgetEditor.setGeometry(qStyleOptionViewItem.rect)
        
        
class MyModel(QStandardItemModel) :
        
    def __init__ (self, datasetUIHeader, row, columns, parent) :        
        QStandardItemModel.__init__ (self, row, columns, parent)        
        self._DatasetUIHeader = datasetUIHeader
    
    
    def flags (self, qModelIndex) :                
        column = qModelIndex.column ()
        if (column == 0) :
            return  QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable 
        if (column == 1) :
            return QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled 

    def setData(self, QModelIndex, value, role):      
        if (role == QtCore.Qt.CheckStateRole):                
            currentData = self.data(QModelIndex,QtCore.Qt.EditRole)
            if (currentData == "Yes") :
                setValue = "No"                                
            else:
                setValue = "Yes"                             
            self.setData(QModelIndex,setValue, QtCore.Qt.EditRole)      
            
            row = QModelIndex.row ()
            headerobject = self._DatasetUIHeader.getHeaderObjectAtIndex (row)
            headerobject.setVisible ( setValue == "Yes")
            self._DatasetUIHeader.setHeaderByIndex (row, headerobject)
            
        return QStandardItemModel.setData (self, QModelIndex, value, role)        
    
    def data(self, qModelIndex, role) :                    
        if (role == QtCore.Qt.CheckStateRole) :                                
            column = qModelIndex.column ()            
            if (column == 1):
                data = QStandardItemModel.data(self, qModelIndex, QtCore.Qt.EditRole)                                    
                if (data == "Yes") :
                    return QtCore.Qt.Checked                
                else:
                    return QtCore.Qt.Unchecked
                
        return QStandardItemModel.data(self, qModelIndex, role)        


class RCC_DatasetTagingDlg (QDialog) :   
    
    def __init__ (self, datasetUIHeader, datasetTagManager, ProjectDataset, parent, ShowRegistrationTag = False) :
        QDialog.__init__ (self, None)        
        self._ProjectDataset = ProjectDataset
        self._datasetTagManager = datasetTagManager
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)         
        self.ui = Ui_RCC_DatasetColumnChooser ()        
        self.ui.setupUi (self)       
        
        self._resizeWidgetHelper = ResizeWidgetHelper ()
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ColumnChooserTable)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.add_Btn)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.remove_Btn)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.Ok_Btn)  
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.Cancel_Btn)  
        
        self.ui.Ok_Btn.clicked.connect (self.Ok_Btn)
        self.ui.Cancel_Btn.clicked.connect (self.Cancel_Btn)
        
        basepath = self._ProjectDataset.getPythonProjectBasePath ()
        iconPath = os.path.join (basepath , "icons") 
        self.ui.add_Btn.setIcon(QtGui.QIcon(iconPath + os.sep + "plus.png"))
        self.ui.add_Btn.setIconSize(QtCore.QSize(20,20))        
        self.ui.remove_Btn.setIcon(QtGui.QIcon(iconPath + os.sep + "minus.png"))
        self.ui.remove_Btn.setIconSize(QtCore.QSize(20,20))
        self.ui.remove_Btn.setEnabled (False)
        
        self.ui.add_Btn.clicked.connect (self._add_Btn)
        self.ui.remove_Btn.clicked.connect (self._remove_Btn)
        
        self._DatasetHeader = datasetUIHeader.copy ()
        self._DatasetHeader.removeDatasetHeader ()
                        
        self._ColumnChooserListOrder = []
        self._ColumnChooserListName = []
        self._OldHeaderObjList = []
        self._ColumnChooserListOrder.append (("Description", "Description"))
        self._ColumnChooserListName.append ("Description")
        self._showregistrationTag = ShowRegistrationTag
        if (self._showregistrationTag) :            
            self._ColumnChooserListOrder.append (("Registration", "Registration"))        
            self._ColumnChooserListName.append ("Registration")
          
        for customTagIndex in range (datasetTagManager.getCustomTagCount ()) :            
             tag = datasetTagManager.getCustomTagByIndex (customTagIndex)
             self._ColumnChooserListOrder.append (tuple (tag.getIdentifer ()))
             self._ColumnChooserListName.append (tag.getName ())                    
        
        for internalTagIndex in range(datasetTagManager.getInternalTagCount ()) :  
             tag = datasetTagManager.getInternalTagByIndex (internalTagIndex)
             self._ColumnChooserListOrder.append (tuple (tag.getIdentifer ()))
             self._ColumnChooserListName.append (tag.getName ())                    
        
        for index in range (self._DatasetHeader.headerObjectCount ()) :            
            headerObject = self._DatasetHeader.getHeaderObjectAtIndex (index)
            if (headerObject.getIdentifer () not in self._ColumnChooserListOrder) :
                self._ColumnChooserListOrder.append (headerObject.getIdentifer ())
                self._ColumnChooserListName.append (headerObject.getName ())
                self._OldHeaderObjList.append (headerObject)
            
        if (self._DatasetHeader.headerObjectCount () == 0) :
            self._add_Btn ()
        else:
            self._UpdateTable ()        
            
    def _add_Btn (self) :
        self._DatasetHeader.addHeaderObject (HeaderObject ("Description","Description"))
        self._UpdateTable ()
        self.ui.remove_Btn.setEnabled (self.ui.ColumnChooserTable.selectionModel() is not None and self.ui.ColumnChooserTable.selectionModel().hasSelection () and self._tableModel.rowCount () > 0)                    
        
    def _remove_Btn (self) :
        selectionModel = self.ui.ColumnChooserTable.selectionModel()
        row = -1
        if (selectionModel.hasSelection ()) :            
            row = selectionModel.currentIndex ().row ()            
        if (row >= 0) :
            self._DatasetHeader.removeHeaderObjectAtIndex (row)
            self._UpdateTable ()
        self.ui.remove_Btn.setEnabled (self.ui.ColumnChooserTable.selectionModel() is not None and self.ui.ColumnChooserTable.selectionModel().hasSelection () and self._tableModel.rowCount () > 0)            
        return 
        
        
    def _UpdateTable (self) :  
        self._oldTableRowSelection  = []        
                
        self._tableModel = MyModel ( self._DatasetHeader , self._DatasetHeader.headerObjectCount (), 2, self)        
        self.ui.ColumnChooserTable.horizontalHeader().setStretchLastSection(True)
        self._tableModel.setHorizontalHeaderLabels (["Column","Visible"])
                            
        for row in range (self._DatasetHeader.headerObjectCount ()) :            
            headerObject = self._DatasetHeader.getHeaderObjectAtIndex (row)
            if (headerObject.getIdentifer () in self._ColumnChooserListOrder) :
                val = self._ColumnChooserListOrder.index (headerObject.getIdentifer ())
                
                index = self._tableModel.index(row,0,QModelIndex())                      
                self._tableModel.setData(index, str(val), QtCore.Qt.UserRole)            
                self._tableModel.setData(index, headerObject.getName(), QtCore.Qt.EditRole)                        
                    
                index = self._tableModel.index(row,1,QModelIndex())
                if (headerObject.getVisible ()) :
                    self._tableModel.setData(index, "Yes", QtCore.Qt.EditRole)            
                else:
                    self._tableModel.setData(index, "No", QtCore.Qt.EditRole)            
                
        self.ui.ColumnChooserTable.setModel (self._tableModel)                        
        self.ui.ColumnChooserTable.setSelectionBehavior( QAbstractItemView.SelectItems );
        self.ui.ColumnChooserTable.setSelectionMode( QAbstractItemView.SingleSelection );
        self.ui.ColumnChooserTable.setItemDelegateForColumn(0, QListDelegate (self._ColumnChooserListName,self._datasetTagManager, self._DatasetHeader, self._OldHeaderObjList, self._showregistrationTag, self.ui.ColumnChooserTable))        
        self.ui.remove_Btn.setEnabled (self.ui.ColumnChooserTable.selectionModel() is not None and self.ui.ColumnChooserTable.selectionModel().hasSelection () and self._tableModel.rowCount () > 0)            
        selectionmodel = self.ui.ColumnChooserTable.selectionModel()
        if (selectionmodel is not None) :
            selectionmodel.selectionChanged.connect(self._tabelSelectionChanged)                                                
    
    def _tabelSelectionChanged (self, selected, deselected) :        
        self.ui.remove_Btn.setEnabled (self.ui.ColumnChooserTable.selectionModel() is not None and self.ui.ColumnChooserTable.selectionModel().hasSelection () and self._tableModel.rowCount () > 0)            
        
    def _internalWindowResize (self, newsize):                            
        if (self._resizeWidgetHelper != None) :                                               
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.ColumnChooserTable, newsize)            
            self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.ColumnChooserTable, newsize)                        
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.add_Btn, newsize.height () - 50)            
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.remove_Btn, newsize.height () - 50)                   
            
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.Ok_Btn, newsize.height () - 44)                        
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.Cancel_Btn, newsize.height () - 44)    
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.Ok_Btn, newsize.width () - 181)                        
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.Cancel_Btn, newsize.width () - 91)    
            
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)                 
        self._internalWindowResize ( qresizeEvent.size () ) 
        
    def getDatasetUIHeader (self) :
        self._DatasetHeader.insertDatasetHeader ()
        return self._DatasetHeader    
    
    def Ok_Btn (self):        
        self.accept ()
        
    def Cancel_Btn (self):        
        self.reject ()