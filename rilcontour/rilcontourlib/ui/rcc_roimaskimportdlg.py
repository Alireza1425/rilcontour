#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 29 13:58:06 2017

@author: m160897
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 14:19:00 2017

@author: m160897
"""

"""
    RCC_ProjectROIEditorDlg
    
    Modal dialog to enable user to define and modify ROI objects
"""    
import nibabel as nib
import os
import numpy as np
from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import  QDialog, QAbstractItemView, QComboBox, QItemDelegate, QFileDialog, QProgressDialog
from PyQt5.QtGui import QStandardItemModel
from PyQt5.QtCore import QModelIndex
from rilcontourlib.util.rcc_util import ResizeWidgetHelper, PlatformMP, FastUnique
from rilcontourlib.util.FileUtil import FileUtil
from rilcontourlib.ui.qt_ui_autogen.RCC_ImportROIMaskDlgAutogen import Ui_RCC_ImportROIMaskDlg
from rilcontourlib.util.NibabelDataAccessHelper import NibabelDataAccessHelper
from rilcontourlib.util.FileUtil import ScanDir

class QListDelegate (QItemDelegate) :    
    def __init__ (self, MaskMapping, maskValue, RoiDefs, parent) :         
        QItemDelegate.__init__ (self, parent)                  
        self._MaskMapping = MaskMapping
        self._RoiDefs = RoiDefs
        self._RoiDefNameList = ["None"] + RoiDefs.getNameLst ()               
        self._MaskValue = maskValue
        self._ColorList = [QtGui.QColor(0,0,0)]
        for index in range (1, len (self._RoiDefNameList)) :
            name = self._RoiDefNameList[index]
            color = RoiDefs.getROIColor (name)
            self._ColorList.append (color)
            
    def createEditor(self, parent, options, qModelIndex) :       
        editor = QComboBox (parent)
        editor.setAutoFillBackground(True)                
        for name in self._RoiDefNameList :
            editor.addItem (str (name))                         
        for index , color in enumerate(self._ColorList) : 
            editor.setItemData(index, color, QtCore.Qt.TextColorRole)            
        name= self._MaskMapping[self._MaskValue]
        index = self._RoiDefNameList.index (name)        
        editor.setCurrentIndex(index) 
        return editor
      
    def setEditorData(self, qWidgetEditor, qModelIndex) :
        # Get the value via index of the Model
        value = qModelIndex.model().data(qModelIndex, QtCore.Qt.EditRole)
        # Put the value into the SpinBox
        index = self._RoiDefNameList.index (str (value))        
        qWidgetEditor.setCurrentIndex(index) 
        #self._defaultV = value
        
    def setModelData(self,  qWidgetEditor, qAbstractModel, qModelIndex) :        
        currentIndex = qWidgetEditor.currentIndex()        
        value = qWidgetEditor.itemText (currentIndex)
        qAbstractModel.setData(qModelIndex, value, QtCore.Qt.EditRole)
        self._MaskMapping[self._MaskValue] = value
                
    def updateEditorGeometry(self, qWidgetEditor, qStyleOptionViewItem, qModelIndex) :
        qWidgetEditor.setGeometry(qStyleOptionViewItem.rect)
        
   
class MyModel(QStandardItemModel) :           

    def setROIDefs (self, roiDefs) :
        self._RoiDefs = roiDefs
        
    def flags (self, qModelIndex) :                
        return  QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable         

    def data(self, index, role):
        if not index.isValid():
            return None
        if role == QtCore.Qt.ForegroundRole :            
            roiName = self.data(index, QtCore.Qt.EditRole)            
            return self._RoiDefs.getROIColor (roiName)            
        
        return QStandardItemModel.data (self, index, role)

def getUniqueMaskValues (tupe, childProcessPipe) :        
        path, filename = "UnknownPath", "UnknownFile"
        try :    
            path, filename = tupe        
            uniqueMaskValues = RCC_ROIMaskImportDlg._isGetNIfTIFileMaskValues (path, filename)
            if (uniqueMaskValues is not None):
                print ("%d Masked Values identified in: %s" % (len (uniqueMaskValues), os.path.join (path, filename)))
            elif (filename.lower().endswith (".nii") or filename.lower().endswith (".nii.gz")) :           
                print ("Warrning: No Masked Values identified in: %s" % os.path.join (path, filename))
            if (childProcessPipe is not None) :
                childProcessPipe.send((uniqueMaskValues, tupe))
                childProcessPipe.close()      
            else:
                return (uniqueMaskValues, tupe)
        except:
            print ("A exception occured trying to get the unique mask falues from file: %s at %s" % (path, filename))
            uniqueMaskValues = np.array ([0], dtype=np.int)
            if (childProcessPipe is not None) :
                childProcessPipe.send((uniqueMaskValues, tupe))
                childProcessPipe.close()      
            else:
                return (uniqueMaskValues, tupe)

def getUniqueMaskValuesPool (tupe) :
    return getUniqueMaskValues (tupe, None)
        
class RCC_ROIMaskImportDlg (QDialog) :
    
    def __init__ (self, ProjectDataset, RoiDefs, parent, importSingleMask = False, SelectedNodeSubset = None, NiftiPath = None) :        
        self._SelectedNodeSubset = []
        if SelectedNodeSubset is not None :
            for node in SelectedNodeSubset :
               self._SelectedNodeSubset.append (node.internalPointer ()) 
        self._importSingleMask = importSingleMask
        self._ProjectDataset = ProjectDataset
        self._parent = parent
        QDialog.__init__ (self, None)
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)
        self._ReturnValue = ()
        self.ui = Ui_RCC_ImportROIMaskDlg ()        
        self.ui.setupUi (self)                
        self._resizeWidgetHelper = ResizeWidgetHelper ()
        self.ui.maskPathTxt.setAutoFillBackground(True)
        self.ui.maskPathTxt.setReadOnly (True)
        self.ui.prefixTxt.setAutoFillBackground(True)
        self.ui.suffixTxt.setAutoFillBackground(True)
        self.ui.overwriteROIBtn.setChecked (True)
        self.ui.ExistingDataPereferedBtn.setChecked (False)
        self.ui.MaskedDataPereferedBtn.setChecked (False)        
        self.ui.MasksExhibitAllValues.setChecked (True)
        self.ui.MasksExhibitAllValues.setEnabled (False)
        self.ui.ExcludeMaskFileMappingMultipleNIfTI.setChecked (True)
        if (self.importingSingleMask ()) :                  
            self._resizeWidgetHelper.setWidgetYPos (self.ui.MaskROIOverLapOptionsGroup, 110)  
            self._resizeWidgetHelper.setWidgetYPos (self.ui.MaskTypeGroupBox, 110+130)  
            self._resizeWidgetHelper.setWidgetYPos (self.ui.maskvalueMappingGroupBox, 210+130)              
            self._resizeWidgetHelper.setWidgetYPos (self.ui.OkBtn, 390 + 135)  
            self._resizeWidgetHelper.setWidgetYPos (self.ui.CancelBtn, 390 + 135)  
            self.setMinimumSize  (593, 420 + 150)
            self.resize  (593, 420 + 150)
                                            
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.Path_groupBox)        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.maskPathTxt)        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.SelectMaskPathBtn)        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.overwriteROIBtn)        
        if (not self.importingSingleMask ()) :                        
            self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ExcludeMaskFileMappingMultipleNIfTI)        
            self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.PrefixSuffixGroupBox)        
            self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.prefixTxt)        
            self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.suffixTxt)        
            self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.getMaskValuesBtn)        
            self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.getMaskMapping)        
        else:    
           self.ui.PrefixSuffixGroupBox.setVisible (False)        
           self.ui.prefixTxt.setVisible (False)        
           self.ui.suffixTxt.setVisible (False)        
           self.ui.ExcludeMaskFileMappingMultipleNIfTI.setVisible (False)                   
           self.ui.getMaskValuesBtn.setVisible (False)        
           self.ui.getMaskMapping.setVisible (False)        
                                        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.MaskTypeGroupBox)                
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.AndMaskRadioBtn)        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.OrMaskRadioBtn)        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.maskvalueMappingGroupBox)
        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.MaskROIOverLapOptionsGroup)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ExistingDataPereferedBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.MaskedDataPereferedBtn)                            
            
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.mappingTableView)       
        if (not self.importingSingleMask ()) :
            self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.maskToNiftILst)        
        else:
            self.ui.maskToNiftILst.setVisible (False)        
        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.OkBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.CancelBtn)        
        self.ui.SelectMaskPathBtn.clicked.connect (self._selectPath)
        self.ui.OkBtn.clicked.connect (self.OkBtn)
        self.ui.CancelBtn.clicked.connect (self.CancelBtn)
        self.ui.getMaskValuesBtn.clicked.connect (self.getMaskValues)
        
        self.ui.AndMaskRadioBtn.clicked.connect (self._setMask)
        self.ui.OrMaskRadioBtn.clicked.connect (self._setMask)
        
        #self.ui.PrefixSuffixGroupBox.setStyleSheet("border: 1px solid black;")
        self._pathMaskPath = None 
        self._rawMaskValues = None
        
        self._maskValueList = []
        self._maskBits = 0
        self._ROIMaskPairs = None
        
        if (not self.importingSingleMask ()) :
            self.ui.prefixTxt.setEnabled (False)
            self.ui.suffixTxt.setEnabled (False)
            self.ui.prefixTxt.textChanged.connect (self.enableMaskValueBtn)
            self.ui.suffixTxt.textChanged.connect (self.enableMaskValueBtn)
            self.ui.getMaskValuesBtn.setEnabled (False)
            self.ui.ExcludeMaskFileMappingMultipleNIfTI.setEnabled (False)
        
        self.ui.AndMaskRadioBtn.setChecked (True)
        self.ui.OrMaskRadioBtn.setChecked (False)
        self.ui.AndMaskRadioBtn.setEnabled (False)
        self.ui.OrMaskRadioBtn.setEnabled (False)
        self.ui.mappingTableView.setEnabled (False)
        self.ui.OkBtn.setEnabled (False)
        self.ui.maskToNiftILst.setEnabled (False)
        self.setMaskAndExistingAreaROIOverlapEnabled (False)
         
        self._RoiDefs = RoiDefs  
        self._MaskMapping = {}
        for name in self._RoiDefs.getNameLst ()       :
            mv = self._RoiDefs.getROIMaskValue(name)
            self._MaskMapping[mv] = name
        
        self.ui.overwriteROIBtn.clicked.connect (self._overwriteROIBtnPress)
        self.ui.ExistingDataPereferedBtn.clicked.connect (self._ExistingDataPereferedBtnPress)
        self.ui.MaskedDataPereferedBtn.clicked.connect (self._MaskedDataPereferedBtnPress)
        if NiftiPath is not None :
            try :
                self._selectPath (placeholder = True, NiftiPath = NiftiPath)
            except:
                pass
    
    def _overwriteROIBtnPress (self) :
        if (self.ui.overwriteROIBtn.isChecked()) :
            self.ui.ExistingDataPereferedBtn.setChecked (False) 
            self.ui.MaskedDataPereferedBtn.setChecked (False)
    
    def _ExistingDataPereferedBtnPress (self) :
        if (self.ui.ExistingDataPereferedBtn.isChecked()) :
            self.ui.overwriteROIBtn.setChecked (False)
            self.ui.MaskedDataPereferedBtn.setChecked (False)
        
    def _MaskedDataPereferedBtnPress (self) :
        if (self.ui.MaskedDataPereferedBtn.isChecked()) :
            self.ui.overwriteROIBtn.setChecked (False)
            self.ui.ExistingDataPereferedBtn.setChecked (False)
    
    def setMaskAndExistingAreaROIOverlapEnabled (self, enabled) :
        self.ui.MaskROIOverLapOptionsGroup.setEnabled (enabled)
        self.ui.MaskedDataPereferedBtn.setEnabled (enabled)
        self.ui.overwriteROIBtn.setEnabled (enabled)
        self.ui.ExistingDataPereferedBtn.setEnabled (enabled)
        
    def importingSingleMask (self) :
        return self._importSingleMask
    
    def enableMaskValueBtn (self) :
        if (not self.importingSingleMask ()) :
            self.ui.getMaskValuesBtn.setEnabled (True)
            self.ui.ExcludeMaskFileMappingMultipleNIfTI.setEnabled (True)
        
    def _selectPath (self, placeholder=None, NiftiPath = None):        
        self.ui.maskToNiftILst.setEnabled (False)
        self.ui.prefixTxt.setEnabled (False)
        self.ui.suffixTxt.setEnabled (False)
        self.ui.getMaskValuesBtn.setEnabled (False)                      
        self.ui.ExcludeMaskFileMappingMultipleNIfTI.setEnabled (False)
        self.ui.AndMaskRadioBtn.setEnabled (False)
        self.ui.OrMaskRadioBtn.setEnabled (False)
        self.ui.mappingTableView.setEnabled (False)
        self.ui.OkBtn.setEnabled (False)
        self.setMaskAndExistingAreaROIOverlapEnabled (False)
        self.ui.MasksExhibitAllValues.setEnabled (False)
        if NiftiPath is None :
            dlg= QFileDialog( self )
            if (self.importingSingleMask ()) :
                dlg.setWindowTitle( 'Select Mask File' )
            else:
                dlg.setWindowTitle( 'Select Root Directory Containing Mask Files' )
            
            #dlg.setViewMode( QFileDialog.Detail )     
            if (self.importingSingleMask ()) :
                dlg.setFileMode (QFileDialog.ExistingFile)
                dlg.setNameFilters( [self.tr('*.gz Mask (*.gz)'), self.tr('nii.gz Mask (*.nii.gz)'), self.tr('nii Mask (*.nii)'), self.tr('Any File (*)')] )        
            else:
                dlg.setFileMode (QFileDialog.Directory)        
                dlg.setNameFilters( [self.tr('Any File (*)'),self.tr('nii.gz Mask (*.nii.gz)'), self.tr('nii Mask (*.nii)')] )        
            dlg.setAcceptMode (QFileDialog.AcceptOpen)
            if dlg.exec_() and (len (dlg.selectedFiles())== 1):
                NiftiPath = dlg.selectedFiles()[0]     
        if NiftiPath is not None and os.path.exists (NiftiPath) :            
            if (self.importingSingleMask ()) :
                uniqueValues = RCC_ROIMaskImportDlg._isGetNIfTIFileMaskValues (NiftiPath)    
                if (uniqueValues is not None) :
                    self._pathMaskPath = NiftiPath            
                    self.ui.maskPathTxt.setText (self._pathMaskPath)                        
                    self.ui.AndMaskRadioBtn.setEnabled (True)
                    self.ui.OrMaskRadioBtn.setEnabled (True)
                    self.ui.mappingTableView.setEnabled (True)
                    self.ui.OkBtn.setEnabled (True)
                    self.setMaskAndExistingAreaROIOverlapEnabled (True)
                                             
                    maskValueList = sorted (uniqueValues.tolist ())        
                    uniqueValues = uniqueValues.astype (np.uint32)
                    maskBits = int (0)
                    for item in uniqueValues:
                         maskBits = maskBits | int (item)
                     
                    self._ROIMaskPairs = NiftiPath
                    self._maskValueList = maskValueList
                    self._maskBits = maskBits
                    self._setMask ()
                    return
            else:                    
                self.ui.MasksExhibitAllValues.setEnabled (True)
                self.ui.prefixTxt.setEnabled (True)
                self.ui.suffixTxt.setEnabled (True)
                self.ui.getMaskValuesBtn.setEnabled (True)      
                self.ui.ExcludeMaskFileMappingMultipleNIfTI.setEnabled (True)
                self.ui.maskToNiftILst.setEnabled (True)
                self._pathMaskPath = NiftiPath                  
                self.ui.maskPathTxt.setText (self._pathMaskPath)
                return
        self._pathMaskPath = None                  
        self.ui.maskPathTxt.setText ("")        
        
        
    def _getMaskFileList (self, path):        
        fileNamesFoundLst = []
        fileprefix  = self.ui.prefixTxt.text().strip ()
        fileprefix = fileprefix.lower ()
        filesuffix  = self.ui.suffixTxt.text().strip ()        
        filesuffix, _ = FileUtil.removeExtension (filesuffix, [".nii.gz", ".nii"])        
        filesuffix  = filesuffix.strip ()        
        filesuffix = filesuffix.lower ()
        
        pathlist = [path]
        while len (pathlist) > 0 :
            path = pathlist.pop ()
            try:
                fileIterator = ScanDir.scandir(path)               
                for dirfileObj in fileIterator :
                    fullname = dirfileObj.path
                    if dirfileObj.is_dir () :
                        pathlist.append (fullname)
                    elif dirfileObj.is_file () :
                        fname = dirfileObj.name                        
                        LFname = fname.lower ()
                        if (len (fileprefix) == 0 or LFname.startswith (fileprefix)) :
                            if len (filesuffix) == 0 :
                                fileNamesFoundLst.append ((path, fname))
                                print ("Found File: %s" % fullname)
                            else:
                                LFname, _ = FileUtil.removeExtension (LFname, [".nii.gz", ".nii"])
                                LFname = LFname.strip ()
                                if LFname.endswith (filesuffix) :
                                    fileNamesFoundLst.append ((path, fname))
                                    print ("Found File: %s" % fullname)

            finally:
                try:
                   fileIterator.close ()
                except:
                    pass
        return fileNamesFoundLst    
    
    @staticmethod 
    def _isGetNIfTIFileMaskValues (path, filename = None):
        try:
            if filename is not None :
                path = os.path.join (path , filename)
            nifti = nib.load (path)              
            imageData = NibabelDataAccessHelper.getNibabelData (nifti)
            uniqueVals = FastUnique.unique (np.round(imageData).astype (np.int))            
            count = len (uniqueVals)
            if count > 200 :                
                print ("Warrning: %s has %d masked values skipping (Count > 200)" % (path, count))
                return None
            else:
                return uniqueVals
        except:
            return None
    
    
    
        
    def _getMatchingROIFileLst (self, filePair, LeafFileCache, ExactMatch = False) :
        path, filename = filePair
        try :
            fileprefix  = self.ui.prefixTxt.text ().strip ()
            filesuffix = self.ui.suffixTxt.text ().strip ()        
            filesuffix, _  = FileUtil.removeExtension (filesuffix, [".nii.gz", ".nii"])
            filesuffix = filesuffix.strip ()
            filename_mask, _ =  FileUtil.removeExtension (filename, [".nii.gz", ".nii"])
            filename_mask = filename_mask.strip ()       
            if  len (filesuffix) > 0 :
                filename_mask = filename_mask [len (fileprefix) : - len (filesuffix)]
            else:
                filename_mask = filename_mask [len (fileprefix) :]
        except:
            return filename_mask,  []
        filePathList = FileUtil.getPathList (path)
        nodeList = self._ProjectDataset.getNIfTIDatasetInterface().getNiftiTreeNodeListForFileList (filePathList, filename_mask, LeafFileCache =  LeafFileCache, ExactMatch = ExactMatch)
        if len (nodeList) > 1 :
            searchMaskPath = os.path.join (filePair[0], filePair[1])
            removeIndexLst = []
            for index, node in enumerate (nodeList):
                if node.getPath () == searchMaskPath :
                    removeIndexLst.append (index)
            removeIndexLst.reverse ()
            for idx in removeIndexLst :
                del nodeList[idx]                
        if len (self._SelectedNodeSubset) < 2 :
            return filename_mask, nodeList
        return filename_mask, self._ProjectDataset.getNIfTIDatasetInterface().getTreeNodeSubset (nodeList, self._SelectedNodeSubset)        
            
    
    getUniqueMaskValues = staticmethod (getUniqueMaskValues)
    getUniqueMaskValuesPool = staticmethod (getUniqueMaskValuesPool)
    
    def _getFileMaskValues (self, rootPath) :
        import psutil
        ROIMaskPairs = []        
        maskValueList = []
        
        fileTreeList = self._getMaskFileList  (rootPath)        
        
        progdialog = QProgressDialog("Finding ROI Masks", "Cancel", int (0), len (fileTreeList), self)
        progdialog.setMinimumDuration (0)
        progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)                        
        progdialog.setWindowModality(QtCore.Qt.ApplicationModal)                        
        progdialog.setAutoClose (False)
        progdialog.setWindowTitle ("Discovering Mask Values")                 
        try:          
            progdialog.forceShow()   
            progdialog.setValue (0)
            self._ProjectDataset.getApp ().processEvents ()        
            if (8000000000 > psutil.virtual_memory()[1]):
                MaxProc = 2
            else:
                MaxProc = 4
            
    
            PoolSize = PlatformMP.getAvailableCPU ()
            PoolSize = min (PoolSize, MaxProc)
            MP_List = []            
            progdialog.setMaximum (len (fileTreeList))
            if self.ui.MasksExhibitAllValues.isChecked () :
                resultList = []
                resultFound = None
                for featureindex in range (len (fileTreeList)) :   
                    path, filename = fileTreeList[featureindex]                                                 
                    MP_List.append ((path, filename))                     
                    processParameters = (path, filename)                    
                    if (resultFound is None or len (resultFound) < 1) :                        
                        resultFound = RCC_ROIMaskImportDlg.getUniqueMaskValuesPool (processParameters)
                        if resultFound is None :
                            resultList.append (resultFound)                                            
                        resultFound = resultFound[0]
                        if resultFound is None :                                     
                            resultList.append ((resultFound , processParameters))                                            
                    if resultFound is not None :                                     
                        resultList.append ((resultFound , processParameters))                                            
                    progdialog.setValue (featureindex)
                    self._ProjectDataset.getApp ().processEvents ()                    
            else:
                procQueue = PlatformMP.processQueue (RCC_ROIMaskImportDlg.getUniqueMaskValuesPool, None, PoolSize = PoolSize, SleepTime = 5, ProjectDataset =  self._ProjectDataset, Debug = True)
                for featureindex in range (len (fileTreeList)) :                
                    path, filename = fileTreeList[featureindex]                                                 
                    MP_List.append ((path, filename)) 
                    processParameters = (path, filename)
                    cleanupParameters = None
                    procQueue.queueJob (self._ProjectDataset.getROISaveThread (), processParameters, cleanupParameters)
                    progdialog.setLabelText (filename)
                    progdialog.setValue (featureindex)
                    self._ProjectDataset.getApp ().processEvents ()
                    if progdialog.wasCanceled():
                        break
                resultList = procQueue.join ()
            
            LeafFileCache = self._ProjectDataset.getNIfTIDatasetInterface().getLeafNIfTINodes ()
            SegmentedNiftiMapping = {}
            for resultTuple in resultList :
                uniqueMaskValues, filePair = resultTuple
                del MP_List[MP_List.index (filePair)]                                
                try :
                    if (uniqueMaskValues is not None and uniqueMaskValues.shape[0] > 0) :                        
                        filenamemask, niftiTreeWidgetList = self._getMatchingROIFileLst (filePair, LeafFileCache)                         
                        if (not self.ui.ExcludeMaskFileMappingMultipleNIfTI.isChecked () or  len (niftiTreeWidgetList) == 1) :
                            for niftiTreeWidget in niftiTreeWidgetList :                    
                                niftiPath = niftiTreeWidget.getPath ()                                
                                if niftiPath not in SegmentedNiftiMapping or len (filenamemask) > len (SegmentedNiftiMapping[niftiPath][0]):
                                    SegmentedNiftiMapping[niftiPath] = (filenamemask, niftiTreeWidget, filePair, uniqueMaskValues)
                                
                                        
                                #ROIMaskPairs.append ((niftiTreeWidget, filePair))
                                #maskValueList.append (uniqueMaskValues)
                                #dirName, fname = filePair
                                #maskName = os.path.join (dirName, fname)                                
                                #self.ui.maskToNiftILst.addItem (niftiPath + "-> " + maskName)                                   
                except:
                    print ("A error occured getting mask child process results")
            
            
                    
            for filePair in MP_List :
                try :                    
                    filenamemask, niftiTreeWidgetList = self._getMatchingROIFileLst (filePair, LeafFileCache)                         
                    if (not self.ui.ExcludeMaskFileMappingMultipleNIfTI.isChecked () or  len (niftiTreeWidgetList) == 1) :
                        for niftiTreeWidget in niftiTreeWidgetList :                    
                            niftiPath = niftiTreeWidget.getPath ()                                
                            if niftiPath not in SegmentedNiftiMapping or len (filenamemask) > len (SegmentedNiftiMapping[niftiPath][0]) :
                                SegmentedNiftiMapping[niftiPath] = (filenamemask, niftiTreeWidget, filePair, None)
                            
                            #ROIMaskPairs.append ((niftiTreeWidget, filePair))                            
                            #dirName, fname = filePair
                            #maskName = os.path.join (dirName, fname)
                            #niftiPath = niftiTreeWidget.getPath ()
                            #self.ui.maskToNiftILst.addItem ("File scan skipped: " + niftiPath + "-> " + maskName)                
                except:
                    print ("A error occured getting mask child process results")
            del MP_List
            
            for niftiPath in sorted (SegmentedNiftiMapping.keys ()) :
                filenamemask, niftiTreeWidget, filePair, uniqueMaskValues = SegmentedNiftiMapping[niftiPath]
                ROIMaskPairs.append ((niftiTreeWidget, filePair))
                if uniqueMaskValues is not None :
                    maskValueList.append (uniqueMaskValues)
                dirName, fname = filePair
                maskName = os.path.join (dirName, fname)                                
                self.ui.maskToNiftILst.addItem (niftiPath + "-> " + maskName)                                   
                    
            
            self.ui.MasksExhibitAllValues.setEnabled (progdialog.wasCanceled())
            self.ui.getMaskValuesBtn.setEnabled (progdialog.wasCanceled())
            self.ui.ExcludeMaskFileMappingMultipleNIfTI.setEnabled (progdialog.wasCanceled())
        finally:
            progdialog.close()          
            self._ProjectDataset.getApp ().processEvents ()        
        
        
        if (len (maskValueList) > 0) :
            maskValueList = np.concatenate (maskValueList)
            uniqueValues =  FastUnique.unique (maskValueList)
            maskValueList = uniqueValues.tolist ()
            uniqueValues = uniqueValues.astype (np.uint32)
            maskBits = int (0)
            for item in uniqueValues:
                maskBits = maskBits | int (item)
            return (ROIMaskPairs, maskValueList, maskBits)
        return (ROIMaskPairs, maskValueList, 0)
   
    def _getMaskList (self) :
        if (self.ui.AndMaskRadioBtn.isChecked ()) :
            return self._maskValueList
        else:
            returnlst = []
            bitVal = 1
            maskBits = int (self._maskBits)
            if maskBits <= 0 :
                return returnlst
            while (maskBits > 0):
                if maskBits & 1 == 1 :
                    returnlst.append (bitVal)
                bitVal = bitVal * 2
                maskBits = maskBits >> 1
            return returnlst
        
        
    def getMaskValues (self) :  
        self.ui.maskToNiftILst.clear ()
        self._maskValueList = []
        self._maskBits = 0
        self._ROIMaskPairs = None
        if (self._pathMaskPath is not None) :              
            self._ROIMaskPairs , self._maskValueList,self._maskBits = self._getFileMaskValues (self._pathMaskPath)            
            if (len (self._ROIMaskPairs) == 0) :
                self.ui.AndMaskRadioBtn.setEnabled (False)
                self.ui.OrMaskRadioBtn.setEnabled (False)
                self.ui.mappingTableView.setEnabled (False)
                self.ui.OkBtn.setEnabled (False)
                self.setMaskAndExistingAreaROIOverlapEnabled (False)
            else:
                self.ui.AndMaskRadioBtn.setEnabled (True)
                self.ui.OrMaskRadioBtn.setEnabled (True)
                self.ui.mappingTableView.setEnabled (True)
                self.ui.OkBtn.setEnabled (True)
                self.setMaskAndExistingAreaROIOverlapEnabled (True)
                self._maskList = self._getMaskList ()        
                if (len (self._maskValueList) > self._RoiDefs.count ()+1) :
                    self.ui.AndMaskRadioBtn.setChecked (False)
                    self.ui.OrMaskRadioBtn.setChecked (True)
                    self._maskList = self._getMaskList ()                        
                self._updateTable ()            
            
            
    def _setMask (self):
        self._maskList = self._getMaskList ()        
        self._updateTable ()            
        return
        
    def _updateTable (self) :      
              
        self.ui.mappingTableView.horizontalHeader().setStretchLastSection(True)
        self.ui.mappingTableView.horizontalHeader().hide ()
                
        maskCount = len (self._maskList)
        self._tableModel = MyModel (maskCount, 1, self)
        self._tableModel.setROIDefs (self._RoiDefs)
        if maskCount == 0 :
            self._tableModel.setVerticalHeaderLabels ([])
        else:
            maskValueStringList = []
            for mask in self._maskList :            
                maskValueStringList.append (str (mask))
            self._tableModel.setVerticalHeaderLabels (maskValueStringList)
                
            for maskIndex, maskValue in enumerate(self._maskList) :                            
                table_index = self._tableModel.index(maskIndex,0,QModelIndex())
                if (maskValue not in self._MaskMapping) :
                    self._MaskMapping[maskValue] = "None"
                self._tableModel.setData(table_index, self._MaskMapping[maskValue], QtCore.Qt.EditRole)            
                #self._tableModel.setData(table_index, self._MaskMapping[maskValue], QtCore.Qt.UserRole)     
                
        self.ui.mappingTableView.setModel (self._tableModel)            
        
        self.ui.mappingTableView.setSelectionBehavior( QAbstractItemView.SelectItems );
        self.ui.mappingTableView.setSelectionMode( QAbstractItemView.SingleSelection );        
        selectionmodel = self.ui.mappingTableView.selectionModel()
        if (selectionmodel is not None) :
            selectionmodel.selectionChanged.connect(self._tabelSelectionChanged)
                        
        """if (not self._TableMouseEventsInitalized) :
            self.ui.tableView.verticalHeader ().sectionDoubleClicked.connect (self.headerRowDoubleClicked)
            self._TableMouseEventsInitalized = True
        elif (self._tableModel.rowCount () == 0):            
            self.ui.tableView.verticalHeader ().sectionDoubleClicked.disconnect (self.headerRowDoubleClicked)
            self._TableMouseEventsInitalized = False"""
                            
        for maskIndex in range (maskCount) :       
            maskValue = self._maskList[maskIndex]
            self.ui.mappingTableView.setItemDelegateForRow(maskIndex, QListDelegate (self._MaskMapping, maskValue, self._RoiDefs, self.ui.mappingTableView))            
        
        return
    
    def _tabelSelectionChanged (self) :
        return
    
    def _internalWindowResize (self, newsize):                            
        if (self._resizeWidgetHelper != None) :                                               
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.Path_groupBox, newsize)            
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.maskPathTxt, newsize)                        
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.SelectMaskPathBtn, self.ui.Path_groupBox.width () - 151)                                                
            if (not self.importingSingleMask ()) :
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.PrefixSuffixGroupBox, newsize)                        
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.prefixTxt, newsize)            
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.suffixTxt, newsize)                        
                
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.MaskTypeGroupBox, newsize)            
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.AndMaskRadioBtn, newsize)                        
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.OrMaskRadioBtn, newsize)                                    
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.maskvalueMappingGroupBox, newsize)            
            self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.maskvalueMappingGroupBox, newsize)            
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.mappingTableView, newsize)        
            
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.MaskROIOverLapOptionsGroup, newsize)           
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.ExistingDataPereferedBtn, newsize)           
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.MaskedDataPereferedBtn, newsize)           
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.overwriteROIBtn, newsize)           
            
            self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.mappingTableView, newsize) 

            if (not self.importingSingleMask ()) :            
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.getMaskMapping, newsize)            
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.maskToNiftILst, newsize)            
                #self._resizeWidgetHelper.setWidgetXPos  (self.ui.getMaskValuesBtn, self.ui.maskvalueMappingGroupBox.width () - 499)                                    
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.getMaskValuesBtn, self.ui.getMaskMapping.width () - 151)                                    
                                
            
            
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.OkBtn, newsize.width () - 189)                        
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.CancelBtn, newsize.width () - 99)                     
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.OkBtn, newsize.height () - 36)                        
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.CancelBtn, newsize.height () - 36)                        
                                       
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)                 
        self._internalWindowResize ( qresizeEvent.size () )                       

    
    def OkBtn (self):           
        if (self.ui.OrMaskRadioBtn.isChecked ()):
            importMethod = "OrMask" 
        elif (self.ui.AndMaskRadioBtn.isChecked ()) :
            importMethod = "AndMask"
                
        removeKeyList = []
        for key in self._MaskMapping.keys () :
            if key not in self._maskList :
                removeKeyList.append (key)
        for removeKey in removeKeyList :
            del self._MaskMapping[removeKey]
        self._ReturnValue = (self._ROIMaskPairs, self._MaskMapping,importMethod, self.ui.overwriteROIBtn.isChecked (), self.ui.MaskedDataPereferedBtn.isChecked (), self.ui.ExistingDataPereferedBtn.isChecked ())
        self.accept ()  
          
    def CancelBtn (self):        
        self._ReturnValue = None
        self.reject ()
    
    def getReturnValue (self) :
        return self._ReturnValue 
