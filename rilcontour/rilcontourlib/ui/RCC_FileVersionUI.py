#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  7 17:34:18 2019

@author: m160897
"""

from PyQt5 import QtCore
from PyQt5.QtWidgets import   QDialog
from rilcontourlib.ui.qt_ui_autogen.RCC_FileCheckinDialogAutogen import Ui_CheckinFileDlg
from rilcontourlib.ui.qt_ui_autogen.RCC_FileHistoryDialogAutogen import Ui_FileHistoryDlg
from rilcontourlib.util.rcc_util import ResizeWidgetHelper
from rilcontourlib.util.DateUtil import TimeUtil, DateUtil

class FileVersionUI  :
    
    @staticmethod
    def showCreateVersionFileDlg (parent):
        dlg = CreateVersionDialog (parent)
        if (dlg.exec_ () == QDialog.Accepted) :
            return True, dlg.getComment ()
        return False, None
        
    @staticmethod
    def showRollBackFileDlg (parent, filehandle, RollBack = False) :
        dlg = CreateFileRollbackDlg (parent, filehandle, RollBack) 
        if (dlg.exec_ () == QDialog.Accepted) :
            return  dlg.getSelectedVersion ()
        return None

class CreateVersionDialog (QDialog) :
    
    def __init__ (self,  parent) :
        QDialog.__init__ (self, parent)
        self._parentWindow = parent
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)        
        self.ui = Ui_CheckinFileDlg ()        
        self.ui.setupUi (self)    
        self.setWindowTitle ("Version File")
        self._resizeWidgetHelper = ResizeWidgetHelper ()
        self.ui.OkBtn.clicked.connect (self._OkBtnPressed)
        self.ui.CancelBtn.clicked.connect (self.reject)
        self._Comment = ""

    def _internalWindowResize (self, newsize):                            
        if (self._resizeWidgetHelper != None) :
            self._resizeWidgetHelper.setWidth  (self.ui.CommentLbl, newsize.width () - 39)         
            self._resizeWidgetHelper.setWidth  (self.ui.Comment, newsize.width () - 19)            
            self._resizeWidgetHelper.setHeight  (self.ui.Comment, newsize.height () - 92)                   
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.OkBtn, newsize.width () - 240)         
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.CancelBtn, newsize.width () - 130)            
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.OkBtn, newsize.height () - 47)    
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.CancelBtn, newsize.height () - 47)    
    
    def _OkBtnPressed (self) :
        self._Comment = self.ui.Comment.toPlainText()
        self.accept  ()
        
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)                 
        self._internalWindowResize ( qresizeEvent.size () ) 

    def getComment (self) :
        return self._Comment



class CreateFileRollbackDlg (QDialog) :
    
    def __init__ (self,  parent, fileHandle, RollBackDlg) :
        QDialog.__init__ (self, parent)
        self._parentWindow = parent
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)        
        self.ui = Ui_FileHistoryDlg ()
        self.ui.setupUi (self)        
        if not RollBackDlg :
            self.setWindowTitle ("File Version History")
            self.ui.RollBackBtn.setVisible (False)
        else:
            self.setWindowTitle ("Select File Version")
        self._resizeWidgetHelper = ResizeWidgetHelper ()
        self.ui.RollBackBtn.clicked.connect ( self.accept)
        self.ui.CancelBtn.clicked.connect (self.reject)
        self.ui.HistoryFrame.resizeEvent = self._resizeHistoryFrame
        self.ui.StatusFrame.resizeEvent = self._resizeStatusFrame
        self._selectedVersion = None
        historyList = fileHandle.getFileVersionHistory ()
        self._fileHandle = fileHandle
        for fileversion in historyList :
            userName = fileversion.getUserName ()
            if userName is None or userName == "" :
                userName = "Unknown"
            else:
               userName =  self._fileHandle.getUserNickname (userName)
            date = DateUtil.dateToString (fileversion.getDateCreated ())
            time = TimeUtil.timeToString (fileversion.getTimeCreated ())
            self.ui.FileList.addItem ("%s created version %d at %s at %s" % (userName, fileversion.getVersionNumber (), date, time))
            self.ui.FileList.item(self.ui.FileList.count () - 1).setData (QtCore.Qt.UserRole, fileversion)

        if self.ui.FileList.count () > 0 :
            lastIndex = self.ui.FileList.count () - 1
            self.ui.FileList.setCurrentRow (lastIndex)
            self.ui.FileList.item(lastIndex).setSelected(True)
        self.ui.FileList.currentRowChanged.connect (self._rowSelectionChanged)
        self._rowSelectionChanged ()
     
            
    def _rowSelectionChanged (self):
        try:
            currentItem = self.ui.FileList.currentItem ()
            selectedFileVersion = currentItem.data (QtCore.Qt.UserRole)
            comment = selectedFileVersion.getVersionComment ()
            versionNumber,fpath = self._fileHandle.getVersionNumberAndPathFromUID (selectedFileVersion.getVersionUID ())
            if versionNumber is None :
                    versionNumber = "Unknown"
            html =  ["<html>"]
            html += ["<b>Version number: </b>" + str(versionNumber)]
            userName = self._fileHandle.getUserNickname (selectedFileVersion.getUserName ())
            html += ["<b>Created by user: </b>" + userName]
            date = DateUtil.dateToString (selectedFileVersion.getDateCreated ())
            time = TimeUtil.timeToString (selectedFileVersion.getTimeCreated ())
            dt = "%s at %s" % (date, time)
            html += ["<b>Created at date/time: </b>" + dt]
            if fpath is not None :
               html += [""]
               html += ["<b>File saved: </b>" + fpath ]
            html += ["<hr>"]
            html += ["<b>Comments</b>"]
            html += [comment]
            html += ["</html>"]
            self.ui.Comment.setText ("<br>".join(html))
            self._selectedVersion = selectedFileVersion
        except:
            self._selectedVersion = None
            self.ui.Comment.setText ("A error occured accessing file version info.")
        
    def _resizeHistoryFrame (self, position) : 
        self._resizeWidgetHelper.setWidth  (self.ui.FileList, self.ui.HistoryFrame.width ()-14)         
        self._resizeWidgetHelper.setHeight  (self.ui.FileList, self.ui.HistoryFrame.height ()-20)
            
    def _resizeStatusFrame (self, position) : 
        self._resizeWidgetHelper.setWidth  (self.ui.CommentLbl, self.ui.StatusFrame.width ()-29)         
        self._resizeWidgetHelper.setWidth  (self.ui.Comment, self.ui.StatusFrame.width ()-14)
        self._resizeWidgetHelper.setHeight  (self.ui.Comment, self.ui.StatusFrame.height ()-44)
        
    def _internalWindowResize (self, newsize):                            
        if (self._resizeWidgetHelper != None) :
            self._resizeWidgetHelper.setWidth  (self.ui.Splitter, newsize.width () - 11)         
            self._resizeWidgetHelper.setHeight  (self.ui.Splitter, newsize.height () - 66)                   
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.RollBackBtn, newsize.width () - 242)         
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.CancelBtn, newsize.width () - 132)            
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.RollBackBtn, newsize.height () - 46)    
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.CancelBtn, newsize.height () - 46)    
        
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)                 
        self._internalWindowResize ( qresizeEvent.size () ) 

    def getSelectedVersion (self) :
        return self._selectedVersion
    
