#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 11 14:57:53 2018

@author: m160897
"""
from PyQt5 import QtCore
from PyQt5.QtWidgets import  QDialog, QProgressDialog
from rilcontourlib.util.rcc_util import ResizeWidgetHelper
from rilcontourlib.ui.qt_ui_autogen.RCC_MLSegmentationQuantificationAutogen import Ui_RCC_MLSegmentationQuantification
from rilcontourlib.ui.qt_widgets.TabDataTableWidget import TabDataTableWidget
from rilcontourlib.ui.rcc_segquantdialog import RCC_SegQuantDialog
from rilcontourlib.ui.RCC_ML_ModelTree import ML_ModelTree, ML_ModelTreeNode
from rilcontourlib.machinelearning.ML_AutoImportMLModel import ML_AutoImportMLModel
from rilcontourlib.util.DateUtil import DateUtil, TimeUtil
from rilcontourlib.machinelearning.ml_predict import ML_PredictionPreferedViewUtil

class RCC_QuantifySegmentationDlg (QDialog) :
    
    def __init__ (self, parent) :                
        self._parent = parent
        self._RunAutoImportMLModel = True
        self._ProjectDataset = parent._ProjectDataset
        QDialog.__init__ (self, parent)
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, False)
        self.ui = Ui_RCC_MLSegmentationQuantification ()      
        self.setWindowFlags(self.windowFlags () | QtCore.Qt.Dialog| QtCore.Qt.WindowStaysOnTopHint )         
        self.ui.setupUi (self)                
        self.ui.ApplyToSelectedSlices.setChecked (True)
        self._resizeWidgetHelper = ResizeWidgetHelper ()
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.CloseBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.QuantifyBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.ModelSplitter)
        self.ui.modelFrame.resizeEvent = self._modelFrameResize 
        self.ui.DescriptionOptionFrame.resizeEvent = self._tabFrameResize
        self.updateMLModelList ()
        self.ui.QuantifyROIMetricsForAllSlices.setChecked (True)
        self.ui.filterText.editingFinished.connect (self._filterTextChanged)
        self.ui.filterBySelectedROICheckBox.stateChanged.connect (self._filterBySelectedCheckBox)
        self.ui.QuantifyBtn.clicked.connect (self._quantifyBtn)
        self.ui.CloseBtn.clicked.connect (self._closeBtn)
        self.ui.clearLogBtn.clicked.connect (self._clearLogBtn)
        self.ui.QuantifyBtn.setEnabled (False)
        self.ui.LimitSegementationToSelectedROI.setEnabled (False)
        self._parent.ROIDefs.addSelectionChangedListener (self._ROISelectionChanged)
        self._treeSelectionChanged ()
        self._filterTextChanged ()
        self.ui.clearLogBtn.setEnabled (False)
        self._quantifyDialogSettings = None
        self.ui.ViewResultsBtn.clicked.connect (self._viewMostRecentResults)
        self._resultTables = None 
        
    
    def _viewMostRecentResults (self) :
        if self._resultTables is not None :
            tableResultsDlg = RCC_SegQuantDialog (self)
            for tablename, tabledata in self._resultTables.items ():
                widget = TabDataTableWidget (tableResultsDlg.ui.ResultsTabWidget)
                widget.setTableData (tablename, tabledata)
                tableResultsDlg.ui.ResultsTabWidget.addTab (widget, tablename)            
            tableResultsDlg.exec_ ()
        
    def _clearLogBtn (self) :
        self.ui.predictionLog.setHtml ("")
        self.ui.clearLogBtn.setEnabled (False)
        self._resultTables = None 
        self.ui.ViewResultsBtn.setEnabled(False)
        
    def updateMLModelList (self, UpdateTreeSelection = False) :
        self._coreModel = ML_ModelTree (self._ProjectDataset, self)
        self._coreModel.addRootNode (ML_ModelTreeNode ("Classification",self._coreModel ))
        self._coreModel.addRootNode (ML_ModelTreeNode ("Segmentation",self._coreModel ))
        if (self._RunAutoImportMLModel ) :
            autoImportMLModels = ML_AutoImportMLModel (self._ProjectDataset)
            autoImportMLModels.runNow (self._coreModel, self)
            self._RunAutoImportMLModel = False            
        ML_ModelTree.loadModelTreeFromPath (self._ProjectDataset.getMLHomeFilePath () , ExistingModelTreeToAddTo = self._coreModel, LoadModelTreeTextLeafs = False, ProjectDataset=self._ProjectDataset)              
        self.ui.ModelTreeView.setModel (self._coreModel)
        selectionModel = self.ui.ModelTreeView.selectionModel ()
        selectionModel.selectionChanged.connect (self._treeSelectionChanged)       
        if (UpdateTreeSelection) :
            self._treeSelectionChanged ()
        
    def _filterTextChanged (self):        
        self._quantifyDialogSettings = None
        modelTypeComboboxFilterTxt = "Segmentation"
        filterROIList = []
        if (self.ui.filterBySelectedROICheckBox.isChecked ()) :
            selectedROIListNameList = self._parent.ROIDefs.getSelectedROI ()
            for name in selectedROIListNameList :
                filterROIList.append (self._parent.ROIDefs.getROIDefCopyByName (name))
        filteredModel = self._coreModel.getFilteredTreeModel (modelType = modelTypeComboboxFilterTxt.lower (), filterROIList = filterROIList, filterText = self.ui.filterText.text ())  
        self.ui.ModelTreeView.setModel (filteredModel)      
        selectionModel = self.ui.ModelTreeView.selectionModel ()
        selectionModel.selectionChanged.connect (self._treeSelectionChanged)       
        
            
    def _filterBySelectedCheckBox (self) :
        self._filterTextChanged ()
   
    def getSelectedTreeNode (self)    :
        selection = self.ui.ModelTreeView.selectionModel ()
        item = selection.currentIndex()                 
        treeNode = item.internalPointer ()                               
        return treeNode    
    
    def _ROISelectionChanged (self) :        
        if (self.ui.filterBySelectedROICheckBox.isChecked ()) and self.isVisible ():
            self._filterTextChanged ()
        
    def _treeSelectionChanged (self, selection = None, deselected = None) :
        self._quantifyDialogSettings = None
        node = self.getSelectedTreeNode ()        
        enabled = node is not None and node.isMLModel ()   
        if (enabled) :
            mlModel = node.getMLModel ()
            self.ui.modelDescriptionTxt.setHtml (mlModel.getHtmlSummary ())
            
            self.ui.QuantifyBtn.setEnabled (True)
                        
            existingDataFlag = mlModel.getHowToHandlePreExistingDataFlags ()
            
            self.ui.ApplyToAllSlices.setChecked ("ImplicitySelectAllSlices" in existingDataFlag)
            self.ui.ApplyToSelectedSlices.setChecked ("ImplicitySelectAllSlices" not in existingDataFlag)
                        
            self.ui.LimitSegementationToSelectedROI.setEnabled (True)
            self.ui.ApplyToAllSlices.setEnabled (True)
            self.ui.ApplyModelToGroupBox.setEnabled (True)
            self.ui.ApplyToSelectedSlices.setEnabled (True)
            self.ui.QuantifyROIMetricsForGroupBox.setEnabled (True)
            self.ui.QuantifyROIMetricsForAllSlices.setEnabled (True)
            self.ui.QuantifyROIMetricsForSlicesWithAnySegementationData.setEnabled (True)
            self.ui.QuantifyROIMetricsForSliceswithROISegementation.setEnabled (True)
            
        else:
            self.ui.LimitSegementationToSelectedROI.setEnabled (False)
            self.ui.modelDescriptionTxt.setHtml ("")
            self.ui.QuantifyBtn.setEnabled (False)
            self.ui.ApplyToAllSlices.setEnabled (False)
            self.ui.ApplyModelToGroupBox.setEnabled (False)
            self.ui.ApplyToSelectedSlices.setEnabled (False)
            self.ui.QuantifyROIMetricsForGroupBox.setEnabled (False)
            self.ui.QuantifyROIMetricsForAllSlices.setEnabled (False)
            self.ui.QuantifyROIMetricsForSlicesWithAnySegementationData.setEnabled (False)
            self.ui.QuantifyROIMetricsForSliceswithROISegementation.setEnabled (False)
            
    
    def _quantifyModelCallBack (self, XPos = None, YPos = None, WholeSlice = False) :
       if (XPos is not None and YPos is not None) or (WholeSlice) :
         if (self._parent.NiftiVolume is not None ) :
            node = self.getSelectedTreeNode ()
            if node is not None and node.isMLModel ()    :
                mlModel = node.getMLModel ()            
                
                LimitSegmentationToSelectedROI = self.ui.LimitSegementationToSelectedROI.isChecked ()
                applyToAllSlices = self.ui.ApplyToAllSlices.isChecked ()       
                if self.ui.QuantifyROIMetricsForAllSlices.isChecked () :
                    QuantifyROIMetricsFor = "AllSlices"
                elif self.ui.QuantifyROIMetricsForSlicesWithAnySegementationData.isChecked () :
                    QuantifyROIMetricsFor = "SlicesWithAnySegmentationData"
                else:
                    QuantifyROIMetricsFor = "SlicesWithROISegmentationData"
                                
                options = {}                
                options["ApplyToAllSlices"] = applyToAllSlices
                options["QuantifyROIMetricsFor"] = QuantifyROIMetricsFor                
                if (not WholeSlice) :
                    options["PatchCenterPos"] = (XPos, YPos)
                if self._quantifyDialogSettings is None : 
                    self._quantifyDialogSettings = {}
                self._quantifyDialogSettings["PatchCenterPos"] = (XPos, YPos)
                
                progdialog = QProgressDialog(self)
                progdialog.setMinimumDuration (4)
                progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)                
                progdialog.setWindowTitle ("Quantifying ML Model")            
                progdialog.setWindowModality(QtCore.Qt.ApplicationModal)                
                progdialog.setRange(0,0)                
                progdialog.setMinimumWidth(300)
                progdialog.setMinimumHeight(100)                
                try:                    
                    progdialog.forceShow()                       
                    progdialog.setValue(0)
                    self._ProjectDataset.getApp ().processEvents()
                    
                    if self._parent.ROIDictionary.isROIinPerimeterMode () :                                                                     
                        loglines, self._resultTables = mlModel.quantifyMLModelSegmentation (self._parent.ui.XYSliceSelectorWidget,self._parent.ui.XYSliceView, self._parent.NiftiVolume, self._parent.ROIDictionary, options, App = self._ProjectDataset.getApp (), LimitSegmentationToSelectedROI = LimitSegmentationToSelectedROI, ProgressDialog = progdialog)                    
                    else:
                        PreferedViewList = mlModel.getModelAxisList ()
                        selectedSliceView, sliceSelector = ML_PredictionPreferedViewUtil.getPreferedSliceViewAndSliceSelection (PreferedViewList, self._parent)                            
                        if (selectedSliceView is not None and sliceSelector is not None ) :                            
                            loglines, self._resultTables = mlModel.quantifyMLModelSegmentation (sliceSelector, selectedSliceView, self._parent.NiftiVolume, self._parent.ROIDictionary, options, App = self._ProjectDataset.getApp (), LimitSegmentationToSelectedROI = LimitSegmentationToSelectedROI, ProgressDialog = progdialog)                    
                        else:                            
                            loglines = []
                            self._resultTables = None
                                                    
                finally:                    
                    progdialog.close ()
                    self._ProjectDataset.getApp ().processEvents()
                if len (loglines) > 0 :
                    loglines = "<br>".join (loglines)
                    currentText = self.ui.predictionLog.toPlainText()
                    if len (currentText) > 0 :
                        currentText = self.ui.predictionLog.toHtml()
                        currentText += "<hr>"
                    else:
                        currentText = self.ui.predictionLog.toHtml()
                    dateStr = DateUtil.dateToString (DateUtil.today ())
                    timeStr = TimeUtil.timeToString(TimeUtil.getTimeNow ())
                    DateTimeStr = dateStr + " at " +timeStr
                    currentText += "<b>Date Time: </b>"+DateTimeStr+"<br>"
                    self.ui.predictionLog.setHtml (currentText + loglines)
                    self.ui.DescriptionOptionTabWidget.setCurrentIndex (2)
                    self.ui.clearLogBtn.setEnabled (True)
                    self.ui.ViewResultsBtn.setEnabled(self._resultTables is not None)
                    if self._resultTables is not None :
                        self._viewMostRecentResults ()
                    
    def _getSelectionSliceView (self, mlModel) :
        if mlModel is not None and not self._parent.ROIDictionary.isROIinPerimeterMode () :
             PreferedViewList = mlModel.getModelAxisList ()
             selectedSliceView, _ = ML_PredictionPreferedViewUtil.getPreferedSliceViewAndSliceSelection (PreferedViewList, self._parent)    
        else:
             selectedSliceView, _ = ML_PredictionPreferedViewUtil.getPreferedSliceViewAndSliceSelection ([], self._parent) 
        return selectedSliceView
    
    def _getPredictedSliceCenter (self, mlModel) :
        if mlModel is not None and not self._parent.ROIDictionary.isROIinPerimeterMode () :
            PreferedViewList = mlModel.getModelAxisList ()
            selectedSliceView, _ = ML_PredictionPreferedViewUtil.getPreferedSliceViewAndSliceSelection (PreferedViewList, self._parent)              
            shape = selectedSliceView.getSliceShape ()
            return int (shape[0]/2), int (shape[1]/2)     
        sliceData = self._parent.NiftiVolume.getImageData ()[:,:, 0]
        return int (sliceData.shape[0]/2), int (sliceData.shape[1]/2)
    
    def _quantifyBtn (self) :           
        if (self._parent.NiftiVolume is not None ) :
            node = self.getSelectedTreeNode ()
            if node is not None and node.isMLModel ()    :
                mlModel = node.getMLModel ()            
                if not mlModel.isSegmentationModel () or self._parent.showFlattenDeepGrowDlg ("One-click generated segmentations must be flattened before a machine learning segmentation model can be quantified. Do you wish to flatten the segmentation?") :
                    if mlModel.getModelInputTransformation().isPredictOnPatchSet () :                                                        
                        #if self._quantifyDialogSettings is None or "PatchCenterPos" not in self._quantifyDialogSettings :                                           
                        inputDataSize = mlModel.getModelInputOutputDescription ().getInputDataSize()
                        xDim, yDim = inputDataSize[1], inputDataSize[2]
                        if xDim is None and yDim is None :
                            xDim, yDim =  self._getPredictedSliceCenter (mlModel)                   
                            self._quantifyModelCallBack  (xDim, yDim, WholeSlice = False)
                        else:     
                            self._parent.getRangeSelectionBoundingBox (xDim, yDim, self._quantifyModelCallBack, SliceView = self._getSelectionSliceView(mlModel))
                        #else:
                        #    posx, posy = self._quantifyDialogSettings["PatchCenterPos"]
                        #    self._quantifyModelCallBack (posx, posy, WholeSlice = False)
                    else:
                        self._quantifyModelCallBack (WholeSlice = True)          

        
    def _closeBtn (self) :
        self._parent.ROIDefs.removeSelectionChangedListener (self._ROISelectionChanged)
        self.close ()
    
    def closeEvent (self, event) :
        self._parent.ROIDefs.removeSelectionChangedListener (self._ROISelectionChanged)        
        
    def _modelFrameResize (self, event) :
        width = self.ui.modelFrame.width ()
        self._resizeWidgetHelper.setWidth (self.ui.filterText, width - 75 )        
        self._resizeWidgetHelper.setWidth (self.ui.filterBySelectedROICheckBox, width - 75 )        
        self._resizeWidgetHelper.setWidth (self.ui.ModelTreeView, width - 18 )
        self._resizeWidgetHelper.setHeight(self.ui.ModelTreeView, self.ui.modelFrame.height () - 100)
    
    def _tabFrameResize (self, event) :
        width = self.ui.DescriptionOptionFrame.width ()
        height = self.ui.DescriptionOptionFrame.height ()
        
        self._resizeWidgetHelper.setWidth (self.ui.DescriptionOptionTabWidget, width  )        
        self._resizeWidgetHelper.setHeight(self.ui.DescriptionOptionTabWidget, height)
        
        self._resizeWidgetHelper.setWidth (self.ui.ModelDescriptionLbl, width - 41 )
        self._resizeWidgetHelper.setWidth (self.ui.modelDescriptionTxt, width - 14 )        
        self._resizeWidgetHelper.setHeight(self.ui.modelDescriptionTxt, height - 74)

        self._resizeWidgetHelper.setWidth (self.ui.ApplyModelToGroupBox, width - 20 )        
        self._resizeWidgetHelper.setWidth (self.ui.QuantifyROIMetricsForGroupBox, width - 20 )        
        self._resizeWidgetHelper.setWidth (self.ui.ApplyToAllSlices, width - 40 )        
        self._resizeWidgetHelper.setWidth (self.ui.ApplyToSelectedSlices, width - 40 )        
        self._resizeWidgetHelper.setWidth (self.ui.QuantifyROIMetricsForAllSlices, width - 40 )        
        self._resizeWidgetHelper.setWidth (self.ui.QuantifyROIMetricsForSlicesWithAnySegementationData, width - 40 )        
        self._resizeWidgetHelper.setWidth (self.ui.QuantifyROIMetricsForSliceswithROISegementation, width - 40 )        
        
        self._resizeWidgetHelper.setWidth (self.ui.predictionLog, width - 18 )
        self._resizeWidgetHelper.setHeight (self.ui.predictionLog, height - 90 )
        self._resizeWidgetHelper.setWidgetXPos (self.ui.clearLogBtn, width - 111 )
        self._resizeWidgetHelper.setWidgetYPos (self.ui.clearLogBtn, height - 70 )
        self._resizeWidgetHelper.setWidgetYPos (self.ui.ViewResultsBtn, height - 70 )
        
        
        
    def _internalWindowResize (self, newsize):                            
        if (self._resizeWidgetHelper != None) :                                               
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.ModelSplitter, newsize)            
            self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.ModelSplitter, newsize)                                                
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.QuantifyBtn, newsize.height () - 38)                        
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.QuantifyBtn, newsize.width () - 289)                                
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.CloseBtn, newsize.height () - 38)                        
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.CloseBtn, newsize.width () - 139)                        
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.LimitSegementationToSelectedROI, newsize.height () - 34)     
            self._resizeWidgetHelper.setWidth (self.ui.LimitSegementationToSelectedROI, self.ui.QuantifyBtn.pos().x() - self.ui.LimitSegementationToSelectedROI.pos().x() - 5)

                                       
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)                 
        self._internalWindowResize ( qresizeEvent.size () )               
    
    
   