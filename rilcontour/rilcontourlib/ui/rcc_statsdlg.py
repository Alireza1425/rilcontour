#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  6 15:33:56 2016

@author: m160897
"""
import numpy as np
import math
#import math
import locale
from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import  QDialog
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import (FigureCanvasQTAgg as FigureCanvas)
from rilcontourlib.ui.qt_ui_autogen.rcc_stats import Ui_RCC_StatsUI
#from scipy.spatial.distance import pdist
#from scipy import ndimage 
#from scipy.spatial.distance import squareform         
from rilcontourlib.util.rcc_util import AreaObjStatisticsHelper                                            

class MatplotlibWidget(FigureCanvas):        
    def __init__(self, parent=None, title='Title', xlabel='x label', ylabel='y label', dpi=72, hold=False):
        super(MatplotlibWidget, self).__init__(Figure())
        self.setParent(parent)
        self.figure = Figure(dpi=dpi)
        self.canvas = FigureCanvas(self.figure)
        self._hasPlot = False        

    def clearVoxelHistogram (self) :
        if (self._hasPlot) :
            self.figure.clf ()            
            self.draw ()
    
    def plotVoxelHistogram (self, voxels, color) :
        self.theplot = self.figure.add_subplot(111)                           
        #data = np.histogram (voxels, bins = 20)
        self.theplot.hist (voxels,bins= 20, facecolor=color.name ())                
        self._hasPlot = True
        self.theplot.set_ylabel ('Voxel count')        
        
    def drawVoxelHistogram (self):
        self.draw ()
        
class StatsDlg (QDialog) :
    
    def __init__ (self, parent) :         
        QDialog.__init__ (self, parent)
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, False)         
        self.ui = Ui_RCC_StatsUI ()                
        self.ui.setupUi (self)
        self._Parent = parent
        self.ui.CloseBtn.clicked.connect (self.HideBtn)        
        self.ui.SliceVolumeSelectionComboBox.addItem ("Volume Statistics")
        self.ui.SliceVolumeSelectionComboBox.addItem ("Slice Statistics")
        self.ui.SliceVolumeSelectionComboBox.currentIndexChanged.connect (self.SliceVolumeSelectionChange)
        self._LastNiftiVolume = None
        self._LastROIDictionary = None
        self._LastSelectedCoordinate = None
        
        
        self.mplwidget = MatplotlibWidget(self.ui.matplotlib)
        self.mplwidget.setGeometry(QtCore.QRect(0, 0, self.ui.matplotlib.width (), self.ui.matplotlib.height ()))
        self.mplwidget.setObjectName("mplwidget")
        self.setWindowFlags(self.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)
        

              
    def _setDialogText (self, Name, Color, mean, count, median, stdev, roiAreaCount, roiContourCount, roiPointCount, NifitVolume, isSlice, largestSliceNumber, largestSliceVoxelCount, maxObjectInSliceVoxelDistanceinMM) :
        self.ui.ROINameLbl.setText (Name)
        palette = QtGui.QPalette()        
        palette.setColor (self.ui.ROINameLbl.foregroundRole(), Color)        
        self.ui.ROINameLbl.setPalette(palette)      
        if (NifitVolume is None or largestSliceNumber is None or largestSliceVoxelCount is None or largestSliceNumber == -1 or largestSliceVoxelCount == -1) :            
            self.ui.LargestSliceDiameter.setText ("")
            self.ui.LargestSliceIndex.setText ("")        
            self.ui.LargestObjectInSliceVoxelDistanceinMM.setText ("")        
        else:
            if (isSlice):
                self.ui.LargestSliceIndex.setText ("Slice index: " + str (largestSliceNumber))        
            else:
                self.ui.LargestSliceIndex.setText ("Largest slice index: " + str (largestSliceNumber))        

            maxObjectInSliceVoxelDistanceinMM = locale.format("%0.2f", maxObjectInSliceVoxelDistanceinMM, grouping=True)           
            if (isSlice):
                self.ui.LargestObjectInSliceVoxelDistanceinMM.setText ("Greatest distance in selected slice: %smm" % (maxObjectInSliceVoxelDistanceinMM))       
            else:
                self.ui.LargestObjectInSliceVoxelDistanceinMM.setText ("Greatest object distance in largest slice: %smm" % (maxObjectInSliceVoxelDistanceinMM))        
        
        if (mean is not None) :
            #mean = ("%0.2f" % mean)
            mean = locale.format("%0.2f", mean, grouping=True)                        
            self.ui.MeanLbl.setText ("Mean: "+ str (mean))
        else:
            self.ui.MeanLbl.setText ("Mean: NA")            
            
        if (stdev is not None) :
            #stdev = ("%0.2f" % stdev)
            stdev = locale.format("%0.2f", stdev, grouping=True)                        
            self.ui.StdevLbl.setText ("Standard dev: "+ str (stdev))
        else:
            self.ui.StdevLbl.setText ("Standard dev: NA")        
            
        if (median is not None) :            
            median = locale.format("%0.2f", median, grouping=True)                        
            self.ui.MedianLbl.setText ("Median: "+ str (median))            
        else:
            self.ui.MedianLbl.setText ("Median: NA")
            
        if (roiAreaCount is not None) :
            roiAreaCount = locale.format("%d", roiAreaCount, grouping=True)                        
            self.ui.ROIAreaCountLbl.setText ("ROI (Area): "+ str (roiAreaCount))            
        else:
            self.ui.ROIAreaCountLbl.setText ("ROI (Area): NA")
            
        if (roiContourCount is not None) :
            roiContourCount = locale.format("%d", roiContourCount, grouping=True)                        
            self.ui.ROIAreaContourCountLbl.setText ("Contours: "+ str (roiContourCount))            
        else:
            self.ui.ROIAreaContourCountLbl.setText ("Contours: NA")
            
        if (roiPointCount is not None) :
            roiPointCount = locale.format("%d", roiPointCount, grouping=True)                        
            self.ui.ROIPointCountLbl.setText ("ROI (Points): "+ str (roiPointCount))            
        else:
            self.ui.ROIPointCountLbl.setText ("ROI (Points): NA")
            
        if (NifitVolume is None) :
            self.ui.VoxelSliceLbl.setText ("Slice (X,Y): NA")
            self.ui.SliceThicknessLbl.setText ("Slice thickness: NA")
            self.ui.AreaLbl.setText ("")
            self.ui.VolumeAreaLbl.setText ("Volume & Area:")
            self.ui.AreaLbl.setText ("Area: NA")                
            self.ui.VolumeLbl.setText ("Volume: NA")        
            self.ui.VoxelCountLbl.setText ("Voxel count: NA")        
            
            self.ui.ScanVolumeVoxelDimLbl.setText ("Voxels (X,Y,Z): NA")
            self.ui.ScanVolumeLbl.setText ("Volume: NA")
            self.ui.A_AxisLbl.setText ("Axial: NA")
            self.ui.C_AxisLbl.setText ("Coronal: NA")
            self.ui.S_AxisLbl.setText ("Sagittal: NA")
        else:            
                        
            
            x, y, z = NifitVolume.getVoxelSize ()
            voxelUnits = NifitVolume.getVoxelUnits ()
            if voxelUnits == "cm" :
                x *= 10.0
                y *= 10.0
                z *= 10.0
            elif voxelUnits != "mm" :
                print ("")
                print ("Unexpected voxel units: " + str (voxelUnits))
                print ("")
            x = round (float(x), 2)
            y = round (float(y), 2)
            z = round (float(z), 2)        
            
            """if (isSlice) :            
                sliceZVoxelDim = z
                if  self._Parent.ui.XYSliceView.areAxisProjectionControlsEnabledForAxis () :
                    self.ui.RotatedVoxelSliceLbl.setVisible (True)                    
                    _, rotatedAxisVoxels = self._Parent.ui.XYSliceView.getAxisProjectionControls().getAxialViewProjection(NifitVolume)                    
                    sliceXVoxelDim = round(float (rotatedAxisVoxels[0]),2)
                    sliceYVoxelDim = round(float (rotatedAxisVoxels[1]),2)                    
                    self.ui.RotatedVoxelSliceLbl.setText ("Rotated slice (X,Y): " + str (sliceXVoxelDim) + "mm x "+ str (sliceYVoxelDim) + "mm" )                    
                else:
                    sliceXVoxelDim = x
                    sliceYVoxelDim = y        """            
                
            self.ui.ScanVolumeVoxelDimLbl.setText ("Voxels (X,Y,Z): %d x %d x %d" % (NifitVolume.getXDimSliceCount (), NifitVolume.getYDimSliceCount (), NifitVolume.getZDimSliceCount ()))
            totalVoxels = NifitVolume.getXDimSliceCount () * NifitVolume.getYDimSliceCount () * NifitVolume.getZDimSliceCount ()
            totalVolume = float (totalVoxels) * (x * y * z)
            totalVolumeStr = locale.format("%0.1f", totalVolume, grouping=True)                        
            self.ui.ScanVolumeLbl.setText ("Volume: %s mm^3" % (totalVolumeStr))
            
            self.ui.A_AxisLbl.setText ("Axial: " + NifitVolume.getDisplayAxis ("Axial"))
            self.ui.C_AxisLbl.setText ("Coronal: " + NifitVolume.getDisplayAxis ("Coronal"))
            self.ui.S_AxisLbl.setText ("Sagittal: " + NifitVolume.getDisplayAxis ("Sagittal"))        
            
            xdim  = ("%0.2f" % x)
            ydim  = ("%0.2f" % y)
            zdim  = ("%0.2f" % z)
            self.ui.VoxelSliceLbl.setText ("Slice (X,Y): " + str (xdim) + "mm x "+ str (ydim) + "mm" )
            self.ui.SliceThicknessLbl.setText ("Slice thickness: " + str (zdim) + "mm")
            self.ui.AreaLbl.setText ("")
            self.ui.VolumeAreaLbl.setText ("Volume:")
            Area = -1
                    
            if (isSlice) :
                Area = x * y  * float (count)
            elif (largestSliceVoxelCount is not None and largestSliceVoxelCount != -1) :
                Area = x * y * float (largestSliceVoxelCount)
            if (Area != -1) :
                #Area  = ("%0.1f" % Area)
                AreaStr = locale.format("%0.1f", Area, grouping=True)
                if (isSlice) :
                    self.ui.AreaLbl.setText ("Area: " + AreaStr + "mm^2")                
                else:
                    self.ui.AreaLbl.setText ("Largest slice area: " + AreaStr + "mm^2")                
                self.ui.VolumeAreaLbl.setText ("Volume & Area:")            
                if (largestSliceVoxelCount != -1) :                    
                    Diameter = AreaObjStatisticsHelper.getDerivedDiameter (Area)
                    if (isSlice) :
                        self.ui.LargestSliceDiameter.setText ("Diameter: %.1fmm  (derieved)" % Diameter)       
                    else:
                        self.ui.LargestSliceDiameter.setText ("Largest slice diameter: %.1fmm (derieved)" % Diameter)       

         
            Volume = x * y * z * float (count)
            #Volume  = ("%0.1f" % Volume)
            Volume = locale.format("%0.1f", Volume, grouping=True)                        
            self.ui.VolumeLbl.setText ("Volume: " + str (Volume) + "mm^3")        
            count = locale.format("%d", count, grouping=True)                        
            self.ui.VoxelCountLbl.setText ("Voxel count: "+ str (count))        
        
    
    def updateDlg (self, NiftiVolume = None, ROIDictionary = None, SelectedCoordinate = None) :        
          largestSliceNumber = -1
          largestSliceVoxelCount = -1
          maxObjectInSliceVoxelDistance = -1
          
          self._LastNiftiVolume = NiftiVolume
          self._LastROIDictionary = ROIDictionary
          self._LastSelectedCoordinate = SelectedCoordinate
          
          self.mplwidget.clearVoxelHistogram ()                                 
          self.ui.SliceVolumeSelectionComboBox.setEnabled (False)
          SliceVolumeSelectionComboBoxIndex = self.ui.SliceVolumeSelectionComboBox.currentIndex ()
          isSlice = (SliceVolumeSelectionComboBoxIndex == 1)

          if (NiftiVolume == None or ROIDictionary == None or SelectedCoordinate == None) :
             self._setDialogText ("None",QtGui.QColor (0,0,0), None, 0, None, None, None, None, None, NiftiVolume, isSlice, largestSliceNumber, largestSliceVoxelCount, maxObjectInSliceVoxelDistance)            
          elif not ROIDictionary.getROIDefs ().isROISelected () :                                
             self._setDialogText ("None",QtGui.QColor (0,0,0), None, 0, None, None, None, None, None, NiftiVolume, isSlice, largestSliceNumber, largestSliceVoxelCount, maxObjectInSliceVoxelDistance)
          else:             
             ROILst = ROIDictionary.getROIDefs ().getSelectedROI ()
             voxelSet = []      
             #SliceVoxelCount = int (NiftiVolume.getXDimSliceCount ()  * NiftiVolume.getYDimSliceCount ())             
             if (len (ROILst) == 1) :
                 ROIName  = ROILst[0]
                 ROIColor = ROIDictionary.getROIDefs ().getROIColor (ROIName)
             else:
                 ROIName  = "Multiple ROI Selected"
                 ROIColor = QtGui.QColor (0,0,0)             
             ROIAreaCount    = 0
             ROIPointCount   = 0
             ROIContourCount = 0
             for roiName in ROILst :
                 if ROIDictionary.isROIDefined (roiName) :                                                                      
                     obj = ROIDictionary.getROIObject(roiName)                       
                     if (SliceVolumeSelectionComboBoxIndex == 0) :
                         voxels, ContoursOrPoints = obj.getVoxels (NiftiVolume, ROIDefs = ROIDictionary.getROIDefs ())                                           
                     else :
                         voxels, ContoursOrPoints = obj.getSliceVoxels (NiftiVolume, SelectedCoordinate.getZCoordinate (), ROIDictionary.getROIDefs (), SliceView = self._Parent.ui.XYSliceView)
                     if voxels.shape[0] > 0 :                                                 
                        voxelSet.append (voxels)
                     if (obj.isROIArea ()) :                                                                         
                         sliceLst = []
                         if (len (ROILst) == 1 and not isSlice) :
                             sliceLst = obj.getSliceNumberList ()                                                                                                                                                                               
                         elif (len (ROILst) == 1 and isSlice) :
                             sliceLst     =   [SelectedCoordinate.getZCoordinate ()]
                         if (len (sliceLst) > 0) :
                            largestSliceNumber, largestSliceVoxelCount, maxObjectInSliceVoxelDistance, ContoursOrPoints = AreaObjStatisticsHelper.getAreaObjStatsHelper (NiftiVolume, obj, sliceLst, ROIDictionary.getROIDefs () )
                            
                     
                         ROIAreaCount += 1
                         ROIContourCount += ContoursOrPoints
                     else:
                         ROIPointCount += ContoursOrPoints
             self.ui.SliceVolumeSelectionComboBox.setEnabled (True)
             if (len (voxelSet) > 0):
                 voxelList = np.concatenate (voxelSet, axis = 0)
                 count = voxelList.shape[0]                  
                 self._setDialogText (ROIName, ROIColor, np.mean (voxelList), count, np.median  (voxelList), math.sqrt(np.var (voxelList,ddof=1)), ROIAreaCount, ROIContourCount, ROIPointCount, NiftiVolume, isSlice, largestSliceNumber, largestSliceVoxelCount, maxObjectInSliceVoxelDistance)
                     
                 self.mplwidget.plotVoxelHistogram (voxelList, ROIColor)                                 
             else:
                 self._setDialogText (ROIName, ROIColor,None, 0, None, None, None, None, None, NiftiVolume, isSlice, largestSliceNumber, largestSliceVoxelCount, maxObjectInSliceVoxelDistance)
          self.mplwidget.drawVoxelHistogram ()                                 
                 
              
                 
    def SliceVolumeSelectionChange (self) :
        self.updateDlg (self._LastNiftiVolume, self._LastROIDictionary, self._LastSelectedCoordinate )                
        
    def HideBtn (self) :
        self.close ()                       
