# -*- coding: utf-8 -*-
"""
Created on Tue Sep 13 11:28:10 2016

@author: Philbrick
"""    
import os
import copy
import tempfile
import numpy as np
import shutil
import nibabel as nib
import math
from six import *
import time
import sys
import gc
import functools
import multiprocessing
from collections import OrderedDict
from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import   QDialog, QMainWindow, QListWidgetItem, QColorDialog, QSpinBox, QLineEdit, QApplication, QProgressDialog, QFileDialog, QAbstractItemView
from rilcontourlib.dataset.roi.rcc_roi import ROIDefinitions, ROIDef, ROIDictionary
from rilcontourlib.dataset.roi.rcc_roiobjects import MultiROIAreaObject, ROIPointObject
from rilcontourlib.util.rcc_util import NiftiVolumeData, Coordinate, HounsfieldVisSettings, ResizeWidgetHelper,VisSettings, ExportVolumeStatsCSV, FindContours, PenSettings, RCC_NoCancelProgressDlg, FastUnique
from rilcontourlib.util.rcc_NIfTIDataFileHandlers import PesscaraDF
from rilcontourlib.util.RCC_AutoContour import RCC_AutoContourROIArea #, RCC_Morph
try:
    try:
        from OpenGL.GL import *
        print ("OpenGL.GL imported")
        from OpenGL.GLU import *
        print ("OpenGL.GLU imported")
        opengl_imported = True
    except:
        opengl_imported = False
    from rilcontourlib.ui.rcc_volumeviewerui import VolumeViewerUI
except:
    pass
from rilcontourlib.ui.rcc_batchcontourdlg import BatchContourUIDlg
from rilcontourlib.ui.rcc_statsdlg import StatsDlg
from rilcontourlib.ui.rcc_hounsfieldVisOptionsDlg import RCC_HounsfieldVisOptionsDlg
from rilcontourlib.ui.RCC_ROIVolumeMaskExportDlg import RCC_ROIVolumeMaskExportDlg
from rilcontourlib.ui.rcc_PesscaraMaskViewerDlg import PesscaraMaskViewerDlg
from rilcontourlib.ui.qt_ui_autogen.rcc_registrationLogViewerDlgAutoGen import Ui_rcc_registrationLogViewerDlg
from rilcontourlib.ui.qt_ui_autogen.rcc_vissettings import Ui_VisSettings
from rilcontourlib.ui.qt_ui_autogen.rcc_roieditor import Ui_RCC_ROIEditor
from rilcontourlib.ui.qt_ui_autogen.rcc_mainwindow import Ui_RCC_MainWindow
from rilcontourlib.ui.qt_ui_autogen.rcc_contournameeditor import Ui_RCC_CountourNameEditor
from rilcontourlib.ui.qt_ui_autogen.rcc_roitranslationdlgautogen import Ui_rcc_ROITranslationDlg
from rilcontourlib.ui.qt_ui_autogen.rcc_mlscandescriptiondlgAutoGen import Ui_RCC_MLDescriptionDlg
from rilcontourlib.ui.qt_ui_autogen.RCC_PenSettingsDlgAutogen import Ui_RCC_PenSettingsDlg
from rilcontourlib.ui.qt_ui_autogen.RCC_ThresholdPresetEditorDlgAutogen import Ui_PenThresholdPresetEditorDlg
from rilcontourlib.ui.qt_ui_autogen.rcc_drag_drop_niftidlgautogen import Ui_NIfTDragDropDlg
from rilcontourlib.ui.qt_ui_autogen.rcc_selectROIdlgAutogen import Ui_MultipleROISelectedDlg
from rilcontourlib.ui.qt_ui_autogen.RCC_OneClickSegmentationDlg import Ui_OneClickSegmentationDlg
from rilcontourlib.ui.qt_ui_autogen.rcc_n4b_corrrectionDlgAutogen import Ui_N4BCorrectionDlg
from rilcontourlib.ui.RCC_FileVersionUI import FileVersionUI
from rilcontourlib.ui.RCC_FilePLockUI import FilePLockUI
from rilcontourlib.ui.rcc_roimaskimportdlg import RCC_ROIMaskImportDlg
from rilcontourlib.radlex.rcc_radlex import RadLexUITree
from PyQt5.QtWidgets import QMenu, QAction
from scipy import ndimage
from rilcontourlib.util.rcc_util import ShortcutKeyList, MessageBoxUtil
from rilcontourlib.util.LongFileNameAPI import LongFileNameAPI
from rilcontourlib.util.rcc_NIfTIDataFileHandlers import FileSystemDF
from rilcontourlib.machinelearning.ml_DatasetInterface import ML_KerasModelLoaderInterface
from rilcontourlib.dataset.RCC_ROIFileLocks import PersistentFSLock, NonPersistentFSLock
from rilcontourlib.util.PandasCompatibleTable import PandasCompatibleTable
from rilcontourlib.util.DateUtil import DateUtil, TimeUtil
from rilcontourlib.ui.rcc_export_tagsDlg import RCC_ExportTagsDlg
try:
    from rilcontourlib.ui.RCC_ML_ModelManagerDlg import RCC_ModelManagerDlg 
    from rilcontourlib.ui.RCC_ML_ModelZooDlg import RCC_ModelZooDlg
    from rilcontourlib.ui.RCC_QuantifySegmentationDlg import RCC_QuantifySegmentationDlg
except:
    pass        

from rilcontourlib.ui.RCC_DatasetTagEditorUIDlg import RCC_DatasetTagingDlg
from rilcontourlib.ui.qt_ui_autogen.RCC_DrawToolsDlgAutogen import Ui_DrawToolsWindow
from rilcontourlib.util.FileUtil import FileUtil
from rilcontourlib.dataset.roi.markers import AxialAxisProjectionControls
#from numba import jit

        

class N4BiasConversionDlg (QDialog) :
    def  __init__ (self, parentWindow, FileName, N4BiasMaskMode, N4BiasMaskThreshold) :
        self._process = None
        self._conversionResult = False                
        QDialog.__init__ (self, parentWindow)            
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)        
        self.ui = Ui_N4BCorrectionDlg ()        
        self.ui.setupUi (self)  
        self.ui.CancelBtn.clicked.connect (self._cancel)            
        self._messageQueue = multiprocessing.Queue()        
        self._progressQueue = multiprocessing.Queue()        
        #self._conversionResult = N4BiasConversionDlg.N4BConversionProcess (FileName, self._messageQueue, N4BiasMaskMode, N4BiasMaskThreshold, self._progressQueue)
        try :
            self._process = multiprocessing.Process(target=N4BiasConversionDlg.N4BConversionProcess, args=(FileName, self._messageQueue, N4BiasMaskMode, N4BiasMaskThreshold, self._progressQueue))
            self._process.start ()            
        except:
           self._process = None
        

    @staticmethod
    def N4BConversionProcess (path,messageQueue, N4BiasMaskMode, N4BiasMaskThreshold, N4BProgressQueue) :
        try :
            nifti = nib.load (path)
            niftiVolumeData = NiftiVolumeData (nifti, OpenAndApplyN4BiasCorrection = True, N4BiasMaskMode = N4BiasMaskMode, N4BiasMaskThreshold =N4BiasMaskThreshold, N4BProgressQueue = N4BProgressQueue)
            retval = niftiVolumeData.didN4BiasCorrectionSucceed ()        
        except:
            retval = False
        messageQueue.put (retval)
        return retval 
        
    def waitForProcessToFinish (self,projectDataset) :                
        while (self._process is not None) :
            projectDataset.getApp ().processEvents ()            
            try :
                while self._progressQueue.qsize () > 0 :                    
                    try :
                        progressResult = self._progressQueue.get(False)
                        if progressResult[0] == "range" :
                            minV = progressResult[1]
                            maxV = progressResult[2]
                            value = progressResult[3]
                            self.ui.progressBar.setMinimum (minV)
                            self.ui.progressBar.setMaximum (maxV)
                            self.ui.progressBar.setValue (value)
                        elif progressResult[0] == "value" :
                            value = progressResult[1]
                            self.ui.progressBar.setValue (value)                            
                    except:
                        pass
            except:
                self.ui.progressBar.setMin (0)
                self.ui.progressBar.setMax (0)
                pass
            
            try :
                if not self._process.is_alive () :
                    break
                elif self._messageQueue.qsize () > 0 :
                    result = self._messageQueue.get(False)                    
                    print (("Message Returned", result))                    
                    self._conversionResult = result  
                    break
            except :
                self._conversionResult = False                
                        
        self.close ()
            
    
    def getN4BiasConversionResult (self) :
        return self._conversionResult
    
    def _cancel (self) :
        try :
            if self._process is not None and self._process.is_alive () :
                self._process.terminate()
                self._process.join ()
        except:
            pass
            
        
        
        
class ShouldFlattenDeepGrowAnnotationDlg (QDialog) :
    def  __init__ (self, parentWindow, Msg = None, Cancel = True) :
        QDialog.__init__ (self, parentWindow)            
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)        
        self.ui = Ui_OneClickSegmentationDlg ()        
        self.ui.setupUi (self)  
        if Msg is not None :                
            self.ui.label.setText (Msg)
        self.ui.AcceptSegmentationBtn.clicked.connect (self._acceptBtn)
        self.ui.RejectSegmentationBtn.clicked.connect (self._clearBtn)
        if Cancel :
            self.ui.CancelBtn.setEnabled (True)
            self.ui.CancelBtn.clicked.connect (self.close) 
            self._result = "Cancel"
        else:
            self.ui.CancelBtn.setEnabled (False)
            self._result = "Clear"
    
    def _acceptBtn (self) :
        self._result = "Flatten"
        self.close ()
    
    def _clearBtn (self) :
        self._result = "Clear"
        self.close ()
        
    def getResult (self) :
        return self._result
    
class SelectROIToFillDlg (QDialog) :
    
    def  __init__ (self, parentWindow, ROIList, Msg = None) :
        QDialog.__init__ (self, parentWindow)    
        self._ROIList = ROIList
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, False)        
        self.ui = Ui_MultipleROISelectedDlg ()        
        self.ui.setupUi (self)                  
        self._returnValue = (None, None)
        self._drawMask = None
        if Msg is not None :
            self.ui.label.setText (Msg)
        self._resizeWidgetHelper = ResizeWidgetHelper ()                
        for item in ROIList :
            self.ui.widgetList.addItem (str(item[0]))
        self.ui.widgetList.currentRowChanged.connect (self._RowSelected)
        self.ui.OkBtn.clicked.connect (self.OkBtnPressed)
        self.ui.CancelBtn.clicked.connect (self.CancelBtnPressed)
        
    def _RowSelected (self, row) :
       try :           
           _, self._returnValue, self._drawMask = self._ROIList[row]
       except:
           self._returnValue = (None, None)
           self._drawMask = None
       
    def OkBtnPressed (self) :
        self.accept ()
        
    def CancelBtnPressed (self) :
        self._returnValue = (None, None)
        self._drawMask = None
        self.reject ()
        
    def getSelectedObject (self) :
        return self._returnValue, self._drawMask

    def _internalWindowResize (self, newsize):                            
        try :
            if (self._resizeWidgetHelper is not None) :                                               
                self._resizeWidgetHelper.setWidth  (self.ui.label, newsize.width () -18)            
                self._resizeWidgetHelper.setWidth  (self.ui.widgetList, newsize.width () -18)            
                self._resizeWidgetHelper.setHeight  (self.ui.widgetList, newsize.height ()-82)                        
                self._resizeWidgetHelper.setWidgetYPos  (self.ui.OkBtn, newsize.height () - 44)            
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.OkBtn, newsize.width () - 229)                   
                self._resizeWidgetHelper.setWidgetYPos  (self.ui.CancelBtn, newsize.height () - 44)            
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.CancelBtn, newsize.width () - 119)                   
        except:
            pass
            
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)                 
        self._internalWindowResize ( qresizeEvent.size () )   
        

class RCC_DrawToolDlg (QDialog) :
    
    def __init__ (self,  ProjectDataset, penSettings, parentWindow) :
        QDialog.__init__ (self, parentWindow)
        self._highlitedButton = None
        self._parentWindow = parentWindow
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, False)        
        self.ui = Ui_DrawToolsWindow ()        
        self.ui.setupUi (self)          
        self.setWindowTitle (" ")
        width = self.ui.PaintBtn.width ()        
        height =  self.ui.PaintBtn.height ()        
        self._penSettings = penSettings
        width *= 8
        height *= 1
        self.setMinimumSize(width,height)
        self.setMaximumSize(width,height)
        self.resize (width , height)       
        
        basepath = ProjectDataset.getPythonProjectBasePath ()
        iconPath = os.path.join (basepath , "icons")       
        self._iconState = {}
        self._iconState["Fill"] = (QtGui.QIcon(iconPath + os.sep + "fill.png"), QtGui.QIcon(iconPath + os.sep + "fillSel.png"))
        self._iconState["Paint"] = (QtGui.QIcon(iconPath + os.sep + "paint.png"), QtGui.QIcon(iconPath + os.sep + "paintSel.png"))
        self._iconState["Erase"] = (QtGui.QIcon(iconPath + os.sep + "erase.png"), QtGui.QIcon(iconPath + os.sep + "eraseSel.png"))
        self._iconState["selectionarrow"] = (QtGui.QIcon(iconPath + os.sep + "selectionarrow.png"), QtGui.QIcon(iconPath + os.sep + "selectionarrowSel.png"))
        self._iconState["zoomin"] = (QtGui.QIcon(iconPath + os.sep + "zoomin.png"), QtGui.QIcon(iconPath + os.sep + "zoominSel.png"))
        self._iconState["zoomout"] = (QtGui.QIcon(iconPath + os.sep + "zoomout.png"), QtGui.QIcon(iconPath + os.sep + "zoomoutSel.png"))
        self._iconState["DeepGrow"] = (QtGui.QIcon(iconPath + os.sep + "magic-wand.png"), QtGui.QIcon(iconPath + os.sep + "magic-wand-selected.png")) 
        self._iconState["SelectIsland"] = (QtGui.QIcon(iconPath + os.sep + "selectIsland.png"), QtGui.QIcon(iconPath + os.sep + "selectIslandSel.png")) 
        
        self.setWindowFlags(self.windowFlags () | QtCore.Qt.Dialog| QtCore.Qt.WindowStaysOnTopHint )         
        self._UpdateWindowToolSelection ()
        self.ui.PaintBtn.clicked.connect (self._paintToolClick)
        self.ui.FillBtn.clicked.connect  (self._fillToolClick)
        self.ui.EraseBtn.clicked.connect (self._eraseToolClick)
        self.ui.DeepGrowBtn.clicked.connect (self._deepGrowToolClick)
        self.ui.PaintBtn.setChecked (True)
        self.ui.ArrowSelectionBtn.clicked.connect (self._selectionToolClick)
        self.ui.ZoomInBtn.clicked.connect  (self._zoomInToolClick)
        self.ui.ZoomOutBtn.clicked.connect (self._zoomOutToolClick)
        self.ui.SelectIslandBtn.clicked.connect (self._selectIslandToolClick)
        if self._parentWindow.ui.XYSliceView.areAxisProjectionControlsEnabledForAxis() :
            self.setButtonEnabledState (DeepGrow = False)
        
    def keyPressEvent(self, event):
        self._parentWindow.keyPressEvent (event)
        
    def _clearButtonSelection (self):
        self._highlitedButton = None
        self._parentWindow.clearUIState()
        self._UpdateWindowToolSelection ()
    
       
    def getDeepGrowButtonEnabled (self) :
        return self.ui.DeepGrowBtn.isEnabled ()
    
    def setButtonEnabledState (self, Select = None, Draw = None, ZoomIn = None, ZoomOut = None, EraseandFill = None, DeepGrow = None):
        if self._parentWindow.ui.XYSliceView.areAxisProjectionControlsEnabledForAxis() :
            DeepGrow = False
        if (EraseandFill is not None) :            
            self.ui.FillBtn.setEnabled (EraseandFill)
            self.ui.EraseBtn.setEnabled (EraseandFill)
            self.ui.SelectIslandBtn.setEnabled (EraseandFill)
            if (not EraseandFill) :
                if (self._highlitedButton in ["Fill_Btn", "Erase_Btn", "SelectIsland_Btn"]) :
                    self._paintToolClick ()
                
        if (ZoomOut is not None) :
            self.ui.ZoomOutBtn.setEnabled (ZoomOut)
            if (not ZoomOut and self._highlitedButton == "ZoomOut_Btn") :
                self._clearButtonSelection ()
                
        if (ZoomIn is not None) :
            self.ui.ZoomInBtn.setEnabled (ZoomIn)   
            if (not ZoomIn and self._highlitedButton == "ZoomIn_Btn") :
                self._clearButtonSelection ()
                
        if (Select is not None) :
            self.ui.ArrowSelectionBtn.setEnabled (Select)
            if (not ZoomIn and self._highlitedButton == "ArrowSelection_Btn") :
                self._clearButtonSelection ()
                
        if (Draw is not None) :
            self.ui.PaintBtn.setEnabled (Draw)
            if (not Draw and self._highlitedButton == "Draw_Btn") :
                self._clearButtonSelection ()
                
        if (DeepGrow is not None) :
            self.ui.DeepGrowBtn.setEnabled  (DeepGrow)
            if (not DeepGrow and self._highlitedButton == "DeepGrow_Btn") :
                self._clearButtonSelection ()
    
    def _selectIslandToolClick (self) :
        if (self._parentWindow.getUIState() != "Contour" or not self._penSettings.isAddPen () or self._penSettings.getPenTool () != "SelectIsland") :
            if (not self._penSettings.isAddPen ()) :
                self._penSettings.setAddPen ()
                self._parentWindow.updatePenSettingsDlg ()
            self._penSettings.setPenTool ("SelectIsland")
            self.setUIStateWidgetState ("Contour")     
            
    def _deepGrowToolClick (self) :
        if (self._parentWindow.getUIState() != "Contour" or not self._penSettings.isAddPen () or self._penSettings.getPenTool () != "DeepGrow") :
            if (not self._penSettings.isAddPen ()) :
                self._penSettings.setAddPen ()
                self._parentWindow.updatePenSettingsDlg ()
            self._penSettings.setPenTool ("DeepGrow")
            self.setUIStateWidgetState ("Contour")   
            
    def _paintToolClick (self) :
        if (self._parentWindow.getUIState() != "Contour" or not self._penSettings.isAddPen () or self._penSettings.getPenTool () != "Draw") :
            if (not self._penSettings.isAddPen ()) :
                self._penSettings.setAddPen ()
                self._parentWindow.updatePenSettingsDlg ()
            self._penSettings.setPenTool ("Draw")
            self.setUIStateWidgetState ("Contour")
           
    def _fillToolClick (self) :        
        if (self._parentWindow.getUIState() != "Contour" or not self._penSettings.isAddPen () or self._penSettings.getPenTool () != "Fill") :
            if (not self._penSettings.isAddPen ()) :
                self._penSettings.setAddPen ()
                self._parentWindow.updatePenSettingsDlg ()
            self._penSettings.setPenTool ("Fill")
            self.setUIStateWidgetState ("Contour")        
            
    def _eraseToolClick (self) :
        if (self._parentWindow.getUIState() != "Contour" or not self._penSettings.isErasePen () or self._penSettings.getPenTool () != "Draw") :
            if (not self._penSettings.isErasePen ()) :
                self._penSettings.setErasePen ()
                self._parentWindow.updatePenSettingsDlg ()
            self._penSettings.setPenTool ("Draw")
            self.setUIStateWidgetState ("Contour")                    
    
    def _selectionToolClick (self) :
        if (self._parentWindow.getUIState() != "SelectionArrow") :
            self.setUIStateWidgetState ("SelectionArrow")        
            
    def _zoomInToolClick (self) :
        if (self._parentWindow.getUIState() != "Zoom In") :
            self.setUIStateWidgetState ("Zoom In")        
            
    def _zoomOutToolClick (self) :
        if (self._parentWindow.getUIState() != "Zoom Out") :
            self.setUIStateWidgetState ("Zoom Out")        
        
    def _UpdateWindowToolSelection (self) :
         state = self._parentWindow.getUIState()
         
         self._highlitedButton = None
         if (state != "SelectionArrow") :
             selectionButtonState = 0
         else:
             selectionButtonState = 1
             self._highlitedButton = "ArrowSelection_Btn"
         
         if (state != "Zoom Out") :
             zoomOutButtonState = 0
         else:
             zoomOutButtonState = 1
             self._highlitedButton = "ZoomOut_Btn"
            
         if (state != "Zoom In") :
             zoomInButtonState = 0
         else:
             zoomInButtonState = 1
             self._highlitedButton = "ZoomIn_Btn"
    
         if (state != "Contour" or self._penSettings.getPenTool () != "Draw" or not self._penSettings.isErasePen ()) :    
            eraseButtonState = 0 
         else:
            eraseButtonState = 1
            self._highlitedButton = "Erase_Btn"
            
         if (state != "Contour" or self._penSettings.getPenTool () != "Draw" or (not self._penSettings.isAddPen () and not self._penSettings.isDeepGrowPen ())) :    
            paintButtonState = 0 
         else:
            paintButtonState = 1
            self._highlitedButton = "Draw_Btn"        
            
         if (state != "Contour" or self._penSettings.getPenTool () != "Fill") :
            fillButtonState = 0
         else: 
            fillButtonState = 1
            self._highlitedButton = "Fill_Btn"
        
         if (state != "Contour" or self._penSettings.getPenTool () != "DeepGrow") :
             DeepGrowButtonState = 0
         else:
             DeepGrowButtonState = 1
             self._highlitedButton = "DeepGrow_Btn"
             
         if (state != "Contour" or self._penSettings.getPenTool () != "SelectIsland") :
             SelectIslandButtonState = 0
         else:
             SelectIslandButtonState = 1
             self._highlitedButton = "SelectIsland_Btn"
             
         self.ui.ZoomOutBtn.setIcon(self._iconState["zoomout"][zoomOutButtonState])
         self.ui.ZoomOutBtn.setIconSize(QtCore.QSize(32,32))
         
         self.ui.ZoomInBtn.setIcon(self._iconState["zoomin"][zoomInButtonState])
         self.ui.ZoomInBtn.setIconSize(QtCore.QSize(32,32))
        
         self.ui.ArrowSelectionBtn.setIcon(self._iconState["selectionarrow"][selectionButtonState])
         self.ui.ArrowSelectionBtn.setIconSize(QtCore.QSize(32,32))
        
         self.ui.EraseBtn.setIcon(self._iconState["Erase"][eraseButtonState])
         self.ui.EraseBtn.setIconSize(QtCore.QSize(32,32))
        
         self.ui.PaintBtn.setIcon(self._iconState["Paint"][paintButtonState])
         self.ui.PaintBtn.setIconSize(QtCore.QSize(32,32))
        
         self.ui.FillBtn.setIcon(self._iconState["Fill"][fillButtonState])
         self.ui.FillBtn.setIconSize(QtCore.QSize(32,32))
         
         self.ui.DeepGrowBtn.setIcon(self._iconState["DeepGrow"][DeepGrowButtonState])
         self.ui.DeepGrowBtn.setIconSize(QtCore.QSize(32,32))
         
         self.ui.SelectIslandBtn.setIcon(self._iconState["SelectIsland"][SelectIslandButtonState])
         self.ui.SelectIslandBtn.setIconSize(QtCore.QSize(32,32))
    
    def updateUIStateToRefectSelectedROIEditingState (self, EditingSelectedROIEnabled) :
         EraseandFillEnabled = True
         try :
            if self._parentWindow is not None and self._parentWindow.ROIDictionary is not None and self._parentWindow.ROIDictionary.isROIinPerimeterMode () :
                EraseandFillEnabled = False
            else:                
                if self._parentWindow.ROIDefs.isROISelected ()  :
                    for name in self._parentWindow.ROIDefs.getSelectedROI () :                        
                        if self._parentWindow.ROIDefs.getROIType (name) != "Area" :                
                            EraseandFillEnabled = False
                            break
         except:
            EraseandFillEnabled = False
         
         EditingAndEraseFillEnabled = EditingSelectedROIEnabled and EraseandFillEnabled
         self.ui.EraseBtn.setEnabled (EditingAndEraseFillEnabled)
         self.ui.PaintBtn.setEnabled (EditingSelectedROIEnabled)
         self.ui.FillBtn.setEnabled (EditingAndEraseFillEnabled)
         self.ui.SelectIslandBtn.setEnabled (EditingAndEraseFillEnabled)
         self.ui.DeepGrowBtn.setEnabled (EditingAndEraseFillEnabled and ML_KerasModelLoaderInterface.isMLSupportEnabled () and not self._parentWindow.ui.XYSliceView.areAxisProjectionControlsEnabledForAxis())
         self.update ()
         
    def setUIStateWidgetState (self, state) :                                
         self._parentWindow.setUIState (state)
         self._UpdateWindowToolSelection ()
         
         if (self._parentWindow.isZoomInXYUIState ()) : 
             self._parentWindow._XYZoomInit = None                          

         bbResizeable = self._parentWindow.isContourXYUIState()   
         bbVisible = self._parentWindow.isSelectionUIState () or bbResizeable           
                       
         self._parentWindow.ui.XYSliceView.setROIBoundingBoxVisible (bbVisible)
         self._parentWindow.ui.YZSliceView.setROIBoundingBoxVisible (bbVisible)
         self._parentWindow.ui.XZSliceView.setROIBoundingBoxVisible (bbVisible)                  
         
         self._parentWindow.ui.XYSliceView.setROIBoundingBoxResizeable (bbResizeable)
         self._parentWindow.ui.YZSliceView.setROIBoundingBoxResizeable (bbResizeable)
         self._parentWindow.ui.XZSliceView.setROIBoundingBoxResizeable (bbResizeable)
         coordinateInidcator = self._parentWindow.isSelectionUIState ()
         self._parentWindow.ui.XYSliceView.setDrawCoordinateIndicator (coordinateInidcator)
         self._parentWindow.ui.YZSliceView.setDrawCoordinateIndicator (coordinateInidcator)
         self._parentWindow.ui.XZSliceView.setDrawCoordinateIndicator (coordinateInidcator)         
         self._parentWindow.updatePaintBrushIfNecessary ()

class PenThresholdPresetEditorDlg (QDialog) :
    def __init__ (self, name, minV, maxV, autoErase, parent) :
        QDialog.__init__ (self, parent)        
        self._Parent = parent
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)        
        self.ui = Ui_PenThresholdPresetEditorDlg ()                
        self.ui.setupUi (self)          
        
        self.ui.PresetName.installEventFilter(parent._Parent._ChildWidgetOverrideEventFilter)
        self.ui.SetPenThresholdLowerThresholdMask.installEventFilter(parent._Parent._ChildWidgetOverrideEventFilter)
        self.ui.SetPenThresholdUpperThresholdMask.installEventFilter(parent._Parent._ChildWidgetOverrideEventFilter)
        self.ui.eraseOutofRangeChk.installEventFilter(parent._Parent._ChildWidgetOverrideEventFilter)
        self.ui.OkBtn.installEventFilter(parent._Parent._ChildWidgetOverrideEventFilter)
        self.ui.CancelBtn.installEventFilter(parent._Parent._ChildWidgetOverrideEventFilter)
                
        self.ui.PresetName.setText (name)
        self.ui.SetPenThresholdLowerThresholdMask.setValue (minV)
        self.ui.SetPenThresholdUpperThresholdMask.setValue (maxV)
        self.ui.eraseOutofRangeChk.setChecked (autoErase)
        self.ui.OkBtn.clicked.connect (self.OkBtn)       
        self.ui.CancelBtn.clicked.connect (self.reject)       
        self.results = ("Error", 0, 1024, True)        
        self.ui.SetPenThresholdUpperThresholdMask.focusOutEvent = self._setUpperThreshold
        self.ui.SetPenThresholdLowerThresholdMask.focusOutEvent = self._setLowerThreshold    
    
    def _setUpperThreshold (self, event):        
        upper = int (self.ui.SetPenThresholdUpperThresholdMask.value ())        
        lower = int (self.ui.SetPenThresholdLowerThresholdMask.value ())        
        if (lower > upper) :            
            self.ui.SetPenThresholdLowerThresholdMask.setValue (upper)
        
    def _setLowerThreshold (self, event):
        upper = int (self.ui.SetPenThresholdUpperThresholdMask.value ())        
        lower = int (self.ui.SetPenThresholdLowerThresholdMask.value ())        
        if (lower > upper) :
            self.ui.SetPenThresholdUpperThresholdMask.setValue (lower)        
        
    def OkBtn (self) :
        self.results = (self.ui.PresetName.text (), self.ui.SetPenThresholdLowerThresholdMask.value (), self.ui.SetPenThresholdUpperThresholdMask.value (), self.ui.eraseOutofRangeChk.isChecked ())
        self.accept ()
        
    def getResults (self) :
        return self.results 
        
            
class RCC_PenSettingsDlg (QDialog) :
    def __init__ (self, penSettings, parent) :
        QDialog.__init__ (self, parent)
        self._ThresholdsLastSetFromPreset = False
        self._cachedNiftiVolume = None      
        self._Parent = parent
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, False)        
        self.ui = Ui_RCC_PenSettingsDlg ()        
        self._PenSettings = penSettings        
        self.ui.setupUi (self)          
        self._setUpperThresholdFocuseOutEventEnabled = True
        self._updateThresholdPresetCombobox ()
                
        self.ui.ThresholdSelectionPresetComboBox.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)
        self.ui.AddThresholdPresetBtn.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)
        self.ui.EditThresholdPresetBtn.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)
        self.ui.RemoveThresholdPresetBtn.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)
        self.ui.RemberROIPenSettings.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)
        self.ui.SetThresholdToSelectedROIBtn.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)
        self.ui.SetPenDrawModeBtn.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)
        self.ui.SetPenEraseModeBtn.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)
        
        self.ui.SetPenCircleShapeBtn.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)
        self.ui.SetPenSquareShapeBtn.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)
        self.ui.SetPenThresholdMaskDisabledBtn.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)
        self.ui.SetPenThresholdMaskEnabledBtn.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)
        self.ui.PenSliceThickness.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)
        self.ui.PenSize.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)
        self.ui.SetPenThresholdLowerThresholdMask.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)
        self.ui.SetPenThresholdUpperThresholdMask.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)
        self.ui.eraseOutofRangeChk.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)
        self.ui.RemberROIPenSettings.setChecked (self._PenSettings.getRememberActiveROIPaintSettings ())
        self.ui.RemberROIPenSettings.clicked.connect (self._toggleRememberROIPenSettings)
        
        self.ui.acceptSliceSegmentationBtn.clicked.connect (self._acceptOneClickSegmentation)
        self.ui.clearSliceSegmentationBtn.clicked.connect (self._clearOneClickSegmentation)
        self.ui.autoAcceptSegmentationChkBox.setChecked (self._PenSettings.getAutoAcceptOneClickSegmentation ())
        self.ui.autoAcceptSegmentationChkBox.clicked.connect (self._autoAcceptOneClickSegmentation)
        
        self.ui.normalizePenSizeToVoxelDimensionsChkBox.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)
        self.ui.normalizePenSizeToVoxelDimensionsChkBox.setChecked (self._PenSettings.getNormalizePenDimensionsToVoxelDimensions ())
        self.ui.SetPenDrawModeBtn.setChecked (self._PenSettings.isAddPen ())
        self.ui.SetPenEraseModeBtn.setChecked (self._PenSettings.isErasePen ())        
        
        self.ui.SetPenCircleShapeBtn.setChecked (self._PenSettings.isCircle ())
        self.ui.SetPenSquareShapeBtn.setChecked (self._PenSettings.isSquare ())
        
        self.ui.SetPenThresholdMaskDisabledBtn.setChecked (not self._PenSettings.isThresholded ())
        self.ui.SetPenThresholdMaskEnabledBtn.setChecked (self._PenSettings.isThresholded ())
        self._toggleThresholdSettingEnabled (self._PenSettings.isThresholded ())
        
        self.ui.PenSliceThickness.setValue (self._PenSettings.getPenSliceThickness ())     
        self.ui.PenSliceThickness.valueChanged.connect (self._setPenSliceThickness)
        
        
        self.ui.PenSize.setValue (self._PenSettings.getPenSize ())
        
        self.ui.normalizePenSizeToVoxelDimensionsChkBox.clicked.connect (self._setNormPensizeToVoxleDim)
        self.ui.CloseBtn.clicked.connect (self._closeWindow)                
        self.ui.SetPenDrawModeBtn.clicked.connect (self._setDrawMode)        
        self.ui.SetPenEraseModeBtn.clicked.connect (self._setEraseMode)        
      
        self.ui.SetPenCircleShapeBtn.clicked.connect (self._setCircleShape)        
        self.ui.SetPenSquareShapeBtn.clicked.connect (self._setSquareShape)        
        self.ui.SetPenThresholdMaskDisabledBtn.clicked.connect (self._setThresholdDisable)        
        self.ui.SetPenThresholdMaskEnabledBtn.clicked.connect (self._setThresholdEnable)        
        
        minV, maxV = self._PenSettings.getThresholdRange ()  
        self.ui.SetPenThresholdLowerThresholdMask.setValue (minV)
        self.ui.SetPenThresholdUpperThresholdMask.setValue (maxV)
        
        self.ui.PenSize.valueChanged.connect (self._setPenSize)
        self.ui.SetPenThresholdUpperThresholdMask.focusOutEvent = self._setUpperThreshold
        self.ui.SetPenThresholdLowerThresholdMask.focusOutEvent = self._setLowerThreshold    
                
        self.setWindowFlags(self.windowFlags () | QtCore.Qt.Dialog| QtCore.Qt.WindowStaysOnTopHint ) 
        self.ui.eraseOutofRangeChk.setChecked (self._PenSettings.getAutoEraseOutofRangeinThresholdDraw ())
        self.ui.eraseOutofRangeChk.clicked.connect (self._setAutoEraseOutOfRange)        
        
        self.ui.AddThresholdPresetBtn.clicked.connect (self._addThresholdPreset)
        self.ui.EditThresholdPresetBtn.clicked.connect (self._editThresholdPreset)
        self.ui.RemoveThresholdPresetBtn.clicked.connect (self._removeThresholdPreset)
        self.ui.SetThresholdToSelectedROIBtn.clicked.connect (self._setToSelectedROI)
        self.ui.ThresholdSelectionPresetComboBox.currentIndexChanged.connect (self._ThresholdPresetChanged)
        
        self.ui.TrainDGonSegmentationBTN.clicked.connect (self._trainOneClickModelForSelectedROI)
        self.ui.SetROIToDefaultDGBTN.clicked.connect (self._setOneClickModelForSelectedROIToDefault)
        
        self.UpdatePaintSettingsOneClickROI ()
    
    def _trainOneClickModelForSelectedROI (self) :
        if self._Parent.ROIDefs.isROISelected () and self._Parent.ROIDefs.isOneROISelected () and self._Parent.NiftiVolume is not None :
            name = self._Parent.ROIDefs.getSelectedROI ()[0]
            if self._Parent.ROIDictionary.isROIDefined (name) :
                obj = self._Parent.ROIDictionary.getROIObject (name)
                if obj.isROIArea () :             
                    if ML_KerasModelLoaderInterface.isMLSupportEnabled () :    
                        if self._Parent.showFlattenDeepGrowDlg ("Training One-Click segmention model may change existing segmentations. Do you wish to flatten the existing one-click segmentations?") :
                            progdialog = RCC_NoCancelProgressDlg(self._Parent)                
                            try :
                                progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)                
                                progdialog.setWindowTitle ("Training %s One-Click Model" % name)
                                progdialog.setWindowModality(QtCore.Qt.ApplicationModal)                  
                                progdialog.setLabelText ("Loading model")
                                progdialog.setRange (0,100)
                                #progdialog.setModal (True)
                                progdialog.setValue (0)
                                progdialog.show ()
                                self._Parent._ProjectDataset.getApp ().processEvents ()
                                self._Parent.ROIDictionary.trainDeepGrowAnnotationModel (name, self._Parent.NiftiVolume, self._Parent._ProjectDataset, ProgressDialog = progdialog)
                            finally :
                                progdialog.closeWindow ()
                            
                           
                            
    def _setOneClickModelForSelectedROIToDefault (self) :
        if self._Parent.ROIDefs.isROISelected () and self._Parent.ROIDefs.isOneROISelected () and self._Parent.NiftiVolume is not None :
            name = self._Parent.ROIDefs.getSelectedROI ()[0]
            if self._Parent.ROIDictionary.isROIDefined (name) :
                obj = self._Parent.ROIDictionary.getROIObject (name)
                if obj.isROIArea () :             
                    if ML_KerasModelLoaderInterface.isMLSupportEnabled () :   
                        if self._Parent.showFlattenDeepGrowDlg ("Restoring the one-click segmention model to the default settings may change existing segmentations. Do you wish to flatten the existing one-click segmentations?") :                                         
                            roiuid = self._Parent.ROIDefs.getROINameUID (name)                
                            roiInterface = self._Parent._ProjectDataset.getDeepGrowROIModelInterface ()
                            roiInterface.deleteROIModel(roiuid)
                            self.ui.SetROIToDefaultDGBTN.setEnabled (False)
    

    def _acceptOneClickSegmentation (self) :
        SaveUndoPoint = True
        CallDictionaryChangeListener = False
        for roiName in self._Parent.ROIDefs.getSelectedROI () :
            if self._Parent.ROIDictionary.hasDeepGrowAnnotations (roiName) :
                if self._Parent.ROIDictionary.flattenDeepGrowAnnotations (roiName, self._Parent.NiftiVolume, self._Parent._ProjectDataset, SaveUndoPoint = SaveUndoPoint, CallDictionaryChangeListener = CallDictionaryChangeListener) :
                    SaveUndoPoint = False
        if not SaveUndoPoint :
           self._Parent.ROIDictionary._CallDictionaryChangedListner ()
        self.ui.acceptSliceSegmentationBtn.setEnabled (False)
        self.ui.clearSliceSegmentationBtn.setEnabled (False)
        self._Parent.update ()  
    
    def _clearOneClickSegmentation (self) :
        SaveUndoPoint = True
        CallDictionaryChangeListener = False
        for roiName in self._Parent.ROIDefs.getSelectedROI () :
            if self._Parent.ROIDictionary.hasDeepGrowAnnotations (roiName) :
                if self._Parent.ROIDictionary.clearDeepGrowAnnotations (roiName, SaveUndoPoint = SaveUndoPoint, CallDictionaryChangeListener = CallDictionaryChangeListener) :
                    SaveUndoPoint = False
        if not SaveUndoPoint :
            self._Parent.ROIDictionary._CallDictionaryChangedListner ()
        self.ui.acceptSliceSegmentationBtn.setEnabled (False)
        self.ui.clearSliceSegmentationBtn.setEnabled (False)
        self._Parent.update ()  
    
    def _autoAcceptOneClickSegmentation (self) :
        val = self.ui.autoAcceptSegmentationChkBox.isChecked ()
        self._PenSettings.setAutoAcceptOneClickSegmentation (val, self._Parent.ROIDefs)
        
     
      
    def _addThresholdPreset (self) :
        autoErase = self._PenSettings.getAutoEraseOutofRangeinThresholdDraw ()
        minV, maxV = self._PenSettings.getThresholdRange ()
        name = "Untitled"
        dlg = PenThresholdPresetEditorDlg (name, minV, maxV, autoErase, self)
        if (dlg.exec_ () == QDialog.Accepted) :
            name, minV, maxV, autoErase = dlg.getResults ()
            uid = self._PenSettings.createThresholdPreset (name, minV, maxV, autoErase)
            self._updateThresholdPresetCombobox ()
            self._setThresholdPresetComboboxSelection (uid)
            
            self._setUpperThresholdFocuseOutEventEnabled = False
            self.ui.SetPenThresholdUpperThresholdMask.setValue (maxV)
            self._setUpperThresholdFocuseOutEventEnabled = True            
            self.ui.SetPenThresholdLowerThresholdMask.setValue (minV)
            self._PenSettings.setThresholdRange (minV,maxV)
            self.ui.eraseOutofRangeChk.setChecked (autoErase)
            self._ThresholdsLastSetFromPreset = True
            
    def _removeThresholdPreset (self) :
        uid = self.ui.ThresholdSelectionPresetComboBox.currentData ()
        if uid is not None :
            self._PenSettings.deleteThreshold (uid)
            self._updateThresholdPresetCombobox ()
    
    def _editThresholdPreset (self) :
        uid = self.ui.ThresholdSelectionPresetComboBox.currentData ()
        if uid is not None :
            preset = self._PenSettings.getSelectedThresholdPreset ()        
            autoErase = preset.getEraseOutofRange ()
            minV, maxV = preset.getMinThreshold (), preset.getMaxThreshold ()
            oldname = preset.getPresetName ()                   
            dlg = PenThresholdPresetEditorDlg (oldname, minV, maxV, autoErase, self)
            if (dlg.exec_ () == QDialog.Accepted) :
                name, minV, maxV, autoErase = dlg.getResults ()
                self._PenSettings.setSelectedThresholdPresetValues (name, minV, maxV, autoErase)                        
                self._setUpperThresholdFocuseOutEventEnabled = False
                self.ui.SetPenThresholdUpperThresholdMask.setValue (maxV)
                self._setUpperThresholdFocuseOutEventEnabled = True
                self.ui.SetPenThresholdLowerThresholdMask.setValue (minV)
                self._PenSettings.setThresholdRange (minV,maxV)
                self.ui.eraseOutofRangeChk.setChecked (autoErase)
                self._ThresholdsLastSetFromPreset = True 
                if oldname != name :
                    self._updateThresholdPresetCombobox ()
                    
    def _setToSelectedROI (self) :
        if self._Parent.ROIDefs.isROISelected () and self._Parent.ROIDefs.isOneROISelected () and self._Parent.NiftiVolume is not None :
            name = self._Parent.ROIDefs.getSelectedROI ()[0]
            if self._Parent.ROIDictionary.isROIDefined (name) :
                obj = self._Parent.ROIDictionary.getROIObject (name)
                if obj.isROIArea () :                         
                    sliceMap = None
                    if self._Parent.ROIDictionary.isROIinAreaMode () : 
                        if self._Parent.ui.XYSliceView.isSelected () :                            
                            sliceView = self._Parent.ui.XYSliceView
                        elif self._Parent.ui.XZSliceView.isSelected () :
                            sliceView = self._Parent.ui.XZSliceView
                        elif self._Parent.ui.YZSliceView.isSelected () :                            
                            sliceView = self._Parent.ui.YZSliceView
                        selectedSlice  = sliceView.getSliceNumber ()
                        axis = sliceView.getSliceAxis ()                        
                        if axis in ["Z","X","Y"] : 
                            sliceData = self._Parent.NiftiVolume.getImageData (selectedSlice, Axis=axis, Coordinate = self._Parent.SelectedCoordinate, AxisProjectionControls = sliceView.getAxisProjectionControls ())
                            sliceMap = obj.getSliceViewObjectMask  (sliceView, selectedSlice, ClipOverlayingROI = self._Parent.ROIDefs.hasSingleROIPerVoxel())                         
                            
                    if sliceMap is None :
                        selectedSlice = self._Parent.SelectedCoordinate.getZCoordinate()
                        if obj.hasSlice (selectedSlice) :
                            sliceData = self._Parent.NiftiVolume.getImageData (Z = selectedSlice, Axis="Z", Coordinate = self._Parent.SelectedCoordinate, AxisProjectionControls = self._Parent.ui.XYSliceView.getAxisProjectionControls ())
                            sliceMap = obj.getSliceViewObjectMask  (sliceView, selectedSlice, ClipOverlayedObjects = self._Parent.ROIDefs.hasSingleROIPerVoxel()) 
                            
                    if sliceMap is not None and np.any (sliceMap) :
                        voxels = sliceData[sliceMap > 0]
                        mean = np.mean (voxels)
                        std = np.std (voxels)
                        offset = max (1.5 * std, 1)
                        minV = np.ceil (mean - offset)
                        maxV = np.ceil (mean + offset)
                        self._setUpperThresholdFocuseOutEventEnabled = False
                        self.ui.SetPenThresholdUpperThresholdMask.setValue (maxV)
                        self._setUpperThresholdFocuseOutEventEnabled = True
                        self.ui.SetPenThresholdLowerThresholdMask.setValue (minV)                            
                        self._PenSettings.setThresholdRange (minV,maxV)
                    else:
                        MessageBoxUtil.showMessage ("Segmentation Not Found", "ROI segmentation not defined in axis.")
                                
    def _ThresholdPresetChanged (self, preset) :
        selectionUid = self.ui.ThresholdSelectionPresetComboBox.currentData ()
        self._PenSettings.setSelectedThresholdPresetUID (selectionUid)
        self._setEditRemovedEnabled ()        
        if selectionUid is not None :
            preset = self._PenSettings.getSelectedThresholdPreset()
            lowerThreshold = preset.getMinThreshold ()
            upperThreshold = preset.getMaxThreshold ()            
            self._setUpperThresholdFocuseOutEventEnabled = False
            self.ui.SetPenThresholdUpperThresholdMask.setValue (upperThreshold)
            self._setUpperThresholdFocuseOutEventEnabled = True
            self.ui.SetPenThresholdLowerThresholdMask.setValue (lowerThreshold)
            self._PenSettings.setThresholdRange (lowerThreshold,upperThreshold)
            self.ui.eraseOutofRangeChk.setChecked (preset.getEraseOutofRange ())
            self._toggleThresholdSettingEnabled (True)
            self._ThresholdsLastSetFromPreset = True 
            
    def _updateThresholdPresetCombobox (self) :
        changed = False
        self.ui.ThresholdSelectionPresetComboBox.setEnabled (False)
        presetList  = self._PenSettings.getPresetList ()
        if len (presetList) + 1 != self.ui.ThresholdSelectionPresetComboBox.count () :
            changed = True 
        else:
            for index, item in enumerate (presetList) :
                if item[0] != self.ui.ThresholdSelectionPresetComboBox.itemData (index + 1) :
                    changed = True 
                    break                
        if (changed) :            
            try :
                self.ui.ThresholdSelectionPresetComboBox.currentIndexChanged.disconnect (self._ThresholdPresetChanged)
                ThresholdPresetChangeListnerDisconnected = True
            except:
                ThresholdPresetChangeListnerDisconnected = False
            selectionUid = self.ui.ThresholdSelectionPresetComboBox.currentData ()        
            self.ui.ThresholdSelectionPresetComboBox.clear ()
            self.ui.ThresholdSelectionPresetComboBox.addItem("None", None)                            
            for index, preset in enumerate (presetList) :
                uid,name = preset                
                self.ui.ThresholdSelectionPresetComboBox.addItem(name, uid)
            self._setThresholdPresetComboboxSelection (selectionUid)                 
            if ThresholdPresetChangeListnerDisconnected :
                self.ui.ThresholdSelectionPresetComboBox.currentIndexChanged.connect (self._ThresholdPresetChanged)        
        self._setEditRemovedEnabled ()        
        self._setThresholdPresetComboBoxEnabled ()
        
    
    def _setThresholdPresetComboboxSelection (self,uid) :
            try :
                self.ui.ThresholdSelectionPresetComboBox.currentIndexChanged.disconnect (self._ThresholdPresetChanged)
                ThresholdPresetChangeListnerDisconnected = True
            except:
                ThresholdPresetChangeListnerDisconnected = False
            if uid is None :
                self.ui.ThresholdSelectionPresetComboBox.setCurrentIndex (0) 
            else:
                valueSet = False
                for index in range (self.ui.ThresholdSelectionPresetComboBox.count ()) :
                    if uid == self.ui.ThresholdSelectionPresetComboBox.itemData (index) :
                        self.ui.ThresholdSelectionPresetComboBox.setCurrentIndex (index) 
                        valueSet = True
                        break
                if not valueSet :
                    self.ui.ThresholdSelectionPresetComboBox.setCurrentIndex (0)                 
            self._PenSettings.setSelectedThresholdPresetUID (self.ui.ThresholdSelectionPresetComboBox.currentData ())        
            self._setEditRemovedEnabled ()
            if ThresholdPresetChangeListnerDisconnected :
                self.ui.ThresholdSelectionPresetComboBox.currentIndexChanged.connect (self._ThresholdPresetChanged)
        
    def _toggleRememberROIPenSettings (self) :
        rememberSettingsChecked = self.ui.RemberROIPenSettings.isChecked ()
        if self._PenSettings.setRememberActiveROIPaintSettings (rememberSettingsChecked) :
            if rememberSettingsChecked :
                if self._Parent.ROIDefs.isROISelected () and self._Parent.ROIDefs.isOneROISelected () and self._Parent.ROIDictionary.isROIinAreaMode () :
                   name = self._Parent.ROIDefs.getSelectedROI ()[0]
                   idNum = self._Parent.ROIDefs.getROINameIDNumber (name)
                   self._Parent.getPenSettings ().setActiveROIID (idNum)
                   self.updateToReflectActiveROI()
        
    def keyPressEvent(self, event):     
        KeyStrokes = ShortcutKeyList.getPassThroughKeyStrokes () 
        if (event.key () in KeyStrokes) : 
            self._Parent.keyPressEvent (event)    

    def updatePenSettingsDrawEraseSetting (self) :
        if (self._PenSettings.isAddPen ()) :
            self.ui.SetPenDrawModeBtn.setChecked (True)
            self.ui.SetPenEraseModeBtn.setChecked (False)
        elif (self._PenSettings.isErasePen ()) :
            self.ui.SetPenDrawModeBtn.setChecked (False)
            self.ui.SetPenEraseModeBtn.setChecked (True)
        elif (self._PenSettings.isDeepGrowPen ()) :
            self.ui.SetPenDrawModeBtn.setChecked (False)
            self.ui.SetPenEraseModeBtn.setChecked (False)
    
    def updateToReflectChangedPenSettings (self) :
        self._updateThresholdPresetCombobox ()
        if self._ThresholdsLastSetFromPreset :
            preset = self._PenSettings.getSelectedThresholdPreset ()        
            autoErase = preset.getEraseOutofRange ()
            minV, maxV = preset.getMinThreshold (), preset.getMaxThreshold ()                                        
            self._setUpperThresholdFocuseOutEventEnabled = False
            self.ui.SetPenThresholdUpperThresholdMask.setValue (maxV)
            self._setUpperThresholdFocuseOutEventEnabled = True
            self.ui.SetPenThresholdLowerThresholdMask.setValue (minV)
            self._PenSettings.setThresholdRange (minV,maxV)
            self.ui.eraseOutofRangeChk.setChecked (autoErase)
    
    def event (self, event) :
        if (event.type() == QtCore.QEvent.WindowActivate) : 
           self.UpdatePaintSettingsOneClickROI ()
        return QDialog.event(self, event)

    
    def UpdatePaintSettingsOneClickROI (self) :
        HasDeepGrowSegmentation = False
        if ML_KerasModelLoaderInterface.isMLSupportEnabled () :
            for roiName in self._Parent.ROIDefs.getSelectedROI () :
                if self._Parent.ROIDictionary.hasDeepGrowAnnotations (roiName) :
                    HasDeepGrowSegmentation = True
                    break
        self.ui.acceptSliceSegmentationBtn.setEnabled (HasDeepGrowSegmentation)
        self.ui.clearSliceSegmentationBtn.setEnabled (HasDeepGrowSegmentation)
        
        self.ui.ROISegGroup.setEnabled (False)
        self.ui.TrainDGonSegmentationBTN.setEnabled (False)
        self.ui.SetROIToDefaultDGBTN.setEnabled (False)
        self.ui.ROISegGroup.setTitle ("ROI segmentation model")
        if self._Parent.ROIDefs.isROISelected () and self._Parent.ROIDefs.isOneROISelected () and self._Parent.NiftiVolume is not None :
            name = self._Parent.ROIDefs.getSelectedROI ()[0]
            self.ui.ROISegGroup.setTitle (name + " segmentation model")
            if self._Parent.ROIDictionary.isROIDefined (name) :
                obj = self._Parent.ROIDictionary.getROIObject (name)
                if obj.isROIArea () :             
                    if ML_KerasModelLoaderInterface.isMLSupportEnabled () :
                        try :
                            hasSliceData = len (obj.getSliceNumberList ()) > 0
                            if not hasSliceData :
                                deepGrowAnnotation = obj.getDeepGrowAnnotation ()
                                if deepGrowAnnotation is not None :
                                    hasSliceData = len (deepGrowAnnotation.getSliceList ()) > 0
                                                                
                            self.ui.ROISegGroup.setEnabled (hasSliceData)
                            self.ui.TrainDGonSegmentationBTN.setEnabled (hasSliceData)
                            roiInterface = self._Parent._ProjectDataset.getDeepGrowROIModelInterface ()
                            roiuid = self._Parent.ROIDefs.getROINameUID (name)     
                            model = roiInterface.getROIModel(roiuid)
                            self.ui.SetROIToDefaultDGBTN.setEnabled (model is not None and not model.isBaselineModel ())
                        except:
                            pass

   
    def updateToReflectActiveROI (self) :
        self._SetToSelectedROIEnabled ()
        self.UpdatePaintSettingsOneClickROI ()
        
        if not self._Parent.ROIDefs.isROISelected () or not self._Parent.ROIDefs.isOneROISelected () :
            self._PenSettings.setActiveROIID (None)            
        else:
            if (self._PenSettings.getRememberActiveROIPaintSettings()) :
                    settings = self._PenSettings.getROIPaintSettings ()
                    if ("SelectedThresholdPreset" in settings) :
                        opt = settings["SelectedThresholdPreset"] 
                        try :
                            self._setThresholdPresetComboboxSelection (opt)
                        except:
                            pass    
                    if ("ThresholdEnabled" in settings) :
                        opt = settings["ThresholdEnabled"] 
                        try :
                            opt = bool (opt)
                            self.ui.SetPenThresholdMaskEnabledBtn.setChecked  (opt)
                            self.ui.SetPenThresholdMaskDisabledBtn.setChecked (not opt)
                        except:
                            pass    
                    if ("EraseOutOfRange" in settings) :
                        try :
                            self.ui.eraseOutofRangeChk.setChecked (bool (settings["EraseOutOfRange"]))
                        except : 
                            pass
                    if "PenOpperation" in settings :
                        if settings["PenOpperation" ] == 1 :                            
                            self.ui.SetPenDrawModeBtn.setChecked (True)        
                            self.ui.SetPenEraseModeBtn.setChecked (False)                                    
                            self._Parent._getPaintToolsDlg ()._UpdateWindowToolSelection ()
                        elif settings["PenOpperation" ] == -1 :
                            self.ui.SetPenDrawModeBtn.setChecked (False)        
                            self.ui.SetPenEraseModeBtn.setChecked (True)                                    
                            self._Parent._getPaintToolsDlg ()._UpdateWindowToolSelection ()
                        elif settings["PenOpperation" ] == 2 :
                            self.ui.SetPenDrawModeBtn.setChecked (False)        
                            self.ui.SetPenEraseModeBtn.setChecked (False)                                    
                            self._Parent._getPaintToolsDlg ()._UpdateWindowToolSelection ()
                    if "PenShape" in settings :
                        if settings["PenShape" ] == "Circle" :
                            self.ui.SetPenCircleShapeBtn.setChecked (True)
                            self.ui.SetPenSquareShapeBtn.setChecked (False)
                        elif settings["PenShape" ] == "Square" :
                            self.ui.SetPenSquareShapeBtn.setChecked (True)
                            self.ui.SetPenCircleShapeBtn.setChecked (False)
                    if "PenSize" in settings :
                        try:
                            penSize = int (settings["PenSize" ])
                            self.ui.PenSize.setValue (penSize)
                        except:
                            pass
                    if "PenThickness" in settings :
                        try:
                            penThickness = int (settings["PenThickness" ])
                            self.ui.PenSliceThickness.setValue (penThickness)
                        except:
                            pass   
                    if "AutoAcceptOneClickSegmentation" in settings :
                        try:
                            autoAcceptSegmentation = bool (settings["AutoAcceptOneClickSegmentation" ])
                            self.ui.autoAcceptSegmentationChkBox.setChecked (autoAcceptSegmentation) 
                        except:
                            pass   
                    if "NormalizePenDimensionsToVoxelDimensions" in settings :
                        try:
                            normVoxelDim = bool (settings["NormalizePenDimensionsToVoxelDimensions" ])
                            self.ui.normalizePenSizeToVoxelDimensionsChkBox.setChecked (normVoxelDim)
                        except:
                            pass
                        
                    if "UpperThreshold" in settings and "LowerThreshold" in settings:
                        try:
                            upperThreshold = int (settings["UpperThreshold" ])
                            lowerThreshold = int (settings["LowerThreshold" ])
                            self._setUpperThresholdFocuseOutEventEnabled = False
                            self.ui.SetPenThresholdUpperThresholdMask.setValue (upperThreshold)
                            self._setUpperThresholdFocuseOutEventEnabled = True
                            self.ui.SetPenThresholdLowerThresholdMask.setValue (lowerThreshold)
                            self._PenSettings.setThresholdRange (lowerThreshold,upperThreshold)                            
                        except:
                            pass                
                    self._toggleThresholdSettingEnabled (self.ui.SetPenThresholdMaskEnabledBtn.isChecked ())
            else:
                self._PenSettings.setActiveROIID (None)
        
    def _setAutoEraseOutOfRange (self):
        val = self.ui.eraseOutofRangeChk.isChecked ()
        self._PenSettings.setAutoEraseOutofRangeThresholdDraw (val)
        self._ThresholdsLastSetFromPreset = False
    
    
        
    def updatePenSettingsDlgThresholdRange (self, NiftVolume):
        if (self._cachedNiftiVolume == NiftVolume):
            return
        self._cachedNiftiVolume = NiftVolume
        if (self._cachedNiftiVolume is not None) :
            
            minVal = -999999#int (np.min (data))
            maxVal = 999999 #int (np.max (data))
            if (minVal != self.ui.SetPenThresholdLowerThresholdMask.minimum ()) or (maxVal != self.ui.SetPenThresholdLowerThresholdMask.maximum ()) :
                self.ui.SetPenThresholdLowerThresholdMask.setMinimum (minVal)
                self.ui.SetPenThresholdLowerThresholdMask.setMaximum (maxVal)
                self.ui.SetPenThresholdUpperThresholdMask.setMinimum (minVal)
                self.ui.SetPenThresholdUpperThresholdMask.setMaximum (maxVal)
                self.ui.SetPenThresholdMaskDisabledBtn.setChecked (True)
                
    def _setPenSliceThickness (self):
        val = int (self.ui.PenSliceThickness.value ())
        self._PenSettings.setPenSliceThickness (val)
        
    def _setPenSize (self):
        val = int (self.ui.PenSize.value ())
        self._PenSettings.setPenSize (val)
    
    def _setUpperThreshold (self, event):
        if not self._setUpperThresholdFocuseOutEventEnabled :
            return
        upper = int (self.ui.SetPenThresholdUpperThresholdMask.value ())        
        lower = int (self.ui.SetPenThresholdLowerThresholdMask.value ())        
        if (lower > upper) :            
            self.ui.SetPenThresholdLowerThresholdMask.setValue (upper)
        self._PenSettings.setThresholdRange (lower, upper)
        self._ThresholdsLastSetFromPreset = False
        
    def _setLowerThreshold (self, event):
        upper = int (self.ui.SetPenThresholdUpperThresholdMask.value ())        
        lower = int (self.ui.SetPenThresholdLowerThresholdMask.value ())        
        if (lower > upper) :
            self.ui.SetPenThresholdUpperThresholdMask.setValue (lower)
        self._PenSettings.setThresholdRange (lower, upper)
        self._ThresholdsLastSetFromPreset = False
        
    def _setThresholdPresetComboBoxEnabled (self) :         
        self.ui.ThresholdSelectionPresetComboBox.setEnabled (self.ui.SetPenThresholdMaskEnabledBtn.isChecked () and self.ui.ThresholdSelectionPresetComboBox.count () > 1)
        
    def _setEditRemovedEnabled (self) :
        if self.ui.SetPenThresholdMaskEnabledBtn.isChecked () :            
            selectionUid = self.ui.ThresholdSelectionPresetComboBox.currentData ()            
            self.ui.EditThresholdPresetBtn.setEnabled (selectionUid is not None)
            self.ui.RemoveThresholdPresetBtn.setEnabled (selectionUid is not None)
            return
        self.ui.EditThresholdPresetBtn.setEnabled (False)
        self.ui.RemoveThresholdPresetBtn.setEnabled (False)
        
    def _SetToSelectedROIEnabled (self) : 
        if self.ui.SetPenThresholdMaskEnabledBtn.isChecked () :
            if self._Parent.ROIDefs.isROISelected () and self._Parent.ROIDefs.isOneROISelected () :
                self.ui.SetThresholdToSelectedROIBtn.setEnabled (True)
                return
        self.ui.SetThresholdToSelectedROIBtn.setEnabled (False)
        
    def _toggleThresholdSettingEnabled (self,setting):
        self.ui.LowerLbl.setEnabled (setting)
        self.ui.UpperLbl.setEnabled (setting)
        self.ui.eraseOutofRangeChk.setEnabled (setting)
        self.ui.SetPenThresholdLowerThresholdMask.setEnabled (setting)
        self.ui.SetPenThresholdUpperThresholdMask.setEnabled (setting)
        self.ui.AddThresholdPresetBtn.setEnabled (setting)
        self.ui.label_3.setEnabled (setting)
        self.ui.line_3.setEnabled (setting)
        self.ui.line.setEnabled (setting)
        self._SetToSelectedROIEnabled () 
        self._setEditRemovedEnabled ()
        self._setThresholdPresetComboBoxEnabled ()
        
    def _setDrawMode (self) :
        if (not self._PenSettings.isAddPen ()) :
           if self._PenSettings.getPenTool () == "Draw" and  self._Parent.getUIState() == "Contour" :
               self._Parent._getPaintToolsDlg ()._paintToolClick ()
           else:
               self._PenSettings.setAddPen ()
    
    def _setDeepGrowMode (self) :
        if (not self._PenSettings.isDeepGrowPen ()) :
           if self._PenSettings.getPenTool () == "Draw" and  self._Parent.getUIState() == "Contour" :
               self._Parent._getPaintToolsDlg ()._paintToolClick ()
               self._PenSettings.setDeepGrowPen ()
           else:
               self._PenSettings.setDeepGrowPen ()
    
    def _setEraseMode (self) :
        if (not self._PenSettings.isErasePen ()) :
            
            if self._PenSettings.getPenTool () == "Draw" and self._Parent.getUIState() == "Contour" :
               self._Parent._getPaintToolsDlg ()._eraseToolClick ()
            else:
                self._PenSettings.setErasePen ()
        
    def _setNormPensizeToVoxleDim (self) :
        self._PenSettings.setNormalizePenDimensionsToVoxelDimensions (self.ui.normalizePenSizeToVoxelDimensionsChkBox.isChecked ())
        
    def _setCircleShape (self) :
        self._PenSettings.setCirclePen ()
        
    def _setSquareShape (self) :
        self._PenSettings.setSquarePen ()    
    
    def _setThresholdDisable (self) :
        self._PenSettings.setThresholdSet (False)
        self._toggleThresholdSettingEnabled (self._PenSettings.isThresholded ())
        
    def _setThresholdEnable (self) :
        self._PenSettings.setThresholdSet (True)
        self._toggleThresholdSettingEnabled (self._PenSettings.isThresholded ())
        
    def _closeWindow (self) :        
        self.close ()
    
    
                

class RCC_MLDatasetDescriptionDlg (QDialog) :
    
    def __init__ (self, parent) :
        QDialog.__init__ (self, parent)
        #self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)        
        self._Parent = parent
        self.ui = Ui_RCC_MLDescriptionDlg ()        
        self.ui.setupUi (self)  
        self.ui.closeBtn.clicked.connect (self.closeWindow)        
        self.ui.setDescriptionBtn.clicked.connect (self.setDescription)                
        self.intWindowSettings (None)         
        self.setWindowFlags(self.windowFlags () | QtCore.Qt.Dialog | QtCore.Qt.WindowStaysOnTopHint)        
   
    def intWindowSettings (self, cWindow) :
        self._cWindow = cWindow
        if (self._cWindow != None  and self._cWindow.NiftiVolume != None and self._cWindow.ROIDictionary != None) :
            tags = self._cWindow.ROIDictionary.getDataFileTagManager ()
            if (tags.hasInternalTag ("ML_DatasetDescription")) :
                tagstring = tags.getInternalTag ("ML_DatasetDescription")                                                        
                self.ui.ScanDescriptionLbl.setText (tagstring)                 
                self.ui.setDescriptionBtn.setEnabled (True)
                self.ui.ScanDescriptionLbl.setEnabled (True)
                return
        self.ui.ScanDescriptionLbl.setText ("Undefined")                 
        self.ui.ScanDescriptionLbl.setEnabled (False)
        self.ui.setDescriptionBtn.setEnabled (False)
        
    def closeWindow (self) :
        if (self._cWindow != None) :            
            self._cWindow._MLDatasetDescriptionDlg = None
        self.close ()
    
    def keyPressEvent(self, event):
        KeyStrokes = ShortcutKeyList.getPassThroughKeyStrokes () 
        if (event.key () in KeyStrokes) : 
            self._Parent.keyPressEvent (event)    
            
    def setDescription (self) :
        if (self._cWindow != None  and self._cWindow.NiftiVolume != None and self._cWindow.ROIDictionary != None) :
            tags = self._cWindow.ROIDictionary.getDataFileTagManager ()
            if (tags.hasInternalTag ("ML_DatasetDescription")) :
                tagstring = tags.getInternalTag ("ML_DatasetDescription")                                                        
                self._cWindow.ROIDictionary.setScanPhase (tagstring)
        
class RCC_RoiTranslationDlg (QDialog) :
    
    def __init__ (self, parent) :
        QDialog.__init__ (self, parent)
        #self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)        
        self._Parent = parent
        self.ui = Ui_rcc_ROITranslationDlg ()        
        self.ui.setupUi (self)  
        self.ui.closeButton.clicked.connect (self.closeWindow)        
        self._uiInit = False    
        self.ui.horizontalScrollBar.setMinimum (0)
        self.ui.verticalScrollBar.setMinimum (0)
        self.ui.sliceScrollBar.setMinimum (0)
        
        self.ui.horizontalScrollBar.sliderReleased.connect (self._updateDictionary)
        self.ui.verticalScrollBar.sliderReleased.connect(self._updateDictionary)
        self.ui.sliceScrollBar.sliderReleased.connect(self._updateDictionary)
        
        self.ui.horizontalScrollBar.sliderPressed.connect (self._saveUndoState)
        self.ui.verticalScrollBar.sliderPressed.connect(self._saveUndoState)
        self.ui.sliceScrollBar.sliderPressed.connect(self._saveUndoState)
        self._cWindow = parent
        self._initScrollbarUI ()
        self.setWindowFlags(self.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)
    
    def keyPressEvent(self, event):
        KeyStrokes = ShortcutKeyList.getPassThroughKeyStrokes ()  
        if (event.key () in KeyStrokes) : 
            self._Parent.keyPressEvent (event)    
            
    def closeWindow (self) :
        if (self._cWindow != None) :
            self._cWindow._TranslationDlg = None
        self.close ()        
    
    def updateScrollbarUI (self) :
        self._initScrollbarUI ()
        
    def _updateDictionary (self) :
        self._cWindow.ROIDictionary._CallDictionaryChangedListner ()
        
    def _saveUndoState (self) :
        self._cWindow.ROIDictionary._SaveUndoPoint ()    
        
    def _initScrollbarUI (self) :
        self.setWindowTitle ("ROI translation")
        if (self._uiInit) :
            self.ui.horizontalScrollBar.valueChanged.disconnect (self._horizontalChanged)
            self.ui.verticalScrollBar.valueChanged.disconnect (self._verticalChanged)
            self.ui.sliceScrollBar.valueChanged.disconnect (self._sliceChanged)
        
        init = False
        if (self._cWindow.ROIDefs.isROISelected () and self._cWindow.NiftiVolume is not None and self._cWindow.ROIDictionary is not None): # if arrow selection tool is selected, test if object is selected                                                                           
            maxPtX, maxPtY, maxPtZ = None, None, None      
            minPtX, minPtY, minPtZ = None, None, None      
            roiList =self._cWindow.ROIDefs.getSelectedROI ()
            if (len (roiList) > 0) :
                roistringlst = ", ".join (roiList)
                self.setWindowTitle ("ROI translation (" + roistringlst +")")            
            for roiName in roiList :
                 if (self._cWindow.ROIDictionary.isROIDefined (roiName) and not self._cWindow.ROIDefs.isROIHidden (roiName) and not self._cWindow.ROIDefs.isROILocked (roiName)) :  
                     obj = self._cWindow.ROIDictionary.getROIObject (roiName)
                     #selectedContourList = self.ROIDictionary.getROISelectedContours (roiName)                     
                     if (self._cWindow.ROIDefs.isROIArea (roiName)) :   
                        #if (self._cWindow.ROIDictionary.isROIinAreaMode ()) :
                        #    continue 
                        xbox, ybox, zbox = obj.getBoundingBox ()                        
                        if (minPtX == None) :
                            minPtX = xbox[0]
                            minPtY = ybox[0]
                            minPtZ = zbox[0]                            
                            maxPtX = xbox[1]
                            maxPtY = ybox[1]
                            maxPtZ = zbox[1]
                        else:
                            minPtX = min (minPtX, xbox[0])                            
                            maxPtX = max (maxPtX, xbox[1])                            
                            minPtY = min (minPtY, ybox[0])                            
                            maxPtY = max (maxPtY, ybox[1])                            
                            minPtZ = min (minPtZ, zbox[0])                            
                            maxPtZ = max (maxPtZ, zbox[1])                            
                     elif (self._cWindow.ROIDefs.isROIPoint (roiName))  :
                         pointIDLst = self._cWindow.ROIDictionary.getROISelectedContours (roiName)                                                        
                         if (len (pointIDLst) == 0) :
                             pointIDLst = obj.getContourIDManger ().getAllocatedIDList()
                         if (len (pointIDLst) > 0) :
                             if (minPtX == None) :
                                 maxPtX, maxPtY, maxPtZ = obj.getPointCoordinate(pointIDLst[0]).getCoordinate ()                             
                                 minPtX, minPtY, minPtZ = obj.getPointCoordinate(pointIDLst[0]).getCoordinate ()                             
                             for pid in pointIDLst :
                                 x, y, z = obj.getPointCoordinate(pid).getCoordinate ()                                                              
                                 minPtX = min (x, minPtX)
                                 maxPtX = max (x, maxPtX)
                                 minPtY = min (y, minPtY)
                                 maxPtY = max (y, maxPtY)
                                 minPtZ = min (z, minPtZ)
                                 maxPtZ = max (z, maxPtZ)                                 
            
            if (minPtX != None) :                            
                 volumeDim = self._cWindow.NiftiVolume.getSliceDimensions ()
                 xmax = volumeDim[0] - (maxPtX + 1) + minPtX
                 if (xmax < 0) :
                     xmax = 0                              
                 ymax = volumeDim[1] - (maxPtY  + 1) + minPtY                           
                 if (ymax < 0) :
                     ymax = 0
                 slicemax = volumeDim[2] - (maxPtZ) + minPtZ
                 if (slicemax < 0) :
                    slicemax = 0

                            
                 self.ui.horizontalScrollBar.setMaximum (xmax)
                 self._oldHorizontalPos = minPtX
                 self.ui.horizontalScrollBar.setValue (self._oldHorizontalPos)
                 self.ui.horizontalScrollBar.update ()                           
                 self.ui.verticalScrollBar.setMaximum (ymax)
                 self._oldVerticalPos = minPtY
                 self.ui.verticalScrollBar.setValue (self._oldVerticalPos)
                 self.ui.verticalScrollBar.update ()                   
                 self.ui.sliceScrollBar.setMaximum (slicemax)
                 self._oldSlicePos = minPtZ
                 self.ui.sliceScrollBar.setValue (self._oldSlicePos)
                 self.ui.sliceScrollBar.update ()           
                 self.ui.horizontalScrollBar.setEnabled (True)
                 self.ui.verticalScrollBar.setEnabled (True)
                 self.ui.sliceScrollBar.setEnabled (True)
                 self.ui.label.setEnabled (True)
                 self.ui.label_2.setEnabled (True)
                 self.ui.label_3.setEnabled (True)
                 self.update()
                 #print ("Scrollbar ui updated")
                 init = True
                
        if (not init) :
            self.ui.horizontalScrollBar.setEnabled (False)
            self.ui.verticalScrollBar.setEnabled (False)
            self.ui.sliceScrollBar.setEnabled (False)
            self.ui.label.setEnabled (False)
            self.ui.label_2.setEnabled (False)
            self.ui.label_3.setEnabled (False)
        
        self.ui.horizontalScrollBar.valueChanged.connect (self._horizontalChanged)
        self.ui.verticalScrollBar.valueChanged.connect (self._verticalChanged)
        self.ui.sliceScrollBar.valueChanged.connect (self._sliceChanged)
        self._uiInit = True
    
    def _setSelectObjectPos (self, dx, dy, dz) :
        if (self._cWindow.ROIDefs.isROISelected () and self._cWindow.NiftiVolume != None ): # if arrow selection tool is selected, test if object is selected                         
             roiList =self._cWindow.ROIDefs.getSelectedROI ()
             volumeDim = self._cWindow.NiftiVolume.getSliceDimensions ()
             for roiName in roiList :
                 if (self._cWindow.ROIDictionary.isROIDefined (roiName) and not self._cWindow.ROIDefs.isROIHidden (roiName) and not self._cWindow.ROIDefs.isROILocked (roiName)) :  
                     obj = self._cWindow.ROIDictionary.getROIObject (roiName)                     
                     x, y, z = self._cWindow.SelectedCoordinate.getCoordinate ()
                     if (self._cWindow.ROIDefs.isROIArea (roiName)) :                                             
                         #if (self._cWindow.ROIDictionary.isROIinAreaMode ()) :
                         #    continue 
                         selectedContourList = self._cWindow.ROIDictionary.getROISelectedContours (roiName)
                         if (len (selectedContourList) == 0) :
                             selectedContourList = obj.getContourIDManger ().getAllocatedIDList()
                         dx, dy, dz, _ = obj.moveContours (selectedContourList, dx, dy, dz, volumeDim, None, SliceView = self._cWindow.ui.XYSliceView, ROIDictionary = self._cWindow.ROIDictionary, BaseCoordinate = (x, y, z))
                     elif (self._cWindow.ROIDefs.isROIPoint (roiName))  :
                         pointIDLst  = self._cWindow.ROIDictionary.getROISelectedContours (roiName)                    
                         if (len (pointIDLst) == 0) :
                             pointIDLst = obj.getContourIDManger ().getAllocatedIDList()
                         dx, dy, dz = obj.movePoint (pointIDLst, dx, dy, dz, volumeDim)                                                    
                     x += dx
                     y += dy
                     z += dz
                     if (x < 0) :
                         x = 0
                     if (y < 0) :
                         y = 0
                     if (z < 0) :
                         z = 0
                     volumeDim = self._cWindow.NiftiVolume.getSliceDimensions ()
                     if (x >= volumeDim[0]):
                         x = volumeDim[0] - 1
                     if (y >= volumeDim[1]):
                         y = volumeDim[1] - 1
                     if (z >= volumeDim[2]):
                         z = volumeDim[2] - 1
                     self._cWindow.SelectedCoordinate.setCoordinate ((x, y, z))
            
    def _horizontalChanged (self) :
        if self._cWindow.showFlattenDeepGrowDlg ("One-click generated segmentations must be flattened before ROI can be moved. Do you wish to flatten the segmentation?") :
            newPos = self.ui.horizontalScrollBar.value ()
            dX = int (newPos - self._oldHorizontalPos)        
            self._oldHorizontalPos = newPos        
            self._setSelectObjectPos (dX, 0, 0)
        else:
            try :
                self.ui.horizontalScrollBar.valueChanged.disconnect (self._horizontalChanged)
                disconnected = True
            except:
                disconnected = False
            self.ui.horizontalScrollBar.setValue (self._oldHorizontalPos)
            if disconnected :
               self.ui.horizontalScrollBar.valueChanged.connect (self._horizontalChanged)
       
               
        
    def _verticalChanged (self) :
        if self._cWindow.showFlattenDeepGrowDlg ("One-click generated segmentations must be flattened before ROI can be moved. Do you wish to flatten the segmentation?") :
            newPos = self.ui.verticalScrollBar.value ()
            dY = int (newPos - self._oldVerticalPos)
            self._oldVerticalPos = newPos
            self._setSelectObjectPos (0, dY, 0)
        else:
            try :
                self.ui.verticalScrollBar.valueChanged.disconnect (self._verticalChanged)
                disconnected = True
            except:
                disconnected = False
            self.ui.verticalScrollBar.setValue (self._oldVerticalPos)
            if disconnected :
                self.ui.verticalScrollBar.valueChanged.connect (self._verticalChanged)
                
    def _sliceChanged (self) :
        if self._cWindow.showFlattenDeepGrowDlg ("One-click generated segmentations must be flattened before ROI can be moved. Do you wish to flatten the segmentation?") :
            newPos = self.ui.sliceScrollBar.value ()
            dZ = int (newPos - self._oldSlicePos)
            self._oldSlicePos = newPos
            self._setSelectObjectPos (0, 0, dZ)
        else:
            try :
                self.ui.sliceScrollBar.valueChanged.disconnect (self._sliceChanged)
                disconnected = True
            except:
                disconnected = False
            self.ui.sliceScrollBar.setValue (self._oldSlicePos)
            if disconnected :
                self.ui.sliceScrollBar.valueChanged.connect (self._sliceChanged)
        
class RCC_RegistrationLogViewerDlg (QDialog) :
    
    def __init__ (self, parent, log) :
        QDialog.__init__ (self, parent)
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)
        self._Parent = parent
        self.ReturnValue = None        
        self.ui = Ui_rcc_registrationLogViewerDlg ()        
        self.ui.setupUi (self)  
        self._resizeWidgetHelper = ResizeWidgetHelper ()
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.RegistrationLogTxt)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.CloseBtn)
        self.ui.RegistrationLogTxt.setText (log)
        self.ui.CloseBtn.clicked.connect (self.close)
    
    def keyPressEvent(self, event):
        KeyStrokes = ShortcutKeyList.getPassThroughKeyStrokes () 
        if (event.key () in KeyStrokes) : 
            self._Parent.keyPressEvent (event) 
            
    def _internalWindowResize (self, newsize):                 
        try:          
            if (self._resizeWidgetHelper is not None) :                                               
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.RegistrationLogTxt, newsize)            
                self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.RegistrationLogTxt, newsize)                        
                self._resizeWidgetHelper.setWidgetYPos  (self.ui.CloseBtn, newsize.height () - 30)            
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.CloseBtn, newsize.width () - 110)                   
        except:
            pass        
            
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)                 
        self._internalWindowResize ( qresizeEvent.size () )   
        
class RCC_ROIEditorDlg (QDialog) :
    
    def __init__ (self, parent, DefinedROINameLst, initROIDef = None, lockROIType = False, lockROIMask = False, ProjectDataset = None) :
        QDialog.__init__ (self, None)
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)
        self.ReturnValue = None        
        self.ui = Ui_RCC_ROIEditor ()        
        self.ui.setupUi (self)             
        self._resizeWidgetHelper = ResizeWidgetHelper ()
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ROISplitter)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.radLexIDEdit)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ROI_Name)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.roiMaskValueTxt)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.RadLexFilter)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.OkBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.CancelBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.line)
        self.ui.RadlexFrame.resizeEvent = self._RadlexFrameResize
        self.ui.ROIFrame.resizeEvent = self._ROIFrameResize
        
        self._ROIIdNumber = -1
        self._RadLexID = "unknown"
        self._ROIName = ""
        self._Type = "Area"
        self._Color = QtGui.QColor (0,0,0)
        self._MaskValue = 1
        self.ui.lockROIBtn.setChecked (False)
        self.ui.hideROIBtn.setChecked (False)
        self._DefinedROINameList = copy.copy (DefinedROINameLst)
        self._defInitalizedFromExistingROI = False
        self._getDefinedUIDObj = None
        self._initalizedType = None
        if (initROIDef is not None) :
            self._ROIIdNumber = initROIDef.getIDNumber ()
            self._defInitalizedFromExistingROI = True
            self._ROIName = initROIDef.getName ()
            if (self._ROIName in self._DefinedROINameList) :
                self._DefinedROINameList.remove (self._ROIName)            
            self._Type = initROIDef.getType ()
            self._initalizedType = self._Type
            color = initROIDef.getColor ()
            self._Color = QtGui.QColor (color[0], color[1], color[2])            
            self._MaskValue = initROIDef.getMask ()
            self.ui.lockROIBtn.setChecked (initROIDef.isLocked ())
            self.ui.hideROIBtn.setChecked (initROIDef.isHidden ())
            radlexID = initROIDef.getRadlexID ()
            self._RadLexID = radlexID + "_inducechange"
            if (radlexID != "unknown") :
                self.ui.radLexIDEdit.setText (radlexID)
            else:
                self.ui.radLexIDEdit.setText ("")       
            self._getDefinedUIDObj =  initROIDef.getDefinedUID ()
        pal = QtGui.QPalette ()                
        pal.setColor (QtGui.QPalette.Background, self._Color )        
        self.ui.ColorWidget.setAutoFillBackground (True)
        self.ui.ColorWidget.setPalette(pal);
        
        if (self._Type == "Point") :
            self.ui.ROI_Point.setChecked (True) 
            self.ui.ROI_Area.setChecked (False) 
        elif (self._Type == "Area"):
            self.ui.ROI_Point.setChecked (False) 
            self.ui.ROI_Area.setChecked (True) 
        else:
            print ("Error Recognized Type")
        
        self.ui.roiMaskValueTxt.setText (str (self._MaskValue))
        self.ui.roiMaskValueTxt.textChanged.connect (self.MaskChanged)
        self.ui.roiMaskValueLbl.setEnabled (not lockROIMask)
        self.ui.roiMaskValueTxt.setEnabled (not lockROIMask)
                
        
        #if type changing is not allowed
        self.ui.RoiTypeLabel.setEnabled (not lockROIType)
        self.ui.ROI_Point.setEnabled (not lockROIType)
        self.ui.ROI_Area.setEnabled (not lockROIType)
        
        self.ui.radlexLabel.setEnabled (not lockROIType)
        self.ui.radLexIDEdit.setEnabled (not lockROIType)
        self.ui.radLexIDEdit.textChanged.connect (self.RadlexIDChanged)
        
        self.ui.ROI_Name.setText (self._ROIName)
        self.ui.ROI_Name.textChanged.connect (self.ROINameChanged)
        self.ui.SelectColorBtn.clicked.connect (self.SelectColorBtnClk)                
        self.ui.CancelBtn.clicked.connect (self.CancelBtn)
        self.ui.OkBtn.clicked.connect (self.OkBtn)
        if (not lockROIType) : #if type changing is not allowed            
            self.ui.ROI_Point.clicked.connect (self.ROI_PointClk)
            self.ui.ROI_Area.clicked.connect (self.ROI_AreaClk)    
            
        self.setMinimumSize(self.size ())        
        self.ui.OkBtn.setEnabled (len (self._ROIName) > 0 and self._ROIName not in self._DefinedROINameList)                            
        self.ui.RadLexFilter.editingFinished.connect (self._radlexFilter)
        
        self.ui.RadLexFilter.editingFinished.connect (self._radlexFilter)
        self._radLexTree = RadLexUITree (ProjectDataset, self)
        self._ProjectDataset = ProjectDataset
        self.ui.RadLexTree.setModel (self._radLexTree)        
        treeSelectionModel = self.ui.RadLexTree.selectionModel ()        
        self.RadlexIDChanged ()
        treeSelectionModel.selectionChanged.connect (self._radlexTreeSelectionChanged)            
    
        self.ui.exportTemplateBtn.clicked.connect (self._exportTemplateBtnClick)
        self.ui.importTemplateBtn.clicked.connect (self._importTemplateBtnClick)
        
        self.ui.lockROIBtn.setEnabled (not self.ui.hideROIBtn.isChecked ())
        self.ui.hideROIBtn.clicked.connect (self._hideROICicked)    
    
    def _hideROICicked (self) :
        self.ui.lockROIBtn.setEnabled (not self.ui.hideROIBtn.isChecked ())
        
    def _exportTemplateBtnClick (self) :
        dlg= QFileDialog()
        dlg.setWindowTitle('Save Radlex template' )
        dlg.setViewMode( QFileDialog.Detail )    
        dlg.setFileMode (QFileDialog.AnyFile)
        dlg.setNameFilters( [self.tr('excel files (*.xlsx)'),self.tr('all files (*)')] )
        dlg.setDefaultSuffix( '.xlsx' )
        dlg.setAcceptMode (QFileDialog.AcceptSave)
        if dlg.exec_() and (len (dlg.selectedFiles())== 1):
            try :
                output = dlg.selectedFiles()[0]                                    
                basepath = self._ProjectDataset.getPythonProjectBasePath ()
                rilContourLibPath = os.path.join (basepath, "rilcontourlib")
                radlexPath = os.path.join (rilContourLibPath, "radlex")
                radlexPath = os.path.join (radlexPath, "RilcontourSimplifiedRadlex.xlsx")            
                shutil.copyfile (radlexPath,output)
            except:
                MessageBoxUtil.showMessage ("Radlex Template Export", "A error occured exporting the radlex template.")
            
    def _importTemplateBtnClick (self) :
        dlg= QFileDialog()
        dlg.setWindowTitle('Import Radlex template' )
        dlg.setViewMode( QFileDialog.Detail )    
        dlg.setFileMode (QFileDialog.ExistingFile)
        dlg.setNameFilters( [self.tr('excel files (*.xlsx)'),self.tr('all files (*)')] )
        dlg.setDefaultSuffix( '.xlsx' )
        dlg.setAcceptMode (QFileDialog.AcceptOpen)
        if dlg.exec_() and (len (dlg.selectedFiles())== 1):
            try :
                inputFile = dlg.selectedFiles()[0]                                    
                homepath = self._ProjectDataset.getHomeFilePath ()
                if (not os.path.isdir (homepath) and not os.path.exists (homepath)):
                    os.mkdir (homepath)
                if (os.path.isdir (homepath)) :
                    destPath = os.path.join (homepath,"CustomRadlexTemplate.xlsx")
                    shutil.copyfile (inputFile,destPath)
                    
                    self._radLexTree = RadLexUITree (self._ProjectDataset, self)        
                    self.ui.RadLexTree.setModel (self._radLexTree)   
                    self._radLexTree.setFilter (self.ui.RadLexFilter.text ())
            except:
                MessageBoxUtil.showMessage ("Radlex Template Export", "A error occured exporting the radlex template.")
    @staticmethod
    def _isValid (txt) :
        return (txt is not None) and (len (txt) > 0) and (txt.lower() != "none")
            
    def RadlexIDChanged (self) :
        self.ui.RadlexNameWarrning.setVisible (False)
        radLexID = self.ui.radLexIDEdit.text ().strip ()    
        if (self._RadLexID != radLexID) :
            self._RadLexID = radLexID
            model = self.ui.RadLexTree.model ()
            if model is None :
                self.ui.radlexNameLabel.setText ("Radlex template not loaded.")
            else:
                radlexmodel = model.getOrigionalRadLexTree ()
                if (radlexmodel  is None) :
                    self.ui.radlexNameLabel.setText ("Radlex template not loaded.")
                else:
                    if (radLexID in ["unknown", ""]) :
                        self.ui.radlexNameLabel.setText ("RadlexID not set.")
                    elif (radlexmodel.hasItem (radLexID)) :
                        radlexname = radlexmodel[radLexID]["NameList"][0]
                        self.ui.radlexNameLabel.setText ("Radlex ID name: " + radlexname)
                        if (radlexname.lower().strip () != self._ROIName.lower().strip ()) :
                           self.ui.RadlexNameWarrning.setVisible (True)
                        else:
                           self.ui.RadlexNameWarrning.setVisible (False)
                    else:
                        self.ui.radlexNameLabel.setText ("RadlexID not found in template.")
            
    def _radlexTreeSelectionChanged (self) :        
        item = self.ui.RadLexTree.selectionModel().currentIndex()                 
        radlexID = item.internalPointer ()                               
        treeDictionary = self._radLexTree.getOrigionalRadLexTree ()        
        if treeDictionary is not None :
            try :        
                radEntry = treeDictionary[radlexID]                                    
            except:
                return
        
            try :
                name = radEntry["NameList"][0]
                name = name.strip ()
            except:
                name = "None"
            try :
                roitype = radEntry["ROIDefType"]
                roitype = roitype.strip ()
                roitype = roitype.lower ()
            except:
                roitype = "None"
            try :
                rgb = radEntry["RGB"]
            except:
                rgb = "None"
            try :
                mask = radEntry["Mask"]
                mask = mask.strip ()
                mask = int (mask)
                mask = str(mask)
            except:
                mask = "None"
            
            if not self._defInitalizedFromExistingROI and RCC_ROIEditorDlg._isValid (name) :
                radLexIDStr = str(radlexID).strip ()
                if (self.ui.ROI_Name.isEnabled() and not self.ui.ROI_Name.isReadOnly ()):
                    self.ui.ROI_Name.setText (name)
                if (RCC_ROIEditorDlg._isValid (radLexIDStr) and self.ui.radLexIDEdit.isEnabled () and not self.ui.radLexIDEdit.isReadOnly ()):
                    self.ui.radLexIDEdit.setText (radLexIDStr)
                if (RCC_ROIEditorDlg._isValid (mask) and self.ui.roiMaskValueTxt.isEnabled() and not self.ui.roiMaskValueTxt.isReadOnly ()):
                    self.ui.roiMaskValueTxt.setText (mask)
                if (RCC_ROIEditorDlg._isValid (str(rgb))):
                    try :
                        red, green, blue = rgb
                        color = QtGui.QColor (red, green, blue)
                        self._colorSelected (color)
                    except:
                        pass                                    
                if (RCC_ROIEditorDlg._isValid (str(roitype)) and self.ui.ROI_Area.isEnabled() and  self.ui.ROI_Point.isEnabled ()):
                    self.ui.ROI_Area.setChecked (roitype == "area")
                    self.ui.ROI_Point.setChecked (roitype == "point")      
                    if roitype == "area" :
                       self._Type = "Area"
                    elif roitype == "point" :
                       self._Type = "Point"
            elif self._defInitalizedFromExistingROI and RCC_ROIEditorDlg._isValid (name) :
                radLexIDStr = str(radlexID).strip ()
                if (RCC_ROIEditorDlg._isValid (radLexIDStr) and self.ui.radLexIDEdit.isEnabled () and not self.ui.radLexIDEdit.isReadOnly ()):
                    self.ui.radLexIDEdit.setText (radLexIDStr)
  

    
    def _radlexFilter (self) : 
        #self.ui.RadLexTree.setModel (None)
        self._radLexTree.setFilter (self.ui.RadLexFilter.text ())
        #self.ui.RadLexTree.setModel (self._radLexTree)
    
    def _ROIFrameResize (self, size) :
        try :
            self._resizeWidgetHelper.setWidth (self.ui.ROI_Name, self.ui.ROIFrame.width () - 101)
            self._resizeWidgetHelper.setWidth (self.ui.roiMaskValueTxt, self.ui.ROIFrame.width () - 141)
            self._resizeWidgetHelper.setWidth (self.ui.radLexIDEdit, self.ui.ROIFrame.width () - 101)
            self._resizeWidgetHelper.setWidth (self.ui.RadlexNameWarrning, self.ui.ROIFrame.width () - 151)
            self._resizeWidgetHelper.setWidth (self.ui.radlexNameLabel, self.ui.ROIFrame.width () - 21)
        except :
            pass
        
        
    def _RadlexFrameResize (self, size) :
        try:
            self._resizeWidgetHelper.setWidth (self.ui.RadLexFilter, self.ui.RadlexFrame.width () - 70)
            self._resizeWidgetHelper.setWidth (self.ui.RadLexTree, self.ui.RadlexFrame.width () - 10)
            self._resizeWidgetHelper.setHeight (self.ui.RadLexTree, self.ui.RadlexFrame.height () - 50)
        except:
            pass
        
        
    def _internalWindowResize (self, newsize):                            
        try:
            if (self._resizeWidgetHelper is not None) :                   
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.ROISplitter, newsize)            
                self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.ROISplitter, newsize)                                      
                self._resizeWidgetHelper.setWidgetYPos  (self.ui.line, newsize.height () - 57)                                                
                self._resizeWidgetHelper.setWidth  (self.ui.line, newsize.width () - 18)                                                            
                self._resizeWidgetHelper.setWidgetYPos  (self.ui.OkBtn, newsize.height ()- 37)            
                self._resizeWidgetHelper.setWidgetYPos  (self.ui.CancelBtn, newsize.height () - 37)                        
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.OkBtn, newsize.width () - 189)                        
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.CancelBtn, newsize.width () - 99)                  
                self._resizeWidgetHelper.setWidgetYPos  (self.ui.exportTemplateBtn, newsize.height () - 37)                        
                self._resizeWidgetHelper.setWidgetYPos  (self.ui.importTemplateBtn, newsize.height () - 37)                        
        except:
            pass
            
    
    def MaskChanged (self) :        
        try :
            test = self.ui.roiMaskValueTxt.text ().strip ()
            if (test == "") :
                test = "1"
            num = int (test)
            if (num > 32768) :
                num = 32768
            if (test != str (num)) :
                self.ui.roiMaskValueTxt.setText (str (num))
            else:                
                self._MaskValue = num
        except:
            self.ui.roiMaskValueTxt.setText (str (self._MaskValue))     
            
            
    def ROINameChanged (self) :        
        test = self.ui.ROI_Name.text ().strip ()
        if (self._ROIName != test) :
            self._ROIName = test
            self.ui.OkBtn.setEnabled (len (self._ROIName) > 0 and self._ROIName not in self._DefinedROINameList)    
            
            radLexID = self._RadLexID
            if (radLexID not in ["unknown", ""]) :
                model = self.ui.RadLexTree.model ()
                if model is not None :
                    radlexmodel = model.getOrigionalRadLexTree ()
                    if (radlexmodel  is not None) :
                        if (radlexmodel.hasItem (radLexID)) :
                            radlexname = radlexmodel[radLexID]["NameList"][0]
                            if (radlexname.lower().strip () != self._ROIName.lower().strip ()) :
                               self.ui.RadlexNameWarrning.setVisible (True)
                            else:
                               self.ui.RadlexNameWarrning.setVisible (False)
                      
            
        
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)                 
        self._internalWindowResize ( qresizeEvent.size () )     
                        
    def _colorSelected (self, color):
        self._Color = QtGui.QColor (color)
        pal = QtGui.QPalette ()        
        pal.setColor (QtGui.QPalette.Background, self._Color)        
        self.ui.ColorWidget.setAutoFillBackground (True)
        self.ui.ColorWidget.setPalette(pal);
        self.ui.ColorWidget.update () 
        
    def SelectColorBtnClk (self) :
        try:
            colordlg = QColorDialog (self._Color, self)            
            colordlg.setOption(QColorDialog.DontUseNativeDialog)
            colordlg.setCurrentColor (self._Color)
            colordlg.colorSelected.connect (self._colorSelected)
            colordlg.open()
        except:
            print ("Error occured in the Color Dialog")
        
                           
    def ROI_AreaClk (self) :
        self._Type = "Area"
        self.ui.ROI_Point.setChecked (False)
          
    def ROI_PointClk (self) :
        self._Type = "Point"
        self.ui.ROI_Area.setChecked (False)
    
    def OkBtn (self):
        #self.setModal (False)              
        if (len (self._ROIName) > 0 and self._ROIName not in self._DefinedROINameList) :
            try :
                test = self.ui.roiMaskValueTxt.text ().strip ()
                num = int (test)
                if (num > 32768) :
                    num = 32768
                self._MaskValue = num
            except:
                self._MaskValue = self._MaskValue
            _lockROI = self.ui.lockROIBtn.isChecked ()
            _hideROI = self.ui.hideROIBtn.isChecked ()
            self._RadLexID = self._RadLexID.strip ()            
            color = (self._Color.red (), self._Color.green (), self._Color.blue ())           
            if self._Type != self._initalizedType :
                self._ROIIdNumber = -1
            self.ReturnValue = ROIDef (roiIDNumber = self._ROIIdNumber, name = self._ROIName, roitype = self._Type, color = color, mask = self._MaskValue, Hidden = _hideROI, RadlexID = self._RadLexID, Locked = _lockROI, ROIDefUIDObj = self._getDefinedUIDObj)
            self.accept ()   
        else:
            self.ui.OkBtn.setEnabled  (False)
        #self.accept ()
        
    def CancelBtn (self):   
        #self.setModal (False)            
        self.ReturnValue = None
        self.reject ()

"""
    RCC_VisOptionsDlg
    
    Non-modal dialog to enable user to interactively modify transparency settings for area and point ROI objects
"""    
class RCC_VisOptionsDlg (QDialog) :   
    def __init__ (self, parent, settings) :
        QDialog.__init__ (self, parent)
        self._Parent = parent
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, False)         
        self.ui = Ui_VisSettings ()        
        self.ui.setupUi (self)               
        self.ui.CloseBtn.clicked.connect (self.closeDlg)                
        self.VisSettings = settings                    
        
        value = self.VisSettings.getUITransparency (IgnoreUITransparancyToggle = True)            
        self.ui.transpValueTxt.setText (str (value))
        self.ui.transpSlider.setValue (value)
        undoredostacksize = self.VisSettings.getUndoRedoStackSize ()            
        self.ui.undoredo_value.setText (str (undoredostacksize))
        self.ui.undoredoSlider.setValue (undoredostacksize)
        self.ui.DashedBorderCheckbox.setChecked (settings.getUIDashIndicator ())
        self.ui.BoxIndicatorCheckbox.setChecked (settings.getUIBoxIndicator ())
        self.ui.objectSelectionMove.setChecked (settings.isObjectSelectionMoveDisabled ())
        self.ui.ROISelectionMoveToLastEditedSlice.setChecked (settings.shouldMoveToLastEditedSliceOnROISelection ())
        self.ui.showROIBoundingBoxInEditMode.setChecked (settings.getShowROIBoundingBoxInEditMode ())
        self.ui.ShowAxisOrientationIndicators.setChecked (settings.showSliceOrientation ())        
        self.ui.AutoLockROIOnHideingBtn.setChecked (settings.shouldAutoLockROIWhenHidden ())
        self.ui.performIslandRemovalIn3D.setChecked (settings.shouldPerformIslandRemovalIn3D ())
        
        mouseWheelSliceSelectionDisabled =  settings.isMouseWheelSliceSelectionDisabled () 
        self.ui.DisableMouseWheelScrolling.setChecked (mouseWheelSliceSelectionDisabled)
        self._disableMouseWheelScrolling (mouseWheelSliceSelectionDisabled)
        self.ui.MouseWheelSensitivitySlider.setValue (settings.getMouseWheelSliceSelectionSensitivity ())
        self.ui.DisableMouseWheelScrolling.stateChanged.connect (self._updateSettings)
        self.ui.MouseWheelSensitivitySlider.valueChanged.connect (self._updateSettings)            
        
        self.ui.undoredoSlider.setMaximum (20)        
        self.ui.undoredo_value.editingFinished.connect (self._undoredoValueChanged)        
        self.ui.undoredoSlider.valueChanged.connect (self._undoredoValueSider)                    
        self.ui.transpValueTxt.editingFinished.connect (self._txtValueChanged)        
        self.ui.transpSlider.valueChanged.connect (self._minValueChanged)                        
        self.ui.DashedBorderCheckbox.stateChanged.connect (self._updateSettings)
        self.ui.BoxIndicatorCheckbox.stateChanged.connect (self._updateSettings)
        self.ui.smoothVisualization.stateChanged.connect (self._updateSettings)
        self.ui.showROIBoundingBoxInEditMode.stateChanged.connect (self._updateSettings)
        self.ui.ShowAxisOrientationIndicators.stateChanged.connect (self._updateSettings)
        self.ui.invertMouseWheelBtn.stateChanged.connect (self._updateSettings)
        self.ui.objectSelectionMove.stateChanged.connect (self._updateSettings)        
        self.ui.ROISelectionMoveToLastEditedSlice.stateChanged.connect (self._updateSettings)
        self.ui.AutoLockROIOnHideingBtn.stateChanged.connect (self._updateSettings)
        self.setWindowFlags(self.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)
        self.ui.performIslandRemovalIn3D.stateChanged.connect (self._updateSettings)

        self.ui.ShowFileNameCheckBox.setChecked (settings.showFileNameTitle ())
        self.ui.ShowFileNameCheckBox.stateChanged.connect (self._updateSettings)
        
        self.ui.DisableMouseWheelScrolling.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)
        self.ui.MouseWheelSensitivitySlider.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)
        self.ui.MouseWheelSensitivityLabel.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)
        self.ui.MouseWheelSensitivityLabel_Low.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)
        self.ui.MouseWheelSensitivityLabel_Normal.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)
        self.ui.MouseWheelSensitivityLabel_High.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)
        
        self.ui.transpValueTxt.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)
        self.ui.transpSlider.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)
        self.ui.undoredo_value.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)
        self.ui.undoredoSlider.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)
                
        self.ui.MovetoCoordinateAtEndofPaintCheckBox.setChecked (settings.moveToCoordinateAtPaintEnd ())
        self.ui.trackPaintToolContinuouslyCheckBox.setChecked (settings.moveToCoordinatDuringPaint ())
        self.ui.AutomaticN4BiasFieldCorrectionCheckBox.setChecked (settings.getPerformAutomaticN4BiasCorrection ())
        self.ui.AutomaticN4BiasFieldCorrectionCheckBox.stateChanged.connect (self._updateSettings)
        
        self.ui.removedIslandROISelectionComboBox.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)
        self.ui.removedIslandROISelectionComboBox.currentIndexChanged.connect (self._updateSettings)
        self.ui.MovetoCoordinateAtEndofPaintCheckBox.stateChanged.connect (self._updateSettings)
        self.ui.trackPaintToolContinuouslyCheckBox.stateChanged.connect (self._updateSettings)        
        self._rebuildROINameList ()   
        self._updateUIToReflectContouringMode () 
        
        isN4BiasInstalled = NiftiVolumeData.hasSimpleITKInstalled () 
        self.ui.AutomaticN4BiasFieldCorrectionCheckBox.setEnabled (isN4BiasInstalled)        
        self.ui.N4BiasFieldInstallInstructionLbl.setVisible (not isN4BiasInstalled)
        self.ui.N4BiasMaskOptions.setVisible (isN4BiasInstalled)
        self.ui.AutomaticN4BiasFieldCorrectionCheckBox.setVisible (isN4BiasInstalled)
        self.ui.N4BiasMaskOptions.setEnabled (isN4BiasInstalled)
        self.ui.DisableN4BiasMaskBtn.setEnabled (isN4BiasInstalled)
        self.ui.AutomaticalyMaskBackgroundBtn.setEnabled (isN4BiasInstalled)
        self.ui.MaskGreyValuesAthThresholdBtn.setEnabled (isN4BiasInstalled)        
        self.ui.DisableN4BiasMaskBtn.setVisible (isN4BiasInstalled)
        self.ui.AutomaticalyMaskBackgroundBtn.setVisible (isN4BiasInstalled)
        self.ui.MaskGreyValuesAthThresholdBtn.setVisible (isN4BiasInstalled)
        self.ui.GreyValueThresholdTxt.setVisible (isN4BiasInstalled)
        
        self.ui.DisableN4BiasMaskBtn.setChecked (settings.isN4BiasMaskDisabled ())
        self.ui.AutomaticalyMaskBackgroundBtn.setChecked (settings.isAutomaticN4BiasMask ())
        self.ui.MaskGreyValuesAthThresholdBtn.setChecked (settings.isN4BiasMaskThreshold ())        
        self.ui.DisableN4BiasMaskBtn.toggled.connect (self._updateSettings)        
        self.ui.AutomaticalyMaskBackgroundBtn.toggled.connect (self._updateSettings)        
        self.ui.MaskGreyValuesAthThresholdBtn.toggled.connect (self._updateSettings)                
        self.ui.GreyValueThresholdTxt.setText (str(settings.getN4BiasCustomMaskThreshold ()))        
        self.ui.GreyValueThresholdTxt.editingFinished.connect (self._N4BThresholdChanged)
        self.ui.GreyValueThresholdTxt.setEnabled (isN4BiasInstalled and self.ui.MaskGreyValuesAthThresholdBtn.isChecked ())
        self.ui.GreyValueThresholdTxt.installEventFilter(self._Parent._ChildWidgetOverrideEventFilter)                
        self.ui.DisableImagingOrientationChangeCheckBox.setChecked (settings.isImageOrientationChangeDisabled ())
        self.ui.DisableImagingOrientationChangeCheckBox.stateChanged.connect (self._updateSettings)        
    
    def _disableMouseWheelScrolling (self, isDisabled) :
        enabledSetting = not isDisabled
        if self.ui.MouseWheelSensitivitySlider.isEnabled () != enabledSetting :
            self.ui.MouseWheelSensitivitySlider.setEnabled (enabledSetting)
            self.ui.invertMouseWheelBtn.setEnabled (enabledSetting)
            self.ui.MouseWheelSensitivityLabel.setEnabled (enabledSetting)
            self.ui.MouseWheelSensitivityLabel_Low.setEnabled (enabledSetting)
            self.ui.MouseWheelSensitivityLabel_Normal.setEnabled (enabledSetting)
            self.ui.MouseWheelSensitivityLabel_High.setEnabled (enabledSetting)
        
    def _N4BThresholdChanged (self) :
        try :
            val = int (self.ui.GreyValueThresholdTxt.text ())
            self.VisSettings.setN4BiasCustomMaskThreshold (val)
        except:
            self.ui.GreyValueThresholdTxt.setText (str(self.VisSettings.getN4BiasCustomMaskThreshold ()))                    
            
        
    def _rebuildROINameList (self) :        
        Selection = self.VisSettings.getSetRemovedIslandToROI ()
        try:
            self.ui.MovetoCoordinateAtEndofPaintCheckBox.currentIndexChanged.disconnect (self._updateSettings)
            reconnect = True
        except:
            reconnect = False            
        self.ui.removedIslandROISelectionComboBox.clear ()
        self.ui.removedIslandROISelectionComboBox.addItem ("None")        
        SelectedIndex = 0        
        index = 0
        for name in self._Parent.ROIDefs.getSortedNameLst () :
            if name != "None" and self._Parent.ROIDefs.isROIArea (name) :                
                index += 1
                self.ui.removedIslandROISelectionComboBox.addItem (name)
                if name == Selection :                
                    SelectedIndex = index
        self.ui.removedIslandROISelectionComboBox.setCurrentIndex(SelectedIndex)
        if reconnect :
            self.ui.MovetoCoordinateAtEndofPaintCheckBox.currentIndexChanged.connect (self._updateSettings)
            
    def event (self, event) :
        if (event.type() == QtCore.QEvent.WindowActivate) : 
            self._rebuildROINameList ()   
            self._updateUIToReflectContouringMode () 
        return QDialog.event(self, event)
    
    def keyPressEvent(self, event):
        KeyStrokes = ShortcutKeyList.getPassThroughKeyStrokes () 
        if (event.key () in KeyStrokes) : 
            self._Parent.keyPressEvent (event) 
            
    def _updateUIToReflectContouringMode (self) :        
        if  self._isInAreaContourMode () :
            self.ui.Settings.setTabEnabled(4,False)
            self.ui.IslandRemovalToolOptions.setEnabled (True)
            self.ui.PaintToolOptions.setEnabled (True)
        else:
            self.ui.Settings.setTabEnabled(4,True)
            self.ui.IslandRemovalToolOptions.setEnabled (False)
            self.ui.PaintToolOptions.setEnabled (False)
            
    def _isInAreaContourMode (self) :
        try :
            return self._Parent.ROIDictionary.isROIinAreaMode ()
        except:
            return False
    
    def _updateSettings (self) :        
        value = int (self.ui.transpSlider.value ())
        self.VisSettings.setUITranparency (value)                        
        self.VisSettings.setShowFileNameTitle ( self.ui.ShowFileNameCheckBox.isChecked())
        self.VisSettings.setMoveToCoordinateAtPaintEnd (self.ui.MovetoCoordinateAtEndofPaintCheckBox.isChecked())
        self.VisSettings.setMoveToCoordinateDuringPaint (self.ui.trackPaintToolContinuouslyCheckBox.isChecked())
        self.VisSettings.setRemovedIslandToROI (self.ui.removedIslandROISelectionComboBox.currentText())
        self.VisSettings.setPerformAutomaticN4BiasCorrection (self.ui.AutomaticN4BiasFieldCorrectionCheckBox.isChecked ())
        self.VisSettings.setUIBoxIndicator (self.ui.BoxIndicatorCheckbox.isChecked())
        self.VisSettings.setUIDashIndicator (self.ui.DashedBorderCheckbox.isChecked ())
        self.VisSettings.setUISmoothVisualization (self.ui.smoothVisualization.isChecked ())        
        self.VisSettings.setShowROIBoundingBoxInEditMode (self.ui.showROIBoundingBoxInEditMode.isChecked ())
        self.VisSettings.setShowSliceOrientation (self.ui.ShowAxisOrientationIndicators.isChecked ())
        self.VisSettings.setObjectSelectionMove (self.ui.objectSelectionMove.isChecked ())
        self.VisSettings.setInvertMouseWheel (self.ui.invertMouseWheelBtn.isChecked ())
        self.VisSettings.setMoveToLastEditedSliceOnROISelection (self.ui.ROISelectionMoveToLastEditedSlice.isChecked ())
        self.VisSettings.setShouldAutoLockROIWhenHidden (self.ui.AutoLockROIOnHideingBtn.isChecked ())
        self.VisSettings.setPerform3DIslandRemoval (self.ui.performIslandRemovalIn3D.isChecked ())
        self._disableMouseWheelScrolling (self.ui.DisableMouseWheelScrolling.isChecked ())
        self.VisSettings.setMouseWheelSliceSelectionEnabled (self.ui.DisableMouseWheelScrolling.isChecked ())
        self.VisSettings.setMouseWheelSliceSelectionSensitivity (self.ui.MouseWheelSensitivitySlider.value ())
        value = int (self.ui.undoredoSlider.value ())
        self.VisSettings.setUndoRedoStackSize (value)
        
        self.VisSettings.setDisableImagingOrientationChange (self.ui.DisableImagingOrientationChangeCheckBox.isChecked ())        
        if self.ui.DisableN4BiasMaskBtn.isChecked () :            
            self.VisSettings.setN4BiasMaskModeDisabled ()
        elif self.ui.AutomaticalyMaskBackgroundBtn.isChecked () :            
            self.VisSettings.setN4BiasMaskModeAutomatic ()
        elif self.ui.MaskGreyValuesAthThresholdBtn.isChecked () :            
            self.VisSettings.setN4BiasMaskModeCustomThreshold ()
        try :
            val = int (self.ui.GreyValueThresholdTxt.text ())
            self.VisSettings.setN4BiasCustomMaskThreshold (val)
        except:
            pass
        isN4BiasInstalled = NiftiVolumeData.hasSimpleITKInstalled () 
        self.ui.GreyValueThresholdTxt.setEnabled (isN4BiasInstalled and self.ui.MaskGreyValuesAthThresholdBtn.isChecked ())
        
        
    def _undoredoValueSider (self, value) :        
        self.ui.undoredo_value.setText (str(self.ui.undoredoSlider.value ()))
        self._updateSettings ()       
        
    def _undoredoValueChanged (self) :
        try:
            value = int (self.ui.undoredo_value.text ())
            self.ui.undoredoSlider.setValue (value)
        except:
            value = self.ui.undoredoSlider.value ()
            self.ui.undoredo_value.setText (str (value))                        
        self._updateSettings ()    
        
    def _txtValueChanged (self) :
        try:
            value = int (self.ui.transpValueTxt.text ())
            self.ui.transpSlider.setValue (value)
        except:
            value = self.ui.transpSlider.value ()
            self.ui.transpValueTxt.setText (str (value))                        
        self._updateSettings ()    
            
    def _minValueChanged (self, value) :        
        self.ui.transpValueTxt.setText (str(self.ui.transpSlider.value ()))
        self._updateSettings ()    
    
    
    
    def UpdateVisSettingsDlgROITransparency (self, val) :
        self.ui.transpSlider.setValue (val)        
            
    def closeDlg (self):   
        try :
            val = int (self.ui.GreyValueThresholdTxt.text ())
            self.VisSettings.setN4BiasCustomMaskThreshold (val)
        except:
            pass                 
        self.accept ()
        
        


"""
    RCC_ContourDlg
    
    Modal dialog user interface for Contour and Point editing. 
"""
class RCC_ContourDlg (QDialog) :   
    
    def __init__ (self, parent, ROIDictionary, ContourID = None, objectType = "Area") :                          
        self._okClicked = False
        self._LastMouseMoveROIState = [None, None, False]
        self._objectType = objectType
        ROIDefs = ROIDictionary.getROIDefs ()
        ROIName = ROIDefs.getSelectedROI ()[0]        
        
        if (ContourID != None) :
            self._ContourID = ContourID 
        else:
            self._ContourID = ROIDictionary.getROISelectedContours (ROIName)[0]
      
        QDialog.__init__ (self, parent)
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)         
        self.ui = Ui_RCC_CountourNameEditor ()                
        self.ui.setupUi (self)               
        self.setMinimumSize(self.size ())
        self.setMaximumSize(self.size ())
        self.ui.ROINameLbl.setText (ROIName)        
        font = self.ui.ROINameLbl.font ()                            
        font.setBold (True)
        self.ui.ROINameLbl.setFont (font) 
        palette = QtGui.QPalette()        
        palette.setColor (self.ui.ROINameLbl.foregroundRole(), ROIDefs.getSelectedROIColor ())
        self.ui.ROINameLbl.setPalette(palette) 
        
        if (objectType == "Point") :
            self.ui.label_2.setText ("Point Name:")
            self.ui.label.setText ("")
            self.setWindowTitle ("Define Point")
        
        self._ROIObj = ROIDictionary.getROIObject (ROIName)        
        contourName = self._ROIObj.getContourName (self._ContourID)
        self.ui.contourNameTxt.setText (contourName)
        if (objectType == "Area") :        
            self.ui.contourType.setEnabled (True)
            contourType = self._ROIObj.getContourType (self._ContourID)
            self.ui.contourType.addItem ("ROI area (include area in ROI)")
            self.ui.contourType.addItem ("Void (exclude area from ROI)")
            self.ui.contourType.addItem ("Auto")        
            
            if (contourType == "Auto") :
                self.ui.contourType.setCurrentIndex(self.ui.contourType.findText("Auto"))
            elif (contourType == "Add") :
                self.ui.contourType.setCurrentIndex(self.ui.contourType.findText("ROI area (include area in ROI)"))
            elif (contourType == "Sub") :
                self.ui.contourType.setCurrentIndex(self.ui.contourType.findText("Void (exclude area from ROI)"))
        else:
            self.ui.contourType.setEnabled (False)
        self.ui.OKBtn.clicked.connect (self.OKClk)        
        self.ui.CancelBtn.clicked.connect (self.CancelClk)        
        
    def OKClk (self):
        self._okClicked = True
        self._ROIObj.setContourName (self._ContourID, self.ui.contourNameTxt.text ())                                
        if (self._objectType == "Area") :
            selection = self.ui.contourType.currentText ()
            if (selection == "Auto"):
                self._ROIObj.setContourType (self._ContourID, "Auto")
            elif (selection == "ROI area (include area in ROI)"):
                self._ROIObj.setContourType (self._ContourID, "Add")
            else:
                self._ROIObj.setContourType (self._ContourID, "Sub")
        self.accept ()
        
    def CancelClk (self) :
        self.accept ()
                
    def okClicked (self) :        
        return self._okClicked
        
   
class NIfTIDropQuestionDlg (QDialog) :   
   def __init__ (self, parent, middleButtonTxt, middleButtonValue) :     
        QDialog.__init__ (self, parent)
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)         
        self.ui = Ui_NIfTDragDropDlg ()                
        self.ui.setupUi (self)     
        self._ButtonPressed = "Cancel"
        self._MiddlebuttonValue = middleButtonValue
        self.ui.CancelBtn.clicked.connect (self.reject)
        self.ui.segpreviewbtn.clicked.connect (self.middleButton)
        self.ui.segpreviewbtn.setText (middleButtonTxt)
        self.ui.primaryimageBtn.clicked.connect (self.mainImageButton)
        
   def middleButton (self) :
       self._ButtonPressed = self._MiddlebuttonValue
       self.accept ()
       
   def mainImageButton (self) :
       self._ButtonPressed = "MainImage"
       self.accept ()  
       
   def getButtonPressed (self) :
       return self._ButtonPressed
     
class ChildWidgetOverrideEventFilter (QtCore.QObject) :
      
        def __init__ (self, app, parent):
          QtCore.QObject.__init__ (self)
          self._ParentWindow = parent
          self._app = app
          
        def clearParentWindow (self) :
            self._ParentWindow = None
            
        def eventFilter (self, sourceObj, event):                        
            if (event.type() == QtCore.QEvent.KeyPress or event.type() == QtCore.QEvent.KeyRelease) :                 
                 if not (self._ParentWindow is None) :                                          
                     KeyStrokes = []                         
                     if (sourceObj.objectName () in ["SetPenThresholdLowerThresholdMask","SetPenThresholdUpperThresholdMask"]):
                         KeyStrokes += ShortcutKeyList.getNonNumericPassThroughKeyStrokesWholeNumbersOnly ()                         
                     elif (sourceObj.objectName () in ["normalizePenSizeToVoxelDimensionsChkBox","XZSliceAxisSliceTextBox","YZSliceAxisSliceTextBox","eraseOutofRangeChk","SetPenDrawModeBtn","SetPenEraseModeBtn","SetPenCircleShapeBtn", "SetPenSquareShapeBtn", "SetPenThresholdMaskDisabledBtn", "SetPenThresholdMaskEnabledBtn", "SetPenThresholdMaskEnabledBtn", "PenSliceThickness","PenSize","LevelPreset","maxamizeWithinSliceContrast","undoredoSlider", "undoredo_value","MajorAxisSliceTextBox","minValueTxt","maxValueTxt","levelValue","windowValue","minSlider","maxSlider","LevelSlider","WindowSlider","fixedRange","transpValueTxt","transpSlider"]) :
                         KeyStrokes += ShortcutKeyList.getNonNumericPassThroughKeyStrokes ()                         
                     elif not isinstance(sourceObj, QLineEdit):                                                              
                         KeyStrokes += ShortcutKeyList.getNonNumericPassThroughKeyStrokes ()
                         if not isinstance(sourceObj, QSpinBox):                        
                             KeyStrokes += [QtCore.Qt.Key_1, QtCore.Qt.Key_2, QtCore.Qt.Key_3, QtCore.Qt.Key_4, QtCore.Qt.Key_5, QtCore.Qt.Key_6, QtCore.Qt.Key_7, QtCore.Qt.Key_8, QtCore.Qt.Key_9, QtCore.Qt.Key_0]                         
                         KeyStrokes += [QtCore.Qt.Key_Delete,QtCore.Qt.Key_Backspace]                                              
                         if isinstance(sourceObj, QDialog):                        
                             KeyStrokes += [QtCore.Qt.Key_Left, QtCore.Qt.Key_Right, QtCore.Qt.Key_Up, QtCore.Qt.Key_Down]
                             #KeyStrokes += [QtCore.Qt.Key_Shift,QtCore.Qt.Key_Control]
                                              
                     if event.key () in KeyStrokes :
                         self._app.sendEvent (self._ParentWindow, event)                         
                         return True
            return False

class IngoreMouseWheelEventFilterObj  (QtCore.QObject) :
        def eventFilter (self, sourceObj, event):    
            return(event.type() == QtCore.QEvent.Wheel)




"""
    RCC_Project
    
    Main applicaiton for the RCC project.
"""     
class RCC_ContourWindow (QMainWindow):       
    
      
    def __init__ (self, ProjectDataset, Parent= None) :   
        super(RCC_ContourWindow, self).__init__ (Parent)
      
        
        # project    \
        self._isActiveContourWindow = None
        self._autoSnappedClose = None                    
        self._LoadedNiftiOrientation = []
        self._XYUIState = "None"
        self._setUIReadOnlyModeFired = False
        self._LastReadOnlyMode = False
        self._windowUpdateEnabled = True
        self._pendingWindowUpdate = False
        self._DisablePenSettingProjectChange = False
        self._lastOpenedNIfTIDataFile  = None
        self._ChildWidgetOverrideEventFilter = ChildWidgetOverrideEventFilter (ProjectDataset.getApp (), self)
        self._HiddenDlgMem = {}
        self._ML_ModelZooDlg = None
        self._ML_QuantifySegmentatinDlg = None
        self._RangeSelectionCallBack = None        
        
        self._datasetChangeOldSelectedROINameLst = []
        self._TranslationDlg = None
        self._MLDatasetDescriptionDlg = None
        self._PenSettingsDlg = None
        self._PaintToolsDlg = None
        self._penSettings = None
        self._SelectionMoveROIState = None
        self._DisableWindowChangeEvent = False
        self._hounsfeildDlg = None        
        self._visOptionsDlg = None
        self._PesscaraMaskViewerDlg = None
        self._ProjectDataset = ProjectDataset         
        self._setPreviewSegmentationMaskPath (None)
        
                
        self.resizeWidgetHelper = None
        self._KeyboardState = {}
        self.clearUIState ()
        self._ROIDictionaryChangeEvent = False   
        self._DisableContourListSelectionChanged = False        
        
        # Load hounsfield visulization settings
        self.hounsfieldVisSettings = HounsfieldVisSettings ()        
        
        # Load visulization settings  
        self.visSettings = VisSettings ()
        self.visSettings.addListener (self._visSettingsChanged)
                 
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)
        self.ui = Ui_RCC_MainWindow ()        
        self.ui.setupUi (self)
        self.setMinimumSize(QtCore.QSize(920, 800))        
        self.ui.horizontalSplitter.setMinimumHeight (460)
        self.ui.mainSplitter.setSizes ([600,self.height ()-610])
        self.ui.mainSplitter.splitterMoved.connect (self._mainSplitterMoved)
        self.menuBar().setNativeMenuBar (False)
        
        self.ui.Quit.triggered.connect (self.QuitMenu)     
        self.ui.actionOpen_New_Slice_Viewing_Window.triggered.connect  (self.CreateNewViewingWindow)        
        
        self.ui.actionCustom_Tags.triggered.connect (self.ShowCustomDatasetTagEditor)        
        self.ui.actionInternal_Tags.triggered.connect (self.ShowInternalDatasetTags)        
        
        self.ui.actionSave_to_DB.setEnabled (False)
        self.ui.actionSave_to_DB.triggered.connect (self.SaveToDB)        
        self.ui.actionOpen_3D_Viewer_2.triggered.connect (self.ROI3DVolumeViewer)        
        self.ui.actionShow_ROI_Statistics.triggered.connect  (self.ShowStatsDlg)                                                   
        self.ui.actionShow_Hounsfield_visualization_settings.triggered.connect  (self.HounsfieldVisualizationDlg)        
        self.ui.actionExportVolume_mask.triggered.connect (self.exportMaskVolume)
        self.ui.actionExportVolume_CSV.triggered.connect (self.exportVolumeCSV)
        self.ui.actionROI_Translation.triggered.connect (self.showTranslationDlg)
        self.ui.actionML_scan_description.triggered.connect (self.showMLDatasetDescriptionDlg)
        self.ui.actionShow_Pesscara_Mask_Viewer_Window.triggered.connect (self.ShowPesscaraMaskViewerDlg)
        self.ui.actionExportSeriesAnnotations.triggered.connect (self.exportVolumeTagsDlg)
        self.ui.actionVisualization_settings_2.triggered.connect  (self.SetVisSettingsAction)            
        self.ui.actionRegistration_log_window.triggered.connect (self.showANTSRegistrationLog)
        self.ui.actionPen_Settings_window.triggered.connect (self.showPenSettingsDlg)
        self.ui.actionPaint_Tools_window.triggered.connect (self.showPaintToolsDlg)
        self.ui.actionCopy.triggered.connect (self.copyROI)
        self.ui.actionPaste.triggered.connect (self.pasteROI)
        self.ui.menuEdit.aboutToShow.connect (self.canCopyandPasteROI)
        self.ui.menuFile.aboutToShow.connect (self.aboutToShowFileMenu)
        self.ui.actionROI_From_NIfTI_Mask.triggered.connect (self.importROIFromMask)        
        self.ui.SetReadOnly.triggered.connect (self.toggleReadOnlyMode)
        self.ui.actionModel_manager.triggered.connect (self.modelManager)
        self.ui.actionModel_zoo.triggered.connect (self.modelZoo)
        self.ui.actionQuantify_segmentation.triggered.connect (self.quantifySegmentation)
        self.ui.actionSave_debugging_report.triggered.connect (self.saveRILContourDebuggingState)
        
        
        self.ui.actionN4Bias_field_corrected.triggered.connect (self.showN4BiasFieldCorrectionImage)
        self.ui.actionShow_regular_image.triggered.connect (self.showRegularImage)
        
        self.ui.createFileVersion.triggered.connect (self.versionFile)
        self.ui.RollBackFileVersion.triggered.connect (self.rollBackFile)
        self.ui.actionView_version_history.triggered.connect (self.viewVersionHistory)
        self.ui.View_lock_history.triggered.connect (self.viewPLockHistory)
        self.ui.CheckoutFileLock.triggered.connect (self.checkoutPLock)
        self.ui.CheckInfileLock.triggered.connect (self.checkInPLock)
        self.ui.Assign_file_lock.triggered.connect (self.assignPLock)
        self.ui.actionRevert_checkout.triggered.connect (self.revertPLockCheckout)
        
        self.ui.actionContour.triggered.connect (self.convertROIToPerimeterContours)
        self.ui.actionPaint.triggered.connect (self.convertROIToAreaMask)
        self.ui.actionAbout.triggered.connect (self.showAboutDlg)

        self.ui.actionHelp.triggered.connect (self.showHelpDlg)
        
        self.ui.showOrigionalImaging.triggered.connect (self._showOrigionalImaging)
        self.ui.showTransformedImaging.triggered.connect (self._showTransformedImaging)
        
        self.ui.menuFilters.aboutToShow.connect (self.setShowFilters)
        self.ui.actionFill_ROI_holes.triggered.connect (self.fillROIHoleInSelectedSlice)
        self.ui.actionErode_ROI.triggered.connect(self.erodeROIInSelectedSlice)
        self.ui.actionDilate_ROI.triggered.connect (self.dilationROIInSelectedSlice)
        self.ui.actionKeep_largest_3D_Island.triggered.connect (self.KeepOnlyLargestObjectFilter)
        self.ui.menuML.aboutToShow.connect (self.aboutToShowML_Menu) 
        self.ui.menuSettings.aboutToShow.connect (self.aboutToShowSettingsMenu) 
        self.ui.menuSet_Voxel_Annotation_Mode.aboutToShow.connect (self.setVoxelAnnotationMenuSettings)
        self.ui.menuWindow.aboutToShow.connect (self.setShowWindow)
        
        # Load ROI definitions
        self.ROIDefs = ROIDefinitions(self._ProjectDataset.getDocumentUIDFileLogInterface ())        
        try:            
            self.ROIDefs.loadROIDefsFromProjectFile ()             
        except :
            self.ROIDefs = ROIDefinitions(self._ProjectDataset.getDocumentUIDFileLogInterface ())                            
        self._setListUI (self.ui.ROI_List, self.ROIDefs)        
        self.ROIDefs.addChangeListener (self.ROIDictionaryChange)
        self.ROIDefs.addSelectionChangedListener (self.ROIDictionaryChange)                
        self.NiftiVolumeTreeSelection = None                 #Stores path to loaded Nifti file          
        self.SelectedCoordinate = Coordinate ()     #Stores currently selected coordinate and provides update mechanism to notifiy application when the selected coordinate changes.     
        
        self.ROIDictionary= ROIDictionary (self.ROIDefs, self.hounsfieldVisSettings, self.visSettings)   #Initalizes the ROI Dictionary. Object stores ROI definitions for the loaded dataset.  Also provides undo/redo mechanisms     
        self.ROIDictionary.addDictionaryChangedListner (self.ROIDictionaryChange) #Callback fired when ROI Dictionary changes
        self.ROIDictionary.addUndoRedoDictionaryChangedListner (self.ROIDictionaryUndoRedo) #Callback fired when for undo/redo
        self.ROIDictionary.addReadOnlyModeChangeListener (self.dictionaryChangedReadOnlyMode)
        # Zoom UI controls.  Zoom controls are mannaged by XYSliceView UI control
        self.ui.verticalZoomScrollBar.setVisible (False) 
        self.ui.verticalZoomScrollBar.resize ( QtCore.QSize(15,15))
        self.ui.horizontalZoomScrollBar.setVisible (False)
        self.ui.horizontalZoomScrollBar.resize (QtCore.QSize (15,15))
        
        self.ui.XZ_VerticalZoomScrollBar.setVisible (False) 
        self.ui.XZ_VerticalZoomScrollBar.resize ( QtCore.QSize(15,15))
        self.ui.XZ_HorizontalZoomScrollBar.setVisible (False)
        self.ui.XZ_HorizontalZoomScrollBar.resize (QtCore.QSize (15,15))
        self.ui.YZ_VerticalZoomScrollBar.setVisible (False) 
        self.ui.YZ_VerticalZoomScrollBar.resize ( QtCore.QSize(15,15))
        self.ui.YZ_HorizontalZoomScrollBar.setVisible (False)
        self.ui.YZ_HorizontalZoomScrollBar.resize (QtCore.QSize (15,15))
        
        # Initalize XY NIfTi slice view widget
        self.ui.XYSliceView.setHounsfieldVisSettings (self.hounsfieldVisSettings)
        self.ui.XYSliceView.setVisSettings (self.visSettings)        
        self.ui.XYSliceView.addSliceListener (self.NiftiAxisSelection)
        self.ui.XYSliceView.addMouseMoveListener (self.MajorAxisMouseMoveEvent)
        self.ui.XYSliceView.addMousePressedListener (self.NiftiAxisMousePressed)       
        self.ui.XYSliceView.addZoomListener (self.ZoomChanged)       
        self.ui.XYSliceView.addSelectionListener (self.ui.XZSliceView.selectionChangeCallback)
        self.ui.XYSliceView.addSelectionListener (self.ui.YZSliceView.selectionChangeCallback)
        self.setDrawSelectedROIOnTop ()
        
        # Initalize XZ NIfTi slice view widget
        self.ui.XZSliceView.setHounsfieldVisSettings (self.hounsfieldVisSettings)
        self.ui.XZSliceView.setVisSettings (self.visSettings)        
        self.ui.XZSliceView.addSliceListener (self.NiftiAxisSelection) 
        self.ui.XZSliceView.addMouseMoveListener (self.SecondaryAxisMouseMoveEvent)        
        self.ui.XZSliceView.addMousePressedListener (self.NiftiAxisMousePressed)       
        self.ui.XZSliceView.addZoomListener (self.ZoomChanged)                
        self.ui.XZSliceView.addSelectionListener (self.ui.XYSliceView.selectionChangeCallback)
        self.ui.XZSliceView.addSelectionListener (self.ui.YZSliceView.selectionChangeCallback)
        
        # Initalize YZ NIfTi slice view widget
        self.ui.YZSliceView.setHounsfieldVisSettings (self.hounsfieldVisSettings)
        self.ui.YZSliceView.setVisSettings (self.visSettings)        
        self.ui.YZSliceView.addSliceListener (self.NiftiAxisSelection)
        self.ui.YZSliceView.addMouseMoveListener (self.SecondaryAxisMouseMoveEvent)  
        self.ui.YZSliceView.addMousePressedListener (self.NiftiAxisMousePressed)       
        self.ui.YZSliceView.addZoomListener (self.ZoomChanged)                
        self.ui.YZSliceView.addSelectionListener (self.ui.XYSliceView.selectionChangeCallback)
        self.ui.YZSliceView.addSelectionListener (self.ui.XZSliceView.selectionChangeCallback)
        
        # Initalize XY slice selector widget
        self.ui.XYSliceSelectorWidget.initWidget ()
        self.ui.XYSliceSelectorWidget.addSliceListener (self.sliceSelectorUpdateSliceSelection)
        self.ui.XYSliceSelectorWidget.setFocusPolicy (QtCore.Qt.ClickFocus)
        
        self.ui.XYSliceVerticalSelector.initWidget (SliceView = self.ui.XYSliceView)
        self.ui.XYSliceVerticalSelector.addSliceListener (self.sliceSelectorUpdateSliceSelection)
        self.ui.XYSliceVerticalSelector.setFocusPolicy (QtCore.Qt.ClickFocus)
        
        self.ui.YZSliceVerticalSelector.initWidget (SliceView = self.ui.YZSliceView)
        self.ui.YZSliceVerticalSelector.addSliceListener (self.YZSliceSelectorUpdateSliceSelection)
        self.ui.YZSliceVerticalSelector.setFocusPolicy (QtCore.Qt.ClickFocus)
        
        self.ui.XZSliceVerticalSelector.initWidget (SliceView = self.ui.XZSliceView)
        self.ui.XZSliceVerticalSelector.addSliceListener (self.XZSliceSelectorUpdateSliceSelection)
        self.ui.XZSliceVerticalSelector.setFocusPolicy (QtCore.Qt.ClickFocus)
        
        self.setFocusPolicy (QtCore.Qt.ClickFocus)
        
        # Initalize slice selector textbox
        self.ui.MajorAxisSliceTextBox.editingFinished.connect (self.MajorAxisTextBoxChanged)        
        self.ui.MajorAxisSliceTextBox.installEventFilter(self._ChildWidgetOverrideEventFilter)
        
        self.ui.YZSliceAxisSliceTextBox.editingFinished.connect (self.YZSliceAxisTextBoxChanged)        
        self.ui.XZSliceAxisSliceTextBox.editingFinished.connect (self.XZSliceAxisTextBoxChanged)        
        self.ui.YZSliceAxisSliceTextBox.installEventFilter(self._ChildWidgetOverrideEventFilter)
        self.ui.XZSliceAxisSliceTextBox.installEventFilter(self._ChildWidgetOverrideEventFilter)
        

        self.ui.ClearSelectedBtn.clicked.connect (self.RemoveROIBtn)                       
        self.SelectedCoordinate.addChangeListenerCallback (self.SelectedCoordinatChanged)                  
        
        # Initalize ROIList and ContourList UI
        self.ui.ROI_List.selectionModel().selectionChanged.connect(self.ROIListSelectionChanged)        
        #self.ui.ROI_List.itemDoubleClicked.connect(self.ROIListDblClk)                         
        self.ui.ContourList.selectionModel().selectionChanged.connect(self.ContourListSelectionChanged)
        self.ui.ContourList.itemDoubleClicked.connect(self.ContourListDblClk)                 
        
        
        self.ui.autoContourBtn.clicked.connect (self.autoContourBtn)        
        self.ui.XYSliceSelectorWidget.addDeleteSliceListener (self.deleteSliceCallback) 
        self.ui.XYSliceVerticalSelector.addDeleteSliceListener (self.deleteSliceCallback) 
        self.ui.XZSliceVerticalSelector.addDeleteSliceListener (self.deleteSliceCallback) 
        self.ui.YZSliceVerticalSelector.addDeleteSliceListener (self.deleteSliceCallback) 
        
        self.ui.contourSingleSliceBtn.clicked.connect (self.contourSingleSlice)
        
        paintTools = self._getPaintToolsDlg()
        paintTools.setButtonEnabledState (Select = False, Draw = False, ZoomIn = False, ZoomOut = False, EraseandFill = False, DeepGrow = False)                
        paintTools.move (self.pos().x () + self.width (), self.pos().y () + 100)
        paintTools.show ()
        
        self.ui.AddContourBtn.clicked.connect (self.addContourBtn)
        self.ui.BatchContourBtn.clicked.connect (self.BatchCountourBtn)
        
        self.ui.XYSliceView.setROIHiddenLabel ("")
        self.ui.XYSliceView.setROILockedLabel ("")
        
        # Initalize UI state.  Saves positions of UI widgets relative to the main window.  
        # Inital widget UI dimensions and position saved in self._WidgetUIMem dictionary for 
        # subsquent automatic window resizeing.
        
        self.resizeWidgetHelper = ResizeWidgetHelper ()
        
        self.resizeWidgetHelper.saveWidgetUIPos (self, self.ui.mainSplitter)        
        
        self.ui.XYFrame.resizeEvent = self._XYFrameResize
        self.ui.XZFrame.resizeEvent = self._XZFrameResize
        self.ui.YZFrame.resizeEvent = self._YZFrameResize
        self.ui.SliceFrame.resizeEvent = self._SliceFrameResizeEvent
        self.ui.ROIFrame.resizeEvent = self._ROIFrameResizeEvent
        self.ui.ToolFrame.resizeEvent = self._ToolFrameResize
        
        self.ui.ThresholdLbl.setText ("")
        
        # UI toolbar initalization, (selection [arrow], contour, zoom in, zoom out, and undo/redo)       
        self._XYZoomInit = None        
        
        basepath = self._ProjectDataset.getPythonProjectBasePath ()
        iconPath = os.path.join (basepath , "icons")        
                        
        self.ui.UndoBtn.setIcon(QtGui.QIcon(iconPath+ os.sep + "undo.png"))
        self.ui.UndoBtn.setIconSize(QtCore.QSize(32,32))
        self.ui.RedoBtn.setIcon(QtGui.QIcon(iconPath+ os.sep + "redo.png"))
        self.ui.RedoBtn.setIconSize(QtCore.QSize(32,32))        
        
        #self.ui.addROIBtn.setIcon(QtGui.QIcon(iconPath + os.sep + "plus.png"))
        #self.ui.addROIBtn.setIconSize(QtCore.QSize(20,20))        
        #self.ui.RemoveROIBtn.setIcon(QtGui.QIcon(iconPath + os.sep + "minus.png"))
        #self.ui.RemoveROIBtn.setIconSize(QtCore.QSize(20,20))
        
        self.ui.AddContourBtn.setIcon(QtGui.QIcon(iconPath + os.sep + "plus.png"))
        self.ui.AddContourBtn.setIconSize(QtCore.QSize(20,20))        
        self.ui.deleteContourBtn.setIcon(QtGui.QIcon(iconPath + os.sep + "minus.png"))
        self.ui.deleteContourBtn.setIconSize(QtCore.QSize(20,20))
        self.ui.deleteContourBtn.clicked.connect (self.deleteSelectedContours)
        #self.ui.addROIBtn.clicked.connect (self.addROIBtnClk)
        #self.ui.RemoveROIBtn.clicked.connect (self.removeROIBtnClk)
        
        self.ui.contourOrientationComboBox.currentIndexChanged.connect (self.contourOrientationChanged)
        
        self.ui.ROI_List.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.ui.ROI_List.customContextMenuRequested.connect(self.showContextMenu)
        
        self.ui.ROI_List.installEventFilter(self._ChildWidgetOverrideEventFilter)
        self.ui.ContourList.installEventFilter(self._ChildWidgetOverrideEventFilter)
        self.ui.contourOrientationComboBox.installEventFilter(self._ChildWidgetOverrideEventFilter)
        self.ui.ScanPhaseComboBox.installEventFilter(self._ChildWidgetOverrideEventFilter)
        
        self.ui.UndoBtn.clicked.connect (self.UndoBtnClk)
        self.ui.RedoBtn.clicked.connect (self.RedoBtnClk)        
        self.clearUIState () # state variable records selected UI Tool button
        
        self._StatsDlg = None
        
        # Initalizes Scan Phase UI selection dialog
        # reads scan phase definitions in from "ScanPhase.txt" file
        ScanPhaseList = self._ProjectDataset.getScanPhaseList ()     
        for phase in ScanPhaseList :
            self.ui.ScanPhaseComboBox.addItem (phase)
        
        self.ui.ScanPhaseComboBox.setEnabled (True)
        self.ui.ScanPhaseComboBox.currentIndexChanged.connect (self.ScanPhaseSel)                
                   
        self.setWindowTitle ("%s (%s)" % (self._ProjectDataset.getFilename (), self._ProjectDataset.getProjectName ()))             
        self.LoadNiftiViewer (None, "Data not selected", None, Orientation = "None")
        
        self._ignoreWheelEvent = IngoreMouseWheelEventFilterObj ()
        self.ui.XZSliceSlider.installEventFilter  (self._ignoreWheelEvent)
        self.ui.YZSliceSlider.installEventFilter  (self._ignoreWheelEvent)
        self.ui.XYSliceSlider.installEventFilter  (self._ignoreWheelEvent)
        self.ui.mainSplitter.setCollapsible (0, False)
        self.setAcceptDrops (True)
        
    def setDrawSelectedROIOnTop (self) :
        isInAreaMode = self.ROIDictionary.isROIinAreaMode ()
        self.ui.XYSliceView.setDrawSelectedROIOnTop (isInAreaMode)
        self.ui.XZSliceView.setDrawSelectedROIOnTop (isInAreaMode)
        self.ui.YZSliceView.setDrawSelectedROIOnTop (isInAreaMode)
        
    def dragEnterEvent(self, e):        
        if e.mimeData().hasText():
           e.accept()
        else:
           e.ignore()
			
    def dropEvent(self, e):
        try :
            pathlist = e.mimeData().text()            
            pathlist = pathlist.split ("\n") 
            for path in pathlist :
                if path.startswith ("file://") :
                    if path.endswith ("\r") :
                        path_src = QtCore.QUrl (path[ :-1])
                    else:
                        path_src = QtCore.QUrl (path)
                    path_src = path_src.path ()
                    if os.path.exists (path_src) :
                        self.openFileFromDrag (path_src)
        except:
            pass            
    
    def generateColorMapForNifti (self, values):
        try :
            import cv2
        except:
            MessageBoxUtil.showMessage ("OpenCV Error", "Error: Could not load OpenCV python library. OpenCV must be installed to preview segmentations.")
            return None, None                
                
        uniqueVals = FastUnique.unique (values)        
        reverse = np.zeros (values.shape, dtype=np.uint16)
        for index, uVal in enumerate (uniqueVals) :
            reverse[values == uVal] = index        
        values = reverse        
        numValues = len (uniqueVals)
        
        colorMapMem = np.zeros ((4,numValues), dtype = np.uint8)
        if numValues == 2 :
            colorMapMem[0,1] = 255
            colorMapMem[3,1] = 255
        if numValues > 2 :
            colorMap = np.arange (0, numValues-1)
            colorMap = (255.0 * (colorMap / float (numValues-2))).astype (np.int)
            colorMap = np.clip (colorMap, 0, 255).astype (np.uint8)
            colorMap = cv2.applyColorMap(colorMap, cv2.COLORMAP_JET)
            colorMapMem[0,1:] = colorMap[:,0, 2] #B
            colorMapMem[1,1:] = colorMap[:,0, 1] #G
            colorMapMem[2,1:] = colorMap[:,0, 0] #R
            colorMapMem[3,1:] = 255
        return values, colorMapMem
    
    def _generateSegmentationPreview (self, niftiSegmentation) :
        try :
            mem, colorMapMem = self.generateColorMapForNifti (niftiSegmentation.getImageData ())
            if mem is not None and colorMapMem is not None :
                mem = NiftiVolumeData.InitFromNumpyArray (mem, niftiSegmentation)
                colorMapMem = colorMapMem.T
                self.ui.XYSliceView.setVolumeMask (mem, colorTable = colorMapMem) 
                self.ui.XZSliceView.setVolumeMask (mem, colorTable = colorMapMem) 
                self.ui.YZSliceView.setVolumeMask (mem, colorTable = colorMapMem)                 
                self.ui.XYSliceVerticalSelector.setSliceColorMask (mem, colorMapMem) 
                self.ui.XYSliceSelectorWidget.setSliceColorMask (None, None, Fast = self.ui.XYSliceVerticalSelector.getSliceColorMask ())
                self.ui.XZSliceVerticalSelector.setSliceColorMask (mem, colorMapMem) 
                self.ui.YZSliceVerticalSelector.setSliceColorMask (mem, colorMapMem) 
                
        except:
            pass
    
    def ShowNIfTIDropQuestionDlg (self, MiddleButtonTxt, MiddleButtonResult) :
        dropQuestionDialog = NIfTIDropQuestionDlg (self, MiddleButtonTxt, MiddleButtonResult)    
        dropQuestionDialog.exec_()
        return dropQuestionDialog.getButtonPressed ()
    
    def _setPreviewSegmentationMaskPath (self, maskPath):
        self._previewSegmentationMaskPath = maskPath
    
    def _getPreviewSegmentationMaskPath (self):
        return self._previewSegmentationMaskPath
        
    def reloadPreviewSegmentationMask (self, path):
        if path is not None :
            droppedFile = nib.load (path)
            nivd = NiftiVolumeData (droppedFile, MatchOrientation = self.NiftiVolume)
            self._generateSegmentationPreview (nivd)
        
    def openFileFromDrag (self, path_src) :        
        teststr = path_src.lower ()
        if teststr.endswith (".nii") or teststr.endswith (".nii.gz") :
            Operation = "MainImage" 
            if self.NiftiVolume is not None :
                xDim, yDim, zDim = self.NiftiVolume.getSliceDimensions ()  # faster test if volume dimensions match.. first test if the scans are have the 
                dropX, dropY, dropZ = None, None, None                     # have the same numbers then check if they are actually aligned if they truely match..
                try :
                    droppedFile = nib.load (path_src)
                    initalShapeTest = droppedFile.shape 
                    shapeTuple = tuple (sorted ([initalShapeTest[0], initalShapeTest[1], initalShapeTest[2]])) 
                    if (shapeTuple == tuple (sorted([xDim, yDim, zDim]))) : #volume shapes appear to be the same, now test if in the same order
                        nivd = NiftiVolumeData (droppedFile, MatchOrientation = self.NiftiVolume)
                        dropX, dropY, dropZ = nivd.getSliceDimensions ()
                except:
                    pass
                if xDim == dropX and yDim == dropY and zDim == dropZ :
                   if self.isROIDictonaryFileLoaded () :
                       Operation = self.ShowNIfTIDropQuestionDlg ("Import segmentation", "ImportSegmentation")
                   else:
                       Operation = self.ShowNIfTIDropQuestionDlg ("Preview segmentation", "PreviewSegmentation")    
            if Operation == "ImportSegmentation" :
                self.importROIFromMask (NiftiPath = path_src)
            elif Operation == "PreviewSegmentation" :
                self._generateSegmentationPreview (nivd)
                self._setPreviewSegmentationMaskPath (path_src)
            elif Operation == "MainImage" :
                niftiDataset = self._ProjectDataset.getNIfTIDatasetInterface ().findFileInDataset (path_src)  # Search for the nifti file in current active project
                if (niftiDataset is not None) :
                    aw = self._ProjectDataset.getActiveWindowShowingNifti (niftiDataset, AcceptEmpty = True)
                    if (aw is not None) :
                        if (aw.getLoadedNIfTIFile () is None):  # No loaded nifti
                            aw.DatasetTreeSelectionChanged (niftiDataset, NIfTIFilePath = None)
                        self._ProjectDataset.setActiveWindow (aw)
                    else:
                        self._ProjectDataset.createContouringWindow ()
                        aw = self._ProjectDataset.getActiveWindow ()
                        aw.DatasetTreeSelectionChanged (niftiDataset, NIfTIFilePath = None)
                        self._ProjectDataset.setActiveWindow (aw)
                    dlg = self._ProjectDataset.getNiftiDatasetDlg ()
                    if dlg is not None :
                        dlg.setTreeViewSelectedDataset (niftiDataset)
                else:
                    # Next search for the nifti in all open projects
                    FoundProject, niftiDataset = self._ProjectDataset.findNIfTIinExistingOpenProject (path_src, IngnoreProject = self._ProjectDataset)
                    if (FoundProject is not None) :    
                        aw = FoundProject.getActiveWindowShowingNifti (niftiDataset, AcceptEmpty = True)
                        if (aw is not None) :
                            if (aw.getLoadedNIfTIFile () is None):  # No loaded nifti
                               aw.DatasetTreeSelectionChanged (niftiDataset, NIfTIFilePath = None)
                               FoundProject.setActiveWindow (aw)    
                        else :
                            FoundProject.createContouringWindow ()
                            aw = FoundProject.getActiveWindow ()
                            aw.DatasetTreeSelectionChanged (niftiDataset, NIfTIFilePath = None)
                            FoundProject.setActiveWindow (aw)    
                        dlg = self.FoundProject.getNiftiDatasetDlg ()
                        if dlg is not None :
                            dlg.setTreeViewSelectedDataset (niftiDataset)
                        dlg = FoundProject.getNiftiDatasetDlg()
                        if dlg is not None :
                            dlg.activateWindow ()
                            dlg.raise_()                    
                        aw = FoundProject.getActiveWindow ()
                        if aw is not None :
                            FoundProject.setActiveWindow (aw)                  
                    else:
                        if self.NiftiVolumeTreeSelection is not None and self.NiftiVolumeTreeSelection.isSelected () :
                            self.NiftiVolumeTreeSelection.setSelected (False)
                            dlg = self._ProjectDataset.getNiftiDatasetDlg ()
                            if dlg is not None :
                                dlg.setTreeViewSelectedDataset (None)
                        if (self.getLoadedNIfTIFile () is None):  # No loaded nifti
                            self.DatasetTreeSelectionChanged (None, NIfTIFilePath = path_src)
                        else:
                            self._ProjectDataset.createContouringWindow ()
                            aw = self._ProjectDataset.getActiveWindow ()
                            if (aw is not None) :
                                aw.DatasetTreeSelectionChanged (None, NIfTIFilePath = path_src)
                                self._ProjectDataset.setActiveWindow (aw)     
            else:
                return
        elif teststr.endswith (".prj") :
            dlg = self._ProjectDataset.getNiftiDatasetDlg ()
            if (dlg is not None) :
                dlg.openProjectFile (path_src)            
        elif teststr.endswith ("_roi.txt") :
            pass
                
    def setUIReadOnlyMode (self, ReadOnlyState) :   
        if self._setUIReadOnlyModeFired : 
            return
        try :
            self._setUIReadOnlyModeFired = True                             
            if not self.ROIDictionary.isHardReadOnly () :            
                if ReadOnlyState :
                    self.ROIDictionary.setSoftReadOnlyMode ()
                else:
                    self.ROIDictionary.trySetOffReadOnlyMode ()         
            self._getPaintToolsDlg().setButtonEnabledState (Select = True, Draw = not self.ROIDictionary.isReadOnly ())
            if self.ROIDictionary.isReadOnly () :
                self.setUIState ("SelectionArrow")
                self._getPaintToolsDlg ().setUIStateWidgetState ("SelectionArrow")
            if self._TranslationDlg is not None :
                self._TranslationDlg.setEnabled (not self.ROIDictionary.isReadOnly ())
            datasetTagEditor = self.getOpenCustomDatasetTagEditor ()
            if datasetTagEditor is not None :
                datasetTagEditor.setEnabled (not self.ROIDictionary.isReadOnly ())   
            if self._ML_ModelZooDlg is not None :
                self._ML_ModelZooDlg.setEnabled (not self.ROIDictionary.isReadOnly ())   
            self.updateUIToRefectSelectedROIHiddenLockedState ()
            self.updatePaintBrushIfNecessary ()            
            descriptionEnabled = self.isROIDictonaryFileLoaded () and not self.ROIDictionary.isReadOnly ()
            self.ui.scanPhaseLbl.setEnabled (descriptionEnabled)
            self.ui.ScanPhaseComboBox.setEnabled (descriptionEnabled)    
            self.ui.UndoBtn.setEnabled (self.isROIDictonaryFileLoaded () and self.ROIDictionary.getUndoStackSize () > 0 and not self.ROIDictionary.isReadOnly ())
            self.ui.RedoBtn.setEnabled (self.isROIDictonaryFileLoaded () and self.ROIDictionary.getRedoStackSize () > 0 and not self.ROIDictionary.isReadOnly ())    
            self.ContourListSelectionChanged (SetContourModeEnabled = False)
            self.updatePenSettingsDlg ()
            self._setROIOrientationChangeEnabled ()
        finally:
            self._setUIReadOnlyModeFired = False    
    
    def dictionaryChangedReadOnlyMode (self):
          self.setUIReadOnlyMode (self.ROIDictionary.isReadOnly ())
          
    def toggleReadOnlyMode (self) :
        self._LastReadOnlyMode = not self.ROIDictionary.isReadOnly ()
        self.setUIReadOnlyMode (self._LastReadOnlyMode)
        
    def aboutToShowML_Menu (self) :
        if (self.NiftiVolume is None or not ML_KerasModelLoaderInterface.isMLSupportEnabled ())  :
           self.ui.actionModel_manager.setEnabled (False)
           self.ui.actionModel_zoo.setEnabled (False)
           self.ui.actionQuantify_segmentation.setEnabled (False)
           self.ui.actionML_scan_description.setEnabled (False)
        else:
           self.ui.actionModel_manager.setEnabled (not self._ProjectDataset.isRunningInDemoMode ())
           self.ui.actionModel_zoo.setEnabled (not self.ROIDictionary.isReadOnly ())
           self.ui.actionQuantify_segmentation.setEnabled (self.isROIDictonaryFileLoaded ())
           enableSetDataSetDescription = False
           if self.ROIDictionary is not None :
               tags = self.ROIDictionary.getDataFileTagManager ()
               if (tags is not None and tags.hasInternalTag ("ML_DatasetDescription")) :
                   enableSetDataSetDescription = True
           self.ui.actionML_scan_description.setEnabled (enableSetDataSetDescription)
           
    
    def showMLDatasetDescriptionDlg (self) :
        if (self._MLDatasetDescriptionDlg is None) :
            self._MLDatasetDescriptionDlg = RCC_MLDatasetDescriptionDlg (self)
            self._MLDatasetDescriptionDlg.installEventFilter(self._ChildWidgetOverrideEventFilter)
        self._MLDatasetDescriptionDlg.intWindowSettings (self)
        self._MLDatasetDescriptionDlg.show ()
        
    def modelManager (self) :
        try :
            self.setEnabled (False)
            modelManager = RCC_ModelManagerDlg (self._ProjectDataset,self)        
            modelManager.exec_() 
            if (self._ML_ModelZooDlg is not None) :
                self._ML_ModelZooDlg.updateModelZooDialogModelList (UpdateTreeSelection = True)
            if (self._ML_QuantifySegmentatinDlg is not None) :
                self._ML_QuantifySegmentatinDlg.updateMLModelList (UpdateTreeSelection = True)
        finally:
            self.setEnabled (True)
        
    def modelZoo (self) :
        if self._ML_ModelZooDlg is None :
           self._ML_ModelZooDlg =  RCC_ModelZooDlg (self)                
        self._ML_ModelZooDlg.show ()
        self._ML_ModelZooDlg.raise_ ()
        return
    
    def quantifySegmentation (self) :
        if self._ML_QuantifySegmentatinDlg is None :
            self._ML_QuantifySegmentatinDlg = RCC_QuantifySegmentationDlg (self)        
        self._ML_QuantifySegmentatinDlg.show ()
        self._ML_QuantifySegmentatinDlg.raise_ ()
        return
    
        
    def listBoxPassThroughKeyEvent  (self, event) :
        KeyStrokes = ShortcutKeyList.getPassThroughKeyStrokes () 
        if (event.key () in KeyStrokes) : 
            print ("firing overriding key event")
            self.keyPressEvent (event)    
            print ("returning from")
        else:
            event.ignore ()
        
    
    def SaveToDB (self) :
        try:
            if (self.ROIDictionary is not None) :
                filehandle = self.ROIDictionary.getROIFileHandle ()
                if (filehandle is not None and filehandle.getHandleType () == "Tactic") :
                    if self.showFlattenDeepGrowDlg ("One-click generated segmentations must be flattened before series can be saved to the database. Do you wish to flatten the segmentation?") :
                        progdialog = QProgressDialog(self)
                        progdialog.setMinimumDuration (4)
                        progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)                
                        progdialog.setWindowTitle ("Backing up changes to database")            
                        progdialog.setWindowModality(QtCore.Qt.ApplicationModal)                
                        progdialog.setRange(0,0)                
                        progdialog.setMinimumWidth(300)
                        progdialog.setMinimumHeight(100)
                        try:
                            progdialog.forceShow()       
                            progdialog.setValue(0)
                            self._ProjectDataset.getApp ().processEvents ()
                            self.ROIDictionary.saveROIDictionaryToFile () # Save current ROI    
                            filehandle.save (DeleteOnSave = False)  
                        finally:
                            progdialog.close ()
                            self._ProjectDataset.getApp ().processEvents ()
        except:
            print ("A error occured trying to save to back to the database")
            
    def showAboutDlg (self) :
        if (self._ProjectDataset is not None) :
            self._ProjectDataset.getNiftiDatasetDlg().showAboutDlg (self)

    def showHelpDlg (self) :
        if (self._ProjectDataset is not None) :
            self._ProjectDataset.getNiftiDatasetDlg().showHelpDlg (self)
                
        
    def saveRILContourDebuggingState (self) :
        dlg= QFileDialog( self )
        dlg.setWindowTitle('Save RIL-Contour Debugging State' )
        dlg.setViewMode( QFileDialog.Detail )    
        dlg.setFileMode (QFileDialog.AnyFile)
        dlg.setNameFilters( ['dump file (*.dump)', 'any file (*)'] )                
        dlg.setAcceptMode (QFileDialog.AcceptSave)        
        dlg.selectFile ("RC_Debug_%s_%s.dump" % (TimeUtil.timeToSortableString (TimeUtil.getTimeNow ()), DateUtil.getSortableString(DateUtil.today ())))
        if (dlg.exec_() == QDialog.Accepted) and (len (dlg.selectedFiles())== 1):            
            progdialog = QProgressDialog(self)
            progdialog.setMinimumDuration (0)
            progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)                
            progdialog.setWindowTitle ("Saving RIL-Contour debug dump")            
            progdialog.setWindowModality(QtCore.Qt.ApplicationModal)                
            progdialog.setRange(0,0)                
            progdialog.setMinimumWidth(300)
            progdialog.setMinimumHeight(100)
            progdialog.forceShow()  
            try:
                progdialog.setValue(0)
                self._ProjectDataset.getApp ().processEvents ()            
                exportPath = dlg.selectedFiles()[0]                  
                self.saveStateForDebugging (exportPath)
            except:
                pass  
            finally:   
                progdialog.close ()
                self._ProjectDataset.getApp ().processEvents ()
    
    def saveStateForDebugging (self, path) :
        ProjectFileLockInterfaceLocksSaved = False
        if self.ROIDictionary._ProjectFileLockInterface is not None :
            lock1, lock2 = self.ROIDictionary._ProjectFileLockInterface.debugGetRLocks ()
            self.ROIDictionary._ProjectFileLockInterface.debugSetRLocks (None, None)        
            ProjectFileLockInterfaceLocksSaved = True        
        filesystemHandleState = None
        pescaraFileHandle = None
        if self.ROIDictionary._filHandle is not None :
            if self.ROIDictionary._filHandle.getHandleType () == "FileSystem" :
               filesystemHandleState = self.ROIDictionary._filHandle.setToDebug () 
            else:
               pescaraFileHandle = self.ROIDictionary._filHandle
               self.ROIDictionary._filHandle = None        
        dataset = self.ROIDictionary._ProjectDataSet
        self.ROIDictionary._ProjectDataSet = None
        cl = self.ROIDictionary._changeListener
        self.ROIDictionary._changeListener = []
        unredo = self.ROIDictionary._undoRedoChangeListener
        self.ROIDictionary._undoRedoChangeListener = []
        readonly = self.ROIDictionary._readOnlyChangeListener
        self.ROIDictionary._readOnlyChangeListener = []
        
        roiDefsSaved = False
        ROIDefs_ProjectFileInterface_Locks_Saved  = False
        if self.ROIDictionary._ROIDefs is not None :
            roiDefsSaved = True
            defschl = self.ROIDictionary._ROIDefs._changeListener
            defsel = self.ROIDictionary._ROIDefs._selectionListener
            self.ROIDictionary._ROIDefs._changeListener = []
            self.ROIDictionary._ROIDefs._selectionListener = []   
            
            if self.ROIDictionary._ROIDefs._ProjectFileInterface is not None and not ProjectFileLockInterfaceLocksSaved :
                lock1, lock2 =  self.ROIDictionary._ROIDefs._ProjectFileInterface.debugGetRLocks ()
                self.ROIDictionary._ROIDefs._ProjectFileInterface.debugSetRLocks (None, None)        
                ROIDefs_ProjectFileInterface_Locks_Saved = True
                
        hfs = self.ROIDictionary._Hounsfieldsettings
        vs = self.ROIDictionary._VisSettings
        self.ROIDictionary._Hounsfieldsettings = None
        self.ROIDictionary._VisSettings = None                
        try :
            import pickle 
            with open (path,"wb") as outfile :
                pickle.dump (self.ROIDictionary, outfile)
            print ("RilContour state dump saved to: " + path)        
        except:
            print ("A error occured creating the RILContour state dump")
        finally:
            if filesystemHandleState is not None :
                self.ROIDictionary._filHandle.restoreFromDebug (filesystemHandleState) 
            else:
                self.ROIDictionary._filHandle = pescaraFileHandle            
            if ProjectFileLockInterfaceLocksSaved :
                self.ROIDictionary._ProjectFileLockInterface.debugSetRLocks (lock1, lock2)
            self.ROIDictionary._ProjectDataSet = dataset            
            self.ROIDictionary._readOnlyChangeListener = readonly
            self.ROIDictionary._undoRedoChangeListener = unredo
            self.ROIDictionary._changeListener = cl
            if roiDefsSaved :
                self.ROIDictionary._ROIDefs._selectionListener = defsel
                self.ROIDictionary._ROIDefs._changeListener = defschl
                if ROIDefs_ProjectFileInterface_Locks_Saved :
                    self.ROIDictionary._ROIDefs._ProjectFileInterface.debugSetRLocks (lock1, lock2)        
                    
            self.ROIDictionary._Hounsfieldsettings = hfs
            self.ROIDictionary._VisSettings = vs
        
    def _moveSelectedROIUp (self) :
        ROIList = self.ROIDefs.getSelectedROI ()
        if (len (ROIList) > 0) :
            self.ROIDefs.moveROIUp (ROIList[0])
    
    def _moveSelectedROIDown (self) :
        ROIList = self.ROIDefs.getSelectedROI ()
        if (len (ROIList) > 0) :
            self.ROIDefs.moveROIDown (ROIList[0])
    
    def setWindowUpdateEnabled (self, stat) :        
        self.ui.XYSliceView.setWindowUpdateEnabled (stat)
        self.ui.XZSliceView.setWindowUpdateEnabled (stat)
        self.ui.YZSliceView.setWindowUpdateEnabled (stat)
        self.ui.XYSliceSelectorWidget.setWindowUpdateEnabled (stat)
        self.ui.XYSliceVerticalSelector.setWindowUpdateEnabled (stat)
        self.ui.XZSliceVerticalSelector.setWindowUpdateEnabled (stat)
        self.ui.YZSliceVerticalSelector.setWindowUpdateEnabled (stat)
        if self._windowUpdateEnabled != stat :
            self._windowUpdateEnabled = stat 
            if (stat and self._pendingWindowUpdate) :
                self.update ()                
    
    def update (self,x1 = None, x2 = None, x3 = None, x4 = None) :
        if self._windowUpdateEnabled :
            if (x2 is not None) :
                super(RCC_ContourWindow, self).update (x1, x2, x3, x4)
            elif (x1 is not None) :
                super(RCC_ContourWindow, self).update (x1)
            else:
                super(RCC_ContourWindow, self).update ()
            self._pendingWindowUpdate = False
        else:
            self._pendingWindowUpdate = True
            
    def toggleSelectedROIShowHide (self, SetState) :
        selROIList = self.ROIDefs.getSelectedROI ()
        if self.NiftiVolume is None :
            SliceShape = None
        else:
            SliceShape = self.NiftiVolume.getSliceDimensions ()
            SliceShape = (int (SliceShape[0]), int (SliceShape[1]))
        for roiName in selROIList :
            if SetState is None :
                isHidden = self.ROIDefs.isROIHidden (roiName)
                self.ROIDefs.setROIHidden (roiName, not isHidden, AutoLock = self.visSettings.shouldAutoLockROIWhenHidden ())
            else:
                self.ROIDefs.setROIHidden (roiName, SetState, AutoLock = self.visSettings.shouldAutoLockROIWhenHidden ())
            if SliceShape is not None :
                self.ROIDictionary.updateDeepGrowLockedObjectClipMaskForROI (roiName, SliceShape)
        self.setROIHiddenLabel ()
        self.updateUIToRefectSelectedROIHiddenLockedState ()
        self.update ()
    
    def updateUIToRefectSelectedROIHiddenLockedState (self):
        #self.ROIListSelectionChanged ()
        self._UpdateROISliceUIandSelection ()
        nameLst = self.ROIDefs.getSelectedROI ()   # get currently selected ROI (Name)   
        ClearSelectedBtnEnabled = False
        PaintToolsEnabled = False
        if not self.ROIDictionary.isReadOnly () :
            if self.isROIDictonaryFileLoaded ()  :
                for name in nameLst :                
                    if (not PaintToolsEnabled and name != "None" and not self.ROIDefs.isROIHidden (name) and not self.ROIDefs.isROILocked (name)):
                        PaintToolsEnabled = True
                    if (not ClearSelectedBtnEnabled and name != "None" and not self.ROIDefs.isROILocked (name) and self.ROIDictionary.isROIDefined (name)):
                        ClearSelectedBtnEnabled = True   
                    if ClearSelectedBtnEnabled and PaintToolsEnabled :
                        break
        self.ui.ClearSelectedBtn.setEnabled (ClearSelectedBtnEnabled) 
        self.updatePaintBrushIfNecessary ()
        self._getPaintToolsDlg ().updateUIStateToRefectSelectedROIEditingState (PaintToolsEnabled and len (nameLst) == 1 and not self.ROIDictionary.isReadOnly ())
        
        
    def toggleSelectedROILocked (self, SetState) :
        selROIList = self.ROIDefs.getSelectedROI ()
        if self.NiftiVolume is None :
            SliceShape = None
        else:
            SliceShape = self.NiftiVolume.getSliceDimensions ()
            SliceShape = (int (SliceShape[0]), int (SliceShape[1]))
        for roiName in selROIList :
            if SetState is None :
                isLocked = self.ROIDefs.isROILocked (roiName)
                self.ROIDefs.setROILocked (roiName, not isLocked)
            else:
                self.ROIDefs.setROILocked (roiName, SetState)
            if SliceShape is not None :
                self.ROIDictionary.updateDeepGrowLockedObjectClipMaskForROI (roiName, SliceShape)
        self.setROIHiddenLabel ()
        self.updateUIToRefectSelectedROIHiddenLockedState ()
        self.update ()
            
    def _isSelectedROIShown (self) :
        selROIList = self.ROIDefs.getSelectedROI ()
        for roiName in selROIList :
            isHidden = self.ROIDefs.isROIHidden (roiName)
            if not isHidden :
                return True
        return False
    
    def _isSelectedROIHidden (self) :
        selROIList = self.ROIDefs.getSelectedROI ()
        for roiName in selROIList :
            isHidden = self.ROIDefs.isROIHidden (roiName)
            if isHidden :
                return True
        return False
    
    def _isSelectedROILocked (self) :
        selROIList = self.ROIDefs.getSelectedROI ()
        for roiName in selROIList :
            isLocked = self.ROIDefs.isROILocked (roiName)
            if isLocked :
                return True
        return False
   
    def _isSelectedROIUnLocked (self) :
        selROIList = self.ROIDefs.getSelectedROI ()
        for roiName in selROIList :
            isLocked = self.ROIDefs.isROILocked (roiName)
            if not isLocked :
                return True
        return False
    
    def hasDeepGrowAnnotations (self, ROIName = None) :
        return self.ROIDictionary.hasDeepGrowAnnotations (ROIName)
    
    def hasOtherDeepGrowAnnotations (self, ROIName) :
        return self.ROIDictionary.hasOtherDeepGrowAnnotations (ROIName)
        
    def showFlattenDeepGrowDlg (self, Msg = None, Cancel = True, SaveUndoPoint = True, CallDictionaryChangeListener = True, ReturnSaveUndoPointCalled = False) :
        SaveUndoPointCalled = False
        if self.NiftiVolume is not None and not self.ROIDictionary.isReadOnly () : 
            objNameWithDeepGrowAnnotationList = self.ROIDictionary.getROIListWithDeepGrowAnnotations ()
            if (len (objNameWithDeepGrowAnnotationList) > 0):
                ROIFlattened = False
                for objName in objNameWithDeepGrowAnnotationList :     
                    if self.getPenSettings ().getAutoAcceptOneClickSegmentation (self.ROIDefs.getROIIDNumber (objName)) : 
                       if self.ROIDictionary.flattenDeepGrowAnnotations (objName, self.NiftiVolume, self._ProjectDataset, SaveUndoPoint = SaveUndoPoint, CallDictionaryChangeListener = False) :
                           ROIFlattened = True 
                           if SaveUndoPoint:
                               SaveUndoPointCalled = True
                           SaveUndoPoint = False
                           
                objNameWithDeepGrowAnnotationList = self.ROIDictionary.getROIListWithDeepGrowAnnotations ()
                if (len (objNameWithDeepGrowAnnotationList) > 0):
                    Dlg = ShouldFlattenDeepGrowAnnotationDlg (self, Msg= Msg, Cancel = Cancel)
                    Dlg.exec_ ()
                    ShouldFlattenAnnotation = Dlg.getResult () 
                    if ShouldFlattenAnnotation == "Cancel" :
                        if ReturnSaveUndoPointCalled :
                            return False, SaveUndoPointCalled
                        else:
                             return False
                    for objName in objNameWithDeepGrowAnnotationList :                
                        if ShouldFlattenAnnotation == "Flatten" :
                            if self.ROIDictionary.flattenDeepGrowAnnotations (objName, self.NiftiVolume, self._ProjectDataset, SaveUndoPoint = SaveUndoPoint, CallDictionaryChangeListener = False) :
                                if SaveUndoPoint:
                                    SaveUndoPointCalled = True
                                SaveUndoPoint = False
                                ROIFlattened = True 
                        elif ShouldFlattenAnnotation == "Clear" :
                            if self.ROIDictionary.clearDeepGrowAnnotations (objName, SaveUndoPoint = SaveUndoPoint, CallDictionaryChangeListener = False) :
                                if SaveUndoPoint:
                                     SaveUndoPointCalled = True
                                SaveUndoPoint = False
                                ROIFlattened = True
                if ROIFlattened :
                    if CallDictionaryChangeListener :
                        self.ROIDictionary._CallDictionaryChangedListner ()
                    self.updatePenSettingsDlgsOneClickROI ()
                    self.update ()   
        if ReturnSaveUndoPointCalled :
           return True, SaveUndoPointCalled  
        return True
            
    def setROISelection (self, selection) :
        if selection == "None" :
            self.ROIDefs.clearSelectedROI(CallSelectionChangeListener = True)
        elif selection == "All" :
            self.ROIDefs.selectAll()
        elif selection == "Toggle" :
            self.ROIDefs.toggleSelection()
        try:        
            self.ui.ROI_List.selectionModel().selectionChanged.disconnect(self.ROIListSelectionChanged)  
            ReconnectROIListSelectionChanged = True
        except :
            ReconnectROIListSelectionChanged = False
        for index in range (self.ui.ROI_List.count ()) :
            roiListItem = self.ui.ROI_List.item(index)
            isROISelected = self.ROIDefs.isROINameSelected (roiListItem.text ())
            roiListItem.setSelected (isROISelected)
            if isROISelected :
                 self.ui.ROI_List.setCurrentItem (roiListItem)
        if ReconnectROIListSelectionChanged :
            self.ui.ROI_List.selectionModel().selectionChanged.connect(self.ROIListSelectionChanged)  
            
    def showContextMenu(self, pos) :
        globalPos = self.ui.ROI_List.mapToGlobal(pos)        
        myMenu = QMenu(self)
        try:
            selROIList = self.ROIDefs.getSelectedROI ()
            if (selROIList is None or len (selROIList) == 0 or "None" in selROIList) :
                isNoneSelected  = True
            else:
                isNoneSelected  = False
        except:
            isNoneSelected  = False
        
        selectNone = QAction ("Select None",self)
        selectNone.setEnabled (not isNoneSelected)
        selectNone.triggered.connect (functools.partial (self.setROISelection, "None"))        
        myMenu.addAction (selectNone)
        
        selectAll = QAction ("Select All",self)
        selectAll.setEnabled (self.ROIDefs.count () > 0 and not self.ROIDefs.areAllROISelected ())
        selectAll.triggered.connect (functools.partial (self.setROISelection, "All"))        
        myMenu.addAction (selectAll)
        
        toggleROISelection = QAction ("Toggle Selection",self)
        toggleROISelection.setEnabled (self.ROIDefs.count () > 0)
        toggleROISelection.triggered.connect (functools.partial (self.setROISelection,"Toggle"))        
        myMenu.addAction (toggleROISelection)
                
        myMenu.addSeparator()
        hide = QAction ("Hide",self)
        hide.setEnabled (not isNoneSelected and self._isSelectedROIShown () and self.isROIDictonaryFileLoaded ())
        hide.triggered.connect (functools.partial (self.toggleSelectedROIShowHide, True))        
        myMenu.addAction (hide)
        
        show = QAction ("Show",self)
        show.setEnabled (not isNoneSelected and self._isSelectedROIHidden () and self.isROIDictonaryFileLoaded ())
        show.triggered.connect (functools.partial (self.toggleSelectedROIShowHide, False))        
        myMenu.addAction (show)
        
        toggle = QAction ("Toggle Hide/Show",self)
        toggle.setEnabled (not isNoneSelected and self.isROIDictonaryFileLoaded ())
        toggle.triggered.connect (functools.partial (self.toggleSelectedROIShowHide, None))        
        myMenu.addAction (toggle)
        
        myMenu.addSeparator()
        hide = QAction ("Lock",self)
        hide.setEnabled (not isNoneSelected  and self._isSelectedROIUnLocked () and self.isROIDictonaryFileLoaded ())
        hide.triggered.connect (functools.partial (self.toggleSelectedROILocked, True))        
        myMenu.addAction (hide)
        
        show = QAction ("Unlock",self)
        show.setEnabled (not isNoneSelected and  self._isSelectedROILocked () and self.isROIDictonaryFileLoaded ())
        show.triggered.connect (functools.partial (self.toggleSelectedROILocked, False))        
        myMenu.addAction (show)
        
        show = QAction ("Toggle Lock/Unlock",self)
        show.setEnabled (not isNoneSelected and self.isROIDictonaryFileLoaded ())
        show.triggered.connect (functools.partial (self.toggleSelectedROILocked, None))      
        myMenu.addAction (show)
        
        myMenu.addSeparator()
        clearROI = QAction ("Clear ROI",self)
        clearROI.setEnabled (self.ui.ClearSelectedBtn.isEnabled ())
        clearROI.triggered.connect (self.RemoveROIBtn)        
        myMenu.addAction (clearROI)
        myMenu.exec_(globalPos)
        
        
    def ROIDictionaryUndoRedo (self, SelectedSlice = None) :    
        if (self.ROIDictionary is not None) :
            if SelectedSlice is not None :                 
                if self.SelectedCoordinate.getCoordinate() is not None :
                    currentSelectedSlice = self.SelectedCoordinate.getZCoordinate()
                    if (currentSelectedSlice is not None and currentSelectedSlice != SelectedSlice) :
                        self.SelectedCoordinate.setCoordinate ([self.SelectedCoordinate.getXCoordinate(), self.SelectedCoordinate.getYCoordinate(), SelectedSlice])
                        self.setEnabled (False)
                        self.update ()                        
                        self._ProjectDataset.getApp ().processEvents ()
                        time.sleep (0.3)       
                        self.setEnabled (True)
                                                
            else:
                obj = self.ROIDictionary.getSelectedObject ()
                if obj is not None :
                    cord = obj.getCoordinate ()
                    if (cord is not None) :  
                        zcord = cord.getZCoordinate ()
                        if self.SelectedCoordinate.getCoordinate() is not None :
                            currentSelectedSlice = self.SelectedCoordinate.getZCoordinate()
                            if (currentSelectedSlice is not None and currentSelectedSlice != zcord) :
                                self.SelectedCoordinate.setCoordinate ([self.SelectedCoordinate.getXCoordinate(), self.SelectedCoordinate.getYCoordinate(), zcord])
            ROIWithDeepGrowAnnotations = self.ROIDictionary.getROIListWithDeepGrowAnnotations ()
            if len (ROIWithDeepGrowAnnotations) > 0 :
                self._getPaintToolsDlg ()._deepGrowToolClick ()
               
    
    # Import ROI From Mask
    def importROIFromMask (self, NiftiPath = None) :  
        if (self.ROIDictionary is not None) :              
            importMaskDialog = RCC_ROIMaskImportDlg (self._ProjectDataset, self.ROIDefs, self, importSingleMask = True, NiftiPath = NiftiPath)
            if (importMaskDialog.exec_()) :
                tup = importMaskDialog.getReturnValue ()   
                if tup is not None :
                    flattenedDeepGrow, SaveUndoPoint = self.showFlattenDeepGrowDlg ("One-click generated segmentations must be flattened before a mask can be imported. Do you wish to flatten the segmentation?", SaveUndoPoint = True, CallDictionaryChangeListener = False, ReturnSaveUndoPointCalled  = True) 
                    if flattenedDeepGrow :
                        maskFilePath, roiMaskMappings, importMethod, doNotOverwriteROI, MaskedDataPerefered, ExistingDataPrefered   = tup                            
                        method = importMethod # "OrMask" # "AndMask"
                        if (self.ROIDictionary.ROICountDefined () == 0) :
                            self.ROIDictionary.setROIAreaMode ()
                        ExcludeExistingMaskedAreaFromImport = ExistingDataPrefered
                        RemoveExistingMaskedAreaFromOverlapping = MaskedDataPerefered
                        
                        progdialog = QProgressDialog(" ", "Cancel", int (0), int (0), self)  
                        progdialog.setMinimumDuration (0)
                        progdialog.setRange (0,100)
                        progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog | QtCore.Qt.WindowStaysOnTopHint)          
                        progdialog.setWindowModality(QtCore.Qt.ApplicationModal)       
                        progdialog.setWindowTitle ("Importing ROI Mask")                           
                        try:                  
                            progdialog.forceShow()                         
                            progdialog.setValue(0)
                            self._ProjectDataset.getApp ().processEvents ()            
                            self.ROIDictionary.versionDocument ("File before importing ROIFileMasks from: " + maskFilePath)
                            self.ROIDictionary.CreateDatasetROIFromMaskForDictionary (roiMaskMappings, maskFilePath, method, doNotOverwriteROI, niftiVolumeToMatchOrientation = self.NiftiVolume, ExcludeExistingMaskedAreaFromImport = ExcludeExistingMaskedAreaFromImport, RemoveExistingMaskedAreaFromOverlapping = RemoveExistingMaskedAreaFromOverlapping, ProgressDialog = progdialog,  FileSaveThreadManger = self._ProjectDataset.getROISaveThread (), ProjectDataset = self._ProjectDataset, SaveUndoPoint = not SaveUndoPoint, ProgressDialogValueRange = (0,100))                        
                            self.ROIDictionary.versionDocument ("File after importing ROIFileMasks from: " + maskFilePath)
                            
                            HasData = (self.ROIDictionary.ROICountDefined () > 0 or self.ROIDictionary.isScanPhaseDefined ())                                                                             
                            if (self.NiftiVolumeTreeSelection is not None) :
                                self.NiftiVolumeTreeSelection.setROIDatasetIndicator (HasData)
                        finally:
                            progdialog.close ()
                            self._ProjectDataset.getApp ().processEvents ()            
        
    def canImportROIFromMask (self) :
        canimportROIFromMask = False     
        canexportROIFromMask = False                               
        if (self.NiftiVolume is not None and self.getLoadedNIfTIFile() is not None and self.isROIDictonaryFileLoaded ()) : # and self.NiftiVolumeTreeSelection.getPesscaraInterace () == None
            canexportROIFromMask = True
            if not self.ROIDictionary.isReadOnly () :
                canimportROIFromMask = True                               
        self.ui.actionROI_From_NIfTI_Mask.setEnabled (canimportROIFromMask)        
        self.ui.menuExport.setEnabled (canexportROIFromMask)
        return canimportROIFromMask
    
    def aboutToShowFileMenu (self) :
        self.ui.SetReadOnly.setEnabled (self.isROIDictonaryFileLoaded () and not self.ROIDictionary.isHardReadOnly () and self.NiftiVolume is not None)
        if self.ROIDictionary.isReadOnly () :
            self.ui.SetReadOnly.setText ("Set edit mode")
        else:
            self.ui.SetReadOnly.setText ("Set read-only mode")
        self.ui.actionSave_to_DB.setEnabled (False)
        self.ui.createFileVersion.setEnabled (False)
        self.ui.RollBackFileVersion.setEnabled (False)
        
        self.ui.CheckoutFileLock.setEnabled (False)
        self.ui.CheckInfileLock.setEnabled (False)
        self.ui.Assign_file_lock.setEnabled (False)
        self.ui.View_lock_history.setEnabled (False)
        self.ui.actionView_version_history.setEnabled (False)
        self.ui.actionRevert_checkout.setEnabled (False)
        self.ui.actionExportSeriesAnnotations.setEnabled (False)
        DemoMode = self._ProjectDataset.isRunningInDemoMode ()        
        try:
            if (self.ROIDictionary is not None and self.isROIDictonaryFileLoaded ()) :
                tagManager =  self.getWindowTagManger ()
                self.ui.actionExportSeriesAnnotations.setEnabled (tagManager is not None)
                filehandle = self.ROIDictionary.getROIFileHandle ()
                if (filehandle is not None) :
                    if (not self.ROIDictionary.isReadOnly() or self._ProjectDataset.isInSuperUserCommandLineMode()) :
                        if (filehandle.getHandleType () == "Tactic") :
                            self.ui.actionSave_to_DB.setEnabled (self.isROIDictonaryFileLoaded ())
                        if filehandle.canVersionFile () :
                            self.ui.createFileVersion.setEnabled ((filehandle.ownsPersistentFileLock () or filehandle.isPLockAvailable ()) and not DemoMode)
                            try :
                                self.ui.RollBackFileVersion.setEnabled ((len (filehandle.getFileVersionHistory ()) > 0 and (filehandle.ownsPersistentFileLock () or filehandle.isPLockAvailable ())) and not DemoMode)
                            except:
                                pass
                            
                            AccessDataAdminMode = (self._ProjectDataset.isInSuperUserCommandLineMode() and filehandle.otherUserHoldsPersistentFileLock ())
                            
                            self.ui.CheckoutFileLock.setEnabled ((filehandle.isPLockAvailable () or AccessDataAdminMode) and not DemoMode)
                            self.ui.CheckInfileLock.setEnabled (filehandle.ownsPersistentFileLock () and not DemoMode)
                            self.ui.actionRevert_checkout.setEnabled ((filehandle.ownsPersistentFileLock () or AccessDataAdminMode) and not DemoMode)
                            self.ui.Assign_file_lock.setEnabled (filehandle.canTransferPersistentFileLockToOtherUserOrGroup () and not DemoMode)
                    if (filehandle.getHandleType () != "Tactic") :
                        self.ui.View_lock_history.setEnabled ((filehandle.hasPLockDataFile (IgnoreNonPersistantFileLock = True) and len (filehandle.getPersistentLockHistory ()) > 0) and not DemoMode)
                        if filehandle.canVersionFile () :
                            self.ui.actionView_version_history.setEnabled ((len (filehandle.getFileVersionHistory ()) > 0) and not DemoMode)
        except:
            self.ui.actionSave_to_DB.setEnabled (False)                            
        return self.canImportROIFromMask ()
    
    def isMultiUserMode (self) :
        try :
            return self._ProjectDataset.getProjectFileInterface().isInMultiUserMode ()
        except:
            return False
    
    def needsToAcquirePLockToSave (self):
        try:
            if self._ProjectDataset.isRunningInDemoMode () :
                return False
            if (self.ROIDictionary is not None and self.isROIDictonaryFileLoaded ()) :
                filehandle = self.ROIDictionary.getROIFileHandle ()
                if (filehandle is not None) :
                    if (not self.ROIDictionary.isReadOnly() or self._ProjectDataset.isInSuperUserCommandLineMode()) :
                        if (filehandle.getHandleType () == "Tactic") :
                            return False
                        if filehandle.canVersionFile () :
                            if filehandle.ownsPersistentFileLock () :
                                return False
                            elif filehandle.otherUserHoldsPersistentFileLock () :
                                return True
                            elif (self._ProjectDataset.getProjectFileInterface().isInMultiUserMode ()) :  
                                return True
                            return False
            return False
        except:
            return False
        
    def canTryToAcquirePLock (self):
        try:
            if (self.ROIDictionary is not None and self.isROIDictonaryFileLoaded ()) :
                filehandle = self.ROIDictionary.getROIFileHandle ()
                if (filehandle is not None) :
                    if (not self.ROIDictionary.isReadOnly() or self._ProjectDataset.isInSuperUserCommandLineMode()) :
                        if (filehandle.getHandleType () == "Tactic") :
                            return False
                        if filehandle.canVersionFile () :
                            if filehandle.ownsPersistentFileLock () :
                                return False
                            AccessDataAdminMode = (self._ProjectDataset.isInSuperUserCommandLineMode() and filehandle.otherUserHoldsPersistentFileLock ())
                            return filehandle.isPLockAvailable () or AccessDataAdminMode
            return False
        except:
            return False
        
        
    def revertPLockCheckout (self) : #  self.ui.actionRevert_checkout.triggered.connect (self.revertPLockCheckout):
        if self._ProjectDataset.isRunningInDemoMode () :
            return
        if (self.ROIDictionary is not None and (not self.ROIDictionary.isReadOnly() or self._ProjectDataset.isInSuperUserCommandLineMode())) :
            filehandle = self.ROIDictionary.getROIFileHandle ()
            if (filehandle is not None) :
                if filehandle.canVersionFile () :
                    if self.showFlattenDeepGrowDlg ("One-click generated segmentations must be flattened before an image can be reverted. Do you wish to flatten the segmentation?") :
                        self.ROIDictionary.setSoftReadOnlyMode ()
                        if filehandle.ownsPersistentFileLock () :
                            if MessageBoxUtil.showWarningMessageBox ("Reverting Changes", "Reverting changes will remove all changes made since the file was checked out. Click 'Yes' to revert the file.") :
                                if filehandle.revertPersistentFileLock ()  :
                                    self.reloadROIDefs ()
                                    return
                        elif self._ProjectDataset.isInSuperUserCommandLineMode() and filehandle.otherUserHoldsPersistentFileLock () :
                            if MessageBoxUtil.showWarningMessageBox ("Reverting Changes", "Reverting changes will remove all changes made since the file was checked out. Click 'Yes' to revert the file.") :
                                if MessageBoxUtil.showWarningMessageBox  ("Administrator Override","Another user currently holds the file lock. Do you really wish to revert the file lock?") :
                                    if filehandle.revertPersistentFileLock (SuperUserOverride = True)  :
                                        self.reloadROIDefs ()
                                        return
                        self.ROIDictionary.trySetOffReadOnlyMode ()
                            
    def assignPLock (self) : 
        if self._ProjectDataset.isRunningInDemoMode () :
            return
        if (self.ROIDictionary is not None and (not self.ROIDictionary.isReadOnly() or self._ProjectDataset.isInSuperUserCommandLineMode())) :
            filehandle = self.ROIDictionary.getROIFileHandle ()
            if (filehandle is not None) :
                if filehandle.canVersionFile () and (filehandle.canTransferPersistentFileLock () or self._ProjectDataset.isInSuperUserCommandLineMode() and filehandle.otherUserHoldsPersistentFileLock ()):   
                    if self.showFlattenDeepGrowDlg ("One-click generated segmentations must be flattened before an image can be assigned. Do you wish to flatten the segmentation?") :
                        assignFile = True 
                        if self._ProjectDataset.isInSuperUserCommandLineMode() and filehandle.otherUserHoldsPersistentFileLock () :
                            if not MessageBoxUtil.showWarningMessageBox  ("Administrator Override","Another user currently holds the file lock. Do you really wish to re-assign the file lock?") :
                                  assignFile = False  
                        if assignFile :
                            succeed, result = FilePLockUI.showAssignPLockDlg (self, filehandle, IncludeSelf = True) 
                            if succeed : 
                                AssignToUserName, comment = result 
                                fileShaingManager = self._ProjectDataset.getProjectFileInterface ().getMultiUserFileSharingManager ()
                                if fileShaingManager.correctNickNameIfNecessary(AssignToUserName) == self._ProjectDataset.getUserName () :                                
                                    self.checkoutPLock ()
                                else:
                                    if self._ProjectDataset.isInSuperUserCommandLineMode() and filehandle.otherUserHoldsPersistentFileLock () :
                                        filehandle.forceCloseOpenPLocks ()                                                                
                                    if filehandle.transferPersistentFileLock (AssignToUserName = AssignToUserName, Comment = comment) :
                                        self.reloadROIDefs ()                                  
                                    
                         
    def checkInPLock (self) : 
        if self._ProjectDataset.isRunningInDemoMode () :
            return
        if (self.ROIDictionary is not None and not self.ROIDictionary.isReadOnly()) :
            filehandle = self.ROIDictionary.getROIFileHandle ()
            if (filehandle is not None) :
                if filehandle.canVersionFile () and filehandle.ownsPersistentFileLock () :
                    if self.showFlattenDeepGrowDlg ("One-click generated segmentations must be flattened before an image can be checked in. Do you wish to flatten the segmentation?") :
                        succeed, comment = FilePLockUI.showCheckinPLockDialog (self)
                        if succeed : 
                            filehandle.releasePersistentFileLock (comment)                            
                         
    def checkoutPLock (self) : 
        if self._ProjectDataset.isRunningInDemoMode () :
            return
        if (self.ROIDictionary is not None and (not self.ROIDictionary.isReadOnly() or self._ProjectDataset.isInSuperUserCommandLineMode())) :
            filehandle = self.ROIDictionary.getROIFileHandle ()
            if (filehandle is not None and filehandle.canVersionFile ()) :
                if self.showFlattenDeepGrowDlg ("One-click generated segmentations must be flattened before a image can be checked out. Do you wish to flatten the segmentation?") :
                    forcedAquire = False
                    if self._ProjectDataset.isInSuperUserCommandLineMode() and filehandle.otherUserHoldsPersistentFileLock () :
                        if MessageBoxUtil.showWarningMessageBox  ("Administrator Override","Another user currently holds the file lock. Do you really wish to acquire the file lock?") :
                            filehandle.forceCloseOpenPLocks ()
                            forcedAquire = True
                    if filehandle.isPLockAvailable () :   
                        filehandle.getPersistentFileLock () 
                        if forcedAquire :
                           self.reloadROIDefs ()  
                    
    def viewPLockHistory (self) :
        if self._ProjectDataset.isRunningInDemoMode () :
            return
        if (self.ROIDictionary is not None and self.isROIDictonaryFileLoaded ()) :
            filehandle = self.ROIDictionary.getROIFileHandle ()
            if (filehandle is not None) :
                 if (filehandle.getHandleType () != "Tactic") :   
                     FilePLockUI.showFilePLockHistoryDlg (self, filehandle)
                    
    
    def viewVersionHistory (self) :
        if self._ProjectDataset.isRunningInDemoMode () :
            return
        if (self.ROIDictionary is not None) :
            filehandle = self.ROIDictionary.getROIFileHandle ()
            if (filehandle is not None) :
                if filehandle.canVersionFile () :
                    FileVersionUI.showRollBackFileDlg (self, filehandle, RollBack=False)
                    
                    
    def versionFile (self, temp=None, Comment = None) :
        if self._ProjectDataset.isRunningInDemoMode () :
            return
        if (self.ROIDictionary is not None and not self.ROIDictionary.isReadOnly()) :
            filehandle = self.ROIDictionary.getROIFileHandle ()
            if (filehandle is not None) :
                if filehandle.canVersionFile () :
                    if self.showFlattenDeepGrowDlg ("One-click generated segmentations must be flattened before a file can be versioned. Do you wish to flatten the segmentation?") :
                        if filehandle.ownsPersistentFileLock () :   
                            if Comment is None :
                                OkPressed, Comment = FileVersionUI.showCreateVersionFileDlg (self)
                            else:
                                OkPressed = True
                            if OkPressed :
                                filehandle.versionFile (Comment, ForceVersion = True)
                        elif (not self.isMultiUserMode ()) :
                            if Comment is None :
                                OkPressed, Comment = FileVersionUI.showCreateVersionFileDlg (self)
                            else:
                                OkPressed = True
                            if OkPressed :
                                if filehandle.getPersistentFileLock (VersionComment = Comment, ForceVersion = True)  :                                
                                    filehandle.releasePersistentFileLock (Comment) 
                            
                            
    def rollBackFile (self) :
        if self._ProjectDataset.isRunningInDemoMode () :
            return
        if (self.ROIDictionary is not None and not self.ROIDictionary.isReadOnly()) :
            filehandle = self.ROIDictionary.getROIFileHandle ()
            if (filehandle is not None) :
                if filehandle.canVersionFile () :
                    if self.showFlattenDeepGrowDlg ("One-click generated segmentations must be flattened before a file can be rolled back. Do you wish to flatten the segmentation?") :
                        if filehandle.ownsPersistentFileLock () :   
                            releaseFilePLock = False
                        else:
                            if filehandle.getPersistentFileLock ()  :
                                releaseFilePLock = True
                            else:
                                return
                        PreviousVersion = FileVersionUI.showRollBackFileDlg (self, filehandle, RollBack=True)
                        if PreviousVersion is not None :
                            self.ROIDictionary.setSoftReadOnlyMode ()
                            filehandle.syncToPreviousFileVersion (PreviousVersion, Comment = "File rolled back to previous version")
                        if releaseFilePLock :
                            filehandle.releasePersistentFileLock (Comment = "File rolled back to previous version") 
                        if PreviousVersion is not None :
                            self.reloadROIDefs ()
                   
                            
    
    def _getObjectListDrawOrder (self, tpl) :
        return self.ROIDefs.getROIIdNumberOrder (tpl[0].getROIDefIDNumber ())
    #start ROI copy and paste functionatiy

    # majority of the lodgic for copy and a pasting is in Paste
    # paste, simply copies ROI data across paste does not take voxel dimensions, image orientation into account
    # roi are not transformed. IE going from small voxels to large voxels is not taken into account.
    # primary use is for coping roi between volumes with the same orientation and dimensions
    def pasteROI (self) :        
        if (self.canPasteROI () and self.ROIDictionary is not None) :            
            NewROIDefAdded = False
            ROIDefList = []
            ROIList = self.ROIDefs.getSelectedROI () 
            SelectedROIName = "None"
            skipROIList = []
            if len(ROIList) > 0 :
                SelectedROIName = ROIList[0]
                
            for copiedROIIndex in range (self._ProjectDataset.getInternalClipboard ().getCopiedROICount ())  :                
                newROIDef, _, _ = self._ProjectDataset.getInternalClipboard ().getCopiedROIByIndex (copiedROIIndex)                            
                #determine if ROI Def exists if not create it.                
                currentDictionaryRoiDef = self.ROIDictionary.getROIDefs()
                
                foundROIName = currentDictionaryRoiDef.findBestMatchingROIName (newROIDef)                
                if (foundROIName != "None" and currentDictionaryRoiDef.hasROI (foundROIName)) :                
                    # current dictionary has ROI with matching Name                                        
                    ROICopyRequired = currentDictionaryRoiDef.getROIType (foundROIName) != newROIDef.getType ()
                    if (not ROICopyRequired) :
                        currentRadlexID = currentDictionaryRoiDef.getROINameRadlexID (foundROIName)
                        newRadlexID = newROIDef.getRadlexID ()
                        if (currentRadlexID != newRadlexID) :
                            NewROIDefAdded = True #Force ROI to be saved and reloaded
                            if (currentRadlexID != "unknown") :
                                ROICopyRequired = True
                            else:
                                currentDictionaryRoiDef.setROINameRadlexID (newRadlexID)
                                                        
                    if ROICopyRequired :
                        #ROI types do not match                       
                        newROIName = currentDictionaryRoiDef.getUniqueName (foundROIName + " Copy")                        
                        newROIDef.setNameAndColor (newROIName, newROIDef.getColor ())       
                        newROIDef = ROIDef (roiIDNumber = None, name = newROIDef.getName (), color = newROIDef.getColor (), roitype = newROIDef.getType (), mask = newROIDef.getMask(), Hidden=False, RadlexID = newROIDef.getRadlexID (), Locked = newROIDef.isLocked (), ROIDefUIDObj = newROIDef.getDefinedUID ())                                                                                             
                        currentDictionaryRoiDef.appendROIDef (newROIDef)     
                        NewROIDefAdded = True
                    else:
                        newROIDef.setNameAndColor (foundROIName, newROIDef.getColor ()) 
                        if currentDictionaryRoiDef.isROILocked (foundROIName) :
                           skipROIList.append (foundROIName)         
                else:
                    newROIDef = ROIDef (roiIDNumber = None, name = newROIDef.getName (), color = newROIDef.getColor (), roitype = newROIDef.getType (), mask = newROIDef.getMask(), Hidden=False, RadlexID = newROIDef.getRadlexID (), Locked = newROIDef.isLocked (), ROIDefUIDObj = newROIDef.getDefinedUID ())
                    currentDictionaryRoiDef.appendROIDef (newROIDef)                                        
                    NewROIDefAdded = True
                ROIDefList.append (newROIDef)
            
            if (NewROIDefAdded) :
                self._setListUI (self.ui.ROI_List, currentDictionaryRoiDef)                          
                self.ROIDefs = currentDictionaryRoiDef
                self.ROIDictionary.setSelected (None)             
                self.ROIDefs.setSelectedROI (SelectedROIName, clearSelection = True) 
                self.ROIDictionary.setROIDefs (self.ROIDefs, CallChangeListener = False, ClearUndoRedo = False) # clear-undo redo stack and repaint
                        
            clipBoundingBoxToVolumeObjectLst = []
            firstDictionaryModification = True
            
            sortedObjectList = []
            for copiedROIIndex in range (self._ProjectDataset.getInternalClipboard ().getCopiedROICount ())  :                
                _, newROIObj, sourceVoxelDim = self._ProjectDataset.getInternalClipboard ().getCopiedROIByIndex (copiedROIIndex)                            
                newROIDef = ROIDefList[copiedROIIndex]
                name = newROIDef.getName ()
                if name not in skipROIList :
                    sortedObjectList.append ((newROIObj, sourceVoxelDim, newROIDef))
            
            sortedObjectList = sorted (sortedObjectList, key = self._getObjectListDrawOrder)
            sortedObjectList.reverse ()
            for tpl in sortedObjectList  :                
                newROIObj, sourceVoxelDim, newROIDef = tpl                
                if (newROIObj is not None) :
                    if (firstDictionaryModification)  :
                            self.ROIDictionary._SaveUndoPoint ( CallDictionaryChangedListner = False)
                            firstDictionaryModification = False
                            
                    newROIObj.clipROIToDimensions (sourceVoxelDim, self.NiftiVolume.getSliceDimensions (), self.NiftiVolume.getVoxelSize ())                    
                    newROIDefName = newROIDef.getName ()
                    if self.ROIDefs.isROIArea (newROIDefName) :                                                    
                        newROIObj.setVolumeShape (self.NiftiVolume.getSliceDimensions(), self.ROIDictionary.getSharedVolumeCacheMem ())
                        
                        
                    if (not self.ROIDictionary.isROIDefined (newROIDefName)) :
                        #if the ROI object does not exist in the current ROI_Dictionary then just add the ROI to the dictionary                                                    
                        self.ROIDictionary.addROI (newROIDefName, newROIObj, SaveUndoPt = False)                         
                        if self.ROIDefs.isROIArea (newROIDefName) :                        
                            clipBoundingBoxToVolumeObjectLst.append (newROIObj)                            
                    else:
                        #merge the copied ROI into the dictonary #harder.
                        obj = self.ROIDictionary.getROIObject (newROIDefName)                        
                        if self.ROIDefs.isROIArea (newROIDefName) :                        
                            obj.mergeROI (newROIObj,  PerimeterAreaObjectMerge = self.ROIDictionary.isROIinPerimeterMode ())     
                            obj.mergeObjectBoundingBoxes (newROIObj)
                            clipBoundingBoxToVolumeObjectLst.append (obj)                            
                        else:
                            obj.mergeROI (newROIObj)                                                            
            #start remove copy and pasted slices that are above or below the volume
            sliceDictionary = self.ROIDictionary.getROISliceDictionary()
            sortedSliceIndex = sorted(list(sliceDictionary.keys ()))
            for slicekey in sortedSliceIndex :
                if slicekey >= 0 :
                    break
                objectLst = sliceDictionary[slicekey]
                for obj in objectLst :
                    obj.removeSlice (slicekey, SharedROIMask = self.ROIDictionary.getSharedVolumeCacheMem ()) 
                    
            sortedSliceIndex.reverse ()
            for slicekey in sortedSliceIndex :
                if slicekey < self.NiftiVolume.getZDimSliceCount ():
                    break
                objectLst = sliceDictionary[slicekey]
                for obj in objectLst :
                    obj.removeSlice (slicekey, SharedROIMask= self.ROIDictionary.getSharedVolumeCacheMem () ) 
            #end remove copy and pasted slices that are above or below the volume    
            
            #Adjust bounding box if necessary after deletion of slices falling outside of the volume to remove extra-volume slices from consideration.
            for obj in clipBoundingBoxToVolumeObjectLst : 
                obj.clipBoundBoxToVolume (self.NiftiVolume.getSliceDimensions ())                                            
                # correct selected coordinate for objects with selection values no longer valid.
                if (obj.getCoordinate () == None ) :
                    objSliceList = obj.getSliceNumberList()
                    if (len (objSliceList) > 0) :
                        obj.setSliceNumber (objSliceList[0])
            
            progdialog = QProgressDialog(self)
            progdialog.setMinimumDuration (4)
            progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)                
            progdialog.setWindowTitle ("Converting ROI")            
            progdialog.setWindowModality(QtCore.Qt.ApplicationModal)                
            progdialog.setRange(0,0)                
            progdialog.setMinimumWidth(300)
            progdialog.setMinimumHeight(100)
            try:
                progdialog.forceShow()          
                progdialog.setValue(0)
                self._ProjectDataset.getApp ().processEvents ()                        
                self.ROIDictionary.verrifyDictionaryIsConsistentWithPerimeterAreaSettings ( self.NiftiVolume.getSliceDimensions (), self.ROIDefs, ProgressDialog = progdialog, FileSaveThreadManger = self._ProjectDataset.getROISaveThread (), ProjectDataset = self._ProjectDataset)
            finally:
                progdialog.close ()
                self._ProjectDataset.getApp ().processEvents ()                        
                
            self.ROIDictionary._CallDictionaryChangedListner  ()  
    
    def copyROI (self) :
        if (self.canCopyROI () and self.ROIDictionary is not None) :       
            if self.showFlattenDeepGrowDlg ("One-click generated segmentations must be flattened before an area ROI can be copied. Do you wish to flatten the segmentation?") :
                ROIList =self.ROIDefs.getSelectedROI () 
                self._ProjectDataset.getInternalClipboard ().clearClipboard ()
                for roiName in ROIList : 
                    roiDef = self.ROIDictionary.getROIDefs().getROIDefCopyByName (roiName)
                    if (roiDef is not None) :
                        roiobj = None
                        if (self.ROIDictionary.isROIDefined (roiName)) :
                            roiobj = self.ROIDictionary.getROIObject (roiName)
                            if self.ROIDefs.isROIArea (roiName) : 
                                roiobj = roiobj.copy (DeepCopySlices = True)  
                            else:
                                roiobj = roiobj.copy ()  
                            roiobj.setObjectClipDictionary (None)
                        voxelsize = self.NiftiVolume.getVoxelSize ()
                        self._ProjectDataset.getInternalClipboard ().addToClipBoard (roiDef,roiobj, (float (voxelsize[0]), float (voxelsize[1]), float (voxelsize[2])))                
    
    def canCopyROI (self) :                
        canCopy = False                               
        if (self.isROIDictonaryFileLoaded () ) :
            ROILst =self.ROIDefs.getSelectedROI ()
            if (len (ROILst)  > 0) :
                for roiName in ROILst :
                    if roiName != "None" :
                        canCopy = True
                        break
        self.ui.actionCopy.setEnabled (canCopy)
        return canCopy
        
    def canPasteROI (self) : 
        canPaste = False                
        if (self.isROIDictonaryFileLoaded () and not self.ROIDictionary.isReadOnly () and self._ProjectDataset.getInternalClipboard ().getCopiedROICount () > 0) :            
            canPaste = True
        self.ui.actionPaste.setEnabled (canPaste)
        return canPaste
    
    def ClearAxisProjectionNiftiCache (self) :
        if self.ui.XYSliceView.areAxisProjectionControlsEnabledForAxis () :
            controls = self.ui.XYSliceView.getAxisProjectionControls ()
            controls.clearNiftiSliceAxisCache ()
            self.update ()
            
    def _showOrigionalImaging (self) :
        try:
            self.NiftiVolume.showOrigionalImaging ()
            self.ClearAxisProjectionNiftiCache ()
        except:
            pass
        
    def _showTransformedImaging (self) :
        try:
            self.NiftiVolume.showTransformedImaging ()
            self.ClearAxisProjectionNiftiCache ()
        except:
            pass
        
    def aboutToShowSettingsMenu (self) :
        result = self.isCurrentImageN4BiasCorrected ()
        if result is None or not  NiftiVolumeData.hasSimpleITKInstalled () :
            self.ui.menuMRI_N4_bias_field_correction.setEnabled (False)
            self.ui.actionN4Bias_field_corrected.setEnabled (False)
            self.ui.actionShow_regular_image.setEnabled (False)                
            self.ui.actionN4Bias_field_corrected.setChecked (False)
            self.ui.actionShow_regular_image.setChecked (False)
        else:
            self.ui.menuMRI_N4_bias_field_correction.setEnabled (True)
            self.ui.actionN4Bias_field_corrected.setEnabled (True)            
            self.ui.actionShow_regular_image.setEnabled (True)            
            self.ui.actionN4Bias_field_corrected.setChecked (result)
            self.ui.actionShow_regular_image.setChecked (not result)
        if self.NiftiVolume is None or not self.NiftiVolume.hasTransformedImaging () :            
            self.ui.showOrigionalImaging.setVisible (False)
            self.ui.showTransformedImaging.setVisible (False)
            self.ui.showOrigionalImaging.setEnabled (False)
            self.ui.showOrigionalImaging.setChecked (True)
            self.ui.showTransformedImaging.setEnabled (False)
        else:
            shownImaging = self.NiftiVolume.getImagingShown ()
            self.ui.showOrigionalImaging.setVisible (True)
            self.ui.showTransformedImaging.setVisible (True)
            self.ui.showOrigionalImaging.setEnabled (shownImaging != "Origional")
            self.ui.showTransformedImaging.setEnabled (shownImaging != "Transformed")
            self.ui.showOrigionalImaging.setChecked (shownImaging == "Origional")
            self.ui.showTransformedImaging.setChecked (shownImaging == "Transformed")
            
    def canCopyandPasteROI (self) :        
        self.canCopyROI ()
        self.canPasteROI () 
        self.ui.menuSet_Voxel_Annotation_Mode.setEnabled (self.isROIDictonaryFileLoaded ())
        
    #end ROI copy and paste functionatiy
    
    def scanPhaseListChanged (self, newScanPhaseList)  :
        selectedScanPhase = self.ui.ScanPhaseComboBox.itemText (self.ui.ScanPhaseComboBox.currentIndex ())
        
        self.ui.ScanPhaseComboBox.currentIndexChanged.disconnect (self.ScanPhaseSel)        
        self.ui.ScanPhaseComboBox.clear ()
        for phase in newScanPhaseList :
            self.ui.ScanPhaseComboBox.addItem (phase)
        if (selectedScanPhase not in newScanPhaseList) :
            self.ui.ScanPhaseComboBox.addItem (selectedScanPhase)
        index = self.ui.ScanPhaseComboBox.findText(selectedScanPhase)
        if (index != -1) :  
            self.ui.ScanPhaseComboBox.setCurrentIndex (index)
            self.ui.ScanPhaseComboBox.currentIndexChanged.connect (self.ScanPhaseSel)                
        else:
            self.ui.ScanPhaseComboBox.currentIndexChanged.connect (self.ScanPhaseSel)                
            index = self.ui.ScanPhaseComboBox.findText("Unknown")
            self.ui.ScanPhaseComboBox.setCurrentIndex (index)
                 

    def setSelectedScanPhase (self, SelectedNiftiData, scanphase):        
        if (self.NiftiVolumeTreeSelection is None or not self.NiftiVolumeTreeSelection.equals (SelectedNiftiData)) :
            return        
        index = self.ui.ScanPhaseComboBox.findText(scanphase)
        if (index != -1 and self.ui.ScanPhaseComboBox.currentIndex () != index) :  
            self.ui.ScanPhaseComboBox.setCurrentIndex (index)    
        elif (index == -1) :
            self.ui.ScanPhaseComboBox.addItem (scanphase)
            index = self.ui.ScanPhaseComboBox.findText(scanphase)
            self.ui.ScanPhaseComboBox.setCurrentIndex (index)
            """index = self.ui.ScanPhaseComboBox.findText("Unknown")
            self.ui.ScanPhaseComboBox.setCurrentIndex (index)"""

    #  ScanPhaseSel: UI call back called when user changes scan phase selection UI.  Sets dictionary scan phase 
    def ScanPhaseSel (self):        
       if  self.ROIDictionary is not None :
           scanPhaseTxt = self.ui.ScanPhaseComboBox.currentText ()
           #scan phase selection has to be set before dictionary to update ui correcetly
           self._ProjectDataset.setSelectedScanPhase  (self.NiftiVolumeTreeSelection, scanPhaseTxt)           
           self.ROIDictionary.setScanPhase (scanPhaseTxt)                 # otherwise, ui is updated but nothing is done, this is done to allow non-saved data to be shown in the UI without alterating the data.
           
            
        
    def getHounsfieldSettingsCopy (self) :
        return self.hounsfieldVisSettings.copy ()
        
    def getVisSettingsCopy (self) :
        return self.visSettings.copy ()
        
    def _setListUI (self, lst, roiDefs, selectedNameList = [], SetROIDefinedBackground = False):
        listSelectionChangeListenerConnected = True
        try :
            self.ui.ROI_List.selectionModel().selectionChanged.disconnect(self.ROIListSelectionChanged)                
        except :
            listSelectionChangeListenerConnected = False
        
        if SetROIDefinedBackground and self.isROIDictonaryFileLoaded () :
            ROIDictionary = self.ROIDictionary
        else:
            ROIDictionary =  None
            
        namelist = ["None"] + roiDefs.getNameLst ()    
        rebuildList = (self.ui.ROI_List.count() != len (namelist)) 
        if not rebuildList :
            for x in range (self.ui.ROI_List.count()) :
                item = self.ui.ROI_List.item(x)
                txt = item.text ()
                if txt != namelist[x] :
                    rebuildList = True
                    break
                if (txt != "None") :
                    color =  item.foreground ().color ()
                    testColor  = roiDefs.getROIColor (txt)  
                    if (color.red () != testColor.red () or color.green () != testColor.green () or color.blue () != testColor.blue ()) :
                        item.setForeground (testColor)  
                setSelected = txt in selectedNameList
                if ROIDictionary is not None and ROIDictionary.isROIDefined (txt) :
                    item.setBackground (QtGui.QColor (255,211,82))
                else :
                    item.setBackground (QtGui.QColor (255,255,255))
                if item.isSelected () != setSelected :
                    item.setSelected (setSelected)                
                
                    
        if (rebuildList) :
            lst.clear ()
            for name in namelist: 
                    item = QListWidgetItem() 
                    item.setText (name) 
                    if (name == "None") :
                        color = QtGui.QColor (0,0,0)
                    else:
                        color  = roiDefs.getROIColor (name)                
                    item.setForeground (color)
                    if ROIDictionary is not None and ROIDictionary.isROIDefined (name) :
                        item.setBackground (QtGui.QColor (255,211,82))
                    else :
                        item.setBackground (QtGui.QColor (255,255,255))
                    item.setSelected (name in selectedNameList)
                    lst.addItem (item)  
        #if (len (selectedNameList) == 0) :
        #    self.ui.RemoveROIBtn.setEnabled(False)    # set to true in dictionary change event       
        if (listSelectionChangeListenerConnected) :
            self.ui.ROI_List.selectionModel().selectionChanged.connect(self.ROIListSelectionChanged)        
                
    
                
    def reloadROIUIList (self) :
        self._setListUI (self.ui.ROI_List, self.ROIDefs, selectedNameList = self.ROIDefs.getSelectedROI (ReturnSet = True))                          
        
    
        
    def QuitMenu (self) :         
        self.close ()  
    
    def getLoadedFileHandlePath (self) :
        try :
            if (self.ROIDictionary is not None) :
                self.ROIDictionary.setSoftReadOnlyMode ()
                filehandle = self.ROIDictionary.getROIFileHandle ()        
                return filehandle.getPath ()
        except:
            return None
            
    def closeUnsavedAndReload (self) :
        if (self.ROIDictionary is not None) :
            self.ROIDictionary.setSoftReadOnlyMode ()
            filehandle = self.ROIDictionary.getROIFileHandle ()        
            if (filehandle is not None and self.NiftiVolume is not None) :
                oldTreeSelection = self.NiftiVolumeTreeSelection            
                NIfTIFilePath = None
                if oldTreeSelection is None :
                    if self.getLoadedNIfTIFile () is not None :
                        NIfTIFilePath = self.getLoadedNIfTIFile ().getFilePath ()                        
                self.ROIDictionary.clear ()
                self.DatasetTreeSelectionChanged (None, None)                       
                self.DatasetTreeSelectionChanged (oldTreeSelection, NIfTIFilePath)
                self.ROIDictionary.setSelected (None)
                self.ResetSelectedRIODef()
                self.UpdateDatasetTagEditor ()
                self.update ()
    
    def reloadROIDefs (self):       
        if (self.ROIDictionary is not None) :         
            selectedCoord = None
            if (self.SelectedCoordinate is not None):
                selectedCoord = self.SelectedCoordinate.getCoordinate()
            oldSelectedROIDefs = self.ROIDefs.getSelectedROI ()
            filehandle = self.ROIDictionary.getROIFileHandle ()        
            if (filehandle is not None and self.NiftiVolume is not None) :
                del filehandle
                oldTreeSelection = self.NiftiVolumeTreeSelection
                NIfTIFilePath = None
                if oldTreeSelection is None :
                    if self.getLoadedNIfTIFile () is not None :
                        NIfTIFilePath = self.getLoadedNIfTIFile ().getFilePath ()       
                        
                self.DatasetTreeSelectionChanged (None, None)
                self.ROIDefs.loadROIDefsFromProjectFile ()                                                                     
                self.DatasetTreeSelectionChanged (oldTreeSelection, NIfTIFilePath =NIfTIFilePath)                
            else:
                self.ROIDefs.loadROIDefsFromProjectFile ()   
            
            self.ROIDefs.clearSelectedROI () # Line of code is likely redundant with calls in resetSelectedROIDef
            self.ROIDictionary.setSelected (None)
            enabled = self.ui.ROI_List.isEnabled ()
            self._setListUI (self.ui.ROI_List, self.ROIDefs)                          
            self.ui.ROI_List.setEnabled(enabled)
            self.ROIDefs.ROIDefsChanged ()          
            self.ResetSelectedRIODef()
            self.UpdateDatasetTagEditor ()
            self.setROIHiddenLabel ()
            clearSelection = True
            for roiName in oldSelectedROIDefs :
                if (self.ROIDefs.hasROI (roiName)) :
                    self.ROIDefs.setSelectedROI (roiName, clearSelection = clearSelection)
                    clearSelection = False
            if selectedCoord is not None :
                self.SelectedCoordinate.setCoordinate (selectedCoord)
                
            self.updateUIToReflectAreaPerimeterDrawing () 
            self.update ()
            
    
    def closeOpenFileResources (self)  :
        self.showFlattenDeepGrowDlg ("One click segmentations must either be flattend or removed before closing an series annotation.",Cancel = False)
        self.ROIDictionary.clear()
        
    # application close event, called when application quits.  detaches event handlers from the App.
    def closeEvent(self,event): 
        self.showFlattenDeepGrowDlg ("One click segmentations must either be flattend or removed before closing an series annotation.",Cancel = False)
        try:
            self._ChildWidgetOverrideEventFilter.clearParentWindow ()
        except:
            print ("Cannot close child widget override event filter")            
        self._ChildWidgetOverrideEventFilter = None 
        self._lastOpenedNIfTIDataFile = None
        self.closePenSettingsDlg ()
        self.closePenToolsDlg ()
        self.closeDatasetTagEditorWindow ()
        self.closeTranslationDlg ()
        self.closeMLDatasetDescriptionDlg ()
        if (self.NiftiVolumeTreeSelection is not None) :
            self.NiftiVolumeTreeSelection.setDatasetOpen (False)
        self._DisableWindowChangeEvent = True                     
        
        try :
              self.ui.ScanPhaseComboBox.currentIndexChanged.disconnect (self.ScanPhaseSel)                
        except :
              print ("ScanPhaseSel not connected.")
              
        try :
            self.ui.ContourList.selectionModel().selectionChanged.disconnect(self.ContourListSelectionChanged)
        except:
            print ("ContourList not connected.")
        
        try :
            self.ui.ROI_List.selectionModel().selectionChanged.disconnect(self.ROIListSelectionChanged)
        except:
            print ("ROI_List not connected.")                    
        
        try:
            self.ui.MajorAxisSliceTextBox.editingFinished.disconnect (self.MajorAxisTextBoxChanged)      
        except:
            print ("MajorAxisSliceTextBox not connected.")
                      
        try:
            self.ui.YZSliceAxisSliceTextBox.editingFinished.disconnect (self.YZSliceAxisTextBoxChanged)      
        except:
            print ("YZSliceAxisSliceTextBox not connected.")
        try:
            self.ui.XZSliceAxisSliceTextBox.editingFinished.disconnect (self.XZSliceAxisTextBoxChanged)      
        except:
            print ("XZSliceAxisSliceTextBox not connected.")
            
        if (self.ROIDictionary is not None) :
            self.ROIDictionary.saveROIDictionaryToFile ()
            self.ROIDictionary.clear ()        
        if (self._StatsDlg != None) :
            self._StatsDlg.close ()
            self._StatsDlg = None
        if (self._hounsfeildDlg != None) :
            self._hounsfeildDlg.close ()
            self._hounsfeildDlg = None
        if (self._visOptionsDlg != None) : 
            self._visOptionsDlg.close ()
            self._visOptionsDlg = None            
        if (self._PesscaraMaskViewerDlg != None) :
            self._PesscaraMaskViewerDlg.close ()
            self._PesscaraMaskViewerDlg = None    
        if (self._ML_ModelZooDlg is not None) :
            self._ML_ModelZooDlg.close ()
            self._ML_ModelZooDlg = None
        if (self._ML_QuantifySegmentatinDlg is not None) :
            self._ML_QuantifySegmentatinDlg.close ()
            self._ML_QuantifySegmentatinDlg = None
        self._ProjectDataset.closeContouringWindow (self)        
        self.ui.XZSliceView.delete ()
        self.ui.YZSliceView.delete ()
        self.ui.XYSliceView.delete ()      
        if (self.ROIDictionary is not None) :               
            self.ROIDictionary.delete ()
        self.ROIDefs.delete () 
        self.ui.XYSliceSelectorWidget.delete()
        self.ui.XYSliceVerticalSelector.delete ()
        self.ui.XZSliceVerticalSelector.delete ()
        self.ui.YZSliceVerticalSelector.delete ()
        self.SelectedCoordinate = Coordinate ()
        self.resizeWidgetHelper = None
        self.NiftiVolume = None
        self._HiddenDlgMem = {}
        QMainWindow.closeEvent (self, event)         
    
    
    def _hideDlg (self, dlg, hiddenDlg) :
        try :
            if (dlg is not None) :
                if (dlg.isVisible ()) :
                    dlg.hide ()
                    hiddenDlg[dlg] = (dlg.pos().x (), dlg.pos().y ())
                else: 
                    if dlg in hiddenDlg :
                        del hiddenDlg[dlg]
        except:
            print ("A error occured hideing Dlg")
        
    def _showHiddenDlg (self, dlg, hiddenDlg) :
        try :
            if (dlg is not None) :
                if (dlg in hiddenDlg) :
                    x,y = hiddenDlg[dlg]
                    dlg.move(x, y)
                    dlg.show ()
                    #dlg.raise_ ()
                    del hiddenDlg[dlg]
                    return True
        except:
            print ("A error occured showng Dlg")
        return False
    
    def setActiveContourWindow (self, setChildDialogVisible) :
        if (self._isActiveContourWindow == setChildDialogVisible) :
            return
        self._isActiveContourWindow = setChildDialogVisible
        if (setChildDialogVisible == False) :  
            #hiding Window
           self._hideDlg (self._StatsDlg, self._HiddenDlgMem)
           self._hideDlg (self._hounsfeildDlg, self._HiddenDlgMem)
           self._hideDlg (self._PesscaraMaskViewerDlg, self._HiddenDlgMem)
           self._hideDlg (self._ML_ModelZooDlg, self._HiddenDlgMem)  
           self._hideDlg (self._ML_QuantifySegmentatinDlg, self._HiddenDlgMem)
           try :   
                self._customDatasetTagEditor = self.getOpenCustomDatasetTagEditor ()
                self._hideDlg (self._customDatasetTagEditor, self._HiddenDlgMem)
           except:
                print ("A Exception occured closing the custom dataset tag editor") 
           
           try :   
                self._internalDatasetTagEditor = self.getOpenInternalDatasetTagEditor ()
                self._hideDlg (self._internalDatasetTagEditor, self._HiddenDlgMem)
           except:
                print ("A Exception occured closing the internal dataset tag editor") 
             

           self._hideDlg (self._TranslationDlg, self._HiddenDlgMem)
           self._hideDlg (self._MLDatasetDescriptionDlg, self._HiddenDlgMem)
           self._hideDlg (self._PenSettingsDlg, self._HiddenDlgMem)
           self._hideDlg (self._PaintToolsDlg, self._HiddenDlgMem)

        else:        
           #show Window
           self._showHiddenDlg (self._StatsDlg, self._HiddenDlgMem)
           self._showHiddenDlg (self._hounsfeildDlg, self._HiddenDlgMem)
           self._showHiddenDlg (self._PesscaraMaskViewerDlg, self._HiddenDlgMem)
           self._showHiddenDlg (self._ML_ModelZooDlg, self._HiddenDlgMem)       
           self._showHiddenDlg (self._ML_QuantifySegmentatinDlg, self._HiddenDlgMem)
           try :   
                self._customDatasetTagEditor = self.getOpenCustomDatasetTagEditor ()
                self._showHiddenDlg (self._customDatasetTagEditor, self._HiddenDlgMem)
           except:
                print ("A Exception occured opeing the custom dataset tag editor")  
           try :   
                self._internalDatasetTagEditor = self.getOpenInternalDatasetTagEditor ()
                self._showHiddenDlg (self._internalDatasetTagEditor, self._HiddenDlgMem)
           except:
                print ("A Exception occured opening the internal dataset tag editor") 
           self._showHiddenDlg (self._TranslationDlg, self._HiddenDlgMem)
           self._showHiddenDlg (self._MLDatasetDescriptionDlg, self._HiddenDlgMem)
           self._showHiddenDlg (self._PenSettingsDlg, self._HiddenDlgMem)              
           self._showHiddenDlg (self._PaintToolsDlg, self._HiddenDlgMem)              
           if (not self.isActiveWindow()): 
               self.raise_ ()
               self.activateWindow ()
     
        
    def changeEvent (self, event) :        
        #print ("contour window chang event fired: " + str (event.type ()))        
        #print (self.isActiveWindow())
        result = QMainWindow.changeEvent (self, event)            
        #print (self.isActiveWindow())
        if (not self._DisableWindowChangeEvent) :
            if (self.isActiveWindow()) :
                #print ("In: set active window")
                #print (self)
                self._ProjectDataset.setActiveWindow (self)   
        return result

    def getSelectedSlice (self) :
        if (self.SelectedCoordinate.getCoordinate() is None) :
            return None
        return int (self.SelectedCoordinate.getZCoordinate())
    
    @staticmethod
    def getWidgetRelativeToParent (widget, relativeParent):
        xp, yp = 0, 0
        while widget is not relativeParent :
            xp += widget.x ()
            yp += widget.y ()
            widget = widget.parent ()
            if widget is None :
                break
        return xp, yp
     
    def isMouseOver (self, mouseWheelEvent, widget1, widget2) :
            #try:
            xP, yP = RCC_ContourWindow.getWidgetRelativeToParent (widget1, self)
            if widget2 is None :
                widgetX_L = xP
                widgetY_T = yP
                widgetX_R = widget1.width () + widgetX_L
                widgetY_B =widget1.height () + widgetY_T
                #name = widget1.objectName ()
            else:
                widgetX1_L = xP
                widgetY1_T = yP
                widgetX1_R = widget1.width () + widgetX1_L
                widgetY1_B = widget1.height () + widgetY1_T
                xP, yP =  RCC_ContourWindow.getWidgetRelativeToParent (widget2, self)
                widgetX2_L = xP
                widgetY2_T = yP
                widgetX2_R = widget2.width () + widgetX2_L
                widgetY2_B = widget2.height () + widgetY2_T
                widgetX_L = min (widgetX1_L, widgetX2_L)
                widgetY_T = min (widgetY1_T, widgetY2_T)
                widgetX_R = max (widgetX1_R, widgetX2_R)
                widgetY_B = max (widgetY1_B, widgetY2_B)
                #name = widget1.objectName () + " " + widget2.objectName ()
            
            mX = mouseWheelEvent.globalX () - self.x ()
            mY = mouseWheelEvent.globalY () - self.y ()
            #print (name + " Mouse: %d, %d   Widget: %d, %d, %d, %d" % (mX, mY,widgetX_L,widgetY_T,widgetX_R, widgetY_B))
            width = widgetX_R - widgetX_L
            height = widgetY_B - widgetY_T
           
            if (mX >= widgetX_L) and (mY >= widgetY_T) and (mX <= widgetX_R) and (mY <= widgetY_B) and (width > 0) and (height>0) :
                #print ("True")
                return True
            return False                
            #except:
            #    return False
    
    def _isMouseOverAnyWidget (self, event, WidgetList) :
        for widget in WidgetList :
            if self.isMouseOver (event, widget, None) :
                return True
        return False
            
    # application mouse wheel event. Mouse wheel adjusts selected slice
    def wheelEvent(self,event) :        
        if (self.SelectedCoordinate.getCoordinate() is None) :
            event.accept()
            return
        if  self._isMouseOverAnyWidget (event, [self.ui.ROI_List, self.ui.ContourList]):
            #print ("MouseOver")
            event.accept()
            return
        if not self.visSettings.isMouseWheelSliceSelectionDisabled () :
            pixelDelta = event.pixelDelta()
            if not pixelDelta.isNull () :
                dStep = int (pixelDelta.y ())
                if (self.visSettings.isMouseWheelInverted ()) :
                    dStep = -dStep
            else:
                numDegrees = event.angleDelta() / 8
                numSteps = numDegrees / 15
                dStep = int (numSteps.y ())
                
                if (self.visSettings.isMouseWheelInverted ()) :
                    dStep = -dStep        
            
            if dStep != 0 :
                sensitivity = self.visSettings.getMouseWheelSliceSelectionSensitivity ()
                if (sensitivity != 50) :
                    sensitivity -= 50
                    sensitivity = float (sensitivity) / 50.0
                    sensitivity *= 10.0
                    if dStep < 0 :
                        if sensitivity < 0 :    
                            dStep = min (int (float (dStep) / -sensitivity ), -1)
                        else:
                            dStep = int (float (dStep) * sensitivity)
                    else:            
                        if sensitivity < 0 :    
                            dStep = max (int (float (dStep) / -sensitivity ), 1)
                        else:
                            dStep = int (float (dStep) * sensitivity)
                    #print (dStep)        
                if self.isZoomInXYUIState () or self.isZoomOutXYUIState () :
                
                    if self.isMouseOver (event, self.ui.XZSliceView, None)  :
                        self.ui.XZSliceView.mouseWheelZoom (dStep)
                    elif self.isMouseOver (event, self.ui.YZSliceView, None)  :
                        self.ui.YZSliceView.mouseWheelZoom (dStep)
                    elif self.isMouseOver (event, self.ui.XYSliceView, None)  :
                        self.ui.XYSliceView.mouseWheelZoom (dStep)                    
                    elif self.ui.XYSliceView.isSelected () :
                        self.ui.XYSliceView.mouseWheelZoom (dStep)    
                    elif self.ui.XZSliceView.isSelected () :
                        self.ui.XZSliceView.mouseWheelZoom (dStep)
                    elif self.ui.YZSliceView.isSelected () :
                        self.ui.YZSliceView.mouseWheelZoom (dStep)
                else:
                    if self.isMouseOver (event, self.ui.XZSliceView, self.ui.XZSliceSlider) or self.isMouseOver (event, self.ui.XZSliceAxisSliceTextBox, self.ui.XZSliceVerticalSelector)  :
                        selectedView = self.ui.XZSliceView
                        slicenumber = int (self.SelectedCoordinate.getYCoordinate ())                
                    elif self.isMouseOver (event, self.ui.YZSliceView, self.ui.YZSliceSlider) or self.isMouseOver (event, self.ui.YZSliceAxisSliceTextBox, self.ui.YZSliceVerticalSelector) :  
                        selectedView = self.ui.YZSliceView
                        slicenumber = int (self.SelectedCoordinate.getXCoordinate ())                
                    else:
                        selectedView = self.ui.XYSliceView
                        slicenumber = int (self.SelectedCoordinate.getZCoordinate ())          
                    slicenumber += dStep
                    if slicenumber < 0 :
                           slicenumber = 0                       
                    elif slicenumber >= selectedView.getMaxSliceCount () :
                           slicenumber = selectedView.getMaxSliceCount () - 1
                           if (slicenumber < 0) :
                                slicenumber = 0
                    self.SelectedCoordinate.setCoordinate (selectedView.getSliceCoordinate (slicenumber), Event="MouseWheel") # Set selected slice
        event.accept()
        

    def _CancelMoveOpperation (self) :
        fireundoevent = False
        if self.ROIDictionary is not None :            
            if self.ROIDefs.isROISelected () and self.ROIDefs.isOneROISelected ()  :    # if ROI is selected otherwise info message
                obj = self.ROIDictionary.getSelectedObject ()
                if (obj != None and obj.hasControlPoints ()) :
                    if (obj.getSelectedBoundingBoxControlPoint () is not None) :
                        obj.setSelectedBoundingBoxControlPoint(None)     
                        fireundoevent = True
                if not fireundoevent :
                    itemName =self.ROIDefs.getSelectedROI ()[0]  # get currently selected ROI                                                         
                    if (not self.ROIDefs.isROIHidden (itemName) and not self.ROIDefs.isROILocked (itemName)) :                        
                       if (self.ROIDefs.isROIArea (itemName)) :  # tests if currently selected ROI is a area object        
                           if self.ROIDictionary.isROIinAreaMode () :                                  
                               isAllocated, obj = self.isTemporaryPaintObjAllocated ()
                               if (isAllocated and obj is not None) :
                                   obj.clearTemporaryObjectSliceMask ()
                                   self.setPenIndex (0)                                    
                                   clearObjList = self.getClearPaintObjectList () 
                                   if (clearObjList is not None) :
                                       for clearObjTuple in clearObjList :
                                           clearObj, origionalClearObjMask = clearObjTuple                                                   
                                           clearObj.clearTemporaryObjectSliceMask ()
                                   self.clearTemporaryPaintObj()    
                                   
            if (self._SelectionMoveROIState is not None) :
                self._SelectionMoveROIState = None
                fireundoevent = True
            if (self._XYZoomInit != None ):
                self._XYZoomInit = None        
            self.ui.XYSliceView.cancelMouseMove ()
            if (fireundoevent):
                self.ROIDictionary.undo ()
                return True
        return False
            
    def _ZoomInKeyEvent (self) :
        cX = self.SelectedCoordinate.getXCoordinate ()
        cY = self.SelectedCoordinate.getYCoordinate ()
        cZ = self.SelectedCoordinate.getZCoordinate ()
        self.ui.XYSliceView.zoomInClick (cX, cY, cZ)     
        self.ui.XYSliceView.update ()
                
    def _ZoomOutKeyEvent (self) :
        self.ui.XYSliceView.zoomOut ()           
        self.ui.XYSliceView.update ()
    
    def _KeystrokeToggleUIROIFill (self) :
        self.visSettings.toggleUITranparency ()
        self.setROIHiddenLabel ()      
        self.update ()
            
    
        
    def keyPressEvent(self, event):
         if (event.type() == QtCore.QEvent.KeyPress or event.type() == QtCore.QEvent.KeyRelease) :             
              if event.key () == QtCore.Qt.Key_Delete or event.key () == QtCore.Qt.Key_Backspace :
                  if (event.type() == QtCore.QEvent.KeyPress) :
                     inAreaMode = self.ROIDictionary.isROIinAreaMode ()
                     if inAreaMode and self.ui.YZSliceView.isSelected () :                         
                         self.ui.YZSliceVerticalSelector.keyPressEvent (event)
                     elif inAreaMode and self.ui.XZSliceView.isSelected () :
                         self.ui.XZSliceVerticalSelector.keyPressEvent (event)                         
                     else:    
                         self.ui.XYSliceSelectorWidget.keyPressEvent (event)
                     return 
              
              if (event.type() == QtCore.QEvent.KeyPress) :
                  if  event.key () in [QtCore.Qt.Key_Left, QtCore.Qt.Key_Right, QtCore.Qt.Key_Up, QtCore.Qt.Key_Down]  :                                                                                                            
                      if self.ui.YZSliceView.isSelected () :                         
                         self.ui.YZSliceVerticalSelector.keyPressEvent (event)
                      elif self.ui.XZSliceView.isSelected () :
                         self.ui.XZSliceVerticalSelector.keyPressEvent (event)                         
                      else:    
                          self.ui.XYSliceSelectorWidget.keyPressEvent (event)                      
                      return                
          
                  KeyStrokes =  ShortcutKeyList.getPassThroughKeyStrokes () 
                  if (event.key () in KeyStrokes) :                                 
                      self.setKeyEvent (event.key (), (event.type() == QtCore.QEvent.KeyPress))            
                      return 
        
              
    # setKeyEvent called by application event handler to save keyboard keypressed state for select keys
    def setKeyEvent (self, key, pressed) :
        self._KeyboardState[key] = pressed
        
        if (self.ROIDictionary is not None) :
            if (key == QtCore.Qt.Key_Escape) :
                self.disableRangeSelectionCallBack ()
                #self.ui.XYSliceView.setRangeSelectionEnabled (False)
                self._CancelMoveOpperation ()
                
            elif (key == QtCore.Qt.Key_R or (key == QtCore.Qt.Key_Y and self.getKeyState (QtCore.Qt.Key_Control))) :
                #print ("Redo Pressed")                
                if (not self._CancelMoveOpperation () and not self.ROIDictionary.isReadOnly ()) :
                    self.ROIDictionary.redo ()
                    
            elif (key == QtCore.Qt.Key_U or (key == QtCore.Qt.Key_Z and self.getKeyState (QtCore.Qt.Key_Control))) :
                #print ("Undo Pressed")
                if (not self._CancelMoveOpperation () and not self.ROIDictionary.isReadOnly ()):
                    self.ROIDictionary.undo ()
           
            elif (key == QtCore.Qt.Key_T and self.getKeyState (QtCore.Qt.Key_Control)) :
                penSettings = self.getPenSettings ()
                thSet = penSettings.isThresholded ()
                thSet = not thSet
                penSettings.setThresholdSet (thSet)                
                self.updatePaintBrushIfNecessary ()
                self.update ()
                if self._PenSettingsDlg is not None :
                    self._PenSettingsDlg.ui.SetPenThresholdMaskDisabledBtn.setChecked (not thSet)        
                    self._PenSettingsDlg.ui.SetPenThresholdMaskEnabledBtn.setChecked (thSet) 
                
            elif (key == QtCore.Qt.Key_F and self.getKeyState (QtCore.Qt.Key_Control) and not self.ROIDictionary.isReadOnly ()) :
                self.fillROIHoleInSelectedSlice ()                
            elif (key == QtCore.Qt.Key_E and self.getKeyState (QtCore.Qt.Key_Control) and not self.ROIDictionary.isReadOnly ()) :
                self.erodeROIInSelectedSlice ()          
            elif (key == QtCore.Qt.Key_D and self.getKeyState (QtCore.Qt.Key_Control) and not self.ROIDictionary.isReadOnly ()) :
                self.dilationROIInSelectedSlice ()          
            elif (key == QtCore.Qt.Key_O and self.getKeyState (QtCore.Qt.Key_Control) and not self.ROIDictionary.isReadOnly ()) :
                self.KeepOnlyLargestObjectFilter ()          
            
                
            elif (key == QtCore.Qt.Key_A)  or (key == QtCore.Qt.Key_D) :
                if (key == QtCore.Qt.Key_A) :
                    self.visSettings.incrementROITransparency (-5)  
                else:
                    self.visSettings.incrementROITransparency (5)  
                if (self._visOptionsDlg is not None) :
                    self._visOptionsDlg.UpdateVisSettingsDlgROITransparency (self.visSettings.getUITransparency (IgnoreUITransparancyToggle = True))
                self.update ()
            
            elif (key == QtCore.Qt.Key_Z) or (key == QtCore.Qt.Key_X) or (key == QtCore.Qt.Key_C) or (key == QtCore.Qt.Key_V) or (key == QtCore.Qt.Key_B) or (key == QtCore.Qt.Key_N) or (key == QtCore.Qt.Key_M):
                if (self._hounsfeildDlg is not None) :
                    self._hounsfeildDlg.keyboardEvent (key)
                else:
                    temphounsfeildDlg = RCC_HounsfieldVisOptionsDlg (self)             
                    temphounsfeildDlg.updateHounsfeildDlgSettings (self.hounsfieldVisSettings)
                    temphounsfeildDlg.keyboardEvent (key)
                    temphounsfeildDlg.close ()
                self.update ()  
                
            elif (key == QtCore.Qt.Key_Plus) :
                #print ("Zoom in Pressed")
                self._CancelMoveOpperation ()
                self._ZoomInKeyEvent ()
                                
                
            elif (key == QtCore.Qt.Key_Minus) :
                #print ("Zoom out Pressed")
                self._CancelMoveOpperation ()
                self._ZoomOutKeyEvent ()
                
            elif (key == QtCore.Qt.Key_S or key == QtCore.Qt.Key_Backslash) :
                self._KeystrokeToggleUIROIFill ()

            elif (key == QtCore.Qt.Key_1) :
                self._hideROI(0)                        
                #self.ui.XYSliceView.setRangeSelectionEnabled (True)
                
            elif (key == QtCore.Qt.Key_2) :
                self._hideROI(1)
            elif (key == QtCore.Qt.Key_3) :
                self._hideROI(2)
            elif (key == QtCore.Qt.Key_4) :
                self._hideROI(3)
            elif (key == QtCore.Qt.Key_5) :
                self._hideROI(4)
            elif (key == QtCore.Qt.Key_6) :
                self._hideROI(5)
            elif (key == QtCore.Qt.Key_7) :
                self._hideROI(6)
            elif (key == QtCore.Qt.Key_8) :
                self._hideROI(7)
            elif (key == QtCore.Qt.Key_9) :
                self._hideROI(8)
            elif (key == QtCore.Qt.Key_0) :
                self._hideROI(9)
            elif (key == QtCore.Qt.Key_H) :
                self._hideROI(None)
            elif (key == QtCore.Qt.Key_I) :
                self._hideROI(-1)
            elif (key == QtCore.Qt.Key_L) :
                self._lockROI(None)
            elif (key == QtCore.Qt.Key_P) :
                self._lockROI(-1)
            elif (key == QtCore.Qt.Key_BracketLeft and not self.ROIDictionary.isROIinPerimeterMode ()) :                
                penSettings = self.getPenSettings ()
                size = penSettings.getPenSize ()
                if size > 1 :                     
                    penSettings.setPenSize (size - 1)
                    self.updatePaintBrushIfNecessary ()
                    self.update ()
                    if self._PenSettingsDlg is not None :
                        self._PenSettingsDlg.ui.PenSize.setValue (penSettings.getPenSize ())
                    
            elif (key == QtCore.Qt.Key_BracketRight and not self.ROIDictionary.isROIinPerimeterMode ()) :                
                penSettings = self.getPenSettings ()
                size = penSettings.getPenSize ()
                if size < 500 :                     
                    penSettings.setPenSize (size + 1)
                    self.updatePaintBrushIfNecessary ()
                    if self._PenSettingsDlg is not None :
                        self._PenSettingsDlg.ui.PenSize.setValue (penSettings.getPenSize ())
                    self.update ()
    
    def _lockROI (self, roiIndex):        
        if (self.ROIDictionary is None) :
            return
        namelist = self.ROIDefs.getNameLst ()
        if roiIndex is None or roiIndex == -1 :           
           changed = False
           
           AllSame = False
           islocked = None
           for name in namelist :
               if self.ROIDictionary.isROIDefined (name) :
                   if islocked is None :
                       islocked = self.ROIDefs.isROILocked (name)
                       AllSame = True
                   elif islocked != self.ROIDefs.isROILocked (name) :
                       AllSame = False
                       break
                   
           for name in namelist :
               if self.ROIDictionary.isROIDefined (name) :                   
                   islocked = self.ROIDefs.isROILocked (name)                   
                   if (roiIndex is None and not AllSame) :
                       if not islocked :
                           self.ROIDefs.setROILocked (name, not islocked)            
                           changed = True
                   elif (roiIndex == -1 or AllSame) :
                       self.ROIDefs.setROILocked (name, not islocked)            
                       changed = True                   
           if (changed) :
               self.setROIHiddenLabel ()
               self.update ()
        elif roiIndex >= 0 and roiIndex < len (namelist) :
            name = namelist[roiIndex]
            if self.ROIDictionary.isROIDefined (name) :
                islocked = self.ROIDefs.isROILocked (name)
                self.ROIDefs.setROILocked (name, not islocked)            
                self.setROIHiddenLabel ()
                self.update ()
        self.updateUIToRefectSelectedROIHiddenLockedState ()
                                        
    def _hideROI (self, roiIndex):        
        if (self.ROIDictionary is None) :
            return
        namelist = self.ROIDefs.getNameLst ()
        if roiIndex is None or roiIndex == -1 :           
           changed = False
           
           AllSame = False
           ishidden = None
           for name in namelist :
               if self.ROIDictionary.isROIDefined (name) :
                   if ishidden is None :
                       ishidden = self.ROIDefs.isROIHidden (name)
                       AllSame = True
                   elif ishidden != self.ROIDefs.isROIHidden (name) :
                       AllSame = False
                       break
                   
           for name in namelist :
               if self.ROIDictionary.isROIDefined (name) :                   
                   ishidden = self.ROIDefs.isROIHidden (name)                   
                   if (roiIndex is None and not AllSame) :
                       if not ishidden :
                           self.ROIDefs.setROIHidden (name, not ishidden, AutoLock = self.visSettings.shouldAutoLockROIWhenHidden ())      
                           changed = True
                   elif (roiIndex == -1  or AllSame) :
                       self.ROIDefs.setROIHidden (name, not ishidden, AutoLock = self.visSettings.shouldAutoLockROIWhenHidden ())            
                       changed = True                   
           if (changed) :
               self.setROIHiddenLabel ()
               self.update ()
        elif roiIndex >= 0 and roiIndex < len (namelist) :
            name = namelist[roiIndex]
            if self.ROIDictionary.isROIDefined (name) :
                ishidden = self.ROIDefs.isROIHidden (name)
                self.ROIDefs.setROIHidden (name, not ishidden, AutoLock = self.visSettings.shouldAutoLockROIWhenHidden ())            
                self.setROIHiddenLabel ()
                self.update ()
        self.updateUIToRefectSelectedROIHiddenLockedState ()
        
    # returns keyboard state for select keys monitored using the application event handler 
    def getKeyState (self, key):
        if (key == QtCore.Qt.Key_Shift):
              return  (QtCore.Qt.ShiftModifier & QApplication.queryKeyboardModifiers () == QtCore.Qt.ShiftModifier)                      
        elif (key == QtCore.Qt.Key_Control) :
            return  (QtCore.Qt.ControlModifier & QApplication.queryKeyboardModifiers () == QtCore.Qt.ControlModifier)                      
        elif (key == QtCore.Qt.Key_Alt) :
            return  (QtCore.Qt.AltModifier & QApplication.queryKeyboardModifiers () == QtCore.Qt.AltModifier)                      
        elif (key not in self._KeyboardState) :
            return False
        else:
            return self._KeyboardState[key]
        
    def _setPaintBrush (self, brush) :
        self.ui.XYSliceView.setPaintBrush (brush)
        self.ui.YZSliceView.setPaintBrush (brush)
        self.ui.XZSliceView.setPaintBrush (brush)
        
    def updatePaintBrushIfNecessary (self) :
       if (self.ROIDictionary is None) :
           self._setPaintBrush (None)           
       elif (self.ROIDictionary.isROIinPerimeterMode ()) :
           self._setPaintBrush (None)
       elif (not self.isContourXYUIState ()) :
           self._setPaintBrush (None)
       elif (not self.isROIDictonaryFileLoaded ()):
           self._setPaintBrush (None)
       elif (self.ROIDictionary.isReadOnly ()) :  
           self._setPaintBrush (None)
       else:           
           ROINameLst = self.ROIDefs.getSelectedROI ()
           if (len (ROINameLst) == 1 and self.ROIDefs.isROIArea (ROINameLst[0])) :
               if not self.ROIDefs.isROIHidden (ROINameLst[0]) and not self.ROIDefs.isROILocked (ROINameLst[0]) : 
                   self._setPaintBrush ((self.getPenSettings ()))
               else:
                   self._setPaintBrush (None)
           else:
               self._setPaintBrush (None)
       #self.ui.XYSliceView.update () 
     
    def _silentClearROIList (self) :
        try :    
            self.ui.ROI_List.selectionModel().selectionChanged.disconnect(self.ROIListSelectionChanged) 
            disconnected = True
        except :
            disconnected = False    
        self.ui.ROI_List.clear ()                    
        if (disconnected) :
            self.ui.ROI_List.selectionModel().selectionChanged.connect(self.ROIListSelectionChanged) 
    
    def _silentClearContourList (self) :
        try :    
            self.ui.ContourList.selectionModel().selectionChanged.disconnect(self.ContourListSelectionChanged)
            disconnected = True
        except :
            disconnected = False    
        self.ui.ContourList.clear ()                    
        if (disconnected) :
             self.ui.ContourList.selectionModel().selectionChanged.connect(self.ContourListSelectionChanged)
    
    def isROIDictonaryFileLoaded (self) :
        return self.ROIDictionary is not None and self.ROIDictionary.getROIFileHandle () is not None
    
    
    def setAxisProjectionControl (self, projectionControls) :
        self.ui.XYSliceView.setAxisProjectionControls (projectionControls)
        self.ui.XZSliceView.setAxisProjectionControls (projectionControls)
        self.ui.YZSliceView.setAxisProjectionControls (projectionControls)
     
    # DatasetTreeSelectionChanged called when dataset selection tree UI selection is changed    
    def DatasetTreeSelectionChanged (self, niftiDataSetTreeNode, NIfTIFilePath = None) :     
        try : 
            self.setEnabled (False)
            self.setWindowUpdateEnabled (False)
            self.ui.XYSliceSelectorWidget.setFocus ()
            self._ProjectDataset.getApp().processEvents()
                        
            if  self.ROIDictionary is  None :
                return                           
            oldSliceCoordinate = None        
            if (self.NiftiVolumeTreeSelection is not None and self.ROIDefs is not None) :
                self._datasetChangeOldSelectedROINameLst = self.ROIDefs.getSelectedROI ()
            else:
                self._datasetChangeOldSelectedROINameLst = []
            
            self.setAxisProjectionControl (None)
          
            
            OldLoadedFileHandle = None     
            ReloadingSameDataset = False
            if (self.NiftiVolumeTreeSelection is not None) :  # If dataset is loaded then save currently loaded dataset before loading the new one 
                if (self.NiftiVolumeTreeSelection == niftiDataSetTreeNode) :
                   ReloadingSelf = True
                elif (self.getLoadedNIfTIFile () is not None and self.getLoadedNIfTIFile ().getFilePath () == NIfTIFilePath) :
                   ReloadingSelf = True
                else:  
                   ReloadingSelf = False   
                if not ReloadingSelf : 
                    self.showFlattenDeepGrowDlg ("One click segmentations must either be flattend or removed before closing an series annotation.",Cancel = False)
                oldSliceCoordinate = (int (self.SelectedCoordinate.getXCoordinate ()), int (self.SelectedCoordinate.getYCoordinate ()), int (self.SelectedCoordinate.getZCoordinate ())) # save the old slice coordiante so the same slice can be shown in the new view if the new dataset has the slice
                self.ROIDictionary.saveROIDictionaryToFile () # Save current ROI     
                OldLoadedFileHandle = self.ROIDictionary.getROIFileHandle ()                
                if (OldLoadedFileHandle is not None) :
                    OldLoadedFileHandle.close ()                                        
                del OldLoadedFileHandle
                if (ReloadingSelf) :
                    print ("Reloading Self")                    
                else:                
                    self._lastOpenedNIfTIDataFile = None
            elif (self.getLoadedNIfTIFile () is not None and self.getLoadedNIfTIFile ().getFilePath () == NIfTIFilePath) :
                self.ROIDictionary.saveROIDictionaryToFile () # Save current ROI     
                OldLoadedFileHandle = self.ROIDictionary.getROIFileHandle ()                
                if (OldLoadedFileHandle is not None) :
                    OldLoadedFileHandle.close ()  
                del OldLoadedFileHandle                                      
                print ("Reloading Self")                    
            else:
                self._lastOpenedNIfTIDataFile = None
                             
            if (niftiDataSetTreeNode is None and NIfTIFilePath is None) :
                self._lastOpenedNIfTIDataFile = None
                self.LoadNiftiViewer (None, "Data not selected", None, Orientation = "None")
                self.NiftiVolumeTreeSelection = None     
                self.ROIDictionary.clear ()
                self._setPaintBrush (None)
                self.UpdateDatasetTagEditor ()
                return
            
            niftiDataFile = self.getLoadedNIfTIFile ()
            if (niftiDataFile is None):
                if niftiDataSetTreeNode is not None :
                    niftiDataFile = niftiDataSetTreeNode.getNIfTIDatasetFile()        
                elif NIfTIFilePath is not None :
                    niftiDataFile = FileSystemDF (None, NIfTIFilePath = NIfTIFilePath)
                self._lastOpenedNIfTIDataFile = niftiDataFile
            
            #carry across hidden ROI settings.
            tempHiddenROILst = []
            tempLockedROILst = []
            roidefs = self.ROIDictionary.getROIDefs ()
            if (roidefs is not None) :
                nameLst = roidefs.getNameLst ()
                for roiName in nameLst :
                    if roidefs.isROIHidden (roiName) :
                        tempHiddenROILst.append (roiName)
                    if roidefs.isROILocked (roiName) :
                        tempLockedROILst.append (roiName)
                    
            self.ui.XYSliceView.setROIObjectList (None) 
            self.ui.XZSliceView.setROIObjectList (None)
            self.ui.YZSliceView.setROIObjectList (None)
            self.ui.XYSliceSelectorWidget.setROISliceDictionary (None)
            self.ui.XYSliceVerticalSelector.setROISliceDictionary (None, None, None)
            self.ui.XZSliceVerticalSelector.setROISliceDictionary (None, None, None)
            self.ui.YZSliceVerticalSelector.setROISliceDictionary (None, None, None)
            
            if (not niftiDataFile.hasDataFile ()) : 
                self.ROIDictionary.clear ()  
            else:   
                try :
                    path = niftiDataFile.getFilePath ()
                    validFile = path is not None and os.path.isfile(path) and (path.lower ().endswith (".nii.gz") or path.lower().endswith (".nii"))
                except:
                    validFile = False
                if not validFile :
                    self.ROIDictionary.clear () 
                else:                           
                    print (path)                        
                    progdialog = RCC_NoCancelProgressDlg(self)                
                    try :
                        previouslyReadOnly = False
                        rccfilename = None                    
                        #progdialog.setMinimumDuration (0)
                        progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)                
                        if (not ReloadingSameDataset) :
                            progdialog.setWindowTitle ("Loading Dataset")
                        else:
                            progdialog.setWindowTitle ("Reorienting Dataset")
                        progdialog.setWindowModality(QtCore.Qt.ApplicationModal)                
                        progdialog.setRange(0,0)  
                        try :  
                            if self._ProjectDataset.areLongFileNamesShown () :
                                displayname = LongFileNameAPI.getLongFileNamePath (path)                        
                            else:
                                displayname = path
                        except:
                            displayname = path
                            
                        progdialog.setLabelText ("Opening NIfTI: " + displayname)
                        progdialog.show ()
                        #progdialog.setMinimumWidth(300)
                        #progdialog.setMinimumHeight(100)
                        #progdialog.forceShow()                  
                        self._ProjectDataset.getApp().processEvents()
                        if (niftiDataSetTreeNode is not None) :
                            if self._ProjectDataset.isPesscaraInterface () :
                                DisplayText = niftiDataSetTreeNode.getSubjectName () + ";   " + niftiDataSetTreeNode.getExamName ()  + ";   " + niftiDataSetTreeNode.getSeriesName ()
                            else:                                
                                pathList = niftiDataSetTreeNode.getTreePathList (ReturnFileSystemPath = not self._ProjectDataset.areLongFileNamesShown ())   
                                if len (pathList) > 3 :
                                    pathList = pathList[-3:]                                
                                DisplayText = ";   ".join (pathList)
                                                        
                        elif NIfTIFilePath is not None :
                            DisplayText = NIfTIFilePath
                            
                        overideAutomaticDefaultOrientation = None
                        if ( ReloadingSameDataset and self.ROIDictionary.isROIinAreaMode ()) :
                           rccfilename = self.ROIDictionary.getROIFileHandle () 
                           #tempdictionary = self.ROIDictionary
                                                  
                           
                           #self.ROIDictionary = None
                           loadedNiftiOrientation = self.ui.contourOrientationComboBox.currentText()               
                        elif ( ReloadingSameDataset and self.ROIDictionary.isROIinPerimeterMode () and self.ROIDictionary.ROICountDefined () == 0) :
                           rccfilename = self.ROIDictionary.getROIFileHandle () 
                           #tempdictionary = self.ROIDictionary
                           #self.ROIDictionary = None
                           loadedNiftiOrientation = self.ui.contourOrientationComboBox.currentText()   
                        else:
                            rccfilename = None
                            if niftiDataSetTreeNode is None and NIfTIFilePath is not None :                                                        
                                loadedNiftiOrientation = self.ui.contourOrientationComboBox.currentText()   
                                self.ROIDefs.reloadROIDefs ()        
                                self._silentClearROIList ()
                                self._silentClearContourList ()                                    
                                self._setListUI (self.ui.ROI_List, self.ROIDefs)            
                                self.ui.XYSliceView.disableUpdateSlice ()
                                self.ui.XZSliceView.disableUpdateSlice ()
                                self.ui.YZSliceView.disableUpdateSlice ()
                                self.ROIDictionary.clear ()       
                                self.ui.XYSliceView.enableUpdateSlice (UpdateSlice = False)
                                self.ui.XZSliceView.enableUpdateSlice (UpdateSlice = False)
                                self.ui.YZSliceView.enableUpdateSlice (UpdateSlice = False)        
                                if self.ui.XYSliceView.getNIfTIVolume ().getSliceDimensions()[2] > 1 : 
                                    projectionControls = AxialAxisProjectionControls (self.ui.XYSliceView)
                                    self.setAxisProjectionControl (projectionControls)
                            else:
                                try :                                                                                
                                    rccfilename = self._ProjectDataset.getROIDatasetInterface ().getROIDataForDatasetPath (niftiDataSetTreeNode, UserNameStr = self._ProjectDataset.getUserName ()) #file handle closing mannaged by dictionary
                                     
                                    #update datasetselectionui for the selected dataset for File system based datasets
                                    try :
                                        niftiInterface = self._ProjectDataset.getNIfTIDatasetInterface ()
                                        if niftiInterface is not None and niftiInterface.getInterfaceName () == "RCC_NiftiFileSystemDataInterface":
                                            eventHandler = niftiInterface.getFileSysetmEventHandler ()
                                            if eventHandler is not None :
                                                ROIFileName = rccfilename.getPath ()
                                                nonpersistantLockPath = NonPersistentFSLock.getFileLockPathForROIFilePath (ROIFileName)
                                                eventHandler.processEvent (nonpersistantLockPath)  
                                                pLockPath = PersistentFSLock.getPersistentLockPathForROIFilePath (ROIFileName)
                                                eventHandler.processEvent (pLockPath)  
                                                pLockPath = PersistentFSLock.getPersistentLockPathForROIFilePath (ROIFileName)
                                                cachPath = self._ProjectDataset.getProjectFileInterface ().getFileCompressedCachePathFromROIFilePath (ROIFileName)
                                                if os.path.exists (cachPath) : 
                                                    eventHandler.processEvent (cachPath)  
                                    except:
                                        print ("Error occured in updating dataseui from lock, plock, and cache files.")
                                    
                                    
                                    self.ROIDictionary.removeTagListener () 
                                    #self.ROIDictionary.clear (KeepFileHandleOpen = rccfilename)  
                                    #tempdictionary = self.ROIDictionary.getBlankDictionary ()                                      
                                    if (rccfilename is  None) :                                         
                                        loadedNiftiOrientation = "Default"          
                                        self._lastOpenedNIfTIDataFile = None
                                        loadedNiftiOrientation = "Default"    
                                        self.ROIDictionary.clear ()        
                                    else:
                                        #clear ROI list before loading.  To initalize ROI List with Project ROI.
                                                                                                        
                                        self.ROIDefs.reloadROIDefs ()          
                                        self._silentClearROIList ()
                                        self._silentClearContourList ()                                    
                                        self._setListUI (self.ui.ROI_List, self.ROIDefs,  selectedNameList = self._datasetChangeOldSelectedROINameLst) 
                                        rcPath = rccfilename.getPath ()
                                        _, fname = os.path.split (rcPath)
                                        self._ProjectDataset.getApp().processEvents()
                                        progdialog.setLabelText ("Opening ROI: " + fname)
                                        self._ProjectDataset.getApp().processEvents()
                                        
                                        self.ui.XYSliceView.disableUpdateSlice ()
                                        self.ui.XZSliceView.disableUpdateSlice ()
                                        self.ui.YZSliceView.disableUpdateSlice ()
                                        previouslyReadOnly =  self.ROIDictionary.isReadOnly () 
                                        
                                        self.ROIDictionary.loadROIDictionaryFromFile (rccfilename, self._ProjectDataset.getDocumentUIDFileLogInterface (), CallDictionaryChangedListener = False,  ProjectDataset = self._ProjectDataset, ProgDialog = progdialog, ReloadROIDefs = False)                                            
                                        if (os.path.exists (rcPath) and not os.access (rcPath, os.W_OK)) :
                                            self.ROIDictionary.setHardReadOnlyMode ()
                                            MessageBoxUtil.showMessage ("Data Loaded Read Only", "Cannot write to loaded ROI Data file. Data loaded as readonly.")
                                        elif not ML_KerasModelLoaderInterface.isMLSupportEnabled () and self.ROIDictionary.hasDeepGrowAnnotations () :   
                                            self.ROIDictionary.setHardReadOnlyMode ()
                                            MessageBoxUtil.showMessage ("Data Loaded Read Only", "ML is dissabled. The loaded datafile contains unflattened one-click segmentations. ML must be enabled to edit.")
                                        
                                      
                                        projectionControls = AxialAxisProjectionControls (self.ui.XYSliceView)
                                        self.setAxisProjectionControl (projectionControls)
                                        
                                        
                                        self.ui.XYSliceView.enableUpdateSlice (UpdateSlice = False)
                                        self.ui.XZSliceView.enableUpdateSlice (UpdateSlice = False)
                                        self.ui.YZSliceView.enableUpdateSlice (UpdateSlice = False)
                                        self.ROIDictionary.setAxisProjection (self.ui.XYSliceView.getAxisProjectionControls ())
                                        
                                        if  self.ROIDictionary.ROICountDefined () == 0 or self.ROIDictionary.isROIinAreaMode() :
                                            loadedNiftiOrientation = self.ui.contourOrientationComboBox.currentText()
                                            if (self.ROIDictionary.ROICountDefined () > 0) :
                                                if (loadedNiftiOrientation == "Default") :
                                                    volumeDescription = self.ROIDictionary.getNIfTIVolumeOrientationDescription ()
                                                    if (volumeDescription != None and volumeDescription.hasParameter ("SliceAxis") and self.ROIDictionary.ROICountDefined () > 0) :                                 
                                                        overideAutomaticDefaultOrientation = volumeDescription.getParameter ("SliceAxis")
                                                
                                        else:
                                            volumeDescription = self.ROIDictionary.getNIfTIVolumeOrientationDescription ()
                                            if (volumeDescription != None and volumeDescription.hasParameter ("SliceAxis") and self.ROIDictionary.ROICountDefined () > 0) :                                 
                                                loadedNiftiOrientation = volumeDescription.getParameter ("SliceAxis")
                                                if (loadedNiftiOrientation != self.ui.contourOrientationComboBox.currentText()) :
                                                    oldSliceCoordinate = None                                                                           
                                            elif (self.ROIDictionary.ROICountDefined () > 0) :
                                                loadedNiftiOrientation = "Default"
                                            elif (not self.ui.contourOrientationComboBox.isEnabled ()) :
                                                loadedNiftiOrientation = "Default"                                    
                                            else:                
                                                loadedNiftiOrientation = self.ui.contourOrientationComboBox.currentText()                                                                                                                       
                                except:                            
                                    rccfilename = None
                                    self._lastOpenedNIfTIDataFile = None
                                    loadedNiftiOrientation = "Default"    
                                    self.ROIDictionary.clear (KeepFileHandleOpen = rccfilename)                                                    
                            #tempdictionary.delete (KeepFileHandleOpen = True) 
                            #tempdictionary = None
                        
                        if (self.getLoadedNIfTIFile () is not None) :
                            fnamePathDir, fname = os.path.split (path)
                            self._ProjectDataset.getApp().processEvents()
                            if (ReloadingSameDataset):
                                progdialog.setLabelText ("Rotating NIfTI")
                            else:                                
                                if self._ProjectDataset.areLongFileNamesShown () :
                                    try :                                
                                        lfapi = LongFileNameAPI (fnamePathDir)
                                        displayname = lfapi.getLongFileName (fname)
                                        if displayname is None :
                                            displayname = fname
                                    except:
                                        displayname = fname
                                else:
                                    displayname = fname
                                progdialog.setLabelText ("Opening NIfTI: " + displayname)
                            self._ProjectDataset.getApp().processEvents()
                            
                            if (overideAutomaticDefaultOrientation is None) :
                                overideAutomaticDefaultOrientation = loadedNiftiOrientation
                            
                            self.LoadNiftiViewer (path, DisplayText, niftiDataSetTreeNode, Orientation = overideAutomaticDefaultOrientation, RotateCurrentlyLoadedNiftiToNewOrientation = ReloadingSameDataset)                            
                            if self.NiftiVolume is not None :      
                                if self.NiftiVolume.getSliceDimensions ()[2] <= 1 :
                                    self.setAxisProjectionControl (None)
                                if (self.NiftiVolumeTreeSelection is not None and rccfilename is not None) :                                 
                                    print ("Loading %s" % (rccfilename.getPath ())) 
                                    
                                    volumeDescription = self.ROIDictionary.getNIfTIVolumeOrientationDescription ()                            
                                    if (volumeDescription != None and volumeDescription.hasParameter ("SliceAxis")) :
                                            contourOrientation = volumeDescription.getParameter ("SliceAxis")                                            
                                            if (self.NiftiVolume.getSliceAxis () != contourOrientation) :     
                                                if (self.ROIDictionary.ROICountDefined () > 0 and self.ROIDictionary.isROIinAreaMode ()) :
                                                    self.ROIDictionary.reorientContourVolumeToMatchNiftiVolume (self.NiftiVolume, FileSaveThreadManger = None, ProjectDataset = self._ProjectDataset)  #disable MP options.. Running in processes is slower.. (sad)                                      
                                                    if (oldSliceCoordinate is not None) :
                                                        progdialog.setLabelText ("Reorienting Data")
                                                        oldSliceCoordinate = self.NiftiVolume.reorientPointToMatchVolumeOrientation (oldSliceCoordinate, contourOrientation)
                                                            
                                startTime = time.time ()
                                progdialog.setLabelText ("Setting volume orientation")
                                self.ROIDictionary.setNIfTIVolumeOrientationDescription (self.NiftiVolume.getNIfTIVolumeOrientationDescription (), ProgDialog = progdialog, App = self._ProjectDataset.getApp())                                                                      
                                print ("Set Orientation Time: " + str (time.time () - startTime))
                                startTime = time.time ()
                                
                                progdialog.setLabelText ("Setting datasource description")
                                if (self.NiftiVolumeTreeSelection is not None) : 
                                     self.ROIDictionary.setNiftiDataSourceDescription (self.NiftiVolumeTreeSelection.getSaveFileNifitFileSourceFileObject ())                
                                print ("Data Source Description: " + str (time.time () - startTime))
                                startTime = time.time ()
                                temp = Coordinate ()
                                temp.setCoordinate ([int (self.NiftiVolume.getXDimSliceCount () /2), int (self.NiftiVolume.getYDimSliceCount () /2), int (self.NiftiVolume.getZDimSliceCount () /2)])
                                self.setDrawSelectedROIOnTop ()
                                self.ui.XYSliceView.setROIObjectList (self.ROIDictionary, SetCoordinate = temp) 
                                self.ui.XZSliceView.setROIObjectList (self.ROIDictionary, SetCoordinate = temp) 
                                self.ui.YZSliceView.setROIObjectList (self.ROIDictionary, SetCoordinate = temp) 
                                self.ui.XYSliceSelectorWidget.setROISliceDictionary (self.ROIDictionary, NIfTIVolume = self.NiftiVolume, AxisProjection = self.ui.XYSliceView.getAxisProjectionControls (), Coordinate = temp)
                                self.ui.XYSliceVerticalSelector.setROISliceDictionary (self.ROIDictionary, self.NiftiVolume, self._ProjectDataset,  AxisProjection = self.ui.XYSliceView.getAxisProjectionControls (), Coordinate = temp)
                                self.ui.XZSliceVerticalSelector.setROISliceDictionary (self.ROIDictionary, self.NiftiVolume, self._ProjectDataset)
                                self.ui.YZSliceVerticalSelector.setROISliceDictionary (self.ROIDictionary, self.NiftiVolume, self._ProjectDataset)
                                
                                self._ROIDictionaryChangeEvent = True                             
                                self._DisableSliceViewerSelectionChange ()
                                self.SelectedCoordinate.setCoordinate ([int (self.NiftiVolume.getXDimSliceCount () /2), int (self.NiftiVolume.getYDimSliceCount () /2), int (self.NiftiVolume.getZDimSliceCount () /2)])                                      
                                self.ui.XYSliceSelectorWidget.setSelectedSlice (int (self.NiftiVolume.getZDimSliceCount () /2))                                                                    
                                self.ui.XZSliceVerticalSelector.setSelectedSlice (int (self.NiftiVolume.getYDimSliceCount () /2))    
                                self.ui.YZSliceVerticalSelector.setSelectedSlice (int (self.NiftiVolume.getXDimSliceCount () /2))    
                                self.ui.XYSliceVerticalSelector.setSelectedSlice (int (self.NiftiVolume.getZDimSliceCount () /2))    
                                self._EnableSliceViewerSelectionChange ()
                                self.ROIDictionary.setCurrentUISlice (int (self.NiftiVolume.getZDimSliceCount () /2))  
                                
                                progdialog.setLabelText ("Setting views")                                
                                
                                print ("Setting Selected Slice: " + str (time.time () - startTime))
                                startTime = time.time ()
                                
                                progdialog.setLabelText ("Checking Volume")
                                self._ProjectDataset.getApp().processEvents()                                                    
                                                            
                                if (not ReloadingSameDataset) :
                                    #self.ROIDictionary.setProjectDataset (self._ProjectDataset)
                                    if self.ROIDictionary.hasDeepGrowAnnotations () and self.ROIDictionary.isROIinPerimeterMode () :
                                        self.showFlattenDeepGrowDlg ("Converting ROI masks to contours. One-Click segmentations must flattened or removed.",Cancel = False)
                                    self.ROIDictionary.verrifyDictionaryIsConsistentWithPerimeterAreaSettings ( self.NiftiVolume.getSliceDimensions (), self.ROIDefs, ProgressDialog = progdialog,  FileSaveThreadManger = self._ProjectDataset.getROISaveThread (), ProjectDataset = self._ProjectDataset)
                                    self.updatePaintBrushIfNecessary ()                            
                                
                                progdialog.setLabelText ("Finishing Loading")
                                self._ProjectDataset.getApp().processEvents()                                                    
                                print ("Checking Dictionary " + str (time.time () - startTime))
                                startTime = time.time ()
                                
                                roidefs = self.ROIDictionary.getROIDefs ()                            
                                if roidefs is not None :                           
                                    for roiName in roidefs.getNameLst () :
                                        if roiName in tempHiddenROILst :
                                            roidefs.setROIHidden (roiName, True, AutoLock = self.visSettings.shouldAutoLockROIWhenHidden ())
                                        if roiName in tempLockedROILst :
                                            roidefs.setROILocked (roiName, True)
                                
                                print ("Setting ROI Lock/Hide " + str (time.time () - startTime))
                                startTime = time.time ()
                                
                                self.ui.actionRegistration_log_window.setEnabled (self.ROIDictionary.hasANTSRegisteredROI ())
                                #self.ROIDictionary.setNIfTIVolumeOrientationDescription (self.NiftiVolume.getNIfTIVolumeOrientationDescription ())                                                            
                                      
                                self._setROIOrientationChangeEnabled ()
                                try :
                                   self.ui.contourOrientationComboBox.currentIndexChanged.disconnect (self.contourOrientationChanged)  
                                   OrientationChangeDisconnect = True
                                except:
                                   OrientationChangeDisconnect = False
                                self.ui.contourOrientationComboBox.setCurrentIndex(self.ui.contourOrientationComboBox.findData(loadedNiftiOrientation, QtCore.Qt.DisplayRole))
                                if loadedNiftiOrientation not in self._LoadedNiftiOrientation :
                                    self._LoadedNiftiOrientation.append (loadedNiftiOrientation)
                                if OrientationChangeDisconnect :
                                    self.ui.contourOrientationComboBox.currentIndexChanged.connect (self.contourOrientationChanged)
                                
                                self.hounsfieldVisSettings.setDataSetMaxRange (self.NiftiVolume.getMinMaxValues())                    
                                #self._setListUI (self.ui.ROI_List, self.ROIDefs, selectedNameList = self._datasetChangeOldSelectedROINameLst)                                                
                                clearSelection = True
                                for name in  self._datasetChangeOldSelectedROINameLst :
                                    if (self.ROIDefs.hasROI (name)) :
                                        self.ROIDefs.setSelectedROI (name, clearSelection = clearSelection)
                                        clearSelection = False                    
                                
                                print ("Setting ROI Lists: " + str (time.time () - startTime))
                                startTime = time.time ()
                                
                                self.UpdateDatasetTagEditor ()
                                self.updatePenSettingsDlg ()
                                
                                if (oldSliceCoordinate is not None) :
                                    self._DisableSliceViewerSelectionChange ()
                                    if (oldSliceCoordinate[2] >= 0 and oldSliceCoordinate[2] < self.NiftiVolume.getZDimSliceCount ()) :
                                        self.SelectedCoordinate.setZCoordinate (oldSliceCoordinate[2])
                                        self.ROIDictionary.setCurrentUISlice (int (oldSliceCoordinate[2]))    
                                    if (oldSliceCoordinate[0] >= 0 and oldSliceCoordinate[0] < self.NiftiVolume.getXDimSliceCount ()) :
                                        self.SelectedCoordinate.setXCoordinate (oldSliceCoordinate[0])
                                    if (oldSliceCoordinate[1] >= 0 and oldSliceCoordinate[1] < self.NiftiVolume.getYDimSliceCount ()) :
                                        self.SelectedCoordinate.setYCoordinate (oldSliceCoordinate[1])
                                    self._EnableSliceViewerSelectionChange ()
                                    
                                self._ROIDictionaryChangeEvent = False
                          
                                print ("Finish Loading: " + str (time.time () - startTime))
                                startTime = time.time ()
                                
                                readonlystate = self.ROIDictionary.isReadOnly () or self._LastReadOnlyMode
                                self.setUIReadOnlyMode (readonlystate)
                                if not readonlystate and previouslyReadOnly :
                                    self.setUIState ("Contour")
                                    self.setContourMode ()
                                
                                
                                self.ROIDictionary._CallDictionaryChangedListner (SkipFileSave = True)
                               
                                print ("Dictionary Change: " + str (time.time () - startTime))
                                startTime = time.time ()
                                #enable force saving to tactic if data loaded from there.                            
                                return                                    
                    finally :
                        try :
                            progdialog.close ()
                        except:
                            pass
            self.LoadNiftiViewer (None, "Data not selected", None, Orientation = "None")
            self.setAxisProjectionControl (None)
            self.NiftiVolumeTreeSelection = None        
            self._lastOpenedNIfTIDataFile = None
            self.UpdateDatasetTagEditor ()
        finally:
            print ("Updating Window")         
            self.updateUIToReflectAreaPerimeterDrawing ()
            self.setWindowUpdateEnabled (True)
            self.setEnabled (True)
            gc.collect()
        
    def _DisableSliceViewerSelectionChange (self) :
        for sliceView in [self.ui.XYSliceView, self.ui.XZSliceView, self.ui.YZSliceView] :
            sliceView.setSelectionChangeDisabled ()
    
    def _EnableSliceViewerSelectionChange (self) :
        for sliceView in [self.ui.XYSliceView, self.ui.XZSliceView, self.ui.YZSliceView] :
            sliceView.setSelectionChangeEnabled ()
    
    def contourOrientationChanged (self) :       
        if self.NiftiVolume is not None :
            if self.ui.contourOrientationComboBox.currentText () not in self._LoadedNiftiOrientation : 
                if not self.showFlattenDeepGrowDlg ("One-click generated segmentations must be flattened before the imaging can be re-oriented. Do you wish to flatten the segmentation?") : 
                   try :
                       self.ui.contourOrientationComboBox.currentIndexChanged.disconnect (self.contourOrientationChanged)
                       OrientationChangeDisconnect = True
                   except:
                       OrientationChangeDisconnect = False
                   if "Default" in self._LoadedNiftiOrientation :
                       contourOrientation = "Default"
                   elif len (self._LoadedNiftiOrientation) > 0 :
                       contourOrientation = self._LoadedNiftiOrientation[0]
                   else:
                       volumeDescription = self.ROIDictionary.getNIfTIVolumeOrientationDescription ()                            
                       if (volumeDescription != None and volumeDescription.hasParameter ("SliceAxis")) :
                           contourOrientation = volumeDescription.getParameter ("SliceAxis")            
                       else:
                           contourOrientation = "Default"
                   self.ui.contourOrientationComboBox.setCurrentIndex(self.ui.contourOrientationComboBox.findData(contourOrientation, QtCore.Qt.DisplayRole))
                   if OrientationChangeDisconnect :
                      self.ui.contourOrientationComboBox.currentIndexChanged.connect (self.contourOrientationChanged)
                else:
                    NIfTIFilePath = None
                    if (self.getLoadedNIfTIFile () is not None):
                        NIfTIFilePath = self.getLoadedNIfTIFile ().getFilePath ()
                    path = self._getPreviewSegmentationMaskPath ()
                    self.DatasetTreeSelectionChanged (self.NiftiVolumeTreeSelection, NIfTIFilePath)
                    if path is not None :
                        self._setPreviewSegmentationMaskPath (path)
                    self.reloadPreviewSegmentationMask (path)
     
    
    # call back called from XYSlice view when zoom is changed used to update/set zoom label UI
    def ZoomChanged (self,  SliceView):     
        precentShown = SliceView.getPrecentShown ()                
        if (precentShown < 1) :            
            if SliceView.getSliceAxis () == "Z" : 
                SliceView.setZoomLabel ("Zoomed: %.1f%% visible" % (100.0*precentShown))
            else:
                SliceView.setZoomLabel ("%.1f%%" % (100.0*precentShown))
        else:
            SliceView.setZoomLabel ("")                            
        self._getPaintToolsDlg().setButtonEnabledState (ZoomOut = self.ui.XYSliceView.isZoomed () or self.ui.XZSliceView.isZoomed () or self.ui.YZSliceView.isZoomed ())
        if SliceView.getSliceAxis () == "Z" : 
            self._XYFrameResize (None)
        elif SliceView.getSliceAxis () == "X" : 
            self._YZFrameResize (None)
        elif SliceView.getSliceAxis () == "Y" : 
            self._XZFrameResize (None)
        #print ("ZoomChanged.update")
        self.update ()
        
    # SelectedCoordinatChanged call back called by self.SelectedCoordinate when SelectedCoordinate is changed.
    def SelectedCoordinatChanged (self, coordinate, Event = None): # new coordinate   
        coordinateChange = False        
        if (coordinate.isXCoordinateChanged ()):                #if xCoordinate is changed update NIfTI slice views
            xc = coordinate.getXCoordinate ()           
        else:
            xc = None        
        if (coordinate.isYCoordinateChanged ()):               #if yCoordinate is changed update NIfTI slice views
            yc = coordinate.getYCoordinate ()            
        else:
            yc = None             
        if (coordinate.isZCoordinateChanged ()):               #if zCoordinate is changed update NIfTI slice views, sliceselector widget, and textbox
            zc = coordinate.getZCoordinate ()     
        else:
            zc = None
            
        self._updateSliceViewCoord (cX = xc, cY = yc, cZ = zc)        
        
        if xc is not None :        
            coordinateChange = True            
            self.ui.YZSliceAxisSliceTextBox.setText (str (xc))
            self.ui.YZSliceVerticalSelector.setSelectedSlice (xc, DisableCallbacks = [self.YZSliceSelectorUpdateSliceSelection])    
            if (self.ROIDictionary is not None) :
                self.ui.YZSliceView.setROIObjectList (self.ROIDictionary)

        if yc is not None :        
            coordinateChange = True
            self.ui.XZSliceAxisSliceTextBox.setText (str (yc))
            self.ui.XZSliceVerticalSelector.setSelectedSlice (yc, DisableCallbacks = [self.XZSliceSelectorUpdateSliceSelection])    
            if (self.ROIDictionary is not None) :
                self.ui.XZSliceView.setROIObjectList (self.ROIDictionary)
        if zc is not None  :
            coordinateChange = True               
            self.ui.XYSliceSelectorWidget.setSelectedSlice (zc, DisableCallbacks = [self.sliceSelectorUpdateSliceSelection])
            self.ui.MajorAxisSliceTextBox.setText (str (zc))     
            self.ui.XYSliceVerticalSelector.setSelectedSlice (zc, DisableCallbacks = [self.sliceSelectorUpdateSliceSelection])                        
            self._UpdateROISliceUIandSelection ()            # Additional UI updates              
            if (self.ROIDictionary is not None) :
                self.ROIDictionary.setCurrentUISlice (zc)
                self.ui.XYSliceView.setROIObjectList (self.ROIDictionary)     
                          
        
        # Sets currently selected slice visible ROI objects
        if coordinateChange :
            if (self.ROIDictionary is not None) :            
                self._updateContourSelectionList ()        
            self.update ()
        # if Z coordinate changed and in selection mode then set the selected ROI to match the ROI in the slice
        """if (coordinate.isZCoordinateChanged () and self.ROIDictionary is not None):               #if zCoordinate is changed update NIfTI slice views, sliceselector widget, and textbox
            ROISliceList = self.ui.XYSliceSelectorWidget.setROISliceDictionary (self.ROIDictionary)
            if (len (ROISliceList) == 1 and self.isSelectionUIState ()) :
                roi = ROISliceList[0]
                if (roi.isROIArea ()) :
                    roi.setSliceNumber (coordinate.getZCoordinate ())                      
                    self.ROIDefs.setSelectedROI (roi.getName (self.ROIDefs), clearSelection = True)
                else: # point object
                    pointIDList = roi.getSlicePointIDList (coordinate.getZCoordinate ())
                    if (len (pointIDList) > 0) :
                        firstPointID = pointIDList[0]
                        roi.setLastModifiedPointID (firstPointID)
                        self.ROIDefs.setSelectedROI (roi.getName (self.ROIDefs), clearSelection = True)"""
        
    def updateStatsDlg (self) :        
          if (self._StatsDlg == None or not self._StatsDlg.isVisible () or self.ROIDictionary is  None) :
              return                        
          self._StatsDlg.updateDlg (self.NiftiVolume, self.ROIDictionary,  self.SelectedCoordinate)
                     
                     
    # Updates ROI, Contour, and other UI state based on current selections        
    def _UpdateROISliceUIandSelection (self) :        
        self.ui.ContourList.setEnabled (False)
        self.ui.SelectedContourLbl.setEnabled (False) 
        self.ui.deleteContourBtn.setEnabled (False)
        self.ui.AddContourBtn.setEnabled(False)
        self.ui.autoContourBtn.setEnabled (False)
        self.ui.contourSingleSliceBtn.setEnabled (False)
        self.ui.BatchContourBtn.setEnabled (False)     
        if (self.ROIDictionary is None) :
            return                               
        self.updateStatsDlg ()     
        self.updateTranslationDlg ()
        self.updateMLDatasetDescriptionDlg ()
        if self.ROIDefs.isROISelected () :                                
                ROILst =self.ROIDefs.getSelectedROI ()
                if (len (ROILst)  > 0):
                    BatchConturingEnabled = False
                    for name in ROILst :
                        if not self.ROIDefs.isROIArea (name) :
                            BatchConturingEnabled = False                            
                            break
                        else:
                            if not self.ROIDefs.isROIHidden (name) and not self.ROIDefs.isROILocked (name) and self.isROIDictonaryFileLoaded () and not self.ROIDictionary.isReadOnly () :
                                BatchConturingEnabled = True
                    self.ui.BatchContourBtn.setEnabled (BatchConturingEnabled)            
            
                if (self.ROIDefs.isOneROISelected ()) :
                    itemName =self.ROIDefs.getSelectedROI ()[0]
                    isAreaROI = self.ROIDefs.isROIArea (itemName)                              
                    HiddenORLocked =  self.ROIDefs.isROIHidden (itemName) or self.ROIDefs.isROILocked (itemName)
                    self.ui.AddContourBtn.setEnabled(not HiddenORLocked and self.isROIDictonaryFileLoaded () and not self.ROIDictionary.isReadOnly () and (self.ROIDefs.getROIType (itemName) == "Point" or self.ROIDictionary.isROIinPerimeterMode ()))      
                    self.ui.ContourList.setEnabled (self.isROIDictonaryFileLoaded ())                                        
                    self.ui.SelectedContourLbl.setEnabled (self.isROIDictonaryFileLoaded ())                
                    if self.ROIDictionary.isROIDefined (itemName) :                             
                        obj = self.ROIDictionary.getROIObject(itemName) 
                        sliceNumber = self.ui.XYSliceView.getSliceNumber ()                            # self.SelectedCoordinate.getZCoordinate ()                                                                                
                        self.ui.deleteContourBtn.setEnabled (obj.areSelectedContoursDeleteableInSlice (sliceNumber) and not self.ROIDefs.isROILocked (itemName) and self.isROIDictonaryFileLoaded () and not self.ROIDictionary.isReadOnly ())                         
                        if  isAreaROI :                        
                            self.ui.autoContourBtn.setEnabled (len (obj.getSelectedContours()) > 0 and not HiddenORLocked and self.isROIDictonaryFileLoaded () and not self.ROIDictionary.isReadOnly ())                        
                            self.ui.contourSingleSliceBtn.setEnabled (not HiddenORLocked and self.isROIDictonaryFileLoaded () and not self.ROIDictionary.isReadOnly ())                        
                            if obj.isSliceContoured (sliceNumber) :                                            
                                obj.setSliceNumber (sliceNumber)                                                        
                        else:
                            self.ui.autoContourBtn.setEnabled (not HiddenORLocked and self.isROIDictonaryFileLoaded () and not self.ROIDictionary.isReadOnly ())                        
        if (self.ui.ContourList.count () == 1):
            self.ui.ContourList.item(0).setSelected (True)
    
    def _visSettingsChanged (self) :
        self._setROIOrientationChangeEnabled ()
        text = self.ui.XYSliceView.getROIHiddenLabel ()
        if self.visSettings.getUITransparency () <= 50 :
            if "ROI transparency set very low" not in text  :
                self.setROIHiddenLabel ()
        else:
            if "ROI transparency set very low" in text  :
                self.setROIHiddenLabel ()
    

   
    
    def setROIHiddenLabel (self):        
        self.ui.XYSliceView.setROIHiddenLabel ("")
        self.ui.XYSliceView.setROILockedLabel ("")
        if self.NiftiVolume is None :
          return
                
    
        if self.visSettings.getUITransparency () <= 50 and self.visSettings.getDrawROI () :
            BaseTxt = "ROI transparency set very low"
        else:
            BaseTxt = ""
        if (not self.visSettings.getDrawROI ()):
            if BaseTxt == "" :
                BaseTxt = "ROI Not Displayed"
            else:
                BaseTxt += "; ROI Not Displayed"
                                
        HiddenROIList = []        
        LockedROIList = []
        if (self.ROIDefs is not None) :
            nameLst = self.ROIDefs.getNameLst ()
            for name in nameLst :   
                if self.ROIDefs.isROIHidden (name):
                    HiddenROIList.append (name)
                if self.ROIDefs.isROILocked (name):
                    LockedROIList.append (name)
                
        if (len (HiddenROIList) == 0) :
            self.ui.XYSliceView.setROIHiddenLabel (BaseTxt)            
        else:
            if BaseTxt == "" :
                self.ui.XYSliceView.setROIHiddenLabel ("Hidden ROI: " + ", ".join (HiddenROIList))                
            else:
                self.ui.XYSliceView.setROIHiddenLabel (BaseTxt + "; Hidden ROI: " + ", ".join (HiddenROIList))                
        if len (LockedROIList) == len (nameLst) :
            self.ui.XYSliceView.setROILockedLabel ("All ROI locked")      
        elif len (LockedROIList) > 0 :
            self.ui.XYSliceView.setROILockedLabel ("Locked ROI: " + ", ".join (LockedROIList))      
    


    # Called when ROIDictionary object is changed. Ie when ROI objects are changed.
    def ROIDictionaryChange (self):          
        # disables additonal ROIDictionaryChange events       
        if (self._ROIDictionaryChangeEvent or self.ROIDictionary is  None) :
            return                
        self._ROIDictionaryChangeEvent = True     
        ROIDictionary = self.ROIDictionary
        ROIDictionaryIsReadOnly = ROIDictionary.isReadOnly ()            
        descriptionEnabled = self.isROIDictonaryFileLoaded () and not ROIDictionaryIsReadOnly
        self.ui.scanPhaseLbl.setEnabled (descriptionEnabled)
        self.ui.ScanPhaseComboBox.setEnabled (descriptionEnabled)        
        
        ROIDictionary.setUndoRedoStackSize (self.visSettings.getUndoRedoStackSize ())        
        nameLst = self.ROIDefs.getSelectedROI ()   # get currently selected ROI (Name)
        self._setListUI (self.ui.ROI_List, self.ROIDefs, selectedNameList = nameLst, SetROIDefinedBackground = True)        
        self.setROIHiddenLabel ()
         
        if (self.ROIDefs.isOneROISelected ()) :     
            name = nameLst[0]
            color = self.ROIDefs.getROIColor (name)
            self.ui.SelectedROI.setText (name)
            palette = QtGui.QPalette()
            palette.setColor (self.ui.SelectedROI.foregroundRole(), color)
            self.ui.SelectedROI.setPalette(palette)            
            
            # updates currently selected coordinates            
            if ROIDictionary.isROIDefined (name) : 
                roiobj = ROIDictionary.getROIObject (name)        
                if (roiobj != ROIDictionary.getSelectedObject ()) :
                    ROIDictionary.setSelected (roiobj)        
                    if (self.visSettings.shouldMoveToLastEditedSliceOnROISelection ()) :
                        if (not roiobj.isROIArea () or roiobj.hasSlices ()) :                   
                            cord = roiobj.getCoordinate ()
                            if (cord is not None and cord.getZCoordinate () != -1) :
                                self.SelectedCoordinate.setCoordinate ([cord.getXCoordinate (), cord.getYCoordinate (), cord.getZCoordinate ()])                
            else:
                ROIDictionary.setSelected (None)            
        else:
            self.ui.SelectedROI.setText ("Multiple ROI")
            palette = QtGui.QPalette()
            palette.setColor (self.ui.SelectedROI.foregroundRole(), QtGui.QColor (0,0,0))
            self.ui.SelectedROI.setPalette(palette)   
                    
        self._updateContourSelectionList () 
        
        self.ui.UndoBtn.setEnabled (ROIDictionary.getUndoStackSize () > 0 and not ROIDictionaryIsReadOnly)
        self.ui.RedoBtn.setEnabled (ROIDictionary.getRedoStackSize () > 0 and not ROIDictionaryIsReadOnly)    
        self.updateUIToRefectSelectedROIHiddenLockedState ()                
        
        # Updates dataSet selection tree item bolding. Data set is bolded if dataset contains one or more ROI objects (point and/or area objects)
        DidTagsChange = False
        if (self.NiftiVolumeTreeSelection is not None and ROIDictionary.getROIFileHandle () is not None) :                        
            doContoursExist = (ROIDictionary.ROICountDefined () > 0 or ROIDictionary.isScanPhaseDefined ())
            self.NiftiVolumeTreeSelection.setROIDatasetIndicator (doContoursExist)           
            DidTagsChange = self.NiftiVolumeTreeSelection.setTags (ROIDictionary.getDataFileTagManager())            
            self._ProjectDataset.updateProjectDatasetTreeNodeFormat (self.NiftiVolumeTreeSelection)            
                
        # Set slice view objects       
        self.setDrawSelectedROIOnTop ()
        self.ui.XYSliceView.setROIObjectList (ROIDictionary)
        self.ui.XZSliceView.setROIObjectList (ROIDictionary)
        self.ui.YZSliceView.setROIObjectList (ROIDictionary)
        
      
        
        self.ui.XYSliceSelectorWidget.setROISliceDictionary (ROIDictionary, NIfTIVolume = self.NiftiVolume, AxisProjection = self.ui.XYSliceView.getAxisProjectionControls (), Coordinate = self.SelectedCoordinate)
        
        self.ui.XZSliceVerticalSelector.setROISliceDictionary (ROIDictionary, self.NiftiVolume, self._ProjectDataset)
        self.ui.YZSliceVerticalSelector.setROISliceDictionary (ROIDictionary, self.NiftiVolume, self._ProjectDataset)
        self.ui.XYSliceVerticalSelector.setROISliceDictionary (ROIDictionary, self.NiftiVolume, self._ProjectDataset, AxisProjection = self.ui.XYSliceView.getAxisProjectionControls (), Coordinate = self.SelectedCoordinate)
             
        phaseTxt = ROIDictionary.getScanPhaseTxt ()
        #print ("Current Dictionary Scan Phase: %s " % phaseTxt)
        if phaseTxt is None  :
            index = self.ui.ScanPhaseComboBox.findText("Unknown")
            self.ui.ScanPhaseComboBox.setCurrentIndex (index)
        elif (self.ui.ScanPhaseComboBox.currentText () != phaseTxt):
            index = self.ui.ScanPhaseComboBox.findText(phaseTxt)
            #print ("Setting Index: %d " % index)
            if (index != -1) :  
                self.ui.ScanPhaseComboBox.setCurrentIndex (index)
            else:
                self.ui.ScanPhaseComboBox.addItem (phaseTxt)
                index = self.ui.ScanPhaseComboBox.findText(phaseTxt)
                self.ui.ScanPhaseComboBox.setCurrentIndex (index)
                    
        self._setROIOrientationChangeEnabled ()    
        #Tag Change events call ROI dictionary change.  Update nifti dataset dialog
        if DidTagsChange :
            try:
                datainterface = self._ProjectDataset.getNIfTIDatasetInterface ()
                if (datainterface is not None and self.NiftiVolumeTreeSelection is not None):        
                    row = self.NiftiVolumeTreeSelection.getRow ()            
                    nodechanged = datainterface.createIndex(row, 0, self.NiftiVolumeTreeSelection)
                    datainterface.layoutAboutToBeChanged.emit ()            
                    datainterface.dataChanged.emit (nodechanged, nodechanged)
                    datainterface.changePersistentIndex(nodechanged, nodechanged)
                    datainterface.layoutChanged.emit ()            
            except:
                print ("A error occured trying update nifti dataset dialog")
        self.updatePenSettingsDlgsOneClickROI ()
        self.update ()
        
        self._ROIDictionaryChangeEvent = False    
        
    def _setROIOrientationChangeEnabled (self) :
        ROIDictionaryIsReadOnly = self.ROIDictionary.isReadOnly ()
        orientationChangeDisabled = self.visSettings.isImageOrientationChangeDisabled ()
        if (self.ROIDictionary.isROIinAreaMode ()) :
            self.ui.contourOrientationLbl.setEnabled (not ROIDictionaryIsReadOnly and not orientationChangeDisabled) 
            self.ui.contourOrientationComboBox.setEnabled (not ROIDictionaryIsReadOnly and not orientationChangeDisabled) 
        else:
            self.ui.contourOrientationLbl.setEnabled (self.ROIDictionary.ROICountDefined () == 0 and not ROIDictionaryIsReadOnly and not orientationChangeDisabled) 
            self.ui.contourOrientationComboBox.setEnabled (self.ROIDictionary.ROICountDefined () == 0 and not ROIDictionaryIsReadOnly and not orientationChangeDisabled) 
        
    # call back sets selected coordinate when nifti slice widget (XYSliceView, XZSliceView, or YZSliceView) is clicked on
    def updateSliceSelection (self, coordinate, Axis) :        
        if Axis == "X" :
            self.SelectedCoordinate.setXCoordinate (coordinate)
        elif Axis == "Y" :
            self.SelectedCoordinate.setYCoordinate (coordinate)
        elif Axis == "Z" :
            self.SelectedCoordinate.setZCoordinate (coordinate)
            
    def sliceSelectorUpdateSliceSelection (self, coordinate, Axis):        
        self.ui.XYSliceView.setSelected(True)
        self.updateSliceSelection (coordinate, Axis) 
      
        
    def XZSliceSelectorUpdateSliceSelection (self, coordinate, Axis):        
        self.ui.XZSliceView.setSelected(True)
        self.updateSliceSelection (coordinate, Axis) 
        
    def YZSliceSelectorUpdateSliceSelection (self, coordinate, Axis):        
        self.ui.YZSliceView.setSelected(True)
        self.updateSliceSelection (coordinate, Axis) 
    
    def getSelectedObjectsAndContours (self, cX, cY, cZ, niftiSliceWidgetSelection) :         
         if (self.ROIDictionary is not None and self.ROIDefs.isROISelected () and self.ROIDefs.isOneROISelected () and (not self.ROIDefs.hasSingleROIPerVoxel () or self.ROIDictionary.isROIinPerimeterMode ())): # if arrow selection tool is selected, test if object is selected                         
             itemName =self.ROIDefs.getSelectedROI ()[0]                                                 
             if (self.ROIDictionary.isROIDefined (itemName)) :  
                obj = self.ROIDictionary.getROIObject (itemName)
                objectList = self.ROIDictionary.getObjectsAtPoint (cZ, cX , cY, niftiSliceWidgetSelection, self.NiftiVolume, self._ProjectDataset, ObjList = [obj])                
                if len (objectList) > 0 :
                    _, contourList = objectList[0]                
                    if (len (contourList) > 0) :
                        if (self.getKeyState (QtCore.Qt.Key_Shift)) :
                             currentSelection = self.ROIDictionary.getROISelectedContours (itemName)
                             contourList = list (set (currentSelection + contourList))                             
                        return [(itemName, contourList)]                        
         roiList = self.ROIDictionary.getObjectsAtPoint (cZ, cX, cY, niftiSliceWidgetSelection, self.NiftiVolume, self._ProjectDataset, ObjList = niftiSliceWidgetSelection.getROIObjectList ())      
         if self.ROIDefs.hasSingleROIPerVoxel () and len (roiList) > 0 :
             found = False
             pointObjLst = []
             areaObjLst =  []
             for roiName in self.ROIDefs.getNameLst () :
                 if self.ROIDefs.isROIArea (roiName):
                     areaObjLst.append (roiName)
                 else:
                     pointObjLst.append (roiName)                         
             for roiName in pointObjLst + areaObjLst :
                 for roi in roiList : 
                      testroiName, contourList = roi               
                      if (roiName == testroiName) :
                          roiList = [roi]
                          found = True
                          break
                 if found :
                     break
         return roiList
         
    
    @staticmethod
    def _setSelectedContourToCurrentlySelectedContoursIfSubsetSelected (contourList, currentSelectedContourList, isShiftPressed) :
        if (len (currentSelectedContourList) > 0) :
            if (isShiftPressed) :
                return list(set(contourList + currentSelectedContourList))
            else:
                selectedSubset = True
                for selectedContour in currentSelectedContourList :
                    if selectedContour not in contourList :
                        selectedSubset = False
                        break
                if (selectedSubset) :  # selected ROI are a subset of  mouse selection return the subset
                    contourList = copy.copy (currentSelectedContourList)    
                else:
                    IsMouseSelectionEntireSelected = True
                    for selectedContour in contourList :
                        if selectedContour not in currentSelectedContourList :
                            IsMouseSelectionEntireSelected = False
                            break
                    if (IsMouseSelectionEntireSelected) :
                        contourList = copy.copy (currentSelectedContourList)    
                    
        return contourList
        
    # call back is called when user presses the mouse on (XYSliceView, XZSliceView, or YZSliceView) widget and is in the contouring state
    # tests if user has clicked on a visible ROI control point. if the user has clicked on one stores the objects selected control point 
    # in the object, via  obj.setSelectedBoundingBoxControlPoint(SelectedBoundingBoxControlPoint)   
    # returns true if control point selected
    def NiftiAxisMousePressed (self, cX, cY, niftiSliceWidget) :                             
           if self.ROIDictionary is  None :
               return
           self._SelectionMoveROIState = None                     
           self._LastMouseMoveROIState = (cX, cY, False)
           
           if self.isRangeSelectionCallBackSet () :
               if niftiSliceWidget.isRangeSelectionEnabled () :
                   self.setRangeSelectionCallBack (cX, cY)
               else:
                   cx,cy,cz = niftiSliceWidget.convertMouseMoveImageCoordiantesToXYZImageCoordinates (cX, cY)                                         
                   self.NiftiAxisSelection (cx, cy, cz, niftiSliceWidget.getSliceAxis (), True)  
               return
           
           elif (self.isContourXYUIState ()) :                
                obj = self.ROIDictionary.getSelectedObject ()
                if obj is not None :
                    itemName = obj.getName (self.ROIDefs)
                    if not self.ROIDefs.isROIHidden (itemName) :
                        if not self.getKeyState (QtCore.Qt.Key_Alt) :
                            if (obj.hasControlPoints () and self.visSettings.getShowROIBoundingBoxInEditMode () and not self.ROIDefs.isROILocked (itemName)):
                                ptSelected, SelectedBoundingBoxControlPoint = obj.isControlPointSelected (  niftiSliceWidget)
                                obj.setSelectedBoundingBoxControlPoint(SelectedBoundingBoxControlPoint)                               
                                if (ptSelected):
                                   self.ROIDictionary._SaveUndoPoint ()                                    
                                   return True  
                            if self.ROIDefs.isROIArea (itemName) and self._penSettings.getPenTool () == "DeepGrow" and not self.ROIDefs.isROILocked (itemName):                                
                                SelectedDeepGrowControlPoint = obj.isDeepGrowPointSelected ( niftiSliceWidget)                     
                                if SelectedDeepGrowControlPoint is not None  :                                                                       
                                   if niftiSliceWidget.getMouseButtonPressed () == QtCore.Qt.RightButton or self.getKeyState (QtCore.Qt.Key_Control) or self.getPenSettings().isErasePen() : 
                                       pt, selection = SelectedDeepGrowControlPoint[0], SelectedDeepGrowControlPoint[1]
                                       self.ROIDictionary._SaveUndoPoint ()
                                       obj.removeDeepGrowPoint (pt)  
                                       self.ROIDictionary._CallDictionaryChangedListner () 
                                       self.updatePenSettingsDlgsOneClickROI ()
                                       self.update ()
                                   else:
                                       previouslySelectedControlPoint = obj.getSelectedDeepGrowControlPoint ()
                                       pt, selection = SelectedDeepGrowControlPoint[0], SelectedDeepGrowControlPoint[1]
                                       Change = False
                                       if previouslySelectedControlPoint is None or previouslySelectedControlPoint[0].getUID () != pt :
                                           self.ROIDictionary._SaveUndoPoint ()
                                           Change = True
                                       obj.setSelectedDeepGrowControlPoint (pt, selection)  
                                       self.ROIDictionary._CallDictionaryChangedListner () 
                                       if Change :
                                           self.update ()
                                else:                                    
                                    SaveUndoPointCalled = False
                                    if self.hasOtherDeepGrowAnnotations (itemName) :
                                        flattendDeepGrowAnnotations, SaveUndoPointCalled = self.showFlattenDeepGrowDlg ("Existng ROI one-click generated segmentations must be flattened before a new ROI segmentation can be added. Do you wish to flatten the segmentation?", SaveUndoPoint = True, CallDictionaryChangeListener = False, ReturnSaveUndoPointCalled  = True)
                                        if not flattendDeepGrowAnnotations :
                                            return True
                                        
                                    sliceNumber = niftiSliceWidget.getSliceNumber()
                                    if not SaveUndoPointCalled :
                                        self.ROIDictionary._SaveUndoPoint ()
                                    if not obj.isDeepGrowSlicePredictionClipSliceSet (sliceNumber) :
                                        SliceShape = niftiSliceWidget.getSliceShape ()
                                        obj.setDeepGrowSlicePredictionClipSlice (sliceNumber, self.ROIDictionary.getLockedObjectClipMask (obj.getName (self.ROIDefs), sliceNumber, SliceShape))
                                        
                                    if niftiSliceWidget.getMouseButtonPressed () == QtCore.Qt.RightButton or self.getKeyState (QtCore.Qt.Key_Control) or self.getPenSettings().isErasePen() :
                                        obj.createDeepGrowControlPoint (cX, cY, niftiSliceWidget, self.NiftiVolume, Positive = False)
                                    else:
                                        obj.createDeepGrowControlPoint (cX, cY, niftiSliceWidget, self.NiftiVolume, Positive = True)
                                    obj.setSliceNumber (sliceNumber)
                                    self.ROIDictionary._CallDictionaryChangedListner () 
                                    self.updatePenSettingsDlgsOneClickROI ()
                                    self.update ()
                                return True  
                else:   
                    if self.ROIDefs.isROISelected () and self.ROIDefs.isOneROISelected () and not self.getKeyState (QtCore.Qt.Key_Alt) :
                        itemName = self.ROIDefs.getSelectedROI ()[0]
                        if not self.ROIDefs.isROIHidden (itemName) and not self.ROIDefs.isROILocked (itemName) and self.ROIDefs.isROIArea (itemName) and self._penSettings.getPenTool () == "DeepGrow" :   
                            SaveUndoPointCalled = False
                            if self.hasOtherDeepGrowAnnotations (itemName) :
                                flattendDeepGrowAnnotations, SaveUndoPointCalled = self.showFlattenDeepGrowDlg ("Existng ROI one-click generated segmentations must be flattened before a new ROI segmentation can be added. Do you wish to flatten the segmentation?", SaveUndoPoint = True, CallDictionaryChangeListener = False, ReturnSaveUndoPointCalled  = True)
                                if not flattendDeepGrowAnnotations :    
                                    return True
                            obj = MultiROIAreaObject (None, None, niftiSliceWidget.getSliceShape (),"Computer")    
                            if niftiSliceWidget.getMouseButtonPressed () == QtCore.Qt.RightButton or self.getKeyState (QtCore.Qt.Key_Control) or self.getPenSettings().isErasePen() :
                                obj.createDeepGrowControlPoint (cX, cY, niftiSliceWidget, self.NiftiVolume, Positive = False)
                            else:
                                obj.createDeepGrowControlPoint (cX, cY, niftiSliceWidget, self.NiftiVolume, Positive = True)
                            obj.setSliceNumber (niftiSliceWidget.getSliceNumber())
                            self.ROIDictionary.addROI (itemName, obj, SaveUndoPt = not SaveUndoPointCalled) 
                            self.updatePenSettingsDlgsOneClickROI ()
                            self.update ()
                            return True  
                 
           elif (self.isSelectionUIState ()) :    
               cx,cy,cz = niftiSliceWidget.convertMouseMoveImageCoordiantesToXYZImageCoordinates (cX, cY)    
               axisRotationControls = niftiSliceWidget.getAxisProjectionControls ()
               if axisRotationControls is not None :
                   if self.getKeyState (QtCore.Qt.Key_Control) :
                       if axisRotationControls.testAndSetMarkerSelection (niftiSliceWidget) :
                           #xDim, yDim ,_  = niftiSliceWidget.getNIfTIVolume().getSliceDimensions ()
                           #self.NiftiAxisSelection (int (xDim/2), int (yDim/2), cz, niftiSliceWidget.getSliceAxis (), True)  
                           return False
               roiList = self.getSelectedObjectsAndContours (cx,cy,cz, niftiSliceWidget)
               if (len (roiList) == 1) :
                   xMouseOffset, yMouseOffset, zMouseOffset = 0,0,0
                   itemName, contourList = roiList[0]                    
                   isAreaROI = self.ROIDefs.isROIArea (itemName)           
                  
                   #if an object is allready selected limit selection to selected object
                   if isAreaROI and self.ROIDefs.isROISelected () and self.ROIDefs.isOneROISelected ():   
                         if (itemName == self.ROIDefs.getSelectedROI ()[0]) :
                             currentSelectedContourList = self.ROIDictionary.getROISelectedContours (itemName)
                             contourList= RCC_ContourWindow._setSelectedContourToCurrentlySelectedContoursIfSubsetSelected (contourList, currentSelectedContourList, self.getKeyState (QtCore.Qt.Key_Shift)) 
                                    
                   #if Point is selected, and selected axis is not in the XY plane and only one pt is selected then set the selected coordinate to match the point coordinate.                                          
                   if (niftiSliceWidget.getSliceAxis () != self.ui.XYSliceView.getSliceAxis ()) :
                       if (isAreaROI) :
                             areaObj = self.ROIDictionary.getROIObject (itemName)                    
                             zCord = self.SelectedCoordinate.getZCoordinate ()                             

                             for contourID in contourList :
                                 if (areaObj.contourDefinedForSlice (contourID, zCord)):
                                     zMouseOffset = zCord - cz
                                     cz = zCord
                                     break;                                 
                       elif (not isAreaROI and len (contourList) == 1): 
                             pointObj = self.ROIDictionary.getROIObject (itemName)
                             coord = pointObj.getPointCoordinate (contourList[0])                           
                             if (niftiSliceWidget.getSliceAxis () != self.ui.YZSliceView.getSliceAxis ()) :
                                 xMouseOffset = coord.getXCoordinate () - cx
                                 cx = coord.getXCoordinate ()
                             if (niftiSliceWidget.getSliceAxis () != self.ui.XZSliceView.getSliceAxis ()) :
                                 yMouseOffset = coord.getYCoordinate () - cy
                                 cy = coord.getYCoordinate ()
                             zMouseOffset = coord.getZCoordinate () - cz                                                        
                             cz = coord.getZCoordinate ()
                       
                   if not self.visSettings.isObjectSelectionMoveDisabled () and not self.ROIDefs.isROILocked (itemName) and not self.ROIDefs.isROIHidden (itemName) and not self.ROIDictionary.isReadOnly () and not self.ui.XYSliceView.areAxisProjectionControlsEnabledForAxis (): 
                       if self.showFlattenDeepGrowDlg ("One-click generated segmentations must be flattened before ROI can be moved. Do you wish to flatten the segmentation?") :
                           self._SelectionMoveROIState = (cx,cy,cz, itemName, contourList , 0, xMouseOffset, yMouseOffset, zMouseOffset, None)
                       else:
                           self._SelectionMoveROIState = None
                   else:
                       if (not self.ROIDefs.isROISelected ()) :
                           setSelectedROI = True
                       elif not self.ROIDefs.isOneROISelected () :
                           setSelectedROI = True
                       else:
                           setSelectedROI =  (self.ROIDefs.getSelectedROI ()[0] != itemName)
                           
                       if setSelectedROI :
                           self.ROIDefs.setSelectedROI (itemName, clearSelection = True)
                   self.NiftiAxisSelection (cx, cy, cz, niftiSliceWidget.getSliceAxis (), True)  
           return False
       
    
    
    def setGobalCoordinate (self, worldcoordinate) :        
        if self.NiftiVolume == None :
            return                
        if (not self.isSelectionUIState ()) :
            return        
        worldcoordinate = np.array ([worldcoordinate[0][0],worldcoordinate[1][0],worldcoordinate[2][0], 1.0], dtype =np.float)
        destAffine = self.NiftiVolume.getAdjustedAffine ()
        destInverAffin = np.linalg.inv (destAffine)
        voxelCord = np.matmul (destInverAffin, worldcoordinate)    
                
        x = int (voxelCord[0])
        y = int (voxelCord[1])
        z = int (voxelCord[2])
        
        xsize, ysize, zsize = self.NiftiVolume.getSliceDimensions ()
        if (x >= xsize) :
            x = xsize -1
        if (y >= ysize) :
            y = ysize -1
        if (z >= zsize) :
            z = zsize -1
        if (x < 0) :
            x = 0
        if (y < 0) :
            y = 0
        if (z < 0) :
            z = 0                
        self.NiftiAxisSelection (x, y, z, "Z", False, GlobalSet = True)

                     
    #NiftiAxisSelection called when the user clicks on one of the visualization windows.
    #if XZ or YZSlice view are selected sets current selected coordinate.  If XYSlice view is 
    #selected then does cd re complex lodgic.
    # CallerAxis is set to calling axis of the niftislice widget clicked on
    # mouse event is set to true if a mouse click induced the method call, is false if method call is result of another ui object (slider)
    def NiftiAxisSelection (self, cX, cY, cZ, CallerAxis, isMouseEvent, GlobalSet = False, EnableROISelection = True) :          
        if  self.ROIDictionary is  None :
            return False
        cX = max (cX, 0)
        cY = max (cY, 0)
        cZ = max (cZ, 0)

        if (self.NiftiVolume is None) :
             cX = 0
             cY = 0
             cZ = 0
        else:
             sliceDim = self.NiftiVolume.getSliceDimensions ()
             cX = min (cX, sliceDim[0] - 1)
             cY = min (cY, sliceDim[1] - 1)
             cZ = min (cZ, sliceDim[2] - 1)

        if self.isZoomInXYUIState () :                      # if zoom in is selected and mouse click caused event then zoom in                if CallerAxis ==  self.ui.YZSliceView.getSliceAxis () : # if user is interacting with XYSliceview        
            if isMouseEvent :
                if CallerAxis ==  self.ui.XYSliceView.getSliceAxis () : # if user is interacting with XYSliceview
                    self.ui.XYSliceView.zoomInClick (cX, cY, cZ)                             
                elif CallerAxis ==  self.ui.YZSliceView.getSliceAxis () : # if user is interacting with XYSliceview
                    self.ui.YZSliceView.zoomInClick (cX, cY, cZ)                             
                if CallerAxis ==  self.ui.XZSliceView.getSliceAxis () : # if user is interacting with XYSliceview
                    self.ui.XZSliceView.zoomInClick (cX, cY, cZ)                             
                self.update ()    
                return True
            else :
                self.SelectedCoordinate.setCoordinate ((cX, cY, cZ)) #if mouse event did not cause event then adjust the selected coordinate
                #print ("NiftiAxisSelection.update")
                self.update ()    
                return False         
        elif self.isZoomOutXYUIState () :                      # if zoom in is selected and mouse click caused event then zoom in                if CallerAxis ==  self.ui.YZSliceView.getSliceAxis () : # if user is interacting with XYSliceview        
            if isMouseEvent :
                if CallerAxis ==  self.ui.XYSliceView.getSliceAxis () : # if user is interacting with XYSliceview
                    self.ui.XYSliceView.zoomOut ()                             
                elif CallerAxis ==  self.ui.YZSliceView.getSliceAxis () : # if user is interacting with XYSliceview
                    self.ui.YZSliceView.zoomOut ()                             
                if CallerAxis ==  self.ui.XZSliceView.getSliceAxis () : # if user is interacting with XYSliceview
                    self.ui.XZSliceView.zoomOut ()                             
                self.update ()    
                return True
            else :
                self.SelectedCoordinate.setCoordinate ((cX, cY, cZ)) #if mouse event did not cause event then adjust the selected coordinate
                #print ("NiftiAxisSelection.update")
                self.update ()    
                return False         
        
        elif (self.isContourXYUIState ()) :                    # if contouring and function call is the result of a mouse click then if a point ROI object is currently selected add/modifiy the seleceted object
                #Add Selected Object        
                self.SelectedCoordinate.setCoordinate ((cX, cY, cZ)) #select the currently selected coordinate               
                if self.ROIDefs.isROISelected () and self.isContourXYUIState () and isMouseEvent and self.ROIDefs.isOneROISelected ():    
                    itemName =self.ROIDefs.getSelectedROI ()[0]                       
                    if (not self.ROIDefs.isROIHidden (itemName) and not self.ROIDefs.isROILocked (itemName)) :
                        if (self.ROIDefs.isROIPoint (itemName) ) :  
                            selectedContourCount = self.ROIDictionary.getROISelectedContourCount (itemName)
                            if (selectedContourCount <= 1) :
                                pointID = None
                                ControlKeyPressed = self.getKeyState (QtCore.Qt.Key_Control)
                                                                
                                #if Adding Point
                                if (selectedContourCount <= 0 or ControlKeyPressed) :                                
                                    
                                    #if range selected extend points along selected slices 
                                    #selection = self.ui.XYSliceSelectorWidget.getSelection ()                                    
                                    
                                    if (not self.ROIDictionary.isROIDefined (itemName)):
                                        obj = ROIPointObject ()
                                    else:                                        
                                        obj = self.ROIDictionary.getROIObject (itemName)
                                        obj = obj.copy ()
                                                                            
                                    if CallerAxis == "Z" :                                            
                                        PointInCallingAxis = obj.isSliceContoured(self.SelectedCoordinate.getZCoordinate ())
                                    elif CallerAxis == "X" :                                            
                                        PointInCallingAxis = obj.hasAnnotationOnXAxis(self.SelectedCoordinate.getXCoordinate ())
                                    elif CallerAxis == "Y" :                                            
                                        PointInCallingAxis = obj.hasAnnotationOnYAxis(self.SelectedCoordinate.getYCoordinate ())
                                    else:
                                        PointInCallingAxis = False
                                        
                                    if (obj.getPointCount () == 0 or ControlKeyPressed or not PointInCallingAxis) :  #  or len (selection) > 1
                                        pointID = obj.addPoint (itemName + " " + str (obj.getPointCount () + 1), self.SelectedCoordinate)                                                             
                                        obj.setSelectedContours ([pointID])                                                            
                                        self.ROIDictionary.addROI (itemName, obj)                                       
                                        
                                else :                                                            
                                    pointID = self.ROIDictionary.getROISelectedContours (itemName)[0]                                   
                                    obj = self.ROIDictionary.getROIObject (itemName)
                                    p1 = obj.getPointCoordinate (pointID)
                                    if (p1.getCoordinate () != self.SelectedCoordinate.getCoordinate ()) :
                                       newobj = obj.copy () # Required for backup Code
                                       newobj.setPoint (pointID, self.SelectedCoordinate)                                
                                       newobj.setSelectedContours ([pointID])                                                            
                                       self.ROIDictionary.addROI (itemName, newobj)   
                return False                                                                  
                
        if self.isSelectionUIState () : # if arrow selection tool is selected, test if object is selected             
             if (cX, cY, cZ) != tuple (self.SelectedCoordinate.getCoordinate ()) :
                 # if multiple windows open broadcast coordinates       
                 if (not GlobalSet and len (self._ProjectDataset.getActiveContourWindowList ()) >= 2) :
                     firstAffine = self.NiftiVolume.getAdjustedAffine ()
                     pt = np.array ([[cX, cY, cZ, 1.0]], dtype = float)
                     worldCord =  np.matmul (firstAffine, pt.T)   
                     self._ProjectDataset.setSelectionCoordinate (self, worldCord)
                 
                 if (not EnableROISelection) :
                     self.SelectedCoordinate.setCoordinate ((cX, cY, cZ)) #select the currently selected coordinate    
                     self.ROIDictionary._CallDictionaryChangedListner (SkipFileSave = True)        
                     return False
                 else:
                     niftiSliceWidgetSelection = None             
                     if CallerAxis ==  self.ui.XYSliceView.getSliceAxis () : # if user is interacting with XYSliceview
                        niftiSliceWidgetSelection = self.ui.XYSliceView
                     elif CallerAxis ==  self.ui.YZSliceView.getSliceAxis () : # if user is interacting with YZSliceview
                        niftiSliceWidgetSelection = self.ui.YZSliceView
                     elif CallerAxis ==  self.ui.XZSliceView.getSliceAxis () : # if user is interacting with XZSliceview
                        niftiSliceWidgetSelection = self.ui.XZSliceView
                    
                     roiList = self.getSelectedObjectsAndContours (cX, cY, cZ, niftiSliceWidgetSelection)       
                     if (self.ROIDefs.isROISelected () and self.ROIDefs.isOneROISelected ()): # if arrow selection tool is selected, test if object is selected                                         
                        for MouseSelectedROI in roiList :
                            selectedROI, contourLst = MouseSelectedROI
                            if (selectedROI == self.ROIDefs.getSelectedROI ()[0]) :
                                currentSelectedContourList = self.ROIDictionary.getROISelectedContours (selectedROI)                        
                                #contours are selected
                                contourLst = RCC_ContourWindow._setSelectedContourToCurrentlySelectedContoursIfSubsetSelected (contourLst, currentSelectedContourList, self.getKeyState (QtCore.Qt.Key_Shift))
                                    
                                self.ROIDictionary.setROISelectedContours (selectedROI, contourLst)                                                                
                                self.SelectedCoordinate.setCoordinate ((cX, cY, cZ)) #select the currently selected coordinate    
                                self.ROIDictionary._CallDictionaryChangedListner ()                            
                                return False
                     if len (roiList) == 0 and not self.getKeyState (QtCore.Qt.Key_Shift)  :
                         if (self.ROIDefs.isROISelected () and self.ROIDefs.isOneROISelected ()): # if arrow selection tool is selected, test if object is selected                                         
                             selectedROIName = self.ROIDefs.getSelectedROI ()[0]
                             self.ROIDictionary.setROISelectedContours (selectedROIName, [])                                                                
                     else :
                         clearSelection = True
                         for roi in roiList :
                             roiName, contourList = roi   
                             obj = self.ROIDictionary.getROIObject (roiName)
                             if not self.ROIDefs.isROILocked (roiName) and not self.ROIDefs.isROIHidden (roiName) :                            
                                 if (obj.isROIArea ()):
                                    obj.setSliceNumber (cZ)
                                 elif (len (contourList) > 0):                                         
                                    obj.setLastModifiedPointID (contourList[0])
                             self.ROIDefs.setSelectedROI (roiName, clearSelection = clearSelection)                                      
                             clearSelection = False      
                         
                         if (len (roiList) == 1) :
                             self.ROIDictionary.setROISelectedContours (roiName, contourList)                                                                              
                         self.SelectedCoordinate.setCoordinate ((cX, cY, cZ)) #select the currently selected coordinate    
                         self.ROIDictionary._CallDictionaryChangedListner (SkipFileSave = True)        
                         return False
                        
                     self.ROIDefs.clearSelectedROI ()  
                     self.SelectedCoordinate.setCoordinate ((cX, cY, cZ)) #select the currently selected coordinate    
                     self.ROIDictionary._CallDictionaryChangedListner (SkipFileSave = True)        
                     return False
             return False       
        self.SelectedCoordinate.setCoordinate ((cX, cY, cZ)) #select the currently selected coordinate                    
        return False
                
    @staticmethod         
    def _AxisSelectionTextBoxChanged (SelectedCoordinate, MajorAxisSliceTextBox, XYSliceView):
        try:            
            slicenumber = int (MajorAxisSliceTextBox.text())
            if slicenumber < 0 :
               slicenumber = 0
               MajorAxisSliceTextBox.setText (str (slicenumber))
            elif slicenumber >= XYSliceView.getMaxSliceCount () :
                slicenumber = XYSliceView.getMaxSliceCount () - 1
                if (slicenumber < 0) :
                    slicenumber = 0
                MajorAxisSliceTextBox.setText (str (slicenumber))
            if SelectedCoordinate.setCoordinate (XYSliceView.getSliceCoordinate (slicenumber)) :
                XYSliceView.setSelected (True)
            return
            
        except ValueError:
            MajorAxisSliceTextBox.setText ( str(XYSliceView.getSliceNumber ()))
            return
        
    # Slice selection textbox is changed. Adjust slice slection.
    def MajorAxisTextBoxChanged (self) :
       RCC_ContourWindow._AxisSelectionTextBoxChanged (self.SelectedCoordinate, self.ui.MajorAxisSliceTextBox, self.ui.XYSliceView)
    
    def YZSliceAxisTextBoxChanged (self) :
        RCC_ContourWindow._AxisSelectionTextBoxChanged (self.SelectedCoordinate, self.ui.YZSliceAxisSliceTextBox, self.ui.YZSliceView)
        
    def XZSliceAxisTextBoxChanged (self) :
        RCC_ContourWindow._AxisSelectionTextBoxChanged (self.SelectedCoordinate, self.ui.XZSliceAxisSliceTextBox, self.ui.XZSliceView)
        
    
    # Updates contour UI list
    def _updateContourSelectionList (self) :
        # Build a list of the old selected contours to re-select the same selelection if the same ROI is selected                
        if self.ROIDictionary is None : 
            return       
        ClearContourList = True
        if (self.ROIDefs.isROISelected () and self.ROIDefs.isOneROISelected ()) :
            selectedROIName = self.ROIDefs.getSelectedROI ()[0] # get currently selected ROI
            idmanager = self.ROIDictionary.getROIContourManger (selectedROIName) # gets the ROI contour (Area) or point (Point) object manager
            if (idmanager != None) :                                                      
                oldSelIDList = self.ROIDictionary.getROISelectedContours (selectedROIName)
                oldROIName = selectedROIName
                ClearContourList = False
                roiobj = self.ROIDictionary.getROIObject (selectedROIName) # gets ROI object
                lst = idmanager.getNameLst ()                              # gets list of ROI contour/point names
                self._DisableContourListSelectionChanged = True
                                       
                AddContourIndexLst = []
                for contourIndex in range(len (lst)) :                                 
                   contourIDNumber = lst[contourIndex][0]
                   contourDefinedInSlice = roiobj.contourDefinedForSlice (contourIDNumber,  self.SelectedCoordinate.getZCoordinate ())
                   if (roiobj.isROIArea () or contourDefinedInSlice) :
                       AddContourIndexLst.append ((contourIndex, contourDefinedInSlice))
                       
                if (self.ui.ContourList.count () != len (AddContourIndexLst)):
                    self.ui.ContourList.clear ()                     # clears current UI List       
                                
                for contourUIListIndex, tupleToAdd in enumerate(AddContourIndexLst)  :                                 
                   contourIndex, contourDefinedInSlice = tupleToAdd
                   contourIDNumber = lst[contourIndex][0]                   
                   if (roiobj.isROIArea () or contourDefinedInSlice) :
                       if contourUIListIndex >= self.ui.ContourList.count () :
                           self.ui.ContourList.addItem (lst[contourIndex][1])                                                                                       
                       listItem = self.ui.ContourList.item (contourUIListIndex)
                       font = listItem.font ()                             
                       
                       if (roiobj.isROIArea ()) :
                           font.setBold (contourDefinedInSlice)
                       else:
                           font.setBold (roiobj.isPointDefined (contourIDNumber))
                           
                       listItem.setFont (font) 
                       
                       if (contourDefinedInSlice or roiobj.isContourDefined (contourIDNumber)[0]) :                   
                           listItem.setForeground (self.ROIDefs.getSelectedROIColor())
                       else:               
                           listItem.setForeground (QtGui.QColor (0,0,0))
                           
                       #listItem.setData (QtCore.Qt.UserRole, (roiobj.getROIDefIDNumber (),lst[i][0]))                      
                       listItem.setData (QtCore.Qt.UserRole, lst[contourIndex][0])                      
                       listItem.setSelected (oldROIName != None and oldROIName == selectedROIName and lst[contourIndex][0] in oldSelIDList)                            
                self._DisableContourListSelectionChanged = False
                self.ContourListSelectionChanged()   
        if (ClearContourList and self.ui.ContourList.count () > 0) :
            self._DisableContourListSelectionChanged = True
            self.ui.ContourList.clear ()                     # clears current UI List               
            self._DisableContourListSelectionChanged = False
            self.ContourListSelectionChanged()   
    
  
              
    # addContourBtn: adds contour / point objects 
    def addContourBtn (self) :
        if self.ROIDictionary is None : 
            return
        objectName = self.ROIDefs.getSelectedROI ()[0]   
        if self.ROIDefs.isROIArea (objectName) and self.ROIDictionary.isROIinAreaMode () :
            return
        if self.ROIDefs.isROILocked (objectName)  :
            return
        
        self.ROIDictionary._SaveUndoPoint ()   
        if self.ROIDefs.isROIArea (objectName) :                # if area ROI object is selected add a contour object
           if not self.ROIDictionary.isROIDefined (objectName) :
              self.ROIDictionary.addROI (objectName, MultiROIAreaObject ())   
           obj = self.ROIDictionary.getROIObject (objectName)
           contourID = obj.addROIAreaContour ("Untitled")    
           # Initalize RCC_ContourDlg object
           dlg = RCC_ContourDlg (self, self.ROIDictionary, ContourID = contourID, objectType="Area")        
           
        else: # if point ROI object is selected add a point object 
           if not self.ROIDictionary.isROIDefined (objectName) :
              self.ROIDictionary.addROI (objectName, ROIPointObject ())   
           obj = self.ROIDictionary.getROIObject (objectName)
           c = Coordinate ()           
           # Point objects initalized to (-1, -1, -1) are undefined
           c.setCoordinate ((-1, -1, -1))
           contourID = obj.addPoint (objectName + " " + str (obj.getPointCount () + 1), c)               
           # Initalize RCC_ContourDlg object
           dlg = RCC_ContourDlg (self, self.ROIDictionary, ContourID = contourID, objectType="Point")        
        
        # if control keyboard button  is not pressed  then show the contour/point editing dlg
        if (not self.getKeyState (QtCore.Qt.Key_Control)) :
            dlg.exec_ ()              
        dlg = 0    # memory associated with  contour/point editing dlg
        self.ROIDictionary.setROISelectedContours (objectName, [contourID])
        self.ROIDictionary._CallDictionaryChangedListner ()        
         
    # ContourListDblClk: called when contour list is double clicked on. Opens contour editing dlg for selected (clicked on) contour/point
    def ContourListDblClk (self) : 
        if self.ROIDictionary is None : 
            return
        if (self.ROIDefs.isROISelected () and self.ROIDefs.isOneROISelected ()) :
            objectName = self.ROIDefs.getSelectedROI ()[0]
            if self.ROIDefs.isROILocked (objectName) :
                return
            roiContours = self.ROIDictionary.getROISelectedContours (objectName)  # get currently selected contours
            if (len (roiContours) > 0 ) :                    # contours are selected            
                if self.ROIDefs.isROIArea (objectName) :
                    objectType =  "Area"
                else:
                    objectType = "Point"
                    
                # initalize contour/point editing dlg.            
                dlg = RCC_ContourDlg (self, self.ROIDictionary, objectType = objectType)        
                dlg.exec_ () 
                if (dlg.okClicked ()):   # if ok selected then update        
                    print ("update contour")
                    self._updateContourSelectionList ()
                    #objectName = self.ROIDefs.getSelectedROI ()[0]                    
                    obj = self.ROIDictionary.getROIObject (objectName)
                    obj.updateContourSlices (roiContours[0])
                    self.ROIDictionary._CallDictionaryChangedListner ()
                    #print ("ContourListDblClk.update")
                    self.update ()
            else:
                MessageBoxUtil.showMessage ("Contour Selection", "Error: No contour selected.")
    
    # Contour list selection changed, sets ROI currently selected contours and updates associated UI enabled/disabled states
    def ContourListSelectionChanged (self, SetContourModeEnabled = True) :
        if (not self._DisableContourListSelectionChanged and self.ROIDictionary is not None) :
            selectedROIList = self.ROIDefs.getSelectedROI ()
            if len (selectedROIList) > 1 :
                selectedContours = []
                for objectName in selectedROIList :
                    self.ROIDictionary.setROISelectedContours (objectName, selectedContours)    
                self.ui.deleteContourBtn.setEnabled (False) 
                self.ui.contourSingleSliceBtn.setEnabled (False)                            
                self.ui.autoContourBtn.setEnabled (False)
                self.ui.SelectedContourLbl.setText ("")        
            else:            
                items = self.ui.ContourList.selectedItems ()                                
                #sets currently selected ROI object in self.ROIDictionarys, current point or contour selection
                selectedContours = []
                for sel in items :               
                    contourID = sel.data (QtCore.Qt.UserRole)
                    selectedContours.append (contourID)            
                            
                objectName = selectedROIList[0]                    
                self.ROIDictionary.setROISelectedContours (objectName, selectedContours)            
                selectioncount = len (items)
            
                # Update UI state
                self.ui.deleteContourBtn.setEnabled (False) 
                
                if self.ROIDictionary.isROIDefined (objectName) :
                    obj = self.ROIDictionary.getROIObject (objectName)
                    if self.ROIDefs.isROILocked (objectName)  :
                        self.ui.deleteContourBtn.setEnabled (False) 
                        self.ui.contourSingleSliceBtn.setEnabled (False)                            
                        self.ui.autoContourBtn.setEnabled (False)
                    else:
                        sliceNumber = self.SelectedCoordinate.getZCoordinate ()
                        self.ui.deleteContourBtn.setEnabled (obj.areSelectedContoursDeleteableInSlice (sliceNumber) and not self.ROIDictionary.isReadOnly ()) 
                        if self.ROIDefs.isROIArea (objectName)  :                                    
                            self.ui.contourSingleSliceBtn.setEnabled (not self.ROIDictionary.isReadOnly ())
                            self.ui.autoContourBtn.setEnabled (len (obj.getSelectedContours()) > 0 and not self.ROIDictionary.isReadOnly ())   
                        else:                
                            self.ui.contourSingleSliceBtn.setEnabled (False)                            
                            self.ui.autoContourBtn.setEnabled (not self.ROIDictionary.isReadOnly ())            
                                        
                if (self.ROIDefs.isROIArea (objectName)) :
                    title = "Contour: "
                elif (objectName != "None") :
                    title = "Point: "
                else:
                    title = ""
                    
                if (selectioncount == 0) :
                    self.ui.SelectedContourLbl.setText (title)        
                elif (selectioncount == 1) :                                
                    self.ui.SelectedContourLbl.setText (title + items[0].text ())
                else :
                    self.ui.SelectedContourLbl.setText (title + "Multiple selected")
                if (not self.isContourXYUIState () and not self.isSelectionUIState () and SetContourModeEnabled) :
                    #self.ui.ContourBtn.setChecked (True)
                    self.setContourMode ()            
            self.update ()                 
                                 
    # Update the UI to refect changes in ROI list selection        
    def ROIListSelectionChanged (self) :  
        items = self.ui.ROI_List.selectedItems ()        
        self.ROIDefs._eraseROISelectionDict ()
        if (len (items) == 0) :
            #self.ui.RemoveROIBtn.setEnabled(False)    # set to true in dictionary change event        
            self.ROIDefs.setSelectedROI ("None")
        else:
            for i in items :
                 name = i.text ()                            
                 color = self.ROIDefs.getROIColor (name)
                 self.ROIDefs.setSelectedROI (name, color, CallChangeListener = False)
                 if name == "None" :
                     break  
            
            if self.ROIDefs.isROISelected () and self.ROIDefs.isOneROISelected () and self.ROIDictionary.isROIinAreaMode () :
               name = self.ROIDefs.getSelectedROI ()[0]
               idNum = self.ROIDefs.getROINameIDNumber (name)
               self.getPenSettings ().setActiveROIID (idNum) 
            if self._PenSettingsDlg is not None :
                 self._PenSettingsDlg.updateToReflectActiveROI()
                                   
            
            if (self.ROIDictionary is not None) :
                self.ROIDictionary._CallDictionaryChangedListner (SkipFileSave = True)       
            if (not self.isContourXYUIState () and not self.isSelectionUIState () ) :
                #self.ui.ContourBtn.setChecked (True)
                self.setContourMode ()                            
            self.update ()
    
    
    def getRangeSelectionBoundingBox (self, DimX, DimY, callBack, SliceView =  None):
        if SliceView is None :
            SliceView = self.ui.XYSliceView
        SliceView.setRangeSelectionDim (DimX, DimY)
        SliceView.setRangeSelectionEnabled (True)
        self._RangeSelectionCallBack = callBack  
        
    def disableRangeSelectionCallBack (self) :
        if self._RangeSelectionCallBack is not None : 
            for sliceView in [self.ui.XYSliceView, self.ui.XZSliceView ,self.ui.YZSliceView] :
                sliceView.setRangeSelectionEnabled (False)
            self._RangeSelectionCallBack (None, None)
            self._RangeSelectionCallBack = None
        
    def setRangeSelectionCallBack (self, XPos, YPos) :
        if self._RangeSelectionCallBack is not None :    
            found = False
            for sliceView in [self.ui.XYSliceView, self.ui.XZSliceView ,self.ui.YZSliceView] :
                if sliceView.isRangeSelectionEnabled () :
                    sliceView.setRangeSelectionEnabled (False)
                    found = True
                    break
            #self._RangeSelectionCallBack (None, None)
            if found :    
                sliceView.setRangeSelectionEnabled (False)
                DimX, DimY = sliceView.getRangeSelectionDim ()            
                sliceShape = sliceView.getSliceShape ()            
                sliceWidth, sliceHeight = sliceShape                        
                halfXPatch = int (DimX/2)
                halfYPatch = int (DimY/2)                        
                XPos = max (halfXPatch, XPos)
                YPos = max (halfYPatch, YPos)
                XPos = max(min ( sliceWidth  - int (DimX - halfXPatch), XPos), 0)
                YPos = max(min ( sliceHeight - int (DimY - halfYPatch), YPos), 0)                               
                if DimX >=  sliceWidth :
                    XPos = int (sliceWidth / 2)
                if DimY >=  sliceHeight :
                    YPos = int (sliceHeight / 2)
                self._RangeSelectionCallBack (XPos, YPos)
                self._RangeSelectionCallBack = None
    
    def isRangeSelectionCallBackSet (self) :
        return self._RangeSelectionCallBack is not None
    
    
    # Remove ROI Button call back
    def RemoveROIBtn (self) :   
        if self.ROIDictionary is None : 
            return
        if self.ROIDefs.isROISelected () :        
            sliceView = None
            if not self.ROIDictionary.isROIinAreaMode () :
                sliceView = self.ui.XYSliceVerticalSelector
            elif self.ui.XYSliceView.isSelected () :
                sliceView = self.ui.XYSliceVerticalSelector
            elif self.ui.YZSliceView.isSelected () :
                sliceView = self.ui.YZSliceVerticalSelector
            elif self.ui.XZSliceView.isSelected () :
                sliceView = self.ui.XZSliceVerticalSelector            
            if len (sliceView.getSelection ()) > 1 :
                self.deleteSliceCallback (sliceView, AllSelectedROI = True) 
            else:                                
                first = True
                for name in self.ROIDefs.getSelectedROI () : # get currently selected ROI                        
                    if name != "None" :
                        if not self.ROIDefs.isROILocked (name) :                            
                            if self.ROIDictionary.removeROIEntry (name, sliceindex = None, first = first, RemoveAllContours = True, callChangeListener = False, RemoveProjectedSlices = True, SliceView = sliceView) :
                                first = False
                if not first :
                   self.ROIDictionary._CallDictionaryChangedListner ()
                   self.updatePenSettingsDlgsOneClickROI ()
            self.update ()
    
    def updatePenSettingsDlgsOneClickROI (self) :
         if self._PenSettingsDlg is not None :
            self._PenSettingsDlg.UpdatePaintSettingsOneClickROI ()
         
            
    def _updateSliceViewCoord (self, cX = None, cY = None , cZ = None, dX = -1, dY= -1, dZ = -1) :
        XAxisChanged = False
        YAxisChanged = False
        ZAxisChanged = False
        YZViewChanged1 = False
        XZViewChanged1 = False
        XYViewChanged1 = False
        YZViewChanged2 = False
        XZViewChanged2 = False
        XYViewChanged2 = False
        if dX == 0 :
            cX = None
        if dY == 0 :
            cY = None
        if dZ == 0 :
            cZ = None
        if cX is not None :    
            XYViewChanged1 = self.ui.XYSliceView.updateSliceSelection (cX, "X", DisableScrollToZoomAndUpdateSlice = True)
            XAxisChanged = self.ui.YZSliceView.updateSliceSelection (cX, "X", DisableScrollToZoomAndUpdateSlice = True)
            XZViewChanged1 = self.ui.XZSliceView.updateSliceSelection (cX, "X", DisableScrollToZoomAndUpdateSlice = True)
        if cY is not None :    
            XYViewChanged2 = self.ui.XYSliceView.updateSliceSelection (cY, "Y", DisableScrollToZoomAndUpdateSlice = True)
            YZViewChanged1 = self.ui.YZSliceView.updateSliceSelection (cY, "Y", DisableScrollToZoomAndUpdateSlice = True)
            YAxisChanged = self.ui.XZSliceView.updateSliceSelection (cY, "Y", DisableScrollToZoomAndUpdateSlice = True)
        if cZ is not None :    
            ZAxisChanged = self.ui.XYSliceView.updateSliceSelection (cZ, "Z", DisableScrollToZoomAndUpdateSlice = True)
            YZViewChanged2 = self.ui.YZSliceView.updateSliceSelection (cZ, "Z", DisableScrollToZoomAndUpdateSlice = True)
            XZViewChanged2 = self.ui.XZSliceView.updateSliceSelection (cZ, "Z", DisableScrollToZoomAndUpdateSlice = True)
            
        if (XAxisChanged or YZViewChanged1 or YZViewChanged2) :
            self.ui.YZSliceView.CoordianteChangedUpdateSliceAxis (XAxisChanged)
        if (YAxisChanged or XZViewChanged1 or XZViewChanged2) :
            self.ui.XZSliceView.CoordianteChangedUpdateSliceAxis (YAxisChanged)
        if (ZAxisChanged or XYViewChanged1 or XYViewChanged2) :
            if ZAxisChanged :
                self.ui.XYSliceView.CoordianteChangedUpdateSliceAxis (ZAxisChanged)
                return 
        if XAxisChanged or YAxisChanged :
            controls = self.ui.XYSliceView.getAxisProjectionControls()
            if controls is not None :
                if (XAxisChanged and controls.isYAxisRotated ()) or (YAxisChanged and controls.isXAxisRotated ()) :
                    self.ui.XYSliceView.CoordianteChangedUpdateSliceAxis (True)    
                    selCoord = self.ui.XYSliceView.getCoordinate ()
                    self.ui.XYSliceSelectorWidget.setROISliceDictionary (self.ROIDictionary, NIfTIVolume = self.NiftiVolume, AxisProjection = self.ui.XYSliceView.getAxisProjectionControls (), Coordinate =  selCoord)
                    self.ui.XYSliceVerticalSelector.setROISliceDictionary (self.ROIDictionary, self.NiftiVolume, self._ProjectDataset, AxisProjection = self.ui.XYSliceView.getAxisProjectionControls (), Coordinate = selCoord)
                    self.ui.XZSliceVerticalSelector.setROISliceDictionary (self.ROIDictionary, self.NiftiVolume, self._ProjectDataset)
                    self.ui.YZSliceVerticalSelector.setROISliceDictionary (self.ROIDictionary, self.NiftiVolume, self._ProjectDataset)
                    return True
        return False
            
    def endAxisRotation (self, sliceview) :
        try :
            controls = sliceview.getAxisProjectionControls () 
            if controls is not None :
                controls.clearMarkerSelection (sliceview)
            ptTools = self._getPaintToolsDlg()
            if ptTools.getDeepGrowButtonEnabled () and self.ui.XYSliceView.areAxisProjectionControlsEnabledForAxis() :
               ptTools.setButtonEnabledState (DeepGrow = False)
            elif not ptTools.getDeepGrowButtonEnabled () and not self.ui.XYSliceView.areAxisProjectionControlsEnabledForAxis() :
               ptTools.setButtonEnabledState (DeepGrow = True)
        except:
            pass
        
    def handleAxisRotationMarkerSelection (self, cx, cy, cz,  sliceview, eventtype) :
        controls = sliceview.getAxisProjectionControls () 
        if controls is not None :
            try :
                if self.showFlattenDeepGrowDlg ("One-click generated segmentations must be flattened before the axis can be rotated. Do you wish to flatten the segmentation?") :
                    if controls.isMarkerSelected (sliceview) :
                        controls.moveMarker (cx, cy, cz, sliceview)
                        #self.ui.XYSliceView.disableUpdateSlice()
                        #self.ui.XYSliceView.UpdateSlice ()
                        #return True
                        return True
            finally:
                if eventtype == "EndMouseMove" :
                    controls.clearMarkerSelection (sliceview)
                    #self.ui.XYSliceView.enableUpdateSlice(True)
                    self.ui.XYSliceView.UpdateSlice ()
            ptTools = self._getPaintToolsDlg()
            if ptTools.getDeepGrowButtonEnabled () and self.ui.XYSliceView.areAxisProjectionControlsEnabledForAxis() :
               ptTools.setButtonEnabledState (DeepGrow = False)
            elif not ptTools.getDeepGrowButtonEnabled () and not self.ui.XYSliceView.areAxisProjectionControlsEnabledForAxis() :
               ptTools.setButtonEnabledState (DeepGrow = True)
        return False
            
        
    
    # mouse move event from YZ or XZ Nifit slice view widgets                         
    def SecondaryAxisMouseMoveEvent (self, line, eventtype, sliceview, MouseButtonPressed, ClickEventHandled):         
       if self.ROIDictionary is None : 
            return       
       if (len (line) > 0) :
            imageX = line[-1][0]
            imageY = line[-1][1]           
            cx,cy,cz = sliceview.convertMouseMoveImageCoordiantesToXYZImageCoordinates (imageX, imageY)
            imgdata = self.NiftiVolume.getImageData ()
            if (cx >= 0 and cx < imgdata.shape[0] and cy >= 0 and cy < imgdata.shape[1] and cz >= 0 and cz < imgdata.shape[2]) :
                self.ui.ThresholdLbl.setText ("Value: %d" % (imgdata[cx, cy, cz]))                
            else:
                self.ui.ThresholdLbl.setText ("")                
       if (eventtype == "MouseMoveButtonUp") :
           return
       if self.isRangeSelectionCallBackSet () :   
           return
       
       if (self.isZoomInXYUIState ()): # If Zoom in tool is selected  
           if self._altScrollZoomWindow (imageX, imageY, sliceview, eventtype) :
               return                          
           if MouseButtonPressed ==  QtCore.Qt.MidButton :
               if len (line) > 1 :                   
                   dy = int (line[-1][1] - line[-2][1])
                   dx = int (line[-1][0] - line[-2][0])
                   dx = int (math.sqrt(dy**2 + dx**2))
                   dx = min (dx,5)                   
                   if dx > 0 :
                       sliceview.mouseWheelZoom (dx)                                  
           else:
              self._XYZoomInit = RCC_ContourWindow._ZoomWindow (line, sliceview, self._XYZoomInit, eventtype, ClickEventHandled)                                           
           return  # Exits     
                    
       if (self.isZoomOutXYUIState ()): # If Zoom in tool is selected          
           if self._altScrollZoomWindow (imageX, imageY, sliceview, eventtype) :
               return                            
           if MouseButtonPressed ==  QtCore.Qt.MidButton :
               if len (line) > 1 :                   
                   dy = int (line[-1][1] - line[-2][1])
                   dx = int (line[-1][0] - line[-2][0])
                   dx = int (math.sqrt(dy**2 + dx**2))
                   dx = min (dx,5)
                   if dx > 0 :
                       sliceview.mouseWheelZoom (-dx)                                  
           return
        
        
       # if ROI control point selected update control point position
       if (self.isContourXYUIState ()) :      
           if self._penSettings.getPenTool () == "DeepGrow" :
              MessageBoxUtil.showMessage ("One-click Segmentation", "The one click segmentation tool is supported only in the primary axis view.")  
              return
           try:
               self.setWindowUpdateEnabled (False)
               if self._altScrollZoomWindow (imageX, imageY, sliceview, eventtype) :
                   return                   
               obj = self.ROIDictionary.getSelectedObject ()        
               if obj is not None :
                   itemName = obj.getName (self.ROIDefs)
                   if self.ROIDefs.isROIArea (itemName) :
                       if (not self.ROIDefs.isROILocked (itemName) and not self.ROIDefs.isROIHidden (itemName)) :
                           if (obj.getSelectedBoundingBoxControlPoint () is not None):                                                           
                               obj.setControlPointPosition (imageX, imageY, SavePosition = eventtype == "EndMouseMove")                                 
                               if (eventtype == "EndMouseMove") :       
                                   obj.setIsMoving (False)
                                   obj.setSelectedBoundingBoxControlPoint (None)
                                   self.ui.XYSliceView.setROIObjectList (self.ROIDictionary) 
                                   self.ui.XZSliceView.setROIObjectList (self.ROIDictionary)
                                   self.ui.YZSliceView.setROIObjectList (self.ROIDictionary) 
                               sliceview.clearTemporaryDrawLine ()               
                               self.update () 
                               return
               
               if self.ROIDictionary.isROIinAreaMode () and self.ROIDefs.isROISelected () and self.ROIDefs.isOneROISelected () :    # if ROI is selected otherwise info message
                    itemName =self.ROIDefs.getSelectedROI ()[0]  # get currently selected ROI
                    if (self.ROIDefs.isROIArea (itemName) and not self.ROIDefs.isROIHidden (itemName) and not self.ROIDefs.isROILocked (itemName)) :                        
                        if self.showFlattenDeepGrowDlg ("One-click generated segmentations must be flattened before an Area ROI can be added. Do you wish to flatten the segmentation?") :
                            self._areaModeContour (cx, cy, cz, sliceview, line, eventtype, MouseButtonPressed)
                            sliceview.clearTemporaryDrawLine ()               
                            self.update () 
                            return                          
                
               cx,cy,cz = sliceview.convertMouseMoveImageCoordiantesToXYZImageCoordinates (imageX, imageY)
               self.NiftiAxisSelection (cx, cy, cz, sliceview.getSliceAxis (), True, EnableROISelection = False)                                        
               sliceview.clearTemporaryDrawLine ()               
               sliceview.setSelected (True)
               self.update () 
               return    
           finally:               
               self.setWindowUpdateEnabled (True)
       elif (self.isSelectionUIState ()) :  
           if self._altScrollZoomWindow (imageX, imageY, sliceview, eventtype) :
               return    
           cx,cy,cz = sliceview.convertMouseMoveImageCoordiantesToXYZImageCoordinates (imageX, imageY)
           
           if (self._SelectionMoveROIState is None and eventtype != "EndMouseMove") :
               self._SelectionMoveROIState = None    
               sliceview.clearTemporaryDrawLine ()     
               sliceview.setSelected (True)
               if self.handleAxisRotationMarkerSelection (cx,cy, cz, sliceview, eventtype) :
                   sliceview.update () 
               else:
                   self._ROIDictionaryChangeEvent = True
                   self.NiftiAxisSelection (cx, cy, cz, sliceview.getSliceAxis (), True, EnableROISelection = False)    
                   self._ROIDictionaryChangeEvent = False                  
                   self.update () 
               return 
            
           elif (self._SelectionMoveROIState is not None ) :
               cx1, cy1, cz1, selectedObjName, selectedContourList, secondPass, xMouseOffset, yMouseOffset, zMouseOffset, moveMemory  = self._SelectionMoveROIState
               cx += xMouseOffset
               cy += yMouseOffset
               cz += zMouseOffset
               dx = cx - cx1
               dy = cy - cy1
               dz = cz - cz1           
               if (secondPass == 0) :
                   self.ROIDictionary._SaveUndoPoint ()                 
               obj = self.ROIDictionary.getROIObject (selectedObjName)
               if obj is None :
                   self._SelectionMoveROIState = None               
               elif (self.ROIDefs.isROIHidden (selectedObjName) or self.ROIDefs.isROILocked (selectedObjName)) :
                     self._SelectionMoveROIState = None         
               else:                                         
                   if (self.ROIDefs.isROIArea (selectedObjName)) :
                         #contourIDLst = self.ROIDictionary.getROISelectedContours (selectedObjName)                                                                         
                         obj.setIsMoving (eventtype != "EndMouseMove")
                         dx, dy,dz, moveMemory = obj.moveContours (selectedContourList, dx, dy, dz, sliceview.getNIfTIVolume().getSliceDimensions (), moveMemory, SliceView = self.ui.XYSliceView, ROIDictionary = self.ROIDictionary, BaseCoordinate = (cx1, cy1, cz1))
                         if  self.ui.XYSliceView.areAxisProjectionControlsEnabledForAxis () :
                              self.ui.XYSliceView.getAxisProjectionControls().invalidateObjectSliceCache (obj.getROIDefIDNumber ())
                   elif (self.ROIDefs.isROIPoint (selectedObjName))  :
                         pointIDLst = selectedContourList #self.ROIDictionary.getROISelectedContours (selectedObjName)                               
                         dx, dy,dz = obj.movePoint (pointIDLst, dx, dy, dz, sliceview.getNIfTIVolume().getSliceDimensions ())                   
                   cx = cx1 + dx
                   cy = cy1 + dy
                   cz = cz1 + dz
                   self._SelectionMoveROIState = (cx,cy,cz, selectedObjName, selectedContourList, 1, xMouseOffset, yMouseOffset, zMouseOffset, moveMemory)
                                  
               if (eventtype != "EndMouseMove") :                                       
                   updatedSliceAxisViews = self._updateSliceViewCoord (cX = cx, cY = cy, cZ = cz, dX = dx, dY = dy, dZ = dz) 
                   if not updatedSliceAxisViews and (dx != 0 or dy != 0 or dz !=0) :
                       #self.ui.XYSliceView.setROIObjectList (self.ROIDictionary)
                       #self.ui.XZSliceView.setROIObjectList (self.ROIDictionary)
                       #self.ui.YZSliceView.setROIObjectList (self.ROIDictionary)
                       self.ui.XYSliceSelectorWidget.setROISliceDictionary (self.ROIDictionary, NIfTIVolume = self.NiftiVolume, AxisProjection = self.ui.XYSliceView.getAxisProjectionControls (), Coordinate = self.SelectedCoordinate)
                       self.ui.XZSliceVerticalSelector.setROISliceDictionary (self.ROIDictionary, self.NiftiVolume, self._ProjectDataset)
                       self.ui.YZSliceVerticalSelector.setROISliceDictionary (self.ROIDictionary, self.NiftiVolume, self._ProjectDataset)
                       self.ui.XYSliceVerticalSelector.setROISliceDictionary (self.ROIDictionary, self.NiftiVolume, self._ProjectDataset, AxisProjection = self.ui.XYSliceView.getAxisProjectionControls (), Coordinate = self.SelectedCoordinate)
                       
           if (eventtype == "EndMouseMove") : #if event is ending then zoom in                                              
                  if not self.handleAxisRotationMarkerSelection (cx,cy,cz, sliceview, eventtype) : 
                      self.NiftiAxisSelection (cx, cy, cz, sliceview.getSliceAxis (), True)                      
                  self._SelectionMoveROIState = None
                  self.ROIDictionary._CallDictionaryChangedListner  ()  
           else: 
               self.ui.XYSliceView.setROIObjectList (self.ROIDictionary) 
               self.ui.XZSliceView.setROIObjectList (self.ROIDictionary)
               self.ui.YZSliceView.setROIObjectList (self.ROIDictionary) 
            
           sliceview.clearTemporaryDrawLine ()    
           sliceview.setSelected (True)           
           self.update () 
           return 
  
    @staticmethod #@jit (cache=True)
    def clipPtDest (pt, maxPt):        
        return min (max (pt, 0), maxPt)            
    
    @staticmethod    #@jit (cache=True)
    def _drawPenMaskAt (temporaryObjectMask, penmask, CenterPenX, CenterPenY, PenDimX, PenDimY, halfPenXDimension, halfPenYDimension, penSettings, NiftiVolumeSlice, ignoreVoxelMask, hasSingleROIPerVoxel, ValidSpaceClipMask):        
        #compute unclipped Position for mask
        original_DestinationStartX = CenterPenX - halfPenXDimension
        original_DestinationStartY = CenterPenY - halfPenYDimension
        px_ed = original_DestinationStartX + PenDimX
        py_ed = original_DestinationStartY + PenDimY
        
        #clip drawing to destination
        destinationWidth,  destinationHeight = temporaryObjectMask.shape
        px_st = RCC_ContourWindow.clipPtDest (original_DestinationStartX, destinationWidth)
        py_st = RCC_ContourWindow.clipPtDest (original_DestinationStartY, destinationHeight)
        px_ed = RCC_ContourWindow.clipPtDest (px_ed, destinationWidth)
        py_ed = RCC_ContourWindow.clipPtDest (py_ed, destinationHeight)
        
        #clip Pen
        sx_st = px_st - original_DestinationStartX
        sy_st = py_st - original_DestinationStartY
        PenWidth = px_ed - px_st 
        PenHeight = py_ed - py_st 
        penmask = np.copy (penmask[sx_st:sx_st + PenWidth, sy_st:sy_st + PenHeight])
        
        if ValidSpaceClipMask is not None :
           ValidSpaceClipMask = ValidSpaceClipMask[px_st:px_ed, py_st:py_ed]
        #draw
        subobj = temporaryObjectMask[px_st:px_ed, py_st:py_ed]        
        if (penSettings.isThresholded ()):
            niftiSliceData = NiftiVolumeSlice[px_st:px_ed, py_st:py_ed]
            thresholdMask = penSettings.getThresholdMask (niftiSliceData)
            if ValidSpaceClipMask is not None :
                np.logical_and (penmask, ValidSpaceClipMask, out = penmask)            
            np.logical_and (thresholdMask, penmask, out = thresholdMask)
            if  hasSingleROIPerVoxel and  ignoreVoxelMask is not None and penSettings.isAddPen () :               
                subIgnoreVoxelMask = ignoreVoxelMask[px_st:px_ed, py_st:py_ed]                
                thresholdMask[subIgnoreVoxelMask] = False
            if (penSettings.getAutoEraseOutofRangeinThresholdDraw ()) :
                subobj[penmask] = -1
            subobj[thresholdMask] = penSettings.getPenOpperation ()
        else:    
            if hasSingleROIPerVoxel and  ignoreVoxelMask is not None and penSettings.isAddPen () :
                subIgnoreVoxelMask = ignoreVoxelMask[px_st:px_ed, py_st:py_ed]                
                penmask[subIgnoreVoxelMask] = False
            
            if ValidSpaceClipMask is not None :
                np.logical_and (penmask, ValidSpaceClipMask, out = penmask)
            subobj[penmask] = penSettings.getPenOpperation ()
        temporaryObjectMask[px_st:px_ed, py_st:py_ed] = subobj
    
    
    def getPenIndex (self) :
        try :
            return self._PenIndex 
        except:
            self._PenIndex = 0
            return self._PenIndex 
        
    def setPenIndex (self, val) :
        self._PenIndex = val 

    def isTemoraryPaintObjDefined (self) :
        try :
            return (self._tempPaintObj is not None) 
        except:
            return False
    
    def updateTemporaryPaintObj (self, newobj):
         if self.isTemoraryPaintObjDefined() :
             self._tempPaintObj["Obj"] = newobj

    def isTemporaryPaintObjAllocated (self) :
        if self.ROIDictionary is not None :             
            try :
                if self.isTemoraryPaintObjDefined() :
                    return True, self.getTemporaryPaintROIObj ()
            except:
                pass
        return False, None
    
    def getTemporaryPaintObj (self,itemName, sliceView) :
        if self.ROIDictionary is None : 
            return None, False        
        try :
            if self.isTemoraryPaintObjDefined() :
                return self.getTemporaryPaintROIObj (), False           
        except:
            self._tempPaintOb = None
        sliceNumber = sliceView.getSliceNumber ()           
        SliceAxis = sliceView.getSliceAxis ()
        if (not self.ROIDictionary.isROIDefined (itemName)) :
            newObj = MultiROIAreaObject ()     
            newObj.setROIIDNumber (self.ROIDefs.getROIIDNumber (itemName))
            newObj.setZBoundBox (self.ui.XYSliceView.getSliceNumber ())
        else:
            newObj =  self.ROIDictionary.getROIObject (itemName)
           
        maskData = newObj.getTemporaryObjectSliceMask (sliceView, ClipOverlayingROI = self.ROIDefs.hasSingleROIPerVoxel (), NiftiVolumeDim = self.NiftiVolume.getSliceDimensions())      
            
        clearObjList = []
        sliceObjList = self.ROIDictionary.getROIAxisList (self.SelectedCoordinate, SliceAxis, niftivolume = self.NiftiVolume, AxisProjectionControls = sliceView.getAxisProjectionControls ()) 
        ignoreVoxelMask = None
        for obj in sliceObjList :
            if obj.isROIArea () and obj !=  newObj:         
                name = obj.getName (self.ROIDefs)
                if (not self.ROIDefs.isROILocked (name) ):            
                    if (self.ROIDefs.hasSingleROIPerVoxel () or self.getPenSettings().isErasePen()) :
                        tempMask = obj.getTemporaryObjectSliceMask (sliceView, ClearBoundingBox = False, ClipOverlayingROI = self.ROIDefs.hasSingleROIPerVoxel (), NiftiVolumeDim = self.NiftiVolume.getSliceDimensions())
                        if (tempMask is not None) :                        
                            clearObjList.append ((obj, obj.getSliceViewObjectMask  (sliceView, ClipOverlayingROI = False)))
                else:                    
                    imask = obj.getSliceViewObjectMask  (sliceView, ClipOverlayingROI = self.ROIDefs.hasSingleROIPerVoxel ()) > 0
                    if (ignoreVoxelMask is None) :                                                
                        ignoreVoxelMask = imask
                    else:
                        ignoreVoxelMask[imask] = True
                        
        newObj.setROIIDNumber (self.ROIDefs.getROINameIDNumber (itemName) )
        #if ignoreVoxelMask is not None :
        #    ignoreVoxelMask = (1 == (1- ignoreVoxelMask))
        self._tempPaintObj = {}
        self._tempPaintObj["Obj"] = newObj
        self._tempPaintObj["MaskData"] = np.copy (maskData)
        self._tempPaintObj["ClearObjectList"] = clearObjList
        self._tempPaintObj["IgnoreVoxelMask"] = ignoreVoxelMask
        self._tempPaintObj["SliceNumber"] = sliceNumber
        self._tempPaintObj["isTemporaryPaintObjCoordinateChangeOnAddDisabled"] = False
        self._tempPaintObj["SliceAxis"] = SliceAxis
        #(newObj, np.copy (maskData), clearObjList, ignoreVoxelMask, sliceNumber, False)
        return newObj, True
    
    def isTemporaryPaintObjCoordinateChangeOnAddDisabled (self) :
        return self._tempPaintObj["isTemporaryPaintObjCoordinateChangeOnAddDisabled"]
        
    def setDisableTemporaryPaintObjCoordinateChangeAdd (self) :        
        if self.isTemoraryPaintObjDefined() :
            self._tempPaintObj["isTemporaryPaintObjCoordinateChangeOnAddDisabled"]  = True        
        
    def getTemporaryPaintROIObj (self) :
        return self._tempPaintObj["Obj"]
    
    def getTemporaryPaintObjMaskData (self) :
        return self._tempPaintObj["MaskData"]
        
    def getTemporaryPaintObjAxis (self) :
        return self._tempPaintObj["SliceAxis"]
    
    def getTemporaryPaintObjSliceNumber (self) :
        return self._tempPaintObj["SliceNumber"]
    
    def getIgnoreVoxelMask (self) :
        return self._tempPaintObj["IgnoreVoxelMask"]
    
    def getClearPaintObjectList (self) :
        return self._tempPaintObj["ClearObjectList"]
        
    def isTemporaryObjectSliceMaskChanged (self, testmask, ClipBox = None) :
        if ClipBox is not None :
            xmin, xmax, ymin, ymax = ClipBox
            if xmin is not None :
                xmin = max( 0, min (xmin, testmask.shape[0] - 1))
                xmax = max( 0, min (xmax + 1, testmask.shape[0] - 1))
                ymin = max( 0, min (ymin, testmask.shape[1] - 1))
                ymax = max( 0, min (ymax + 1, testmask.shape[1] - 1))
                return  np.any (testmask[xmin:xmax, ymin:ymax]- self.getTemporaryPaintObjMaskData()[xmin:xmax, ymin:ymax])
            return False
        return  np.any (testmask - self.getTemporaryPaintObjMaskData())
    
    def getTemporaryObjectSliceMaskChangedBox (self, ChangeMap = None) :
           box = ChangeMap
           minX, minY, maxX, maxY = None, None, None, None
           for x in range (box.shape[0]) :
               if np.any (box[x,:] != 0) :
                   minX = x
                   break
           if (minX is None) :
               return (minX, minY, maxX, maxY)
           for x in range (box.shape[0]-1,-1,-1) :
               if np.any (box[x,:] != 0) :
                   maxX = x
                   break
           for y in range (box.shape[1]) :
               if np.any (box[:,y] != 0) :
                   minY = y
                   break
           for y in range (box.shape[1]-1,-1,-1) :
               if np.any (box[:,y] != 0) :
                   maxY = y
                   break
           return (minX, minY, maxX, maxY)
    
    def clearTemporaryPaintObj (self) :
        self._tempPaintObj = None 
    
    def setPenSettingsFromProject (self, settings) :
        if (self._penSettings is not None) :                
            self._DisablePenSettingProjectChange = True
            self._penSettings.copyThresholdPresetsAndROIPresetsFromProject (settings)
            self.updatePaintBrushIfNecessary ()
            if self._PenSettingsDlg is not None :
                self._PenSettingsDlg.updateToReflectChangedPenSettings ()
            self._DisablePenSettingProjectChange = False
            
    def getPenSettings (self) :
        try :
            if (self._penSettings is not None) :                
                return self._penSettings                    
        except:
             self._penSettings = None        
        if (self._ProjectDataset is not None) :
            self._penSettings = self._ProjectDataset.getPenSettings ()
        else:
            self._penSettings = PenSettings ()
        self._penSettings.addChangeListener (self.penSettingsChanged)        
        return self._penSettings
    
    def penSettingsChanged (self) :        
        if not self._DisablePenSettingProjectChange  :
            if (self._ProjectDataset is not None and self._penSettings is not None) :
                self._ProjectDataset.setPenSettings (self._penSettings.copy (), self)        
        self.updatePaintBrushIfNecessary ()
        if (self._penSettings is not None and self._penSettings.getPenTool () == "Draw") :
            self.ui.XYSliceView.setCursor(QtCore.Qt.BlankCursor)
            self.ui.YZSliceView.setCursor(QtCore.Qt.BlankCursor)
            self.ui.XZSliceView.setCursor(QtCore.Qt.BlankCursor)
        else:
            self.ui.XYSliceView.setCursor(QtCore.Qt.ArrowCursor)
            self.ui.YZSliceView.setCursor(QtCore.Qt.ArrowCursor)
            self.ui.XZSliceView.setCursor(QtCore.Qt.ArrowCursor)
       
        
    
    
    #@jit (cache=True)
    def computeLineDrawPoints (self, line, PenIndex) :                  
        returnPointList = []
        isList = type(line) == type([])         
        if (isList) :
            lineLength = len (line)        
        else:
            lineLength = line.shape[0]    
        if (PenIndex < lineLength) :  
            if PenIndex > 0 :
                PenIndex -= 1             
            if (lineLength - PenIndex == 1) :                  
                if (isList) :
                    returnPointList.append ((line[PenIndex][0], line[PenIndex][1]))               
                else:
                    returnPointList.append ((line[PenIndex, 0], line[PenIndex, 1]))                
            else:
                if (isList) :
                    line = np.array (line, dtype = np.int)
                firstP = line[PenIndex:lineLength-1]                
                dP = line[PenIndex+1:lineLength]-firstP
                dist = dP * dP                
                dist = np.sqrt (dist[:, 0] + dist[:, 1]) + sys.float_info.epsilon                
                points = np.array (list (range (int (np.max (dist))+1)),dtype = np.float)
                
                dx = dP[:,0] / dist
                dy = dP[:,1] / dist                           
                for index in range (dx.shape[0]) :                    
                    if (dist[index] > sys.float_info.epsilon) :                        
                        ptList = points[:int (dist[index])+1]                        
                        yPoints = (ptList * dy[index] + firstP[index, 1]).astype (np.int)
                        xPoints = (ptList * dx[index] + firstP[index, 0]).astype (np.int)
                            
                        for ptIndex in range (xPoints.shape[0]) :                                                    
                            returnPointList.append ((xPoints[ptIndex], yPoints[ptIndex]))
        self.setPenIndex (lineLength)
        return returnPointList
        
    def drawPointList (self, temporaryObjectMask, PointDrawList, NiftiVolumeSliceData, penSettings, penmask, ClipMask = None) :
        pointDrawListLen = len (PointDrawList)
        xMin, xMax, yMin, yMax = None, None, None, None
        if (pointDrawListLen > 0) :            
            px, py = penmask.shape
            hpx = int (px / 2)
            hpy = int (py / 2)
            IgnoreVoxelMask = self.getIgnoreVoxelMask ()       
            SingleROIPerVoxel = self.ROIDefs.hasSingleROIPerVoxel()                 
            for ptIndex in range(pointDrawListLen) :
                xPt, yPt = PointDrawList[ptIndex]
                RCC_ContourWindow._drawPenMaskAt (temporaryObjectMask, penmask, xPt, yPt , px, py, hpx, hpy,  penSettings, NiftiVolumeSliceData, IgnoreVoxelMask, SingleROIPerVoxel, ClipMask)        
                if xMin is None :
                    xMin, xMax, yMin, yMax = xPt, xPt, yPt, yPt
                else:
                    xMin = min (xMin, xPt)
                    xMax = max (xMax, xPt)
                    yMin = min (yMin, yPt)
                    yMax = max (yMax, yPt)
            if xMin is not None :
                xMin -= hpx + 1
                xMax += hpx + 1
                yMin -= hpy + 1
                yMax += hpy + 1
        return (xMin, xMax, yMin, yMax)
    
    @staticmethod #@jit (cache=True)
    def applyDrawMask (drawMask, temporaryObjectMask, penSettings, invertAdd = False, ClipBox = None, ReturnChangeMask = False, ReturnAnyChange = True):        
        if ClipBox is not None :
            xmin, xmax, ymin, ymax = ClipBox
            if xmin is not None :
                xmin = max( 0, min (xmin, drawMask.shape[0]))
                xmax = max( 0, min (xmax + 1, drawMask.shape[0]))
                ymin = max( 0, min (ymin, drawMask.shape[1]))
                ymax = max( 0, min (ymax + 1, drawMask.shape[1]))
                clippedDrawMask = drawMask[xmin:xmax, ymin:ymax]
                IsMaskChanged = np.copy (temporaryObjectMask[xmin:xmax, ymin:ymax]).astype(dtype=np.int8)
                if (invertAdd and not penSettings.isErasePen ()):
                    temporaryObjectMask[xmin:xmax, ymin:ymax][clippedDrawMask > 0] = 0
                elif (not invertAdd or penSettings.isErasePen ()):            
                    if (penSettings.isErasePen ()) :
                        temporaryObjectMask[xmin:xmax, ymin:ymax][clippedDrawMask < 0] = 0                                
                    else:
                        temporaryObjectMask[xmin:xmax, ymin:ymax][clippedDrawMask < 0] = 0
                        temporaryObjectMask[xmin:xmax, ymin:ymax][clippedDrawMask > 0] = 1
                if ReturnAnyChange :
                    return np.any ((temporaryObjectMask[xmin:xmax, ymin:ymax]).astype(np.int8) - IsMaskChanged)
                return
        else:
            IsMaskChanged = np.copy (temporaryObjectMask).astype (np.int8)
            if (invertAdd and not penSettings.isErasePen ()):
                temporaryObjectMask[drawMask > 0] = 0
            elif not invertAdd or penSettings.isErasePen ():            
                if (penSettings.isErasePen ()) :
                    temporaryObjectMask[drawMask < 0] = 0                                
                else:
                    temporaryObjectMask[drawMask < 0] = 0
                    temporaryObjectMask[drawMask > 0] = 1
            if ReturnChangeMask :                
                changeMask = temporaryObjectMask.astype (np.int8) - IsMaskChanged
                return np.any (changeMask), changeMask
            elif  ReturnAnyChange :
                return np.any (temporaryObjectMask.astype (np.int8) - IsMaskChanged)
            return
                                                     

   
             
    def _getTemporaryObjectBoundingBoxChange (self, sliceView, obj, ChangeMap, SliceNumber= None, FirstSlice=None, LastSlice=None) :
        AreaOfImageAdded = self.getTemporaryObjectSliceMaskChangedBox (ChangeMap)                   
        x1, y1, x2, y2 = AreaOfImageAdded[0], AreaOfImageAdded[1], AreaOfImageAdded[2], AreaOfImageAdded[3]
        #xbox, ybox, zbox = obj.getBoundingBox ()
        sliceAxis = sliceView.getSliceAxis ()    
        if FirstSlice is None :
            FirstSlice = SliceNumber
            LastSlice = SliceNumber
        if sliceAxis == "Z" :
            return (x1, y1, FirstSlice, x2, y2,LastSlice)
        elif sliceAxis == "X" :
            return (FirstSlice, x1, y1, LastSlice, x2, y2)
        elif sliceAxis == "Y" :
            return (x1, FirstSlice, y1, x2, LastSlice, y2)
        
    def paintLineOnAdjacentSlice (self, sliceView, obj, crossSliceCord, penSettings, list_of_lines, rightMouseButton, centerSliceIndex, thickness, objectCopyTrackingDictionary, objSliceChangeDictionary):                
        if (thickness <= 1) :
            return
        
        listOfLineDrawPoints = []
        for line in list_of_lines :
            if len (line) > 0 :
                line = self.computeLineDrawPoints (np.array (line, dtype=np.int), 0)
                if len (line) > 0 :
                    listOfLineDrawPoints.append (line)
        if len (listOfLineDrawPoints) <= 0 :
            return obj
        
        sliceDims =  self.NiftiVolume.getSliceDimensions ()        
        thickness = int ((thickness -1) / 2)        
        sliceShape = sliceView.getSliceShape ()
        
        penmask = penSettings.getPenMask (self.NiftiVolume, sliceView) #np.ones ((10,10),dtype=np.uint8)
        drawMask =     drawMask = np.zeros (sliceShape, dtype = np.int8)                
        isFirstPass = True
        
        firstslice = centerSliceIndex-thickness
        lastslice = centerSliceIndex+thickness
        sliceAxis = sliceView.getSliceAxis ()
        if sliceAxis == "Z" :
            axisDim = sliceDims[2] - 1
        elif sliceAxis == "X" :
            axisDim = sliceDims[0] - 1
        elif sliceAxis == "Y" :
            axisDim = sliceDims[1] - 1
        firstslice = max(min(firstslice, axisDim),0)
        lastslice = max(min(lastslice, axisDim),0)        
        oldTempPaintObj = self._tempPaintObj
        for crossSliceIndex in range (firstslice, lastslice + 1) :
            if crossSliceIndex != centerSliceIndex :                                
                clearObjList = []
                ignoreVoxelMask = None
                if sliceAxis == "Z" :
                    crossSliceCord.setZCoordinate (crossSliceIndex)                                                    
                elif sliceAxis == "X" :
                    crossSliceCord.setXCoordinate (crossSliceIndex)                                                    
                elif sliceAxis == "Y" :
                    crossSliceCord.setYCoordinate (crossSliceIndex)                                                    
                sliceObjList = self.ROIDictionary.getROIAxisList (crossSliceCord,sliceAxis, niftivolume = self.NiftiVolume, AxisProjectionControls = sliceView.getAxisProjectionControls ())                                                            
                for sliceobj in sliceObjList :
                    if sliceobj.isROIArea () and obj !=  sliceobj:   
                        name = sliceobj.getName (self.ROIDefs)
                        objname = obj.getName (self.ROIDefs)
                        if objname != name :
                            if (not self.ROIDefs.isROILocked (name)):            
                                if self.ROIDefs.hasSingleROIPerVoxel () or self.getPenSettings().isErasePen() :                                  
                                    tempMask = sliceobj.getSliceViewObjectMask (sliceView, SliceNumber = crossSliceIndex, ClipOverlayingROI = False) > 0
                                    if (tempMask is not None) :
                                        clearObjList.append ((sliceobj, np.copy(tempMask)))
                            else:                    
                                ivm = sliceobj.getSliceViewObjectMask (sliceView, SliceNumber = crossSliceIndex, ClipOverlayingROI = self.ROIDefs.hasSingleROIPerVoxel ()) > 0
                                if (ignoreVoxelMask is None) :                        
                                    ignoreVoxelMask = ivm
                                else:
                                    ignoreVoxelMask[ivm] = True                                                                    
                
                #self._tempPaintObj = (None, None, clearObjList, None)
                self._tempPaintObj = {}
                self._tempPaintObj["Obj"] = None
                self._tempPaintObj["MaskData"] = None
                self._tempPaintObj["ClearObjectList"] = clearObjList
                self._tempPaintObj["IgnoreVoxelMask"] = None
                self._tempPaintObj["SliceNumber"] = None
                self._tempPaintObj["isTemporaryPaintObjCoordinateChangeOnAddDisabled"] = True
                self._tempPaintObj["SliceAxis"] = sliceAxis                
                
                if penSettings.isThresholded () :
                    niftisliceData, niftisliceDataClipMask = self.NiftiVolume.getImageData (crossSliceIndex, Axis=sliceAxis, Coordinate = self.SelectedCoordinate, AxisProjectionControls = sliceView.getAxisProjectionControls (), ReturnClipMask = True)
                elif  self.ui.XYSliceView.areAxisProjectionControlsEnabledForAxis () :
                    niftisliceData = None
                    _, niftisliceDataClipMask = self.NiftiVolume.getImageData (crossSliceIndex, Axis=sliceAxis, Coordinate = self.SelectedCoordinate, AxisProjectionControls = sliceView.getAxisProjectionControls (), ReturnClipMask = True)
                else:
                    niftisliceData = None
                    niftisliceDataClipMask = None
                    
                crossSliceMap = obj.getSliceViewObjectMask (sliceView, SliceNumber = crossSliceIndex, ClipOverlayingROI = self.ROIDefs.hasSingleROIPerVoxel ())                                                
                if penSettings.isErasePen () :
                    if (isFirstPass) :
                        isFirstPass = False
                        drawMask[...] = 0         
                        for linedrawpoints in listOfLineDrawPoints :
                            self.drawPointList ( drawMask, linedrawpoints, niftisliceData,penSettings, penmask, ClipMask = niftisliceDataClipMask)                                       
                    if ignoreVoxelMask is not None : 
                        drawMaskCopy = np.copy (drawMask)
                        drawMaskCopy[ignoreVoxelMask] = 0
                    else:
                        drawMaskCopy = drawMask
                        
                    maskChanged, ChangeMap = RCC_ContourWindow.applyDrawMask (drawMaskCopy, crossSliceMap, penSettings, ReturnChangeMask = True)
                    IDList = obj.getContourIDManger ().getAllocatedIDList ()
                    if len (IDList) > 0 :
                        contourID = IDList[0]
                    else:
                        contourID = obj.getContourIDManger ().allocateID() 
                    if maskChanged :    
                        obj = RCC_ContourWindow._copyObjectIfNecessary (objectCopyTrackingDictionary, obj)
                        obj.setSliceViewObjectMask (sliceView,  contourID, crossSliceMap, crossSliceIndex, ChangeMap, objSliceChangeDictionary)
                    for tpl in clearObjList :
                        sliceobj, crossSliceMap  = tpl                                                                                                                                                                                                                          
                        maskChanged, ChangeMap = RCC_ContourWindow.applyDrawMask (drawMaskCopy, crossSliceMap, penSettings, invertAdd = False, ReturnChangeMask = True)
                        if maskChanged :
                            sliceobj = RCC_ContourWindow._copyObjectIfNecessary (objectCopyTrackingDictionary, sliceobj)
                            IDList = sliceobj.getContourIDManger ().getAllocatedIDList ()
                            if len (IDList) > 0 :
                                contourID = IDList[0]
                            else:
                                contourID = sliceobj.getContourIDManger ().allocateID()                                                                      
                            #sliceobj.setSliceROIAreaMask (contourID, crossSliceIndex , crossSliceMap, sliceShape, "Human")                                                                                  
                            sliceobj.setSliceViewObjectMask (sliceView,  contourID, crossSliceMap,  crossSliceIndex, ChangeMap, objSliceChangeDictionary)
                                        
                elif penSettings.isAddPen () :                    
                    if (isFirstPass or penSettings.isThresholded ()) :
                        isFirstPass = False
                        drawMask[...] = 0                                
                        if (rightMouseButton) :
                            penSettings.setErasePen (UpdateSettings = False)
                            for linedrawpoints in listOfLineDrawPoints :
                                self.drawPointList ( drawMask, linedrawpoints, niftisliceData ,penSettings, penmask, ClipMask = niftisliceDataClipMask)                                       
                            penSettings.setAddPen (UpdateSettings = False)
                        else:
                            for linedrawpoints in listOfLineDrawPoints :
                                self.drawPointList ( drawMask, linedrawpoints, niftisliceData ,penSettings, penmask, ClipMask = niftisliceDataClipMask)                                                               
                    if ignoreVoxelMask is not None : 
                        drawMaskCopy = np.copy (drawMask)
                        drawMaskCopy[ignoreVoxelMask] = 0
                    else:
                        drawMaskCopy = drawMask
                    isMapChanged, ChangeMap = RCC_ContourWindow.applyDrawMask (drawMaskCopy, crossSliceMap, penSettings, ReturnChangeMask = True)
                    if (isMapChanged) :  
                        obj = RCC_ContourWindow._copyObjectIfNecessary (objectCopyTrackingDictionary, obj)                          
                        AreaOfImageAdded = self._getTemporaryObjectBoundingBoxChange (sliceView, obj, ChangeMap, FirstSlice = firstslice, LastSlice = lastslice)
                             
                        IDList = obj.getContourIDManger ().getAllocatedIDList ()
                        if len (IDList) > 0 :
                            contourID = IDList[0]
                        else:
                            contourID = obj.getContourIDManger ().allocateID()                                                                      
                        #obj.setSliceROIAreaMask (contourID, crossSliceIndex , crossSliceMap, sliceShape, "Human")   
                        obj.setSliceViewObjectMask ( sliceView,  contourID, crossSliceMap, crossSliceIndex, ChangeMap, objSliceChangeDictionary)
                        obj.adjustBoundBoxToFitSlice(crossSliceIndex, self.NiftiVolume.getSliceDimensions (), NewBoxArea = AreaOfImageAdded)
                        
                        if (self.ROIDefs.hasSingleROIPerVoxel() and not rightMouseButton) :                         
                            for tpl in clearObjList :
                                sliceobj, crossSliceMap  = tpl                                                                                                                                                                                                                                          
                                isMapChanged, ChangeMap = RCC_ContourWindow.applyDrawMask (drawMaskCopy, crossSliceMap, penSettings, invertAdd = True, ReturnChangeMask = True)
                                if isMapChanged :
                                   sliceobj = RCC_ContourWindow._copyObjectIfNecessary (objectCopyTrackingDictionary, sliceobj)
                                   IDList = sliceobj.getContourIDManger ().getAllocatedIDList ()
                                   if len (IDList) > 0 :
                                        contourID = IDList[0]
                                   else:
                                        contourID = sliceobj.getContourIDManger ().allocateID()                                                                      
                                   #sliceobj.setSliceROIAreaMask (contourID, crossSliceIndex , crossSliceMap, sliceShape, "Human")  
                                   sliceobj.setSliceViewObjectMask (sliceView,  contourID, crossSliceMap, crossSliceIndex, ChangeMap, objSliceChangeDictionary)                                                                                  
        self._tempPaintObj = oldTempPaintObj
        return obj

    @staticmethod
    def _tryFill (point, drawMask, temporaryObjectMask, penSettings, fillContour, ignoreVoxelMask, FillDiagonal = True, SingleROIPerVoxelMode = False):
        cX,cY = point        
        if (cX >= 0 and cY >= 0 and cX < temporaryObjectMask.shape[0] and cY < temporaryObjectMask.shape[1] and temporaryObjectMask.shape == drawMask.shape) :
            if penSettings.isAddPen () or (penSettings.isErasePen () and temporaryObjectMask[cX, cY] == 1) :
                if (fillContour) :
                    background = temporaryObjectMask > 0
                else:
                    contours = temporaryObjectMask > 0
                    background = ndimage.morphology.binary_fill_holes (contours)
                    background[contours] = False
                
                if SingleROIPerVoxelMode :                
                    if (ignoreVoxelMask is not None) :  
                        background[ignoreVoxelMask] = False
                    
                if background [cX, cY] == 0 :
                    return False
                else:
                    structure = np.ones ((3,3), dtype=np.uint8)  
                    if not FillDiagonal :
                        structure[0,0] = 0
                        structure[2,0] = 0
                        structure[0,2] = 0
                        structure[2,2] = 0
                    labelimage, features = ndimage.measurements.label (background, structure)
                    selectedFeature = labelimage[cX,cY]                        
                    drawMask[labelimage == selectedFeature] = penSettings.getPenOpperation ()
                    return True
            
    
    def _ShowSelectROIToFillDlg (self, ROIList, drawMask, Msg = None) :
        dlg = SelectROIToFillDlg (self, ROIList, Msg = Msg)
        if dlg.exec_ () != QDialog.Accepted :
            drawMask[...] = False
            return None, None
        returnTpl, cachedDrawMask = dlg.getSelectedObject ()
        if cachedDrawMask is not None : 
            drawMask[...] = cachedDrawMask[...]
        else:
            drawMask[...] = False
        return returnTpl
        
           
    def selectIsland (self, sliceView, point, drawMask, penSettings, Skip2DIslandTest = False) :
        objsinsliceLst = self.ROIDictionary.getROIAxisList (self.SelectedCoordinate, sliceView.getSliceAxis (), niftivolume = self.NiftiVolume, AxisProjectionControls = sliceView.getAxisProjectionControls ()) 
        testObjList = []
        for obj in objsinsliceLst :  
            objName = obj.getName (self.ROIDictionary.getROIDefs ())
            if self.ROIDictionary.getROIDefs ().isROIHidden (objName) or self.ROIDictionary.getROIDefs ().isROILocked (objName) or not obj.isROIArea () :
                continue
            else:
                obj.clearTemporaryObjectSliceMask ()
                testObjList.append (obj)
        if (len (testObjList) <= 0) :
           MessageBoxUtil.showMessage ("ROI Island Removal Tool", "No editable ROI on selected slice.") 
           return None, None 
                
        SingleROIPerVoxelMode = self.ROIDictionary.getROIDefs ().hasSingleROIPerVoxel ()
        ROIIslandSelectionList = []
        structure = np.ones ((3,3), dtype=np.uint8)  
        cX,cY = point        
        ClickedOnObject = False
        for obj in testObjList :
            sliceMap =  obj.getSliceViewObjectMask (sliceView, ClipOverlayingROI = SingleROIPerVoxelMode) > 0                
            if (cX >= 0 and cY >= 0 and cX < sliceMap.shape[0] and cY < sliceMap.shape[1]) :
                if sliceMap[cX, cY] :
                    ClickedOnObject = True
                    labelimage, features = ndimage.measurements.label (sliceMap, structure)
                    selectedFeature = labelimage[cX,cY]
                    if selectedFeature > 0 :
                        if penSettings.isAddPen () :   
                            selection = labelimage != selectedFeature
                            selection[labelimage == 0] = False
                            if Skip2DIslandTest or np.any (sliceMap[selection]) :
                                changeMask = np.copy (drawMask)
                                changeMask[selection] = -1
                                objName = obj.getName (self.ROIDictionary.getROIDefs ())
                                ROIIslandSelectionList.append ((objName,(obj, sliceMap), changeMask))
                        elif penSettings.isErasePen () :
                            selection = labelimage == selectedFeature
                            if Skip2DIslandTest or np.any (sliceMap[selection]) :
                                changeMask = np.copy (drawMask)
                                changeMask[selection] = -1
                                objName = obj.getName (self.ROIDictionary.getROIDefs ())
                                ROIIslandSelectionList.append ((objName,(obj, sliceMap), changeMask))
        
        if len (ROIIslandSelectionList) == 1 :
            _, returnTuple, DrawMaskCache = ROIIslandSelectionList[0]
            drawMask[...] = DrawMaskCache[...]
            return returnTuple
        elif len (ROIIslandSelectionList) > 1 :
            return self._ShowSelectROIToFillDlg (ROIIslandSelectionList, drawMask, Msg = "Select ROI to remove islands from.")
        elif not ClickedOnObject :
            MessageBoxUtil.showMessage ("ROI Island Removal Tool", "Select a ROI to remove islands from.") 
        return None, None 
                    
                   
        
        
        
    def fillMask (self, sliceView, point, drawMask, penSettings, selectedobj) :                
        objList = []
        objHiddenList = []
        objOrigionalMask = {}
        objsinsliceLst = self.ROIDictionary.getROIAxisList (self.SelectedCoordinate,sliceView.getSliceAxis (), niftivolume = self.NiftiVolume, AxisProjectionControls = sliceView.getAxisProjectionControls ()) 
        if (selectedobj not in objsinsliceLst) :
            objsinsliceLst.append (selectedobj)
        objsinsliceLst = self.ROIDefs.getObjectLstInROIOrder (objsinsliceLst)          
        sliceShape = sliceView.getSliceShape ()
        ignoreVoxelMask  = None        
        SingleROIPerVoxelMode = self.ROIDictionary.getROIDefs ().hasSingleROIPerVoxel ()
        for obj in objsinsliceLst :
            if (obj.isROIArea ()) :
                    objName = obj.getName (self.ROIDictionary.getROIDefs ())
                    if  self.ROIDictionary.getROIDefs ().isROILocked (objName):
                        objHiddenList.append (obj)
                    else:
                        objList.append (obj)
                        objOrigionalMask[obj] = obj.getSliceViewObjectMask (sliceView, ClipOverlayingROI = SingleROIPerVoxelMode) > 0                        
        
        
        if SingleROIPerVoxelMode :
            cX,cY = point 
            #print (("FillMask", cX,cY))
            for obj in objHiddenList :
                ignoremask = obj.getSliceViewObjectMask (sliceView, ClipOverlayingROI = SingleROIPerVoxelMode) > 0
                if ignoremask[cX,cY] > 0 :
                   MessageBoxUtil.showMessage ("Fill Tool", "ROI '%s' is locked and cannot be filled." % obj.getName (self.ROIDictionary.getROIDefs ()))                         
                   return None, None  
                if ignoreVoxelMask is None:
                    ignoreVoxelMask = ignoremask
                else:
                    ignoreVoxelMask[ignoremask] = True
                
            background = np.ones(sliceShape, dtype = np.uint8)  
            FillContourOperationList = [True]
        else:
             background = np.zeros(sliceShape, dtype = np.uint8)  
             FillContourOperationList = [True, False]
        
        niftisliceDataClipMask = None
        if (penSettings.isThresholded ()):  
            sliceAxis =  sliceView.getSliceAxis ()
            sliceNumber =  sliceView.getSliceNumber ()
            sliceData, niftisliceDataClipMask = self.NiftiVolume.getImageData (sliceNumber, Axis=sliceAxis, Coordinate = self.SelectedCoordinate, AxisProjectionControls = sliceView.getAxisProjectionControls (), ReturnClipMask = True)
            minTh, maxTh = penSettings.getThresholdRange ()          
            clearMask = sliceData < minTh
            clearMask[sliceData > maxTh] = True  
            if niftisliceDataClipMask is not None :
                np.logical_and (clearMask, niftisliceDataClipMask, out=clearMask)
            del sliceData
            del minTh
            del maxTh
        elif self.ui.XYSliceView.areAxisProjectionControlsEnabledForAxis () :
            sliceNumber =  sliceView.getSliceNumber ()
            sliceAxis =  sliceView.getSliceAxis ()
            _, niftisliceDataClipMask = self.NiftiVolume.getImageData (sliceNumber, Axis=sliceAxis, Coordinate = self.SelectedCoordinate, AxisProjectionControls = sliceView.getAxisProjectionControls (), ReturnClipMask = True)
        
        DrawMaskCache = np.copy (drawMask)
        MultipleROIPerVoxelFillObjList = []
        for fillContour in FillContourOperationList :                        
            for obj in objList :
                    drawMask[...] = DrawMaskCache[...]
                    slicemap = np.copy (objOrigionalMask[obj])
                    obj.clearTemporaryObjectSliceMask ()
                    if (RCC_ContourWindow._tryFill (point, drawMask, slicemap, penSettings, fillContour, ignoreVoxelMask, FillDiagonal = fillContour, SingleROIPerVoxelMode = SingleROIPerVoxelMode)) :
                        if (penSettings.isThresholded ()):  
                            if (fillContour and penSettings.getAutoEraseOutofRangeinThresholdDraw () and obj == selectedobj):
                                tempClearMask = np.copy (clearMask) 
                                tempClearMask[drawMask == 0] = False
                                drawMask[tempClearMask] = -1
                                del tempClearMask
                            else:
                                drawMask[clearMask] = 0
                        if SingleROIPerVoxelMode :       
                            if (fillContour) :
                                return obj, objOrigionalMask[obj]
                            else:
                                return selectedobj, objOrigionalMask[selectedobj]
                        else:
                            objName = obj.getName (self.ROIDictionary.getROIDefs ())
                            if (fillContour) :
                                if (obj != selectedobj) :
                                    fillCanidate = True
                                else:
                                    fillCanidate = False                                                                        
                                    if RCC_ContourWindow.applyDrawMask (drawMask, np.copy (slicemap), penSettings) :                                    
                                         fillCanidate = True
                                if fillCanidate :
                                    if niftisliceDataClipMask is not None :
                                        drawMask[niftisliceDataClipMask==False] = 0
                                    MultipleROIPerVoxelFillObjList.append ((objName,(obj, objOrigionalMask[obj]), np.copy (drawMask)))
                            else:
                                if niftisliceDataClipMask is not None :
                                    drawMask[niftisliceDataClipMask==False] = 0
                                MultipleROIPerVoxelFillObjList.append ((objName,(selectedobj, objOrigionalMask[selectedobj]), np.copy (drawMask)))
                    if SingleROIPerVoxelMode :
                        background[slicemap > 0] = 0   
                    else:
                        if (fillContour) :
                            background[slicemap > 0] = 1  
                
        if len (MultipleROIPerVoxelFillObjList) == 1 :
            _, returnTuple, DrawMaskCache = MultipleROIPerVoxelFillObjList[0]
            drawMask[...] = DrawMaskCache[...]
            return returnTuple
        elif len (MultipleROIPerVoxelFillObjList) > 1 :
            return self._ShowSelectROIToFillDlg (MultipleROIPerVoxelFillObjList, drawMask)
        
        backgroundFilled = False
        if SingleROIPerVoxelMode :
            if  RCC_ContourWindow._tryFill (point, drawMask, background, penSettings, fillContour, ignoreVoxelMask, FillDiagonal = False, SingleROIPerVoxelMode = SingleROIPerVoxelMode) :     
                backgroundFilled = True
        else:
            if RCC_ContourWindow._tryFill (point, drawMask, background.astype(np.uint8), penSettings, False, ignoreVoxelMask, FillDiagonal = False, SingleROIPerVoxelMode = SingleROIPerVoxelMode) :
                backgroundFilled = True
            background = 1 - background
            if RCC_ContourWindow._tryFill (point, drawMask, background.astype(np.uint8), penSettings, True, ignoreVoxelMask, FillDiagonal = False, SingleROIPerVoxelMode = SingleROIPerVoxelMode) :
                backgroundFilled = True
        if backgroundFilled :
            if (penSettings.isThresholded ()):  
                drawMask[clearMask] = 0
            if niftisliceDataClipMask is not None :
                drawMask[niftisliceDataClipMask==False] = 0
            return selectedobj, objOrigionalMask[selectedobj]    
        MessageBoxUtil.showMessage ("Fill Tool", "The background cannot be filled. Select a ROI to fill or a whole within an ROI.")                         
        return None, None 
    
    
    #getSliceViewObjectMask (self, sliceView, SliceNumber = None, ClipOverlayingROI = False)
    
    def _applyFilterToSliceHelper (self, filterName, sliceView, sliceNumber, selectedobj, penSettings) :                                
        ignoreVoxelMask  = None
        objOrigionalMask = OrderedDict ()
        sliceAxis = sliceView.getSliceAxis ()
        objsinsliceLst = self.ROIDictionary.getROIAxisList (self.SelectedCoordinate, sliceAxis, niftivolume = self.NiftiVolume, AxisProjectionControls = sliceView.getAxisProjectionControls ()) 
        if (selectedobj not in objsinsliceLst) :
            objsinsliceLst.append (selectedobj)
        objsinsliceLst = self.ROIDefs.getObjectLstInROIOrder (objsinsliceLst)  
        
        hasSingleROIPerVoxel = self.ROIDefs.hasSingleROIPerVoxel ()
        for obj in objsinsliceLst :
            if (obj.isROIArea ()) :
                    objName = obj.getName (self.ROIDictionary.getROIDefs ())
                    if  self.ROIDictionary.getROIDefs ().isROILocked (objName) :
                         if ignoreVoxelMask is None:
                             ignoreVoxelMask = obj.getSliceViewObjectMask (sliceView, sliceNumber, ClipOverlayingROI = hasSingleROIPerVoxel) > 0
                         else:
                             slmsk = obj.getSliceViewObjectMask (sliceView, sliceNumber, ClipOverlayingROI = hasSingleROIPerVoxel) > 0
                             np.bitwise_or (ignoreVoxelMask, slmsk, out = ignoreVoxelMask)                             
                    else:                        
                        objOrigionalMask[obj] = obj.getSliceViewObjectMask (sliceView, sliceNumber, ClipOverlayingROI = hasSingleROIPerVoxel) > 0
        
        origionalImage = selectedobj.getSliceViewObjectMask (sliceView, sliceNumber, ClipOverlayingROI = hasSingleROIPerVoxel)
        origionalMask = origionalImage > 0   
        if (filterName == "binary_fill_holes") :
            filledDrawMask = ndimage.morphology.binary_fill_holes (origionalMask)
        elif (filterName == "binary_erosion") :
            filledDrawMask = ndimage.morphology.binary_erosion (origionalMask)
        elif (filterName == "binary_dilation") :          
            filledDrawMask = ndimage.morphology.binary_dilation (origionalMask) 
        else:
            return None
        
        filledDrawMask = filledDrawMask.astype (np.int8) -  origionalMask.astype (np.int8)
        if (penSettings.isThresholded ()):  
            sliceData, niftisliceDataClipMask = self.NiftiVolume.getImageData (sliceNumber,Axis=sliceAxis, Coordinate = self.SelectedCoordinate, AxisProjectionControls = sliceView.getAxisProjectionControls (), ReturnClipMask = True)
            minTh, maxTh = penSettings.getThresholdRange ()
            filledDrawMask[sliceData < minTh] = 0
            filledDrawMask[sliceData > maxTh] = 0 
            if niftisliceDataClipMask is not None :
                filledDrawMask[niftisliceDataClipMask == False] = 0
        elif self.ui.XYSliceView.areAxisProjectionControlsEnabledForAxis () :
            _, niftisliceDataClipMask  = self.NiftiVolume.getImageData (sliceNumber,Axis=sliceAxis, Coordinate = self.SelectedCoordinate, AxisProjectionControls = sliceView.getAxisProjectionControls (), ReturnClipMask = True)
            if niftisliceDataClipMask is not None :
                filledDrawMask[niftisliceDataClipMask == False] = 0
                
        if (ignoreVoxelMask is not None) :
           filledDrawMask[ignoreVoxelMask] = 0
           
        if not np.any (filledDrawMask) :
           return None        
        returnObjLst = [(selectedobj, origionalImage, filledDrawMask)]
        if not self.ROIDefs.hasSingleROIPerVoxel () :  #if not in single ROI mode then only return object
           return returnObjLst
        elif (penSettings.isAddPen ()) :
           filledIndex = filledDrawMask > 0
           for obj in objOrigionalMask.keys () :
              if obj is not None and obj != selectedobj :
                  origionalMask = (objOrigionalMask[obj]).astype (np.int8)
                  maskCopy = np.copy (origionalMask)
                  maskCopy[filledIndex] = 0
                  maskCopy -= origionalMask
                  if np.any (maskCopy) :
                      returnObjLst.append ((obj, origionalMask, maskCopy))
        return returnObjLst
              
    def filterSlice (self, filterName, firstSlice, lastSlice, sliceView,  selectedobj) : 
           if(self.NiftiVolume is None) :
               return
           
           objName = selectedobj.getName (self.ROIDictionary.getROIDefs ())
           if self.ROIDictionary.getROIDefs ().isROIHidden (objName) or self.ROIDictionary.getROIDefs ().isROILocked (objName) :
               return
           
           penSettings = self.getPenSettings().copy ()
           if (filterName == "binary_fill_holes") :
                penSettings.setAddPen ()
           elif (filterName == "binary_erosion") :
                penSettings.setErasePen () 
           elif (filterName == "binary_dilation") :          
                penSettings.setAddPen () 
            
           sliceViewAxis = sliceView.getSliceAxis ()
           roiList = []
           newObjDict = {}
           if sliceViewAxis != "Z" or sliceView.areAxisProjectionControlsEnabledForAxis () :
               for sliceNumber in range (firstSlice, lastSlice + 1) :
                   objlst = self._applyFilterToSliceHelper (filterName, sliceView, sliceNumber, selectedobj, penSettings)
                   if objlst is not None :
                       SliceViewObjectsChanged = set () 
                       for objtpl in objlst :
                           obj, origionalMask ,changeMask = objtpl
                           maskChanged, ChangeMap = RCC_ContourWindow.applyDrawMask (changeMask, origionalMask, penSettings, ReturnChangeMask = True)
                           if maskChanged :
                               itemName = obj.getName (self.ROIDefs)
                               if itemName in  newObjDict :
                                   obj = newObjDict[itemName]
                               else:
                                   obj = obj.copy (MoveSliceMaskVolumeCache = True) 
                                   roiList.append ((itemName, obj))
                                   IDList = obj.getContourIDManger ().getAllocatedIDList ()
                                   if len (IDList) > 0 :
                                       contourID = IDList[0]
                                   else:
                                       contourID = obj.getContourIDManger ().allocateID()  
                                   obj.setSelectedContours ([contourID])
                                   newObjDict[itemName] = obj
                               obj.setSliceViewObjectMask (sliceView,  contourID, origionalMask, sliceNumber, ChangeMap, SliceViewObjectsChanged, ContourType = "Human")   
                       for obj in SliceViewObjectsChanged :
                           obj.endCrossSliceChange ()
           else:
               for sliceNumber in range (firstSlice, lastSlice + 1) :
                   objlst = self._applyFilterToSliceHelper (filterName, sliceView, sliceNumber, selectedobj, penSettings)
                   if objlst is not None :
                       for objtpl in objlst :
                           obj, origional,changeMask = objtpl
                           maskChanged, ChangeMap = RCC_ContourWindow.applyDrawMask (changeMask, origional, penSettings, ReturnChangeMask = True)
                           if  maskChanged :
                               itemName = obj.getName (self.ROIDefs)
                               if itemName in  newObjDict :
                                   obj = newObjDict[itemName]
                               else:
                                    obj = obj.copy (MoveSliceMaskVolumeCache = True) 
                                    roiList.append ((itemName, obj))   
                                    IDList = obj.getContourIDManger ().getAllocatedIDList ()
                                    if len (IDList) > 0 :
                                       contourID = IDList[0]
                                    else:
                                       contourID = obj.getContourIDManger ().allocateID()     
                                    obj.setSelectedContours ([contourID])
                                    newObjDict[itemName] = obj
                           AreaOfImageAdded = self.getTemporaryObjectSliceMaskChangedBox (ChangeMap)
                           if (obj.isBoundingBoxDefined () and AreaOfImageAdded[0] is None) :
                               xbox, ybox, _ = obj.getBoundingBox ()
                               AreaOfImageAdded = ( xbox[0], ybox[0], xbox[1], ybox[1])
                           obj.adjustBoundBoxToFitSlice(sliceNumber, self.NiftiVolume.getSliceDimensions (), NewBoxArea =  AreaOfImageAdded)
                           #done increasing bounding box
                           obj.setSliceROIAreaMask (contourID, sliceNumber , origional, origional.shape, "Human")   
                           
            
           if len (roiList) > 0:             
              self.ROIDictionary.addROIList (roiList)
                                           
              for view in [self.ui.XYSliceView, self.ui.YZSliceView, self.ui.XZSliceView] :
                  view.setTempROIObjectList ([])
                  view.setROIObjectList (self.ROIDictionary)
                  view.update ()
    
    def _canFilterSlice (self, ShowErrorMsg = False, EnableMultiROISelection = False) :
         try :
             setEnabled = False
             msg = "Slice filtering requires NIfTI data and ROI data to be loaded."
             if (self.ROIDictionary is not None and self.NiftiVolume is not None and self.isROIDictonaryFileLoaded ()) :
                msg = "Filtering is disabled, data is readonly."
                if (not self.ROIDictionary.isReadOnly ()) :
                    msg = "Slice filtering requires the selection of one ROI with data defined for the currently selected slice."
                    if self.ROIDefs.isROISelected () :
                        msg = "Slice filtering requires the selection of one ROI with data defined for the currently selected slice."
                        if not EnableMultiROISelection :
                            if self.ROIDefs.isOneROISelected () or EnableMultiROISelection :    # if ROI is selected otherwise info message
                                itemName =self.ROIDefs.getSelectedROI ()[0]  # get currently selected ROI      
                                msg = "Slice filtering requires the selected ROI to be visible."                                       
                                if (not self.ROIDefs.isROIHidden (itemName)) :
                                    msg = "Slice filtering requires the selected ROI to be unlocked."                
                                    if (not self.ROIDefs.isROILocked (itemName)) :
                                        msg = "Slice filtering can only be applied to area ROI types. The selected ROI is not an area ROI."                
                                        if ( self.ROIDefs.isROIArea (itemName)) :  # tests if currently selected ROI is a area object  
                                            msg = "Slice filtering can only be applied when in area drawing mode."      
                                            if  self.ROIDictionary.isROIinAreaMode () :
                                                msg = "Slice filtering can only be applied on a ROI defined in the current dataset."    
                                                if self.ROIDictionary.isROIDefined (itemName) : 
                                                    msg = "Slice filtering requires the selection of one ROI with data defined for the currently selected slice." 
                                                    if (itemName != "None") :
                                                        obj = self.ROIDictionary.getROIObject(itemName)
                                                        msg = "Slice filtering requires the selection of one ROI with data defined." 
                                                        if (obj is not None) :
                                                            setEnabled = True
                        else:
                            for itemName in self.ROIDefs.getSelectedROI () :
                                if (not self.ROIDefs.isROILocked (itemName)) :
                                    msg = "Slice filtering can only be applied to area ROI types. The selected ROI is not an area ROI."                
                                    if ( self.ROIDefs.isROIArea (itemName)) :  # tests if currently selected ROI is a area object  
                                        msg = "Slice filtering can only be applied when in area drawing mode."      
                                        if  self.ROIDictionary.isROIinAreaMode () :
                                            msg = "Slice filtering can only be applied on a ROI defined in the current dataset."    
                                            if self.ROIDictionary.isROIDefined (itemName) : 
                                                msg = "Slice filtering requires the selection of one ROI with data defined for the currently selected slice." 
                                                if (itemName != "None") :
                                                    obj = self.ROIDictionary.getROIObject(itemName)
                                                    msg = "Slice filtering requires the selection of one ROI with data defined." 
                                                    if (obj is not None) :
                                                        setEnabled = True
                                                        break
             if (setEnabled == False and ShowErrorMsg == True):
                  MessageBoxUtil.showMessage ("Slice Filtering", msg)
             return setEnabled
         except:
             MessageBoxUtil.showMessage ("Filtering Slice", "An exception occured trying to determine if filtering could be applied to the slice.")
             print ("ERROR: An exception occured trying to determine if filtering could be applied to the slice at last step: " + msg)
             return False
    
    def setShowFilters (self):      
        canFilterSlice = self._canFilterSlice ()
        self.ui.actionFill_ROI_holes.setEnabled (canFilterSlice)
        self.ui.actionErode_ROI.setEnabled (canFilterSlice)
        self.ui.actionDilate_ROI.setEnabled (canFilterSlice)
        self.ui.actionKeep_largest_3D_Island.setEnabled (self._canFilterSlice (EnableMultiROISelection = True))
        try:
            penSettings = self.getPenSettings ()
            thSet = penSettings.isThresholded ()
            if thSet :
                self.ui.actionErode_ROI.setText ("Erode ROI (Threshold enabled)")
                self.ui.actionFill_ROI_holes.setText ("Fill ROI holes (Threshold enabled)")
                self.ui.actionDilate_ROI.setText ("Dilate ROI (Threshold enabled)")
            else:
                self.ui.actionErode_ROI.setText ("Erode ROI")
                self.ui.actionFill_ROI_holes.setText ("Fill ROI holes")
                self.ui.actionDilate_ROI.setText ("Dilate ROI")
        except:
            self.ui.actionErode_ROI.setText ("Erode ROI")
            self.ui.actionFill_ROI_holes.setText ("Fill ROI holes")
            self.ui.actionDilate_ROI.setText ("Dilate ROI")
    
    def _tryFilterSlice (self, filterName) :     
        if self.showFlattenDeepGrowDlg ("One-click generated segmentations must be flattened before a slice can be filtered. Do you wish to flatten the segmentation?") :
            if (self._canFilterSlice (ShowErrorMsg = True)) :                    
                itemName =self.ROIDefs.getSelectedROI ()[0]  # get currently selected ROI                                             
                if (itemName != "None") :
                    obj = self.ROIDictionary.getROIObject(itemName)
                    if (obj is not None) :
                        if self.ui.YZSliceView.isSelected () :
                           selection = self.ui.YZSliceVerticalSelector.getSelection ()
                           firstSlice = selection[0]
                           lastSlice = selection[-1]
                           self.filterSlice (filterName, firstSlice, lastSlice, self.ui.YZSliceView,  obj) 
                        elif self.ui.XZSliceView.isSelected () :
                           selection = self.ui.XZSliceVerticalSelector.getSelection ()
                           firstSlice = selection[0]
                           lastSlice = selection[-1]
                           self.filterSlice (filterName, firstSlice, lastSlice, self.ui.XZSliceView,  obj) 
                        else:
                            selection = self.ui.XYSliceSelectorWidget.getSelection ()
                            firstSlice = selection[0]
                            lastSlice = selection[-1]
                            self.filterSlice (filterName, firstSlice, lastSlice, self.ui.XYSliceView,  obj)  
                                        
    def fillROIHoleInSelectedSlice (self) :
        self._tryFilterSlice ("binary_fill_holes")
    
    def erodeROIInSelectedSlice (self) :
        self._tryFilterSlice ("binary_erosion")
        
    def dilationROIInSelectedSlice (self) :
        self._tryFilterSlice ("binary_dilation")
       
    def _clearTemporaryMask (self, clearObjList, penSettings, sliceView, drawMask, NoCopy = False, ClipBox = None, InvertAddOverride = None) :
        maskChanged = False
        SingleROIPerVoxelMode = self.ROIDefs.hasSingleROIPerVoxel ()
        for clearObjTuple in clearObjList :
            clearObj = clearObjTuple[0]
            clearObjTempMask = clearObj.getTemporaryObjectSliceMask (sliceView, ClearBoundingBox = False, NoCopy = NoCopy,ClipOverlayingROI = SingleROIPerVoxelMode, NiftiVolumeDim = self.NiftiVolume.getSliceDimensions())
            if (clearObjTempMask is not None) :                        
                if InvertAddOverride is not None :
                    invertAdd = InvertAddOverride
                else:
                    invertAdd = penSettings.isAddPen ()                
                if RCC_ContourWindow.applyDrawMask (drawMask, clearObjTempMask, penSettings, invertAdd = invertAdd, ClipBox = ClipBox) :                    
                    clearObj.setTemporaryObjectPixMapMask (sliceView, clearObjTempMask, NoCopy = NoCopy, ClipBox = ClipBox)                            
                    maskChanged = True
        return maskChanged
    
    @staticmethod
    def _ZoomWindow (line, sliceview, ZoomInit, eventtype, ClickEventHandled) :        
        X1 = line[-1][0]
        Y1 = line[-1][1]
        sliceview.clearTemporaryDrawLine () # clears current mouse memory        
        if (ZoomInit is None):      # if first call, saves inital position                    
            if (eventtype != "EndMouseMove") : #if event is ending then zoom in                                               
                ZoomInit = (X1, Y1)                               
        else:                               # otherwise
           if (eventtype == "EndMouseMove") : #if event is ending then zoom in                                               
               if (ClickEventHandled) :                   
                   ZoomInit = None                    
               else:
                   X2,Y2 = ZoomInit                  
                   ZoomInit = None                    
                   if (abs (X1 - X2) <= 5 or abs (Y1 - Y2) <= 5) :
                       ZC = sliceview.getSliceNumber ()
                       sliceview.zoomInClick (X2,Y2, ZC)   #zoom in based on selection                                    
                   else:
                       sliceview.zoomIn (min (X1, X2), min (Y1,Y2), max (X1,X2), max (Y1,Y2))   #zoom in based on selection                                    
           else:                           # if event is not ending construct a temorary line to illustrate the current selection 
               X2,Y2 = ZoomInit
               line = [(X1, Y1),(X2, Y1), (X2,Y2), (X1,Y2), (X1,Y1)]                 
               sliceview.setTemporaryDrawLine (line, QtGui.QColor (255, 255,255))
           sliceview.update () # repaint
        return ZoomInit
         
    def _altScrollZoomWindow (self, imageX, imageY, sliceview, eventtype) :
        if self.getKeyState (QtCore.Qt.Key_Alt) and sliceview.isZoomed () and self._LastMouseMoveROIState is not None :                  
              self._XYZoomInit = None
              pMX, pMY, _ = self._LastMouseMoveROIState
              sliceview.scroll (pMX - imageX ,pMY - imageY)                                             
              sX, sY = sliceview.getLastSystemMouseCoordinate  ()
              s_X, s_Y = sliceview.transformPointFromScreenToImage  (sX, sY)               
              self._LastMouseMoveROIState = (s_X, s_Y, True)
              #cx,cy,cz = sliceview.convertMouseMoveImageCoordiantesToXYZImageCoordinates (s_X, s_Y)
              sliceview.clearTemporaryDrawLine ()  
              if (eventtype != "EndMouseMove") :  
                  return True        
              if self.ROIDictionary.isROIinPerimeterMode () :  
                  return True
        else:
              self._LastMouseMoveROIState = (imageX, imageY, self._LastMouseMoveROIState[2])
              if (self._LastMouseMoveROIState[2] and self.ROIDictionary.isROIinPerimeterMode ()) :
                  sliceview.clearTemporaryDrawLine ()                 
                  return True
        return False
     
    @staticmethod      
    def _copyObjectIfNecessary (objectCopyTrackingDictionary, obj):
        roiIDNum = obj.getROIDefIDNumber ()
        if roiIDNum not in objectCopyTrackingDictionary :
            obj = obj.copy (MoveSliceMaskVolumeCache = True)      
            objectCopyTrackingDictionary[roiIDNum] = obj    
            return obj
        else:
            return objectCopyTrackingDictionary[roiIDNum]
    
    def _getROINameToSetRemovedIslandsTo (self) :
        try :
            removeROIName = self.visSettings.getSetRemovedIslandToROI ()
            if self.ROIDefs.hasROI (removeROIName) :
                return removeROIName
            return "None"
        except:            
            return "None"
    
    @staticmethod    
    def _getPerpendicularSliceViewReductionAxis (sliceView):
        axis = sliceView.getSliceAxis ()
        if axis == "X" :
            return (1,2)
        elif axis == "Y" :
            return (0,2)
        else:
            return (0,1)
        
    def getSliceViewSelection (self, sliceView) :
        axis = sliceView.getSliceAxis ()
        if axis == "X" :
            return self.ui.YZSliceSelectorWidget.getSelection ()
        elif axis == "Y" :
            return self.ui.XZSliceSelectorWidget.getSelection ()
        else:
            return self.ui.XYSliceSelectorWidget.getSelection () 
                          
                                   
    @staticmethod    
    def _getSliceFromSliceViewVolume (sliceView, ObjectsToRemoveIslandsFrom3D, Index = None)  :
        axis = sliceView.getSliceAxis ()
        if Index is None :
            Index = sliceView.getSliceNumber ()
        if axis == "X" :
            try:
                return ObjectsToRemoveIslandsFrom3D[Index, : ,:]
            except:
                zeroShape = (ObjectsToRemoveIslandsFrom3D.shape[1], ObjectsToRemoveIslandsFrom3D.shape[2])
        elif axis == "Y" :
            try:
                return ObjectsToRemoveIslandsFrom3D[:, Index ,:]
            except:
                zeroShape = (ObjectsToRemoveIslandsFrom3D.shape[0], ObjectsToRemoveIslandsFrom3D.shape[2])
        else:
            try :
                return ObjectsToRemoveIslandsFrom3D[:, : , Index]
            except:
                zeroShape = (ObjectsToRemoveIslandsFrom3D.shape[0], ObjectsToRemoveIslandsFrom3D.shape[1])
        return np.zeros (zeroShape, np.int8)
        
    def _setIslandObj (self, islandObj, sliceIndex, MaskChange, sliceView, objectCopyTrackingDictionary, objSliceChangeDictionary) :
        if islandObj is None :
            IslandROIName = self._getROINameToSetRemovedIslandsTo ()            
            if IslandROIName is not None and IslandROIName != "None"  :
               if self.ROIDefs.hasROI (IslandROIName) :
                   if self.ROIDictionary.isROIDefined (IslandROIName) :
                       islandObj = self.ROIDictionary.getROIObject (IslandROIName)
                       islandObj = RCC_ContourWindow._copyObjectIfNecessary (objectCopyTrackingDictionary, islandObj)
                   else:
                       islandObj = MultiROIAreaObject ()     
                       islandObj.setROIIDNumber (self.ROIDefs.getROIIDNumber (IslandROIName))                                   
        if islandObj is not None :
            IDList = islandObj.getContourIDManger ().getAllocatedIDList ()
            if len (IDList) > 0 :
               islandObj_contourID = IDList[0]
            else:
               islandObj_contourID = islandObj.getContourIDManger ().allocateID()                
            islandMask = islandObj.getSliceViewObjectMask (sliceView, SliceNumber = sliceIndex, ClipOverlayingROI = self.ROIDefs.hasSingleROIPerVoxel())                                                                                                                                                   
            newIslandMask = np.copy (islandMask)
            newIslandMask[MaskChange < 0] =  1
            change = newIslandMask - islandMask
            if np.any (change) :
                AreaOfIslandAdded = self._getTemporaryObjectBoundingBoxChange (sliceView, islandObj, change, SliceNumber =sliceIndex)                   
                islandObj.setSliceViewObjectMask (sliceView, islandObj_contourID, newIslandMask, sliceIndex, change, objSliceChangeDictionary)                                                                                                       
                islandObj.adjustBoundBoxToFitSlice(sliceIndex, self.NiftiVolume.getSliceDimensions (), NewBoxArea =  AreaOfIslandAdded)
        return islandObj
    
    def KeepOnlyLargestObjectFilter (self, temp = False, ObjList = None, SliceViewObjectsChanged = None, CopyObjectsForUndoRedo = True, IslandROIName = None, ProgressDialog = None) :
        
        def _tryGetIslandRemoval3dFilter (objectFilled) :
            volumeObj = objectFilled.getMaskedVolume (self.NiftiVolume.getSliceDimensions (), ClipOverlayedObjects = True, UseVolumeCache = True, SharedROIMask =  self.ROIDictionary.getSharedVolumeCacheMem ())    
            structure = np.ones ((3,3,3), dtype = np.uint8)
            ObjectsToRemoveIslandsFrom3D, features = ndimage.measurements.label (volumeObj, structure)
            if features <= 0 :
                return False, None
            maxObj = 1
            maxObjCount = np.sum (ObjectsToRemoveIslandsFrom3D == 1)
            for index in range (2, features+1) :
                self._ProjectDataset.getApp ().processEvents ()
                count = np.sum (ObjectsToRemoveIslandsFrom3D == index)
                if count > maxObjCount :
                     maxObj = index
                     maxObjCount = count
            IslandsToRemoveIn3D = ObjectsToRemoveIslandsFrom3D != maxObj
            IslandsToRemoveIn3D[ObjectsToRemoveIslandsFrom3D == 0] = False
            del ObjectsToRemoveIslandsFrom3D                                    
            if not np.any (IslandsToRemoveIn3D) :
                return False, None
            return True, IslandsToRemoveIn3D
        
        def _processIslandRemoval3dFilter (IslandROIName, objectFilled, IslandsToRemoveIn3D, sliceView, objectCopyTrackingDictionary, objSliceChangeDictionary, roiChangeDictionary, CopyObjectsForUndoRedo = True) :                  
           objectRemovedName = objectFilled.getName(self.ROIDefs)
           if objectRemovedName ==IslandROIName :
               return 
           islandObj = None
           if IslandROIName is not None and IslandROIName != "None"  :
              if self.ROIDefs.hasROI (IslandROIName) :
                   if self.ROIDictionary.isROIDefined (IslandROIName) :
                       islandObj = self.ROIDictionary.getROIObject (IslandROIName)
                       if CopyObjectsForUndoRedo  :
                           islandObj = RCC_ContourWindow._copyObjectIfNecessary (objectCopyTrackingDictionary, islandObj)
                   else:
                       islandObj = MultiROIAreaObject ()     
                       islandObj.setROIIDNumber (self.ROIDefs.getROIIDNumber (IslandROIName))                                   
                                                     
                   IDList = islandObj.getContourIDManger ().getAllocatedIDList ()
                   if len (IDList) > 0 :
                       islandObj_contourID = IDList[0]
                   else:
                       islandObj_contourID = islandObj.getContourIDManger ().allocateID()                
                   islandObj.setSelectedContours ([islandObj_contourID])
                     
           sliceIndexToAdjust = np.any (IslandsToRemoveIn3D, axis = RCC_ContourWindow._getPerpendicularSliceViewReductionAxis (sliceView))
           sliceIndexToAdjust = sliceIndexToAdjust.nonzero ()[0]
           if len (sliceIndexToAdjust) > 0 :
              selectedSlicesToRemoveIslandFrom = self.getSliceViewSelection (sliceView)
              if len (selectedSlicesToRemoveIslandFrom) > 1 :
                  clippedSliceIndexList = []
                  for si in selectedSlicesToRemoveIslandFrom :
                      if si in sliceIndexToAdjust :
                          clippedSliceIndexList.append (si)
                  sliceIndexToAdjust = clippedSliceIndexList   
           if len (sliceIndexToAdjust) > 0 :    
              IDList = objectFilled.getContourIDManger ().getAllocatedIDList ()
              if len (IDList) > 0 :
                  Obj_contourID = IDList[0]
              else:
                  Obj_contourID = objectFilled.getContourIDManger ().allocateID()   
              objectRemovedName = objectFilled.getName(self.ROIDefs)
              if CopyObjectsForUndoRedo  :
                  objectFilled = RCC_ContourWindow._copyObjectIfNecessary (objectCopyTrackingDictionary, objectFilled)
              for islandRemovalIndex in sliceIndexToAdjust :
                  self._ProjectDataset.getApp ().processEvents ()
                  islandRemovalSliceMask = RCC_ContourWindow._getSliceFromSliceViewVolume (sliceView, IslandsToRemoveIn3D, Index = islandRemovalIndex) 
                  oldSliceMask = objectFilled.getSliceViewObjectMask (sliceView, SliceNumber = islandRemovalIndex, ClipOverlayingROI = self.ROIDefs.hasSingleROIPerVoxel())                                                                                                                                                   
                  newSliceMask = np.copy (oldSliceMask)
                  newSliceMask[islandRemovalSliceMask] = 0
                  change = -islandRemovalSliceMask.astype (np.int8)                
                  objectFilled.setSliceViewObjectMask (sliceView, Obj_contourID, newSliceMask, islandRemovalIndex, change, objSliceChangeDictionary)                                     
                  roiChangeDictionary[objectFilled.getROIDefIDNumber ()] = (objectRemovedName, objectFilled)
                  if islandObj is not None :
                      islandObj = self._setIslandObj (islandObj, islandRemovalIndex, change, sliceView, objectCopyTrackingDictionary, objSliceChangeDictionary)                                          
                      if islandObj is not None :
                          roiChangeDictionary[islandObj.getROIDefIDNumber ()] = (IslandROIName, islandObj)                               
           return
                
        if self.ROIDictionary is None or self.ROIDictionary.isReadOnly () :
            return            
                
        listOfObjectsToFilter =[]        
        if ObjList is not None :
            selROIName = ObjList 
            progdialog = ProgressDialog
            CloseProgressDialog = False
        else:
            selROIName = self.ROIDefs.getSelectedROI ()
            progdialog = QProgressDialog("Removing Small 3D Islands", "Cancel", int (0), int (0),self)
            progdialog.setMinimumDuration (0)
            progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)                
            progdialog.setWindowTitle ("Removing Small 3D Islands")
            progdialog.setLabelText ("Initializing")
            progdialog.setWindowModality(QtCore.Qt.ApplicationModal)                                
            CloseProgressDialog = True
        try :
            for roiName in  selROIName :            
                if roiName is not None and roiName != "None" and self.ROIDefs.hasROI (roiName) and self.ROIDefs.isROIArea (roiName) :
                    if self.ROIDictionary.isROIDefined (roiName) and not self.ROIDefs.isROILocked (roiName) :
                        roiObj = self.ROIDictionary.getROIObject (roiName)
                        if roiObj is not None :
                            listOfObjectsToFilter.append (roiObj)
            if len (listOfObjectsToFilter) <= 0 :
                return
            if progdialog is not None :
                progdialog.setRange (0, len (listOfObjectsToFilter))
                progdialog.show ()                
            sliceView = self.ui.XYSliceView
            objectCopyTrackingDictionary = {}
                    
            if SliceViewObjectsChanged is None : 
                objSliceChangeDictionary = {}    
                objSliceChangeDictionaryCreated = True
            else:
                objSliceChangeDictionaryCreated = False
            roiChangeDictionary = {}
            if IslandROIName is None :
                IslandROIName = self._getROINameToSetRemovedIslandsTo ()    
            for index, obj in enumerate (listOfObjectsToFilter) :
                ROIName = obj.getName(self.ROIDefs)
                if progdialog is not None :
                    progdialog.setValue (index)
                    progdialog.setLabelText ("Testing %s for small 3d islands." % ROIName)
                    if progdialog.wasCanceled () :
                        break
                self._ProjectDataset.getApp ().processEvents ()
                requiresFilter, islandToRemove = _tryGetIslandRemoval3dFilter (obj)
                if requiresFilter :                 
                    self._ProjectDataset.getApp ().processEvents ()
                    if progdialog is not None :                        
                        progdialog.setLabelText ("Removing small 3D Islands from %s." % ROIName)
                        if progdialog.wasCanceled () :
                            break
                    _processIslandRemoval3dFilter (IslandROIName, obj, islandToRemove, sliceView, objectCopyTrackingDictionary, objSliceChangeDictionary, roiChangeDictionary, CopyObjectsForUndoRedo)
            if objSliceChangeDictionaryCreated :
                for sliceobj in objSliceChangeDictionary :
                   sliceobj.endCrossSliceChange ()           
            if CopyObjectsForUndoRedo and len (roiChangeDictionary) > 0:               
               self.ROIDictionary.addROIList (list (roiChangeDictionary.values ()))
               self.getPenSettings().UpdateSelectedROIPaintSettings ()
        finally:
            try:
                if CloseProgressDialog and progdialog is not None :
                    progdialog.close ()
            except:
                pass
           
    
    
    
    
    def _areaModeContour (self, cx, cy, cz, sliceView, line, eventtype, QTButtonPressed) :
       itemName =self.ROIDefs.getSelectedROI ()[0]  # get currently selected ROI           
       obj, isFirstRequest = self.getTemporaryPaintObj (itemName, sliceView)       
       penSettings = self.getPenSettings ()                    
       sliceNumber = sliceView.getSliceNumber ()   
       sliceShape = sliceView.getSliceShape()      
       sliceAxis = sliceView.getSliceAxis ()
       
       if self.getTemporaryPaintObjSliceNumber () != sliceNumber or sliceAxis != self.getTemporaryPaintObjAxis () :
           self.setDisableTemporaryPaintObjCoordinateChangeAdd ()
           sliceView.triggerSliceChangeMouseMoveUpdate (self.getTemporaryPaintObjSliceNumber ())
           self.setPenIndex (0)
           try :
               if self.getTemporaryPaintObjSliceNumber () != sliceNumber :
                   self.clearTemporaryPaintObj ()
                   print ("Slice Numbers do not match")
           except:
               pass                                        
       else:                    
           if (isFirstRequest) :
               gc.disable()
               self._drawMask = np.zeros (sliceShape, dtype = np.int8)                               
               self._adjacentSliceDrawLine = [[]]
               
           IDList = obj.getContourIDManger ().getAllocatedIDList ()
           if len (IDList) > 0 :
               contourID = IDList[0]
           else:
               contourID = obj.getContourIDManger ().allocateID()
           
           if sliceAxis == 'Z' and not obj.hasSlice (sliceNumber) :    
               newobj = obj.copy (MoveSliceMaskVolumeCache = True)
               obj.clearTemporaryObjectSliceMask()
               obj = newobj
               self.updateTemporaryPaintObj (obj)
               dims = sliceView.getNIfTIVolume().getSliceDimensions ()
               if not sliceView.areAxisProjectionControlsEnabledForAxis () :
                   newobj.setSliceROIAreaMask (contourID, sliceNumber , np.zeros (sliceShape, dtype = np.uint8), (dims[0], dims[1]), "Human", createSliceIfUndefined = True)   
               else:
                    newobj.setSliceDataForCrossSliceChange ((dims[0], dims[1]), contourID, sliceNumber, np.zeros ((dims[0], dims[1]), dtype = np.uint8), X = None, Y=None, ContourType = "Human", createSliceIfUndefined = True)
           objLst = [obj]
           self.ui.XYSliceView.setTempROIObjectList (objLst) 
           self.ui.XZSliceView.setTempROIObjectList (objLst)
           self.ui.YZSliceView.setTempROIObjectList (objLst)            
           temporaryObjectMask = obj.getTemporaryObjectSliceMask (sliceView, NoCopy = True, ClipOverlayingROI = self.ROIDefs.hasSingleROIPerVoxel (), NiftiVolumeDim = self.NiftiVolume.getSliceDimensions())
                       
                  
           penSettings = self.getPenSettings ()
           penIndex = self.getPenIndex()
           penmask = penSettings.getPenMask (self.NiftiVolume, sliceView) 
               
           clearObjList = []
           if  self.getKeyState (QtCore.Qt.Key_Alt)  :
                self.setPenIndex (len (line))
                self._adjacentSliceDrawLine.append ([])
           else:                                           
               if (penSettings.getPenTool () == "Draw") :
                   self._adjacentSliceDrawLine[-1]  += line[penIndex:]
                   PointDrawList = self.computeLineDrawPoints (line, penIndex) 
                     
                   if penSettings.isThresholded () :
                       imageData, niftisliceDataClipMask = self.NiftiVolume.getImageData (sliceNumber, Axis = sliceAxis, Coordinate = self.SelectedCoordinate, AxisProjectionControls = sliceView.getAxisProjectionControls (), ReturnClipMask = True)
                   elif  self.ui.XYSliceView.areAxisProjectionControlsEnabledForAxis () :
                       imageData = None
                       _, niftisliceDataClipMask = self.NiftiVolume.getImageData (sliceNumber, Axis = sliceAxis, Coordinate = self.SelectedCoordinate, AxisProjectionControls = sliceView.getAxisProjectionControls (), ReturnClipMask = True)
                   else:
                       imageData = None
                       niftisliceDataClipMask = None
                   ClipBox = None                                                       
                   if (QTButtonPressed == QtCore.Qt.LeftButton and not self.getKeyState (QtCore.Qt.Key_Control)) or penSettings.isErasePen () :                            
                       ClipBox = self.drawPointList ( self._drawMask, PointDrawList, imageData,penSettings, penmask, ClipMask = niftisliceDataClipMask)                              
                       isMaskChanged =  RCC_ContourWindow.applyDrawMask (self._drawMask, temporaryObjectMask, penSettings, ClipBox = ClipBox)                                           
                   elif ((QTButtonPressed == QtCore.Qt.LeftButton and self.getKeyState (QtCore.Qt.Key_Control)) or QTButtonPressed == QtCore.Qt.RightButton) and penSettings.isAddPen () :
                       penSettings.setErasePen (UpdateSettings = False)
                       ClipBox = self.drawPointList ( self._drawMask, PointDrawList, imageData,penSettings, penmask, ClipMask = niftisliceDataClipMask)                              
                       isMaskChanged = RCC_ContourWindow.applyDrawMask (self._drawMask, temporaryObjectMask, penSettings, ClipBox = ClipBox)
                       penSettings.setAddPen (UpdateSettings = False)
                   else:
                       isMaskChanged = False
                   
                   clearObjectMaskChanged = False
                   if (penSettings.isErasePen() and self.ROIDefs.hasSingleROIPerVoxel()) :
                       clearObjList = self.getClearPaintObjectList ()                                                                  
                       if self._clearTemporaryMask (clearObjList, penSettings, sliceView, self._drawMask, NoCopy = True, ClipBox = ClipBox) :
                           clearObjectMaskChanged = True
                                                                             
                   if isMaskChanged :                                                   
                       obj.setTemporaryObjectPixMapMask (sliceView, temporaryObjectMask, NoCopy = True, ClipBox = ClipBox)
                       
                   if self.visSettings.moveToCoordinatDuringPaint() :
                       if sliceView != self.ui.XZSliceView :                   
                           CordChange1 = self.ui.XZSliceView.updateSliceSelection (cx, "X", DisableScrollToZoomAndUpdateSlice = True)                                                                     
                           UpdateSliceAxis = self.ui.XZSliceView.updateSliceSelection (cy, "Y", DisableScrollToZoomAndUpdateSlice = True)                                                                     
                           CordChange2 = self.ui.XZSliceView.updateSliceSelection (cz, "Z", DisableScrollToZoomAndUpdateSlice = True)                           
                           if CordChange1 or CordChange2 or UpdateSliceAxis :
                               self.ui.XZSliceView.CoordianteChangedUpdateSliceAxis (UpdateSliceAxis)
                       if sliceView != self.ui.XYSliceView :                   
                           CordChange1 = self.ui.XYSliceView.updateSliceSelection (cx, "X", DisableScrollToZoomAndUpdateSlice = True)                                                                     
                           CordChange2 = self.ui.XYSliceView.updateSliceSelection (cy, "Y", DisableScrollToZoomAndUpdateSlice = True)                                                                     
                           UpdateSliceAxis = self.ui.XYSliceView.updateSliceSelection (cz, "Z", DisableScrollToZoomAndUpdateSlice = True)
                           if CordChange1 or CordChange2 or UpdateSliceAxis :
                               self.ui.XYSliceView.CoordianteChangedUpdateSliceAxis (UpdateSliceAxis)
                       if sliceView != self.ui.YZSliceView :                   
                           UpdateSliceAxis = self.ui.YZSliceView.updateSliceSelection (cx, "X", DisableScrollToZoomAndUpdateSlice = True)                                                                     
                           CordChange1 = self.ui.YZSliceView.updateSliceSelection (cy, "Y", DisableScrollToZoomAndUpdateSlice = True)                                                                     
                           CordChange2 = self.ui.YZSliceView.updateSliceSelection (cz, "Z", DisableScrollToZoomAndUpdateSlice = True)
                           if CordChange1 or CordChange2 or UpdateSliceAxis :
                               self.ui.YZSliceView.CoordianteChangedUpdateSliceAxis (UpdateSliceAxis)
                   #cord = Coordinate ()
                   #cord.setCoordinate ([cx, cy, cz])
                   #self.SelectedCoordinatChanged (cord)                   
                   if isMaskChanged or clearObjectMaskChanged  : 
                       if ClipBox is None :
                           sliceView.update ()
                       else:                          
                           xMin, xMax, yMin, yMax = ClipBox                              
                           ul, ur = sliceView.transformPointFromImageToScreen (xMin, yMin)
                           ll, lr = sliceView.transformPointFromImageToScreen (xMax, yMax)
                           ul, ll = min (ul,ll), max (ul,ll)
                           ur, lr = min (ur,lr), max (ur,lr)
                           sliceView.update (ul,ur, ll,lr)                                                                                                     
                       
               if (eventtype == "EndMouseMove") :
                       if penSettings.getPenTool () == "Fill" :                                                    
                           if ((self.getKeyState (QtCore.Qt.Key_Control) and QTButtonPressed == QtCore.Qt.LeftButton)  or QTButtonPressed == QtCore.Qt.RightButton) :
                                penSettings.setErasePen (UpdateSettings = False)
                                objectFilled, origionalObjMask = self.fillMask (sliceView, line[-1], self._drawMask, penSettings, obj)
                                if objectFilled is not None :
                                    if (self.ROIDefs.hasSingleROIPerVoxel()) :
                                        clearObjList = self.getClearPaintObjectList ()        
                                    elif objectFilled is not obj :                                                                                                                                                      
                                        clearObjList = [(objectFilled, origionalObjMask)]
                                penSettings.setAddPen (UpdateSettings = False)                                            
                                InvertAddOverride = False
                                if (obj == objectFilled) :
                                    RCC_ContourWindow.applyDrawMask (self._drawMask, temporaryObjectMask, penSettings, ReturnAnyChange = False)
                           else:              
                                InvertAddOverride = None
                                objectFilled, origionalObjMask = self.fillMask (sliceView, line[-1], self._drawMask, penSettings, obj)  
                                if objectFilled is not None :
                                    if (self.ROIDefs.hasSingleROIPerVoxel()) :
                                        clearObjList = self.getClearPaintObjectList ()        
                                    elif objectFilled is not obj :                                                                                                                                                      
                                        clearObjList = [(objectFilled, origionalObjMask)]
                                if (obj == objectFilled or penSettings.isAddPen ()) :
                                    RCC_ContourWindow.applyDrawMask (self._drawMask, temporaryObjectMask, penSettings, ReturnAnyChange = False)
                           if len (clearObjList) > 0 :
                               PointDrawList = []
                               self._clearTemporaryMask (clearObjList, penSettings, sliceView, self._drawMask, InvertAddOverride = InvertAddOverride)
                       elif penSettings.getPenTool () == "SelectIsland" :                          
                           IslandsToRemoveIn3D = None
                           if ((self.getKeyState (QtCore.Qt.Key_Control) and QTButtonPressed == QtCore.Qt.LeftButton)  or QTButtonPressed == QtCore.Qt.RightButton) :
                                penSettings.setErasePen (UpdateSettings = False)
                                objectFilled, origionalObjMask = self.selectIsland (sliceView, line[-1], self._drawMask, penSettings, Skip2DIslandTest = self.visSettings.shouldPerformIslandRemovalIn3D ())                                
                                penSettings.setAddPen (UpdateSettings = False)                                            
                           else:
                               objectFilled, origionalObjMask = self.selectIsland (sliceView, line[-1], self._drawMask, penSettings, Skip2DIslandTest = self.visSettings.shouldPerformIslandRemovalIn3D ())  
                            
                           if objectFilled is not None and self._getROINameToSetRemovedIslandsTo () == objectFilled.getName(self.ROIDefs) :
                                MessageBoxUtil.showMessage ("Island Removal Tool", "Cannot remove selected island. Island removal set to selected object. Change island removal tool settings to remove selected objects islands.")
                                clearObjList = []
                           else:                                
                                if objectFilled is not None and self.visSettings.shouldPerformIslandRemovalIn3D () :
                                    volumeObj = objectFilled.getMaskedVolume (self.NiftiVolume.getSliceDimensions (), ClipOverlayedObjects = True, UseVolumeCache = True, SharedROIMask =  self.ROIDictionary.getSharedVolumeCacheMem ())    
                                    structure = np.ones ((3,3,3), dtype = np.uint8)
                                                                        
                                    xPos, yPos = line[-1]
                                    ObjectsToRemoveIslandsFrom3D, features = ndimage.measurements.label (volumeObj, structure)
                                    selectedSlice = RCC_ContourWindow._getSliceFromSliceViewVolume (sliceView, ObjectsToRemoveIslandsFrom3D)                                    
                                    selectedObjIndex = selectedSlice[xPos, yPos]
                                    if ((self.getKeyState (QtCore.Qt.Key_Control) and QTButtonPressed == QtCore.Qt.LeftButton)  or QTButtonPressed == QtCore.Qt.RightButton) :
                                        IslandsToRemoveIn3D = ObjectsToRemoveIslandsFrom3D == selectedObjIndex
                                    else:
                                        IslandsToRemoveIn3D = ObjectsToRemoveIslandsFrom3D != selectedObjIndex
                                        IslandsToRemoveIn3D[ObjectsToRemoveIslandsFrom3D == 0] = False
                                    del ObjectsToRemoveIslandsFrom3D                                    
                                    
                                    if not np.any (IslandsToRemoveIn3D) :
                                        objectFilled = None
                                        self._drawMask[...] = 0
                                    else:
                                        selectedSlice = RCC_ContourWindow._getSliceFromSliceViewVolume (sliceView, IslandsToRemoveIn3D)
                                        self._drawMask = -1 * np.copy (selectedSlice)
    
                                if objectFilled is not None and objectFilled is not obj :                                                                                                                                                      
                                    clearObjList = [(objectFilled, origionalObjMask)]                                    
                                if (obj == objectFilled) :
                                    RCC_ContourWindow.applyDrawMask (self._drawMask, temporaryObjectMask, penSettings, ReturnAnyChange = False)                           
                           if len (clearObjList) > 0 :
                               PointDrawList = []
                               self._clearTemporaryMask (clearObjList, penSettings, sliceView, self._drawMask, InvertAddOverride = False)                                                             
                           
           if(eventtype == "EndMouseMove") :    
               gc.enable()
               isCoordinateChangeDisabled = self.isTemporaryPaintObjCoordinateChangeOnAddDisabled()
               if (penSettings.getPenTool () == "Draw" and penSettings.isAddPen () and self.ROIDefs.hasSingleROIPerVoxel()) :
                   clearObjList = self.getClearPaintObjectList ()                                           
                   self._clearTemporaryMask (clearObjList, penSettings, sliceView, self._drawMask)                                               
                              
               ChangeMap = temporaryObjectMask.astype (np.int8) - self.getTemporaryPaintObjMaskData().astype (np.int8) 
               maskChanged = np.any (ChangeMap) 
              
               if (maskChanged):                   
                   AreaOfImageAdded = self._getTemporaryObjectBoundingBoxChange (sliceView, obj, ChangeMap, SliceNumber = sliceView.getSliceNumber())                   
               else:
                   AreaOfImageAdded = None
                   
               obj.clearTemporaryObjectSliceMask ()
               self.setPenIndex (0) 
               self.clearTemporaryPaintObj()          
               objectCopyTrackingDictionary = {}
               objSliceChangeDictionary = set ()
               roiChangeDictionary = {}        
               
               if penSettings.getPenTool () == "SelectIsland" :                   
                   IslandROIName = self._getROINameToSetRemovedIslandsTo ()
                   islandObj = None
                   if IslandROIName is not None and IslandROIName != "None"  :
                       if self.ROIDefs.hasROI (IslandROIName) :
                           IslandsRemoved = self._drawMask == -1
                           if np.any (IslandsRemoved) :
                               if self.ROIDictionary.isROIDefined (IslandROIName) :
                                   islandObj = self.ROIDictionary.getROIObject (IslandROIName)
                                   islandObj = RCC_ContourWindow._copyObjectIfNecessary (objectCopyTrackingDictionary, islandObj)
                               else:
                                   islandObj = MultiROIAreaObject ()    
                                   islandObj.setROIIDNumber (self.ROIDefs.getROIIDNumber (IslandROIName))                                   
                               islandObj_temporaryObjectMask = islandObj.getSliceViewObjectMask (sliceView, SliceNumber = sliceView.getSliceNumber(), ClipOverlayingROI = self.ROIDefs.hasSingleROIPerVoxel())                                                               
                               newIslandMask = np.copy (islandObj_temporaryObjectMask)
                               newIslandMask[IslandsRemoved] = 1
                               IslandChangeMask = newIslandMask - islandObj_temporaryObjectMask
                               if np.any (IslandChangeMask) :                                   
                                   IDList = islandObj.getContourIDManger ().getAllocatedIDList ()
                                   if len (IDList) > 0 :
                                       islandObj_contourID = IDList[0]
                                   else:
                                       islandObj_contourID = islandObj.getContourIDManger ().allocateID()                
                                       
                                   islandObj.setSliceViewObjectMask (sliceView, islandObj_contourID, newIslandMask, sliceView.getSliceNumber (), IslandChangeMask, objSliceChangeDictionary)                              
                                   islandObj.setSelectedContours ([islandObj_contourID])
                                   
                                   AreaOfIslandAdded = self._getTemporaryObjectBoundingBoxChange (sliceView, islandObj, IslandChangeMask, SliceNumber = sliceView.getSliceNumber())                   
                                   islandObj.adjustBoundBoxToFitSlice(sliceNumber, self.NiftiVolume.getSliceDimensions (), NewBoxArea =  AreaOfIslandAdded)
                                   roiChangeDictionary[islandObj.getROIDefIDNumber ()] = (IslandROIName, islandObj)
                                 
                   if IslandsToRemoveIn3D is not None :                                 
                      sliceIndexToAdjust = np.any (IslandsToRemoveIn3D, axis = RCC_ContourWindow._getPerpendicularSliceViewReductionAxis (sliceView))
                      sliceIndexToAdjust = sliceIndexToAdjust.nonzero ()[0]
                      if len (sliceIndexToAdjust) > 0 :
                          selectedSlicesToRemoveIslandFrom = self.getSliceViewSelection (sliceView)
                          if len (selectedSlicesToRemoveIslandFrom) > 1 :
                              clippedSliceIndexList = []
                              for si in selectedSlicesToRemoveIslandFrom :
                                  if si in sliceIndexToAdjust :
                                      clippedSliceIndexList.append (si)
                              sliceIndexToAdjust = clippedSliceIndexList   
                      if len (sliceIndexToAdjust) > 0 :
                          if objectFilled == obj :
                              maskChanged = True
                          sliceNum = sliceView.getSliceNumber ()      
                          IDList = objectFilled.getContourIDManger ().getAllocatedIDList ()
                          if len (IDList) > 0 :
                              Obj_contourID = IDList[0]
                          else:
                              Obj_contourID = objectFilled.getContourIDManger ().allocateID()   
                          objectRemovedName = objectFilled.getName(self.ROIDefs)
                          for islandRemovalIndex in  sliceIndexToAdjust :
                              if islandRemovalIndex != sliceNum :
                                  islandRemovalSliceMask = RCC_ContourWindow._getSliceFromSliceViewVolume (sliceView, IslandsToRemoveIn3D, Index = islandRemovalIndex) 
                                  oldSliceMask = objectFilled.getSliceViewObjectMask (sliceView, SliceNumber = islandRemovalIndex, ClipOverlayingROI = self.ROIDefs.hasSingleROIPerVoxel())                                                                                                                                                   
                                  newSliceMask = np.copy (oldSliceMask)
                                  newSliceMask[islandRemovalSliceMask] = 0
                                  change = -islandRemovalSliceMask.astype (np.int8)
                                
                                  objectFilled = RCC_ContourWindow._copyObjectIfNecessary (objectCopyTrackingDictionary, objectFilled)
                                  objectFilled.setSliceViewObjectMask (sliceView, Obj_contourID, newSliceMask, islandRemovalIndex, change, objSliceChangeDictionary)                                     
                                  roiChangeDictionary[objectFilled.getROIDefIDNumber ()] = (objectRemovedName, objectFilled)
                                  islandObj = self._setIslandObj (islandObj, islandRemovalIndex, change, sliceView, objectCopyTrackingDictionary, objSliceChangeDictionary)                                          
                                  if islandObj is not None :
                                      roiChangeDictionary[islandObj.getROIDefIDNumber ()] = (IslandROIName, islandObj)
                                      
                                
               if (maskChanged) :
                   obj = RCC_ContourWindow._copyObjectIfNecessary (objectCopyTrackingDictionary, obj)
                   if penSettings.getPenSliceThickness () > 1 and penSettings.getPenTool () == "Draw" :
                       thickness = penSettings.getPenSliceThickness ()                       
                       crossSliceCord = self.SelectedCoordinate.copy ()    
                       self.paintLineOnAdjacentSlice (sliceView, obj, crossSliceCord, penSettings, self._adjacentSliceDrawLine, (self.getKeyState (QtCore.Qt.Key_Control) and QTButtonPressed == QtCore.Qt.LeftButton) or (QTButtonPressed == QtCore.Qt.RightButton), sliceNumber, thickness, objectCopyTrackingDictionary, objSliceChangeDictionary)
                   
                   obj.setSliceViewObjectMask (sliceView, contourID, temporaryObjectMask, sliceView.getSliceNumber (), ChangeMap, objSliceChangeDictionary)
                   #obj.setSliceROIAreaMask (contourID, sliceNumber , temporaryObjectMask, sliceView.getSliceShape(), "Human")   
                   obj.setSelectedContours ([contourID])
                   if AreaOfImageAdded is not None :
                       obj.adjustBoundBoxToFitSlice(sliceNumber, self.NiftiVolume.getSliceDimensions (), NewBoxArea =  AreaOfImageAdded)
                   roiChangeDictionary[obj.getROIDefIDNumber ()] = (itemName, obj)
     
               elif penSettings.getPenSliceThickness () > 1 and penSettings.getPenTool () == "Draw" :
                       thickness = penSettings.getPenSliceThickness ()
                       crossSliceCord = self.SelectedCoordinate.copy ()    
                       delayedObjCopy = self.paintLineOnAdjacentSlice (sliceView, obj, crossSliceCord, penSettings, self._adjacentSliceDrawLine, (self.getKeyState (QtCore.Qt.Key_Control) and QTButtonPressed == QtCore.Qt.LeftButton) or (QTButtonPressed == QtCore.Qt.RightButton), sliceNumber, thickness, objectCopyTrackingDictionary, objSliceChangeDictionary)
                       if delayedObjCopy is not None and delayedObjCopy != obj :
                           obj = delayedObjCopy
                           roiChangeDictionary[obj.getROIDefIDNumber ()] = (itemName, obj)
                           
               for clearObjTuple in clearObjList:
                   clearObj, origionalClearObjMask = clearObjTuple                                               
                   clearObjTempMask = clearObj.getTemporaryObjectSliceMask (sliceView,  ClearBoundingBox = False,ClipOverlayingROI = self.ROIDefs.hasSingleROIPerVoxel (), NiftiVolumeDim = self.NiftiVolume.getSliceDimensions())
                   clearObj.clearTemporaryObjectSliceMask ()                                               
                   #if mask changed
                   if (clearObjTempMask is not None) :
                       ChangeMap = clearObjTempMask.astype(np.int8) - origionalClearObjMask.astype(np.int8)
                       if np.any (ChangeMap) :
                           tobj = RCC_ContourWindow._copyObjectIfNecessary (objectCopyTrackingDictionary, clearObj)                           
                           IDList = tobj.getContourIDManger ().getAllocatedIDList ()
                           if len (IDList) > 0 :
                               tcontourID = IDList[0]
                           else:
                               tcontourID = obj.getContourIDManger ().allocateID()                                                       
                           tobj.setSliceViewObjectMask (sliceView, tcontourID, clearObjTempMask, sliceView.getSliceNumber (), ChangeMap, objSliceChangeDictionary)
                           #tobj.setSliceROIAreaMask (tcontourID, sliceNumber , clearObjTempMask, sliceView.getSliceShape(), "Human")                                                      
                        
                           tname = tobj.getName (self.ROIDefs)
                           roiChangeDictionary[tobj.getROIDefIDNumber ()] = (tname, tobj)
               for sliceobj in objSliceChangeDictionary :
                   sliceobj.endCrossSliceChange ()
               if len (roiChangeDictionary) > 0:       
                   self.ROIDictionary.addROIList (list (roiChangeDictionary.values ()))
                   self.getPenSettings().UpdateSelectedROIPaintSettings ()
                               
               if not isCoordinateChangeDisabled and self.visSettings.moveToCoordinateAtPaintEnd () :
                   #Disable coordate selection at the end of mouse move for performance
                   cord = Coordinate ()
                   cord.setCoordinate ([cx, cy, cz])
                   self.SelectedCoordinatChanged (cord)
               
               for view in [self.ui.XYSliceView, self.ui.YZSliceView, self.ui.XZSliceView] :
                   view.setTempROIObjectList ([])
                   view.setROIObjectList (self.ROIDictionary)
                   view.update ()                                                                           
               sliceView.setSelected (True)
               
    # mouse move event from XY slice widget
    def MajorAxisMouseMoveEvent (self, line, eventtype, sliceview, QTButtonPressed, ClickEventHandled):    
       if self.ROIDictionary is None : 
            return                               
       if (len (line) > 0) :
            imageX = line[-1][0]
            imageY = line[-1][1]           
            cx,cy,cz = sliceview.convertMouseMoveImageCoordiantesToXYZImageCoordinates (imageX, imageY)
            imgdata = self.NiftiVolume.getImageData ()
            if (cx >= 0 and cx < imgdata.shape[0] and cy >= 0 and cy < imgdata.shape[1] and cz >= 0 and cz < imgdata.shape[2]) :
                self.ui.ThresholdLbl.setText ("Value: %d" % (imgdata[cx, cy, cz]))                
            else:
                self.ui.ThresholdLbl.setText ("")                
       if (eventtype == "MouseMoveButtonUp") :
           return              
       if self.isRangeSelectionCallBackSet () :   
           return
       
       if (self.isSelectionUIState ()) :           
           if self._altScrollZoomWindow (imageX, imageY, sliceview, eventtype) :
               return               
                    
           # clear mouse move list and update UI. 
           if (self._SelectionMoveROIState is None and eventtype != "EndMouseMove") :
               self._ROIDictionaryChangeEvent = True
               self.NiftiAxisSelection (cx, cy, cz, sliceview.getSliceAxis (), True, EnableROISelection = False) 
               sliceview.setSelected (True)                     
               self._ROIDictionaryChangeEvent = False
               self._SelectionMoveROIState = None            
           elif (self._SelectionMoveROIState is not None ) :
               cx1, cy1, cz1, selectedObjName, selectedContourList, secondPass, xMouseOffset, yMouseOffset, zMouseOffset, moveMemory = self._SelectionMoveROIState
               cx += xMouseOffset
               cy += yMouseOffset
               cz += zMouseOffset
               dx = cx - cx1
               dy = cy - cy1
               dz = 0#cz - cz1           
               if (secondPass == 0) :
                   self.ROIDictionary._SaveUndoPoint ()                 
               obj = self.ROIDictionary.getROIObject (selectedObjName)
               itemName = obj.getName (self.ROIDefs)
               if (obj is not None and (self.ROIDefs.isROIHidden (itemName) or self.ROIDefs.isROILocked (itemName))) :
                   self._SelectionMoveROIState = None   
                   return
               if (eventtype ==  "EndMouseMove" and not obj._isMoving ()):
                   self._SelectionMoveROIState = None   
                   return
               if obj == None :
                   self._SelectionMoveROIState = None               
               else:
                   if (self.ROIDefs.isROIArea (selectedObjName)) :
                         #if self.ROIDictionary.isROIinAreaMode () :
                         #   self._SelectionMoveROIState = None   
                         #   return
                         #contourIDLst = self.ROIDictionary.getROISelectedContours (selectedObjName)                                                                         
                         obj.setIsMoving (eventtype != "EndMouseMove")
                         dx, dy,dz, moveMemory = obj.moveContours (selectedContourList, dx, dy, dz, sliceview.getNIfTIVolume().getSliceDimensions (), moveMemory, SliceView = self.ui.XYSliceView, ROIDictionary = self.ROIDictionary, BaseCoordinate = (cx1, cy1, cz1), MousePos = (imageX, imageY))
                   elif (self.ROIDefs.isROIPoint (selectedObjName))  :
                         pointIDLst = selectedContourList #self.ROIDictionary.getROISelectedContours (selectedObjName)                               
                         dx, dy,dz = obj.movePoint (pointIDLst, dx, dy, dz, sliceview.getNIfTIVolume().getSliceDimensions ())                   
                   cx = cx1 + dx
                   cy = cy1 + dy
                   cz = cz1 + dz
                   self._SelectionMoveROIState = (cx,cy,cz, selectedObjName, selectedContourList, 1, xMouseOffset, yMouseOffset, zMouseOffset, moveMemory)
                                  
               if (eventtype != "EndMouseMove") :                                                                            
                   self._updateSliceViewCoord (cX = cx, cY = cy, cZ = None, dX = dx, dY = dy) 
           if (eventtype == "EndMouseMove") : #if event is ending then zoom in                                              
                  self.NiftiAxisSelection (cx, cy, cz, sliceview.getSliceAxis (), True)   
                  sliceview.setSelected (True)
                  self._SelectionMoveROIState = None
                  self.ROIDictionary._CallDictionaryChangedListner  ()  
              
           sliceview.clearTemporaryDrawLine ()               
           self.update () 
           return 
       
       if (self.isZoomInXYUIState ()): # If Zoom in tool is selected          
           if self._altScrollZoomWindow (imageX, imageY, sliceview, eventtype) :
               return                            
           if QTButtonPressed ==  QtCore.Qt.MidButton :
               if len (line) > 1 :                   
                   dy = int (line[-1][1] - line[-2][1])
                   dx = int (line[-1][0] - line[-2][0])
                   dx = int (math.sqrt(dy**2 + dx**2))
                   dx = min (dx,5)                   
                   if dx > 0 :
                       self.ui.XYSliceView.mouseWheelZoom (dx)                                  
           else:
               self._XYZoomInit = RCC_ContourWindow._ZoomWindow (line, sliceview, self._XYZoomInit, eventtype, ClickEventHandled)                                           
           return  # Exits
       if (self.isZoomOutXYUIState ()): # If Zoom in tool is selected          
           if self._altScrollZoomWindow (imageX, imageY, sliceview, eventtype) :
               return                            
           if QTButtonPressed ==  QtCore.Qt.MidButton :
               if len (line) > 1 :                   
                   dy = int (line[-1][1] - line[-2][1])
                   dx = int (line[-1][0] - line[-2][0])
                   dx = int (math.sqrt(dy**2 + dx**2))
                   dx = min (dx,5)
                   if dx > 0 :
                       self.ui.XYSliceView.mouseWheelZoom (-dx)                                  
           return
       
       # If contouring tool is selected and bounding box control point is selected then update bounding box control point position.
       if (self.isContourXYUIState ()) :  
           try:                              
               self.setWindowUpdateEnabled (False)
               if self._altScrollZoomWindow (imageX, imageY, sliceview, eventtype) :
                   return                          
                   
               
               obj = self.ROIDictionary.getSelectedObject ()
               if (obj is not None) :
                  itemName = obj.getName (self.ROIDefs)
               if (obj is not None and not self.ROIDefs.isROIHidden (itemName) and not self.ROIDefs.isROILocked (itemName)) :
                   if (self.ROIDefs.isROIArea (itemName) and len (line) > 0):
                       if obj.getSelectedBoundingBoxControlPoint () is not None or (obj.getSelectedDeepGrowControlPoint () is not None and self._penSettings.getPenTool () == "DeepGrow") :                           
                           imageX = line[-1][0]
                           imageY = line[-1][1]
                           if obj.getSelectedBoundingBoxControlPoint () is not None :
                               if not self.getKeyState (QtCore.Qt.Key_Alt) :
                                   obj.setControlPointPosition ( imageX, imageY, SavePosition = eventtype == "EndMouseMove") 
                               if (eventtype == "EndMouseMove") :       
                                   obj.setSelectedBoundingBoxControlPoint (None)                   
                                   obj.setIsMoving (False)
                                   self.ROIDictionary._CallDictionaryChangedListner ()
                           elif (obj.getSelectedDeepGrowControlPoint () is not None and self._penSettings.getPenTool () == "DeepGrow") :
                               selectedDeepGrowControlPoint = obj.getSelectedDeepGrowControlPoint ()
                               if selectedDeepGrowControlPoint is not None :
                                   selectedDeepGrowPoint, Selection = selectedDeepGrowControlPoint
                                   axis = sliceview.getSliceAxis ()
                                   if selectedDeepGrowPoint.getAxisSlice (axis) != sliceview.getSliceNumber() :
                                       obj.clearSelectedDeepGrowControlPoint ()
                                   else:    
                                       if Selection == "Radius" :
                                           if not self.getKeyState (QtCore.Qt.Key_Alt) :
                                               centerX, centerY = selectedDeepGrowPoint.getCenterCoordinateInSliceView (axis)
                                               radiusDist = math.sqrt((float(imageX - centerX)) ** 2 + (float(imageY-centerY))**2)
                                               selectedDeepGrowPoint.setRadius (radiusDist)
                                               obj.setSliceNumber (sliceview.getSliceNumber())
                                       elif Selection == "Center" :
                                           if not self.getKeyState (QtCore.Qt.Key_Alt) :
                                               selectedDeepGrowPoint.setCenterPosInSlice (axis, imageX, imageY)
                                               obj.setSliceNumber (sliceview.getSliceNumber())
                                   if (eventtype == "EndMouseMove") :       
                                       obj.setIsMoving (False)
                                       self.ROIDictionary._CallDictionaryChangedListner ()
                           sliceview.clearTemporaryDrawLine ()                     
                           self.update ()
                           return
                       
                           
                           
               if self._penSettings.getPenTool () != "DeepGrow" :
                   if self.ROIDefs.isROISelected () and self.ROIDefs.isOneROISelected () :    # if ROI is selected otherwise info message
                           if not self.showFlattenDeepGrowDlg ("One-click generated segmentations must be flattened before an Area ROI can be added. Do you wish to flatten the segmentation?") :
                               return 
                           
                           itemName =self.ROIDefs.getSelectedROI ()[0]  # get currently selected ROI                                             
                           if (self.ROIDefs.isROIHidden (itemName) or self.ROIDefs.isROILocked (itemName)) :
                                self.NiftiAxisSelection (cx, cy, cz, sliceview.getSliceAxis (), True, EnableROISelection = False)
                                sliceview.setSelected (True)       
                           else:
                               if (self.ROIDefs.isROIArea (itemName)) :  # tests if currently selected ROI is a area object        
                                   if self.ROIDictionary.isROIinAreaMode () : 
                                       self._areaModeContour (cx, cy, cz, sliceview, line, eventtype, QTButtonPressed)
                                                                                                                     
                                       
                                   elif self.ROIDictionary.isROIinPerimeterMode () :                                                  
                                       selectedContourCount = self.ROIDictionary.getROISelectedContourCount (itemName) #
                                       if (selectedContourCount <= 1) : # if zero contours are selected then add one otherwise edit/replace the selected contour                   
                                           
                                           if (eventtype != "EndMouseMove") : 
                                               # if mouse move is not over then update line UI
                                               itemColor =self.ROIDefs.getSelectedROIColor ()
                                               sliceview.setTemporaryDrawLine (line, itemColor)
                                               sliceview.update ()
                                           else:
                                               # if mouse move is over then modify
                                               sliceview.clearTemporaryDrawLine ()     
                                               sliceview.update ()             
                                               
                                               ControlKeyPressed = self.getKeyState (QtCore.Qt.Key_Control)       
                                               roiAddedName = None
                                               if (not self.ROIDictionary.isROIDefined(itemName)) : # if object is not defined then add it.
                                                   line = FindContours.correctContourWinding (sliceview.getSliceShape(), line)
                                                   if (len (line) > 0) :
                                                       self.ROIDictionary.addROI (itemName, MultiROIAreaObject (self.ui.XYSliceView.getSliceNumber ()  , line, sliceview.getSliceShape(), "Human"))   # Add ROI Objects Only Allow the User to define ROI on the Z Stack
                                                       roiAddedName = itemName
                                               elif (selectedContourCount == 0 or ControlKeyPressed) :  # if no contour is selected then add another contour                                   
                                                   line = FindContours.correctContourWinding (sliceview.getSliceShape(), line)
                                                   if (len (line) > 0) :
                                                       contourID = self.ROIDictionary.getROIContourManger (itemName).allocateID ()                                   
                                                       oldobj = self.ROIDictionary.getROIObject (itemName)
                                                       oldobj = oldobj.copy (MoveSliceMaskVolumeCache = True)  # copy required for backup code                                   
                                                       sliceNumber = self.ui.XYSliceView.getSliceNumber ()   
                                                       oldobj.setSliceROIPerimeter (contourID, sliceNumber , line, sliceview.getSliceShape(), "Human")   
                                                       oldobj.setSelectedContours ([contourID])
                                                       self.ROIDictionary.addROI (itemName, oldobj)
                                                       roiAddedName = itemName
                                               elif (selectedContourCount == 1) :  # if a contour is selected then
                                                   contourID = self.ROIDictionary.getROISelectedContours (itemName)[0]                                   
                                                                                                                         
                                                   oldobj = self.ROIDictionary.getROIObject (itemName)
                                                   oldobj = oldobj.copy (MoveSliceMaskVolumeCache = True)  # copy required for backup code
                                                   
                                                   sliceNumber = self.ui.XYSliceView.getSliceNumber ()   
                                                   
                                                   #try to merge line segments if segements cannot be merged then replace old contour with the new one
                                                   if (oldobj.mergeLineSegments (contourID, sliceNumber, sliceview.getSliceShape (), line)) :   
                                                      if (not oldobj.isHumanEditedContour (contourID, sliceNumber)) : # sets the contours to being human edited
                                                           oldobj.setHumanEditedContour (contourID, sliceNumber, True)           
                                                           oldobj.setHumanEditedContourSnapCount (contourID, sliceNumber, 0)     
                                                      self.ROIDictionary.addROI (itemName, oldobj)                                                                                                                                                                
                                                      roiAddedName = itemName
                                                      
                                                   else :
                                                       line = FindContours.correctContourWinding (sliceview.getSliceShape(), line)
                                                       if (len (line) > 0) :
                                                           oldobj.setSliceROIPerimeter (contourID, sliceNumber , line, sliceview.getSliceShape(), "Human")                                       
                                                           self.ROIDictionary.addROI (itemName, oldobj) 
                                                           roiAddedName = itemName
                                               else:
                                                  print ("Error, unexpected contour number")
                                                  
                                               if (roiAddedName != None) :
                                                   obj = self.ROIDictionary.getROIObject (roiAddedName)  
                                                   if (obj != None) :
                                                       obj.adjustBoundBoxToFitSlice(self.ui.XYSliceView.getSliceNumber (), self.NiftiVolume.getSliceDimensions ())
                                                       #self.ROIDictionary._CallDictionaryChangedListner ()
                                                       
                                                       #disabed changing coordinate at end of mouse move for perfomance
                                                       self.SelectedCoordinatChanged (obj.getCoordinate ())
                                       else:
                                           MessageBoxUtil.showMessage ("To many contours selected", "Only one contour may be selected during adding/editing.")                    
                   else:
                       MessageBoxUtil.showMessage ("No ROI Selected", "An ROI must be selected before slice contours can be added.")
           finally:               
               self.setWindowUpdateEnabled (True)
           return
    
       sliceview.clearTemporaryDrawLine ()  
       return
       
    def deleteSliceCallback (self, SliceSelectionWidget, AllSelectedROI = True) :
        if self.ROIDictionary is None or self.ROIDictionary.isReadOnly (): 
            return        
        First = True
        axis = SliceSelectionWidget.getSliceAxis ()
        for SliceView in [self.ui.XYSliceView, self.ui.XZSliceView, self.ui.YZSliceView, None] :
            if SliceView is not None :
                if SliceView.getSliceAxis () == axis :
                    break        
        if axis != "Z" :
            if not self.ROIDictionary.isROIinAreaMode () :
                return
            if not self.showFlattenDeepGrowDlg ("One-click generated segmentations must be flattened before a slice can be deleted. Do you wish to flatten the segmentation?") :
                return  
        roiEntryRemoved = False        
        sliceSelection = SliceSelectionWidget.getSelection ()
        firstIndex = sliceSelection[0]
        lastIndex = sliceSelection[-1]
        if (not AllSelectedROI) :
            if self.ROIDefs.isOneROISelected() :        
                name =self.ROIDefs.getSelectedROI ()[0]     
                if  not self.ROIDefs.isROILocked (name) :
                    if self.ROIDictionary.removeROIEntry (name, firstIndex, First, callChangeListener = False, Axis=axis, lastIndex = lastIndex, RemoveProjectedSlices = True, SliceView = SliceView) :
                        roiEntryRemoved = True
        else:
            selectROIList = self.ROIDefs.getSelectedROI ()            
            if (self.ROIDefs.isOneROISelected()) :
                RemoveAllContours = False
            else:
                RemoveAllContours = True
            for name in selectROIList :                    
                if name != "None" :                
                    if  not self.ROIDefs.isROILocked (name) :
                        if self.ROIDictionary.removeROIEntry (name, firstIndex, First, callChangeListener = False, RemoveAllContours = RemoveAllContours, Axis=axis, lastIndex = lastIndex, RemoveProjectedSlices = True, SliceView = SliceView) :
                            First = False
                            roiEntryRemoved = True                            
        if roiEntryRemoved :          
            self.updatePenSettingsDlgsOneClickROI ()
            self.ROIDictionary._CallDictionaryChangedListner ()
            

    
        
            
    def getLoadedNIfTIFile (self) :
        return self._lastOpenedNIfTIDataFile
    
    def getSelectedNIfTIDataset (self):
        return self.NiftiVolumeTreeSelection
    
    def incrementSlice (self, dslice) :
        if (self.NiftiVolume != None and self.SelectedCoordinate != None) :
            sliceNumber = self.getSelectedSlice ()
            sliceNumber += dslice
            if (sliceNumber < 0) :
                sliceNumber = 0
            if (sliceNumber >= self.NiftiVolume.getZDimSliceCount ()) :
                sliceNumber = self.NiftiVolume.getZDimSliceCount () -1
            self.SelectedCoordinate.setZCoordinate (sliceNumber)                                                     
    
    def _getPaintToolsDlg (self) :
        if (self._PaintToolsDlg is None) :
            self._PaintToolsDlg = RCC_DrawToolDlg (self._ProjectDataset, self.getPenSettings (),self)           
        return self._PaintToolsDlg
    
    
    def isCurrentImageN4BiasCorrected (self) :
        if self.NiftiVolume is not None :
            return self.NiftiVolume.didN4BiasCorrectionSucceed ()
        return None
    
    def showN4BiasFieldCorrectionImage (self) :
        if self.NiftiVolume is not None :
            isCurrentImageN4BiasCorrected = self.NiftiVolume.didN4BiasCorrectionSucceed ()
            if not isCurrentImageN4BiasCorrected :
                self._toggleN4BiasFieldCorrectionOnLoadedImage ()
                
    def showRegularImage (self) :
       if self.NiftiVolume is not None :
            isCurrentImageN4BiasCorrected = self.NiftiVolume.didN4BiasCorrectionSucceed ()
            if isCurrentImageN4BiasCorrected :
                self._toggleN4BiasFieldCorrectionOnLoadedImage ()
    
    def _toggleN4BiasFieldCorrectionOnLoadedImage (self):
        if self.NiftiVolume is not None :
            isCurrentImageN4BiasCorrected = self.NiftiVolume.didN4BiasCorrectionSucceed ()
            setToN4BiasCorrectedImage = not isCurrentImageN4BiasCorrected
            
            path = self.NiftiVolume.getFileName ()
            volumeData = None
            if setToN4BiasCorrectedImage and not NiftiVolumeData.hasN4BiasCache (path, self.visSettings.getAutomaticN4BiasMaskMode (), self.visSettings.getN4BiasCustomMaskThreshold ()) :            
                    n4BConversionDialog = N4BiasConversionDlg (self,path, self.visSettings.getAutomaticN4BiasMaskMode (), self.visSettings.getN4BiasCustomMaskThreshold ())
                    n4BConversionDialog.setModal (True)
                    n4BConversionDialog.show ()
                    n4BConversionDialog.waitForProcessToFinish (self._ProjectDataset)
                    didSucceed = n4BConversionDialog.getN4BiasConversionResult ()
                    if not didSucceed :
                        MessageBoxUtil.showMessage ("N4 Bias Field Correction Not Loaded", "N4 Bias Field correction was canceled or failed.")                    
                    else:
                        nifti = nib.load (path)              
                        volumeData = NiftiVolumeData (nifti, MatchOrientation = self.NiftiVolume, OpenAndApplyN4BiasCorrection = True, N4BiasMaskMode = self.visSettings.getAutomaticN4BiasMaskMode (), N4BiasMaskThreshold=self.visSettings.getN4BiasCustomMaskThreshold ())
            else:            
                nifti = nib.load (path)              
                volumeData = NiftiVolumeData (nifti, MatchOrientation = self.NiftiVolume, OpenAndApplyN4BiasCorrection = setToN4BiasCorrectedImage, N4BiasMaskMode = self.visSettings.getAutomaticN4BiasMaskMode (), N4BiasMaskThreshold=self.visSettings.getN4BiasCustomMaskThreshold ())
            if volumeData is not None :
                self.NiftiVolume.setImageDataFromNiftiVolumeData(volumeData)
                self.ui.XYSliceView.UpdateSlice ()
                self.ui.XZSliceView.UpdateSlice ()
                self.ui.YZSliceView.UpdateSlice ()

            
    
    # loads NIfTI file     
    def LoadNiftiViewer (self, path, name, selectedNiftiDataSetTreeNode, Orientation = "None", ReOrientNiftiVolume = False, RotateCurrentlyLoadedNiftiToNewOrientation = False)  :                     
        self.disableRangeSelectionCallBack ()
        self.ui.XYSliceView.setMouseTracker (False)
        self.ui.XZSliceView.setMouseTracker (False)
        self.ui.YZSliceView.setMouseTracker (False)        
        self.ui.XYSliceView.setROIHiddenLabel ("")
        self.ui.XYSliceView.setROILockedLabel ("")
        self.ui.ThresholdLbl.setText ("")          
        try :                    
            try :                
                self.ui.MajorAxisSliceTextBox.editingFinished.connect (self.MajorAxisTextBoxChanged)                
                ReconnectMajorAxis = True
            except:
                ReconnectMajorAxis = False
            
            try:                
                self.ui.YZSliceAxisSliceTextBox.editingFinished.connect (self.YZSliceAxisTextBoxChanged)        
                ReconnectYZAxis = True
            except:
                ReconnectYZAxis = False
            
            try:                
                self.ui.XZSliceAxisSliceTextBox.editingFinished.connect (self.XZSliceAxisTextBoxChanged)   
                ReconnectXZAxis = True
            except:
                ReconnectXZAxis = False
            
            if (path is None) :
                self._LoadedNiftiOrientation = []
                self.ROIDefs.reloadROIDefs ()            
                self._silentClearROIList ()
                self._silentClearContourList ()
                self.ui.ContourList.clear ()
                self._setListUI (self.ui.ROI_List, self.ROIDefs)                        
                    
                self.NiftiVolume = None                 
                self.updateTranslationDlg ()
                self.updateMLDatasetDescriptionDlg ()
                self.ui.actionRegistration_log_window.setEnabled (False)                             
                self.ui.XYSliceView.setFileNameLabel (name)
                self.ui.XYSliceView.setNiftiDataSet ( None, None)     
                self.ui.XZSliceView.setNiftiDataSet ( None, None)        
                self.ui.YZSliceView.setNiftiDataSet ( None, None)    
                self.ui.contourOrientationLbl.setEnabled (False) 
                self.ui.contourOrientationComboBox.setEnabled (False) 
                coordinate = None
                self.ui.XYSliceView.initNiftiDataSet ( coordinate,  "Z", callback = False, slider = self.ui.XYSliceSlider, zoomScrollX = self.ui.horizontalZoomScrollBar, zoomScrollY=  self.ui.verticalZoomScrollBar)     
                self.ui.XZSliceView.initNiftiDataSet ( coordinate,  "Y", callback = False, slider = self.ui.XZSliceSlider, zoomScrollX = self.ui.XZ_HorizontalZoomScrollBar, zoomScrollY=  self.ui.XZ_VerticalZoomScrollBar)     
                self.ui.YZSliceView.initNiftiDataSet ( coordinate,  "X", callback = False, slider = self.ui.YZSliceSlider, zoomScrollX = self.ui.YZ_HorizontalZoomScrollBar, zoomScrollY=  self.ui.YZ_VerticalZoomScrollBar)     
                
                self.ui.actionShow_scan_phase_selection_window.setEnabled (False)
                self.ui.XYSliceSelectorWidget.setSliceCount (0,  self.ui.XYSliceView.getSliceAxis (), callback = False)             
                self.ui.XZSliceVerticalSelector.setSliceCount (0,  self.ui.XZSliceView.getSliceAxis (), callback = False) 
                self.ui.YZSliceVerticalSelector.setSliceCount (0,  self.ui.YZSliceView.getSliceAxis (), callback = False) 
                self.ui.XYSliceVerticalSelector.setSliceCount (0,  self.ui.XYSliceView.getSliceAxis (), callback = False) 
                
                self.ui.MajorAxisSliceTextBox.setText ("")
                self.ui.MajorAxisSliceTextBox.setEnabled (False)
                self.ui.YZSliceAxisSliceTextBox.setText ("")
                self.ui.YZSliceAxisSliceTextBox.setEnabled (False)
                self.ui.XZSliceAxisSliceTextBox.setText ("")
                self.ui.XZSliceAxisSliceTextBox.setEnabled (False)
                self.ui.ScanPhaseComboBox.setEnabled (False)
                self.ui.ROI_List.setEnabled (False)                   
                self.ui.ContourList.setEnabled (False)            
                self.ui.contourSingleSliceBtn.setEnabled (False)
                self.ui.autoContourBtn.setEnabled (False)
                self._getPaintToolsDlg().setButtonEnabledState (Select = False, Draw = False, ZoomIn = False, ZoomOut = False, EraseandFill = False, DeepGrow = False)            
                self.ui.UndoBtn.setEnabled (False)
                self.ui.RedoBtn.setEnabled (False)
                self.ui.ClearSelectedBtn.setEnabled (False)
                self.ui.BatchContourBtn.setEnabled (False)
                self.ui.AddContourBtn.setEnabled (False)
                self.ui.deleteContourBtn.setEnabled (False)
                self.ui.actionShow_Pesscara_Mask_Viewer_Window.setEnabled (False)
                self.ui.actionExportVolume_mask.setEnabled (False)
                self.ui.actionExportVolume_CSV.setEnabled (False)
                self.ui.Slicelbl.setEnabled (False)
                self.ui.scanPhaseLbl.setEnabled (False)
                self.ui.label_7.setEnabled (False)            
                
                #self.ui.addROIBtn.setEnabled(False)
                #self.ui.RemoveROIBtn.setEnabled(False)
                
                self.ui.SelectedROI.setEnabled (False)
                self.ui.SelectedROI.setText ("")
                self.ui.SelectedContourLbl.setEnabled (False)
                self.ui.SelectedContourLbl.setText ("")
                self.ui.actionShow_ROI_Statistics.setEnabled (False)
                self.ui.actionOpen_3D_Viewer_2.setEnabled (False)            
                self.ui.actionShow_Hounsfield_visualization_settings.setEnabled (False)
                self.ui.XYSliceView.setVolumeMask (None)
                self.ui.YZSliceView.setVolumeMask (None)
                self.ui.XZSliceView.setVolumeMask (None)
                self._setPreviewSegmentationMaskPath (None)
                self.ui.XYSliceSelectorWidget.setSliceColorMask (None, None)
                self.ui.XZSliceVerticalSelector.setSliceColorMask (None, None)
                self.ui.YZSliceVerticalSelector.setSliceColorMask (None, None)
                self.ui.XYSliceVerticalSelector.setSliceColorMask (None, None)
                self._updatePesscaMaskViewDlg (Clear = True)           
                if (self.NiftiVolumeTreeSelection is not None) :
                    self.NiftiVolumeTreeSelection.setDatasetOpen (False)
                self.NiftiVolumeTreeSelection = None
                self.updateStatsDlg ()  
                self.update ()
                self.updateUIToReflectAreaPerimeterDrawing () 
                return
            
            lastSliceAxis = "None"
            if (self.NiftiVolume is not None):        
                lastSliceAxis  = self.NiftiVolume.getSliceAxis ()
                    
            try:
                self.ui.actionShow_scan_phase_selection_window.setEnabled (True)
                #self.ui.addROIBtn.setEnabled(True)
                #self.ui.RemoveROIBtn.setEnabled(False)            
                self.ui.XYSliceView.setVolumeMask (None)
                self.ui.YZSliceView.setVolumeMask (None)
                self.ui.XZSliceView.setVolumeMask (None)
                self._setPreviewSegmentationMaskPath (None)
                self.ui.XYSliceSelectorWidget.setSliceColorMask (None, None)
                self.ui.XZSliceVerticalSelector.setSliceColorMask (None, None)
                self.ui.YZSliceVerticalSelector.setSliceColorMask (None, None)
                self.ui.XYSliceVerticalSelector.setSliceColorMask (None, None)
                
                self.ui.Slicelbl.setEnabled (True)
                self.ui.scanPhaseLbl.setEnabled (not self.ROIDictionary.isReadOnly ())
                self.ui.ScanPhaseComboBox.setEnabled (not self.ROIDictionary.isReadOnly ())
                self.ui.label_7.setEnabled (True)            
                self.ui.SelectedROI.setEnabled (True)
                self.ui.SelectedContourLbl.setEnabled (True)
                self.ui.ROI_List.setEnabled (self.isROIDictonaryFileLoaded ())
                self.ui.ContourList.setEnabled (True)
                self.ui.contourSingleSliceBtn.setEnabled (not self.ROIDictionary.isReadOnly ())
                self.ui.autoContourBtn.setEnabled (not self.ROIDictionary.isReadOnly ())
                self._getPaintToolsDlg().setButtonEnabledState (Select = True, Draw = not self.ROIDictionary.isReadOnly (), ZoomIn = True, ZoomOut = False, EraseandFill = False)            
                self.ui.UndoBtn.setEnabled (not self.ROIDictionary.isReadOnly ())
                self.ui.RedoBtn.setEnabled (not self.ROIDictionary.isReadOnly ())
                self.ui.ClearSelectedBtn.setEnabled (not self.ROIDictionary.isReadOnly ())
                self.ui.BatchContourBtn.setEnabled (self.isROIDictonaryFileLoaded () and not self.ROIDictionary.isReadOnly ())
                self.ui.AddContourBtn.setEnabled (self.isROIDictonaryFileLoaded () and not self.ROIDictionary.isReadOnly ())
                self.ui.deleteContourBtn.setEnabled (self.isROIDictonaryFileLoaded () and not self.ROIDictionary.isReadOnly ())
                self.ui.actionShow_ROI_Statistics.setEnabled (True)
                self.ui.actionOpen_3D_Viewer_2.setEnabled (True)
                self.ui.actionShow_Hounsfield_visualization_settings.setEnabled (True)
               
                
                oldSelectedTreeNode = self.NiftiVolumeTreeSelection  # save the previously loaded file if any so if an error occures the old file can be reloaded
                if (oldSelectedTreeNode is not None) :
                    oldSelectedTreeNode.setDatasetOpen (False)   
                oldnamelabel = self.ui.XYSliceView.getFileNameLabel ()
                
               
                    
                self.NiftiVolumeTreeSelection = selectedNiftiDataSetTreeNode
                if self.NiftiVolumeTreeSelection is not None :
                    self.NiftiVolumeTreeSelection.setDatasetOpen (True)
                            
                self.ui.actionShow_Pesscara_Mask_Viewer_Window.setEnabled (self.NiftiVolumeTreeSelection is not None and self.NiftiVolumeTreeSelection.getPesscaraInterace () is not None)
                self.ui.actionExportVolume_mask.setEnabled (True)
                self.ui.actionExportVolume_CSV.setEnabled (True)
                
                if (RotateCurrentlyLoadedNiftiToNewOrientation) :
                     self.NiftiVolume.rotateLoadedVolumeToNewOrientation (Orientation)
                else:
                    # Load the new dataset    
                    self.ui.XYSliceView.setNiftiDataSet ( None, None)     
                    self.ui.XZSliceView.setNiftiDataSet ( None, None)        
                    self.ui.YZSliceView.setNiftiDataSet ( None, None)                         
                    
                    LoadN4BiasCorrectedImaged = self.visSettings.getPerformAutomaticN4BiasCorrection ()
                    if (LoadN4BiasCorrectedImaged) :
                        if not NiftiVolumeData.hasN4BiasCache (path, self.visSettings.getAutomaticN4BiasMaskMode (), self.visSettings.getN4BiasCustomMaskThreshold ()) :
                            n4BConversionDialog = N4BiasConversionDlg (self,path, self.visSettings.getAutomaticN4BiasMaskMode (), self.visSettings.getN4BiasCustomMaskThreshold ())
                            n4BConversionDialog.setModal (True)
                            n4BConversionDialog.show ()
                            n4BConversionDialog.waitForProcessToFinish ()
                            didSucceed = n4BConversionDialog.getN4BiasConversionResult ()
                            if not didSucceed :
                                MessageBoxUtil.showMessage ("N4 Bias Field Correction Not Loaded", "N4 Bias Field correction was canceled or failed.")
                                LoadN4BiasCorrectedImaged = False                        
                            
                    nifti = nib.load (path)  
                    self.NiftiVolume = NiftiVolumeData (nifti, SliceAxis = Orientation, OpenAndApplyN4BiasCorrection = LoadN4BiasCorrectedImaged, N4BiasMaskMode = self.visSettings.getAutomaticN4BiasMaskMode (), N4BiasMaskThreshold=self.visSettings.getN4BiasCustomMaskThreshold ())
                    # Start Slight hack to correctly orient axel scan data  in secondary off axis visulizations
                    orientation = nib.aff2axcodes(nifti.affine)
                    print (orientation)
                    del nifti
                self._LoadedNiftiOrientation = [Orientation]
               
                #print (self.NiftiVolume.getNormalizedVoxelSize ())            
                coordinate = Coordinate ()
                coordinate.setCoordinate ([int (self.NiftiVolume.getXDimSliceCount () /2), int (self.NiftiVolume.getYDimSliceCount () /2), int (self.NiftiVolume.getZDimSliceCount () /2)])       
                self.ui.XZSliceView.setCoordinate (coordinate, callChangeListener = False)
                self.ui.YZSliceView.setCoordinate (coordinate, callChangeListener = False)
                self.ui.XYSliceView.setNiftiDataSet (self.NiftiVolume, self._ProjectDataset)
                self.ui.XZSliceView.setNiftiDataSet (self.NiftiVolume, self._ProjectDataset)
                self.ui.YZSliceView.setNiftiDataSet (self.NiftiVolume, self._ProjectDataset)
                self.ui.XYSliceView.initNiftiDataSet ( coordinate,  "Z", callback = False, slider = self.ui.XYSliceSlider,  zoomScrollX = self.ui.horizontalZoomScrollBar, zoomScrollY=  self.ui.verticalZoomScrollBar)     
                self.ui.XZSliceView.initNiftiDataSet ( coordinate,  "Y", callback = False, slider = self.ui.XZSliceSlider, zoomScrollX = self.ui.XZ_HorizontalZoomScrollBar, zoomScrollY=  self.ui.XZ_VerticalZoomScrollBar)     
                self.ui.YZSliceView.initNiftiDataSet ( coordinate,  "X", callback = False, slider = self.ui.YZSliceSlider, zoomScrollX = self.ui.YZ_HorizontalZoomScrollBar, zoomScrollY=  self.ui.YZ_VerticalZoomScrollBar)     
           
                self.ui.XZSliceView._ViewPortTransformation.setFlipHorizontal (False)
                self.ui.YZSliceView._ViewPortTransformation.setFlipHorizontal (False)
                self.ui.XZSliceView._ViewPortTransformation.setFipVertical (False)
                self.ui.YZSliceView._ViewPortTransformation.setFipVertical (False)
                self.ui.XZSliceView._ViewPortTransformation.setRotation (0)            
                self.ui.YZSliceView._ViewPortTransformation.setRotation (0)
                
                self.NiftiVolume.correctViewPortTransformation (self.ui.XZSliceView._ViewPortTransformation, self.ui.YZSliceView._ViewPortTransformation)
                
    
                
                    #self.ui.XYSliceView._ViewPortTransformation.setFipVertical (True)
                
                # End Slight hack to correctly orient some scans
                        
                self.ui.XYSliceSelectorWidget.setSliceCount (self.ui.XYSliceView.getMaxSliceCount (),  self.ui.XYSliceView.getSliceAxis (), callback = False) 
                self.ui.XZSliceVerticalSelector.setSliceCount (self.ui.XZSliceView.getMaxSliceCount (),  self.ui.XZSliceView.getSliceAxis (), callback = False) 
                self.ui.YZSliceVerticalSelector.setSliceCount (self.ui.YZSliceView.getMaxSliceCount (),  self.ui.YZSliceView.getSliceAxis (), callback = False) 
                self.ui.XYSliceVerticalSelector.setSliceCount (self.ui.XYSliceView.getMaxSliceCount (),  self.ui.XYSliceView.getSliceAxis (), callback = False) 
                
                self.ui.MajorAxisSliceTextBox.setEnabled (self.ui.XYSliceView.getMaxSliceCount () > 0)   
                self.ui.YZSliceAxisSliceTextBox.setEnabled (self.ui.YZSliceView.getMaxSliceCount () > 0)   
                self.ui.XZSliceAxisSliceTextBox.setEnabled (self.ui.XZSliceView.getMaxSliceCount () > 0)   
                
                                
                self.ui.XYSliceView.setFileNameLabel (name)
                
                if (ReOrientNiftiVolume == False) :
                    #self.ui.ContourBtn.setChecked (True)
                    self.ResetSelectedRIODef ()
                    ROIListWithDGAnnotations = self.ROIDictionary.getROIListWithDeepGrowAnnotations ()
                    if len (ROIListWithDGAnnotations) > 0 :
                        self.ROIDefs.setSelectedROI (ROIListWithDGAnnotations[0], clearSelection = True, CallChangeListener = False) 
                        self.setDeepGrowMode ()
                    elif (self.isSelectionUIState ()):
                        self.setSelectionMode ()
                    else:
                        self.setContourMode ()
                
                self._updatePesscaMaskViewDlg ()
                self.ui.XYSliceView.setMouseTracker (True)
                self.ui.XZSliceView.setMouseTracker (True)
                self.ui.YZSliceView.setMouseTracker (True)
                self.updateUIToReflectAreaPerimeterDrawing () 
                self.ui.XYSliceView.setSelected (True)
                print ("Done loading Nifti")
                        
            except Exception as exp: # error occured load the old data file.
                if (oldSelectedTreeNode is not None) :
                    niftiDatasetFile = oldSelectedTreeNode.getNIfTIDatasetFile ()
                    if niftiDatasetFile.hasDataFile () :
                        try :
                            self.LoadNiftiViewer (niftiDatasetFile.getPath (), oldnamelabel, oldSelectedTreeNode, Orientation = lastSliceAxis)                
                        except:
                            self.LoadNiftiViewer (None, "No Data Selected", None, Orientation = "None")                
                MessageBoxUtil.showMessage ("File Open Error", "An error occured opening NifTi data file.", path + "\n\n" + str(exp))
                 
        finally:            
            if ReconnectMajorAxis : 
                self.ui.MajorAxisSliceTextBox.editingFinished.connect (self.MajorAxisTextBoxChanged)  
            if ReconnectYZAxis :              
                self.ui.YZSliceAxisSliceTextBox.editingFinished.connect (self.YZSliceAxisTextBoxChanged)        
            if ReconnectXZAxis :
                self.ui.XZSliceAxisSliceTextBox.editingFinished.connect (self.XZSliceAxisTextBoxChanged)   
   

    def _updatePesscaMaskViewDlg (self, Clear = False) : 
        try :
            if (self._PesscaraMaskViewerDlg != None) :
                if (not Clear) :
                    self._PesscaraMaskViewerDlg.updateMaskList (self)
                else:
                    self._PesscaraMaskViewerDlg.updateMaskList (None)
        except:
            print ("Could Not Update Mask viewing dlg. Closing")
            if (self._PesscaraMaskViewerDlg != None) :
                self._PesscaraMaskViewerDlg.close ()
                self._PesscaraMaskViewerDlg = None
            
   
                            
    def ResetSelectedRIODef (self):   
        try :    
            self.ui.ROI_List.selectionModel().selectionChanged.disconnect(self.ROIListSelectionChanged) 
            disconnected = True
        except :
            disconnected = False    
                        
        for item in self.ui.ROI_List.selectedItems () :
            item.setSelected (False)        
        
        self.ui.ROI_List.item(0).setSelected (True)
        self.ui.SelectedROI.setText ("None")
        palette = QtGui.QPalette()
        palette.setColor (self.ui.SelectedROI.foregroundRole(), QtGui.QColor ( 0,0,0))
        self.ui.SelectedROI.setPalette (palette)
        self.ROIDefs.clearSelectedROI ()

        if (disconnected) :
            self.ui.ROI_List.selectionModel().selectionChanged.connect(self.ROIListSelectionChanged) 
       
    # delete selected contour or point    
    def deleteSelectedContours (self)  :     
        if self.ROIDictionary is None : 
            return
        if self.ROIDefs.isROISelected () and self.ROIDefs.isOneROISelected () :   
             itemName =self.ROIDefs.getSelectedROI ()[0]  
             if  not self.ROIDefs.isROILocked (itemName) :                       
                 if  self.ROIDictionary.isROIDefined(itemName) :
                     # obj = self.ROIDictionary.getROIObject (itemName)   
                     self.deleteSliceCallback (self.ui.XYSliceSelectorWidget, AllSelectedROI = False)
                     """selectionIndexList = self.ui.XYSliceSelectorWidget.getSelection ()           
                     first = True
                     for index in selectionIndexList :
                         self.deleteSliceCallback (index, first)
                         first = False
                     self.deleteSliceCallback (index, first, Done = True)
                     self.ui.XYSliceSelectorWidget.clearSelection ()"""
    

                   

    def _ContourSetSliceIndexCallback (self, sliceIndex, Axis="Z") :
        
        if Axis == "Z" :
            if (not self.SelectedCoordinate.setZCoordinate (sliceIndex)):
                self.SelectedCoordinatChanged (self.SelectedCoordinate) 
        elif Axis == "X" :
            if (not self.SelectedCoordinate.setXCoordinate (sliceIndex)):
                self.SelectedCoordinatChanged (self.SelectedCoordinate) 
        else:
            if (not self.SelectedCoordinate.setYCoordinate (sliceIndex)):
                self.SelectedCoordinatChanged (self.SelectedCoordinate) 
        self.ui.XYSliceView.setROIObjectList (self.ROIDictionary)
        self.ui.XZSliceView.setROIObjectList (self.ROIDictionary)
        self.ui.YZSliceView.setROIObjectList (self.ROIDictionary)
        self.ui.XYSliceSelectorWidget.setROISliceDictionary (self.ROIDictionary, NIfTIVolume = self.NiftiVolume, AxisProjection = self.ui.XYSliceView.getAxisProjectionControls (), Coordinate = self.SelectedCoordinate)        
        self.ui.XZSliceVerticalSelector.setROISliceDictionary (self.ROIDictionary, self.NiftiVolume, self._ProjectDataset)
        self.ui.YZSliceVerticalSelector.setROISliceDictionary (self.ROIDictionary, self.NiftiVolume, self._ProjectDataset)
        self.ui.XYSliceVerticalSelector.setROISliceDictionary (self.ROIDictionary, self.NiftiVolume, self._ProjectDataset, AxisProjection = self.ui.XYSliceView.getAxisProjectionControls (), Coordinate = self.SelectedCoordinate) 
        
    # Called to update contouring for manually contoured sections    
    def contourSingleSlice (self) :           
        if self.ROIDictionary is None : 
            return
        if self.ROIDefs.isROISelected () and self.ROIDefs.isOneROISelected () :        
               itemName = self.ROIDefs.getSelectedROI ()[0]                  
               if self.ROIDefs.isROIArea (itemName) and self.ROIDictionary.isROIDefined(itemName) :      
                   if not self.ROIDefs.isROIHidden (itemName) and not self.ROIDefs.isROILocked (itemName) :    
                       if self.showFlattenDeepGrowDlg ("One-click generated segmentations must be flattened before an area ROI can be refined. Do you wish to flatten the segmentation?") :
                           if not self.ROIDictionary.isROIinAreaMode () or (self.ui.XYSliceView.isSelected () and not self.ui.XYSliceView.areAxisProjectionControlsEnabledForAxis () ):
                               selectionIndexList = self.ui.XYSliceSelectorWidget.getSelection ()
                               StartSliceIndex = int (self.ui.XYSliceView.getSliceNumber ()   )                                                                            
                               RCC_AutoContourROIArea.contourSingleSlice (self.NiftiVolume, self.ROIDictionary, self.hounsfieldVisSettings, itemName, selectionIndexList, StartSliceIndex, SetSlicIndexCallBack =  self._ContourSetSliceIndexCallback, ShowProgressDialogParentWindow = self, CliptoROIXYBoundingBox = True, FileSaveThreadManger = self._ProjectDataset.getROISaveThread (), ProjectDataset = self._ProjectDataset)                                                                     
                           else:
                               if self.ui.XZSliceView.isSelected () :
                                   sliceView = self.ui.XZSliceView
                                   selectionIndexList = self.ui.XZSliceVerticalSelector.getSelection ()
                               elif self.ui.YZSliceView.isSelected () :
                                   sliceView = self.ui.YZSliceView
                                   selectionIndexList = self.ui.YZSliceVerticalSelector.getSelection ()
                               else:
                                   sliceView = self.ui.XYSliceView
                                   selectionIndexList = self.ui.XYSliceSelectorWidget.getSelection ()                               
                               StartSliceIndex = int (sliceView.getSliceNumber ())                                                                            
                               RCC_AutoContourROIArea.contourSingleSlice_SliceView (sliceView, self.NiftiVolume, self.ROIDictionary, self.hounsfieldVisSettings, itemName, selectionIndexList, StartSliceIndex, SetSlicIndexCallBack =  self._ContourSetSliceIndexCallback, ShowProgressDialogParentWindow = self, CliptoROIXYBoundingBox = True, FileSaveThreadManger = self._ProjectDataset.getROISaveThread (), ProjectDataset = self._ProjectDataset)                                                                     
                           self.update ()

                       
    # Called to automatically generate contoured sections based on manually contoured sections 
    def autoContourBtn (self, itemName = False, selContours = None, firstslice = None, lastslice = None, StartSliceIndex = None, AutoContourHumanSlicesOnce = False) :                                
        if self.ROIDictionary is None : 
            return
        try :
            self.setEnabled (False)
            if ((self.ROIDefs.isROISelected () and self.ROIDefs.isOneROISelected ()) or (itemName != None)) :                       
                   if (itemName == False):
                       itemName = self.ROIDefs.getSelectedROI ()[0]          
                   if not self.ROIDefs.isROIHidden (itemName) and not self.ROIDefs.isROILocked (itemName) :  
                       if self.showFlattenDeepGrowDlg ("One-click generated segmentations must be flattened before an area ROI can be auto-contoured. Do you wish to flatten the segmentation?") :
                           if self.ROIDefs.isROIArea (itemName) and self.ROIDictionary.isROIDefined(itemName) :
                               obj = self.ROIDictionary.getROIObject (itemName)
                               if (selContours == None) :
                                   selContours = obj.getSelectedContours ()
                                   
                               if (firstslice == None) :
                                   firstslice, lastslice = obj.getBoundingBoxSliceRange ()
                                   firstslice = int (firstslice)
                                   lastslice = int (lastslice)
                               StartSliceIndex = int (self.ui.XYSliceView.getSliceNumber ()   )                                                                            
                               selectionIndexList = self.ui.XYSliceSelectorWidget.getSelection ()
                               
                               #return RCC_Morph.morphSliceRange ( self.ROIDictionary, itemName, StartSliceIndex, selectionIndexList[0], selectionIndexList[len (selectionIndexList)-1], ShowProgressDialogParentWindow = self, SetSlicIndexCallBack = self._ContourSetSliceIndexCallback, ContourIDList = selContours, CliptoROIXYBoundingBox = True) 
                               
                               return RCC_AutoContourROIArea.autoContour (self.NiftiVolume, self.ROIDictionary, self.hounsfieldVisSettings, itemName, selContours, firstslice, lastslice, StartSliceIndex, selectionIndexList,  SetSlicIndexCallBack =  self._ContourSetSliceIndexCallback, ShowProgressDialogParentWindow = self, CliptoROIXYBoundingBox = True, AutoContourHumanSlicesOnce = AutoContourHumanSlicesOnce, ProjectDataset = self._ProjectDataset)                  
                           
                           elif self.ROIDefs.isROIPoint (itemName) and self.ROIDictionary.isROIDefined(itemName) :
                                #slice selected but not contoured.    
                                obj = self.ROIDictionary.getROIObject (itemName)
                                obj = obj.copy ()
                                selection = self.ui.XYSliceSelectorWidget.getSelection ()
                                if len (selection) > 1 :
                                    obj.removeComputerEditedPointsInSliceRange (selection)
                                else:
                                    obj.removeComputerEditedPointsInSliceRange (None)
                                sortedSliceNumberList = obj.getSliceNumberList ()                
                                firstSliceContouredIndex   = None
                                nextSliceContouredIndex = 0
                                
                                if (len (selection) == 1):
                                    firstindex = None
                                    lastindex = None
                                    #zCord = selection[0]             
                                    if (len (sortedSliceNumberList) >= 2) :                            
                                        firstindex = sortedSliceNumberList[0]
                                        lastindex = sortedSliceNumberList[len (sortedSliceNumberList)-1]
                                    """
                                    for sliceindex in sortedSliceNumberList :
                                        if sliceindex < zCord :
                                            firstindex = sliceindex
                                        elif sliceindex > zCord :
                                            lastindex = sliceindex
                                            break
                                        elif sliceindex == zCord :
                                            if (firstindex == None) :
                                                firstindex = sliceindex
                                            else:
                                                lastindex = sliceindex"""
                                    if (firstindex is not None and lastindex is not None) :
                                        selection = range (firstindex, lastindex + 1)
                                else:
                                   firstsel = min (selection) 
                                   if firstsel not in sortedSliceNumberList and firstsel - 1 in sortedSliceNumberList :
                                       firstsel -= 1
                                   lastsel = max (selection) 
                                   if lastsel not in sortedSliceNumberList and lastsel + 1 in sortedSliceNumberList :
                                       lastsel += 1
                                   selection = range (firstsel, lastsel + 1)
                                   
                                if len (selection) > 1 :                            
                                    SaveUndoPt = True
                                    for  sliceindex in selection :
                                        if sliceindex in sortedSliceNumberList : #slice selected and is allready contoured, ignore 
                                            firstSliceContouredIndex = sortedSliceNumberList.index (sliceindex)
                                            if (firstSliceContouredIndex + 1 < len (sortedSliceNumberList)) :
                                                nextSliceContouredIndex = firstSliceContouredIndex + 1
                                            else:
                                                nextSliceContouredIndex = None
                                        else:
                                            #slice selected but not contoured.    
                                            closestContouredIndex = None
                                            if (firstSliceContouredIndex is None) :
                                                closestContouredIndex = nextSliceContouredIndex
                                            elif (nextSliceContouredIndex is None) :
                                                closestContouredIndex = firstSliceContouredIndex
                                            if (closestContouredIndex is not None) : #only one slice point found
                                                closestCord = obj.getPointCoordinate (obj.getSlicePointIDClosestToXY (sortedSliceNumberList[closestContouredIndex], self.SelectedCoordinate.getXCoordinate (), self.SelectedCoordinate.getYCoordinate ()))
                                                if (closestCord is not None) :
                                                    closestCord = closestCord.copy ()
                                                    closestCord.setZCoordinate (int (sliceindex))
                                                    obj.addPoint (itemName + " " + str (obj.getPointCount () + 1), closestCord, humanEditedPoint = False)                                                                         
                                                    self.ROIDictionary.addROI (itemName, obj, SaveUndoPt = SaveUndoPt)   
                                                    SaveUndoPt = False
                                            else: #two slices Points found
                                                firstClosestCord = obj.getPointCoordinate (obj.getSlicePointIDClosestToXY (sortedSliceNumberList[firstSliceContouredIndex], self.SelectedCoordinate.getXCoordinate (), self.SelectedCoordinate.getYCoordinate ()))
                                                nextClosestCord = obj.getPointCoordinate (obj.getSlicePointIDClosestToXY (sortedSliceNumberList[nextSliceContouredIndex], self.SelectedCoordinate.getXCoordinate (),  self.SelectedCoordinate.getYCoordinate ()))
                                                if (firstClosestCord is not None and nextClosestCord is not None) :
                                                    precent = float (sliceindex - sortedSliceNumberList[firstSliceContouredIndex])  / float (sortedSliceNumberList[nextSliceContouredIndex] - sortedSliceNumberList[firstSliceContouredIndex])                                                    
                                                    X = int (firstClosestCord.getXCoordinate ()) + int (precent * float(nextClosestCord.getXCoordinate () - firstClosestCord.getXCoordinate ()))
                                                    Y = int (firstClosestCord.getYCoordinate ()) + int (precent * float(nextClosestCord.getYCoordinate () - firstClosestCord.getYCoordinate ()))
                                                    Z = int (sliceindex)                                                
                                                    interpolatedCoord = Coordinate ()
                                                    interpolatedCoord.setCoordinate ((X,Y,Z))
                                                    obj.addPoint (itemName + " " + str (obj.getPointCount () + 1), interpolatedCoord, humanEditedPoint = False)                                                                         
                                                    self.ROIDictionary.addROI (itemName, obj, SaveUndoPt = SaveUndoPt)   
                                                    SaveUndoPt = False
            return False
        finally:
            self.setEnabled (True)
               
    
    @staticmethod
    def _resizeViewScrollbars (XYSliceView, verticalZoomScrollBar, horizontalZoomScrollBar, resizeWidgetHelper):
        if (verticalZoomScrollBar.isVisible () or horizontalZoomScrollBar.isVisible ())  :
            xpos = XYSliceView.pos ().x()
            ypos = XYSliceView.pos ().y()
            width = XYSliceView.width ()
            height = XYSliceView.height ()            
                                
            if (verticalZoomScrollBar.isVisible ()) :       
                verticalWidthOffset = verticalZoomScrollBar.width ()
                resizeWidgetHelper.setWidgetXPos (verticalZoomScrollBar, xpos + width - verticalWidthOffset)
                resizeWidgetHelper.setWidgetYPos (verticalZoomScrollBar, ypos)                    
            else:
                verticalWidthOffset = 0
                
            if (horizontalZoomScrollBar.isVisible ()) :       
                horizontalHeightOffset = horizontalZoomScrollBar.height ()
                resizeWidgetHelper.setWidgetXPos (horizontalZoomScrollBar, xpos )
                resizeWidgetHelper.setWidgetYPos (horizontalZoomScrollBar, ypos+height - horizontalHeightOffset)                                                
            else:
                horizontalHeightOffset = 0
            
            if (verticalZoomScrollBar.isVisible ()) :       
                resizeWidgetHelper.setHeight (verticalZoomScrollBar, height-horizontalHeightOffset)
            if (horizontalZoomScrollBar.isVisible ()) :       
                resizeWidgetHelper.setWidth (horizontalZoomScrollBar, width-verticalWidthOffset)                
            if (horizontalHeightOffset > 0) :
                resizeWidgetHelper.setHeight (XYSliceView, height - horizontalHeightOffset)
            if (verticalWidthOffset > 0) :
                resizeWidgetHelper.setWidth (XYSliceView, width - verticalWidthOffset)
                        
                        
    def _XYFrameResize (self, position) : 
        try :                                          
            self.resizeWidgetHelper.setWidgetXPos (self.ui.XYSliceView, 0)        
            self.resizeWidgetHelper.setWidgetYPos (self.ui.XYSliceView, 0)        
            self.resizeWidgetHelper.setWidgetYPos (self.ui.XYSliceVerticalSelector, 0)
            self.resizeWidgetHelper.setWidth (self.ui.XYSliceVerticalSelector, 12)            
            XYFrameHeight = self.ui.XYFrame.height ()
            XYSliceSliderWidth = self.ui.XYSliceSlider.width ()            
            self.resizeWidgetHelper.setWidth (self.ui.XYSliceView, self.ui.XYFrame.width () - XYSliceSliderWidth - self.ui.XYSliceVerticalSelector.width () - 2)        
            self.resizeWidgetHelper.setHeight (self.ui.XYSliceView, XYFrameHeight)            
            self.resizeWidgetHelper.setHeight (self.ui.XYSliceVerticalSelector, XYFrameHeight)
            SliceViewWidth = self.ui.XYSliceView.width () 
            self.resizeWidgetHelper.setWidgetXPos (self.ui.XYSliceSlider, SliceViewWidth + 1)        
            self.resizeWidgetHelper.setWidgetXPos (self.ui.XYSliceVerticalSelector, SliceViewWidth + 2 + XYSliceSliderWidth)        
            RCC_ContourWindow._resizeViewScrollbars (self.ui.XYSliceView, self.ui.verticalZoomScrollBar, self.ui.horizontalZoomScrollBar, self.resizeWidgetHelper)                        
            self.resizeWidgetHelper.setHeight (self.ui.XYSliceSlider, XYFrameHeight)        
            self.resizeWidgetHelper.setWidgetYPos (self.ui.XYSliceSlider, 0)                
        except:
            pass
        
    @staticmethod    
    def _resizeOffAxisFrame (resizeWidgetHelper, XZSliceAxisSliceTextBox, XZSliceView, XZSliceSlider, XZSliceVerticalSelector, XZ_HorizontalZoomScrollBar, XZ_VerticalZoomScrollBar, XZFrame):        
        try:
            sliceTxtBoxHeight =  XZSliceAxisSliceTextBox.height ()
            resizeWidgetHelper.setWidgetYPos (XZSliceView, 0)
            resizeWidgetHelper.setWidgetYPos (XZSliceAxisSliceTextBox, 0)         
            sliceTxtBoxOffset = sliceTxtBoxHeight + 5
            resizeWidgetHelper.setWidgetYPos (XZSliceSlider, sliceTxtBoxOffset)
            resizeWidgetHelper.setWidgetYPos (XZSliceVerticalSelector, sliceTxtBoxOffset)
                                    
            frameHeight = XZFrame.height ()            
            resizeWidgetHelper.setHeight (XZSliceView, frameHeight)            
            DimHeight = frameHeight - sliceTxtBoxHeight - 5
            resizeWidgetHelper.setHeight (XZSliceSlider, DimHeight)            
            resizeWidgetHelper.setHeight (XZSliceVerticalSelector, DimHeight)            
            resizeWidgetHelper.setWidth (XZSliceVerticalSelector, 12)            
            
            frameWidth = XZFrame.width ()    
            VerticalSelectorWidth = XZSliceVerticalSelector.width ()
            XZSliceSliderWidth = XZSliceSlider.width ()
            resizeWidgetHelper.setWidth (XZSliceView, frameWidth - XZSliceSliderWidth-10 - VerticalSelectorWidth)            
            resizeWidgetHelper.setWidth (XZSliceAxisSliceTextBox, XZSliceSliderWidth + VerticalSelectorWidth + 1 + 8)            
                                    
            resizeWidgetHelper.setWidgetXPos (XZSliceView, 0)            
            XZSliceViewWidth = XZSliceView.width () + 1
            resizeWidgetHelper.setWidgetXPos (XZSliceSlider, XZSliceViewWidth)
            resizeWidgetHelper.setWidgetXPos (XZSliceVerticalSelector, XZSliceViewWidth  + XZSliceSliderWidth+3)            
            resizeWidgetHelper.setWidgetXPos (XZSliceAxisSliceTextBox, XZSliceViewWidth)                            
            RCC_ContourWindow._resizeViewScrollbars (XZSliceView, XZ_VerticalZoomScrollBar, XZ_HorizontalZoomScrollBar, resizeWidgetHelper)            
        except:
            pass
            
    def _XZFrameResize (self, position) :
        RCC_ContourWindow._resizeOffAxisFrame (self.resizeWidgetHelper, self.ui.XZSliceAxisSliceTextBox, self.ui.XZSliceView, self.ui.XZSliceSlider, 
                                               self.ui.XZSliceVerticalSelector, self.ui.XZ_HorizontalZoomScrollBar, self.ui.XZ_VerticalZoomScrollBar, self.ui.XZFrame)
        
                
    
    def _YZFrameResize (self, position) :
        RCC_ContourWindow._resizeOffAxisFrame (self.resizeWidgetHelper, self.ui.YZSliceAxisSliceTextBox, self.ui.YZSliceView, self.ui.YZSliceSlider, 
                                               self.ui.YZSliceVerticalSelector, self.ui.YZ_HorizontalZoomScrollBar, self.ui.YZ_VerticalZoomScrollBar, self.ui.YZFrame)
    
    def _ToolFrameResize (self, position) :
        try:
            self.resizeWidgetHelper.setWidgetXPos (self.ui.SliceROISplitter, 0)
            self.resizeWidgetHelper.setWidgetYPos (self.ui.SliceROISplitter, 0)
            self.resizeWidgetHelper.setHeight (self.ui.SliceROISplitter, self.ui.ToolFrame.height())
            self.resizeWidgetHelper.setWidth (self.ui.SliceROISplitter,  self.ui.ToolFrame.width())
        except:
            pass
 
        
    def _SliceFrameResizeEvent (self, position) :
        try:
            self.resizeWidgetHelper.setWidgetYPos (self.ui.SliceFrame, 0)
            self.resizeWidgetHelper.setHeight (self.ui.SliceFrame, self.ui.ToolFrame.height ())  
            self.resizeWidgetHelper.setWidgetYPos (self.ui.Slicelbl, 10)
            self.resizeWidgetHelper.setWidgetYPos (self.ui.MajorAxisSliceTextBox, 10)
            self.resizeWidgetHelper.setWidgetYPos (self.ui.scanPhaseLbl, 10)
            self.resizeWidgetHelper.setWidgetYPos (self.ui.ScanPhaseComboBox, 5)      
            self.resizeWidgetHelper.setWidgetXPos (self.ui.Slicelbl, 10)
            self.resizeWidgetHelper.setWidgetXPos (self.ui.MajorAxisSliceTextBox, 70)
            self.resizeWidgetHelper.setWidgetXPos (self.ui.scanPhaseLbl, 190)
            
            widthOffset = (self.ui.SliceFrame.width () - 550)
            if widthOffset > 0 :
                self.resizeWidgetHelper.setWidgetXPos (self.ui.scanPhaseLbl, 190)
                self.resizeWidgetHelper.setWidgetXPos (self.ui.ScanPhaseComboBox, 280) 
                self.resizeWidgetHelper.setWidth (self.ui.ScanPhaseComboBox, 261 + widthOffset) 
                self.resizeWidgetHelper.setWidth (self.ui.MajorAxisSliceTextBox, 113) 
            else:
                lefthalf = int (widthOffset/2)
                righthalf = widthOffset - lefthalf
                self.resizeWidgetHelper.setWidth (self.ui.MajorAxisSliceTextBox, 113 + lefthalf) 
                self.resizeWidgetHelper.setWidgetXPos (self.ui.scanPhaseLbl, 190 + lefthalf)
                self.resizeWidgetHelper.setWidgetXPos (self.ui.ScanPhaseComboBox, 280 + lefthalf) 
                self.resizeWidgetHelper.setWidth (self.ui.ScanPhaseComboBox, 261 + righthalf) 
            
            heightOffset = (self.ui.SliceFrame.height () - 231)        
            self.resizeWidgetHelper.setWidgetYPos (self.ui.XYSliceSelectorWidget, 40)
            self.resizeWidgetHelper.setWidgetXPos (self.ui.XYSliceSelectorWidget, 10)
            self.resizeWidgetHelper.setWidth (self.ui.XYSliceSelectorWidget, 531 + widthOffset)
            self.resizeWidgetHelper.setHeight (self.ui.XYSliceSelectorWidget, heightOffset +131 )   
            self.resizeWidgetHelper.setWidgetYPos (self.ui.contourSingleSliceBtn, heightOffset + 180)        
            self.resizeWidgetHelper.setWidgetYPos (self.ui.autoContourBtn, heightOffset + 180)                         
            self.resizeWidgetHelper.setWidgetXPos (self.ui.contourSingleSliceBtn, 20)        
            self.resizeWidgetHelper.setWidgetXPos (self.ui.autoContourBtn, 150)    
        except:
            pass
        
            
    def _ROIFrameResizeEvent (self, position) :
        try :            
            self.resizeWidgetHelper.setWidgetYPos (self.ui.ROIFrame, 0)
            self.resizeWidgetHelper.setHeight (self.ui.ROIFrame, self.ui.ToolFrame.height ())   
            self.resizeWidgetHelper.setWidgetYPos (self.ui.contourOrientationLbl, 10)
            self.resizeWidgetHelper.setWidgetYPos (self.ui.contourOrientationComboBox, 5)
            self.resizeWidgetHelper.setWidgetYPos (self.ui.ThresholdLbl, 10)
            self.resizeWidgetHelper.setWidgetYPos (self.ui.label_7, 45) 
            self.resizeWidgetHelper.setWidgetYPos (self.ui.SelectedROI, 45)  
            self.resizeWidgetHelper.setWidgetYPos (self.ui.SelectedContourLbl, 45)
            widthOffset = (self.ui.ROIFrame.width () - 491)
            self.resizeWidgetHelper.setWidgetXPos (self.ui.contourOrientationLbl, 10)
            self.resizeWidgetHelper.setWidgetXPos (self.ui.contourOrientationComboBox, 111)
            self.resizeWidgetHelper.setWidgetXPos (self.ui.ThresholdLbl, self.ui.ROIFrame.width () - 161)
            self.resizeWidgetHelper.setWidth (self.ui.contourOrientationComboBox, 211 + widthOffset)  
            self.resizeWidgetHelper.setWidgetYPos (self.ui.ROI_List, 70)
            self.resizeWidgetHelper.setWidgetYPos (self.ui.ContourList, 70) 
            self.resizeWidgetHelper.setWidgetXPos (self.ui.label_7, 10)
            self.resizeWidgetHelper.setWidgetXPos (self.ui.SelectedROI, 58)
        
            heightOffset = (self.ui.ROIFrame.height () - 231)        
            self.resizeWidgetHelper.setWidgetXPos (self.ui.ROI_List, 10)
            
            self.resizeWidgetHelper.setHeight (self.ui.ROI_List, heightOffset + 101)  
            self.resizeWidgetHelper.setHeight (self.ui.ContourList, heightOffset + 101)  
            inPerimeterMode = self.ROIDictionary.isROIinPerimeterMode () or self.ROIDefs.arePointROIDefined ()
            if inPerimeterMode :
                leftHalfWidthOffset = int (widthOffset / 2)
                rightHalfWidthOfffset = widthOffset - leftHalfWidthOffset
                self.resizeWidgetHelper.setWidth (self.ui.ROI_List, 211+ leftHalfWidthOffset)
                self.resizeWidgetHelper.setWidth (self.ui.SelectedROI, 161+ leftHalfWidthOffset)    
                self.resizeWidgetHelper.setWidgetXPos (self.ui.BatchContourBtn, 211 + 19 + leftHalfWidthOffset)  
                self.resizeWidgetHelper.setWidth (self.ui.SelectedContourLbl, 251 + rightHalfWidthOfffset)  
                self.resizeWidgetHelper.setWidgetXPos (self.ui.SelectedContourLbl, 211 + 19 + leftHalfWidthOffset)
                self.resizeWidgetHelper.setWidth (self.ui.ContourList, 251 + rightHalfWidthOfffset)  
                self.resizeWidgetHelper.setWidgetXPos (self.ui.ContourList, 211 + 19 + leftHalfWidthOffset) 
            else:  
                self.resizeWidgetHelper.setWidth (self.ui.ROI_List, 460+ widthOffset)
                self.resizeWidgetHelper.setWidth (self.ui.SelectedROI, 471+ widthOffset)
                self.resizeWidgetHelper.setWidgetXPos (self.ui.BatchContourBtn, 230)  
    
                
            top = heightOffset + 180
            self.resizeWidgetHelper.setWidgetYPos (self.ui.UndoBtn, top)        
            self.resizeWidgetHelper.setWidgetYPos (self.ui.RedoBtn, top)        
            self.resizeWidgetHelper.setWidgetYPos (self.ui.ClearSelectedBtn, top)        
            self.resizeWidgetHelper.setWidgetYPos (self.ui.BatchContourBtn, top)        
            self.resizeWidgetHelper.setWidgetYPos (self.ui.AddContourBtn, top)        
            self.resizeWidgetHelper.setWidgetYPos (self.ui.deleteContourBtn, top)       
            self.resizeWidgetHelper.setWidgetXPos (self.ui.UndoBtn, 130)        
            self.resizeWidgetHelper.setWidgetXPos (self.ui.RedoBtn, 180)        
            self.resizeWidgetHelper.setWidgetXPos (self.ui.ClearSelectedBtn, 10)     
            rightEdge = self.ui.ROIFrame.width ()
            self.resizeWidgetHelper.setWidgetXPos (self.ui.AddContourBtn, rightEdge - 101)        
            self.resizeWidgetHelper.setWidgetXPos (self.ui.deleteContourBtn, rightEdge - 52)  
        except:
            pass
              
    
    def _mainSplitterMoved (self, pos, index) :
        sizelst = self.ui.mainSplitter.sizes ()
        if sizelst[1] == 0 :            
            self.setMinimumSize(QtCore.QSize(550, 550))        
        else:
            self.setMinimumSize(QtCore.QSize(920, 800))                    
            
        
    # Window resize event, resizes main window objects proportionally to reflect current window size
    def _internalWindowResize (self, newsize):            
        try:                
            if (self.resizeWidgetHelper is not None) :                                         
                if self.height () < 800 or self.width () < 920:                                        
                    if self._autoSnappedClose is None :                        
                        widgetSizeList = self.ui.mainSplitter.sizes ()                    
                        if widgetSizeList[1] > 0 :
                            self._autoSnappedClose = widgetSizeList
                            self.ui.mainSplitter.setSizes ([self.height()-10,0])                                                    
                else :
                     if self._autoSnappedClose is not None :
                         self.ui.mainSplitter.setSizes (self._autoSnappedClose)
                         self._autoSnappedClose = None      
                self.resizeWidgetHelper.setAbsoluteWidth (self.ui.mainSplitter, newsize)
                self.resizeWidgetHelper.setAbsoluteHeight (self.ui.mainSplitter, newsize)                                                                                                                  
        except:
            pass
            
    
    def resizeEvent (self, qresizeEvent) :                       
        QMainWindow.resizeEvent (self, qresizeEvent) 
        self._internalWindowResize ( qresizeEvent.size () )               
 
    ## ------------------------------------
    ##   Start Dataset Tag Editor Window
    
    #self.ui.actionCustom_Tags.triggered.connect (self.ShowCustomDatasetTagEditor)        
    #self.ui.actionInternal_Tags.triggered.connect (self.ShowInternalDatasetTags)        
    def getOpenCustomDatasetTagEditor (self) :
        try :            
            return self._customDatasetTagEditor
        except:
            return None
            
    def getOpenInternalDatasetTagEditor (self) :
        try :            
            return self._internalDatasetTagEditor
        except:
            return None
            
    def closeDatasetTagEditorWindow (self) :
        try :   
            self._customDatasetTagEditor = self.getOpenCustomDatasetTagEditor ()
            if (self._customDatasetTagEditor is not None) :
                self._customDatasetTagEditor.close ()                
        except:
            print ("A Exception occured closing the custom dataset tag editor")
        try :            
            self._internalDatasetTagEditor = self.getOpenInternalDatasetTagEditor ()
            if (self._internalDatasetTagEditor is not None) :
                self._internalDatasetTagEditor.close ()
            
        except:
            print ("A Exception occured closing the internal dataset tag editor")
        self._customDatasetTagEditor = None
        self._internalDatasetTagEditor = None
    
    def UpdateDatasetTagEditor (self) :
        if self.ROIDictionary is None : 
            return
        datasetTagEditor = self.getOpenCustomDatasetTagEditor ()
        if (datasetTagEditor is not None) :            
            datasetTagEditor.setTags (self.ROIDictionary.getDataFileTagManager (), CustomDataSetTags = True)
            datasetTagEditor.setEnabled (self.NiftiVolumeTreeSelection is not None)            
        
        datasetTagEditor = self.getOpenInternalDatasetTagEditor ()
        if (datasetTagEditor is not None) :            
            datasetTagEditor.setTags (self.ROIDictionary.getDataFileTagManager (), InternalDataSetTags = True)            
            datasetTagEditor.setEnabled (False)
            
    def ShowCustomDatasetTagEditor (self) :      
        if self.ROIDictionary is None : 
            return          
        datasetTagEditor = self.getOpenCustomDatasetTagEditor ()
        if (datasetTagEditor is not None) :
            datasetTagEditor.setTags (self.ROIDictionary.getDataFileTagManager (), CustomDataSetTags = True)
            datasetTagEditor.show ()                        
        else:
            datasetTagEditor = RCC_DatasetTagingDlg (self._ProjectDataset, self)
            datasetTagEditor.setTags (self.ROIDictionary.getDataFileTagManager (), CustomDataSetTags = True)
            datasetTagEditor.show ()   
            self._customDatasetTagEditor = datasetTagEditor
        datasetTagEditor.setEnabled (self.NiftiVolumeTreeSelection is not None)
        datasetTagEditor.raise_ ()   
    
    def ShowInternalDatasetTags (self) :     
        if self.ROIDictionary is None : 
            return           
        datasetTagEditor = self.getOpenInternalDatasetTagEditor ()
        if (datasetTagEditor is not None) :
            datasetTagEditor.setTags (self.ROIDictionary.getDataFileTagManager (), InternalDataSetTags = True)
            datasetTagEditor.show ()                        
        else:
            datasetTagEditor = RCC_DatasetTagingDlg (self._ProjectDataset, self)
            datasetTagEditor.setTags (self.ROIDictionary.getDataFileTagManager (), InternalDataSetTags = True)
            datasetTagEditor.show ()   
            self._internalDatasetTagEditor = datasetTagEditor
        datasetTagEditor.setEnabled (self.NiftiVolumeTreeSelection is not None)
        datasetTagEditor.ui.tableView.setEditTriggers(QAbstractItemView.NoEditTriggers)
        datasetTagEditor.raise_ ()   
    
    def getWindowTagManger  (self) :
        if self.ROIDictionary is None : 
            return
        return self.ROIDictionary.getDataFileTagManager ()

    ##   End Dataset Tag Editor Window
    ## ------------------------------------

       
    def ROI3DVolumeViewer (self):   
        if self.ROIDictionary is None : 
            return             
        if self.ROIDefs.isROISelected ()  :    
               nameList =self.ROIDefs.getSelectedROI ()
               objLst = []
               for itemName in nameList :        
                   obj = self.ROIDictionary.getROIObject (itemName)
                   if obj is not None and obj.hasSlices () :
                       objLst.append (obj)
               if len (objLst) > 0 :                       
                   try:
                       viewer = VolumeViewerUI(self)         
                       if (viewer.setModel (objLst,self.ROIDefs, self.NiftiVolume.getSliceDimensions (), self.NiftiVolume.getNormalizedVoxelSize (), self, self._ProjectDataset.getApp (), self.ui.XYSliceSelectorWidget.getSelection ())):
                           viewer.setModal (True)                       
                           viewer.exec_ () 
                   except:
                       if opengl_imported :
                           MessageBoxUtil.showMessage ("ROI Visualization Error", "A unxpected error occured launching the ROI volume visualizer.")
                       else:
                           MessageBoxUtil.showMessage ("ROI Visualization Error","To enable ROI volume visualization 'pip install pyopengl'.")
                       self.ui.actionOpen_3D_Viewer_2.setEnabled(False)
    
    def UndoBtnClk (self) :
         if self.ROIDictionary is None : 
            return
         self.setEnabled (False)     
         isSelection = self.isSelectionUIState ()           
         uiState = self.getUIState ()
         self.ROIDictionary.undo ()         
         if not self.isDeepGrowXYUIState () :
             if (isSelection) : 
                 self.setSelectionMode ()
             else:
                 if (uiState not in ["Zoom In", "Zoom Out"]) :
                     try:
                         self._getPaintToolsDlg ().setUIStateWidgetState (uiState)
                     except:
                         self.setContourMode ()
                 else:
                     self.setContourMode ()
         self.setEnabled (True)     
         #print ("undo.repaint")
         #self.update ()
         self.repaint ()
         
         
    def RedoBtnClk (self) :
         if self.ROIDictionary is None : 
            return
         self.setEnabled (False)       
         isSelection = self.isSelectionUIState ()     
         uiState = self.getUIState ()
         self.ROIDictionary.redo ()      
         if not self.isDeepGrowXYUIState () :
             if (isSelection) : 
                 self.setSelectionMode ()
             else:
                 if (uiState not in ["Zoom In", "Zoom Out"]) :
                     try:
                         self._getPaintToolsDlg ().setUIStateWidgetState (uiState)
                     except:
                         self.setContourMode ()
                 else:
                     self.setContourMode ()
         self.setEnabled (True)     
         #print ("redo.repaint")
         #self.update ()
         self.repaint ()
   
    def setContourMode (self) :
        self._getPaintToolsDlg()._paintToolClick ()
        

    def setSelectionMode (self) :        
        self._getPaintToolsDlg()._selectionToolClick ()
    
    def setDeepGrowMode (self) :        
        self._getPaintToolsDlg()._deepGrowToolClick ()
        
    def isZoomOutXYUIState (self):        
         return self.getUIState() == "Zoom Out"    
     
    def isZoomInXYUIState (self):        
         return self.getUIState() == "Zoom In"    

    def isContourXYUIState (self):
        return self.getUIState() == "Contour"     
        
    def isDeepGrowXYUIState (self) :
        return self.getUIState() == "Contour" and self.getPenSettings().getPenTool () == "DeepGrow"
    
    def isSelectionUIState (self):
        return self.getUIState() == "SelectionArrow" 
    
    def getUIState (self) :
        return self._XYUIState
    
    def setUIState (self, val):              
        self._XYUIState = val
        
        
    def clearUIState (self) :
        self.setUIState ("None")        
    
    def BatchCountourBtn (self) :  
        if self.ROIDictionary is None : 
            return
        try:
            self.setEnabled (False)
            startSlice = self.SelectedCoordinate.getZCoordinate ()
            selectedROIList = []
            for roiName in self.ROIDefs.getSelectedROI () :
                if not self.ROIDefs.isROIHidden (roiName) and not self.ROIDefs.isROILocked (roiName) :         
                    selectedROIList.append (roiName)
            if len (selectedROIList) <= 0 :
                return
            if self.showFlattenDeepGrowDlg ("One-click generated segmentations must be flattened before an area ROI can be batch-contoured. Do you wish to flatten the segmentation?") :
                dlg = BatchContourUIDlg (self._ProjectDataset.getApp (), self, self.SelectedCoordinate, selectedROIList)        
                dlg.exec_ ()
                if (dlg.getReturnResult () == "OK") :
                    mergeDictionary = dlg.getDialogROIDictionary ()
                    roiList = mergeDictionary.getDefinedROINames ()             
                    volumeShape = (self.NiftiVolume.getXDimSliceCount () - 1, self.NiftiVolume.getYDimSliceCount () - 1, self.NiftiVolume.getZDimSliceCount () -1)
                    SaveUndoPoint = True
                    cancled = False
                    undoCount = 0
                    for ROIName in roiList :
                        obj = mergeDictionary.getROIObject (ROIName)
                        undoCount += 1
                        contourIDLst, firstslice, lastslice = self.ROIDictionary._mergeROI  (ROIName, obj, volumeShape, SaveUndoPoint)                            
                        firstslice = max (0, firstslice - 2)
                        lastslice = min (self.NiftiVolume.getZDimSliceCount () -1, lastslice + 2)
                        if (len (contourIDLst) > 0) :
                            cancled = self.autoContourBtn (ROIName, contourIDLst, firstslice, lastslice, startSlice, AutoContourHumanSlicesOnce = True)
                            if (cancled) : 
                                break
                        SaveUndoPoint = False
                    if (cancled) :
                        for ct in range (undoCount) :
                            self.ROIDictionary.undo ()
                    
                    # updates currently selected coordinates
                    if (len (selectedROIList) == 1):
                       name = selectedROIList[0]
                       if self.ROIDictionary.isROIDefined (name) : 
                            roiobj = self.ROIDictionary.getROIObject (name)        
                            self.ROIDictionary.setSelected (roiobj)        
                            if (not roiobj.isROIArea () or roiobj.hasSlices ()) :                   
                                cord = roiobj.getCoordinate ()
                                if (cord.getZCoordinate () != -1) :
                                    self.SelectedCoordinate.setCoordinate ([cord.getXCoordinate (), cord.getYCoordinate (), cord.getZCoordinate ()])                                
                    
                    self.ROIDictionary._CallDictionaryChangedListner  ()                                      
                del dlg      
        finally:
            self.setEnabled (True)                         
        
    def ShowStatsDlg (self) :
        if self.ROIDictionary is None : 
            return
        if (self._StatsDlg is None) :
            self._StatsDlg = StatsDlg (self)        
            self._StatsDlg.installEventFilter(self._ChildWidgetOverrideEventFilter)
        if (not self._StatsDlg.isVisible ()) :
            self._StatsDlg.show ()                    
            self.ROIDictionary._CallDictionaryChangedListner  ()                                      
        self._StatsDlg.raise_()
        
    def CreateNewViewingWindow (self) :
        print ("Create New Window")
        self._ProjectDataset.createContouringWindow ()
        
        
    def HounsfieldVisualizationDlg (self):                    
         if (self._hounsfeildDlg == None) :
             self._hounsfeildDlg = RCC_HounsfieldVisOptionsDlg (self)             
         self._hounsfeildDlg.updateHounsfeildDlgSettings (self.hounsfieldVisSettings)
         self._hounsfeildDlg.show ()
         self._hounsfeildDlg.raise_()

    def SetVisSettingsAction (self) :
        if (self._visOptionsDlg == None) :
            self._visOptionsDlg = RCC_VisOptionsDlg (self, self.visSettings)
        self._visOptionsDlg.show ()  
        self._visOptionsDlg.raise_ ()  
         
    # Test code to export masks volume 
    def exportMaskVolume (self) :
        if self.ROIDictionary is None : 
            return
        ROI_FileSystemPath = ""
        PesscaraContext  = ""
        if (self.NiftiVolumeTreeSelection is not None) :
            ROI_FileSystemPath = self.NiftiVolumeTreeSelection.getExportMaskFilesystemPath  ()                                             
            if (self.NiftiVolumeTreeSelection.getPesscaraInterace () != None) :
                PesscaraContext = self.NiftiVolumeTreeSelection.getPesscaraContex ()
        
        try :
            fileName = self.NiftiVolume.getFileName ()
            stripLongFName = LongFileNameAPI.getLongFileNamePath (fileName)
            _,fileName = os.path.split (fileName)
            _,stripLongFName = os.path.split (stripLongFName)
            EnableTryLongFileNameExport = fileName != stripLongFName
        except:
            EnableTryLongFileNameExport = False
        dlg = RCC_ROIVolumeMaskExportDlg (self, self.ROIDictionary.getROIDefs (), PesscaraContext = PesscaraContext, ROI_FileSystemPath = ROI_FileSystemPath, ProjectDataset= self._ProjectDataset, DisableAppendButton = True, EnableTryLongFileNameExport = EnableTryLongFileNameExport)
        dlg.exec_ ()
        if (dlg._result != "Export"):
            return
            
        objLst = []
        for roiNames in dlg._SelectedROILst :
            if (self.ROIDictionary.isROIDefined (roiNames)) :
                obj = self.ROIDictionary.getROIObject (roiNames)
                objLst.append (obj)
        if (len (objLst) == 0) :                                    
            print ("ERROR: None of the selected ROI are defined for the object no mask file written out")                
            return
        
        if self.showFlattenDeepGrowDlg ("One-click generated segmentations must be flattened before a series mask can be exported. Do you wish to flatten the segmentation?") :     
            if (dlg._ExportDEST == "FileSystem") :                    
                writepath = os.path.join (dlg._ExportPath, dlg._ExportName)
                
                ExportDestNiftiFileName = None
                if dlg._TryExportLongFileNames :
                    try :                    
                        fileName = self.NiftiVolume.getFileName ()                    
                        stripLongFName = LongFileNameAPI.getLongFileNamePath (fileName)
                        if (stripLongFName != fileName) :
                            exportDir, _ = os.path.split (writepath)
                            _, extension = FileUtil.removeExtension (fileName, [".nii", ".nii.gz"])                        
    
                            _, stripLongFName = os.path.split (stripLongFName)
                            stripLongFName, _ = FileUtil.removeExtension (stripLongFName, [".nii", ".nii.gz"])                        
                            stripLongFName.strip ()
                            if len (stripLongFName) > 0 :                            
                                stripLongFName += extension                        
                                if fileName != stripLongFName :
                                   testFname = os.path.join (exportDir, stripLongFName)
                                   with open (testFname,"wt") as _ :
                                       ExportDestNiftiFileName = stripLongFName
                                   os.remove (testFname)                                
                    except:
                        ExportDestNiftiFileName = None
                    
                try :                    
                    self.NiftiVolume.writeMaskVolume (writepath, objLst, MaskValue = None, InSlicePointSize = dlg._pointInSliceSize, CrossSlicePointSize = dlg._pointCrossSliceSize, RoiDefs = self.ROIDictionary.getROIDefs (), BinaryOrMask = not self.ROIDictionary.getROIDefs ().hasSingleROIPerVoxel (), ExportNiftiFile = dlg._ExportNiftiFile, ExportNiftiMaskfileDescription = True, roiDictionary = self.ROIDictionary, ExportAreaROIUsingBoundingVolumeDefinition = dlg._ExportROIMaskAsBoundingBox, OverwriteFiles = dlg._OverwriteExistingFiles, ExportDestNiftiFileName = ExportDestNiftiFileName)
                except :
                    print ("Error could not save volume mask to file system")            
                                
            elif (dlg._ExportDEST == "PESSCARA") :            
                try:
                    temppath = tempfile.mkdtemp ()
                    try :                                                   
                        tempNiftiFileName = os.path.join (temppath, dlg._ExportName)
                        try:
                            self.NiftiVolume.writeMaskVolume (tempNiftiFileName, objLst, MaskValue = None, InSlicePointSize = dlg._pointInSliceSize, CrossSlicePointSize = dlg._pointCrossSliceSize, RoiDefs =  self.ROIDictionary.getROIDefs (), BinaryOrMask = not self.ROIDictionary.getROIDefs ().hasSingleROIPerVoxel (), ExportAreaROIUsingBoundingVolumeDefinition = dlg._ExportROIMaskAsBoundingBox)
                            Series = self.NiftiVolumeTreeSelection.getSeries ()
                            self.NiftiVolumeTreeSelection.getPesscaraInterace ().setSeriesROIMask (tempNiftiFileName, Series, dlg._ExportPesscaraContext)
                            os.remove (tempNiftiFileName)
                        except:
                            try:
                                os.remove (tempNiftiFileName)
                            except Exception as exceptional:
                                print (str (exceptional))
                    finally:
                        os.rmdir (temppath)
                except:
                    print ("Error could not save volume mask to pesscara")            
                    
            else:                    
                print ("Error unknown mask export destination.")            
            
            self._updatePesscaMaskViewDlg ()


   
        
        
        
    def ShowPesscaraMaskViewerDlg (self):
        if (self._PesscaraMaskViewerDlg == None) :
            self._PesscaraMaskViewerDlg = PesscaraMaskViewerDlg (self)
        self._PesscaraMaskViewerDlg.show ()
        self._updatePesscaMaskViewDlg ()
        self._PesscaraMaskViewerDlg.raise_ ()
        
        
    def updatePesscaraVolumeMask (self, maskContextName) :
        if (maskContextName != None and self.NiftiVolume != None) :        
            if (self.NiftiVolumeTreeSelection is not None): # item selected
                if (self.NiftiVolumeTreeSelection.getPesscaraInterace () != None ): #item has a pesscara interface
                    series = self.NiftiVolumeTreeSelection.getSeries () # has series
                    if (series != None) :
                        try:
                            volumeMask = PesscaraDF (self.NiftiVolumeTreeSelection.getPesscaraInterace (), self.NiftiVolumeTreeSelection, context=maskContextName)
                            niftiVolumeMask = NiftiVolumeData (nib.load (volumeMask.getFilePath ()), MatchOrientation = self.NiftiVolume)                                        
                            if (niftiVolumeMask.getSliceDimensions () == self.NiftiVolume.getSliceDimensions () ) :
                                self.ui.XYSliceView.setVolumeMask (niftiVolumeMask)
                                self.ui.YZSliceView.setVolumeMask (niftiVolumeMask)
                                self.ui.XZSliceView.setVolumeMask (niftiVolumeMask)
                                self._setPreviewSegmentationMaskPath (None)
                                self.ui.XYSliceSelectorWidget.setSliceColorMask (None, None)
                                self.ui.XZSliceVerticalSelector.setSliceColorMask (None, None)
                                self.ui.YZSliceVerticalSelector.setSliceColorMask (None, None)
                                self.ui.XYSliceVerticalSelector.setSliceColorMask (None, None)
                                return
                            else:
                                print ("Cannot overlay mask, dimensions of mask and target nifti do not match")
                        except:                        
                            print ("Could not retrieve mask from pesscara.")                    
        self.ui.XYSliceView.setVolumeMask (None)
        self.ui.YZSliceView.setVolumeMask (None)
        self.ui.XZSliceView.setVolumeMask (None)
        self._setPreviewSegmentationMaskPath (None)
        self.ui.XYSliceSelectorWidget.setSliceColorMask (None, None)
        self.ui.XZSliceVerticalSelector.setSliceColorMask (None, None)
        self.ui.YZSliceVerticalSelector.setSliceColorMask (None, None)
        self.ui.XYSliceVerticalSelector.setSliceColorMask (None, None)

    def exportVolumeCSV (self) :
        if (self.NiftiVolume is not None and self.ROIDictionary is not None) :             
            roiDefs = self.ROIDictionary.getROIDefs ()            
            exportPath, pointInSlice, pointCrossSlice, exportROIList, CombineNonTouchingROI= ExportVolumeStatsCSV.exportVolumeCSVUI (self, roiDefs) 
            descriptionList = self._ProjectDataset.getScanPhaseList () 
            if (exportPath != "") :
                if self.showFlattenDeepGrowDlg ("One-click generated segmentations must be flattened before a volume csv can be generated. Do you wish to flatten the segmentation?") :
                    try :
                        returnData = ExportVolumeStatsCSV.getDatasetExportData (descriptionList, self.NiftiVolume , self.ROIDictionary, exportROIList, pointInSlice, pointCrossSlice, CombineNonTouchingROI= CombineNonTouchingROI)                    
                        DatasetList = [returnData]
                                        
                        progdialog = QProgressDialog("Generating Table", "Cancel", int (0), int (0),self)
                        progdialog.setMinimumDuration (0)
                        progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)                
                        progdialog.setWindowTitle ("Exporting Tables")
                        progdialog.setWindowModality(QtCore.Qt.ApplicationModal)        
                        try:
                            progdialog.forceShow()    
                            progdialog.setValue(0)
                            self._ProjectDataset.getApp ().processEvents ()                                                    
                            exportTable = ExportVolumeStatsCSV.getDatasetTable (DatasetList)
                            if (exportTable is not None) :
                                pd = exportTable.convertTableToPandasDataFrame()
                                if (exportPath.lower().endswith (".xlsx")):
                                    pd.to_excel (exportPath,index=False,header=False)
                                else:
                                    pd.to_csv (exportPath,index=False,header=False)
                        finally:
                            progdialog.close ()
                            self._ProjectDataset.getApp ().processEvents ()                        
                    except Exception as excpt:
                        print ("An error occured exporting.\n\n" + str(excpt))
    

    def exportVolumeTagsDlg (self) :
        if (self.NiftiVolume is not None and self.ROIDictionary is not None and self.NiftiVolumeTreeSelection is not None) :   
            tagManager =  self.getWindowTagManger ()
            ShowRegistrationColumnTagIndicator = False # self.isLoadingROIFromFileSystem ()
            exportTagsDlg = RCC_ExportTagsDlg (self, self._ProjectDataset.getNIfTIDatasetInterface ().getUIHeader(), tagManager, self._ProjectDataset, ShowRegistrationTag = ShowRegistrationColumnTagIndicator, LimitToSeriesWithTagsEnabled = False)
            if (QDialog.Accepted == exportTagsDlg.exec_ ()) :            
                exportTagList = exportTagsDlg.getExportTagList ()
                if (len (exportTagList) > 0) :
                    dlg= QFileDialog( None )
                    dlg.setWindowTitle( 'Select CSV file to save project tags to' )
                    dlg.setViewMode( QFileDialog.Detail )    
                    dlg.setFileMode (QFileDialog.AnyFile)
                    dlg.setNameFilters( ['csv file (*.csv)', 'excel file (*.xlsx)', 'any file (*)'] )                
                    dlg.setAcceptMode (QFileDialog.AcceptSave)
                    if dlg.exec_() and (len (dlg.selectedFiles())== 1):
                        exportPath = dlg.selectedFiles()[0]                                                    
                        if ("xlsx" in dlg.selectedNameFilter ()) :
                            if not exportPath.lower ().endswith (".xlsx") :
                                exportPath += ".xlsx"
                        elif not exportPath.lower ().endswith (".csv") :
                            exportPath += ".csv"
                                    
                        progdialog = QProgressDialog("Generating Table", "Cancel", int (0), int (0),self)
                        progdialog.setMinimumDuration (0)
                        progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)                         
                        progdialog.setWindowTitle ("Exporting Tables")
                        progdialog.setWindowModality(QtCore.Qt.ApplicationModal)        
                        try :
                            progdialog.forceShow()    
                            progdialog.setValue(0)
                            self._ProjectDataset.getApp ().processEvents ()
                        
                            
                            DatasetList = []
                            tagManager = self.ROIDictionary.getDataFileTagManager ()
                            outputLst = []
                            output = self.NiftiVolumeTreeSelection.getTreePath ()
                            outputLst.append (output)
                            for exportTag in exportTagList :
                                txt = ""
                                if (exportTag.getType () == "Description"):
                                    txt = self.ROIDictionary.getScanPhaseTxt ()                                        
                                elif(exportTag.getType () == "Tag"): 
                                    tagidentifer = exportTag.getIdentifer ()
                                    tag = tagManager.getTagByIdentifer (tagidentifer)                        
                                    if (tag is not None) :
                                        txt = tag.getTextValue ()                                                
                                    else:
                                        txt = "[Value_Not_Set]"
                                else:
                                    print ("Export Tag Type Not implemented: " + exportTag.getType ())
                                    txt = ""            
                                outputLst.append (txt)
                            DatasetList.append (outputLst)
                            
                            exportTable = PandasCompatibleTable ()
                            exportTable.insertDataVector (0,0, ["Generated", DateUtil.dateToString (DateUtil.today()), TimeUtil.timeToString (TimeUtil.getTimeNow ())  ])                        
                            headerVec = ["Data Source"]                        
                            for tag in exportTagList :
                                try:
                                    tagName = tag.getName ()
                                except:
                                    tagName = "Unknown"
                                headerVec.append (tagName)                            
                            exportTable.insertDataVector (2,0,headerVec)
                            for row, datavector in enumerate(DatasetList) :
                                exportTable.insertDataVector (row+3, 0, datavector)
                            if (exportTable.getRowCount () > 0) :
                                pd = exportTable.convertTableToPandasDataFrame()
                                if (exportPath.lower().endswith (".xlsx")):
                                    pd.to_excel (exportPath,index=False,header=False)
                                else:
                                    pd.to_csv (exportPath,index=False,header=False)                            
                        finally:
                            progdialog.close ()
                            self._ProjectDataset.getApp ().processEvents ()
            
            
         
                    
    def showANTSRegistrationLog (self) :
        if self.ROIDictionary is None : 
            return
        dlg = RCC_RegistrationLogViewerDlg (self, self.ROIDictionary.getANTSRegistrationLog ())
        dlg.exec_ ()
        dlg = 0

    def showTranslationDlg (self) :
        if (self._TranslationDlg is None) :
            self._TranslationDlg = RCC_RoiTranslationDlg (self)            
        self._TranslationDlg.setEnabled (not self.ui.XYSliceView.areAxisProjectionControlsEnabledForAxis ())
        self._TranslationDlg.show ()
        self._TranslationDlg.raise_()
    
    def updateTranslationDlg (self) :
        if (self._TranslationDlg is not None) :
            self._TranslationDlg.setEnabled (not self.ui.XYSliceView.areAxisProjectionControlsEnabledForAxis ())
            self._TranslationDlg.updateScrollbarUI ()
    
    def closeTranslationDlg (self) :
        if (self._TranslationDlg is not None) :
            self._TranslationDlg.closeWindow ()
            self._TranslationDlg = None


    
    def updateMLDatasetDescriptionDlg (self) :
        if (self._MLDatasetDescriptionDlg is not None) :
          self._MLDatasetDescriptionDlg.intWindowSettings (self)
    
    def closeMLDatasetDescriptionDlg (self) :
        if (self._MLDatasetDescriptionDlg is not None) :
            self._MLDatasetDescriptionDlg.closeWindow ()
            self._MLDatasetDescriptionDlg = None
    
    def showPaintToolsDlg (self) :
        dlg = self._getPaintToolsDlg()
        self.updatePenSettingsDlg ()                        
        if (not self._showHiddenDlg (dlg, self._HiddenDlgMem)) :
            dlg.show ()
        dlg.raise_ ()                                

            
    def showPenSettingsDlg (self):        
        if (self._PenSettingsDlg is None) :
            self._PenSettingsDlg = RCC_PenSettingsDlg (self.getPenSettings (),self)            
        self.updatePenSettingsDlg ()
        self._PenSettingsDlg.updateToReflectActiveROI()
        self._PenSettingsDlg.show () 
        self._PenSettingsDlg.raise_ ()
        
    def updatePenSettingsDlg (self) :
        if self.ROIDictionary is None : 
            return
        if (self._PenSettingsDlg is not None) :
            if (self.NiftiVolume is None or self.ROIDictionary is None) :
                self._PenSettingsDlg.setEnabled (False)        
            elif not self.isROIDictonaryFileLoaded () or self.ROIDictionary.isReadOnly ():
                self._PenSettingsDlg.setEnabled (False)                
            elif (self.ROIDictionary.isROIinPerimeterMode ()) :
                self._PenSettingsDlg.setEnabled (False)                
            else:        
                self._PenSettingsDlg.setEnabled (True)                
                self._PenSettingsDlg.updatePenSettingsDlgThresholdRange (self.NiftiVolume)
                self._PenSettingsDlg.updatePenSettingsDrawEraseSetting ()
            
        if (self.NiftiVolume is None or self.ROIDictionary is None or not self.isROIDictonaryFileLoaded () or self.ROIDictionary.isROIinPerimeterMode () or  self.ROIDictionary.isReadOnly ()) :
            self._getPaintToolsDlg ().setButtonEnabledState (EraseandFill = False)        
        else:                    
            self._getPaintToolsDlg ().setButtonEnabledState (EraseandFill = True)
            
    def closePenSettingsDlg (self):
        if (self._PenSettingsDlg is not None) :
            self._PenSettingsDlg.close () 
            self._PenSettingsDlg = None
    
    def closePenToolsDlg (self):
        if (self._PaintToolsDlg is not None) :
            self._PaintToolsDlg.close () 
            self._PaintToolsDlg = None
    
    def setVoxelAnnotationMenuSettings (self) :
        if (self.NiftiVolume is None or self.ROIDictionary is None or not self.isROIDictonaryFileLoaded () or self.ROIDictionary.isReadOnly ()) :
            self.ui.actionContour.setChecked (False)
            self.ui.actionContour.setEnabled (False)
            self.ui.actionPaint.setChecked (False)
            self.ui.actionPaint.setEnabled (False)
        else:
            self.ui.actionContour.setEnabled (True)
            self.ui.actionPaint.setEnabled (True)
            self.ui.actionContour.setChecked (self.ROIDictionary.isROIinPerimeterMode ()) 
            self.ui.actionPaint.setChecked (self.ROIDictionary.isROIinAreaMode ()) 
    
    def setShowWindow (self) :
        if (self.NiftiVolume is None or self.ROIDictionary is None or not self.isROIDictonaryFileLoaded () or self.ROIDictionary.isReadOnly ()) :
            self.ui.actionPen_Settings_window.setEnabled (False)            
        elif (self.ROIDictionary.isROIinPerimeterMode ()) :
            self.ui.actionPen_Settings_window.setEnabled (False)            
        else:
            self.ui.actionPen_Settings_window.setEnabled (True)
        try:
            selected = self.NiftiVolume is not None and self.isROIDictonaryFileLoaded () 
            self.ui.actionCustom_Tags.setEnabled (selected)
            selected = selected and self.ROIDefs.isROISelected ()
            self.ui.actionOpen_3D_Viewer_2.setEnabled (selected)
            self.ui.actionShow_ROI_Statistics.setEnabled (selected)
            self.ui.actionROI_Translation.setEnabled (selected and not self.ROIDictionary.isReadOnly () and not self.ui.XYSliceView.areAxisProjectionControlsEnabledForAxis ())            
        except:
            pass
            
    def updateUIToReflectAreaPerimeterDrawing (self) :        
        if (self.ROIDictionary is not None):
            inPerimeterMode = self.ROIDictionary.isROIinPerimeterMode () or self.ROIDefs.arePointROIDefined ()
            self.ui.SelectedContourLbl.setHidden (not inPerimeterMode)
            self.ui.ContourList.setHidden (not inPerimeterMode)
            self.ui.AddContourBtn.setHidden (not inPerimeterMode)
            self.ui.deleteContourBtn.setHidden (not inPerimeterMode)
            widthOffset = (self.ui.ROIFrame.width () - 491)
            inPerimeterMode = self.ROIDictionary.isROIinPerimeterMode () or self.ROIDefs.arePointROIDefined ()
            if inPerimeterMode :
                leftHalfWidthOffset = int (widthOffset / 2)
                rightHalfWidthOfffset = widthOffset - leftHalfWidthOffset
                self.resizeWidgetHelper.setWidth (self.ui.ROI_List, 211+ leftHalfWidthOffset)
                self.resizeWidgetHelper.setWidth (self.ui.SelectedROI, 161+ leftHalfWidthOffset)    
                self.resizeWidgetHelper.setWidgetXPos (self.ui.BatchContourBtn, 211 + 19 + leftHalfWidthOffset)
                self.resizeWidgetHelper.setWidth (self.ui.SelectedContourLbl, 251 + rightHalfWidthOfffset)  
                self.resizeWidgetHelper.setWidgetXPos (self.ui.SelectedContourLbl, 211 + 19 + leftHalfWidthOffset)
                self.resizeWidgetHelper.setWidth (self.ui.ContourList, 251 + rightHalfWidthOfffset)  
                self.resizeWidgetHelper.setWidgetXPos (self.ui.ContourList, 211 + 19 + leftHalfWidthOffset)
            else:  
                self.resizeWidgetHelper.setWidth (self.ui.ROI_List, 460+ widthOffset)
                self.resizeWidgetHelper.setWidth (self.ui.SelectedROI, 471+ widthOffset)
                self.resizeWidgetHelper.setWidgetXPos (self.ui.BatchContourBtn, 230)  
            try:
                controls = self.ui.XZSliceView.getAxisProjectionControls ()
                if controls is not None :
                    controls.setAxisTransformationEnabled (not self.ROIDictionary.isROIinPerimeterMode ())
                    controls.resetAxisRotation ()
                    self.update ()  
            except:
                pass
            
            
                    
    def convertROIToPerimeterContours (self) :
        if self.ROIDictionary is None : 
            return
        if not self.showFlattenDeepGrowDlg ("One-click generated segmentations must be flattened before the annotation can be converted to perimeter annotations. Do you wish to flatten the segmentation?") : 
            return    
        if (self.NiftiVolume is not None) :            
            progdialog = QProgressDialog(self)
            progdialog.setMinimumDuration (4)
            progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)                
            progdialog.setWindowTitle ("Converting ROI to perimeter definitions.")            
            progdialog.setWindowModality(QtCore.Qt.ApplicationModal)                
            progdialog.setRange(0,0)                
            progdialog.setMinimumWidth(300)
            progdialog.setMinimumHeight(100)
            try :
                progdialog.forceShow()   
                progdialog.setValue(0)          
                self._ProjectDataset.getApp ().processEvents ()
                if self.isDeepGrowXYUIState () :
                    self.setContourMode ()
            
                self.ROIDictionary.setROIPerimeterMode (self.NiftiVolume.getSliceDimensions (), self.ROIDefs, ProgressDialog = progdialog, FileSaveThreadManger = self._ProjectDataset.getROISaveThread (), ProjectDataset = self._ProjectDataset)
            finally:
                progdialog.close ()     
                self._ProjectDataset.getApp ().processEvents ()
        self.updatePenSettingsDlg ()
        self.updatePaintBrushIfNecessary ()
        self.updateUIToReflectAreaPerimeterDrawing ()
       
        
    def convertROIToAreaMask (self) :
        if self.ROIDictionary is None : 
            return
        progdialog = QProgressDialog(self)
        progdialog.setMinimumDuration (4)
        progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)                
        progdialog.setWindowTitle ("Converting ROI to area definitions.")            
        progdialog.setWindowModality(QtCore.Qt.ApplicationModal)                
        progdialog.setRange(0,0)                
        progdialog.setMinimumWidth(300)
        progdialog.setMinimumHeight(100)
        try :
            progdialog.forceShow()          
            progdialog.setValue(0)
            self._ProjectDataset.getApp ().processEvents ()
            self.ROIDictionary.setROIAreaMode (ProgressDialog = progdialog, QApp = self._ProjectDataset.getApp())
        finally:
            progdialog.close ()
            self._ProjectDataset.getApp ().processEvents ()
        self.updatePenSettingsDlg ()
        self.updatePaintBrushIfNecessary ()
        self.updateUIToReflectAreaPerimeterDrawing ()
        
