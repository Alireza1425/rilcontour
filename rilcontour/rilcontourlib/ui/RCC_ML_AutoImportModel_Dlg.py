#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug  4 09:27:01 2018

@author: KennethPhilbrick
"""

import os
from PyQt5 import QtCore
from PyQt5.QtWidgets import  QDialog, QFileDialog
from rilcontourlib.util.rcc_util import ResizeWidgetHelper
from rilcontourlib.ui.qt_ui_autogen.RCC_AutoImportMLModelDlgAutogen import Ui_AutoImportMLModelDlg
from rilcontourlib.machinelearning.ML_AutoImportMLModel import ML_AutoImportMLModel
        
class RCC_AutoImportModelDlg (QDialog) :
    
    def __init__ (self, ProjectDataset, treeModel, parent) :                
        self._parent = parent
        self._parentMLTreeModel = treeModel
        self._ProjectDataset = ProjectDataset
        QDialog.__init__ (self, parent)
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)
        self.ui = Ui_AutoImportMLModelDlg ()        
        self.ui.setupUi (self)                
        
        self._autoImportMLModels = ML_AutoImportMLModel (ProjectDataset)
        
        self._resizeWidgetHelper = ResizeWidgetHelper ()
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.CloseBtn)        
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.line2)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.autoImportLogGroupBox)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.ResetAutoImportLog)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.AutoImportLogText)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.RunAutoImportNowBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.AutoImportLastRunLbl)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.directorySelGroupBox)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.selDirectoryBtn)                
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.directorytxt)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.line1)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.AutoImportMLModelEnabledBtn)
        self._updateDialog ()
        
        self.ui.CloseBtn.clicked.connect (self._CloseBtn)
        self.ui.ResetAutoImportLog.clicked.connect (self._resetLogBtn)
        self.ui.RunAutoImportNowBtn.clicked.connect (self._runAutoImportModelNow)
        self.ui.selDirectoryBtn.clicked.connect (self._selectDirectoryBtn)
        self.ui.AutoImportMLModelEnabledBtn.toggled.connect (self._AutoImportEnabledToggle)
        self._AutoImportEnabledToggle ()
    
    def _AutoImportEnabledToggle (self) :
         enabled = self.ui.AutoImportMLModelEnabledBtn.isChecked ()
         self.ui.autoImportLogGroupBox.setEnabled (enabled)
         self.ui.ResetAutoImportLog.setEnabled (enabled)
         self.ui.AutoImportLogText.setEnabled (enabled)
         self.ui.RunAutoImportNowBtn.setEnabled (enabled)
         self.ui.AutoImportLastRunLbl.setEnabled (enabled)
         self.ui.directorySelGroupBox.setEnabled (enabled)
         self.ui.selDirectoryBtn.setEnabled (enabled)           
         self.ui.directorytxt.setEnabled (enabled)
         self._autoImportMLModels.setEnabled (enabled)
        
    def _selectDirectoryBtn (self):
        dlg= QFileDialog( self )
        dlg.setWindowTitle('Select directory to auto-import ML model from')
        path = self.ui.directorytxt.text ()
        if os.path.isdir (path) and os.path.exists (path) :
            dlg.setDirectory (path)
        dlg.setViewMode( QFileDialog.Detail )    
        dlg.setFileMode (QFileDialog.Directory)
        dlg.setNameFilters( [self.tr('All Files (*)'), self.tr('HDF5 (*.hdf5)'), self.tr('model (*.model)')] )
        dlg.setAcceptMode (QFileDialog.AcceptOpen)
        if dlg.exec_() and (len (dlg.selectedFiles())== 1):
            path = dlg.selectedFiles()[0]
            if os.path.exists (path) and os.path.isdir (path) :
                AutoImportMLModel = self._autoImportMLModels
                AutoImportMLModel.setDirectory (path)
                self._updateDialog ()
        return
    
    def  _updateDialog (self) :
        AutoImportMLModel = self._autoImportMLModels
        self.ui.AutoImportMLModelEnabledBtn.setChecked (AutoImportMLModel.isEnabled ())
        self.ui.directorytxt.setText (AutoImportMLModel.getDirectory ())
        self.ui.AutoImportLastRunLbl.setText ("Auto-import last run: " + AutoImportMLModel.getLastRun ())
        self.ui.AutoImportLogText.setHtml (AutoImportMLModel.getLog ())
        
    def _runAutoImportModelNow (self):
        self._autoImportMLModels.runNow (self._parentMLTreeModel, self)
        self._updateDialog ()
        return
    
    def _resetLogBtn (self):
        AutoImportMLModel = self._autoImportMLModels
        AutoImportMLModel.resetLog ()
        self._updateDialog ()
        return
        
    def _internalWindowResize (self, newsize):                            
        if (self._resizeWidgetHelper is not None) :                                               
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.line1, newsize)           
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.AutoImportMLModelEnabledBtn, newsize)         
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.directorySelGroupBox, newsize)  
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.directorytxt, newsize)  
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.autoImportLogGroupBox, newsize)  
            self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.autoImportLogGroupBox, newsize)  
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.AutoImportLastRunLbl, newsize)  
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.AutoImportLogText, newsize)  
            self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.AutoImportLogText, newsize)  
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.line2, newsize)           
            
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.selDirectoryBtn, newsize.width () - 168)   
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.RunAutoImportNowBtn, newsize.width () - 128)   
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.ResetAutoImportLog, newsize.width () - 128)                 
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.CloseBtn, int ((newsize.width () - self.ui.CloseBtn.width ())/2))           
                     
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.ResetAutoImportLog, self.ui.autoImportLogGroupBox.height () - 31)    
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.line2, newsize.height () - 53)         
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.CloseBtn, newsize.height () - 43)                                
                                       
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)                 
        self._internalWindowResize ( qresizeEvent.size () )               
    
    def _CloseBtn (self) :        
        self.close ()