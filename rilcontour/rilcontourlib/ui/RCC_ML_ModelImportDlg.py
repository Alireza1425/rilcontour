#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 11 14:57:53 2018

@author: m160897
"""
from PyQt5 import QtCore
from PyQt5.QtWidgets import  QDialog
from rilcontourlib.util.rcc_util import ResizeWidgetHelper
from rilcontourlib.ui.qt_ui_autogen.RCC_MLModelImportDlgAutogen import Ui_RCC_MLModelImportDlg
from rilcontourlib.ui.RCC_ML_ModelTree import ML_ModelTree, ML_ModelTreeNode, ML_ModelFileIO
import copy
import numpy as np
import os
import shutil
from rilcontourlib.util.FileUtil import ScanDir

class RCC_ModelImportDlg (QDialog) :
    
    def __init__ (self, PathToLoad, parent) :                
        self._ImportedModels = set ()
        self._parent = parent
        self._ProjectDataset = parent._ProjectDataset
        QDialog.__init__ (self, parent)
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)
        self.ui = Ui_RCC_MLModelImportDlg ()
        self.setWindowFlags(self.windowFlags () | QtCore.Qt.Dialog| QtCore.Qt.WindowStaysOnTopHint )
        self.ui.setupUi (self)                
        self._resizeWidgetHelper = ResizeWidgetHelper ()
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.CloseBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.ImportBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.ModelSplitter)
        self.ui.modelFrame.resizeEvent = self._modelFrameResize 
        self.ui.DescriptionOptionFrame.resizeEvent = self._tabFrameResize
        self._coreModel = ML_ModelTree (self._ProjectDataset, self)
        self._coreModel.addRootNode (ML_ModelTreeNode ("Classification",self._coreModel ))
        self._coreModel.addRootNode (ML_ModelTreeNode ("Segmentation",self._coreModel ))
        ML_ModelTree.loadModelTreeFromPath (PathToLoad, ExistingModelTreeToAddTo = self._coreModel, LoadModelTreeTextLeafs = False, ProjectDataset=self._ProjectDataset)        
        self.ui.ModelTreeView.setModel (self._coreModel)
        selectionModel = self.ui.ModelTreeView.selectionModel ()
        selectionModel.selectionChanged.connect (self._treeSelectionChanged)       
        self.ui.modelTypeFilter.addItem ("Classification & Segmentation")
        self.ui.modelTypeFilter.addItem ("Classification")
        self.ui.modelTypeFilter.addItem ("Segmentation")
        self.ui.modelTypeFilter.currentIndexChanged.connect (self._modelTypeFilterIndexChanged)
        self.ui.filterText.editingFinished.connect (self._filterTextChanged)
        self.ui.filterBySelectedROICheckBox.stateChanged.connect (self._filterBySelectedCheckBox)
        self.ui.ImportBtn.clicked.connect (self._importBtn)
        self.ui.CloseBtn.clicked.connect (self._closeBtn)
        self.ui.ImportBtn.setEnabled (False)
        self._treeSelectionChanged ()
        
        self._currentModel = ML_ModelTree (self._ProjectDataset, self)
        self._currentModel.addRootNode (ML_ModelTreeNode ("Classification",self._coreModel ))
        self._currentModel.addRootNode (ML_ModelTreeNode ("Segmentation",self._coreModel ))
        ML_ModelTree.loadModelTreeFromPath (self._ProjectDataset.getMLHomeFilePath () , ExistingModelTreeToAddTo = self._currentModel, LoadModelTreeTextLeafs = False, ProjectDataset=self._ProjectDataset)        
        
    def _filterTextChanged (self):
        modelTypeComboboxFilterTxt = self.ui.modelTypeFilter.currentText ()
        filterROIList = []
        if (self.ui.filterBySelectedROICheckBox.isChecked ()) :
            selectedROIListNameList = self._parent.ROIDefs.getSelectedROI ()
            for name in selectedROIListNameList :
                filterROIList.append (self._parent.ROIDefs.getROIDefCopyByName (name))
        filteredModel = self._coreModel.getFilteredTreeModel (modelType = modelTypeComboboxFilterTxt.lower (), filterROIList = filterROIList, filterText = self.ui.filterText.text ())  
        self.ui.ModelTreeView.setModel (filteredModel)      
        selectionModel = self.ui.ModelTreeView.selectionModel ()
        selectionModel.selectionChanged.connect (self._treeSelectionChanged)       
    
    def wasModelImported (self) :
        return len (self._ImportedModels) > 0
        
    def _doesCurrentModelHaveNode (self, node):
        if not node.isMLModel ():
            return False
        if node.isVersionNode ():
            versionLst = [node]
        else:
            versionLst = node.getModelVersions ()
        canImport = False
        for modelVersion in versionLst :
                mLModel =  modelVersion.getMLModel ()
                if mLModel is not None :
                    hdf5Path = mLModel.getCurrentKerasModelHDF5Path ()
                    if (hdf5Path not in self._ImportedModels) :
                        canImport = True
                        break
        if not canImport :
            return True
        mlModel = node.getMLModel ()
        return self._currentModel.doesModelDefinitionOrTreeNodeExist (mlModel)
   
    @staticmethod
    def _getNewModelFileName (ml_homePath) :        
        fileNumberLst = []
        try:
           fileIterator = ScanDir.scandir(ml_homePath)       
           for fObj in fileIterator :
                Lfilename = fObj.name.lower()
                if Lfilename.startswith ("model_") and Lfilename.endswith (".model") :
                    try :
                        numstr = fObj.name[len ("Model_") :-len (".model")]
                        num = int (numstr)
                        fileNumberLst.append (num)
                    except :
                        pass 
        finally:
            try:
                fileIterator.close ()
            except:
                pass
            
        numbers = np.array (sorted (fileNumberLst), dtype=np.int)
        if 0 not in numbers :
            return os.path.join (ml_homePath, "Model_0.model")
        elif 1 not in numbers :
            return os.path.join (ml_homePath, "Model_1.model")
        else:        
            firstset = numbers[0:-1]  
            index = (firstset - numbers[1:]) > 1 
            lst = firstset[index]
            if lst.shape[0] > 0 :
                return  os.path.join (ml_homePath, "Model_"+str (lst[0] + 1)+".model")
            else:
                return  os.path.join (ml_homePath, "Model_"+str (numbers[numbers.shape[0]-1] + 1)+".model")    

    @staticmethod
    def importModel (mlModelDef, currentKerasPath, ml_homePath, AutoImported = None):
        modelFilePath = RCC_ModelImportDlg._getNewModelFileName (ml_homePath)
        if (modelFilePath is not None) :                
            hdf5modelPath = None
            try :
                hdf5modelPath = modelFilePath.split (".")
                hdf5modelPath[len (hdf5modelPath) - 1] = "hdf5"
                hdf5modelPath = ".".join (hdf5modelPath)                
                shutil.copyfile (currentKerasPath, hdf5modelPath)                    
            except:                    
                try:
                   os.path.remove (hdf5modelPath)
                except:
                   hdf5modelPath = None
                 
            if (hdf5modelPath is not None) :
                try :
                    mlModelDef.setKerasModelHDF5Path (hdf5modelPath)
                    mlModelDef.setModelAutoImported (AutoImported)
                    if (AutoImported is not None) :
                        mlModelDef.setOrigionalKerasModelHDF5Path (hdf5modelPath)      
                    ML_ModelFileIO.saveModelDef (mlModelDef, modelFilePath) 
                    return True
                except:                    
                    try:
                        os.path.remove (hdf5modelPath)
                    except:
                        hdf5modelPath = None
                    try:
                        os.path.remove (modelFilePath)
                    except:
                        modelFilePath = None
        return False
           

        
        
    def _importBtn (self) :
        importedFiles = False
        selectedModelNode = self.getSelectedTreeNode ()
        if not self._doesCurrentModelHaveNode (selectedModelNode) :
            if selectedModelNode.isVersionNode ():
                versionLst = [selectedModelNode]
            else:
                versionLst = copy.copy (selectedModelNode.getModelVersions ())
            ml_homePath = self._ProjectDataset.getMLHomeFilePath ()
            for modelVersion in versionLst :
                mlModel = modelVersion.getMLModel (ForceAllocation = True)
                if mlModel is not None :
                    hdf5Path = mlModel.getCurrentKerasModelHDF5Path ()
                    if (hdf5Path not in self._ImportedModels) : 
                        if mlModel is not None and not self._currentModel.doesModelDefinitionOrTreeNodeExist (mlModel) :
                           currentKerasPath = mlModel.getCurrentKerasModelHDF5Path ()
                           if os.path.exists (currentKerasPath) and os.path.isfile (currentKerasPath) :
                               
                               if (RCC_ModelImportDlg.importModel (mlModel, currentKerasPath, ml_homePath, AutoImported = None)) : 
                                   self._ImportedModels.add (hdf5Path)
                                   importedFiles = True
        if (importedFiles):
            self.ui.ImportBtn.setEnabled (False)
                       
                    
    
    
    def _modelTypeFilterIndexChanged (self,index) :
        self._filterTextChanged ()
            
    def _filterBySelectedCheckBox (self) :
        self._filterTextChanged ()
   
    def getSelectedTreeNode (self)    :
        selection = self.ui.ModelTreeView.selectionModel ()
        item = selection.currentIndex()                 
        treeNode = item.internalPointer ()                               
        return treeNode    
        
    def _treeSelectionChanged (self, selection = None, deselected = None) :
        node = self.getSelectedTreeNode ()        
        enabled = node is not None and node.isMLModel ()   
        if (enabled) :
            mlModel = node.getMLModel ()
            self.ui.modelDescriptionTxt.setHtml (mlModel.getHtmlSummary ())
            self.ui.ImportBtn.setEnabled (not self._doesCurrentModelHaveNode (node) )
        else:
            self.ui.modelDescriptionTxt.setHtml ("")
            self.ui.ImportBtn.setEnabled (False)
        
    def _closeBtn (self) :
        self.close ()  
    
    def _tabFrameResize (self, evemt):
        width = self.ui.DescriptionOptionFrame.width ()
        height = self.height ()
        self._resizeWidgetHelper.setWidth (self.ui.modelDescriptionTxt, width - 20 )        
        self._resizeWidgetHelper.setHeight (self.ui.modelDescriptionTxt, height - 127 )        
        self._resizeWidgetHelper.setWidth (self.ui.DescriptionOptionTabWidget, width  )        
        self._resizeWidgetHelper.setHeight (self.ui.DescriptionOptionTabWidget, height  )        
        
    def _modelFrameResize (self, event) :
        width = self.ui.modelFrame.width ()
        self._resizeWidgetHelper.setWidth (self.ui.filterText, width - 75 )        
        self._resizeWidgetHelper.setWidth (self.ui.filterBySelectedROICheckBox, width - 75 )
        self._resizeWidgetHelper.setWidth (self.ui.modelTypeFilter, width - 75 )
        self._resizeWidgetHelper.setWidth (self.ui.ModelTreeView, width - 18 )
        self._resizeWidgetHelper.setHeight(self.ui.ModelTreeView, self.ui.modelFrame.height () - 100)
    
    def _internalWindowResize (self, newsize):                            
        if (self._resizeWidgetHelper != None) :                                               
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.ModelSplitter, newsize)            
            self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.ModelSplitter, newsize)                                                
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.ImportBtn, newsize.height () - 43)                        
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.ImportBtn, newsize.width () - 289)                                
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.CloseBtn, newsize.height () - 43)                        
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.CloseBtn, newsize.width () - 139)                                
                                       
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)                 
        self._internalWindowResize ( qresizeEvent.size () )               
    
    
   