# -*- coding: utf-8 -*-
"""
Created on Fri Oct 14 11:02:14 2016

@author: Philbrick
"""
from PyQt5.QtWidgets import  QDialog, QProgressDialog, QListWidgetItem
from PyQt5 import QtCore
from OpenGL.GL import *
from OpenGL.GLU import *
from rilcontourlib.ui.qt_ui_autogen.RCC_OpenGLViewerDlgAutogen import Ui_OpenGLDialog
#from skimage import transform
from rilcontourlib.util.rcc_util import  PlatformMP #,FastUnique
import numpy as np
from rilcontourlib.util.rcc_util import ResizeWidgetHelper
from skimage.measure import correct_mesh_orientation
import math
from skimage.measure import marching_cubes_lewiner
from skimage.measure import marching_cubes_classic
 
"""def findDuplicates (verts) :
  returnLst = []
  XG = verts[:,0]
  YG = verts[:,1]
  ZG = verts[:,2]
  XVals = FastUnique.unique (XG)  
  for x in XVals :
      index = XG == x 
      #if (x == 229) :
      #    print ("stop")
      if np.sum (index) > 1 : # mutliple found x value 
          YV = verts[index,1]
          YVals = FastUnique.unique (YV)  
          for y in YVals :
              #if (y == 156) :
              #    print ("stop y")
              index2 = np.copy (index)
              index2[YG != y] = False
              if np.sum (index2) > 1 : # mutliple found y value
                  ZV = verts[index2,2]
                  ZVals = FastUnique.unique (ZV)  
                  for z in ZVals :
                    index3 = np.copy (index2)
                    index3[ZG != z] = False
                    if np.sum (index3) > 1 : # mutliple found z value
                       #print ("Found duplicate")
                       dL = index3.nonzero ()[0]
                       #print (verts[dL])
                       first = dL[0]
                       for dI in range (1, len (dL)) :
                           returnLst.append ((first, dL[dI]))                                    
  return returnLst"""
  
def MP_createObject (tpl) :
        name, volume, shape, voxeldim, color, orientation, objectBoundingBox = tpl
        xbox, ybox, firstslice, lastslice = objectBoundingBox
        print (name + " Generating model")        
        
       
        
        try :
            verts, faces, _, _ = marching_cubes_lewiner (volume,  0.0, spacing=(voxeldim[0], voxeldim[1], voxeldim[2]), step_size = 1, allow_degenerate = False)     
        except : 
            try :
                verts, faces = marching_cubes_classic (volume, 0.0, spacing=(voxeldim[0], voxeldim[1], voxeldim[2]))      
                faces = correct_mesh_orientation  (volume, verts, faces, spacing=(voxeldim[0], voxeldim[1], voxeldim[2]))  
            except:
                print ("Sci-kit Learn: Marching cubes failed returning")                
                return None
        del volume
        
        verts[:,0] += voxeldim[0] * xbox[0]
        verts[:,1] += voxeldim[1] * ybox[0]       
        verts[:,2] += voxeldim[2] * (shape[2] - lastslice)
        
        """duplicateList = findDuplicates (verts)
        for duplicate in duplicateList :
            firstV, SecondV =  duplicate
            faces[faces==SecondV] =firstV """
        
        
        

        print (name + " Smoothing model")   
        ## Smooth Mesh            
        verts, normals = VolumeViewerUI._smoothMeshObjAndGenerateNormals (name, verts, faces,voxeldim)
        
        #verts = verts.astype (np.float)
        faces = np.ndarray.flatten (faces).astype (np.int)
        #normals = normals.astype (np.float)
                
        
        if (orientation == "Axial") :
            verts[:,1], verts[:,2] = np.copy (verts[:,2]), np.copy (verts[:,1])
            normals[:,1],normals[:,2] = np.copy (normals[:,2]), np.copy (normals[:,1])
        elif (orientation == "Sagittal") :
            verts[:,0], verts[:,2] = np.copy (verts[:,2]), np.copy (verts[:,0])
            normals[:,0],normals[:,2] = np.copy (normals[:,2]), np.copy (normals[:,0])
      
            
        minX = np.min (verts[:,0])
        maxX = np.max (verts[:,0])
        minY = np.min (verts[:,1])
        maxY = np.max (verts[:,1])
        minZ = np.min (verts[:,2])
        maxZ = np.max (verts[:,2])
        
        return (name, verts, normals, faces, (color[0], color[1], color[2]), (minX, maxX, minY, maxY, minZ,maxZ ))

class VolumeViewerUI (QDialog):
   
   def __init__(self, parent=None):   
        QDialog.__init__ (self, parent)               
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)
        self.ui = Ui_OpenGLDialog ()                
        self.ui.setupUi (self)     
        self.ui.openGL.paintGL = self.paintGL  
        self.ui.openGL.resizeGL = self.resizeGL  
        self.ui.openGL.initializeGL = self.initializeGL                 
        self.resize (self.ui.openGL.width (),self.ui.openGL.height ())
        self._modelSet = False
        self.init = False
        self._YRotate = 0
        self._XRotate = 0
        self._oldMouseX = None
        self._oldMouseY = None
        self.setWindowFlags(self.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)
        self._objMeshDictionary = {}
        self._objMeshNameDrawList = []
        self._Scale = 1.0
        self._XMouseDir = 1.0
        
        self._resizeWidgetHelper = ResizeWidgetHelper ()        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.splitter)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.HLine)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.CloseBtn)
        self.ui.CloseBtn.clicked.connect (self.close)
        
        self.ui.ObjectList.selectionModel().selectionChanged.connect(self.ObjectListSelectionChanged)     

   def ObjectListSelectionChanged (self) :
       self._objMeshNameDrawList = []
       for selectedObject in self.ui.ObjectList.selectedItems() :
          self._objMeshNameDrawList.append (selectedObject.text ())
       self.ui.openGL.update()
       self.update ()
   
   def _internalWindowResize (self, newsize):                            
        if (self._resizeWidgetHelper != None) :                                               
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.splitter, newsize)            
            self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.splitter, newsize)        
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.HLine, newsize)    
            
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.HLine, self.height () - 62)   
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.CloseBtn, self.height () - 42)                    
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.CloseBtn, self.width () - 145)                  
            
   def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)                 
        self._internalWindowResize ( qresizeEvent.size () )                  
        
   def paintGL(self):
        if not self.ui.openGL.isValid():
            return
        
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glMatrixMode(GL_MODELVIEW)
            
        if self._modelSet :            
            glLoadIdentity ()            
            glTranslatef (0, 0, self._ZOffset) 
            glRotatef(self._YRotate, 1.0,0.0,0.0)      
            glRotatef(self._XRotate, 0.0,1.0,0.0)      
            glScalef (self._Scale,self._Scale,self._Scale  )                  
            glEnableClientState(GL_VERTEX_ARRAY)
            glEnableClientState(GL_NORMAL_ARRAY)                                                    
            point_obj_list = []
            for objName in self._objMeshNameDrawList  :         
                meshobj = self._objMeshDictionary[objName] 
                
                if "Vertex" in meshobj :
                    vertexList, normalList, faceList, color = meshobj["Vertex"], meshobj["Normals"], meshobj["Faces"], meshobj["Color"]                                
                    glColor4f (color[0],color[1],color[2], 1.0)  
                    glVertexPointer(3, GL_FLOAT, 0, vertexList)
                    glNormalPointer(GL_FLOAT, 0, normalList)
                    glDrawElements(GL_TRIANGLES, len (faceList), GL_UNSIGNED_INT,  faceList)   
                    """glColor3f (0.0, 0.0 , 1.0)   
                                    glLineWidth (2)
                                    glBegin (GL_LINES)
                                    for vI in range (int (len (vertexList)/3)) :
                                        vP = vI * 3
                                        glVertex3f (vertexList[vP], vertexList[vP + 1], vertexList[vP + 2])
                                        glVertex3f (vertexList[vP] + normalList[vP], vertexList[vP + 1] + normalList[vP + 1] , vertexList[vP + 2] + normalList[vP + 2])
                                    glEnd ()"""     
                elif "Points" in meshobj :
                    point_obj_list.append (meshobj)
            if len (point_obj_list) > 0 :
                glDisable(GL_LIGHTING)
                glDisableClientState(GL_NORMAL_ARRAY)
                for pointobj in point_obj_list  :         
                        vertexList, faceList,  color = pointobj["Points"], pointobj["Faces"],  pointobj["Color"]                                
                        glColor4f (color[0],color[1],color[2], 1.0)  
                        glVertexPointer(3, GL_FLOAT, 0, vertexList)
                        glDrawElements(GL_POINTS, len (faceList), GL_UNSIGNED_INT,  faceList)   
                glEnableClientState(GL_NORMAL_ARRAY)    
                glEnable(GL_LIGHTING)
                    
            glDisableClientState(GL_VERTEX_ARRAY)
            glDisableClientState(GL_NORMAL_ARRAY)
            glFlush()
             
            """glEnable( GL_ALPHA_TEST );
            glAlphaFunc( GL_GREATER, 0.03)
            # glDisable(GL_COLOR_MATERIAL)   
            glEnable(GL_BLEND);
            glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA )
            glMatrixMode( GL_TEXTURE );
            glLoadIdentity();
            glTranslatef( 0.5, 0.5, 0.5)
            glRotatef(self._YRotate, 1.0,0.0,0.0)      
            glRotatef(self._XRotate, 0.0,1.0,0.0)    
           
            glTranslatef( -0.5,-0.5, -0.5);
            glMatrixMode(GL_MODELVIEW)    
            glLoadIdentity ()  
            glTranslatef (0, 0, self._ZOffset) 
            glScalef (self._Scale,self._Scale,self._Scale  )  
            glRotatef(90, 0.0,0.0,1.0)      
            if (self._textureObj is None) :
                self._textureObj= glGenTextures(1)    
                glBindTexture( GL_TEXTURE_3D, self._textureObj )
                glActiveTexture(GL_TEXTURE0);
                glPixelStorei(GL_PACK_ALIGNMENT, 1)
            
                glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA, self._imgData.shape[2], self._imgData.shape[1],self._imgData.shape[0] ,  0, GL_RGBA, GL_UNSIGNED_BYTE, self._imgData );
            
                glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER)
                glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER)
                glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_BORDER)
                glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
                glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
                
            glColor4f (1.0,1.0,1.0,0.2)
            glBindTexture( GL_TEXTURE_3D, self._textureObj)
            glEnable(GL_TEXTURE_3D);
            glDisable (GL_CULL_FACE)            
            dViewPortSize = 1.0
            glBegin(GL_QUADS)
            for index in range (-1000, 1001, 3) :
                TexIndex = index / 1000
                
                glTexCoord3f(0.0, 0.0, (TexIndex+1.0)/2.0) 
                glVertex3f(-dViewPortSize,-dViewPortSize,TexIndex)
                glTexCoord3f(1.0, 0.0, (TexIndex+1.0)/2.0)
                glVertex3f(dViewPortSize,-dViewPortSize,TexIndex)
                glTexCoord3f(1.0, 1.0, (TexIndex+1.0)/2.0) 
                glVertex3f(dViewPortSize,dViewPortSize,TexIndex)
                glTexCoord3f(0.0, 1.0, (TexIndex+1.0)/2.0)  
                glVertex3f(-dViewPortSize,dViewPortSize,TexIndex)
            glEnd()
            glDisable(GL_TEXTURE_3D);
            glEnable (GL_CULL_FACE) """
            #glEnable(GL_COLOR_MATERIAL)   


   def mouseReleaseEvent (self, event) :
        self._oldMouseX = None
        self._oldMouseY = None


   def _stepZoom (self, dStep):
       oldScale = self._Scale
       self._Scale += dStep       
       if (self._Scale > 3 ) :
            self._Scale = 3
       elif (self._Scale < 0.25) :
            self._Scale = 0.25
       if (oldScale != self._Scale) :
            self.ui.openGL.update()
            self.update ()
            
   def wheelEvent(self,event) :
        pixelDelta = event.pixelDelta()
        if not pixelDelta.isNull () :
            dStep = int (pixelDelta.y ())
            if (self.visSettings.isMouseWheelInverted ()) :
                dStep = -dStep
        else:
            numDegrees = event.angleDelta() / 8
            numSteps = numDegrees / 15
            dStep = int (numSteps.y ())
            
            if (self.visSettings.isMouseWheelInverted ()) :
                dStep = -dStep
        if (dStep < 0) :
            self._stepZoom (min (0.01 *  dStep, -0.01))
        elif (dStep > 0) :
            self._stepZoom (max (0.01 *  dStep, 0.01))
        event.accept()
        
   def mouseMoveEvent (self, event) :
        pos = event.pos ()
        xPos = pos.x ()
        yPos = pos.y ()
        if (event.buttons () == QtCore.Qt.LeftButton) :
            if (self._oldMouseX is not None and xPos != self._oldMouseX) or (self._oldMouseY is not None and yPos != self._oldMouseY) :
                oldXRot = self._XRotate
                oldYRot = self._YRotate
                self._XRotate +=  self._XMouseDir * (xPos - self._oldMouseX) 
                self._YRotate +=  (yPos - self._oldMouseY) 
                self._XRotate = self._XRotate % 360.0
                self._YRotate = self._YRotate % 360.0
                if (oldXRot != self._XRotate or oldYRot != self._YRotate) :
                    self.ui.openGL.update()
                    self.update ()
        elif (event.buttons () == QtCore.Qt.RightButton) :
            if (self._oldMouseY is not None and yPos != self._oldMouseY) :
                deltaZ = yPos - self._oldMouseY
                self._stepZoom (deltaZ / 100)
                
        self._oldMouseX = xPos
        self._oldMouseY = yPos
        
        
   def resizeGL(self, w, h):
       glViewport(0, 0, w, h)       
       if (not self.init) :
            self.init = True
            
            xMin, xMax, yMin, yMax, zMin, zMax = -1.0, 1.0, -1.0, 1.0, -1.0, 1.0#self._globalBoundingBox
            xDim = xMax - xMin + 1
            yDim = yMax - yMin + 1
            zDim = zMax - zMin + 1
            planeDim = max (zDim, xDim)
            self._ZOffset = -planeDim * (2.0/3.0)
            
            glMatrixMode(GL_PROJECTION)
            glLoadIdentity()
                
            ratio = 0.5 * planeDim * h/w            
            glFrustum (-0.5 * planeDim, 0.5 * planeDim, -0.5*ratio, 0.5*ratio, 1, 5*planeDim)
  
            glEnable(GL_DEPTH_TEST)      
            glClearColor(0.0, 0.0, 0.0, 1.0)
            glDisable (GL_CULL_FACE)  
            glCullFace(GL_BACK)
            glFrontFace (GL_CW)   
            
            glShadeModel(GL_SMOOTH)
            
            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity ()
            glEnable(GL_LIGHTING)
            glEnable(GL_LIGHT0);
            
            glLightfv(GL_LIGHT0, GL_POSITION, [0, 0, 20.0, 0.0])
            
            glMaterialf(GL_FRONT, GL_SHININESS, 50.0)      
            glColorMaterial (GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
            glColorMaterial (GL_FRONT, GL_SPECULAR);
            glColorMaterial (GL_FRONT, GL_AMBIENT);
            glLightfv(GL_LIGHT0, GL_DIFFUSE,  [0.70, 0.70, 0.70, 1.0])
            glLightfv(GL_LIGHT0, GL_AMBIENT,  [0.15,0.15,0.15,1.0])
            glLightfv(GL_LIGHT0, GL_SPECULAR, [0.15,0.15,0.15,1.0])
            glEnable(GL_COLOR_MATERIAL)     
            
            try:
                glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST)
                glEnable(GL_LINE_SMOOTH)
                glEnable(GL_POINT_SMOOTH)
                glHint(GL_LINE_SMOOTH_HINT,GL_NICEST)
                glHint(GL_POINT_SMOOTH_HINT,GL_NICEST)
                glEnable(GL_NORMALIZE)
                glEnable (GL_MULTISAMPLE)
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
            except:
                pass
            try:
                glHint(GL_MULTISAMPLE_FILTER_HINT_NV, GL_NICEST)
            except:
                pass
            glPointSize (10.0)
           
            self._textureObj=None
           
            

        
            
   def initializeGL(self):
        return     
    
   """ @staticmethod
   def _addSmoothVertexAdjustment (smoothVector, smoothCounts, v1, a1, a2, a3, smoothInit, voxeldim) :                            
        smoothVector[v1] += voxeldim * (a2[:] - a1[:] + a3[:] - a1[:]) # [(x2 - x1) + (x3 - x1),(y2 - y1) + (y3 - y1),(z2 - z1) + (z3 - z1)] 
        if (smoothInit) :
            smoothCounts[v1] += 2
   
   @staticmethod
   def _smoothMesh (verts, v1, v2, v3, sf, smoothVector,smoothCounts, smoothInit, voxeldim) :         
        smoothVector [...] = 0 
        #smoothCounts [...] = 0                    
                
        a1 = verts[v1]
        a2 = verts[v2]
        a3 = verts[v3]
            
        VolumeViewerUI._addSmoothVertexAdjustment (smoothVector,smoothCounts, v1, a1, a2, a3, smoothInit, voxeldim)
        VolumeViewerUI._addSmoothVertexAdjustment (smoothVector,smoothCounts, v2, a2, a1, a3, smoothInit, voxeldim)
        VolumeViewerUI._addSmoothVertexAdjustment (smoothVector,smoothCounts, v3, a3, a2, a1, smoothInit, voxeldim)
        if (smoothInit) :
            smoothCounts = smoothCounts.astype (np.float) + 0.00001
        for index in range (3) :
            verts[:,index] += sf * (smoothVector[:,index] / smoothCounts )
        return smoothCounts """  
    
   @staticmethod
   def _addSmoothVertexAdjustment2 (smoothVector, smoothCounts, v1, a1, a2, EdgeWeight) :                            
        for index in range (3) :
            smoothVector[v1,index] += EdgeWeight * (a2[:,index] - a1[:,index]) # [(x2 - x1) + (x3 - x1),(y2 - y1) + (y3 - y1),(z2 - z1) + (z3 - z1)] 
        smoothCounts[v1] += EdgeWeight
            
   @staticmethod
   def _smoothMesh2 (verts, sf, smoothVector,smoothCounts,  Edge, EdgeWeight) :         
        smoothVector [...] = 0 
        smoothCounts [...] = 0                    
        v1 = Edge[:,0]
        v2 = Edge[:,1]
        a1 = verts[v1]
        a2 = verts[v2]
        
        VolumeViewerUI._addSmoothVertexAdjustment2 (smoothVector,smoothCounts, v1, a1, a2, EdgeWeight)
        VolumeViewerUI._addSmoothVertexAdjustment2 (smoothVector,smoothCounts, v2, a2, a1, EdgeWeight)
     
        smoothCounts += 0.00001
        for index in range (3) :
            verts[:,index] += sf * (smoothVector[:,index] / smoothCounts )
        #return smoothCounts       
    
   @staticmethod 
   def _computeEdgeDistSq (v1, v2) :
       return np.sum (np.square (v1 - v2), axis = -1)
   
   @staticmethod 
   def _getCot (asq, bsq, csq) :
        # NOTE: cos(alpha) = (a^2 + b^2  - c^2) / (2.a.b)
        # with a, b, c the lengths of of the sides of the triangle and (a, b)
        # forming the angle alpha.
        cos_alpha = np.abs(asq + bsq - csq) / (2 * np.sqrt (asq) * np.sqrt(bsq))
        cos_alpha = np.clip (cos_alpha, 0.0, 1.0)
        # and recall cos(x)^2 + sin(x)^2 = 1
        # then sin(x) = sqrt(1-cos(x))
        sin_alpha = np.sqrt(1.0 - np.square (cos_alpha))
        
        cot = np.copy (cos_alpha)
        validI = sin_alpha != 0
        cot[validI] /= sin_alpha[validI]
        cot[sin_alpha == 0] = 0
        
        cot[cot == np.nan] = 0
        eps = 0.000001
        cotan_max = math.cos( eps ) / math.sin( eps )
        # NOTE: cot(x) = cos(x)/sin(x)
        cot [cot > cotan_max] = cotan_max
        return cot

   @staticmethod
   def _addTriEdge (v1, v2, v3, trimesh) :
       if v1 in trimesh :
           mesh = trimesh[v1]
           if v2 not in mesh :
               mesh[v2] = [v3]
           elif (v3 not in mesh[v2]) :
               mesh[v2].append (v3)
       elif v2 in trimesh :
           mesh = trimesh[v2]
           if v1 not in mesh :
               mesh[v1] = [v3]
           elif (v3 not in mesh[v1]) :
               mesh[v1].append (v3)
       else:
           trimesh[v1] = {}
           trimesh[v1][v2] = [v3]
   
   @staticmethod        
   def _addTri (v1, v2, v3, trimesh) :
       VolumeViewerUI._addTriEdge (v1, v2, v3, trimesh)
       VolumeViewerUI._addTriEdge (v2, v3, v1, trimesh)
       VolumeViewerUI._addTriEdge (v1, v3, v2, trimesh)
  
   @staticmethod
   def getAngle (verts, v1PT, v2PT, V1_V2, ValidDist, v3, wj, wjCounts)  :
       selection = v3 != -1
       if np.any (selection) :
           v3 = v3[selection]
           v3PT = verts[v3]
           V2_V3 = VolumeViewerUI._computeEdgeDistSq (v2PT[selection], v3PT)
           V3_V1 = VolumeViewerUI._computeEdgeDistSq (v3PT, v1PT[selection])
           ValidDist = np.logical_and (ValidDist[selection], V2_V3 > 0)
           np.logical_and (ValidDist, V3_V1 > 0, out = ValidDist)
          
           wj[selection][ValidDist] += VolumeViewerUI._getCot (V3_V1[ValidDist], V2_V3[ValidDist], V1_V2[selection][ValidDist])
           wjCounts[selection][ValidDist] += 1.0
           
           
   @staticmethod
   def _computeEdgeWeights (verts, FlattenList, wj, wjCounts) :
       wj[...] = 0.0
       wjCounts[...] = 0.0
       v1 = FlattenList[:,0]
       v2 = FlattenList[:,1]
       
       v1PT = verts[v1]
       v2PT = verts[v2]
       V1_V2 = VolumeViewerUI._computeEdgeDistSq (v1PT, v2PT)
       ValidDist = V1_V2 > 0
       
       for vi  in range(2, FlattenList.shape[1]) :
           v3 = FlattenList[:,vi]
           VolumeViewerUI.getAngle (verts, v1PT, v2PT, V1_V2, ValidDist, v3, wj, wjCounts)
       index = wjCounts >0
       wj[index] /= wjCounts[index]

   # Smoothing mesh using: cotangent weighted smoothing algorithm Posted : http://rodolphe-vaillant.fr/?e=69
   @staticmethod
   def _smoothMeshObjAndGenerateNormals (name, verts, faces, voxeldim):        
        smoothVector = np.zeros (verts.shape, dtype = np.float)
        smoothCounts = np.zeros ((verts.shape[0],), dtype = np.float)
        voxeldim = np.copy (voxeldim)
        voxeldim = np.min (voxeldim) / voxeldim
        v1 = faces[:,0] 
        v2 = faces[:,1] 
        v3 = faces[:,2] 
        print ("building edge")
        triangleEdgeList = {}
        for index in range (v1.shape[0]) :
            VolumeViewerUI._addTri (v1[index],v2[index], v3[index], triangleEdgeList)
        print ("flattening edges")
        FlattenList = []
        padLen = 0
        for vert1 in triangleEdgeList.keys ():
            V2Edge = triangleEdgeList[vert1]
            for vert2 in V2Edge.keys () :
                lst = V2Edge[vert2]
                base = [vert1, vert2]
                for vtx in lst :
                    base.append (vtx)
                if len (base) > 2 :
                    FlattenList.append (base)
                    padLen = max (padLen, len (base))
        for lst in FlattenList :
            length = len (lst)
            for _ in range (length, padLen) :
                lst.append (-1)
        FlattenList = np.array (FlattenList, dtype=np.int)
        Edge = np.zeros ((FlattenList.shape[0],2), dtype=np.int)
        EdgeWeight = np.zeros ((FlattenList.shape[0],), dtype=np.float)
        Edge[:,0] = FlattenList[:,0]
        Edge[:,1] = FlattenList[:,1]
        
        EdgeWeight = np.zeros ((FlattenList.shape[0],), dtype = np.float)
        EdgeWeightCounts  = np.zeros ((FlattenList.shape[0],), dtype = np.float)
       
        #verts = verts.astype (np.float)
        for superCycle in range (10) :            
            print (name + " smoothing: %d/10" % superCycle)            
            #for cycles in range (10) :                
                
            VolumeViewerUI._computeEdgeWeights (verts, FlattenList, EdgeWeight, EdgeWeightCounts)
            VolumeViewerUI._smoothMesh2 (verts, 0.99, smoothVector, smoothCounts, Edge, EdgeWeight)
            
            VolumeViewerUI._computeEdgeWeights (verts, FlattenList, EdgeWeight, EdgeWeightCounts)
            VolumeViewerUI._smoothMesh2 (verts, -1.0, smoothVector, smoothCounts,  Edge, EdgeWeight)       

        print (name + " Generating normals")                                  
        vec1 = verts[v1]
        vec2 = verts[v2]
        vec3 = verts[v3]
                            
        nVec1 = vec2 - vec1
        nVec2 = vec3 - vec1
        normal = np.cross (nVec2[:], nVec1[:])        
        normal[:] /= (np.sqrt(np.sum (np.square (normal[:] )))+ 0.00001)

        normalVector = smoothVector
        normalsCounts = smoothCounts
        normalVector[...] = 0.0
        normalsCounts[:] = 0
        VolumeViewerUI._setNormal (normalVector, normalsCounts, v1, normal)
        VolumeViewerUI._setNormal (normalVector, normalsCounts, v2, normal)
        VolumeViewerUI._setNormal (normalVector, normalsCounts, v3, normal)                        
        normalsCounts +=  0.00001
        for index in range (3) :
            normalVector[:, index] /=  normalsCounts[:] 
        normalVector[:] /= (np.sqrt(np.sum (np.square (normalVector[:])))+ 0.00001)
        return (verts, normalVector)
    
   @staticmethod
   def _setNormal (normalVecAry, normalVecCountAry, vertexIndex, normal) :       
           normalVecAry [vertexIndex] += normal[:]         
           normalVecCountAry [vertexIndex] += 1  
    
   @staticmethod
   def _getClippedObjectVolume (obj, sliceShape, SelectedSlices) :
        initalized = False
        firstslice, lastslice = obj.getBoundingBoxSliceRange ()
        xbox, ybox = obj._getAxisBoundingBox (sliceAxis = 'Z')
        firstslice = int (firstslice)
        lastslice = int (lastslice)
        volume = np.zeros ((int (xbox[1]) - int (xbox[0]) + 2, int (ybox[1]) - int (ybox[0]) + 2,  lastslice - firstslice+3))        
        
        for sliceindex in range (firstslice, lastslice+1):
            if SelectedSlices is None or len (SelectedSlices) < 2 or sliceindex in SelectedSlices :
                try:
                   contour = obj.getSliceMap (sliceindex,(sliceShape[0],sliceShape[1]))
                   if not np.any (contour) : 
                       contour = None
                except:
                    contour = None
                if contour is not None :                  
                   volume [1:-1,1:-1,lastslice - sliceindex+1] = contour [int (xbox[0]) : int(xbox[1]),int (ybox[0]) : int(ybox[1])]   
                   initalized = True
        if not initalized :
            volume = None
        return volume, (xbox, ybox, firstslice, lastslice)
    
   MP_createObject = staticmethod (MP_createObject)
   
   def _clipPointListandTransform (self, pointList, SelectedSlices, orientation) :
         clipList = []
         for point in pointList :
              x,y,z = point.getCoordinate ()
              if len (SelectedSlices) <= 1 or z in SelectedSlices :
                  if (orientation == "Axial") :
                      #y, z = z, y
                      clipList.append ([x,z,y])
                  elif (orientation == "Sagittal") :
                      #x,z = z,x
                      clipList.append ([z,y,x])
                  else:
                      clipList.append ([x,y,z])
         return clipList
   
   
   
   def setModel (self, objlst, RoiDefs, shape, voxeldim, parentWindow, app, SelectedSlices):
        self._objMeshDictionary = {}
        self._objMeshNameDrawList = []
        self.visSettings = parentWindow.visSettings
        progdialog = QProgressDialog("", "Cancel", 0, 100, parentWindow)
        progdialog.setMinimumDuration (0)
        progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)
        progdialog.setWindowTitle("Generating Slice 3D Model")
        progdialog.setWindowModality(QtCore.Qt.ApplicationModal)
        progdialog.setMaximum (len(objlst))
        try:
            progdialog.forceShow()                           
            progdialog.setLabelText ("Generating Model")
            progdialog.setValue (0)
            parentWindow._ProjectDataset.getApp ().processEvents ()
            
            imageOrientation = parentWindow.NiftiVolume.getDisplaySliceOrientation ()
            MP_List = []
            PointObjList = []
            ProcessDialogTextList = []
            
            """data = parentWindow.NiftiVolume.getImageData ()
            data = data.astype (np.float)
            data = np.clip (data, -160, 240)
            data = data + 160
            data = data.astype (np.float)
            data /= 400
            data *= 255
            data = data.astype (np.uint8)
            self._imgData = np.zeros ((data.shape[0], data.shape[1], data.shape[2], 4), dtype = data.dtype)"""
            
            for obj in objlst :
                 if (obj is not None) :
                     name = obj.getName (RoiDefs)
                     color = RoiDefs.getROIColor (name)
                     if (obj.isROIArea ()) :
                         objectClippedVolume, objectBoundingBox = VolumeViewerUI._getClippedObjectVolume (obj, shape, SelectedSlices)                     
                         if objectClippedVolume is not None :
                             
                             """xdim, ydim, firstslice, lastslice = objectBoundingBox
                             xmin,xmax = xdim
                             ymin,ymax = ydim
                             bindex = objectClippedVolume[1:-1,1:-1,1:-1].astype (np.bool)
                             bindex = bindex[::,::,::-1]
                             
                             vals = data[xmin:xmax, ymin:ymax, firstslice:lastslice+1][bindex]
                             self._imgData[xmin:xmax, ymin:ymax, firstslice:lastslice+1,0][bindex] = vals / 2 + (0.5 * color.red ())
                             self._imgData[xmin:xmax, ymin:ymax, firstslice:lastslice+1,1][bindex] = vals / 2 + (0.5 * color.green ())
                             self._imgData[xmin:xmax, ymin:ymax, firstslice:lastslice+1,2][bindex] = vals / 2 + (0.5 * color.blue ())
                             self._imgData[xmin:xmax, ymin:ymax, firstslice:lastslice+1,3][bindex] = 0.25*vals #data[xmin:xmax, ymin:ymax, firstslice:lastslice+1][bindex]
                             scaledVoxeldim, objectClippedVolume, objectBoundingBox, scaledShape = VolumeViewerUI.resizeImage (voxeldim,  objectClippedVolume, objectBoundingBox, shape)"""
                             MP_List.append ((name, objectClippedVolume, shape, voxeldim, (color.red ()/255.0, color.green()/255.0, color.blue ()/255.0), imageOrientation, objectBoundingBox))
                             ProcessDialogTextList.append ("Processing: " + name)
                     elif (obj.getType () == "Point") :
                         pointList = obj.getPointList ()
                         pointList = self._clipPointListandTransform (pointList, SelectedSlices, imageOrientation)
                         if len (pointList) > 0 :
                             PointObjList.append ((name, color, pointList))
            
            
            
            PoolSize = PlatformMP.getAvailableCPU ()
            objLst = PlatformMP.processMap (VolumeViewerUI.MP_createObject, MP_List, parentWindow._ProjectDataset.getROISaveThread (), PoolSize = PoolSize, UseProcessBasedMap = True, QApp = parentWindow._ProjectDataset.getApp (), ProgressDialog = progdialog, ProcessDialogTextList = ProcessDialogTextList, Debug=False, ProjectFileInterface = parentWindow._ProjectDataset.getProjectFileInterface())
            
            if (imageOrientation == "Sagittal") :
                self._YRotate = 180
                self._XRotate = 0
                self._XMouseDir = -1
            elif (imageOrientation == "Coronal") :
                self._YRotate = 180
                self._XRotate = 180
                self._XMouseDir = -1
            elif (imageOrientation == "Axial") :
                self._YRotate = 0
                self._XRotate = 180
                self._XMouseDir = 1
            
            globalClippingBox = None        
            for MeshObj in objLst :
                if MeshObj is not None :                
                    name, verts, normals, faces, color, clippingBox = MeshObj
                    if globalClippingBox is None :
                        globalClippingBox = clippingBox
                    else:
                        xMin = min (clippingBox[0], globalClippingBox[0])
                        xMax = max (clippingBox[1], globalClippingBox[1])
                        yMin = min (clippingBox[2], globalClippingBox[2])
                        yMax = max (clippingBox[3], globalClippingBox[3])
                        zMin = min (clippingBox[4], globalClippingBox[4])
                        zMax = max (clippingBox[5], globalClippingBox[5])
                        globalClippingBox =  (xMin, xMax, yMin, yMax, zMin,zMax)
            for PointObj in PointObjList :
                name, color, pointList = PointObj
                for point in pointList :
                    px, py, pz = point
                    if globalClippingBox is None :
                        globalClippingBox = (px,px,py,py,pz,pz)
                    else:
                        xMin = min (px, globalClippingBox[0])
                        xMax = max (px, globalClippingBox[1])
                        yMin = min (py, globalClippingBox[2])
                        yMax = max (py, globalClippingBox[3])
                        zMin = min (pz, globalClippingBox[4])
                        zMax = max (pz, globalClippingBox[5])
                        globalClippingBox =  (xMin, xMax, yMin, yMax, zMin,zMax)
                    
        
            if (globalClippingBox is None) :
                self._objMeshDictionary = {}
                self._objMeshNameDrawList = []
            else:
                xMin, xMax, yMin, yMax, zMin, zMax = globalClippingBox
                xC = int ((xMin + xMax) / 2)
                yC = int ((yMin + yMax) / 2)
                zC = int ((zMin + zMax) / 2)        
                normFactor = float (np.max (np.abs ([xMax - xMin + 1, yMax - yMin + 1, zMax - zMin + 1])))+2.0
                newMeshDictionary = {}
                for MeshObj in objLst :    
                    if (MeshObj is not None) :
                        name, verts, normals, faces, color, clippingBox = MeshObj            
                        verts[:,0] -= xC
                        verts[:,1] -= yC
                        verts[:,2] -= zC            
                        verts[:,0] /= normFactor
                        verts[:,1] /= normFactor
                        verts[:,2] /= normFactor
                        newMeshDictionary[name] = {}
                        newMeshDictionary[name]["Vertex"] = verts.flatten ()
                        newMeshDictionary[name]["Normals"] = normals.flatten ()
                        newMeshDictionary[name]["Faces"] = faces
                        newMeshDictionary[name]["Color"] = color
            
                for PointObj in PointObjList :
                    name, color, pointList = PointObj
                    pointMem = np.zeros ((len (pointList), 3), dtype = np.float)
                    for index, point in enumerate (pointList) :
                        pointMem[index,0] = point[0]
                        pointMem[index,1] = point[1]
                        pointMem[index,2] = point[2]
                    pointMem[:,0] -= xC
                    pointMem[:,1] -= yC
                    pointMem[:,2] -= zC
                    pointMem /= normFactor
                   
                    newMeshDictionary[name] = {}
                    newMeshDictionary[name]["Faces"]  = (np.arange (pointMem.shape[0])).astype (np.int)
                    newMeshDictionary[name]["Points"] =  pointMem.flatten ()
                    newMeshDictionary[name]["Color"] = (color.red ()/255.0, color.green ()/255.0, color.blue ()/255.0)
                    
                self._objMeshDictionary = newMeshDictionary        
                self._objMeshNameDrawList = list (newMeshDictionary.keys ())
            
            self.ui.ObjectList.clear ()
            objectNameList = sorted (self._objMeshNameDrawList)
            for objectName in objectNameList :
                item = QListWidgetItem() 
                item.setText (objectName) 
                color  = RoiDefs.getROIColor (objectName)                
                item.setForeground (color)
                self.ui.ObjectList.addItem (item)
            try :
                self.ui.ObjectList.selectionModel().selectionChanged.disconnect(self.ObjectListSelectionChanged)     
                reconnectSelectionChange = True
            except:
                reconnectSelectionChange = False     
            for itemIndex in range (self.ui.ObjectList.count ()) :
                self.ui.ObjectList.item (itemIndex).setSelected (True)
            if reconnectSelectionChange :
                self.ui.ObjectList.selectionModel().selectionChanged.connect(self.ObjectListSelectionChanged)     
        finally:
            progdialog.close ()
            parentWindow._ProjectDataset.getApp ().processEvents ()
        self._modelSet = True        
        return True