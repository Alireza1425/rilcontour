#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  7 17:34:18 2019

@author: m160897
"""

from PyQt5 import QtCore
from PyQt5.QtWidgets import   QDialog
from rilcontourlib.ui.qt_ui_autogen.RCC_FileCheckinDialogAutogen import Ui_CheckinFileDlg
from rilcontourlib.ui.qt_ui_autogen.RCC_FileHistoryDialogAutogen import Ui_FileHistoryDlg
from rilcontourlib.ui.qt_ui_autogen.RCC_AssignFileLockDlgAutogen import Ui_AssignFileLockDlg
from rilcontourlib.util.rcc_util import ResizeWidgetHelper
from rilcontourlib.util.DateUtil import TimeUtil, DateUtil



class FilePLockUI  :
    
    @staticmethod
    def showCheckinPLockDialog (parent):
        dlg = CheckinPLockDialog (parent)
        if (dlg.exec_ () == QDialog.Accepted) :
            return True, dlg.getComment ()
        return False, None
        
    @staticmethod
    def showFilePLockHistoryDlg (parent, filehandle) :
        dlg = ViewFilePLockHistoryDlg (parent, filehandle) 
        dlg.exec_ ()
        return

    @staticmethod
    def showAssignPLockDlg (parent, filehandle, IncludeSelf = False) :
        assignments = filehandle.getMultuserFileSharingAssignments (IncludeSelf = IncludeSelf)
        
        manager = filehandle.getMultiUserFileSharingManager ()
        
        if len (assignments) == 0 :
            return False,None
        previousFileOwner = filehandle.getPreviousPlockOwner ()
        dlg = AssignFileDialog (parent, assignments, manager.getUserNickname(previousFileOwner), MutipleFiles = False) 
        if (dlg.exec_ () == QDialog.Accepted) :
            return True, (manager.correctNickNameIfNecessary (dlg.getAssignedTo ()), dlg.getComment ())
        return False,None
  
    @staticmethod
    def showMultiFileAssignPLockDlg (parent, assignments, manager) :
        if len (assignments) == 0 :
            return False,None
        dlg = AssignFileDialog (parent, assignments, None, MutipleFiles = True) 
        if (dlg.exec_ () == QDialog.Accepted) :
            return True, (manager.correctNickNameIfNecessary (dlg.getAssignedTo ()),dlg.getAssignedToPreviousUser (), dlg.getComment ())
        return False,None
  
class CheckinPLockDialog (QDialog) :
    
    def __init__ (self,  parent) :
        QDialog.__init__ (self, parent)
        self._parentWindow = parent
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)        
        self.ui = Ui_CheckinFileDlg ()        
        self.ui.setupUi (self)          
        self.setWindowTitle ("Check-in File")
        self._resizeWidgetHelper = ResizeWidgetHelper ()
        self.ui.OkBtn.clicked.connect (self._OkBtnPressed)
        self.ui.CancelBtn.clicked.connect (self.reject)
        self._Comment = ""

    def _internalWindowResize (self, newsize):                            
        if (self._resizeWidgetHelper != None) :
            self._resizeWidgetHelper.setWidth  (self.ui.CommentLbl, newsize.width () - 39)         
            self._resizeWidgetHelper.setWidth  (self.ui.Comment, newsize.width () - 19)            
            self._resizeWidgetHelper.setHeight  (self.ui.Comment, newsize.height () - 92)                   
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.OkBtn, newsize.width () - 240)         
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.CancelBtn, newsize.width () - 130)            
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.OkBtn, newsize.height () - 47)    
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.CancelBtn, newsize.height () - 47)    
    
    def _OkBtnPressed (self) :
        self._Comment = self.ui.Comment.toPlainText()
        self.accept  ()
        
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)                 
        self._internalWindowResize ( qresizeEvent.size () ) 

    def getComment (self) :
        return self._Comment



class ViewFilePLockHistoryDlg (QDialog) :
    
    def __init__ (self,  parent, fileHandle) :
        QDialog.__init__ (self, parent)
        self._parentWindow = parent
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)        
        self.ui = Ui_FileHistoryDlg ()        
        self.ui.setupUi (self)          
        self._resizeWidgetHelper = ResizeWidgetHelper ()
        self.ui.RollBackBtn.setVisible (False)
        self.setWindowTitle ("File Assignment History")
        self.ui.CancelBtn.setText ("Ok")
        self.ui.CancelBtn.clicked.connect (self.accept)
        self.ui.HistoryFrame.resizeEvent = self._resizeHistoryFrame
        self.ui.StatusFrame.resizeEvent = self._resizeStatusFrame
        self._fileHandle = fileHandle
        
        historyList = fileHandle.getPersistentLockHistory (IgnoreNonPersistantFileLock = True)
        for pLockData in historyList :
            isUser = True
            userName = pLockData.getUserName ()
            if userName is not None :
                userName = self._fileHandle.getUserNickname (userName)
            elif userName is None :
                userName = pLockData.getGroupName ()
                isUser = False
                if userName is None :
                    userName = "None"
            
            if not pLockData.isClosed () :
                date = DateUtil.dateToString (pLockData.getCreatedDate ())
                time = TimeUtil.timeToString (pLockData.getCreatedTime ())
                dt = "%s at %s" % (date, time) 
                msg = "Assigned to %s on %s" % (userName, dt)
            else:
                date = DateUtil.dateToString (pLockData.getClosedDate ())
                time = TimeUtil.timeToString (pLockData.getClosedTime ())
                dt = "%s at %s" % (date, time) 
                if isUser :
                    msg = "Closed by %s on %s" % (userName, dt) 
                else:
                    msg = "Removed from %s on %s" % (userName, dt) 
            self.ui.FileList.addItem (msg)
            self.ui.FileList.item(self.ui.FileList.count () - 1).setData (QtCore.Qt.UserRole, pLockData)
               

        if self.ui.FileList.count () > 0 :
            lastIndex = self.ui.FileList.count () - 1
            self.ui.FileList.setCurrentRow (lastIndex)
            self.ui.FileList.item(lastIndex).setSelected(True)
        self.ui.FileList.currentRowChanged.connect (self._rowSelectionChanged)
        self._rowSelectionChanged ()
      
    def _rowSelectionChanged (self):
        if self.ui.FileList.count () == 0 :
           msg = ["<html>"]
           msg = ["<b>File has not been checked out</b>"]
           msg += ["</html>"]
           self.ui.Comment.setText ("<br>\n".join (msg))
           return
       
        try:
            currentItem = self.ui.FileList.currentItem ()
            pLockData = currentItem.data (QtCore.Qt.UserRole)
            msg = ["<html>"]
            msg += ["<b>File: </b>" + pLockData.getDocumentPath ()]
            msg += [""]
            userName = pLockData.getUserName ()
            if userName is not None :
                userName = self._fileHandle.getUserNickname (userName)
            elif userName is None :
                userName = pLockData.getGroupName ()
            date = DateUtil.dateToString (pLockData.getCreatedDate ())
            time = TimeUtil.timeToString (pLockData.getCreatedTime ())
            dt = "%s at %s" % (date, time) 
            
            msg += ["<b>Assigned to</b>: " + userName]
            msg += ["<b>Checkout date/time: </b>" + dt]
            uid = pLockData.getOpenedVersionUID ()
            fpath = None
            if uid is None :
                versionNumber = "No previous file version"
            else:
                versionNumber, fpath = self._fileHandle.getVersionNumberAndPathFromUID (uid)
                if versionNumber is None :
                    versionNumber = "Unknown"
                    fpath = None
            
            msg += ["<b>File version checked out: </b>" + str (versionNumber)]
            if fpath is not None :
                msg += ["<b>File version path: </b>" + fpath]
            if  pLockData.isClosed () :
                date = DateUtil.dateToString (pLockData.getClosedDate ())
                time = TimeUtil.timeToString (pLockData.getClosedTime ())
                dt = "%s at %s" % (date, time) 
                msg += [""]
                msg += ["<b>Check-in date/time: </b>" + dt]
                uid = pLockData.getClosedVersionUID ()
                versionNumber,fpath = self._fileHandle.getVersionNumberAndPathFromUID (uid)
                if versionNumber is None :
                    versionNumber = "Unknown"
                    fpath = None 
                msg += ["<b>File version checked in: </b>" + str (versionNumber)]
                if fpath is not None :
                    msg += ["<b>File version path: </b>" + fpath]
            openComment = pLockData.getOpenComment ()
            if openComment != "" :
                msg += ["<hr>"]
                msg += ["<b>Open comments</b>"]
                msg += [""]
                msg += [openComment]
            closeComment = pLockData.getCloseComment ()
            if closeComment != "" :
                msg += ["<hr>"]
                msg += ["<b>Check-in comments</b>"]
                msg += [""]
                msg += [closeComment]
            msg += ["</html>"]
            self.ui.Comment.setText ("<br>\n".join (msg))
        except:
            self.ui.Comment.setText ("A error occured accessing file lock status")
        
    def _resizeHistoryFrame (self, position) : 
        self._resizeWidgetHelper.setWidth  (self.ui.FileList, self.ui.HistoryFrame.width ()-14)         
        self._resizeWidgetHelper.setHeight  (self.ui.FileList, self.ui.HistoryFrame.height ()-20)
            
    def _resizeStatusFrame (self, position) : 
        self._resizeWidgetHelper.setWidth  (self.ui.CommentLbl, self.ui.StatusFrame.width ()-29)         
        self._resizeWidgetHelper.setWidth  (self.ui.Comment, self.ui.StatusFrame.width ()-14)
        self._resizeWidgetHelper.setHeight  (self.ui.Comment, self.ui.StatusFrame.height ()-44)
        
    def _internalWindowResize (self, newsize):                            
        if (self._resizeWidgetHelper != None) :
            self._resizeWidgetHelper.setWidth  (self.ui.Splitter, newsize.width () - 11)         
            self._resizeWidgetHelper.setHeight  (self.ui.Splitter, newsize.height () - 66)                   
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.RollBackBtn, newsize.width () - 242)         
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.CancelBtn, newsize.width () - 132)            
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.RollBackBtn, newsize.height () - 46)    
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.CancelBtn, newsize.height () - 46)    
        
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)                 
        self._internalWindowResize ( qresizeEvent.size () ) 

   
    
class AssignFileDialog (QDialog) :
    
    def __init__ (self,  parent, fileassignment, previousOwner, MutipleFiles = False, windowTitle = None) :
        QDialog.__init__ (self, parent)
        self._parentWindow = parent
        if windowTitle is not None :
            self.setWindowTitle (windowTitle)
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)        
        self.ui = Ui_AssignFileLockDlg ()        
        self.ui.setupUi (self)          
        self._resizeWidgetHelper = ResizeWidgetHelper ()
        self.ui.OkBtn.clicked.connect (self._OkBtnPressed)
        self.ui.CancelBtn.clicked.connect (self.reject)
        self._Comment = ""
        self._AssignTo = None
        self._assignToPreviousFileHolder = None
        for userName in fileassignment :
            self.ui.AssignToCombobox.addItem (userName)
        if MutipleFiles :
             self.ui.PreviousOwnerBtn.setEnabled (True)
             self.ui.PreviousOwnerLbl.setText ("Previous owner: multiple files selected")
             self._PreviousOwnerUserName = "Multiple Files Selected"
        else:
            self._PreviousOwnerUserName = previousOwner
            if previousOwner is None :
                self.ui.PreviousOwnerLbl.setText ("Previous owner: None")
                previousOwner = "none"
            else:
                self.ui.PreviousOwnerLbl.setText ("Previous owner: " + previousOwner)
                previousOwner = previousOwner.lower ()
            self._PreviousOwnerUserName = previousOwner
            self.ui.PreviousOwnerBtn.setEnabled (previousOwner in fileassignment)
        self.ui.PreviousOwnerBtn.clicked.connect (self._setToPrevousOwnerPressed)
      
    def _internalWindowResize (self, newsize):                            
        if (self._resizeWidgetHelper != None) :
            self._resizeWidgetHelper.setWidth  (self.ui.PreviousOwnerLbl, newsize.width () - 39)   
            self._resizeWidgetHelper.setWidth  (self.ui.AssignToCombobox, newsize.width () - 129)
            self._resizeWidgetHelper.setWidth  (self.ui.CommentLbl, newsize.width () - 29)   
            self._resizeWidgetHelper.setWidth  (self.ui.Comment, newsize.width () - 19)            
            self._resizeWidgetHelper.setHeight  (self.ui.Comment, newsize.height () - 184)
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.OkBtn, newsize.width () - 240)         
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.CancelBtn, newsize.width () - 130) 
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.OkBtn, newsize.height () - 45)    
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.CancelBtn, newsize.height () - 45)   
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.PreviousOwnerBtn, newsize.height () - 45)   
    
    def getAssignedToPreviousUser (self):
        return self._assignToPreviousFileHolder 
        
    def _setToPrevousOwnerPressed (self) :
        self._Comment = self.ui.Comment.toPlainText ()
        self._AssignTo = self._PreviousOwnerUserName
        self._assignToPreviousFileHolder = True
        self.accept  ()
        
    def _OkBtnPressed (self) :
        self._Comment = self.ui.Comment.toPlainText ()
        self._AssignTo = self.ui.AssignToCombobox.currentText ()
        self.accept  ()
        
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)                 
        self._internalWindowResize ( qresizeEvent.size () ) 

    def getComment (self) :
        return self._Comment
    
    def getAssignedTo (self) :
        return self._AssignTo

   


