#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 17 11:49:59 2017

@author: m160897
"""
from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import   QDialog, QComboBox, QItemDelegate,  QAbstractItemView, QTextEdit
from PyQt5.QtGui import QStandardItemModel
from PyQt5.QtCore import QModelIndex#, QAbstractTableModel
from six import *
from rilcontourlib.ui.qt_ui_autogen.RCC_DatasetTagEditorAutogen import Ui_TagInterfaceDlg
from rilcontourlib.ui.qt_ui_autogen.RCC_TagEditorAutoGen import Ui_RCC_TagEditorDlg
from rilcontourlib.ui.qt_ui_autogen.RCC_ProjectTagEditorAutogen import Ui_RCC_ProjectTagEditorDlg
import os
from rilcontourlib.util.rcc_util import ResizeWidgetHelper, Tag, MessageBoxUtil




# Dataset tag editor

class RCC_ProjectTagEditorDlg (QDialog) :   
    
    def __init__ (self, ProjectDataset, datasetTagManager, parent, projectDescriptions, projectDescriptionContext) :
        QDialog.__init__ (self, None)
        self._ProjectDataset = ProjectDataset
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)         
        self.ui = Ui_RCC_ProjectTagEditorDlg ()        
        self.ui.setupUi (self)       
        self._datasetTagManager = datasetTagManager.copy ()
                
        projectDescriptionTag = Tag  (self._datasetTagManager._getID (),"Description","Description","Unknown", ParamaterList=projectDescriptions, Override_Tactic_Context = projectDescriptionContext)
        self._datasetTagManager.insertTagAtFront (projectDescriptionTag)        
        
        self._resizeWidgetHelper = ResizeWidgetHelper ()        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.TagList)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.AddTagBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.RemoveTagBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.OkBtn)                
        
        self._rebuildList()
        
        self.ui.TagList.doubleClicked.connect (self._taglistdblclick)
        self.ui.AddTagBtn.clicked.connect (self._addTag)
        self.ui.RemoveTagBtn.clicked.connect (self._removeTag)
        self.ui.OkBtn.clicked.connect (self._okBtn)
        
        basepath = self._ProjectDataset.getPythonProjectBasePath ()
        iconPath = os.path.join (basepath , "icons") 
        self.ui.MoveUpBtn.setIcon(QtGui.QIcon(iconPath + os.sep + "up.png"))
        self.ui.MoveUpBtn.setIconSize(QtCore.QSize(20,20))        
        self.ui.MoveDownBtn.setIcon(QtGui.QIcon(iconPath + os.sep + "down.png"))
        self.ui.MoveDownBtn.setIconSize(QtCore.QSize(20,20))   
        self.ui.MoveDownBtn.setText ("")
        self.ui.MoveUpBtn.setText ("")
        self.ui.MoveUpBtn.clicked.connect (self._moveUp)
        self.ui.MoveDownBtn.clicked.connect (self._moveDown)
        self.ui.MoveUpBtn.setEnabled (False)
        self.ui.MoveDownBtn.setEnabled (False)
        
        basepath = self._ProjectDataset.getPythonProjectBasePath ()
        iconPath = os.path.join (basepath , "icons") 
        self.ui.AddTagBtn.setIcon(QtGui.QIcon(iconPath + os.sep + "plus.png"))
        self.ui.AddTagBtn.setIconSize(QtCore.QSize(20,20))        
        self.ui.RemoveTagBtn.setIcon(QtGui.QIcon(iconPath + os.sep + "minus.png"))
        self.ui.RemoveTagBtn.setIconSize(QtCore.QSize(20,20))
        self.ui.RemoveTagBtn.setEnabled (self.ui.TagList.currentRow () is not None and self.ui.TagList.count () > 1)
        self.ui.TagList.currentRowChanged.connect (self._selectionchanged) 
        
        
    def _selectionchanged (self, index) :
        self.ui.RemoveTagBtn.setEnabled (self.ui.TagList.currentRow () is not None and self.ui.TagList.count () > 1 and self.ui.TagList.currentRow () > 0)        
        index = self.ui.TagList.currentRow ()
        self.ui.MoveUpBtn.setEnabled (index is not None and index > 1)
        self.ui.MoveDownBtn.setEnabled (index is not None and index >= 1 and index < self._datasetTagManager.getCustomTagCount ()-1)
        
    def isROIDataStoredInTatic (self) : 
        if (self._ProjectDataset is not None):
            return self._ProjectDataset.isROIDataStoredInTatic ()
        return False
    
    def _moveUp (self) :
        self._moveTag (-1)
        
    def _moveDown (self) :
        self._moveTag (1)
        
    def _rebuildList (self):
        self.ui.TagList.clear ()        
        for tagIndex in range (self._datasetTagManager.getCustomTagCount ()) :
            tag = self._datasetTagManager.getCustomTagByIndex (tagIndex)
            uistr = tag.getUIString ()
            self.ui.TagList.addItem (uistr)
            
    def _internalWindowResize (self, newsize):                            
        if (self._resizeWidgetHelper != None) :                                               
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.TagList, newsize)            
            self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.TagList, newsize)                   
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.OkBtn, newsize.height () - 45)            
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.OkBtn, newsize.width () - 106)                   
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.RemoveTagBtn, newsize.height () - 50)                        
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.AddTagBtn, newsize.height () - 50)            
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.MoveUpBtn, newsize.height () - 50)                        
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.MoveDownBtn, newsize.height () - 50)            
            
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)                 
        self._internalWindowResize ( qresizeEvent.size () ) 
    
    def _taglistdblclick (self) :
        row = self.ui.TagList.currentRow()
        if (row >= 0) :
            tag = self._datasetTagManager.getCustomTagByIndex (row)
            if (tag != None) :
                tagEditorDlg = RCC_TagEditorDlg (self, InitalizeWithTagValues = tag, DataSetStoredInTactic = self.isROIDataStoredInTatic (), ProjectContext = self._ProjectDataset.getProjectName ())                        
                if (QDialog.Accepted == tagEditorDlg.exec_ ()) :
                    tag = tagEditorDlg.getTag ()
                    if tag is not None :
                        if (tag.getID () is None) : 
                            tag.setID (self._datasetTagManager._getID ())
                        self._datasetTagManager.setCustomTagByIndex (row, tag)
                        self._rebuildList()
                        self.ui.TagList.setCurrentRow (row)
        
    def getTagDataSetManager (self) :
        tag = self._datasetTagManager.popFrontTag ()
        return (self._datasetTagManager, tag.getParameterList (), tag.getTagTacticContext(self._ProjectDataset.getProjectName ()))
        
    def _moveTag (self,direction) :
        if (direction == 1 or direction == -1) :
            row = self.ui.TagList.currentRow()
            newrow = row + direction
            customTagCount = self._datasetTagManager.getCustomTagCount ()
            if (row >= 1 and newrow >= 1 and row < customTagCount and newrow < customTagCount) :
                selectedTag = self._datasetTagManager.getCustomTagByIndex (row)
                swapTag = self._datasetTagManager.getCustomTagByIndex (newrow)
                self._datasetTagManager.setCustomTagByIndex (row, swapTag)
                self._datasetTagManager.setCustomTagByIndex (newrow, selectedTag)
                self._rebuildList()
                self.ui.TagList.setCurrentRow (newrow)
        
    def _addTag (self) :
        tagEditorWindow = RCC_TagEditorDlg (self, DataSetStoredInTactic = self.isROIDataStoredInTatic (), ProjectContext = self._ProjectDataset.getProjectName ())                        
        if (QDialog.Accepted == tagEditorWindow.exec_ ()) :
             tag = tagEditorWindow.getTag ()             
             if (tag is not None) :
                 self._datasetTagManager.addTag (tag)
                 self._rebuildList()
                 self.ui.TagList.setCurrentRow (self.ui.TagList.count ()-1)
        self.ui.RemoveTagBtn.setEnabled (self.ui.TagList.currentRow () is not None and self.ui.TagList.count () > 1)
    
    def _removeTag (self) :
        row = self.ui.TagList.currentRow()
        if (row >= 1) :
            customTag = self._datasetTagManager.getCustomTagByIndex (row)
            self._datasetTagManager.removeTag (customTag)        
            self._rebuildList()
        self.ui.RemoveTagBtn.setEnabled (self.ui.TagList.currentRow () is not None and self.ui.TagList.count () > 1)
        
    def _okBtn (self):
        self.accept ()

# Dataset tag editor

class RCC_TagEditorDlg (QDialog) :   
    
    def __init__ (self, parent, InitalizeWithTagValues = None, DataSetStoredInTactic = True, ProjectContext = "") :        
        QDialog.__init__ (self, parent)
        self._ProjectTacticContext = ProjectContext
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)         
        self.ui = Ui_RCC_TagEditorDlg ()        
        self.ui.setupUi (self)       
        
        self._freeTxtRadioBtn ()                        
        self.ui.defaultCheckState.setChecked (False)     
        self.ui.defaultCheckState.stateChanged.connect (self.updatedcheckboxtxt)
        self.ui.defaultFreeText.setText ("") 
        self.ui.ListOptions.setText ("")
        self._tagID = None
        
        #handle Description Tag special requires Flag
        self._isDescriptionType = False
        
        if (InitalizeWithTagValues is not None) :
            self._tagID = InitalizeWithTagValues.getID ()
            self.ui.tagName.setText (InitalizeWithTagValues.getName ())
            self.ui.mangleTagNameWithTacticContext.setChecked (InitalizeWithTagValues.shouldMangleTagNameWithTaticContext ())
            self.ui.TagTacticContextTxt.setText (InitalizeWithTagValues.getTagTacticContext (ProjectContext))
            tagtype = InitalizeWithTagValues.getType ()            
            if (tagtype == "Text") :
                self._freeTxtRadioBtn ()                
            elif (tagtype == "ItemList") :
                self._listRadioBtn ()                
            elif (tagtype == "Checked") :
                self._checkBoxRadioBtn ()
            elif (tagtype == "Description") :
                self._listRadioBtn ()     
                self._isDescriptionType = True
            else:
                print ("Unrecongized tag name")
            defaultValue = InitalizeWithTagValues.getDefaultValue()                
            self.ui.defaultCheckState.setChecked (defaultValue == "Yes") 
            self.ui.defaultFreeText.setText (defaultValue)         
            paramList = InitalizeWithTagValues.getParameterList ()
            defaultParameterTxt = ""
            first = True
            
            if self._isDescriptionType :        
                if (len (paramList) == 0) :
                    paramList = ["Unknown"]
                elif (paramList[0] != "Unknown") :
                    paramList = ["Unknown"] + paramList
                    
            for param in paramList :
                txt = param.strip ()
                if len (txt) > 0 :
                    if not first :
                        defaultParameterTxt += "\n"
                    defaultParameterTxt += param
                    first = False                    
            self.ui.ListOptions.setText (defaultParameterTxt)
                
        self._resizeWidgetHelper = ResizeWidgetHelper ()
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.tagName)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.optionsGroupBox)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.OptionsGroupBoxLabel)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.mangleTagNameWithTacticContext)        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.tagTypeGroupBox)        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.tagTypeStackedWidget)        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ListOptions)        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.defaultFreeText)        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.TagTacticContextTxt)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.okBtn)        

        self.ui.mangleTagNameWithTacticContext.setChecked (True)        
        self.ui.tagName.setEnabled (not self._isDescriptionType)
        self.ui.mangleTagNameWithTacticContext.setEnabled (DataSetStoredInTactic and not self._isDescriptionType)
        self.ui.TagTacticContextTxt.setEnabled (DataSetStoredInTactic)
        
        self.ui.OptionsGroupBoxLabel.setEnabled (DataSetStoredInTactic and not self._isDescriptionType)
        self.ui.label_6.setEnabled (DataSetStoredInTactic and not self._isDescriptionType)
        self.ui.optionsGroupBox.setEnabled (DataSetStoredInTactic)
        
        self.ui.freeTextRadioBtn.setEnabled (not self._isDescriptionType)    
        self.ui.checkBoxRadioBtn.setEnabled (not self._isDescriptionType)
        self.ui.listRadioBtn.setEnabled (not self._isDescriptionType)        
        if not self._isDescriptionType :            
            self.ui.freeTextRadioBtn.clicked.connect (self._freeTxtRadioBtn)
            self.ui.checkBoxRadioBtn.clicked.connect (self._checkBoxRadioBtn)
            self.ui.listRadioBtn.clicked.connect (self._listRadioBtn)
        self._internalTag = None
        self.ui.okBtn.clicked.connect (self._okBtn)
     
    def updatedcheckboxtxt (self, state):
       if (state == 0) :
           self.ui.defaultCheckState.setText ("Un-Checked") 
       else:
           self.ui.defaultCheckState.setText ("Checked") 
    
    def _internalWindowResize (self, newsize):                            
        if (self._resizeWidgetHelper != None) :                       
            self._resizeWidgetHelper.setAbsoluteWidth (self.ui.TagTacticContextTxt, newsize)                   
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.mangleTagNameWithTacticContext, newsize)            
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.tagName, newsize)            
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.optionsGroupBox, newsize)            
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.OptionsGroupBoxLabel, newsize)            
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.tagTypeGroupBox, newsize)            
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.tagTypeStackedWidget, newsize)            
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.ListOptions, newsize)            
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.defaultFreeText, newsize)                        
            self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.tagTypeGroupBox, newsize)            
            self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.tagTypeStackedWidget, newsize)            
            self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.ListOptions, newsize)            
            self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.defaultFreeText, newsize)                                    
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.okBtn, newsize.height () - 40)            
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.okBtn, newsize.width () - 121)                   
            
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)                 
        self._internalWindowResize ( qresizeEvent.size () ) 
    
    def _freeTxtRadioBtn (self) :
        self.ui.listRadioBtn.setChecked (False)
        self.ui.checkBoxRadioBtn.setChecked (False)        
        self.ui.freeTextRadioBtn.setChecked (True)
        self.ui.tagTypeStackedWidget.setCurrentIndex (2)

    def _checkBoxRadioBtn (self) :
        self.ui.listRadioBtn.setChecked (False)
        self.ui.checkBoxRadioBtn.setChecked (True)        
        self.ui.freeTextRadioBtn.setChecked (False)
        self.ui.tagTypeStackedWidget.setCurrentIndex (0)
        
    def _listRadioBtn (self) :
        self.ui.listRadioBtn.setChecked (True)
        self.ui.checkBoxRadioBtn.setChecked (False)        
        self.ui.freeTextRadioBtn.setChecked (False)
        self.ui.tagTypeStackedWidget.setCurrentIndex (1)             
        
    def getTag (self) :
        if (self._internalTag is not None) :
            return self._internalTag[0]
        
        tagName = self.ui.tagName.text ().strip ()
        if (tagName == "") :
            return None
                        
        if self.ui.listRadioBtn.isChecked () :
            if self._isDescriptionType :        
                tagtype = "Description"            
            else:
                tagtype = "ItemList"            
            ParamaterList = []
            txt = self.ui.ListOptions.toPlainText() 
            txtlines = txt.split ("\n")
                    
            for line in txtlines :
                val = line.strip ()
                if len (val) > 0 :
                   ParamaterList.append (val)
            
            if self._isDescriptionType :        
                if (len (ParamaterList) == 0) :
                    ParamaterList = ["Unknown"]
                elif (ParamaterList[0] != "Unknown") :
                    ParamaterList = ["Unknown"] + ParamaterList
                
            if (len (ParamaterList) > 0) :
                defaultValue = ParamaterList[0]
            else:
                defaultValue = ""
        elif self.ui.checkBoxRadioBtn.isChecked () :
            tagtype = "Checked"
            if (self.ui.defaultCheckState.isChecked ()) :
                defaultValue = "Yes"
            else:
                defaultValue = "No"
            ParamaterList = []
        elif self.ui.freeTextRadioBtn.isChecked () :
            tagtype = "Text"
            defaultValue = self.ui.defaultFreeText.toPlainText() 
            ParamaterList = []
        else:
            print ("Error")
        
        internalTag = False
        storeinTatic = True
        mangleTagNameWithTaticContext = self.ui.mangleTagNameWithTacticContext.isChecked ()
        Override_Tactic_Context = self.ui.TagTacticContextTxt.text ().strip ()
        if len (Override_Tactic_Context) == 0 :
            Override_Tactic_Context = self._ProjectTacticContext
        return Tag (self._tagID, tagName, tagtype, defaultValue, internalTag, storeinTatic, mangleTagNameWithTaticContext, ParamaterList, Override_Tactic_Context = Override_Tactic_Context)
                
    def _okBtn (self):        
        self._internalTag = (self.getTag (),)
        self.accept ()






# Dataset tag UI Manager


class QListDelegate (QItemDelegate) :    
    def __init__ (self, taginterface, defaultValue, parameterList, parent) :
         QItemDelegate.__init__ (self, parent)
         self._tagInterface = taginterface  
         self._defaultV = defaultValue
         self._parameterList = []
         for item in parameterList :
             self._parameterList.append (item)
     
    def _setTagByIndex (self, index, tag) :
        if (self._tagInterface is not None) :
            self._tagInterface._setTagByIndex (index, tag)
            
    def _getTagByIndex (self, index) :
        if (self._tagInterface is None) :
            return None
        return self._tagInterface._getTagByIndex (index)    
    
    def createEditor(self, parent, optiosn, qModelInde) :       
        editor = QComboBox (parent)
        editor.setAutoFillBackground(True)
        for x in self._parameterList :
            editor.addItem (str (x))    
        index = editor.findText (str(self._defaultV))        
        editor.setCurrentIndex(index) 
        return editor
        
    def setEditorData(self, qWidgetEditor, qModelIndex) :
        # Get the value via index of the Model
        value = qModelIndex.model().data(qModelIndex, QtCore.Qt.EditRole)
        # Put the value into the SpinBox
        index = qWidgetEditor.findText (str(value))        
        qWidgetEditor.setCurrentIndex(index) 
        #self._defaultV = value
        
    def setModelData(self,  qWidgetEditor, qAbstractModel, qModelIndex) :        
        currentIndex = qWidgetEditor.currentIndex()        
        value = qWidgetEditor.itemText (currentIndex)
        qAbstractModel.setData(qModelIndex, value, QtCore.Qt.EditRole)
        row = qModelIndex.row ()
        tag = self._getTagByIndex (row)  
        tag.setValue (value)
        self._setTagByIndex  (row, tag)
        
    def updateEditorGeometry(self, qWidgetEditor, qStyleOptionViewItem, qModelIndex) :
        qWidgetEditor.setGeometry(qStyleOptionViewItem.rect)

class QLineEditDelegate (QItemDelegate) :    
    def __init__ (self, taginterface, defaultValue, parent) :
         QItemDelegate.__init__ (self, parent)
         self._tagInterface = taginterface  
         self._defaultV = defaultValue        
        
    def _setTagByIndex (self, index, tag) :
        if (self._tagInterface is not None) :
            self._tagInterface._setTagByIndex (index, tag)
            
    def _getTagByIndex (self, index) :
        if (self._tagInterface is None) :
            return None
        return self._tagInterface._getTagByIndex (index)     
    
    def createEditor(self, parent, optiosn, qModelInde) :       
        editor = QTextEdit (parent)       
        editor.setText (self._defaultV)
        editor.setAutoFillBackground(True)        
        return editor        
    
    def setEditorData(self, qWidgetEditor, qModelIndex) :
        # Get the value via index of the Model
        value = qModelIndex.model().data(qModelIndex, QtCore.Qt.EditRole)        
        value = str (value)
        qWidgetEditor.setText(value)
        #self._defaultV = value
        
    def setModelData(self,  qWidgetEditor, qAbstractModel, qModelIndex) :        
        value = qWidgetEditor.toPlainText()
        qAbstractModel.setData(qModelIndex, value, QtCore.Qt.EditRole)
        row = qModelIndex.row ()
        tag = self._getTagByIndex (row)  
        tag.setValue (value)
        self._setTagByIndex  (row, tag)        
    
    def updateEditorGeometry(self, qWidgetEditor, qStyleOptionViewItem, qModelIndex) :
        qWidgetEditor.setGeometry(qStyleOptionViewItem.rect)


class MyModel(QStandardItemModel) :
    
    def __init__ (self,  parent) :        
        self._tagInterface = parent        
        row = self._getTagCount () 
        QStandardItemModel.__init__ (self, row, 1, parent)                
    
    def _getTagCount (self) :
        if (self._tagInterface is None) :
            return 0
        return self._tagInterface._getTagCount ()
    
    def _setTagByIndex (self, index, tag) :
        if (self._tagInterface is not None) :
            self._tagInterface._setTagByIndex (index, tag)
            
    def _getTagByIndex (self, index) :
        if (self._tagInterface is None) :
            return None
        return self._tagInterface._getTagByIndex (index)    
    
    def flags (self, qModelIndex) :
        #return QtCore.Qt.ItemIsSelectable
        if (self._getTagCount () == 0) :
            return 0
        row = qModelIndex.row ()
        tag = self._getTagByIndex(row)
        testType = tag.getType()
        isInternalType = tag.isInternalTag () # internal types should not be editable
        if (testType == "Checked"):
            return QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled
        elif (testType == "Text"):
            if (not isInternalType) :
                return  QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable 
            else:
                return  QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled  
        elif (testType == "ItemList"):
            if (not isInternalType) :
                return  QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable 
            else:
                return  QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled
        else:
            print ("Invalid Item Type")
   
    def setData(self, QModelIndex, value, role):        
        if (role == QtCore.Qt.CheckStateRole):                
            currentData = self.data(QModelIndex,QtCore.Qt.EditRole)
            if (currentData == "Yes") :
                setValue = "No"                                
            else:
                setValue = "Yes"                             
            self.setData(QModelIndex,setValue, QtCore.Qt.EditRole)                  
            tag = self._getTagByIndex (QModelIndex.row ())
            tag.setValue (setValue)
            self._setTagByIndex (QModelIndex.row (), tag)
        return  QStandardItemModel.setData (self, QModelIndex, value, role)                
        
    
    def data(self, qModelIndex, role) :            
        if (self._getTagCount () == 0) :
            return None
        if (role == QtCore.Qt.CheckStateRole) :                    
            row = qModelIndex.row ()
            tag = self._getTagByIndex (row)
            if (tag.getType () == "Checked"):
                data = QStandardItemModel.data(self, qModelIndex, QtCore.Qt.EditRole)                                    
                if (data == "Yes") :
                    return QtCore.Qt.Checked                
                else:
                    return QtCore.Qt.Unchecked
        return QStandardItemModel.data(self, qModelIndex, role)        


class RCC_DatasetTagingDlg (QDialog) :   
    
    def __init__ (self, ProjectDataset, parent) :
        QDialog.__init__ (self, parent)        
        self._TableMouseEventsInitalized = False
        self._ProjectDataset = ProjectDataset
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, False)         
        self.ui = Ui_TagInterfaceDlg ()        
        self.ui.setupUi (self)       
        
        self._showCustomDataSetTags = False 
        self._InternalDataSetTags  = False
        
        self._resizeWidgetHelper = ResizeWidgetHelper ()
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.tableView)
        #self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.closeButton)
        #self.ui.closeButton.clicked.connect (self._closeDlg)
        
        #basepath = self._ProjectDataset.getPythonProjectBasePath ()
        #iconPath = os.path.join (basepath , "icons") 
        #self.ui.AddTagBtn.setIcon(QtGui.QIcon(iconPath + os.sep + "plus.png"))
        #self.ui.AddTagBtn.setIconSize(QtCore.QSize(20,20))        
        #self.ui.RemoveTagBtn.setIcon(QtGui.QIcon(iconPath + os.sep + "minus.png"))
        #self.ui.RemoveTagBtn.setIconSize(QtCore.QSize(20,20))
        #self.ui.RemoveTagBtn.setEnabled (False)
        self._tagDataManager = None
        #self.ui.AddTagBtn.clicked.connect (self._addTag)
        #self.ui.RemoveTagBtn.clicked.connect (self._removeTag)
        self._UIDialog = parent   
        self.setWindowFlags(self.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)         
        
       
    def _tabelSelectionChanged (self, selected, deselected) :        
        #self.ui.RemoveTagBtn.setEnabled (self._showCustomDataSetTags and self.ui.tableView.selectionModel() is not None and self.ui.tableView.selectionModel().hasSelection () and self._tableModel.rowCount () > 0)        
        pass
    
    #def setEnabled (self, val) :
    #    super ()
        
    def isROIDataStoredInTatic (self) : 
        if (self._ProjectDataset is not None):
            return self._ProjectDataset.isROIDataStoredInTatic ()
        return False
        
    def _addTag (self) :
        tagEditorWindow = RCC_TagEditorDlg (self, DataSetStoredInTactic = self.isROIDataStoredInTatic (), ProjectContext = self._ProjectDataset.getProjectName ())                        
        if (QDialog.Accepted == tagEditorWindow.exec_ ()) :
             tag = tagEditorWindow.getTag ()             
             if tag is not None :
                 if (self.isROIDataStoredInTatic ()) :
                     tag.setValue (self._ProjectDataset.getROIDatasetInterface ().getTaticTagValue (self._ProjectDataset.getSelectedDataset (), tag.getName(), tag.getValue ()))
                     if (tag.getType () == "ItemList" and tag.getValue () not in tag.getParameterList ()):
                         tag.getParameterList ().append (tag.getValue ())
                 self._tagDataManager.addTag (tag)
                 self.setTags(self._tagDataManager, CustomDataSetTags = True)                                  
        #self.ui.RemoveTagBtn.setEnabled (self._showCustomDataSetTags and self.ui.tableView.selectionModel() is not None and self.ui.tableView.selectionModel().hasSelection () and self._tableModel.rowCount () > 0)        
        return
        
    def _removeTag (self) :
        selectionModel = self.ui.tableView.selectionModel()
        row = -1
        if (selectionModel.hasSelection ()) :            
            rowlist = [selectionModel.currentIndex ().row ()]
            if len (rowlist) > 0 :
              row = rowlist[0]        
        if (row >= 0) :
            customTag = self._tagDataManager.getCustomTagByIndex (row)
            
            self._tagDataManager.removeTag (customTag)        
            self.setTags(self._tagDataManager, CustomDataSetTags = True)                             
        #self.ui.RemoveTagBtn.setEnabled (self._showCustomDataSetTags and self.ui.tableView.selectionModel() is not None and self.ui.tableView.selectionModel().hasSelection () and self._tableModel.rowCount () > 0)        
        return 
        
    
    def _getTagCount (self) :
        if (self._tagDataManager is None):
            return 0        
        elif (self._showCustomDataSetTags):
            return self._tagDataManager.getCustomTagCount ()
        elif (self._InternalDataSetTags):
            return self._tagDataManager.getInternalTagCount ()
        else:
            print ("Error")
            return 0
    
    def _getTagByIndex (self, index) :
        if (self._tagDataManager is None):
            return None
        elif (self._showCustomDataSetTags):
            return self._tagDataManager.getCustomTagByIndex (index)
        elif (self._InternalDataSetTags):
            return self._tagDataManager.getInternalTagByIndex (index)
        else:
            print ("Error")
            return None
        
    def _setTagByIndex (self, index, tag) :
        if (self._tagDataManager is None):
            return 
        elif (self._showCustomDataSetTags):
            return self._tagDataManager.setCustomTagByIndex (index, tag)
        elif (self._InternalDataSetTags):
            return self._tagDataManager.setInternalTagByIndex (index, tag)
        else:
            print ("Error")
            return
        
    def setTags (self, datasetTagManager, CustomDataSetTags = False, InternalDataSetTags = False) :  
        if not ((CustomDataSetTags and not InternalDataSetTags) or (not CustomDataSetTags and InternalDataSetTags)) :
            print ("Param Error: One paramater must be set to true CustomDataSetTags and InternalDataSetTags cannot both be set to true or both be set to false")
            return
        if (CustomDataSetTags) :
            self.setWindowTitle ("Custom Series Annotations")
        elif (InternalDataSetTags) :
            self.setWindowTitle ("Internal Series Annotations")
        else:
            self.setWindowTitle ("Unknown Tags")
        self._showCustomDataSetTags = CustomDataSetTags
        self._InternalDataSetTags = InternalDataSetTags
        self._tagDataManager = datasetTagManager
        #DialogParameters = datasetTagManager.getTags ()
        self._oldTableRowSelection  = []
        self._tableModel = MyModel (self)
        self.ui.tableView.horizontalHeader().setStretchLastSection(True)
        self.ui.tableView.horizontalHeader().hide ()
                
        parameterNameLst = []
        tagCount = self._getTagCount ()
        if tagCount == 0 :
            self._tableModel.setVerticalHeaderLabels ([])
        else:
            for tagindex in range (tagCount) :
                tag = self._getTagByIndex (tagindex)
                parameterNameLst.append (tag.getName ())
            self._tableModel.setVerticalHeaderLabels (parameterNameLst)
                
            for tagindex in range (tagCount) :
                tag = self._getTagByIndex (tagindex)
                table_index = self._tableModel.index(tagindex,0,QModelIndex());
                self._tableModel.setData(table_index, tag.getValue (), QtCore.Qt.EditRole)            
                    
        self.ui.tableView.setModel (self._tableModel)            
        
        self.ui.tableView.setSelectionBehavior( QAbstractItemView.SelectItems );
        self.ui.tableView.setSelectionMode( QAbstractItemView.SingleSelection );        
        selectionmodel = self.ui.tableView.selectionModel()
        if (selectionmodel is not None) :
            selectionmodel.selectionChanged.connect(self._tabelSelectionChanged)
            
        #self.ui.RemoveTagBtn.setEnabled (self._showCustomDataSetTags and self.ui.tableView.selectionModel() is not None and self.ui.tableView.selectionModel().hasSelection () and self._tableModel.rowCount () > 0)        
        
        if (not self._TableMouseEventsInitalized) :
            self.ui.tableView.verticalHeader ().sectionDoubleClicked.connect (self.headerRowDoubleClicked)
            self._TableMouseEventsInitalized = True
        elif (self._tableModel.rowCount () == 0):            
            self.ui.tableView.verticalHeader ().sectionDoubleClicked.disconnect (self.headerRowDoubleClicked)
            self._TableMouseEventsInitalized = False
                    
        for tagindex in range (tagCount) :
            tag = self._getTagByIndex (tagindex)
            testType = tag.getType()
            if (testType == "ItemList") :
                self.ui.tableView.setItemDelegateForRow(tagindex, QListDelegate (self, tag.getDefaultValue (), tag.getParameterList (), self.ui.tableView))
            elif (testType == "Text") :
                self.ui.tableView.setItemDelegateForRow(tagindex, QLineEditDelegate (self, tag.getDefaultValue (), self.ui.tableView))
            elif (testType  == "Checked") :
                self.ui.tableView.setItemDelegateForRow(tagindex, None)
            else:
                print ("Invalid parameter type.")        

    def headerRowDoubleClicked (self, row = -1) :
        if (row >= 0 and row < self._tableModel.rowCount ()) :             
            tag = self._getTagByIndex (row)
            if (tag != None) :
                if (tag.isInternalTag ()) :
                    MessageBoxUtil.showMessage (WindowTitle="Tag not editable",WindowMessage = "Automatically generated tags cannot be editied.", Details = None)                  
                else:
                    old_name  = tag.getName   ()
                    old_value =  tag.getValue ()
                    tagEditorDlg = RCC_TagEditorDlg (self, InitalizeWithTagValues = tag, DataSetStoredInTactic = self.isROIDataStoredInTatic (), ProjectContext = self._ProjectDataset.getProjectName ())                        
                    if (QDialog.Accepted == tagEditorDlg.exec_ ()) :
                        tag = tagEditorDlg.getTag ()
                        if tag is not None :
                            if (old_name == tag.getName ()):
                                tag.setValue (old_value)
                            elif (self.isROIDataStoredInTatic ()) :
                                tag.setValue (self._ProjectDataset.getROIDatasetInterface ().getTaticTagValue (self._ProjectDataset.getSelectedDataset (), tag.getName(), tag.getValue ()))
                            if (tag.getType () == "ItemList" and tag.getValue () not in tag.getParameterList ()):
                                tag.getParameterList ().append (tag.getValue ())
                            if (tag.getID () is None) : 
                                tag.setID (self._tagDataManager._getID ())
                            self._setTagByIndex (row, tag)
                            self.setTags(self._tagDataManager)                                            
    
    def _internalWindowResize (self, newsize):                            
        if (self._resizeWidgetHelper != None) :                                               
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.tableView, newsize)            
            self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.tableView, newsize)                        
            #self._resizeWidgetHelper.setWidgetYPos  (self.ui.closeButton, newsize.height () - 45)            
            #self._resizeWidgetHelper.setWidgetXPos  (self.ui.closeButton, newsize.width () - 117)                   
            #self._resizeWidgetHelper.setWidgetYPos  (self.ui.RemoveTagBtn, newsize.height () - 50)                        
            #self._resizeWidgetHelper.setWidgetYPos  (self.ui.AddTagBtn, newsize.height () - 50)    
            
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)                 
        self._internalWindowResize ( qresizeEvent.size () ) 
        
    """def getDialogParameters (self):
        returnData = []
        if (self._tableModel.getDialogParamters () is not None) :
            for row, param in enumerate(self._tableModel.getDialogParamters ()) :            
                newParam = param.copy()
                modelindex = self._tableModel.index (row, 0)
                value = self._tableModel.data(modelindex, QtCore.Qt.EditRole)       
                newParam.setValue (value)
                returnData.append (newParam)
        return returnData"""    
    
    def _closeDlg (self):
        self._tagDataManager = None
        self.accept ()
        