# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'RCC_PesscaraLogin.ui'
#
# Created by: PyQt5 UI code generator 5.15.0
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_LoginDlg(object):
    def setupUi(self, LoginDlg):
        LoginDlg.setObjectName("LoginDlg")
        LoginDlg.resize(400, 180)
        LoginDlg.setMinimumSize(QtCore.QSize(400, 150))
        LoginDlg.setMaximumSize(QtCore.QSize(400, 200))
        LoginDlg.setModal(True)
        self.server = QtWidgets.QLineEdit(LoginDlg)
        self.server.setGeometry(QtCore.QRect(90, 20, 301, 22))
        self.server.setObjectName("server")
        self.label = QtWidgets.QLabel(LoginDlg)
        self.label.setGeometry(QtCore.QRect(36, 20, 51, 20))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(LoginDlg)
        self.label_2.setGeometry(QtCore.QRect(11, 80, 71, 16))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(LoginDlg)
        self.label_3.setGeometry(QtCore.QRect(20, 110, 71, 16))
        self.label_3.setObjectName("label_3")
        self.password = QtWidgets.QLineEdit(LoginDlg)
        self.password.setGeometry(QtCore.QRect(90, 110, 301, 22))
        self.password.setEchoMode(QtWidgets.QLineEdit.Password)
        self.password.setObjectName("password")
        self.username = QtWidgets.QLineEdit(LoginDlg)
        self.username.setGeometry(QtCore.QRect(90, 80, 301, 22))
        self.username.setObjectName("username")
        self.OkBtn = QtWidgets.QPushButton(LoginDlg)
        self.OkBtn.setGeometry(QtCore.QRect(140, 150, 81, 22))
        self.OkBtn.setObjectName("OkBtn")
        self.CancelBtn = QtWidgets.QPushButton(LoginDlg)
        self.CancelBtn.setGeometry(QtCore.QRect(240, 150, 81, 22))
        self.CancelBtn.setObjectName("CancelBtn")
        self.label_4 = QtWidgets.QLabel(LoginDlg)
        self.label_4.setGeometry(QtCore.QRect(35, 50, 51, 20))
        self.label_4.setObjectName("label_4")
        self.project = QtWidgets.QLineEdit(LoginDlg)
        self.project.setGeometry(QtCore.QRect(90, 50, 301, 22))
        self.project.setObjectName("project")

        self.retranslateUi(LoginDlg)
        QtCore.QMetaObject.connectSlotsByName(LoginDlg)
        LoginDlg.setTabOrder(self.server, self.project)
        LoginDlg.setTabOrder(self.project, self.username)
        LoginDlg.setTabOrder(self.username, self.password)
        LoginDlg.setTabOrder(self.password, self.OkBtn)
        LoginDlg.setTabOrder(self.OkBtn, self.CancelBtn)

    def retranslateUi(self, LoginDlg):
        _translate = QtCore.QCoreApplication.translate
        LoginDlg.setWindowTitle(_translate("LoginDlg", "Pesscara Login"))
        self.label.setText(_translate("LoginDlg", " Server:"))
        self.label_2.setText(_translate("LoginDlg", "User name:"))
        self.label_3.setText(_translate("LoginDlg", "Password:"))
        self.OkBtn.setText(_translate("LoginDlg", "Ok"))
        self.CancelBtn.setText(_translate("LoginDlg", "Cancel"))
        self.label_4.setText(_translate("LoginDlg", "Project:"))
