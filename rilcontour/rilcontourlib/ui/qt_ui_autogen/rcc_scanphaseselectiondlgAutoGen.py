# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'RCC_ScanPhaseSelectionDlg.ui'
#
# Created by: PyQt5 UI code generator 5.15.0
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_rcc_scanphaseselectiondlgautogen(object):
    def setupUi(self, rcc_scanphaseselectiondlgautogen):
        rcc_scanphaseselectiondlgautogen.setObjectName("rcc_scanphaseselectiondlgautogen")
        rcc_scanphaseselectiondlgautogen.resize(1390, 803)
        rcc_scanphaseselectiondlgautogen.setMinimumSize(QtCore.QSize(900, 300))
        self.centralwidget = QtWidgets.QWidget(rcc_scanphaseselectiondlgautogen)
        self.centralwidget.setObjectName("centralwidget")
        self.PhaseLbl6 = QtWidgets.QLabel(self.centralwidget)
        self.PhaseLbl6.setGeometry(QtCore.QRect(1070, 720, 91, 16))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.PhaseLbl6.setFont(font)
        self.PhaseLbl6.setObjectName("PhaseLbl6")
        self.ScanPhaseSel4 = QtWidgets.QComboBox(self.centralwidget)
        self.ScanPhaseSel4.setGeometry(QtCore.QRect(499, 720, 221, 22))
        self.ScanPhaseSel4.setObjectName("ScanPhaseSel4")
        self.SliceSelectionSlider = QtWidgets.QSlider(self.centralwidget)
        self.SliceSelectionSlider.setGeometry(QtCore.QRect(370, 10, 31, 731))
        self.SliceSelectionSlider.setOrientation(QtCore.Qt.Vertical)
        self.SliceSelectionSlider.setObjectName("SliceSelectionSlider")
        self.ScanPhaseLbl3 = QtWidgets.QLabel(self.centralwidget)
        self.ScanPhaseLbl3.setGeometry(QtCore.QRect(1080, 20, 281, 16))
        self.ScanPhaseLbl3.setObjectName("ScanPhaseLbl3")
        self.PhaseLbl4 = QtWidgets.QLabel(self.centralwidget)
        self.PhaseLbl4.setGeometry(QtCore.QRect(409, 720, 91, 16))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.PhaseLbl4.setFont(font)
        self.PhaseLbl4.setObjectName("PhaseLbl4")
        self.CloseBtn = QtWidgets.QPushButton(self.centralwidget)
        self.CloseBtn.setGeometry(QtCore.QRect(9, 721, 81, 21))
        self.CloseBtn.setObjectName("CloseBtn")
        self.DatasetRangeErrorLbl6 = QtWidgets.QLabel(self.centralwidget)
        self.DatasetRangeErrorLbl6.setGeometry(QtCore.QRect(1069, 490, 301, 20))
        self.DatasetRangeErrorLbl6.setAlignment(QtCore.Qt.AlignCenter)
        self.DatasetRangeErrorLbl6.setObjectName("DatasetRangeErrorLbl6")
        self.sliceOffsetAdjustment4 = QtWidgets.QSlider(self.centralwidget)
        self.sliceOffsetAdjustment4.setGeometry(QtCore.QRect(709, 380, 16, 301))
        self.sliceOffsetAdjustment4.setOrientation(QtCore.Qt.Vertical)
        self.sliceOffsetAdjustment4.setObjectName("sliceOffsetAdjustment4")
        self.ScanPhaseLbl2 = QtWidgets.QLabel(self.centralwidget)
        self.ScanPhaseLbl2.setGeometry(QtCore.QRect(750, 20, 281, 16))
        self.ScanPhaseLbl2.setObjectName("ScanPhaseLbl2")
        self.ScanPhaseLbl6 = QtWidgets.QLabel(self.centralwidget)
        self.ScanPhaseLbl6.setGeometry(QtCore.QRect(1080, 390, 281, 16))
        self.ScanPhaseLbl6.setObjectName("ScanPhaseLbl6")
        self.ScanPhaseSel2 = QtWidgets.QComboBox(self.centralwidget)
        self.ScanPhaseSel2.setGeometry(QtCore.QRect(830, 350, 221, 22))
        self.ScanPhaseSel2.setObjectName("ScanPhaseSel2")
        self.SliceLbl = QtWidgets.QLabel(self.centralwidget)
        self.SliceLbl.setGeometry(QtCore.QRect(19, 410, 41, 16))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.SliceLbl.setFont(font)
        self.SliceLbl.setObjectName("SliceLbl")
        self.sliceOffsetAdjustment3 = QtWidgets.QSlider(self.centralwidget)
        self.sliceOffsetAdjustment3.setGeometry(QtCore.QRect(1370, 10, 16, 301))
        self.sliceOffsetAdjustment3.setOrientation(QtCore.Qt.Vertical)
        self.sliceOffsetAdjustment3.setObjectName("sliceOffsetAdjustment3")
        self.NiftiSliceWidget2 = NiftiSliceWidget(self.centralwidget)
        self.NiftiSliceWidget2.setGeometry(QtCore.QRect(740, 10, 301, 300))
        self.NiftiSliceWidget2.setObjectName("NiftiSliceWidget2")
        self.sliceOffsetAdjustment5 = QtWidgets.QSlider(self.centralwidget)
        self.sliceOffsetAdjustment5.setGeometry(QtCore.QRect(1040, 380, 16, 301))
        self.sliceOffsetAdjustment5.setOrientation(QtCore.Qt.Vertical)
        self.sliceOffsetAdjustment5.setObjectName("sliceOffsetAdjustment5")
        self.ScanPhaseSel3 = QtWidgets.QComboBox(self.centralwidget)
        self.ScanPhaseSel3.setGeometry(QtCore.QRect(1160, 350, 221, 22))
        self.ScanPhaseSel3.setObjectName("ScanPhaseSel3")
        self.NiftiSliceWidget3 = NiftiSliceWidget(self.centralwidget)
        self.NiftiSliceWidget3.setGeometry(QtCore.QRect(1070, 10, 300, 300))
        self.NiftiSliceWidget3.setObjectName("NiftiSliceWidget3")
        self.SliceNumberTxt = QtWidgets.QLineEdit(self.centralwidget)
        self.SliceNumberTxt.setGeometry(QtCore.QRect(69, 410, 81, 22))
        self.SliceNumberTxt.setMaxLength(4)
        self.SliceNumberTxt.setObjectName("SliceNumberTxt")
        self.ScanPhaseLbl5 = QtWidgets.QLabel(self.centralwidget)
        self.ScanPhaseLbl5.setGeometry(QtCore.QRect(750, 390, 281, 16))
        self.ScanPhaseLbl5.setObjectName("ScanPhaseLbl5")
        self.DatasetTree = QtWidgets.QTreeWidget(self.centralwidget)
        self.DatasetTree.setGeometry(QtCore.QRect(9, 10, 361, 391))
        self.DatasetTree.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
        self.DatasetTree.setObjectName("DatasetTree")
        self.DatasetTree.headerItem().setText(0, "1")
        self.NiftiSliceWidget6 = NiftiSliceWidget(self.centralwidget)
        self.NiftiSliceWidget6.setGeometry(QtCore.QRect(1070, 380, 300, 300))
        self.NiftiSliceWidget6.setObjectName("NiftiSliceWidget6")
        self.DatasetRangeErrorLbl2 = QtWidgets.QLabel(self.centralwidget)
        self.DatasetRangeErrorLbl2.setGeometry(QtCore.QRect(739, 160, 301, 20))
        self.DatasetRangeErrorLbl2.setAlignment(QtCore.Qt.AlignCenter)
        self.DatasetRangeErrorLbl2.setObjectName("DatasetRangeErrorLbl2")
        self.sliceOffsetAdjustment6 = QtWidgets.QSlider(self.centralwidget)
        self.sliceOffsetAdjustment6.setGeometry(QtCore.QRect(1369, 380, 16, 301))
        self.sliceOffsetAdjustment6.setOrientation(QtCore.Qt.Vertical)
        self.sliceOffsetAdjustment6.setObjectName("sliceOffsetAdjustment6")
        self.DatasetRangeErrorLbl3 = QtWidgets.QLabel(self.centralwidget)
        self.DatasetRangeErrorLbl3.setGeometry(QtCore.QRect(1069, 160, 301, 20))
        self.DatasetRangeErrorLbl3.setAlignment(QtCore.Qt.AlignCenter)
        self.DatasetRangeErrorLbl3.setObjectName("DatasetRangeErrorLbl3")
        self.ScanPhaseSel1 = QtWidgets.QComboBox(self.centralwidget)
        self.ScanPhaseSel1.setGeometry(QtCore.QRect(499, 350, 221, 22))
        self.ScanPhaseSel1.setObjectName("ScanPhaseSel1")
        self.PhaseLbl3 = QtWidgets.QLabel(self.centralwidget)
        self.PhaseLbl3.setGeometry(QtCore.QRect(1070, 350, 91, 16))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.PhaseLbl3.setFont(font)
        self.PhaseLbl3.setObjectName("PhaseLbl3")
        self.PhaseLbl2 = QtWidgets.QLabel(self.centralwidget)
        self.PhaseLbl2.setGeometry(QtCore.QRect(740, 350, 91, 16))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.PhaseLbl2.setFont(font)
        self.PhaseLbl2.setObjectName("PhaseLbl2")
        self.ScanPhaseLbl4 = QtWidgets.QLabel(self.centralwidget)
        self.ScanPhaseLbl4.setGeometry(QtCore.QRect(419, 390, 281, 16))
        self.ScanPhaseLbl4.setObjectName("ScanPhaseLbl4")
        self.PhaseLbl1 = QtWidgets.QLabel(self.centralwidget)
        self.PhaseLbl1.setGeometry(QtCore.QRect(409, 350, 91, 16))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.PhaseLbl1.setFont(font)
        self.PhaseLbl1.setObjectName("PhaseLbl1")
        self.sliderResetBtn = QtWidgets.QPushButton(self.centralwidget)
        self.sliderResetBtn.setGeometry(QtCore.QRect(260, 410, 101, 22))
        self.sliderResetBtn.setObjectName("sliderResetBtn")
        self.NiftiSliceWidget1 = NiftiSliceWidget(self.centralwidget)
        self.NiftiSliceWidget1.setGeometry(QtCore.QRect(409, 9, 300, 300))
        self.NiftiSliceWidget1.setObjectName("NiftiSliceWidget1")
        self.ScanPhaseSel6 = QtWidgets.QComboBox(self.centralwidget)
        self.ScanPhaseSel6.setGeometry(QtCore.QRect(1160, 720, 221, 22))
        self.ScanPhaseSel6.setObjectName("ScanPhaseSel6")
        self.DatasetRangeErrorLbl4 = QtWidgets.QLabel(self.centralwidget)
        self.DatasetRangeErrorLbl4.setGeometry(QtCore.QRect(409, 500, 301, 20))
        self.DatasetRangeErrorLbl4.setAlignment(QtCore.Qt.AlignCenter)
        self.DatasetRangeErrorLbl4.setObjectName("DatasetRangeErrorLbl4")
        self.sliceOffsetAdjustment2 = QtWidgets.QSlider(self.centralwidget)
        self.sliceOffsetAdjustment2.setGeometry(QtCore.QRect(1040, 9, 16, 301))
        self.sliceOffsetAdjustment2.setOrientation(QtCore.Qt.Vertical)
        self.sliceOffsetAdjustment2.setObjectName("sliceOffsetAdjustment2")
        self.DatasetRangeErrorLbl5 = QtWidgets.QLabel(self.centralwidget)
        self.DatasetRangeErrorLbl5.setGeometry(QtCore.QRect(739, 500, 301, 20))
        self.DatasetRangeErrorLbl5.setAlignment(QtCore.Qt.AlignCenter)
        self.DatasetRangeErrorLbl5.setObjectName("DatasetRangeErrorLbl5")
        self.PhaseLbl5 = QtWidgets.QLabel(self.centralwidget)
        self.PhaseLbl5.setGeometry(QtCore.QRect(740, 720, 91, 16))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.PhaseLbl5.setFont(font)
        self.PhaseLbl5.setObjectName("PhaseLbl5")
        self.ScanPhaseLbl1 = QtWidgets.QLabel(self.centralwidget)
        self.ScanPhaseLbl1.setGeometry(QtCore.QRect(419, 20, 281, 16))
        self.ScanPhaseLbl1.setObjectName("ScanPhaseLbl1")
        self.SliceSelectionWidget = SliceSelectionWidget(self.centralwidget)
        self.SliceSelectionWidget.setGeometry(QtCore.QRect(9, 440, 361, 271))
        self.SliceSelectionWidget.setObjectName("SliceSelectionWidget")
        self.NiftiSliceWidget5 = NiftiSliceWidget(self.centralwidget)
        self.NiftiSliceWidget5.setGeometry(QtCore.QRect(740, 380, 300, 300))
        self.NiftiSliceWidget5.setObjectName("NiftiSliceWidget5")
        self.ScanPhaseSel5 = QtWidgets.QComboBox(self.centralwidget)
        self.ScanPhaseSel5.setGeometry(QtCore.QRect(830, 720, 221, 22))
        self.ScanPhaseSel5.setObjectName("ScanPhaseSel5")
        self.NiftiSliceWidget4 = NiftiSliceWidget(self.centralwidget)
        self.NiftiSliceWidget4.setGeometry(QtCore.QRect(409, 379, 300, 300))
        self.NiftiSliceWidget4.setObjectName("NiftiSliceWidget4")
        self.PredictionLbl4 = QtWidgets.QLabel(self.centralwidget)
        self.PredictionLbl4.setEnabled(False)
        self.PredictionLbl4.setGeometry(QtCore.QRect(410, 690, 91, 16))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.PredictionLbl4.setFont(font)
        self.PredictionLbl4.setObjectName("PredictionLbl4")
        self.PhasePredictionLbl4 = QtWidgets.QLabel(self.centralwidget)
        self.PhasePredictionLbl4.setEnabled(False)
        self.PhasePredictionLbl4.setGeometry(QtCore.QRect(490, 690, 171, 16))
        self.PhasePredictionLbl4.setText("")
        self.PhasePredictionLbl4.setObjectName("PhasePredictionLbl4")
        self.PhasePrediction4SetBtn = QtWidgets.QPushButton(self.centralwidget)
        self.PhasePrediction4SetBtn.setEnabled(False)
        self.PhasePrediction4SetBtn.setGeometry(QtCore.QRect(670, 690, 51, 22))
        self.PhasePrediction4SetBtn.setObjectName("PhasePrediction4SetBtn")
        self.PhasePrediction5SetBtn = QtWidgets.QPushButton(self.centralwidget)
        self.PhasePrediction5SetBtn.setEnabled(False)
        self.PhasePrediction5SetBtn.setGeometry(QtCore.QRect(1000, 690, 51, 22))
        self.PhasePrediction5SetBtn.setObjectName("PhasePrediction5SetBtn")
        self.PhasePredictionLbl5 = QtWidgets.QLabel(self.centralwidget)
        self.PhasePredictionLbl5.setEnabled(False)
        self.PhasePredictionLbl5.setGeometry(QtCore.QRect(820, 690, 171, 16))
        self.PhasePredictionLbl5.setText("")
        self.PhasePredictionLbl5.setObjectName("PhasePredictionLbl5")
        self.PredictionLbl5 = QtWidgets.QLabel(self.centralwidget)
        self.PredictionLbl5.setEnabled(False)
        self.PredictionLbl5.setGeometry(QtCore.QRect(740, 690, 91, 16))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.PredictionLbl5.setFont(font)
        self.PredictionLbl5.setObjectName("PredictionLbl5")
        self.PredictionLbl6 = QtWidgets.QLabel(self.centralwidget)
        self.PredictionLbl6.setEnabled(False)
        self.PredictionLbl6.setGeometry(QtCore.QRect(1070, 690, 91, 16))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.PredictionLbl6.setFont(font)
        self.PredictionLbl6.setObjectName("PredictionLbl6")
        self.PhasePredictionLbl6 = QtWidgets.QLabel(self.centralwidget)
        self.PhasePredictionLbl6.setEnabled(False)
        self.PhasePredictionLbl6.setGeometry(QtCore.QRect(1150, 690, 171, 16))
        self.PhasePredictionLbl6.setText("")
        self.PhasePredictionLbl6.setObjectName("PhasePredictionLbl6")
        self.PhasePrediction6SetBtn = QtWidgets.QPushButton(self.centralwidget)
        self.PhasePrediction6SetBtn.setEnabled(False)
        self.PhasePrediction6SetBtn.setGeometry(QtCore.QRect(1330, 690, 51, 22))
        self.PhasePrediction6SetBtn.setObjectName("PhasePrediction6SetBtn")
        self.PredictionLbl2 = QtWidgets.QLabel(self.centralwidget)
        self.PredictionLbl2.setEnabled(False)
        self.PredictionLbl2.setGeometry(QtCore.QRect(740, 320, 91, 16))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.PredictionLbl2.setFont(font)
        self.PredictionLbl2.setObjectName("PredictionLbl2")
        self.PredictionLbl3 = QtWidgets.QLabel(self.centralwidget)
        self.PredictionLbl3.setEnabled(False)
        self.PredictionLbl3.setGeometry(QtCore.QRect(1070, 320, 91, 16))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.PredictionLbl3.setFont(font)
        self.PredictionLbl3.setObjectName("PredictionLbl3")
        self.PhasePredictionLbl2 = QtWidgets.QLabel(self.centralwidget)
        self.PhasePredictionLbl2.setEnabled(False)
        self.PhasePredictionLbl2.setGeometry(QtCore.QRect(820, 320, 171, 16))
        self.PhasePredictionLbl2.setText("")
        self.PhasePredictionLbl2.setObjectName("PhasePredictionLbl2")
        self.PhasePrediction1SetBtn = QtWidgets.QPushButton(self.centralwidget)
        self.PhasePrediction1SetBtn.setEnabled(False)
        self.PhasePrediction1SetBtn.setGeometry(QtCore.QRect(670, 320, 51, 22))
        self.PhasePrediction1SetBtn.setObjectName("PhasePrediction1SetBtn")
        self.PhasePredictionLbl1 = QtWidgets.QLabel(self.centralwidget)
        self.PhasePredictionLbl1.setEnabled(False)
        self.PhasePredictionLbl1.setGeometry(QtCore.QRect(490, 320, 171, 16))
        self.PhasePredictionLbl1.setText("")
        self.PhasePredictionLbl1.setObjectName("PhasePredictionLbl1")
        self.PhasePredictionLbl3 = QtWidgets.QLabel(self.centralwidget)
        self.PhasePredictionLbl3.setEnabled(False)
        self.PhasePredictionLbl3.setGeometry(QtCore.QRect(1150, 320, 171, 16))
        self.PhasePredictionLbl3.setText("")
        self.PhasePredictionLbl3.setObjectName("PhasePredictionLbl3")
        self.PhasePrediction3SetBtn = QtWidgets.QPushButton(self.centralwidget)
        self.PhasePrediction3SetBtn.setEnabled(False)
        self.PhasePrediction3SetBtn.setGeometry(QtCore.QRect(1330, 320, 51, 22))
        self.PhasePrediction3SetBtn.setObjectName("PhasePrediction3SetBtn")
        self.PredictionLbl1 = QtWidgets.QLabel(self.centralwidget)
        self.PredictionLbl1.setEnabled(False)
        self.PredictionLbl1.setGeometry(QtCore.QRect(410, 320, 71, 16))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.PredictionLbl1.setFont(font)
        self.PredictionLbl1.setObjectName("PredictionLbl1")
        self.PhasePrediction2SetBtn = QtWidgets.QPushButton(self.centralwidget)
        self.PhasePrediction2SetBtn.setEnabled(False)
        self.PhasePrediction2SetBtn.setGeometry(QtCore.QRect(1000, 320, 51, 22))
        self.PhasePrediction2SetBtn.setObjectName("PhasePrediction2SetBtn")
        self.NiftiSliceWidget4.raise_()
        self.NiftiSliceWidget5.raise_()
        self.NiftiSliceWidget6.raise_()
        self.NiftiSliceWidget3.raise_()
        self.NiftiSliceWidget2.raise_()
        self.PhaseLbl6.raise_()
        self.ScanPhaseSel4.raise_()
        self.SliceSelectionSlider.raise_()
        self.ScanPhaseLbl3.raise_()
        self.PhaseLbl4.raise_()
        self.CloseBtn.raise_()
        self.DatasetRangeErrorLbl6.raise_()
        self.sliceOffsetAdjustment4.raise_()
        self.ScanPhaseLbl2.raise_()
        self.ScanPhaseLbl6.raise_()
        self.ScanPhaseSel2.raise_()
        self.SliceLbl.raise_()
        self.sliceOffsetAdjustment3.raise_()
        self.sliceOffsetAdjustment5.raise_()
        self.ScanPhaseSel3.raise_()
        self.SliceNumberTxt.raise_()
        self.ScanPhaseLbl5.raise_()
        self.DatasetTree.raise_()
        self.DatasetRangeErrorLbl2.raise_()
        self.sliceOffsetAdjustment6.raise_()
        self.DatasetRangeErrorLbl3.raise_()
        self.ScanPhaseSel1.raise_()
        self.PhaseLbl3.raise_()
        self.PhaseLbl2.raise_()
        self.ScanPhaseLbl4.raise_()
        self.PhaseLbl1.raise_()
        self.sliderResetBtn.raise_()
        self.NiftiSliceWidget1.raise_()
        self.ScanPhaseSel6.raise_()
        self.DatasetRangeErrorLbl4.raise_()
        self.sliceOffsetAdjustment2.raise_()
        self.DatasetRangeErrorLbl5.raise_()
        self.PhaseLbl5.raise_()
        self.ScanPhaseLbl1.raise_()
        self.SliceSelectionWidget.raise_()
        self.ScanPhaseSel5.raise_()
        self.PredictionLbl4.raise_()
        self.PhasePredictionLbl4.raise_()
        self.PhasePrediction4SetBtn.raise_()
        self.PhasePrediction5SetBtn.raise_()
        self.PhasePredictionLbl5.raise_()
        self.PredictionLbl5.raise_()
        self.PredictionLbl6.raise_()
        self.PhasePredictionLbl6.raise_()
        self.PhasePrediction6SetBtn.raise_()
        self.PredictionLbl2.raise_()
        self.PredictionLbl3.raise_()
        self.PhasePredictionLbl2.raise_()
        self.PhasePrediction1SetBtn.raise_()
        self.PhasePredictionLbl1.raise_()
        self.PhasePredictionLbl3.raise_()
        self.PhasePrediction3SetBtn.raise_()
        self.PredictionLbl1.raise_()
        self.PhasePrediction2SetBtn.raise_()
        rcc_scanphaseselectiondlgautogen.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(rcc_scanphaseselectiondlgautogen)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1390, 22))
        self.menubar.setObjectName("menubar")
        self.menuSettings = QtWidgets.QMenu(self.menubar)
        self.menuSettings.setObjectName("menuSettings")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        self.menuWindow = QtWidgets.QMenu(self.menubar)
        self.menuWindow.setObjectName("menuWindow")
        rcc_scanphaseselectiondlgautogen.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(rcc_scanphaseselectiondlgautogen)
        self.statusbar.setObjectName("statusbar")
        rcc_scanphaseselectiondlgautogen.setStatusBar(self.statusbar)
        self.actionHounsfield_visualization_settings = QtWidgets.QAction(rcc_scanphaseselectiondlgautogen)
        self.actionHounsfield_visualization_settings.setObjectName("actionHounsfield_visualization_settings")
        self.Quit = QtWidgets.QAction(rcc_scanphaseselectiondlgautogen)
        self.Quit.setObjectName("Quit")
        self.actionAbout = QtWidgets.QAction(rcc_scanphaseselectiondlgautogen)
        self.actionAbout.setObjectName("actionAbout")
        self.menuSettings.addAction(self.actionHounsfield_visualization_settings)
        self.menuFile.addAction(self.Quit)
        self.menuWindow.addAction(self.actionAbout)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuSettings.menuAction())
        self.menubar.addAction(self.menuWindow.menuAction())

        self.retranslateUi(rcc_scanphaseselectiondlgautogen)
        QtCore.QMetaObject.connectSlotsByName(rcc_scanphaseselectiondlgautogen)

    def retranslateUi(self, rcc_scanphaseselectiondlgautogen):
        _translate = QtCore.QCoreApplication.translate
        rcc_scanphaseselectiondlgautogen.setWindowTitle(_translate("rcc_scanphaseselectiondlgautogen", "Dataset Description Selection Window"))
        self.PhaseLbl6.setText(_translate("rcc_scanphaseselectiondlgautogen", "Description:"))
        self.ScanPhaseLbl3.setText(_translate("rcc_scanphaseselectiondlgautogen", "TextLabel"))
        self.PhaseLbl4.setText(_translate("rcc_scanphaseselectiondlgautogen", "Description:"))
        self.CloseBtn.setText(_translate("rcc_scanphaseselectiondlgautogen", "Close"))
        self.DatasetRangeErrorLbl6.setText(_translate("rcc_scanphaseselectiondlgautogen", "Selected slice is outside of dataset slice range"))
        self.ScanPhaseLbl2.setText(_translate("rcc_scanphaseselectiondlgautogen", "TextLabel"))
        self.ScanPhaseLbl6.setText(_translate("rcc_scanphaseselectiondlgautogen", "TextLabel"))
        self.SliceLbl.setText(_translate("rcc_scanphaseselectiondlgautogen", "Slice:"))
        self.ScanPhaseLbl5.setText(_translate("rcc_scanphaseselectiondlgautogen", "TextLabel"))
        self.DatasetRangeErrorLbl2.setText(_translate("rcc_scanphaseselectiondlgautogen", "Selected slice is outside of dataset slice range"))
        self.DatasetRangeErrorLbl3.setText(_translate("rcc_scanphaseselectiondlgautogen", "Selected slice is outside of dataset slice range"))
        self.PhaseLbl3.setText(_translate("rcc_scanphaseselectiondlgautogen", "Description:"))
        self.PhaseLbl2.setText(_translate("rcc_scanphaseselectiondlgautogen", "Description:"))
        self.ScanPhaseLbl4.setText(_translate("rcc_scanphaseselectiondlgautogen", "TextLabel"))
        self.PhaseLbl1.setText(_translate("rcc_scanphaseselectiondlgautogen", "Description:"))
        self.sliderResetBtn.setText(_translate("rcc_scanphaseselectiondlgautogen", "Reset Sliders"))
        self.DatasetRangeErrorLbl4.setText(_translate("rcc_scanphaseselectiondlgautogen", "Selected slice is outside of dataset slice range"))
        self.DatasetRangeErrorLbl5.setText(_translate("rcc_scanphaseselectiondlgautogen", "Selected slice is outside of dataset slice range"))
        self.PhaseLbl5.setText(_translate("rcc_scanphaseselectiondlgautogen", "Description:"))
        self.ScanPhaseLbl1.setText(_translate("rcc_scanphaseselectiondlgautogen", "TextLabel"))
        self.PredictionLbl4.setText(_translate("rcc_scanphaseselectiondlgautogen", "Predicted:"))
        self.PhasePrediction4SetBtn.setText(_translate("rcc_scanphaseselectiondlgautogen", "Set"))
        self.PhasePrediction5SetBtn.setText(_translate("rcc_scanphaseselectiondlgautogen", "Set"))
        self.PredictionLbl5.setText(_translate("rcc_scanphaseselectiondlgautogen", "Predicted:"))
        self.PredictionLbl6.setText(_translate("rcc_scanphaseselectiondlgautogen", "Predicted:"))
        self.PhasePrediction6SetBtn.setText(_translate("rcc_scanphaseselectiondlgautogen", "Set"))
        self.PredictionLbl2.setText(_translate("rcc_scanphaseselectiondlgautogen", "Predicted:"))
        self.PredictionLbl3.setText(_translate("rcc_scanphaseselectiondlgautogen", "Predicted:"))
        self.PhasePrediction1SetBtn.setText(_translate("rcc_scanphaseselectiondlgautogen", "Set"))
        self.PhasePrediction3SetBtn.setText(_translate("rcc_scanphaseselectiondlgautogen", "Set"))
        self.PredictionLbl1.setText(_translate("rcc_scanphaseselectiondlgautogen", "Predicted:"))
        self.PhasePrediction2SetBtn.setText(_translate("rcc_scanphaseselectiondlgautogen", "Set"))
        self.menuSettings.setTitle(_translate("rcc_scanphaseselectiondlgautogen", "Settings"))
        self.menuFile.setTitle(_translate("rcc_scanphaseselectiondlgautogen", "File"))
        self.menuWindow.setTitle(_translate("rcc_scanphaseselectiondlgautogen", "Window"))
        self.actionHounsfield_visualization_settings.setText(_translate("rcc_scanphaseselectiondlgautogen", "Voxel threshold visualization settings"))
        self.Quit.setText(_translate("rcc_scanphaseselectiondlgautogen", "Quit"))
        self.actionAbout.setText(_translate("rcc_scanphaseselectiondlgautogen", "About"))
from rilcontourlib.ui.qt_widgets.niftislicewidget import NiftiSliceWidget
from rilcontourlib.ui.qt_widgets.sliceselectionwidget import SliceSelectionWidget
