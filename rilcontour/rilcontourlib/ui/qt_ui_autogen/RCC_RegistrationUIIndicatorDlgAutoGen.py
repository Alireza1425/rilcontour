# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'rcc_registrationUIIndicatorFilterDlg.ui'
#
# Created by: PyQt5 UI code generator 5.15.0
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_rcc_RegistrationIndicatorFilter(object):
    def setupUi(self, rcc_RegistrationIndicatorFilter):
        rcc_RegistrationIndicatorFilter.setObjectName("rcc_RegistrationIndicatorFilter")
        rcc_RegistrationIndicatorFilter.resize(323, 284)
        rcc_RegistrationIndicatorFilter.setMinimumSize(QtCore.QSize(323, 284))
        rcc_RegistrationIndicatorFilter.setMaximumSize(QtCore.QSize(323, 284))
        rcc_RegistrationIndicatorFilter.setModal(True)
        self.calendarWidget = QtWidgets.QCalendarWidget(rcc_RegistrationIndicatorFilter)
        self.calendarWidget.setGeometry(QtCore.QRect(30, 80, 271, 161))
        self.calendarWidget.setObjectName("calendarWidget")
        self.ShowAllDatesRadioBtn = QtWidgets.QRadioButton(rcc_RegistrationIndicatorFilter)
        self.ShowAllDatesRadioBtn.setGeometry(QtCore.QRect(10, 20, 291, 20))
        self.ShowAllDatesRadioBtn.setChecked(True)
        self.ShowAllDatesRadioBtn.setObjectName("ShowAllDatesRadioBtn")
        self.FilterDatesRadioBtn = QtWidgets.QRadioButton(rcc_RegistrationIndicatorFilter)
        self.FilterDatesRadioBtn.setGeometry(QtCore.QRect(10, 50, 301, 20))
        self.FilterDatesRadioBtn.setObjectName("FilterDatesRadioBtn")
        self.CloseBtn = QtWidgets.QPushButton(rcc_RegistrationIndicatorFilter)
        self.CloseBtn.setGeometry(QtCore.QRect(220, 250, 81, 22))
        self.CloseBtn.setObjectName("CloseBtn")

        self.retranslateUi(rcc_RegistrationIndicatorFilter)
        QtCore.QMetaObject.connectSlotsByName(rcc_RegistrationIndicatorFilter)

    def retranslateUi(self, rcc_RegistrationIndicatorFilter):
        _translate = QtCore.QCoreApplication.translate
        rcc_RegistrationIndicatorFilter.setWindowTitle(_translate("rcc_RegistrationIndicatorFilter", "Registration UI Indicator Filter"))
        self.ShowAllDatesRadioBtn.setText(_translate("rcc_RegistrationIndicatorFilter", "Show all ANTS registration indicators"))
        self.FilterDatesRadioBtn.setText(_translate("rcc_RegistrationIndicatorFilter", "Show ANTS registration indicators on/after:"))
        self.CloseBtn.setText(_translate("rcc_RegistrationIndicatorFilter", "Close"))
