# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'RCC_HelpDlg.ui'
#
# Created by: PyQt5 UI code generator 5.15.0
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_RCC_Help(object):
    def setupUi(self, RCC_Help):
        RCC_Help.setObjectName("RCC_Help")
        RCC_Help.setWindowModality(QtCore.Qt.NonModal)
        RCC_Help.resize(400, 500)
        RCC_Help.setMinimumSize(QtCore.QSize(400, 500))
        RCC_Help.setMaximumSize(QtCore.QSize(400, 500))
        RCC_Help.setModal(False)
        self.toolBox = QtWidgets.QToolBox(RCC_Help)
        self.toolBox.setGeometry(QtCore.QRect(0, 0, 400, 500))
        self.toolBox.setMinimumSize(QtCore.QSize(400, 500))
        self.toolBox.setMaximumSize(QtCore.QSize(400, 500))
        self.toolBox.setObjectName("toolBox")
        self.paint = QtWidgets.QWidget()
        self.paint.setGeometry(QtCore.QRect(0, 0, 400, 413))
        self.paint.setObjectName("paint")
        self.splashMessage_3 = QtWidgets.QLabel(self.paint)
        self.splashMessage_3.setGeometry(QtCore.QRect(20, 10, 341, 381))
        self.splashMessage_3.setObjectName("splashMessage_3")
        self.toolBox.addItem(self.paint, "")
        self.roi = QtWidgets.QWidget()
        self.roi.setGeometry(QtCore.QRect(0, 0, 400, 413))
        self.roi.setObjectName("roi")
        self.splashMessage_4 = QtWidgets.QLabel(self.roi)
        self.splashMessage_4.setGeometry(QtCore.QRect(20, 10, 191, 341))
        self.splashMessage_4.setObjectName("splashMessage_4")
        self.toolBox.addItem(self.roi, "")
        self.window = QtWidgets.QWidget()
        self.window.setGeometry(QtCore.QRect(0, 0, 400, 413))
        self.window.setObjectName("window")
        self.splashMessage_5 = QtWidgets.QLabel(self.window)
        self.splashMessage_5.setGeometry(QtCore.QRect(20, 10, 301, 241))
        self.splashMessage_5.setObjectName("splashMessage_5")
        self.toolBox.addItem(self.window, "")

        self.retranslateUi(RCC_Help)
        self.toolBox.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(RCC_Help)

    def retranslateUi(self, RCC_Help):
        _translate = QtCore.QCoreApplication.translate
        RCC_Help.setWindowTitle(_translate("RCC_Help", "Hotkey Shortcuts"))
        self.splashMessage_3.setText(_translate("RCC_Help", "<html><head/><body><p>Left/Right Mouse: Paint/Erase Selected ROI</p><p>Alt + Left Mouse: Move View without Painting</p><p>[: Decrease Pen Size</p><p>]: Increase Pen Size</p><p>Escape: Cancel</p><p>R or Ctrl-Y: Redo</p><p>U or Ctrl-Z: Undo</p><p>Ctrl-T: Toggle Paint Threshold Constraint</p><p>Ctrl-F: Fill hole for selected ROI in slice</p><p>Ctrl-E: Erode ROI in slice</p><p>Ctrl-D: Dilate ROI in slice</p><p>Ctrl-O: Keep only the largest object for the selected ROI</p><p>A: Increase ROI Transparency</p><p>D: Decrease ROI Transparency</p></body></html>"))
        self.toolBox.setItemText(self.toolBox.indexOf(self.paint), _translate("RCC_Help", "Paint Tools"))
        self.splashMessage_4.setText(_translate("RCC_Help", "<html><head/><body><p>S or \\: Hide all ROI</p><p>H or I: Hide and Lock all ROI</p><p>L or P: Lock all ROI</p><p>1: Hide and Lock ROI 0</p><p>2: Hide and Lock ROI 1</p><p>3: Hide and Lock ROI 2</p><p>4: Hide and Lock ROI 3</p><p>5: Hide and Lock ROI 4</p><p>6: Hide and Lock ROI 5</p><p>7: Hide and Lock ROI 6</p><p>8: Hide and Lock ROI 7</p><p>9: Hide and Lock ROI 8</p><p>0: Hide and Lock ROI 9</p></body></html>"))
        self.toolBox.setItemText(self.toolBox.indexOf(self.roi), _translate("RCC_Help", "ROI Settings"))
        self.splashMessage_5.setText(_translate("RCC_Help", "<html><head/><body><p>+: Zoom in</p><p>-: Zoom out</p><p>Z: Maximize Within Slice Contrast</p><p>X: Set Gray Threshold to Lung Preset</p><p>C: Set Gray Threshold to Custom range</p><p>V: Set Gray Threshold to Soft Tissue Preset</p><p>B: Set Gray Threshold to Bone Preset</p><p>N: Set Gray Threshold to Liver Preset</p><p>M: Set Gray Threshold to Brain Preset</p></body></html>"))
        self.toolBox.setItemText(self.toolBox.indexOf(self.window), _translate("RCC_Help", "Window Settings"))
