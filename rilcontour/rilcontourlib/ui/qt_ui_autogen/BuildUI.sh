#!/bin/bash
pyuic5 rcc_multilinemessagebox.ui > rcc_multilinemessageboxautogen.py
pyuic5 RenameLongFileNameDlg.ui > rcc_renamelongfilenamedlgautogen.py
pyuic5 RCC_OneClickSegmentation.ui > RCC_OneClickSegmentationDlg.py
pyuic5 RCC_ROIEditor.ui > rcc_roieditor.py
pyuic5 RCC_MainWindow.ui > rcc_mainwindow.py
pyuic5 RCC_HounsfieldVisualizationOptionsDlg.ui > rcc_hounsfieldOptionsDlg.py
pyuic5 RCC_ContourNameEditor.ui > rcc_contournameeditor.py
pyuic5 RCC_VisSettings.ui > rcc_vissettings.py
pyuic5 RCC_BatchContour.ui > rcc_batchcontour.py
pyuic5 RCC_Stats.ui > rcc_stats.py
pyuic5 RCC_PesscaraLogin.ui > rcc_pesscaralogin.py
pyuic5 RCC_PesscaraSearch.ui > rcc_pesscarasearch.py
pyuic5 RCC_Dataset.ui > rcc_dataset.py
pyuic5 RCC_NewProjectWizard.ui > rcc_newprojectwizard.py
pyuic5 rcc_volumemaskexport.ui > rcc_roivolumemaskexportdlgAutoGen.py
pyuic5 RCC_PesscaraMaskViewer.ui > rcc_pesscaramaskviewerdlgAutoGen.py
pyuic5 RCC_ProjectROIEditor.ui > rcc_projectroieditor.py
pyuic5 RCC_ScanPhaseSelectionDlg.ui > rcc_scanphaseselectiondlgAutoGen.py
pyuic5 RCC_ScanPhaseEditor.ui > rcc_ScanPhaseEditDlgAutoGen.py
pyuic5 rcc_volumecsvexport.ui > rcc_volumecsvexportAutoGen.py
pyuic5 rcc_registrationLogViewerUI.ui > rcc_registrationLogViewerDlgAutoGen.py
pyuic5 RCC_ROITranslation.ui > rcc_roitranslationdlgautogen.py
pyuic5 rcc_mlscandescriptiondlg.ui > rcc_mlscandescriptiondlgAutoGen.py
pyuic5 RCC_UserNameEditor.ui > RCC_UserNameEditorAutoGen.py
pyuic5 rcc_registrationUIIndicatorFilterDlg.ui > RCC_RegistrationUIIndicatorDlgAutoGen.py
pyuic5 RCC_TagEditor.ui > RCC_TagEditorAutoGen.py
pyuic5 RCC_DatasetTagEditor.ui > RCC_DatasetTagEditorAutogen.py
pyuic5 RCC_ProjectTagEditor.ui > RCC_ProjectTagEditorAutogen.py
pyuic5 RCC_ColumnChooserDlg.ui > RCC_DatasetColumnChooserAutogen.py
pyuic5 RCC_PesscaraTemporaryDataStorageDlg.ui > RCC_PesscaraTemporaryDataStorageDlgAutogen.py
pyuic5 RCC_ExportTags.ui > RCC_ExportTagsDlgAutogen.py
pyuic5 RCC_ImportROIMaskDlg.ui > RCC_ImportROIMaskDlgAutogen.py
pyuic5 RCC_PenSettingsDlg.ui > RCC_PenSettingsDlgAutogen.py
pyuic5 RCC_MLModelZooDlg.ui > RCC_MLModelZooDlgAutogen.py
pyuic5 RCC_MLModelDefDlg.ui > RCC_ML_ModelDefDlgAutogen.py
pyuic5 RCC_SplashScreen.ui > RCC_SplashScreenAutogen.py
pyuic5 RCC_AboutDlg.ui > RCC_AboutDlgAutogen.py
pyuic5 RCC_DrawToolsWindow.ui  > RCC_DrawToolsDlgAutogen.py
pyuic5 RCC_ModelManagerDlg.ui > RCC_ModelManagerDlgAutogen.py
pyuic5 RCC_TreeNodeTxtEditDlg.ui > RCC_TreeNodeTxtEditDlgAutogen.py
pyuic5 RCC_Duplicate_ModelQuestion.ui > RCC_Duplicate_ModelQuestionAutogen.py
pyuic5 RCC_MLModelImportDlg.ui > RCC_MLModelImportDlgAutogen.py
pyuic5 RCC_AutoImportMLModelDlg.ui > RCC_AutoImportMLModelDlgAutogen.py
pyuic5 RCC_MLModelSegmentationQuailityDlg.ui > RCC_MLSegmentationQuantificationAutogen.py
pyuic5 RCC_OpenglViewer.ui > RCC_OpenGLViewerDlgAutogen.py
pyuic5 RCC_NoCancelProgressDlg.ui > RCC_NoCancelProgressDlgAutogen.py
pyuic5 RCC_ThresholdPresetEditorDlg.ui > RCC_ThresholdPresetEditorDlgAutogen.py
pyuic5 RCC_FileCheckinDialog.ui > RCC_FileCheckinDialogAutogen.py
pyuic5 RCC_FileHistoryDialog.ui > RCC_FileHistoryDialogAutogen.py
pyuic5 RCC_AssignFileLockDlg.ui > RCC_AssignFileLockDlgAutogen.py
pyuic5 rcc_listdlg.ui > rcc_listdlgautogen.py
pyuic5 RCC_LoadROIDataWarrningDlg.ui > rcc_loadroidatawarrningdlgautogen.py
pyuic5 rcc_drag_drop_niftidlg.ui > rcc_drag_drop_niftidlgautogen.py
pyuic5 RCC_SelectROIDlg.ui > rcc_selectROIdlgAutogen.py
pyuic5 rcc_n4b_correction.ui > rcc_n4b_corrrectionDlgAutogen.py
pyuic5 RCC_SegmentationQuantificationResults.ui > rcc_segquantresultsAutogen.py
pyuic5 RCC_TabTableWidget.ui > rcc_tabtablewidgetAutogen.py
pyuic5 RCCAdvancedFileFilters.ui > rcc_advancedfilefilterAutogen.py
pyuic5 RCC_FileProperties.ui > rcc_FilePropertiesDlgAutogen.py
pyuic5 rcc_ImportImagingDlg.ui > rcc_importimagingdlg.py
pyuic5 RCC_HelpDlg.ui > RCC_HelpDlgAutogen.py
