# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'rcc_volumecsvexport.ui'
#
# Created by: PyQt5 UI code generator 5.15.0
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_rcc_ROIVolumeMaskExportDlg(object):
    def setupUi(self, rcc_ROIVolumeMaskExportDlg):
        rcc_ROIVolumeMaskExportDlg.setObjectName("rcc_ROIVolumeMaskExportDlg")
        rcc_ROIVolumeMaskExportDlg.resize(339, 330)
        rcc_ROIVolumeMaskExportDlg.setMinimumSize(QtCore.QSize(339, 330))
        rcc_ROIVolumeMaskExportDlg.setSizeGripEnabled(True)
        rcc_ROIVolumeMaskExportDlg.setModal(True)
        self.ROIObjectList = QtWidgets.QListWidget(rcc_ROIVolumeMaskExportDlg)
        self.ROIObjectList.setGeometry(QtCore.QRect(10, 130, 321, 131))
        self.ROIObjectList.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.ROIObjectList.setObjectName("ROIObjectList")
        self.ExportBtn = QtWidgets.QPushButton(rcc_ROIVolumeMaskExportDlg)
        self.ExportBtn.setGeometry(QtCore.QRect(240, 300, 81, 22))
        self.ExportBtn.setAutoDefault(False)
        self.ExportBtn.setObjectName("ExportBtn")
        self.ROIObjectExportLbl = QtWidgets.QLabel(rcc_ROIVolumeMaskExportDlg)
        self.ROIObjectExportLbl.setGeometry(QtCore.QRect(20, 110, 161, 20))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.ROIObjectExportLbl.setFont(font)
        self.ROIObjectExportLbl.setObjectName("ROIObjectExportLbl")
        self.InSliceLbl = QtWidgets.QLabel(rcc_ROIVolumeMaskExportDlg)
        self.InSliceLbl.setGeometry(QtCore.QRect(30, 35, 51, 20))
        self.InSliceLbl.setObjectName("InSliceLbl")
        self.InSliceSlider = QtWidgets.QSlider(rcc_ROIVolumeMaskExportDlg)
        self.InSliceSlider.setGeometry(QtCore.QRect(150, 30, 171, 20))
        self.InSliceSlider.setMinimum(1)
        self.InSliceSlider.setMaximum(99)
        self.InSliceSlider.setOrientation(QtCore.Qt.Horizontal)
        self.InSliceSlider.setObjectName("InSliceSlider")
        self.CancelBtn = QtWidgets.QPushButton(rcc_ROIVolumeMaskExportDlg)
        self.CancelBtn.setGeometry(QtCore.QRect(150, 300, 81, 22))
        self.CancelBtn.setAutoDefault(False)
        self.CancelBtn.setObjectName("CancelBtn")
        self.CrossSliceLbl = QtWidgets.QLabel(rcc_ROIVolumeMaskExportDlg)
        self.CrossSliceLbl.setGeometry(QtCore.QRect(10, 60, 81, 20))
        self.CrossSliceLbl.setObjectName("CrossSliceLbl")
        self.InSliceTxt = QtWidgets.QLineEdit(rcc_ROIVolumeMaskExportDlg)
        self.InSliceTxt.setGeometry(QtCore.QRect(90, 30, 51, 21))
        self.InSliceTxt.setMaxLength(2)
        self.InSliceTxt.setObjectName("InSliceTxt")
        self.PointROILbl = QtWidgets.QLabel(rcc_ROIVolumeMaskExportDlg)
        self.PointROILbl.setGeometry(QtCore.QRect(10, 10, 311, 20))
        font = QtGui.QFont()
        font.setBold(False)
        font.setWeight(50)
        self.PointROILbl.setFont(font)
        self.PointROILbl.setObjectName("PointROILbl")
        self.CrossSliceTxt = QtWidgets.QLineEdit(rcc_ROIVolumeMaskExportDlg)
        self.CrossSliceTxt.setGeometry(QtCore.QRect(90, 60, 51, 22))
        self.CrossSliceTxt.setMaxLength(2)
        self.CrossSliceTxt.setObjectName("CrossSliceTxt")
        self.CrossSliceSlider = QtWidgets.QSlider(rcc_ROIVolumeMaskExportDlg)
        self.CrossSliceSlider.setGeometry(QtCore.QRect(150, 60, 171, 20))
        self.CrossSliceSlider.setMinimum(1)
        self.CrossSliceSlider.setMaximum(99)
        self.CrossSliceSlider.setOrientation(QtCore.Qt.Horizontal)
        self.CrossSliceSlider.setObjectName("CrossSliceSlider")
        self.line_2 = QtWidgets.QFrame(rcc_ROIVolumeMaskExportDlg)
        self.line_2.setGeometry(QtCore.QRect(20, 90, 301, 20))
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.combineNonTouchingROI = QtWidgets.QCheckBox(rcc_ROIVolumeMaskExportDlg)
        self.combineNonTouchingROI.setGeometry(QtCore.QRect(10, 270, 321, 20))
        self.combineNonTouchingROI.setChecked(True)
        self.combineNonTouchingROI.setObjectName("combineNonTouchingROI")

        self.retranslateUi(rcc_ROIVolumeMaskExportDlg)
        QtCore.QMetaObject.connectSlotsByName(rcc_ROIVolumeMaskExportDlg)

    def retranslateUi(self, rcc_ROIVolumeMaskExportDlg):
        _translate = QtCore.QCoreApplication.translate
        rcc_ROIVolumeMaskExportDlg.setWindowTitle(_translate("rcc_ROIVolumeMaskExportDlg", "Voxel Annotation CSV Export"))
        self.ExportBtn.setText(_translate("rcc_ROIVolumeMaskExportDlg", "Export"))
        self.ROIObjectExportLbl.setText(_translate("rcc_ROIVolumeMaskExportDlg", "ROI objects to export"))
        self.InSliceLbl.setText(_translate("rcc_ROIVolumeMaskExportDlg", "In Slice:"))
        self.CancelBtn.setText(_translate("rcc_ROIVolumeMaskExportDlg", "Cancel"))
        self.CrossSliceLbl.setText(_translate("rcc_ROIVolumeMaskExportDlg", "Cross Slice:"))
        self.PointROILbl.setText(_translate("rcc_ROIVolumeMaskExportDlg", "Point ROI object size (voxels):"))
        self.combineNonTouchingROI.setText(_translate("rcc_ROIVolumeMaskExportDlg", "Combine non-touching ROI with same name"))
