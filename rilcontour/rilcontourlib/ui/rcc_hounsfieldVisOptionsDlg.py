#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  1 12:40:46 2017

@author: m160897
"""
from PyQt5 import QtCore
from PyQt5.QtWidgets import   QDialog
from six import *
from rilcontourlib.ui.qt_ui_autogen.rcc_hounsfieldOptionsDlg import Ui_HounsfieldOptions
from rilcontourlib.util.rcc_util import ShortcutKeyList

"""
    RCC_HounsfieldVisOptionsDlg
    
    Non-modal dialog to enable user to interactively modify hounsfield visulization settings
"""    

class RCC_HounsfieldVisOptionsDlg (QDialog) :   
    def __init__ (self, parent) :
        QDialog.__init__ (self, parent)
        self._parentWindow = parent
        
        self._windowLevelPresets = {}
        self._windowLevelPresets["Soft Tissue"] = (400, 50)
        self._windowLevelPresets["Lung"] = (1500, -600)
        self._windowLevelPresets["Liver"] = (150, 30)
        self._windowLevelPresets["Bone"] = (1800, 400)
        self._windowLevelPresets["Brain"] = (80, 40)
        self._windowLevelPresets["Subdural"] = (130, 10)
        self._windowLevelPresets["Stroke"] = (40, 40)
        self._windowLevelPresets["Temporal Bones"] = (2800, 600)
        self._windowLevelPresets["Mediastinum"] = (350, 50)
        
        
        
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, False)         
        self.ui = Ui_HounsfieldOptions ()        
        self.ui.setupUi (self)       
        self.ui.CloseBtn.clicked.connect (self.closeDlg)            
        self.setMinimumSize(self.size ())
        self.setMaximumSize(self.size ())
        self._dlgUIQTInit = False  
        self.ui.minValueTxt.setFocusPolicy (QtCore.Qt.StrongFocus)
        
        self.ui.minValueTxt.installEventFilter(self._parentWindow._ChildWidgetOverrideEventFilter)
        self.ui.maxValueTxt.installEventFilter(self._parentWindow._ChildWidgetOverrideEventFilter)
        self.ui.levelValue.installEventFilter(self._parentWindow._ChildWidgetOverrideEventFilter)
        self.ui.windowValue.installEventFilter(self._parentWindow._ChildWidgetOverrideEventFilter)        
        self.ui.minSlider.installEventFilter(self._parentWindow._ChildWidgetOverrideEventFilter)
        self.ui.maxSlider.installEventFilter(self._parentWindow._ChildWidgetOverrideEventFilter)
        self.ui.LevelSlider.installEventFilter(self._parentWindow._ChildWidgetOverrideEventFilter)
        self.ui.WindowSlider.installEventFilter(self._parentWindow._ChildWidgetOverrideEventFilter)
        self.ui.maxamizeWithinSliceContrast.installEventFilter(self._parentWindow._ChildWidgetOverrideEventFilter)
        self.ui.fixedRange.installEventFilter(self._parentWindow._ChildWidgetOverrideEventFilter)
        self.ui.LevelPreset.installEventFilter(self._parentWindow._ChildWidgetOverrideEventFilter)
        
        self.ui.setToVolumePrecentRange.installEventFilter(self._parentWindow._ChildWidgetOverrideEventFilter)
        self.ui.setToSlicePrecentRange.installEventFilter(self._parentWindow._ChildWidgetOverrideEventFilter)
        
        self.ui.maxValueTxt.setFocusPolicy (QtCore.Qt.StrongFocus)
        self.ui.levelValue.setFocusPolicy (QtCore.Qt.StrongFocus)
        self.ui.windowValue.setFocusPolicy (QtCore.Qt.StrongFocus)
        self.setWindowFlags(self.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)        
    
    def _tryGetWindowLevelPreset (self, window, level) :
        for key, values in self._windowLevelPresets.items () :
            windowPreset, levelPreset = values 
            if (window == windowPreset and level == levelPreset) :
                return key
        return None
            
    def keyPressEvent(self, event):        
        KeyStrokes = ShortcutKeyList.getPassThroughKeyStrokes () 
        if (event.key () in KeyStrokes) : 
            self._parentWindow.keyPressEvent (event)
        
    def updateHounsfeildDlgSettings (self, settings) : 
        if (self._dlgUIQTInit) :
            self.ui.maxValueTxt.editingFinished.disconnect (self._txtValueChanged)
            self.ui.minValueTxt.editingFinished.disconnect (self._txtValueChanged)
            self.ui.minSlider.valueChanged.disconnect (self._minValueChanged)
            self.ui.maxSlider.valueChanged.disconnect (self._maxValueChanged)   
            self.ui.maxamizeWithinSliceContrast.clicked.disconnect (self.radioBtnEvent)
            self.ui.fixedRange.clicked.disconnect (self.radioBtnEvent)
            self.ui.setToVolumePrecentRange.clicked.disconnect (self.radioBtnEvent)
            self.ui.setToSlicePrecentRange.clicked.disconnect (self.radioBtnEvent)            
            self.ui.levelValue.editingFinished.disconnect (self._txtWindowValueChanged)
            self.ui.windowValue.editingFinished.disconnect (self._txtWindowValueChanged)
            self.ui.LevelSlider.valueChanged.disconnect (self._windowLevelValueChanged)
            self.ui.WindowSlider.valueChanged.disconnect (self._windowLevelValueChanged)   
            self.ui.LevelPreset.currentTextChanged.disconnect (self._levelPresetValueChanged)   
            self.ui.windowBWPortionOfColorImaging.stateChanged.disconnect (self._windowBWPortionofColorImaging)
            
        self.VisSettings = settings
        self.ui.windowBWPortionOfColorImaging.setChecked (settings.windowBWComponentOfColorData ())
        minValue, maxValue = self.VisSettings.getDatasetMinMaxValue ()        
        if (minValue is not None and maxValue is not None) :
            self.ui.maxSlider.setMaximum (maxValue)
            self.ui.minSlider.setMaximum (maxValue)
            self.ui.maxSlider.setMinimum (minValue)
            self.ui.minSlider.setMinimum (minValue)     
            
            self.ui.LevelSlider.setMaximum (maxValue)
            self.ui.WindowSlider.setMaximum (3000)
            self.ui.LevelSlider.setMinimum (minValue)
            self.ui.WindowSlider.setMinimum (3)   
        
        lowV, highV = self.VisSettings.getCustomHounsfieldRange ()
        
        self.ui.minValueTxt.setText (str (lowV))
        self.ui.maxValueTxt.setText (str (highV))
        self.ui.minSlider.setValue (lowV)
        self.ui.maxSlider.setValue (highV)
        
        level = int ((lowV + highV) / 2)
        window = highV - lowV
        if (level  < self.ui.LevelSlider.minimum ()):
            self.ui.LevelSlider.setMinimum (level)
        if (level  > self.ui.LevelSlider.maximum ()):
            self.ui.LevelSlider.setMaximum (level)
        if (window < self.ui.WindowSlider.minimum ()):
            self.ui.WindowSlider.setMinimum (window)
        if (window > self.ui.WindowSlider.maximum ()):
            self.ui.WindowSlider.setMaximum (window)    
        self.ui.LevelSlider.setValue (level)
        self.ui.WindowSlider.setValue (window)  
        
        preset = self._tryGetWindowLevelPreset (window, level)
        if preset is not None :
            self._levelPresetValueChanged (preset)        
        else:
            self._levelPresetValueChanged ("Custom")            
            self.VisSettings.setCustomLevelWindowPreset (level, window)
            
        if (self.VisSettings.maximizeWithinSliceContrast ()) :
            self.ui.maxamizeWithinSliceContrast.setChecked (True)
        else :
            self.ui.fixedRange.setChecked (True)              
        self.radioBtnEvent ()        
        
        self.ui.windowBWPortionOfColorImaging.stateChanged.connect (self._windowBWPortionofColorImaging)
        self.ui.maxValueTxt.editingFinished.connect (self._txtValueChanged)
        self.ui.minValueTxt.editingFinished.connect (self._txtValueChanged)
        self.ui.minSlider.valueChanged.connect (self._minValueChanged)
        self.ui.maxSlider.valueChanged.connect (self._maxValueChanged)   
        
        self.ui.levelValue.editingFinished.connect (self._txtWindowValueChanged)
        self.ui.windowValue.editingFinished.connect (self._txtWindowValueChanged)
        self.ui.LevelSlider.valueChanged.connect (self._windowLevelValueChanged)
        self.ui.WindowSlider.valueChanged.connect (self._windowLevelValueChanged)   
        self.ui.LevelPreset.currentTextChanged.connect (self._levelPresetValueChanged)   
        
        self.ui.setToVolumePrecentRange.clicked.connect (self.radioBtnEvent)
        self.ui.setToSlicePrecentRange.clicked.connect (self.radioBtnEvent)
        self.ui.maxamizeWithinSliceContrast.clicked.connect (self.radioBtnEvent)
        self.ui.fixedRange.clicked.connect (self.radioBtnEvent)
        self._dlgUIQTInit = True 
    
    def _windowBWPortionofColorImaging (self, index) :
        self.VisSettings.setWindowBWComponentOfColorData (self.ui.windowBWPortionOfColorImaging.isChecked ()) 
        
    def keyboardEvent (self, key) :
        if key == QtCore.Qt.Key_Z :
            self.ui.maxamizeWithinSliceContrast.setChecked (True)
            self.ui.fixedRange.setChecked (False)  
            self.radioBtnEvent ()
        elif key == QtCore.Qt.Key_X :
            self.ui.maxamizeWithinSliceContrast.setChecked (False)
            self.ui.fixedRange.setChecked (True)            
            self._levelPresetValueChanged ("Lung")
        elif key == QtCore.Qt.Key_C :
            self.ui.maxamizeWithinSliceContrast.setChecked (False)
            self.ui.fixedRange.setChecked (True)            
            self._levelPresetValueChanged ("Custom")
        elif key == QtCore.Qt.Key_V :
            self.ui.maxamizeWithinSliceContrast.setChecked (False)
            self.ui.fixedRange.setChecked (True)
            self._levelPresetValueChanged ("Soft Tissue")
        elif key == QtCore.Qt.Key_B :
            self.ui.maxamizeWithinSliceContrast.setChecked (False)
            self.ui.fixedRange.setChecked (True)            
            self._levelPresetValueChanged ("Bone") 
        elif key == QtCore.Qt.Key_N :
            self.ui.maxamizeWithinSliceContrast.setChecked (False)
            self.ui.fixedRange.setChecked (True)            
            self._levelPresetValueChanged ("Liver") 
        elif key == QtCore.Qt.Key_M :
            self.ui.maxamizeWithinSliceContrast.setChecked (False)
            self.ui.fixedRange.setChecked (True)            
            self._levelPresetValueChanged ("Brain") 
    
    def _setWindowLevelSlider (self, window, level) :
         sliderSet = False 
         if (self.ui.LevelSlider.value () != level) : 
                if self.ui.LevelSlider.maximum () < level :
                    self.ui.LevelSlider.setMaximum (level+1)
                if self.ui.LevelSlider.minimum () > level :
                    self.ui.LevelSlider.setMinimum (level-1)
                self.ui.LevelSlider.setValue (level)
                sliderSet = True
         if (self.ui.WindowSlider.value () != window) :
                if self.ui.WindowSlider.maximum () < window :
                    self.ui.WindowSlider.setMaximum (window+1)
                if self.ui.WindowSlider.minimum () > window :
                    self.ui.WindowSlider.setMinimum (window-1)
                self.ui.WindowSlider.setValue (window)
                sliderSet = True
         return sliderSet
                
    def _levelPresetValueChanged (self, selection) :                                
        try :
            self.ui.LevelSlider.valueChanged.disconnect (self._windowLevelValueChanged)            
            levelSliderDisconnect = True
        except :        
            levelSliderDisconnect = False
        try :
            self.ui.WindowSlider.valueChanged.disconnect (self._windowLevelValueChanged)   
            windowSliderDisconnect = True
        except:
            windowSliderDisconnect = False
        try :
            self.ui.LevelPreset.currentTextChanged.disconnect (self._levelPresetValueChanged)   
            LevelPresetDisconnect = True
        except:
            LevelPresetDisconnect = False            
        if (self.ui.LevelPreset.currentText () != selection) :
            self.ui.LevelPreset.setCurrentText (selection)        
        windowLevelSliderSet = False
        if (selection == "Custom") :
            try :
                level, window = self.VisSettings.getCustomLevelWindowPreset ()
            except:
                level = 40
                window = 400           
            windowLevelSliderSet = self._setWindowLevelSlider (window, level)            
        elif (selection in self._windowLevelPresets) :
            window, level = self._windowLevelPresets[selection]
            windowLevelSliderSet = self._setWindowLevelSlider (window, level)                        
        
        
        if (LevelPresetDisconnect) :
            self.ui.LevelPreset.currentTextChanged.connect (self._levelPresetValueChanged)               
        if windowSliderDisconnect :
            self.ui.WindowSlider.valueChanged.connect (self._windowLevelValueChanged)   
        if levelSliderDisconnect :
            self.ui.LevelSlider.valueChanged.connect (self._windowLevelValueChanged)            
        if (windowLevelSliderSet) :
            self._windowLevelValueChanged (-1)            
        self.radioBtnEvent ()
        
    def _updateSettings (self) :        
        if self.ui.setToVolumePrecentRange.isChecked (): 
             self.VisSettings.setMaximizeContrastAcross5To95PrecentInVolume ()
        elif self.ui.setToSlicePrecentRange.isChecked (): 
            self.VisSettings.setMaximizeContrastAcross5To95PrecentInSlice ()
        elif self.ui.maxamizeWithinSliceContrast.isChecked (): 
            self.VisSettings.setMaximizeWithinSliceContrast ()
        else:
            self.VisSettings.setCustomHounsfieldRange (self.ui.minSlider.value (),self.ui.maxSlider.value ())  
            
                
    def radioBtnEvent (self) :
        val = not (self.ui.maxamizeWithinSliceContrast.isChecked () or self.ui.setToVolumePrecentRange.isChecked () or self.ui.setToSlicePrecentRange.isChecked ())
        self.ui.maxValueTxt.setEnabled (val)
        self.ui.minValueTxt.setEnabled (val)
        self.ui.maxSlider.setEnabled (val)
        self.ui.minSlider.setEnabled (val)
        self.ui.LevelPreset.setEnabled (val)
         
        self.ui.levelValue.setEnabled (val)
        self.ui.windowValue.setEnabled (val)
        self.ui.LevelSlider.setEnabled (val)
        self.ui.WindowSlider.setEnabled (val)
        
        self._updateSettings ()    
    
    def _txtWindowValueChanged (self) :
        try:
            level = int (self.ui.levelValue.text ())
            self.ui.LevelSlider.setValue (level)
        except:
            level = self.ui.LevelSlider.value ()
            self.ui.levelValue.setText (str (level))
        
        try:
            window = int (self.ui.windowValue.text ())
            self.ui.WindowSlider.setValue (window)
        except:
            window = self.ui.WindowSlider.value ()
            self.ui.windowValue.setText (str (window))
        
        
        
    def _txtValueChanged (self) :
        try:
            minV = int (self.ui.minValueTxt.text ())
            self.ui.minSlider.setValue (minV)
        except:
            minV = self.ui.minSlider.value ()
            self.ui.minValueTxt.setText (str (minV))
        
        try:
            maxV = int (self.ui.maxValueTxt.text ())
            self.ui.maxSlider.setValue (maxV)
        except:
            maxV = self.ui.maxSlider.value ()
            self.ui.maxValueTxt.setText (str (maxV))
            
        self._updateSettings ()  
            
    def _minValueChanged (self, value) :
        minV = self.ui.minSlider.value ()
        maxV = self.ui.maxSlider.value ()
        if (minV > maxV) :            
            if (minV >= self.ui.minSlider.maximum ()):
                maxV = self.ui.minSlider.maximum ()
                minV = maxV - 1
            else:
                maxV = minV + 1
            self.ui.minSlider.setValue (minV)
            self.ui.maxSlider.setValue (maxV)
        self.ui.maxValueTxt.setText (str(self.ui.maxSlider.value ()))
        self.ui.minValueTxt.setText (str(self.ui.minSlider.value ()))
        
        level = int ((minV + maxV) / 2)
        window = maxV - minV
        if (level  < self.ui.LevelSlider.minimum ()):
            self.ui.LevelSlider.setMinimum (level)
        if (level  > self.ui.LevelSlider.maximum ()):
            self.ui.LevelSlider.setMaximum (level)
        
        if (window < self.ui.WindowSlider.minimum ()):
            self.ui.WindowSlider.setMinimum (window)
        if (window > self.ui.WindowSlider.maximum ()):
            self.ui.WindowSlider.setMaximum (window)   
            
                
        self.ui.levelValue.editingFinished.disconnect (self._txtWindowValueChanged)
        self.ui.windowValue.editingFinished.disconnect (self._txtWindowValueChanged)
        self.ui.LevelSlider.valueChanged.disconnect (self._windowLevelValueChanged)
        self.ui.WindowSlider.valueChanged.disconnect (self._windowLevelValueChanged)   
        self.ui.LevelSlider.setValue (level)
        self.ui.WindowSlider.setValue (window)  
        self.ui.levelValue.setText (str(self.ui.LevelSlider.value ()))
        self.ui.windowValue.setText (str(self.ui.WindowSlider.value ()))
        try :
            self.ui.LevelPreset.currentTextChanged.disconnect (self._levelPresetValueChanged) 
            reconnectPreset = True
        except:
             reconnectPreset = False
        
        preset = self._tryGetWindowLevelPreset (window, level)
        if preset is not None :
            self._levelPresetValueChanged (preset)        
        else:        
            self.ui.LevelPreset.setCurrentText ("Custom")
            self.VisSettings.setCustomLevelWindowPreset (level, window)            
        if (reconnectPreset) :
            self.ui.LevelPreset.currentTextChanged.connect (self._levelPresetValueChanged)  
        
        self.ui.levelValue.editingFinished.connect (self._txtWindowValueChanged)
        self.ui.windowValue.editingFinished.connect (self._txtWindowValueChanged)
        self.ui.LevelSlider.valueChanged.connect (self._windowLevelValueChanged)
        self.ui.WindowSlider.valueChanged.connect (self._windowLevelValueChanged)
        self._updateSettings ()    
    
    def _maxValueChanged (self, value) :
        minV = self.ui.minSlider.value ()
        maxV = self.ui.maxSlider.value ()
        if (minV > maxV) :            
            if (maxV > self.ui.minSlider.minimum ()):
                minV = maxV - 1
            else:
                maxV = minV + 1
            self.ui.minSlider.setValue (minV)
            self.ui.maxSlider.setValue (maxV)
        self.ui.maxValueTxt.setText (str(self.ui.maxSlider.value ()))
        self.ui.minValueTxt.setText (str(self.ui.minSlider.value ()))            
        level = int ((minV + maxV) / 2)
        window = maxV - minV
        if (level  < self.ui.LevelSlider.minimum ()):
            self.ui.LevelSlider.setMinimum (level)
        if (level  > self.ui.LevelSlider.maximum ()):
            self.ui.LevelSlider.setMaximum (level)
        
        if (window < self.ui.WindowSlider.minimum ()):
            self.ui.WindowSlider.setMinimum (window)
        if (window > self.ui.WindowSlider.maximum ()):
            self.ui.WindowSlider.setMaximum (window)    
        self.ui.levelValue.editingFinished.disconnect (self._txtWindowValueChanged)
        self.ui.windowValue.editingFinished.disconnect (self._txtWindowValueChanged)
        self.ui.LevelSlider.valueChanged.disconnect (self._windowLevelValueChanged)
        self.ui.WindowSlider.valueChanged.disconnect (self._windowLevelValueChanged)   
        self.ui.LevelSlider.setValue (level)
        self.ui.WindowSlider.setValue (window)  
        self.ui.levelValue.setText (str(self.ui.LevelSlider.value ()))
        self.ui.windowValue.setText (str(self.ui.WindowSlider.value ()))
        try :
            self.ui.LevelPreset.currentTextChanged.disconnect (self._levelPresetValueChanged) 
            reconnectPreset = True
        except:
             reconnectPreset = False
        preset = self._tryGetWindowLevelPreset (window, level)
        if preset is not None :
            self._levelPresetValueChanged (preset)        
        else:
            self.ui.LevelPreset.setCurrentText ("Custom")
            self.VisSettings.setCustomLevelWindowPreset (level, window)
        if (reconnectPreset) :
            self.ui.LevelPreset.currentTextChanged.connect (self._levelPresetValueChanged)       
        self.ui.levelValue.editingFinished.connect (self._txtWindowValueChanged)
        self.ui.windowValue.editingFinished.connect (self._txtWindowValueChanged)
        self.ui.LevelSlider.valueChanged.connect (self._windowLevelValueChanged)
        self.ui.WindowSlider.valueChanged.connect (self._windowLevelValueChanged)
        self._updateSettings ()    
        
    def _windowLevelValueChanged (self, value) :
        self.ui.levelValue.setText (str(self.ui.LevelSlider.value ()))
        self.ui.windowValue.setText (str(self.ui.WindowSlider.value ()))
        
        minValue = self.ui.LevelSlider.value () - int (self.ui.WindowSlider.value () / 2)
        maxValue = minValue + self.ui.WindowSlider.value () 
        
        if (minValue < self.ui.minSlider.minimum ()):
            self.ui.minSlider.setMinimum (minValue)
        if (minValue > self.ui.minSlider.maximum ()):
            self.ui.minSlider.setMaximum (minValue)    
        
        if (maxValue < self.ui.maxSlider.minimum ()):
            self.ui.maxSlider.setMinimum (maxValue)
        if (maxValue > self.ui.maxSlider.maximum ()):
            self.ui.maxSlider.setMaximum (maxValue)    
        try :       
            self.ui.maxValueTxt.editingFinished.disconnect (self._txtValueChanged)
            self.ui.minValueTxt.editingFinished.disconnect (self._txtValueChanged)
            self.ui.minSlider.valueChanged.disconnect (self._minValueChanged)
            self.ui.maxSlider.valueChanged.disconnect (self._maxValueChanged)  
            reconnectwidgets = True
        except:
            reconnectwidgets = False
        self.ui.minSlider.setValue (minValue)
        self.ui.maxSlider.setValue (maxValue)
        self.ui.maxValueTxt.setText (str(self.ui.maxSlider.value ()))
        self.ui.minValueTxt.setText (str(self.ui.minSlider.value ())) 
        
        level = self.ui.LevelSlider.value ()
        window = self.ui.WindowSlider.value ()
        try :
            self.ui.LevelPreset.currentTextChanged.disconnect (self._levelPresetValueChanged) 
            reconnectPreset = True
        except:
             reconnectPreset = False
        preset = self._tryGetWindowLevelPreset (window, level)
        if preset is not None :
            self._levelPresetValueChanged (preset)        
        else:
            self.ui.LevelPreset.setCurrentText ("Custom")
            self.VisSettings.setCustomLevelWindowPreset (level, window)
        if (reconnectPreset) :
            self.ui.LevelPreset.currentTextChanged.connect (self._levelPresetValueChanged)      
        if (reconnectwidgets) :
            self.ui.maxValueTxt.editingFinished.connect (self._txtValueChanged)
            self.ui.minValueTxt.editingFinished.connect (self._txtValueChanged)
            self.ui.minSlider.valueChanged.connect (self._minValueChanged)
            self.ui.maxSlider.valueChanged.connect (self._maxValueChanged)  
        self._updateSettings ()    
    
    
        
    def closeDlg (self):                    
        self.accept ()
