#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 14:19:00 2017

@author: m160897
"""

"""
    RCC_ProjectROIEditorDlg
    
    Modal dialog to enable user to define and modify ROI objects
"""    
import os
from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import  QDialog, QAbstractItemView, QComboBox, QItemDelegate
from PyQt5.QtGui import QStandardItemModel
from PyQt5.QtCore import QModelIndex
from rilcontourlib.util.rcc_util import ResizeWidgetHelper, HeaderObject
from rilcontourlib.ui.qt_ui_autogen.RCC_ExportTagsDlgAutogen import Ui_RCC_ExportTagDlg


class QListDelegate (QItemDelegate) :    
    def __init__ (self, columnchooserlistorder, datasetTagManager, datasetUIHeader, OldHeaderObjList, RegistrationTag = False, parent = None) :         
         QItemDelegate.__init__ (self, parent)                  
         self._registrationTag = RegistrationTag
         self._parameterList = columnchooserlistorder         
         self._DatasetUIHeader = datasetUIHeader
         self._DatasetTagManager = datasetTagManager
         self._OldHeaderObjList = OldHeaderObjList
         
    def createEditor(self, parent, options, qModelIndex) :       
        editor = QComboBox (parent)
        editor.setAutoFillBackground(True)
        for x in self._parameterList :
            editor.addItem (str (x))         
        index = int (qModelIndex.model().data(qModelIndex, QtCore.Qt.UserRole))                
        editor.setCurrentIndex(index) 
        return editor
        
    def setEditorData(self, qWidgetEditor, qModelIndex) :
        # Get the value via index of the Model        
        index = int (qModelIndex.model().data(qModelIndex, QtCore.Qt.UserRole))        
        qWidgetEditor.setCurrentIndex(index) 
        
        
    def setModelData(self,  qWidgetEditor, qAbstractModel, qModelIndex) :        
        currentIndex = qWidgetEditor.currentIndex()                
        currentText = qWidgetEditor.currentText() 
        qAbstractModel.setData(qModelIndex, str (currentIndex), QtCore.Qt.UserRole)        
        qAbstractModel.setData(qModelIndex, currentText, QtCore.Qt.EditRole)        
        rowindex = qModelIndex.row ()
        
        if (currentIndex == 0) :
            self._DatasetUIHeader.setHeaderByIndex (rowindex, HeaderObject ("Description","Description"))
        elif (currentIndex == 1 and self._registrationTag) :
            self._DatasetUIHeader.setHeaderByIndex (rowindex, HeaderObject ("Registration","Registration"))
        else:
            if (self._registrationTag) :
                tagIndex = currentIndex - 2
            else:
                tagIndex = currentIndex - 1        
            if (tagIndex < self._DatasetTagManager.getCustomTagCount ()) :
                tag = self._DatasetTagManager.getCustomTagByIndex (tagIndex)
            else:
                tagIndex = tagIndex - self._DatasetTagManager.getCustomTagCount ()
                if (tagIndex < self._DatasetTagManager.getInternalTagCount ()) :
                    tag = self._DatasetTagManager.getInternalTagByIndex (tagIndex)                
                else: 
                    tag = None
                    tagIndex = tagIndex - self._DatasetTagManager.getInternalTagCount ()
            
            if (tag is not None) : # tag was found
                self._DatasetUIHeader.setHeaderByIndex (rowindex, HeaderObject ("Tag",tag.getName (), tag.getIdentifer ()))
            else:
                # tag was not found old header value chosen
                self._DatasetUIHeader.setHeaderByIndex (rowindex, self._OldHeaderObjList[tagIndex])
        
    def updateEditorGeometry(self, qWidgetEditor, qStyleOptionViewItem, qModelIndex) :
        qWidgetEditor.setGeometry(qStyleOptionViewItem.rect)
        
        
class MyModel(QStandardItemModel) :           
    
    def flags (self, qModelIndex) :                
        return  QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable         


class RCC_ExportTagsDlg (QDialog) :
    
    def __init__ (self, parent, datasetUIHeader, datasetTagManager, ProjectDataset, ShowRegistrationTag = False, LimitToSeriesWithTagsEnabled = False) :
        self._parent = parent
        QDialog.__init__ (self, None)
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)
        self.ReturnValue = []
        self._limitToSeriesWithData = True
        self._datasetTagManager = datasetTagManager
        self.ui = Ui_RCC_ExportTagDlg ()        
        self.ui.setupUi (self)                
        self._resizeWidgetHelper = ResizeWidgetHelper ()
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.tags_tableView)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.Add_Btn)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.Remove_Btn)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.Ok_Btn)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.Cancel_Btn)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.LimitToSerieWithDataChkBox)
        self.ui.LimitToSerieWithDataChkBox.setChecked (True)
        self.ui.LimitToSerieWithDataChkBox.setEnabled (LimitToSeriesWithTagsEnabled)
        basepath = ProjectDataset.getPythonProjectBasePath ()
        iconPath = os.path.join (basepath , "icons")
        self.ui.Add_Btn.setIcon(QtGui.QIcon(iconPath + os.sep + "plus.png"))
        self.ui.Add_Btn.setIconSize(QtCore.QSize(15,15))        
        self.ui.Remove_Btn.setIcon(QtGui.QIcon(iconPath + os.sep + "minus.png"))
        self.ui.Remove_Btn.setIconSize(QtCore.QSize(15,15))
        self.ui.Remove_Btn.setEnabled (False)
        
        self.ui.Cancel_Btn.clicked.connect (self.CancelBtn)
        self.ui.Ok_Btn.clicked.connect (self.OkBtn)        
        
        self.ui.Add_Btn.clicked.connect (self._addTagBtn)
        self.ui.Remove_Btn.clicked.connect (self._removeTagBtn)                

        self._DatasetHeader = datasetUIHeader.copy ()
        self._DatasetHeader.removeDatasetHeader ()
        self._ColumnChooserListOrder = []
        self._ColumnChooserListName = []
        self._OldHeaderObjList = []
        self._ColumnChooserListOrder.append (("Description", "Description"))
        self._ColumnChooserListName.append ("Description")
        self._showregistrationTag = ShowRegistrationTag
        if (self._showregistrationTag) :            
            self._ColumnChooserListOrder.append (("Registration", "Registration"))        
            self._ColumnChooserListName.append ("Registration")
        
        for customTagIndex in range (datasetTagManager.getCustomTagCount ()) :            
             tag = datasetTagManager.getCustomTagByIndex (customTagIndex)
             self._ColumnChooserListOrder.append (tuple (tag.getIdentifer ()))
             self._ColumnChooserListName.append (tag.getName ())                    
        
        for internalTagIndex in range(datasetTagManager.getInternalTagCount ()) :  
             tag = datasetTagManager.getInternalTagByIndex (internalTagIndex)
             self._ColumnChooserListOrder.append (tuple (tag.getIdentifer ()))
             self._ColumnChooserListName.append (tag.getName ())                    
        
        for index in range (self._DatasetHeader.headerObjectCount ()) :            
            headerObject = self._DatasetHeader.getHeaderObjectAtIndex (index)
            if (headerObject.getIdentifer () not in self._ColumnChooserListOrder) :
                self._ColumnChooserListOrder.append (headerObject.getIdentifer ())
                self._ColumnChooserListName.append (headerObject.getName ())
                self._OldHeaderObjList.append (headerObject)
            
        if (self._DatasetHeader.headerObjectCount () == 0) :
            self._addTagBtn ()
        else:
            self._UpdateTable ()        
                
       
    def _UpdateTable (self) :  
        self._oldTableRowSelection  = []        
                
        self._tableModel = MyModel ( self._DatasetHeader.headerObjectCount (), 1, self)        
        self.ui.tags_tableView.horizontalHeader().setStretchLastSection(True)
        self._tableModel.setHorizontalHeaderLabels (["Tags"])
                            
        for row in range (self._DatasetHeader.headerObjectCount ()) :            
            headerObject = self._DatasetHeader.getHeaderObjectAtIndex (row)
            if (headerObject.getIdentifer () in self._ColumnChooserListOrder) :
                val = self._ColumnChooserListOrder.index (headerObject.getIdentifer ())
                
                index = self._tableModel.index(row,0,QModelIndex())                      
                self._tableModel.setData(index, str(val), QtCore.Qt.UserRole)            
                self._tableModel.setData(index, headerObject.getName(), QtCore.Qt.EditRole)                        
                                    
                
        self.ui.tags_tableView.setModel (self._tableModel)                        
        self.ui.tags_tableView.setSelectionBehavior( QAbstractItemView.SelectItems );
        self.ui.tags_tableView.setSelectionMode( QAbstractItemView.SingleSelection );
        self.ui.tags_tableView.setItemDelegateForColumn(0, QListDelegate (self._ColumnChooserListName,self._datasetTagManager, self._DatasetHeader, self._OldHeaderObjList, self._showregistrationTag, self.ui.tags_tableView))        
        self.ui.Remove_Btn.setEnabled (self.ui.tags_tableView.selectionModel() is not None and self.ui.tags_tableView.selectionModel().hasSelection () and self._tableModel.rowCount () > 0)            
        selectionmodel = self.ui.tags_tableView.selectionModel()
        if (selectionmodel is not None) :
            selectionmodel.selectionChanged.connect(self._tabelSelectionChanged)                                                
    
    def _tabelSelectionChanged (self, selected, deselected) :        
        self.ui.Remove_Btn.setEnabled (self.ui.tags_tableView.selectionModel() is not None and self.ui.tags_tableView.selectionModel().hasSelection () and self._tableModel.rowCount () > 0)            
       
        
        
    def _internalWindowResize (self, newsize):                            
        if (self._resizeWidgetHelper != None) :                                               
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.tags_tableView, newsize)            
            self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.tags_tableView, newsize)   
                        
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.LimitToSerieWithDataChkBox, newsize.height () - 92)            
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.Add_Btn, newsize.height () - 52)                        
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.Remove_Btn, newsize.height () - 52)                                            
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.Ok_Btn, newsize.height () - 48)                        
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.Cancel_Btn, newsize.height () - 48)                                    
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.Ok_Btn, newsize.width () - 183)                        
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.Cancel_Btn, newsize.width () - 93)                                
                                       
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)                 
        self._internalWindowResize ( qresizeEvent.size () )                       
    
    
    def _addTagBtn (self) :
        self._DatasetHeader.addHeaderObject (HeaderObject ("Description","Description"))
        self._UpdateTable ()
        self.ui.Remove_Btn.setEnabled (self.ui.tags_tableView.selectionModel() is not None and self.ui.tags_tableView.selectionModel().hasSelection () and self._tableModel.rowCount () > 0)                    
    
    def _removeTagBtn (self) :
        selectionModel = self.ui.tags_tableView.selectionModel()
        row = -1
        if (selectionModel.hasSelection ()) :            
            row = selectionModel.currentIndex ().row ()            
        if (row >= 0) :
            self._DatasetHeader.removeHeaderObjectAtIndex (row)
            self._UpdateTable ()
        self.ui.Remove_Btn.setEnabled (self.ui.tags_tableView.selectionModel() is not None and self.ui.tags_tableView.selectionModel().hasSelection () and self._tableModel.rowCount () > 0)            
        return 
    
    def OkBtn (self):        
        self.ReturnValue = []
        for index in range (self._DatasetHeader.headerObjectCount ()):
            obj = self._DatasetHeader.getHeaderObjectAtIndex (index)
            self.ReturnValue.append (obj)
        self._limitToSeriesWithData = self.ui.LimitToSerieWithDataChkBox.isChecked ()
        self.accept ()
        #self.accept ()
    
    def getLimitToSeriesWithData (self) :
        return self._limitToSeriesWithData
    
    def getExportTagList (self):
        return self.ReturnValue
    
    def CancelBtn (self):
        self.ReturnValue = []
        self._limitToSeriesWithData = True
        self.reject ()
        
