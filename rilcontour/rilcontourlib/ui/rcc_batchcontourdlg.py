#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  5 09:19:27 2016

@author: m160897
"""
from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import  QDialog
from rilcontourlib.ui.qt_ui_autogen.rcc_batchcontour import  Ui_RCC_BatchContourUI
from rilcontourlib.util.rcc_util import ResizeWidgetHelper, FindContours
from rilcontourlib.dataset.roi.rcc_roi import ROIDictionary
from rilcontourlib.dataset.roi.rcc_roiobjects import MultiROIAreaObject

class BatchContourUIDlg (QDialog) :
    
    def __init__ (self, App, parent, startingCoordinate, selectedROI) : 
        self._tempPointIndex = -1
        self.returnResult = "Cancel"        
        selectedROI = sorted (selectedROI)
        self.resizeWidgetHelper = None        
        QDialog.__init__ (self, parent)
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)         
        self.ui = Ui_RCC_BatchContourUI ()                
        self.ui.setupUi (self)                       
        self.setMinimumSize(self.size ())
        self.resizeWidgetHelper = ResizeWidgetHelper ()    
        self.resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ROI_lbl)        
        self.resizeWidgetHelper.saveWidgetUIPos (self, self.ui.NextObjBtn)
        self.resizeWidgetHelper.saveWidgetUIPos (self, self.ui.XYSliceView)
        self.resizeWidgetHelper.saveWidgetUIPos (self, self.ui.SliceLbl)
        self.resizeWidgetHelper.saveWidgetUIPos (self, self.ui.OK_BTN)
        self.resizeWidgetHelper.saveWidgetUIPos (self, self.ui.Cancel_BTN)

        self.ui.NextObjBtn.clicked.connect (self.NextObj)
        
        self.ui.OK_BTN.clicked.connect (self.OkBtn)
        self.ui.Cancel_BTN.clicked.connect (self.CloseBtn)
        self.ui.ResetBtn.clicked.connect (self.ResetSliceROIBtn)
        self.ui.XYSliceView.setHounsfieldVisSettings (parent.hounsfieldVisSettings)
        self.ui.XYSliceView.setVisSettings (parent.visSettings)        
        self.ui.XYSliceView.addMouseMoveListener (self.MoveEvent)
        self.ui.XYSliceView.addMouseDblClickListener (self.MouseDblClick)
        self.ui.XYSliceView.addMousePressedListener (self.MousePressedEvent)   
        
        self._CurrentSlice = startingCoordinate.copy ()                     
        zCord = self._CurrentSlice.getZCoordinate ()
                        
        self._ContourUP = True
        self._firstSlice = 0
        self._lastSlice =  parent.NiftiVolume.getZDimSliceCount () -1    
        self._startSlice = zCord
        
        self._ROICountourAdded = []        
        for roi in selectedROI :
            self._ROICountourAdded.append (True)            
            
        self._TemporaryROIDictionary = ROIDictionary (parent.ROIDictionary.getROIDefs (), parent.ROIDictionary.getHounsfieldSettings(), parent.ROIDictionary.getVisSettings())                        
        
        self.ui.XYSliceView.setNiftiDataSet (parent.NiftiVolume, parent._ProjectDataset)
        self.ui.XYSliceView.initNiftiDataSet ( self._CurrentSlice,  "Z", callback = False)             
        self._parent = parent
        self._selectedROIList = selectedROI
        self._selectedROIIndex = 0

                
        self._contour = []
        self.ui.XYSliceView.clearTemporaryDrawLine ()
                
        self.ui.SliceLbl.setText ("Slice: " + str (self._CurrentSlice.getZCoordinate ()))
        
        palette = QtGui.QPalette()
        palette.setColor (self.ui.ROI_lbl.foregroundRole(), self._getSelectedROIColor ())
        self.ui.ROI_lbl.setPalette(palette)         
        self.ui.ROI_lbl.setText (self._selectedROIList[self._selectedROIIndex])
        self.ui.XYSliceView.setMouseTracking (True)
    
    def _cleanup (self):
       self.ui.XYSliceView.setMouseTracking (False)
       self.ui.XYSliceView.delete ()  
       self.resizeWidgetHelper = None
               
    def closeEvent(self,event):    
        #print ("Close Event")
        self._cleanup ()       
        QDialog.closeEvent (self, event)   

    def reject(self) :
        #print ("Rejected")
        self._cleanup ()
        QDialog.reject (self)
    
    def _getSelectedROIColor (self):
         defs = self._TemporaryROIDictionary.getROIDefs ()
         activeROI = self._selectedROIList[self._selectedROIIndex]
         return defs.getROIColor (activeROI)
    
    def ResetSliceROIBtn (self) :
        self._tempPointIndex = -1
        self._contour = []
        self.ui.XYSliceView.clearTemporaryDrawLine ()
        self.ui.XYSliceView.update () # repaint        
    
    def _contourAddTempPoint (self, x, y):
        addPt = True
        lastindex = len (self._contour)  - 1
        if (lastindex > 0) :
            x2 = self._contour[lastindex][0]
            y2 = self._contour[lastindex][1]
            if (x2 == x) and (y2 == y) :
                addPt = False
                
        if (self._tempPointIndex == -1):
            if (addPt) :
                self._contour.append ((x, y))
                self._tempPointIndex = len (self._contour) - 1
        else:
            if (addPt) :
                self._contour[self._tempPointIndex] = (x, y)
            
    def _contourAddPoint (self, x, y) :
        
        addPt = True
        lastindex = len (self._contour)  - 1
        if (lastindex > 0) :
            x2 = self._contour[lastindex][0]
            y2 = self._contour[lastindex][1]
            if (x2 == x) and (y2 == y) :
                addPt = False
                
        if (self._tempPointIndex == -1):            
            if (addPt) :
                self._contour.append ((x, y))
        else:
            if (addPt) :
                self._contour[self._tempPointIndex] = (x, y)            
            self._tempPointIndex = -1
    
    def MoveEvent (self, line, eventtype, sliceview, MouseButtonPressed, ClickEventHandled):                                       
        length = len (line) - 1
        x1 = int (line[length][0])
        y1 = int (line[length][1])        

        if (eventtype == "MouseMoveButtonUp") :
            if (length >= 0) :            
                self._contourAddTempPoint (x1, y1)                            
                self.ui.XYSliceView.setTemporaryDrawLine (self._contour, self._getSelectedROIColor ())            
                self.ui.XYSliceView.update ()
                #print ("Draw Line!!!")
        else:        
            if (length >= 0) :
                self._contourAddPoint (x1, y1)            
                self.ui.XYSliceView.setTemporaryDrawLine (self._contour, self._getSelectedROIColor ())
            else:
                self._contourAddPoint (x1, y1)
            self.ui.XYSliceView.update ()
                 
    def MousePressedEvent  (self, x1, y1, niftiSliceWidgt) :        
        x1 = int (x1)
        y1 = int (y1)
        length = len (self._contour) - 1
        if (length >= 0) :
            self._contourAddPoint (x1, y1)            
            self.ui.XYSliceView.setTemporaryDrawLine (self._contour, self._getSelectedROIColor ())
        else:
            self._contourAddPoint (x1, y1)            
            
        self.ui.XYSliceView.update () # repaint        

        
    def MouseDblClick  (self, x, y) :                    
        length = len (self._contour) - 1
        if (length > 0) :
            x1 = self._contour[0][0]
            y1 = self._contour[0][1]
            self._contourAddPoint (x1, y1)            
            self.ui.XYSliceView.setTemporaryDrawLine (self._contour, self._getSelectedROIColor ())        
        self.ui.XYSliceView.update () # repaint        
        self.NextObj ()
        

    
    def NextObj (self) :               
       self._tempPointIndex = -1
       itemName = self._selectedROIList[self._selectedROIIndex]
       sliceNumber = self._CurrentSlice.getZCoordinate ()       
       stepsize = 3
       if (len (self._contour) > 2) :
           self._contour = FindContours.correctContourWinding ( self.ui.XYSliceView.getSliceShape(), self._contour)
       if (len (self._contour) > 2) :
           selectedROI = self._selectedROIList[self._selectedROIIndex]
           
           if (not self._TemporaryROIDictionary.isROIDefined (selectedROI)):       
               obj = MultiROIAreaObject (sliceNumber, self._contour, self.ui.XYSliceView.getSliceShape(), "Human" )                             
           else:
               obj = self._TemporaryROIDictionary.getROIObject (selectedROI)                              
               #slight hack, only allocate 1 contour per ROI. get next slice ROI simply by 
               contourID = self._TemporaryROIDictionary.getROIContourManger (selectedROI).getAllocatedIDList ()[0] 
               obj.setSliceROIPerimeter (contourID, sliceNumber , self._contour, self.ui.XYSliceView.getSliceShape(), "Human")                                      
           obj.adjustBoundBoxToFitSlice(sliceNumber, self._parent.NiftiVolume.getSliceDimensions ())                      
           self._TemporaryROIDictionary.addROI (itemName, obj)
           obj.setSelected (False)                                 
                      
           self.ui.XYSliceView.setROIObjectList (self._TemporaryROIDictionary)               
           self._ROICountourAdded[self._selectedROIIndex] = True
       else:
           self._ROICountourAdded[self._selectedROIIndex] = False
       self._contour = []           

       self._selectedROIIndex += 1
       while (self._selectedROIIndex < len (self._ROICountourAdded) and self._ROICountourAdded[self._selectedROIIndex] != True) :
           self._selectedROIIndex += 1
                                 
       if (self._selectedROIIndex >= len (self._selectedROIList)):
           if (self._ContourUP) :
               if (sliceNumber > self._firstSlice) and (True in self._ROICountourAdded) :
                   if  (sliceNumber - stepsize >= self._firstSlice)  :
                       sliceNumber -= stepsize
                   else:
                       sliceNumber = self._firstSlice
                   self._selectedROIIndex = 0                                          
                   while (self._selectedROIIndex < len (self._ROICountourAdded) and self._ROICountourAdded[self._selectedROIIndex] != True):
                       self._selectedROIIndex += 1                                  
               else:
                   sliceNumber = self._startSlice           
                   self._ContourUP = False                   
                   for index in range (len (self._ROICountourAdded)) :
                       self._ROICountourAdded[index] = True                                 
           if (not self._ContourUP) :
               if  (sliceNumber  < self._lastSlice) and (True in self._ROICountourAdded) :
                   if  (sliceNumber + stepsize <= self._lastSlice)  :
                       sliceNumber += stepsize
                   else:
                       sliceNumber = self._lastSlice
                   self._selectedROIIndex = 0                       
                   while (self._selectedROIIndex < len (self._ROICountourAdded) and self._ROICountourAdded[self._selectedROIIndex] != True):
                       self._selectedROIIndex += 1                                       
               else:                    
                   self.OkBtn ()           
                   return
            
           self._CurrentSlice.setZCoordinate (sliceNumber)                                      
           self.ui.XYSliceView.setNiftiDataSet (self._parent.NiftiVolume, self._parent._ProjectDataset)
           self.ui.XYSliceView.initNiftiDataSet ( self._CurrentSlice,  "Z", callback = False)    
                     
           self.ui.XYSliceView.setROIObjectList (self._TemporaryROIDictionary)        
           self.ui.SliceLbl.setText ("Slice: " + str (self._CurrentSlice.getZCoordinate ()))    
                                    
       self.ui.ROI_lbl.setText (self._selectedROIList[self._selectedROIIndex])
       palette = QtGui.QPalette()
       palette.setColor (self.ui.ROI_lbl.foregroundRole(), self._getSelectedROIColor ())
       self.ui.ROI_lbl.setPalette(palette)                 
       self.ui.ROI_lbl.setText (self._selectedROIList[self._selectedROIIndex])       
       self.ui.XYSliceView.update ()
       
           
    # Window resize event, resizes main window objects proportionally to reflect current window size
    def _internalWindowResize (self, newsize):                   
        if (self.resizeWidgetHelper != None) :                                   
            self.resizeWidgetHelper.setProportionalWidth  (self.ui.ROI_lbl, newsize)                               
            self.resizeWidgetHelper.setProportionalXPos  (self.ui.NextObjBtn, newsize)
            self.resizeWidgetHelper.setProportionalXPos  (self.ui.XYSliceView, newsize)
            self.resizeWidgetHelper.setProportionalYPos  (self.ui.XYSliceView, newsize)
            self.resizeWidgetHelper.setProportionalHeight  (self.ui.XYSliceView, newsize)            
            self.resizeWidgetHelper.setProportionalWidth  (self.ui.XYSliceView, newsize)       
            self.resizeWidgetHelper.setProportionalWidth  (self.ui.SliceLbl, newsize)                   
            self.resizeWidgetHelper.setProportionalYPos  (self.ui.SliceLbl, newsize)                   
            self.resizeWidgetHelper.setProportionalXPos  (self.ui.OK_BTN, newsize)
            self.resizeWidgetHelper.setProportionalYPos  (self.ui.OK_BTN, newsize)
            self.resizeWidgetHelper.setProportionalXPos  (self.ui.Cancel_BTN, newsize)
            self.resizeWidgetHelper.setProportionalYPos  (self.ui.Cancel_BTN, newsize)
    
    def resizeEvent (self, qresizeEvent) :                       
        self._internalWindowResize ( qresizeEvent.size () )               
        QDialog.resizeEvent (self, qresizeEvent)  
    
        
    def OkBtn (self):
        if (len (self._contour) > 0) :
            self.NextObj ()
        self.returnResult = "OK"
        self.ui.XYSliceView.setMouseTracking (False)
        self.accept ()
        self.close ()        
        
    def getReturnResult (self) :
        return self.returnResult 
        
    def getDialogROIDictionary (self) :
        return self._TemporaryROIDictionary 
        
    def CloseBtn (self):
        self.ui.XYSliceView.setMouseTracking (False)
        self.reject ()        
        self.close ()
