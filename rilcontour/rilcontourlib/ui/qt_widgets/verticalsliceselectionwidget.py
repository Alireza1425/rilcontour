#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  7 14:38:57 2019

@author: m160897
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Sep 16 12:52:22 2016

@author: Philbrick
"""

from PyQt5 import QtCore, QtGui
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QWidget
from six import *
import numpy as np
import copy
from PyQt5.QtWidgets import QApplication
from rilcontourlib.util.rcc_util import FastUnique

class VerticalSliceSelectionWidget (QtWidgets.QWidget):
    
    def __init__ (self, parent=None):
        QWidget.__init__ (self, parent)
        self._ColorMask = None
        self._WHITEColorList = [QtGui.QColor (255,255,255, 255)]
        self._colorCache = {}
        self._parent = parent
        self._windowUpdateEnabled = True
        self._pendingWindowUpdate = False    
        self._offscreenBitmapDim = None        
        self._OffScreenBuffer = None
        self._ROISliceDictionary = {}        
        self._SlicesContoured = []
        self._shiftKeySelection = []
        self._SliceCount = None
        self._sliceListener = []
        self._deleteSliceCallback = []
        self._shiftKey = False
        self._SelectedSlice = 0
        self._AxisDim = None
        self._SliceDictionaryChangedSinceLastPaint = True
        self._TempBufferShape = None 
        self._TempBuffer = None
        self._ROIDefs = None
        self._mouseDown = False
        self._rangeSelectionEnabled = False
        self._deleteSliceEnabled = False
        self._oldSelection = None
        self._oldFocus = None
        self._sliceView = None
        self._colorTable = None
        
    def initWidget (self, SliceView = None) :
        self.setFocusPolicy( QtCore.Qt.NoFocus )     
        self._ColorMask = None
        self._OffScreenBuffer = QtGui.QPixmap (self.width  (), self.height ())        
        self._ROISliceDictionary = {}        
        self._SliceAxis = None
        self._offscreenBitmapDim = None        
        self._SlicesContoured = []
        self._shiftKeySelection = []
        self._shiftKey = False
        self._SliceCount = None
        self._sliceListener = []
        self._deleteSliceCallback = []
        self._SelectedSlice = 0
        self._windowUpdateEnabled = True
        self._pendingWindowUpdate = False
        self._SliceDictionaryChangedSinceLastPaint = True
        self._TempBufferShape = None 
        self._TempBuffer = None
        self._AxisDim = None
        self._ROIDefs = None
        self._mouseDown = False
        self._rangeSelectionEnabled = False
        self._deleteSliceEnabled = False
        self._oldSelection = None
        self._oldFocus = None
        self._sliceView  = SliceView
        self._colorTable = None
        
    def delete (self) :
        self._ColorMask = None        
        self._OffScreenBuffer = None
        self._ROISliceDictionary = {}                
        self._SliceAxis = None        
        self._SliceCount = None        
        self._SlicesContoured = []
        self._shiftKeySelection = []
        self._shiftKey = False
        self._sliceListener = []
        self._deleteSliceCallback = []
        self._SelectedSlice = 0
        self._windowUpdateEnabled = True
        self._pendingWindowUpdate = False
        self._SliceDictionaryChangedSinceLastPaint = True
        self._TempBufferShape = None 
        self._TempBuffer = None
        self._AxisDim = None
        self._ROIDefs = None
        self._mouseDown = False
        self._rangeSelectionEnabled = False
        self._deleteSliceEnabled = False
        self._oldSelection = None
        self._oldFocus = None
        self._sliceView = None
        self._colorTable = None

    def getSelectionDescription (self) :        
        return (self._SelectedSlice, self._shiftKey, self._shiftKeySelection)
    
    def resetSelectionFromDescription (self, description) :
        selSlice, shiftKey, shiftSel = description
        self._SelectedSlice =  selSlice 
        self._shiftKey = shiftKey
        self._shiftKeySelection =  shiftSel 
        self.update ()           
        self._callSliceListener (selSlice) 
        
    def getSliceAxis (self):
        return self._SliceAxis 
        
    def addDeleteSliceListener (self, callback):
        if callback not in self._deleteSliceCallback :
            self._deleteSliceCallback.append (callback)         
    
    def _callDeleteSliceListener (self):        
        for i in self._deleteSliceCallback :
            i (self)
       
    
    def setWindowUpdateEnabled (self, stat) :
        if self._windowUpdateEnabled != stat :
            self._windowUpdateEnabled = stat 
            if (stat and self._pendingWindowUpdate) :
                self.update ()                
    
    def update (self,x1 = None, x2 = None, x3 = None, x4 = None) :
        if self._windowUpdateEnabled :
            if (x1 is not None) :
                super(VerticalSliceSelectionWidget, self).update (x1, x2, x3, x4)
            else:
                super(VerticalSliceSelectionWidget, self).update ()
            self._pendingWindowUpdate = False
        else:
            self._pendingWindowUpdate = True    
            
    def drawColorBox (self, qp, colorlist, xC, yC, xSize, ySize, DrawBorder):      
        if xSize > 0 and ySize > 0 :
            numberofColors = len (colorlist)                    
            if numberofColors >= xSize :          
                yP = int (yC + ySize)            
                for i in range (xSize) :
                    color = colorlist[i]
                    qp.setBrush (color)
                    qp.setPen (color)                
                    xE = int (i + xC)
                    qp.drawLine (xE, yC, xE, yP)  
            else:                            
                rightP = int (xC)                 
                scaleFactor = float (xSize) / float (numberofColors)                
                rightPos = rightP + xSize -1
                for i in range (numberofColors) :
                    leftP    = rightP
                    rightP = int (xC +scaleFactor * float (i+1))                
                    color = colorlist[i]
                    qp.setBrush (color)
                    qp.setPen (color)                
                    if (leftP < rightPos) :
                        qp.drawRect (leftP, yC, max (rightP - leftP, 1), ySize)                
            if DrawBorder :
                qp.setPen ( QtGui.QColor(0,0,0))
                qp.setBrush ( QtGui.QColor(0,0,0, 0))
                qp.drawRect (xC, yC, xSize-1, ySize)                                                              
                
    def setSliceCount (self,  count, sliceAxis, callback=True) :        
        self._SliceAxis = sliceAxis
        self._ROISliceDictionary = {}       
        self._SliceCount = count        
        self._SlicesContoured = [] #np.empty ((self.SliceCount),dtype=SliceContours)        
        self.setSelectedSlice (0, callback)    
    
    @staticmethod
    def listsEqual (list1, list2) :
        if (list1 is  None and list2 is None) :
            return True
        if (list1 is not None and list2 is None) :
            return False
        if (list1 is None and list2 is not None) :
            return False
        if (len (list1) != len (list2)) :
            return False
        if tuple (list1) != tuple (list2) :
            return False
        return True
    
    def addSliceListener (self, event) :
         if event not in self._sliceListener :
             self._sliceListener.append (event)
         
    def _callSliceListener (self, cordinate, DisableCallbacks = None) :    
        for i in self._sliceListener :        
            if DisableCallbacks is None or i not in DisableCallbacks :
                i ( cordinate, self._SliceAxis)     
            
    def isShiftPressed (self):
        #self._mouseDown = (self._mouseDown and QApplication.mouseButtons() == QtCore.Qt.LeftButton)         
        #result =  self._mouseDown or (QtCore.Qt.ShiftModifier & QApplication.queryKeyboardModifiers () == QtCore.Qt.ShiftModifier)        
        if not self._rangeSelectionEnabled :
            return False
        result =  (QtCore.Qt.ShiftModifier & QApplication.queryKeyboardModifiers () == QtCore.Qt.ShiftModifier)        
        if (result and not self._shiftKey) :     
                 self._shiftKey = True
                 self._shiftKeySelection = [self._SelectedSlice]                 
        elif (not result and self._shiftKey) :     
            self._shiftKey = False
            self._shiftKeySelection = []
        return self._shiftKey
    
    def getSelectdSlice (self) :
        return  self._SelectedSlice
    
    def setSelectedSlice (self, index, callback=True, DisableCallbacks = None):        
        if (self._SliceCount > 0) :            
            if index < 0 :
                index = 0 
            elif index > self._SliceCount -1 :
                index = self._SliceCount -1     
                
            oldSelection = copy.copy(self._shiftKeySelection)
            isShiftPressed = self.isShiftPressed () #if shift is pressed sets shift selection
            selectedIndex = int (index)
            if (not isShiftPressed and len (self._shiftKeySelection) > 0):
                self._shiftKeySelection = []
            if (self._SelectedSlice != selectedIndex or not VerticalSliceSelectionWidget.listsEqual (oldSelection, self._shiftKeySelection)) :
                self._SelectedSlice = selectedIndex
                self.update () 
                if (callback) :
                    #print ("setSelectedSlice.update")
                    self._callSliceListener (index, DisableCallbacks = DisableCallbacks)
    
    @staticmethod
    def _getColorMaskList (sliceNumber, mask, colorTable, Axis, ZeroMem = None) :        
        if Axis == "X" :
            slicedata = FastUnique.unique (mask[sliceNumber,:,:], MaxInt = colorTable.shape[1], ZeroMem = ZeroMem)
        elif Axis == "Y" :
            slicedata = FastUnique.unique (mask[:,sliceNumber,:], MaxInt = colorTable.shape[1], ZeroMem = ZeroMem)
        else:
            slicedata = FastUnique.unique (mask[:,:,sliceNumber], MaxInt = colorTable.shape[1], ZeroMem = ZeroMem)        
        if len (slicedata) <= 1 :
            return  [0]
        colorList = []
        color = colorTable[slicedata] 
        maxColor = min (color.shape[0],23)
        for index in range (1,maxColor) :
            colorList.append (index)
        return colorList
    
    def setSliceColorMask (self, colorMask, colorTable) :        
        if colorMask is None :
            if self._ColorMask is not None:
             self._ColorMask = None
             self._colorTable = None
             self._SliceDictionaryChangedSinceLastPaint = True
        else:
            self._ColorMask = {}
            colorMask = colorMask.getImageData ()
            if self._SliceAxis == "X" :
                AxisDim = colorMask.shape[0]
            elif self._SliceAxis == "Y" :
                AxisDim = colorMask.shape[1]
            elif self._SliceAxis == "Z" :
                AxisDim = colorMask.shape[2]
            
            ZeroMem = np.zeros ((colorTable.shape[1]), dtype = np.bool)
            for sliceindex in range(AxisDim) :
                self._ColorMask[sliceindex] = VerticalSliceSelectionWidget._getColorMaskList (sliceindex, colorMask, colorTable, self._SliceAxis, ZeroMem = ZeroMem)
            self._colorTable = {}
            self._colorTable[0] = QtGui.QColor(255,255,255)                    
            for index in range (1, colorTable.shape[0]) :
                self._colorTable[index] = QtGui.QColor(colorTable[index,2],colorTable[index,1],colorTable[index,0])                    
            self._SliceDictionaryChangedSinceLastPaint = True
        self.update ()  
    
    def getSliceColorMask (self) :
        return (self._ColorMask, self._colorTable)
    
    def setROISliceDictionary (self, ROIDictionary, NIfTIVolume, ProjectDataset,  AxisProjection = None, Coordinate = None):        
        if (ROIDictionary is None or NIfTIVolume is None) :
            self._rangeSelectionEnabled = False 
            self._deleteSliceEnabled = False 
            if len (self._ROISliceDictionary) > 0 or self._ROIDefs is not None  :
                self._SliceDictionaryChangedSinceLastPaint = True
                self._ROISliceDictionary = {}
                self._ROIDefs = None
                self.update ()
            return 
        self._rangeSelectionEnabled = self._SliceAxis == "Z" or ROIDictionary.isROIinAreaMode ()
        self._deleteSliceEnabled = self._rangeSelectionEnabled 
        ObjList = []
        self._ROIDefs = ROIDictionary.getROIDefs ()
        nameList = self._ROIDefs.getSelectedROI() 
        for name in nameList :
            if ROIDictionary.isROIDefined (name) :
                ObjList.append (ROIDictionary.getROIObject (name))
        SliceDictionaryChangedSinceLastPaint = False
        if self._SliceAxis == "Z" :
            if AxisProjection is not None and AxisProjection.isAxisRotated () :
               projDict = AxisProjection.getROIProjectionSliceAxis (NIfTIVolume, ROIDictionary, Coordinate) 
               NewROISliceDictionary = ROIDictionary.convertAxisProjectiontoROIDictionary (projDict, ObjList= ObjList)
            else:
               NewROISliceDictionary = ROIDictionary.getROISliceDictionary (IncludeDeepGrowAnnotatedSlices = True, ObjList= ObjList)
            dim = float (NIfTIVolume.getSliceDimensions()[2])
            if dim != self._AxisDim :
                self._AxisDim = dim
                SliceDictionaryChangedSinceLastPaint = True
        else:
            xDictionary, yDictionary = ROIDictionary.getROIXYSliceDictionary (NIfTIVolume, ProjectDataset, IncludeDeepGrowAnnotatedSlices = True, ObjList = ObjList) 
            if self._SliceAxis == "X" :
                NewROISliceDictionary = xDictionary
                dim = float (NIfTIVolume.getSliceDimensions()[0])
                if dim != self._AxisDim :
                    self._AxisDim = dim
                    SliceDictionaryChangedSinceLastPaint = True
            else:
                NewROISliceDictionary = yDictionary  
                dim = float (NIfTIVolume.getSliceDimensions()[1])
                if dim != self._AxisDim :
                    self._AxisDim = dim
                    SliceDictionaryChangedSinceLastPaint = True
        
        ROIIDNumDictionary = {}
        for key, ObjectList in NewROISliceDictionary.items ():
            sliceColorList = set () 
            for roiObj in ObjectList :
                sliceColorList.add (roiObj.getROIDefIDNumber ())
            ROIIDNumDictionary[key] = sliceColorList
            
        if not SliceDictionaryChangedSinceLastPaint :
             if tuple (ROIIDNumDictionary.keys ()) != tuple (self._ROISliceDictionary.keys ()) :
                 SliceDictionaryChangedSinceLastPaint = True
             else:
                 for key, value in ROIIDNumDictionary.items () :
                     if tuple (value) != tuple (self._ROISliceDictionary[key]) :
                         SliceDictionaryChangedSinceLastPaint = True
                         break
        if SliceDictionaryChangedSinceLastPaint :
            self._SliceDictionaryChangedSinceLastPaint = True 
            self._ROISliceDictionary = ROIIDNumDictionary
            self.update ()        
    
        
    def DrawAllSlicesRange (self, qp, sliceDictionaryChanged, selectionChanged, hasFocus, selection) :
        TempBuffer = None
        width = self.width  ()
        height = self.height ()        
        if (self._TempBufferShape != (width-2, self._AxisDim, height-2)) :
            Buffer = QtGui.QPixmap (width-2, height-2)  
            self._TempBuffer = Buffer
            self._TempBufferShape = (width-2, self._AxisDim, height-2)  
            Index = np.arange (height-2).astype (np.float)    
            Index /= float (height-3)
            Index[-1] = 1.0
            Index *= float (self._SliceCount-1)
            Index = Index.astype(np.int)
            self._SliceIndexMem = Index
            sliceDictionaryChanged = True
        else:
            Buffer = self._TempBuffer
            Index = self._SliceIndexMem
        
        if sliceDictionaryChanged :
            TempBuffer = QtGui.QPainter (Buffer)
            TempBuffer.setPen (QtGui.QColor (255,255, 255))
            TempBuffer.setBrush (QtGui.QColor (255,255,255))
            TempBuffer.drawRect (0,0, width-3, height-3)               
            if len (self._ROISliceDictionary) > 0 or (self._ColorMask is not None and len (self._ColorMask) > 0) :                      
                for drawI  in range (Index.shape[0]):
                    startDrawI = Index[drawI]
                    if drawI == Index.shape[0] - 1 :
                        nextDrawI = int (self._SliceCount - 1)
                    else:
                        nextDrawI = int (max (Index[drawI+1]-1, startDrawI))
                    colorBand = []
                    
                    for ir in range (startDrawI, nextDrawI+1) :
                        if self._ColorMask is not None  :
                            if ir in self._ColorMask :
                                colorBand += self._ColorMask[ir]                        
                        elif ir in self._ROISliceDictionary :
                            colorBand += self._ROISliceDictionary[ir]                
                    if len (colorBand) > 0 :                    
                        colorBand = set (colorBand)
                        if len (colorBand) > 1 :
                            colorBand = sorted (list (colorBand))                    
                        if self._ColorMask is None  :
                            sliceColorList = VerticalSliceSelectionWidget._getColorList (self._ROIDefs, colorBand, self._colorCache, self._WHITEColorList)                   
                        else:
                            sliceColorList = []
                            for index in colorBand :
                                sliceColorList.append (self._colorTable[index])                                    
                        if len (sliceColorList) > 0 :
                            self.drawColorBox (TempBuffer, sliceColorList,0,drawI, width, 1, DrawBorder = False)   
                
                
                        
                    
                     
            TempBuffer.end ()        
            selectionChanged = True
        
        if selectionChanged :
            startRange = Index < selection[0]
            startRange = startRange.nonzero ()[0]
            if len (startRange) == 0 :        
                minIndex = 0
            else:
                minIndex = np.max (startRange)+1
                
            startRange = selection[-1] < Index
            startRange = startRange.nonzero ()[0]
            if len (startRange) == 0 :        
                maxIndex = Index.shape[0] -1
            else:
                maxIndex = np.min (startRange)            
            self._minSelectedIndex = minIndex
            self._maxSelectedIndex = maxIndex
        else:
            minIndex = self._minSelectedIndex
            maxIndex = self._maxSelectedIndex
            
        qp.drawPixmap (1,1, Buffer) 
        if (hasFocus) :
            qp.setPen ( QtGui.QColor(116,170,215))
            qp.setBrush ( QtGui.QColor(116,170,215,100))
        else:   
            qp.setPen ( QtGui.QColor(178,178,178))                
            qp.setBrush ( QtGui.QColor(178,178,178,100))
        qp.drawRect (1,minIndex, width-3, maxIndex-minIndex+1)   
             
    def  paintEvent (self, event):      
        event.accept ()
        width, height = self.width  (), self.height ()
        paintDim = (width, height)
        newOffScreenBitmap = False
        if self._offscreenBitmapDim != paintDim :
            self.OffScreenBuffer = QtGui.QPixmap (width, height)        
            self._offscreenBitmapDim = paintDim
            newOffScreenBitmap = True 
        
        currentSelection = tuple (self.getSelection ()) 
        selectionChanged = (self._SliceCount > 0 and currentSelection != self._oldSelection)        
        sliceDictionaryChanged = self._SliceDictionaryChangedSinceLastPaint or newOffScreenBitmap        
        if self._sliceView is not None :
            hasfocus = self.hasFocus () or self._sliceView.isSelected ()       
        else:
            hasfocus = self.hasFocus ()            
        if sliceDictionaryChanged or selectionChanged or (self._SliceCount > 0 and self._oldFocus != hasfocus) : 
            self._oldSelection = currentSelection
            self._oldFocus = hasfocus
            qp = QtGui.QPainter (self.OffScreenBuffer)            
            #if len (self._ROISliceDictionary) > 0 or (self._ColorMask is not None and len (self._ColorMask) > 0) :                    
            qp.setPen (QtGui.QColor (0,0, 0))
            qp.setBrush (QtGui.QColor (255,255,255))
            qp.drawRect (0,0, width-1, height-1)   
            self.DrawAllSlicesRange (qp, sliceDictionaryChanged, selectionChanged, hasfocus, currentSelection)
            #else:
            #    qp.setPen (QtGui.QColor (0,0, 0))
            #    qp.setBrush (QtGui.QColor (178,178,178))
            #    qp.drawRect (0,0, width-1, height-1)   
            qp.end ()
            self._SliceDictionaryChangedSinceLastPaint = False
        qp = QtGui.QPainter (self)
        qp.drawPixmap (0,0, self.OffScreenBuffer)
        qp.end ()
        return
        
    def _getSelectedCell (self, event) :
        pos = event.pos ()        
        precent = float (pos.y()) / float (self.height())
        index = int (precent * float (self._SliceCount))
        return max(min (index ,self._SliceCount - 1),0)
        
    
    def mouseMoveEvent (self, event) :    
       if (self._SliceCount > 0) :
            index = self._getSelectedCell (event)
            self.setSelectedSlice (index)      
            #print ("mouse Press Event.update")    
    
    def mouseReleaseEvent(self, event) :
        self._mouseDown = False
        
    def mousePressEvent (self, event) :
        if (self._SliceCount > 0) :
            index = self._getSelectedCell (event)
            self.setSelectedSlice (index)   
            self._mouseDown = True
            #print ("mouse Press Event.update")
   
        
    def resizeEvent (self, event):
        self.update ()
        QtWidgets.QWidget.resizeEvent (self, event)
     
    def keyPressEvent(self, event):
        if (self._SliceCount > 0) :
             if event.key() == QtCore.Qt.Key_Right :
                 self.setSelectedSlice (self._SelectedSlice + 1)
                 return
             elif event.key() == QtCore.Qt.Key_Left :
                 self.setSelectedSlice (self._SelectedSlice - 1)
                 return
             elif event.key() == QtCore.Qt.Key_Up :
                 self.setSelectedSlice (self._SelectedSlice - 1) 
                 return
             elif event.key() == QtCore.Qt.Key_Down :
                 self.setSelectedSlice (self._SelectedSlice + 1)
                 return
             elif self._deleteSliceEnabled and (event.key() == QtCore.Qt.Key_Delete  or event.key () == QtCore.Qt.Key_Backspace) :
                 self._callDeleteSliceListener ()
                 if (not self.isShiftPressed () and len (self._shiftKeySelection) > 0) :
                      self._shiftKeySelection = []                      
                      self.update ()      
                 return       
        self._parent.keyPressEvent (event)      
       
    
    def clearSelection (self) :        
        if (not self.isShiftPressed () and len (self._shiftKeySelection) > 0) :
            self._shiftKeySelection = []            
            self.update ()            
             
    def getSelection (self):
        #self.isShiftPressed()
        if (len (self._shiftKeySelection) == 0):
            return [self._SelectedSlice]
        first = int (min (self._SelectedSlice, self._shiftKeySelection[0]))
        last  = int (max (self._SelectedSlice, self._shiftKeySelection[0]))
        return range (first, last+1) 
    
        
    # method: getColorList (namelist : python list of string) returns a list of colors defined by the ROI names list
    @staticmethod
    def _getColorList (ROIDefs, namelist, colorCache, WHITEColorList) :
         if ROIDefs is None :
             return  [QtGui.QColor (255,255,255, 255)]
         colorList = []      
         for ROIIDNumber in namelist :          
             
             if ROIIDNumber in colorCache :                 
                 color = colorCache[ROIIDNumber]
                 if color is not None :
                     colorList.append (color)
                 continue
                 
             if ROIIDNumber not in colorCache :
                 colorCache[ROIIDNumber] = {}
                 
             ROIName = ROIDefs.getROIIDNumberName (ROIIDNumber)
             if (ROIName is not None and ROIDefs.hasROI (ROIName)): 
                 if ROIDefs.isROIHidden (ROIName) :  
                     colorCache[ROIIDNumber] = None
                 else:                          
                     roiColor = ROIDefs.getROIColor (ROIName)                     
                     colorCache[ROIIDNumber] = roiColor
                     colorList.append (roiColor)                     
             else:
                 colorList.append (QtGui.QColor (0,0,0,255))  
         
         length = len (colorList)
         if length == 0 :
             return WHITEColorList
         else:
             return colorList
             
