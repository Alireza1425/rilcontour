# -*- coding: utf-8 -*-
"""
Created on Fri Sep 23 15:38:26 2016

@author: Philbrick
"""
from PyQt5 import  QtWidgets, QtGui
from six import *



class ColorWidget (QtWidgets.QWidget):
   
   def __init__ (self, parent=None):            
      self.Color = QtGui.QColor (255,0,0)
      QtWidgets.QWidget.__init__ (self, parent)
        
   def  QPainterpaintEvent (self, event):  
     event.accept ()
     qp = QtGui.QPainter ()
     qp.begin (self)
     qp.setPen (QtGui.QColor (0,0, 0))
     qp.setBrush (self.Color)
     qp.drawRect (0,0, self.width ()-1, self.height ()-1)
     qp.end ()
     QtWidgets.QWidget.paintEvent (self, event)
     
       