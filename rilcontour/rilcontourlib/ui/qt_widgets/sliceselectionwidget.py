# -*- coding: utf-8 -*-
"""
Created on Fri Sep 16 12:52:22 2016

@author: Philbrick
"""

from PyQt5 import QtCore, QtGui
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QWidget
from six import *
import math
import copy
import numpy as np
from rilcontourlib.util.rcc_util import FastUnique

class SliceContours :
    def __init__ (self):
        return

    def setSliceNumber (self, slicenumber):
        self.SliceNumber = slicenumber
  
     
    def adjustPositionAndDimensions (self, x, y, size):
        self.x = x
        self.y = y
        self.size = size
                  
    def draw (self, qp, colorlist):        
        numbeofColors = len (colorlist)                
        yC = self.y                 
        xC = self.x
        size = self.size
        if numbeofColors >= size :          
            xE = int (xC + size)
            for i in range (size) :
                color = colorlist[i]
                qp.setBrush (color)
                qp.setPen (color)
                yP = int (i+yC)
                qp.drawLine (xC, yP, xE, yP)  
        else:            
            bottomP = int (yC)                 
            scaleFactor = float (size) / float (numbeofColors)
            bottomPos = bottomP + size -1
            for i in range (numbeofColors) :
                topP    = bottomP
                bottomP = int (yC +scaleFactor * float (i+1))                
                color = colorlist[i]
                qp.setBrush (color)
                qp.setPen (color)                
                if (topP < bottomPos) :
                    qp.drawRect (xC, topP, size, max (bottomP - topP, 1))                    
                                          
        qp.setPen ( QtGui.QColor(0,0,0))
        qp.setBrush ( QtGui.QColor(0,0,0, 0))
        qp.drawRect (xC, yC, size, size)
    
    def drawSelected (self, qp, color, focus) :      
        self.draw (qp, color)  
        qp.setBrush (QtGui.QColor(0,0,0,0))
        if (focus) :
            qp.setPen ( QtGui.QColor(116,170,215))
        else:   
            qp.setPen ( QtGui.QColor(178,178,178))        
        cX = self.x
        cY = self.y
        size = self.size
        qp.drawRect (cX,cY, size, size)   
        qp.drawRect (cX+1,cY+1, size-2, size-2)   
        qp.drawRect (cX+2,cY+2, size-4, size-4)   
        if (focus) :
            qp.setPen ( QtGui.QColor(90,171,238))
        qp.drawRect (cX+3,cY+3, size-6, size-6) 
        
class SliceSelectionWidget (QtWidgets.QWidget):
   
    def _AdjustSliceGrid (self, width, height, slicecount, startsize ) :
        int_gridsize = np.arange (1,startsize)
        gridSize = int_gridsize.astype (np.float)
        hCount = np.floor (float (width) / gridSize)
        vCount = np.floor (float (slicecount) / hCount)        
        hCount = hCount.astype (np.int)
        vCount = vCount.astype (np.int)
        index = (slicecount % hCount) > 0
        vCount[index] += 1                
        index =  np.max(((vCount * int_gridsize) <= height).nonzero ()[0])
        return hCount[index], vCount[index], int_gridsize[index]

            
    def __init__ (self, parent=None):
        QWidget.__init__ (self, parent)
        self._ColorMask = None
        self._WHITEColorList = [QtGui.QColor (255,255,255, 255)]
        self._parent = parent
        self._windowUpdateEnabled = True
        self._pendingWindowUpdate = False
        self._offscreenBitmapDim = None
        self._DrawState = None
        self._DisableComptuerEditedContourVis = False
        self._setDictionaryCount = 0
        
    def setWindowUpdateEnabled (self, stat) :
        if self._windowUpdateEnabled != stat :
            self._windowUpdateEnabled = stat 
            if (stat and self._pendingWindowUpdate) :
                self.update ()                
    
    def update (self,x1 = None, x2 = None, x3 = None, x4 = None) :
        if self._windowUpdateEnabled :
            if (x2 is not None) :
                super(SliceSelectionWidget, self).update (x1, x2, x3, x4)
            elif (x1 is not None) :
                super(SliceSelectionWidget, self).update (x1)
            else:
                super(SliceSelectionWidget, self).update ()
            self._pendingWindowUpdate = False
        else:
            self._pendingWindowUpdate = True
            
    def initWidget (self) :
        self.setFocusPolicy( QtCore.Qt.NoFocus )
        self.SelectedSlice = 0
        self._setDictionaryCount = 0
        self._offscreenBitmapDim = (self.width  (), self.height ())
        self.OffScreenBuffer = QtGui.QPixmap (self.width  (), self.height ())        
        self._sliceListener = []
        self._ROISliceDictionary = {}
        self._deleteSliceCallback = []
        self._shiftKey = False
        self._shiftKeySelection = []
        self._mouseDown = False
        #self.SliceCount = -1
        self._SliceAxis = None
        #self.SlicesContoured = [] #np.empty ((self.SliceCount),dtype=SliceContours)
        self.setSliceCount (0, 'Z') 
        self._DrawState = None
        self._DisableComptuerEditedContourVis = False
        
    def delete (self) :
        self._ColorMask = None
        self.SelectedSlice = 0
        self.OffScreenBuffer = None
        self._sliceListener = []
        self._ROISliceDictionary = {}
        self._deleteSliceCallback = []
        self._shiftKey = False
        self._shiftKeySelection = []        
        self._SliceAxis = None        
        self.SliceCount = 0
        self.SlicesContoured = []
        
    def getSliceAxis (self):
        return self._SliceAxis 
    
    def addSliceListener (self, event) :
         if event not in self._sliceListener :
             self._sliceListener.append (event)
         
    def _callSliceListener (self, cordinate, DisableCallbacks = None) :    
        for i in self._sliceListener :    
            if DisableCallbacks is None or i not in DisableCallbacks :
                i ( cordinate, self._SliceAxis)     
    
    def setROISliceDictionary (self, ROIDictionary, NIfTIVolume = None, AxisProjection = None, Coordinate = None):        
        self._DrawState = None
        self._setDictionaryCount += 1
        if (ROIDictionary is None) :
            self._ROISliceDictionary = {}
            self._ROIDefs = None
            self.update
            return# []
        if AxisProjection is not None and NIfTIVolume is not None and AxisProjection.isAxisRotated () :
           projDict = AxisProjection.getROIProjectionSliceAxis (NIfTIVolume, ROIDictionary, Coordinate)            
           self._ROISliceDictionary = ROIDictionary.convertAxisProjectiontoROIDictionary (projDict)
           self._DisableComptuerEditedContourVis = True
        else:
            self._ROISliceDictionary = ROIDictionary.getROISliceDictionary (IncludeDeepGrowAnnotatedSlices = True)      
            self._DisableComptuerEditedContourVis = False
        self._ROIDefs = ROIDictionary.getROIDefs ()
        self.update ()
        """if (self.SelectedSlice in self._ROISliceDictionary) :
            return  self._ROISliceDictionary[self.SelectedSlice]
        else:
            return []"""
        
        
    def setSliceCount (self,  count, sliceAxis, callback=True) :
        self._SliceAxis = sliceAxis
        self._ROISliceDictionary = {}       
        self.SliceCount = count
        self._DrawState = None
        #self._sliceListener = []        
        self._SliceGridX, self._SliceGridY, self._SliceGridDim = self._AdjustSliceGrid (self.width  (), self.height (), count, 50)          
        x = 0
        y = 0    
        self.SlicesContoured = [] #np.empty ((self.SliceCount),dtype=SliceContours)
        w = self.width  ()        
        for i in range (self.SliceCount) :
            c = SliceContours ()
            c.setSliceNumber (i)            
            c.adjustPositionAndDimensions ( x, y, self._SliceGridDim) 
            self.SlicesContoured.append (c)
            x += self._SliceGridDim
            if x + self._SliceGridDim > w :
                x = 0
                y += self._SliceGridDim          
        self.setSelectedSlice (0, callback)  
        
    def resizeEvent (self, event):
        self._DrawState = None
        if (self.width () > 0 and self.height () > 0) :            
            self._SliceGridX, self._SliceGridYY, self._SliceGridDim = self._AdjustSliceGrid (self.width (), self.height (),  self.SliceCount, 50)    
            x = 0
            y = 0
            width = self.width  ()          
            for c in self.SlicesContoured :
                c.adjustPositionAndDimensions ( x, y, self._SliceGridDim) 
                x += self._SliceGridDim
                if (x + self._SliceGridDim > width):
                    x = 0
                    y += self._SliceGridDim
        else:
            print ("Width and or Height == 0")
        #print ("resizeEvent.update")
        self.update ()
        QtWidgets.QWidget.resizeEvent (self, event)
  
    def getSelectdSlice (self):
        return self.SelectedSlice
    
    @staticmethod
    def _getColorMaskList (sliceNumber, mask, colorTable, ZeroMem = None) :        
        slicedata = FastUnique.unique (mask[:,:,sliceNumber], MaxInt = colorTable.shape[1],ZeroMem=ZeroMem)
        if len (slicedata) <= 1 :
            return  [QtGui.QColor (255,255,255, 255)]
        colorList = []
        color = colorTable[slicedata] 
        maxColor = min (color.shape[0],23)
        for index in range (1,maxColor) :
            colorList.append (QtGui.QColor(color[index,2],color[index,1],color[index,0]))
        return colorList
        
    # method: getColorList (namelist : python list of string) returns a list of colors defined by the ROI names list
    @staticmethod
    def _getColorList (ROIDefs, namelist, colorCache, WHITEColorList) :
         if ROIDefs is None :
             return  [QtGui.QColor (255,255,255, 255)]
         colorList = []      
         for test in namelist :          
             ROIIDNumber, isHumanContoured = test
             
             if ROIIDNumber in colorCache :
                 if isHumanContoured in colorCache[ROIIDNumber] :
                     color = colorCache[ROIIDNumber][isHumanContoured]
                     if color is not None :
                         colorList.append (color)
                     continue
                 
             if ROIIDNumber not in colorCache :
                 colorCache[ROIIDNumber] = {}
                 
             ROIName = ROIDefs.getROIIDNumberName (ROIIDNumber)
             if (ROIName is not None and ROIDefs.hasROI (ROIName)): 
                 if ROIDefs.isROIHidden (ROIName) :  
                     colorCache[ROIIDNumber][isHumanContoured] = None
                 else:                          
                     roiColor = ROIDefs.getROIColor (ROIName)
                     if (isHumanContoured) : # Is Human contoured
                         colorCache[ROIIDNumber][True] = roiColor
                         colorList.append (roiColor)
                     else: #whiten color
                         r, g, b, a = roiColor.getRgb ()
                         precentcolor = 50
                         white = 255 * (100 - precentcolor)
                         r = int ((r * precentcolor + white)/ 100)
                         g = int ((g * precentcolor + white) / 100)
                         b = int ((b * precentcolor + white) / 100)
                         colorTuple = QtGui.QColor (r , g, b, a)
                         colorCache[ROIIDNumber][False] = colorTuple
                         colorList.append (colorTuple)  
             else:
                 colorList.append (QtGui.QColor (0,0,0,255))  
         
         length = len (colorList)
         if length == 0 :
             return WHITEColorList
         else:
             return colorList
             
    
    @staticmethod    
    def GetObjectColorList (index, sliceDictionary, DisableComptuerEditedContourVis = False) :
        if (index not in sliceDictionary):
            return []
        objectList = sliceDictionary [index]
        returnList = []
        if not DisableComptuerEditedContourVis :
            for objects in objectList :
                returnList.append ((objects.getROIDefIDNumber (), objects.doesSliceContainHumanEditedContour (index)))
        else:
            for objects in objectList :
                returnList.append ((objects.getROIDefIDNumber (), True))
        return returnList
    
    def setSliceColorMask (self, colorMask, colorTable, Fast = None) :
        self._DrawState = None
        if Fast is not None :
            ColorMask, colorTable = Fast
            self._ColorMask = {}
            for sliceindex, valuelist in ColorMask.items ():
                colorlist = []
                for colorIndex in valuelist :
                    colorlist.append (QtGui.QColor (colorTable[colorIndex]))
                self._ColorMask[sliceindex] = colorlist
        else:
            if colorMask is None :
                 self._ColorMask = None
            else:
                self._ColorMask = {}
                colorMask = colorMask.getImageData ()
                ZeroMem = np.zeros ((colorTable.shape[1]), dtype = np.bool)
                for sliceindex in range(colorMask.shape[2]) :
                    self._ColorMask[sliceindex] = SliceSelectionWidget._getColorMaskList (sliceindex, colorMask, colorTable, ZeroMem = ZeroMem)
        self.update ()     
        
    def  paintEvent (self, event):      
        event.accept ()
        """eventRec = event.rect ()
        if (eventRec.width () < self.width () or eventRec.height () < self.height ()) :
            #print ("Skipping Paint Event")
            return"""
        #print ("Slices Drawn")               
        hasFocus = self.hasFocus ()
        DrawState = tuple ([hasFocus, self.SelectedSlice, self._setDictionaryCount] + self._shiftKeySelection)
        if self._DrawState != DrawState :
            width = self.width ()
            height = self.height ()    
            paintDim = (width, height)        
            if self._offscreenBitmapDim != paintDim :
                self.OffScreenBuffer = QtGui.QPixmap (width, height)        
                self._offscreenBitmapDim = paintDim                    
            qp = QtGui.QPainter (self.OffScreenBuffer)
            qp.setPen (QtGui.QColor (0,0, 0))
            qp.setBrush (QtGui.QColor (178,178,178))
            qp.drawRect (0,0, width, height)
            colorCache = {}      
            if (self.SliceCount > 0) :
                #print ("ReDrawing Slices")
                for  c in  self.SlicesContoured : 
                    if (self._ColorMask is not None) :
                        c.draw (qp, self._ColorMask [c.SliceNumber])
                    elif (c.SliceNumber in self._ROISliceDictionary) :
                        c.draw (qp, SliceSelectionWidget._getColorList (self._ROIDefs, SliceSelectionWidget.GetObjectColorList (c.SliceNumber, self._ROISliceDictionary, self._DisableComptuerEditedContourVis), colorCache, self._WHITEColorList))
                    else:
                        c.draw (qp,self._WHITEColorList)
                
                sel = self.getSelection ()            
                for index in sel :
                    index = int (index)
                    if (self._ColorMask is not None and index in self._ColorMask) :
                        self.SlicesContoured[index].drawSelected (qp, self._ColorMask [index], hasFocus)
                    elif (index in self._ROISliceDictionary) :
                        try :
                            SelectedName = SliceSelectionWidget.GetObjectColorList (index, self._ROISliceDictionary, self._DisableComptuerEditedContourVis)
                            self.SlicesContoured[index].drawSelected (qp, SliceSelectionWidget._getColorList (self._ROIDefs, SelectedName, colorCache, self._WHITEColorList), hasFocus)
                        except :
                            print ("A error occured drawing selected slice in slice dictionary")
                    else:
                        try :
                            self.SlicesContoured[index].drawSelected (qp,[QtGui.QColor (255,255,255)], hasFocus)
                        except :
                            print ("A error occured drawing selected slice")
                            
                if (hasFocus):                 
                    qp.setPen ( QtGui.QColor(116,170,215))
                    qp.drawRect (0, 0, width, height)   
                    qp.drawRect (1,1, width-2, height-2)   
                    qp.drawRect (2,2, width-4, height-4)   
                    qp.setPen ( QtGui.QColor(90,171,238))
                    qp.drawRect (3,3, width-6, height-6)   
      
                qp.setPen (QtGui.QColor (0,0, 0))
                qp.setBrush (QtGui.QColor (0,0,0,0))
                qp.drawRect (0,0, width-1, height-1)
            qp.end ()
            self._DrawState = DrawState
        qp = QtGui.QPainter (self)
        qp.drawPixmap (0,0, self.OffScreenBuffer)
        qp.end ()
        #QtWidgets.QWidget.paintEvent (self, event)    
        
        
    def isShiftPressed (self):
        #self._mouseDown = (self._mouseDown and QApplication.mouseButtons() == QtCore.Qt.LeftButton)         
        #result =  self._mouseDown or (QtCore.Qt.ShiftModifier & QApplication.queryKeyboardModifiers () == QtCore.Qt.ShiftModifier)        
        result =  (QtCore.Qt.ShiftModifier & QApplication.queryKeyboardModifiers () == QtCore.Qt.ShiftModifier)        
        if (result and not self._shiftKey) :     
                 self._shiftKey = True
                 self._shiftKeySelection = [self.SelectedSlice]                 
        elif (not result and self._shiftKey) :     
            self._shiftKey = False
            self._shiftKeySelection = []
        return self._shiftKey
                 #self._parent.keyPressEvent (event)   
        #return self._shiftKey
    
    @staticmethod
    def listsEqual (list1, list2) :
        if (list1 is  None and list2 is None) :
            return True
        if (list1 is not None and list2 is None) :
            return False
        if (list1 is None and list2 is not None) :
            return False
        if (len (list1) != len (list2)) :
            return False
        if tuple (list1) != tuple (list2) :
            return False        
        return True
        
    def setSelectedSlice (self, index, callback=True, DisableCallbacks = None):
        self._DrawState = None
        if (self.SliceCount > 0) :            
            if index < 0 :
                index = 0 
            elif index > self.SliceCount -1 :
                index = self.SliceCount -1     
                
            oldSelection = copy.copy(self._shiftKeySelection)
            isShiftPressed = self.isShiftPressed () #if shift is pressed sets shift selection
            selectedIndex = int (index)
            if (not isShiftPressed and len (self._shiftKeySelection) > 0):
                self._shiftKeySelection = []
            if (self.SelectedSlice != selectedIndex or not SliceSelectionWidget.listsEqual (oldSelection, self._shiftKeySelection)) :
                self.SelectedSlice = selectedIndex
                self.update () 
                if (callback) :
                    #print ("setSelectedSlice.update")
                    self._callSliceListener (index, DisableCallbacks = DisableCallbacks)
    
    def getSelectionDescription (self) :        
        return (self.SelectedSlice, self._shiftKey, self._shiftKeySelection)
    
    def resetSelectionFromDescription (self, description):
        self._DrawState = None
        selSlice, shiftKey, shiftSel = description
        self.SelectedSlice =  selSlice 
        self._shiftKey = shiftKey
        self._shiftKeySelection =  shiftSel 
        self.update ()           
        self._callSliceListener (selSlice)
    

    def _getSelectedCell (self, event) :
        pos = event.pos ()
        #print (self._SliceGridDim)
        cellX = math.floor (pos.x() / self._SliceGridDim) 
        if cellX > self._SliceGridX - 1 :
            cellX = self._SliceGridX - 1
        cellY = math.floor (pos.y() / self._SliceGridDim)         
        index = cellY * self._SliceGridX + cellX  
        if index >= self.SliceCount :
            index =  self.SliceCount - 1
        return index
    
    def mouseMoveEvent (self, event) :    
       if (self.SliceCount > 0) :
            index = self._getSelectedCell (event)
            self.setSelectedSlice (index)      
            #print ("mouse Press Event.update")
    
    
    def mouseReleaseEvent(self, event) :
        self._mouseDown = False
        
    def mousePressEvent (self, event) :
        if (self.SliceCount > 0) :
            index = self._getSelectedCell (event)
            self.setSelectedSlice (index)   
            self._mouseDown = True
            #print ("mouse Press Event.update")
           
    
    def addDeleteSliceListener (self, callback):
        if callback not in self._deleteSliceCallback :
            self._deleteSliceCallback.append (callback)         
    
    def _callDeleteSliceListener (self):
        for i in self._deleteSliceCallback :
            i (self)
      
           
    def keyPressEvent(self, event):
        if (self.SliceCount > 0) :
             if event.key() == QtCore.Qt.Key_Right :
                 self.setSelectedSlice (self.SelectedSlice + 1)
                 return
             elif event.key() == QtCore.Qt.Key_Left :
                 self.setSelectedSlice (self.SelectedSlice - 1)
                 return
             elif event.key() == QtCore.Qt.Key_Up :
                 self.setSelectedSlice (self.SelectedSlice - self._SliceGridX) 
                 return
             elif event.key() == QtCore.Qt.Key_Down :
                 self.setSelectedSlice (self.SelectedSlice + self._SliceGridX)
                 return
             elif event.key() == QtCore.Qt.Key_Delete  or event.key () == QtCore.Qt.Key_Backspace :
                 self._callDeleteSliceListener ()
                 if (not self.isShiftPressed () and len (self._shiftKeySelection) > 0) :
                      self._shiftKeySelection = []                      
                      self.update ()      
                 return
       
        self._parent.keyPressEvent (event)      
       
    
    def clearSelection (self) :
        self._DrawState = None
        if (not self.isShiftPressed () and len (self._shiftKeySelection) > 0) :
            self._shiftKeySelection = []            
            self.update ()            
             
    def getSelection (self):
        #self.isShiftPressed()
        if (len (self._shiftKeySelection) == 0):
            return [self.SelectedSlice]
        first = int (min (self.SelectedSlice, self._shiftKeySelection[0]))
        last  = int (max (self.SelectedSlice, self._shiftKeySelection[0]))
        return range (first, last+1) 
        
             