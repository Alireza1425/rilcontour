#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 17 13:41:13 2018

@author: KennethPhilbrick
"""

from PyQt5 import QtCore
from PyQt5.QtWidgets import   QWidget, QFileDialog, QDialog, QTableWidgetItem
from rilcontourlib.ui.qt_ui_autogen.rcc_tabtablewidgetAutogen import Ui_TabDataTableWidget
from rilcontourlib.util.rcc_util import ResizeWidgetHelper, MessageBoxUtil
import pandas as pd

class TabDataTableWidget (QWidget) :
    def __init__ (self, parent) :
        QWidget.__init__ (self, parent)
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)        
        self.ui = Ui_TabDataTableWidget ()     
        self.ui.setupUi (self)
        self._resizeWindowHelper = ResizeWidgetHelper ()
        self._resizeWindowHelper.saveWidgetUIPos (self, self.ui.tableWidget)
        self._resizeWindowHelper.saveWidgetUIPos (self, self.ui.ExportTableBtn)
        self.ui.ExportTableBtn.clicked.connect (self.exportTableData) 
        self.ui.CopyTableBtn.clicked.connect (self.copyTable) 
        self._tableData = None
        self.ui.ExportTableBtn.setEnabled (False)
        self.ui.CopyTableBtn.setEnabled (False)
        self._tableName = ""
        
    def setTableData (self, tableName, table, ColumnWidth = None):
        self.ui.tableWidget.clear ()
        self._tableName = tableName
        columnCount = table.getColumnCount ()
        rowCount = table.getRowCount ()
        
        self.ui.tableWidget.setColumnCount ( columnCount)
        self.ui.tableWidget.setRowCount (rowCount)
        self.ui.tableWidget.setHorizontalHeaderLabels (table.getColumnHeader ())
        for cI in range (columnCount) :
            columnData = table.getColumnByRef (cI)
            for rI in range (rowCount) :
                try :
                    flt = float (columnData[rI])
                    wholenum = int (flt)
                    if (flt - wholenum == 0) :
                       cellstr = "%d" % wholenum
                       cellAlignment = QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter
                    else:
                       cellstr = "%0.2f" % flt
                       cellAlignment = QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter
                except:
                    cellstr = str (columnData[rI])
                    cellAlignment = QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter
                tableWidgetItem = QTableWidgetItem (cellstr)
                tableWidgetItem.setTextAlignment(cellAlignment) 
                self.ui.tableWidget.setItem (rI, cI, tableWidgetItem)
        
        if (ColumnWidth is not None) :
            lastColumn = len (ColumnWidth) - 1
            for cI in range (columnCount) :
                if cI <= lastColumn :
                    width = ColumnWidth[cI]
                self.ui.tableWidget.setColumnWidth(cI, width)
        
        self._tableData = table
        self.ui.ExportTableBtn.setEnabled (rowCount > 0)
        self.ui.CopyTableBtn.setEnabled (rowCount > 0)
        
    def getTableName (self) :
        return self._tableName
        
    def copyTable (self) :
         df = self._tableData.convertTableToPandasDataFrame (IncludeHeader = True)
         df.to_clipboard (excel=True,index=False,header=False)
         
    def exportTableData (self) :
        if (self._tableData is not None) :
            dlg= QFileDialog(self)
            dlg.setWindowTitle('Export Table Data')
            dlg.setViewMode( QFileDialog.Detail )    
            dlg.setFileMode (QFileDialog.AnyFile)
            dlg.setNameFilters( [self.tr('excel files (*.xlsx)'),self.tr('comma seperated value (*.csv)')] )
            dlg.setAcceptMode (QFileDialog.AcceptSave)
            if dlg.exec_() == QDialog.Accepted and (len (dlg.selectedFiles())== 1):
                path = dlg.selectedFiles()[0]
                
                if ("xlsx" in dlg.selectedNameFilter ()) :
                    if not path.lower ().endswith (".xlsx") :
                            path += ".xlsx"
                elif not path.lower ().endswith (".csv") :
                        path += ".csv"
                
                df = self._tableData.convertTableToPandasDataFrame (IncludeHeader = True)
                if (path.endswith (".csv")) :
                    try:
                        df.to_csv (path,index=False,header=False)
                    except:
                        MessageBoxUtil.showMessage ("Export Table Data","A error occured trying to export the table dataset as an csv data file.")   
                else:                     
                    try:
                        df.to_excel (path ,index=False,header=False)                        
                    except:
                        MessageBoxUtil.showMessage ("Export Table Data","A error occured trying to export the table dataset as an excel data file.")   
        
    def _internalWindowResize (self, size) :
        self._resizeWindowHelper.setAbsoluteWidth  (self.ui.tableWidget, size)
        self._resizeWindowHelper.setAbsoluteHeight (self.ui.tableWidget, size)
        self._resizeWindowHelper.setWidgetXPos (self.ui.ExportTableBtn, self.width () - 111)
        self._resizeWindowHelper.setWidgetYPos (self.ui.ExportTableBtn, self.height () - 44)
        self._resizeWindowHelper.setWidgetXPos (self.ui.CopyTableBtn, self.width () - 231)
        self._resizeWindowHelper.setWidgetYPos (self.ui.CopyTableBtn, self.height () - 44)
       
        
    def resizeEvent (self, qresizeEvent) :                       
        QWidget.resizeEvent (self, qresizeEvent)         
        self._internalWindowResize ( qresizeEvent.size () )   