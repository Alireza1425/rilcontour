#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 11 14:57:53 2018

@author: m160897
"""

import os
import shutil
import copy
import numpy as np
from PyQt5 import QtCore
from PyQt5.QtWidgets import  QDialog, QFileDialog
from rilcontourlib.util.rcc_util import ResizeWidgetHelper
from rilcontourlib.ui.qt_ui_autogen.RCC_ModelManagerDlgAutogen import Ui_ML_ModelManagerDialog
from rilcontourlib.ui.qt_ui_autogen.RCC_TreeNodeTxtEditDlgAutogen import Ui_ML_TreeNodeNameDlg
from rilcontourlib.ui.qt_ui_autogen.RCC_Duplicate_ModelQuestionAutogen import Ui_Duplicate_Model_QuestionDlg
from rilcontourlib.ui.RCC_ML_ModelTree import ML_ModelTree, ML_ModelTreeNode, ML_ModelFileIO
from rilcontourlib.ui.RCC_ML_ModelDefDlg import RCC_ModelDefDlg
from rilcontourlib.ui.RCC_ML_ModelImportDlg import RCC_ModelImportDlg
from rilcontourlib.ui.RCC_ML_AutoImportModel_Dlg import  RCC_AutoImportModelDlg
from rilcontourlib.machinelearning.ML_AutoImportMLModel import ML_AutoImportMLModel
from rilcontourlib.util.FileUtil import ScanDir

class RCC_DuplicateModelQuestion (QDialog) :
    def __init__ (self,  parent) :                
        self._parent = parent  
        self._result = "cancel"
        QDialog.__init__ (self, parent)
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)
        self.ui = Ui_Duplicate_Model_QuestionDlg ()        
        self.ui.setupUi (self)                        
        self.ui.modelBtn.clicked.connect (self.modelBtn) 
        self.ui.versionBtn.clicked.connect (self.versionBtn)
        self.ui.cancelBtn.clicked.connect (self.cancelBtn)         
        
    def modelBtn (self) :
        self._result = "model"
        self.accept ()
        
    def versionBtn (self) :
        self._result = "version"
        self.accept ()
        
    def cancelBtn (self) :        
        self.reject ()

    def getResult (self) :
        return self._result
    


class RCC_ModeManagerTxtEdit (QDialog) :
    def __init__ (self, txt, parent) :                
        self._parent = parent        
        QDialog.__init__ (self, parent)
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)
        self.ui = Ui_ML_TreeNodeNameDlg ()        
        self.ui.setupUi (self)                
        self._resizeWidgetHelper = ResizeWidgetHelper ()
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.CancelBtn)        
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.OkBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.lineEditTxt)
        self.ui.CancelBtn.clicked.connect (self.reject) 
        self.ui.OkBtn.clicked.connect (self.okBtn) 
        self.ui.lineEditTxt.setText (txt)
        
    def okBtn (self) :
        self._text = self.ui.lineEditTxt.text ()
        self.accept ()
    
    def getText (self) :
        self._text = self._text.replace (":","")
        return self._text.strip ()
    
    def _internalWindowResize (self, newsize):                            
        if (self._resizeWidgetHelper is not None) :                                               
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.lineEditTxt, newsize)            
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.CancelBtn, newsize.width () - 208)                                            
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.OkBtn, newsize.width () - 108)                                
                                       
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)                 
        self._internalWindowResize ( qresizeEvent.size () )    

class RCC_ModelManagerDlg (QDialog) :
    
    def __init__ (self, ProjectDataset, parent) :                
        self._parent = parent
        self._ProjectDataset = ProjectDataset
        QDialog.__init__ (self, parent)
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)
        self.ui = Ui_ML_ModelManagerDialog ()        
        self.ui.setupUi (self)                
        
        self._NiftiVolume = parent.NiftiVolume 
        self._ROIDictionary = parent.ROIDictionary
        self._selectedSlice = parent.SelectedCoordinate.getZCoordinate ()            
        
        self._resizeWidgetHelper = ResizeWidgetHelper ()
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.CloseBtn)        
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.createTreeNodeBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.modelLine)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.removeModelBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.importModelBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.exportModelBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.editModelBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.duplicateModelBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.newModelBtn)     
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.autoImportButton)
          
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.TreeModelSplitter)
                
        self.ui.newModelBtn.clicked.connect (self._newBtn)
        self.ui.duplicateModelBtn.clicked.connect (self._duplicateBtn)        
        self.ui.editModelBtn.clicked.connect (self._editBtn)        
        self.ui.exportModelBtn.clicked.connect (self._exportBtn)        
        self.ui.importModelBtn.clicked.connect (self._importBtn)        
        self.ui.removeModelBtn.clicked.connect (self._removeBtn)
        self.ui.createTreeNodeBtn.clicked.connect (self._createNodeBtn)
        self.ui.CloseBtn.clicked.connect (self._CloseBtn)
        self._coreModel = ML_ModelTree (self._ProjectDataset, self)
        self._coreModel.addRootNode (ML_ModelTreeNode ("Classification",self._coreModel ))
        self._coreModel.addRootNode (ML_ModelTreeNode ("Segmentation",self._coreModel ))
        self._coreModel.setClearSelectionCallback (self._clearSelection)
        ML_ModelTree.loadModelTreeFromPath (self._ProjectDataset.getMLHomeFilePath () , ExistingModelTreeToAddTo = self._coreModel, ProjectDataset=self._ProjectDataset)        
        
        autoImportMLModels = ML_AutoImportMLModel (ProjectDataset)
        autoImportMLModels.runNow (self._coreModel, self)
        
        self.ui.ModelTreeView.setModel (self._coreModel)
        
        self.ui.ModelTreeView.setDragEnabled(True)
        self.ui.ModelTreeView.setAcceptDrops(True)
        self.ui.ModelTreeView.setDropIndicatorShown(True)
        
        selectionModel = self.ui.ModelTreeView.selectionModel ()
        selectionModel.selectionChanged.connect (self._TreeSelectionChanged)         
        self.ui.ModelTreeView.mouseDoubleClickEvent = self._TreeNodeEdit
        enabled = False        
        self.ui.newModelBtn.setEnabled (True)
        self.ui.duplicateModelBtn.setEnabled (enabled)
        self.ui.editModelBtn.setEnabled (enabled)
        self.ui.exportModelBtn.setEnabled (enabled)
        self.ui.importModelBtn.setEnabled (True)   
        self.ui.removeModelBtn.setEnabled (enabled)
        self.ui.createTreeNodeBtn.setEnabled (enabled)
        self.ui.ModelTxtSummary.setEnabled (False)
        self.ui.autoImportButton.setEnabled (True)
        self.ui.autoImportButton.clicked.connect (self._showAutoImportDlg)
    
    def _showAutoImportDlg (self) :
        dlg = RCC_AutoImportModelDlg (self._ProjectDataset, self._coreModel, self)
        dlg.exec_ ()
        
        
    def _TreeSelectionChanged  (self, itemSelection = None, itemDeselected = None) :           
        if (itemSelection is not None) :
            selectionList = itemSelection.indexes ()
            if len (selectionList) <= 0 :
                node = None
            else:
                node = selectionList[0].internalPointer ()
        else:
            node = self.getSelectedTreeNode ()
            
        enabled = node is not None         
        self.ui.duplicateModelBtn.setEnabled (enabled and node.isMLModel ())
        self.ui.editModelBtn.setEnabled (enabled and node.getParent () is not None)
        self.ui.exportModelBtn.setEnabled (enabled and node.isMLModel ())                
        self.ui.removeModelBtn.setEnabled (enabled and node.isNodeRemovable () and node.getParent () is not None)
        self.ui.createTreeNodeBtn.setEnabled (enabled and not node.isVersionNode ())
        if (node is None or not node.isMLModel ()) :
            self.ui.ModelTxtSummary.setText ("")
            self.ui.ModelTxtSummary.setEnabled (False)
        else:
            self.ui.ModelTxtSummary.setEnabled (True)
            mlModel = node.getMLModel ()
            self.ui.ModelTxtSummary.setHtml (mlModel.getHtmlSummary ())
        
    def _TreeNodeEdit (self, event) :
         node = self.getSelectedTreeNode ()
         if (node is not None and node.getParent () is not None) :
               self.editTreeModelNode (node)
               event.accept()
             
    def _newBtn (self) :
        node = self.getSelectedTreeNode ()
        self.newTreeModel (node)       
                        
    def _duplicateBtn (self) :
        node = self.getSelectedTreeNode ()
        if node is not None and node.getParent () is not None:            
            self.duplicateTreeModel (node)
                
    def _editBtn (self) :
        node = self.getSelectedTreeNode ()
        if node is not None and node.getParent () is not None :
            self.editTreeModelNode (node)
    
    def _getNewHDF5Path (self, basepath, count) :
        if count == 0 :
            return basepath
        testPath = basepath[:-len (".hdf5")] + "_" + str (count + 1) + ".hdf5"
        return testPath
        
        
    def _exportBtn (self) : 
        node = self.getSelectedTreeNode ()
        if node is not None :
            if node.isMLModel () :
                dlg= QFileDialog( self )
                dlg.setWindowTitle('Export ML Model')
                dlg.setViewMode( QFileDialog.Detail )    
                dlg.setFileMode (QFileDialog.AnyFile)
                dlg.setNameFilters( [self.tr('HDF5 (*.hdf5)'), self.tr('All Files (*)')] )
                dlg.setDefaultSuffix( 'hdf5' )
                dlg.setAcceptMode (QFileDialog.AcceptSave)
                if dlg.exec_() and (len (dlg.selectedFiles())== 1):
                    if (node.isVersionNode ()) :
                        versionNodeLst = [node]
                    else:
                        versionNodeLst = copy.copy(node.getModelVersions ())
                    basehdf5Path = dlg.selectedFiles()[0]    
                    if (not basehdf5Path.endswith (".hdf5")) :
                        basehdf5Path += ".hdf5"
                    for count, versionNode in enumerate(versionNodeLst) :
                        try :                    
                            hdf5Path = self._getNewHDF5Path (basehdf5Path, count)                             
                            pathlst = hdf5Path.split (".")
                            pathlst[-1] = "model"
                            modelpath = ".".join (pathlst)
            
                            mlExportModel = versionNode.getMLModel (ForceAllocation = True)                            
                            current_hdf5path = mlExportModel.getCurrentKerasModelHDF5Path ()
                            mlExportModel.setKerasModelHDF5Path (hdf5Path)
                            if os.path.exists (current_hdf5path) :                                                    
                                ML_ModelFileIO.saveModelDef (mlExportModel, modelpath)
                                shutil.copyfile (current_hdf5path, hdf5Path)
                        except:
                            try:
                                os.path.remove (modelpath)
                            except:
                                pass
                            try:
                                os.path.remove (hdf5Path)
                            except:
                                pass
        
    def _importBtn (self) :
        dlg= QFileDialog( self )
        dlg.setWindowTitle('Select directory to import ML model from')
        dlg.setViewMode( QFileDialog.Detail )    
        dlg.setFileMode (QFileDialog.Directory)
        dlg.setNameFilters( [self.tr('All Files (*)'), self.tr('HDF5 (*.hdf5)'), self.tr('model (*.model)')] )
        dlg.setAcceptMode (QFileDialog.AcceptOpen)
        if dlg.exec_() and (len (dlg.selectedFiles())== 1):
            path = dlg.selectedFiles()[0]
            if os.path.exists (path) and os.path.isdir (path) :
                importDlg = RCC_ModelImportDlg (path, self)
                importDlg.exec_ () 
                if importDlg.wasModelImported () :
                    self._coreModel = ML_ModelTree (self._ProjectDataset, self)
                    self._coreModel.addRootNode (ML_ModelTreeNode ("Classification",self._coreModel ))
                    self._coreModel.addRootNode (ML_ModelTreeNode ("Segmentation",self._coreModel ))
                    self._coreModel.setClearSelectionCallback (self._clearSelection)
                    ML_ModelTree.loadModelTreeFromPath (self._ProjectDataset.getMLHomeFilePath () , ExistingModelTreeToAddTo = self._coreModel, ProjectDataset=self._ProjectDataset)        
                    self.ui.ModelTreeView.setModel (self._coreModel)
                    self.ui.ModelTreeView.setDragEnabled(True)
                    self.ui.ModelTreeView.setAcceptDrops(True)
                    self.ui.ModelTreeView.setDropIndicatorShown(True)
                    selectionModel = self.ui.ModelTreeView.selectionModel ()
                    selectionModel.selectionChanged.connect (self._TreeSelectionChanged) 
    
    
    def _removeBtn (self) :
        node = self.getSelectedTreeNode ()
        if node is not None and node.getParent () is not None :
            wasremoved, modelremoved = self._removeNodeFromTree (node)
            if (wasremoved and (modelremoved is not None)) :
                RCC_ModelManagerDlg._RemoveModelFromFileSystem (modelremoved, ML_HomePath = self._ProjectDataset.getMLHomeFilePath ())
                selectionModel = self.ui.ModelTreeView.selectionModel ()
                selectionModel.clear ()
            
    def _createNodeBtn (self) :
        node = self.getSelectedTreeNode ()        
        if node is None :
            return
        newTextNode = None
        if  not node.isMLModel () :
            newTextNode = self.create_or_editSpacerTreeNode ()
            if newTextNode is not None and not node.hasChildNamed (newTextNode.getText ()) :
                node.addChild (newTextNode)
                self._expandParent (newTextNode)
            else:
                newTextNode = None
        else:
            parent = node.getParent ()
            if node.isVersionNode () :
                node = parent
                parent = node.getParent ()
            if parent is not None :
                newTextNode = self.create_or_editSpacerTreeNode ()
                if (newTextNode is not None and not parent.hasChildNamed (newTextNode.getText ())) :
                    parent.removeChild (node)
                    parent.addChild (newTextNode)
                    newTextNode.addChild (node)
                    node.updateTreePath ()
                    self._expandParent (newTextNode)
                    self._expandParent (node)
                else:
                    newTextNode = None
        if (newTextNode is not None):
            treemodel = self.ui.ModelTreeView.model ()
            selectionModel = self.ui.ModelTreeView.selectionModel ()
            row = newTextNode.getRow ()
            qindex = treemodel.createIndex(row, 0, newTextNode)        
            selectionModel.select (qindex, QtCore.QItemSelectionModel.ClearAndSelect | QtCore.QItemSelectionModel.Current)
            selectionModel.setCurrentIndex (qindex, QtCore.QItemSelectionModel.ClearAndSelect | QtCore.QItemSelectionModel.Current)
            #self._TreeSelectionChanged ()
                        
    def getSelectedTreeNode (self)    :
        selection = self.ui.ModelTreeView.selectionModel ()
        item = selection.currentIndex()                 
        treeNode = item.internalPointer ()                               
        return treeNode
        

    def _getNewModelFileName (self) :
        ml_homePath = self._ProjectDataset.getMLHomeFilePath ()        
        fileNumberLst = []
        try:
            fileIterator = ScanDir.scandir(ml_homePath)       
            for fobj in fileIterator :
                fLowerName = fobj.name.lower()        
                if fLowerName.startswith ("model_") and fLowerName.endswith (".model") :
                    try :
                        numstr = fobj.name[len ("Model_") :-len (".model")]
                        num = int (numstr)
                        fileNumberLst.append (num)
                    except :
                        pass 
        finally:
            try:
                fileIterator.close ()
            except:
                pass
        numbers = np.array (sorted (fileNumberLst), dtype=np.int)
        if 0 not in numbers :
            return os.path.join (ml_homePath, "Model_0.model")
        elif 1 not in numbers :
            return os.path.join (ml_homePath, "Model_1.model")
        else:        
            firstset = numbers[0:-1]  
            index = (firstset - numbers[1:]) > 1 
            lst = firstset[index]
            if lst.shape[0] > 0 :
                return  os.path.join (ml_homePath, "Model_"+str (lst[0] + 1)+".model")
            else:
                return  os.path.join (ml_homePath, "Model_"+str (numbers[numbers.shape[0]-1] + 1)+".model")
                
    def create_or_editSpacerTreeNode (self, node = None) :
        if node is None :
            txtEditDlg = RCC_ModeManagerTxtEdit ("", self)
        else:
            txtEditDlg = RCC_ModeManagerTxtEdit (node.getText (), self)
        if (txtEditDlg.exec_ () == QDialog.Accepted) :
            txt = txtEditDlg.getText ()
            if len (txt) > 0 :
                if node is not None :
                    if not node.getParent ().hasChildNamed (txt) :
                        node.setText (txt)  
                    return node
                else:
                    treemodel = self.ui.ModelTreeView.model ()
                    return ML_ModelTreeNode (nodeTextName = txt, treeModel = treemodel)
        return None
    
    def _clearSelection (self) :
        selectionModel = self.ui.ModelTreeView.selectionModel ()
        selectionModel.clear ()
       
           
    def _removeNodeFromTree (self, treenode) :               
       treemodel = self.ui.ModelTreeView.model ()
       #self.ui.ModelTreeView.selectionModel ().reset ()
       #row = treenode.getRow ()
       #qindex = treemodel.createIndex(row, 0, treenode)
       """selectionModel = self.ui.ModelTreeView.selectionModel ()
       qindex = selectionModel.currentIndex()  
       if (qindex is not None) :
           selectionModel.select (qindex, QtCore.QItemSelectionModel.Clear)"""
                         
       wasModelRemoved, modelRemoved = treemodel.removeTreeNode (treenode)
       #self._TreeSelectionChanged ()           
       return wasModelRemoved, modelRemoved
    
    def _expandParent (self, node) :
        if (node is not None) :
           parentNode = node.getParent ()
           if parentNode is not None :
                row = parentNode.getRow ()
                treemodel = self.ui.ModelTreeView.model ()
                parentIndex = treemodel.createIndex(row, 0, parentNode)     
                if not self.ui.ModelTreeView.isExpanded (parentIndex) :
                    self.ui.ModelTreeView.setExpanded (parentIndex,True) 
                    
                    
    def _addModelToTree (self, model, UpdateSelection = True) :                
        treemodel = self.ui.ModelTreeView.model ()
        treenode = ML_ModelTree.createTreeNode (treemodel, Model = model, HandleDuplicateVersion="Append", ProjectDataset=self._ProjectDataset)
        selectionModel = self.ui.ModelTreeView.selectionModel ()
        if (treenode is None) :
            selectionModel.reset ()
        else:
            if (treenode.isVersionNode ()) :
                treenode.unloadVersionModels ()
                treenode.getParent ().unloadVersionModels ()
            else:
                treenode.unloadVersionModels ()
            if (treenode.isVersionNode () and 1 == len (treenode.getParent ().getModelVersions())) :
                treenode = treenode.getParent ()
            else:
                self._expandParent (treenode)
            if (UpdateSelection) :
                selectionModel = self.ui.ModelTreeView.selectionModel ()
                selectionModel.clear ()
                row = treenode.getRow ()
                qindex = treemodel.createIndex(row, 0, treenode)                        
                selectionModel.select (qindex, QtCore.QItemSelectionModel.ClearAndSelect | QtCore.QItemSelectionModel.Current)         
                selectionModel.setCurrentIndex (qindex, QtCore.QItemSelectionModel.ClearAndSelect | QtCore.QItemSelectionModel.Current)         
                
                #self._TreeSelectionChanged ()
        return treenode
    
    def _AddModelToFileSystem (self,model) :        
        try :
            filePath = self._getNewModelFileName ()                    
        except:
            filePath = None
                    
        if (filePath is not None) :                
            hdf5modelPath = None
            try :
                hdf5modelPath = filePath.split (".")
                hdf5modelPath[-1] = "hdf5"
                hdf5modelPath = ".".join (hdf5modelPath)                
                currentKerasPath = model.getCurrentKerasModelHDF5Path ()
                shutil.copyfile (currentKerasPath, hdf5modelPath)                    
            except:                    
                try:
                   os.path.remove (hdf5modelPath)
                except:
                   hdf5modelPath = None
            
            if (hdf5modelPath is not None) :
                try :
                    model.setKerasModelHDF5Path (hdf5modelPath)
                    ML_ModelFileIO.saveModelDef (model, filePath) 
                except:                    
                    try:
                        os.path.remove (hdf5modelPath)
                    except:
                        hdf5modelPath = None
                    try:
                        os.path.remove (filePath)
                    except:
                        filePath = None
            if (hdf5modelPath is not None and filePath is not None) :                        
                return True
        return False

    @staticmethod    
    def _saveModelDescriptionFile (model) :
        try :
            hdf5modelPath = model.getCurrentKerasModelHDF5Path ()
            modelPath = hdf5modelPath.split (".")
            modelPath[-1] = "model"
            modelPath = ".".join (modelPath)            
            ML_ModelFileIO.saveModelDef (model, modelPath)            
            return True
        except:
            return False
        
    @staticmethod 
    def _RemoveModelFromFileSystem (model, ML_HomePath = None) : 
        hdf5modelPath = model.getCurrentKerasModelHDF5Path ()
        modelDefPath = hdf5modelPath.split (".")
        linear_hdf5 = copy.copy (modelDefPath)
        modelDefPath[-1] = "model"
        modelDefPath = ".".join (modelDefPath)
        linear_hdf5[-1] = "lhdf5"
        linear_hdf5 = ".".join (linear_hdf5)
        if os.path.exists (linear_hdf5) and (ML_HomePath is None or linear_hdf5.startswith (ML_HomePath)):
            try : 
                os.remove (linear_hdf5) 
            except:
                print ("A error occured trying to remove: " + linear_hdf5)
        if os.path.exists (hdf5modelPath) and (ML_HomePath is None or hdf5modelPath.startswith (ML_HomePath)):
            try : 
                os.remove (hdf5modelPath) 
            except:
                print ("A error occured trying to remove: " + hdf5modelPath)
        if os.path.exists (modelDefPath) and (ML_HomePath is None or modelDefPath.startswith (ML_HomePath)):
            try : 
                os.remove (modelDefPath) 
            except:
                print ("A error occured trying to remove: " + modelDefPath)
            
    def _removeTempLinearModel (self, path) :
        try :
            hdf5modelPath = path.split (".")
            hdf5modelPath[-1] = "lhdf5"
            linearModel = ".".join (hdf5modelPath)
            ML_HomePath = self._ProjectDataset.getMLHomeFilePath ()
            if os.path.exists (linearModel) and (ML_HomePath is None or linearModel.startswith (ML_HomePath)):
                os.remove (linearModel)
        except:
            pass
        
    def editTreeModelNode (self, treeNode) :
         if (not treeNode.isMLModel ()) :
             self.create_or_editSpacerTreeNode (node = treeNode)
         else:            
            model = treeNode.getMLModel ()
            oldHDF5Path = model.getCurrentKerasModelHDF5Path ()
            dlg = RCC_ModelDefDlg (self._ProjectDataset, self._NiftiVolume, self._selectedSlice, self._ROIDictionary,  self._parent , self) 
            dlg.initFromModelDefinition (model)
            if (dlg.exec_() == QDialog.Accepted):
                newModelDefinition = dlg.getModelDefinition ()                
                treemodel = self.ui.ModelTreeView.model ()
                if not treeNode.isVersionNode () :
                    selectParentNode = treeNode
                    treeNode = treeNode.getMostRecentModelVersion ()
                else:
                    selectParentNode = None
                if not treemodel.doesModelDefinitionOrTreeNodeExist (newModelDefinition, IgnoreNode = treeNode) :
                    
                    if (selectParentNode is not None) :
                        versionNodeLst = copy.copy(selectParentNode.getModelVersions ())
                    else:
                        versionNodeLst = [treeNode]
                    
                    newModelName = newModelDefinition.getModelName ()
                    for treeVersionNode in versionNodeLst :
                        if treeNode == treeVersionNode :
                            UpdateSelection = True
                            UpdateModelDefinition = newModelDefinition
                        else:
                            UpdateSelection = False
                            tempModel = treeVersionNode.getMLModel ()
                            if tempModel.getModelName () ==  newModelName :           
                                UpdateModelDefinition = None
                            else:
                                tempModel.setModelName (newModelName)
                                UpdateModelDefinition = tempModel
                                
                        if (UpdateModelDefinition is not None) :    
                            self._removeNodeFromTree (treeVersionNode)
                            newHDF5Path = UpdateModelDefinition.getCurrentKerasModelHDF5Path ()
                            self._removeTempLinearModel (newHDF5Path)
                            if (newHDF5Path == oldHDF5Path or not UpdateSelection) :  # if HDF5 path not changed or if just updating model name          
                                if (RCC_ModelManagerDlg._saveModelDescriptionFile (UpdateModelDefinition)) :
                                    treeVersionNode = self._addModelToTree (UpdateModelDefinition, UpdateSelection = UpdateSelection)                                       
                            else:
                                RCC_ModelManagerDlg._RemoveModelFromFileSystem (model, ML_HomePath = self._ProjectDataset.getMLHomeFilePath ())
                                if (self._AddModelToFileSystem (UpdateModelDefinition)) :
                                    treeVersionNode = self._addModelToTree (UpdateModelDefinition, UpdateSelection = UpdateSelection)                                    
                                    if (treeVersionNode is None) :
                                         RCC_ModelManagerDlg._RemoveModelFromFileSystem (UpdateModelDefinition, ML_HomePath = self._ProjectDataset.getMLHomeFilePath ())
                            if (UpdateSelection and selectParentNode is not None) :
                                if (treeVersionNode is not None) :
                                    if (treeVersionNode.isVersionNode ()) :
                                       selectParentNode = treeVersionNode.getParent ()
                                    else:
                                       selectParentNode = treeVersionNode
                                else:
                                    selectParentNode = None
                            elif (UpdateSelection) :
                                selectParentNode = treeVersionNode
                    if (selectParentNode is not None):                         
                         row = selectParentNode.getRow ()
                         qindex = treemodel.createIndex(row, 0, selectParentNode)
                         selectionModel = self.ui.ModelTreeView.selectionModel ()
                         selectionModel.clear ()
                         selectionModel.select (qindex, QtCore.QItemSelectionModel.ClearAndSelect | QtCore.QItemSelectionModel.Current)
                         selectionModel.setCurrentIndex (qindex, QtCore.QItemSelectionModel.ClearAndSelect | QtCore.QItemSelectionModel.Current)
                         #self._TreeSelectionChanged ()
                              

    def _getNewModelorVersion (self) :
        dialog = RCC_DuplicateModelQuestion (self)
        dialog.exec_ ()
        return dialog.getResult ()
    
    def duplicateTreeModel (self, treeNode) :                            
        if (not treeNode.isMLModel ()) :
            newModelName = treeNode.getNewModelName ()
            path_list = copy.copy (treeNode.getTreePath ())
            path_list[len (path_list) -1] = newModelName
            treemodel = self.ui.ModelTreeView.model ()
            treemodel.addNonModelPath (path_list)                                
        else:
            newVersionName = ""
            newModelName = ""
            selectedVersion = None        
            model = treeNode.getMLModel ()
            if treeNode.isVersionNode () :                           
                newVersionName = treeNode.getNewVersionName ()
                selectedVersion = treeNode
            else:
                selectedVersion = treeNode.getMostRecentModelVersion ()
                result =  self._getNewModelorVersion ()
                if (result == "version") :
                    newVersionName = selectedVersion.getNewVersionName ()
                elif (result=="model") :
                    newModelName = treeNode.getNewModelName ()
                else:
                    return                        
            model = selectedVersion.getMLModel (ForceAllocation = True)
            model.setModelAutoImported (None)
            if newVersionName != "" :
                log = model.getModelDescriptionLog ()         
                model.setModelDescriptionLog (self._ProjectDataset.getUserName (), newVersionName, log.getDescription())
                

            elif newModelName != "" :
                model.setModelName (newModelName)
            if (self._AddModelToFileSystem (model)) :
                self._addModelToTree (model)                                
                
    def newTreeModel (self, parentNode) :        
        if parentNode is not None and parentNode.isVersionNode () :
            parentNode = parentNode.getParent ()            
        dlg = RCC_ModelDefDlg (self._ProjectDataset, self._NiftiVolume, self._selectedSlice, self._ROIDictionary,  self._parent , self) 
        if (parentNode is not None) :
            if (parentNode.isMLModel ()) :
                if parentNode.isVersionNode () :
                    dlg.setTreePath (parentNode.getParent().getParent ().getTreePath ())
                    dlg.setModelName (parentNode.getParent().getText ())
                else:
                    dlg.setTreePath (parentNode.getParent().getTreePath ())
                    dlg.setModelName (parentNode.getText ())
            else:
                dlg.setTreePath (parentNode.getTreePath ())
        else:
            dlg.setTreePath (["Classification"])
        if dlg.exec_ () == QDialog.Accepted :
            modelDefinition = dlg.getModelDefinition ()
            
            treemodel = self.ui.ModelTreeView.model ()
            if not treemodel.doesModelDefinitionOrTreeNodeExist (modelDefinition) :                
                if (self._AddModelToFileSystem (modelDefinition)) :                
                    if (self._addModelToTree (modelDefinition) is None) :   
                        RCC_ModelManagerDlg._RemoveModelFromFileSystem (modelDefinition, ML_HomePath = self._ProjectDataset.getMLHomeFilePath ())
                                              
            
    def _internalWindowResize (self, newsize):                            
        if (self._resizeWidgetHelper is not None) :                                               
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.TreeModelSplitter, newsize)            
            self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.TreeModelSplitter, newsize)                                                            
            
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.newModelBtn, newsize.width () - 141)                                    
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.duplicateModelBtn, newsize.width () - 141)                                    
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.editModelBtn, newsize.width () - 141)                    
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.exportModelBtn, newsize.width () - 141)                                
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.importModelBtn, newsize.width () - 141)                                
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.removeModelBtn, newsize.width () - 141)
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.autoImportButton, newsize.width () - 141)
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.modelLine, newsize.width () - 141)                                
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.createTreeNodeBtn, newsize.width () - 141)                                            
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.CloseBtn, newsize.width () - 141)                                
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.CloseBtn, newsize.height () - 36)                                
                                       
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)                 
        self._internalWindowResize ( qresizeEvent.size () )               
        
                
       
    def _CloseBtn (self) :        
        self.close ()