#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 30 16:46:42 2018

@author: m160897
"""

import pandas as pd
import os
from PyQt5.QtCore import QModelIndex
from PyQt5 import QtCore, QtGui
import copy


class rcc_radlex:
    
    def __init__ (self, homePath, basePythonPath, ProjectDataset = None, RadLexDictionary = None) :    
        self._TemplateType = "Error Template"
        self._homePath = homePath
        self._basePythonPath = basePythonPath
        try:
            if (RadLexDictionary is not None) :
                self._RadLexDictionary = RadLexDictionary                
            else:                
                self._RadLexDictionary = {}
                try:                    
                    homepath = homePath 
                    if (os.path.isdir (homepath)) :
                        templatePath = os.path.join (homepath,"CustomRadlexTemplate.xlsx")
                        if (os.path.isfile (templatePath)) :
                            self._LoadRadLexDictionary (templatePath)                                                
                            self._TemplateType = "Custom"
                except:
                    print ("A error occured loading custom radlex template")
                    self._RadLexDictionary = {}
                                
                try :
                    if len (self._RadLexDictionary) <= 0 :                                        
                        basepath = basePythonPath
                        rilContourLibPath = os.path.join (basepath, "rilcontourlib")
                        radlexPath = os.path.join (rilContourLibPath, "radlex")
                        radlexPath = os.path.join (radlexPath, "RilcontourSimplifiedRadlex.xlsx")
                        self._LoadRadLexDictionary (radlexPath)                                                
                        self._TemplateType = "Default"
                except:
                    print ("A error occured loading the default radlex template")
                    self._RadLexDictionary = {}                    
                    raise
        except:
            print ("Intializing RadLex Failed.") 
            self._TemplateType = "Error Loading"
        self._validateTree ()
    
    def getTemplateType (self):
        return self._TemplateType
    
    def _LoadRadLexDictionary (self, filepath):
                print ("Trying to load radlex template from: " + filepath)        
                self._RadLexDictionary = {}
                self._radlexPath = filepath
                data = pd.read_excel (self._radlexPath)
                rows, columns = data.shape
                for rowIndex in range (rows) :
                    name, rid, pid = data.iloc[rowIndex, 0], data.iloc[rowIndex, 1], data.iloc[rowIndex, 2]
                    name = str (name).strip ()
                    if (len (name ) == 0) :
                        continue
                                        
                    rid = str (rid).strip ()
                    pid = str (pid).strip ()
                    if (rid not in self._RadLexDictionary) :
                        self._RadLexDictionary[rid] = {"NameList" : [name], "Parent" : pid, "ChildList" : []}
                        try :
                            test = str(data.iloc[rowIndex, 3]).strip().lower()
                            if test == "none" :
                                self._RadLexDictionary[rid]["ROIDefType"] = "category"
                            elif test == "point" :
                                self._RadLexDictionary[rid]["ROIDefType"] = "point"
                            else:
                                self._RadLexDictionary[rid]["ROIDefType"] = "area"
                        except:
                            self._RadLexDictionary[rid]["ROIDefType"] = "area"                        
                        try :
                            red = int (str(data.iloc[rowIndex, 4]).strip())
                            green = int (str(data.iloc[rowIndex, 5]).strip())
                            blue = int (str(data.iloc[rowIndex, 6]).strip())
                            self._RadLexDictionary[rid]["RGB"] = (red, green, blue)
                        except:
                            self._RadLexDictionary[rid]["RGB"]  = (0,0,0)                            
                        try :
                            mask = int (str (data.iloc[rowIndex, 8]).strip ())
                            self._RadLexDictionary[rid]["Mask"] = mask
                        except:
                            self._RadLexDictionary[rid]["Mask"] = 1
                    else:
                        self._RadLexDictionary[rid]["NameList"].append (name)                    
                for key in self._RadLexDictionary.keys () :
                    parentID = self._RadLexDictionary[key]["Parent"]
                    if (parentID in self._RadLexDictionary) :
                        parentChildren = self._RadLexDictionary[parentID]["ChildList"]
                        if (key not in parentChildren) :
                            parentChildren.append (key)
                    elif (parentID != ":THING"):
                        #repair make child of root
                        self._RadLexDictionary[key]["Parent"] = "RID0"
                        if (key not in self._RadLexDictionary["RID0"]["ChildList"]) :
                            self._RadLexDictionary["RID0"]["ChildList"].append (key)  
                            
    def hasItem (self, index) :
        return index in self._RadLexDictionary
    
    def  __getitem__ (self, index) :        
        return self._RadLexDictionary[index]
    
    def items (self) :
        return self._RadLexDictionary.items ()
    
    def keys (self) :
        return self._RadLexDictionary.keys ()
    
    def values (self) :
        return self._RadLexDictionary.values ()
        
    def _AddNodeChildern (self, rid, FilteredDictionary):        
        childern = self._RadLexDictionary[rid]["ChildList"]
        for childRid in childern :
            if (childRid not in FilteredDictionary) :                
                FilteredDictionary[childRid] = copy.deepcopy (self._RadLexDictionary[childRid])
                self._AddNodeChildern (childRid, FilteredDictionary)
    
    def _validateTree (self) :
        """print ("")
        print ("Validating Radlex Tree")
        print ("")
        for key in self._RadLexDictionary.keys ():
            names, parent, childernlst = self._RadLexDictionary[key]
            if (parent != ":THING"):
                if parent not in self._RadLexDictionary :
                    print ("Parent RFID not found")
                    print (parent)
                else:
                    if (key not in self._RadLexDictionary[parent][2]):
                        print ("Not listed as child in parents: %s %s" % (key, parent))
            for crid in childernlst :
                if crid not in self._RadLexDictionary :
                    print ("Child RID not found")
                elif self._RadLexDictionary[crid][1] != key :
                    print ("Child does not point to parent") 
        print ("")
        print ("Done Validating Radlex Tree")
        print ("")"""
        return
        
    def filterDictionary (self, filterText):
        searchTxt = filterText.lower ()
        FilteredDictionary = {}
        for rid,val in self._RadLexDictionary.items () :            
            found = False
            if str(rid).lower ().startswith ("rid") and searchTxt in str(rid).lower () :
                found = True
            else:
                for name in val["NameList"] :
                    if searchTxt in name.lower () :                        
                        found = True
                        break
            if found :
                if (rid not in FilteredDictionary) :
                    newNode = copy.deepcopy (val)
                    newNode["ChildList"] = []
                    FilteredDictionary[rid] = newNode                                                    
                    self._AddNodeChildern (rid, FilteredDictionary)             
        keyList = list(FilteredDictionary.keys ())
        for key in keyList :
            parentID = FilteredDictionary[key]["Parent"]
            if (parentID in FilteredDictionary) :                
                parentChildren = FilteredDictionary[parentID]["ChildList"]
                if (key not in parentChildren) :
                    parentChildren.append (key)                        
            else :
                rid = key                
                while parentID not in FilteredDictionary and parentID in self._RadLexDictionary : 
                    newNode = copy.deepcopy (self._RadLexDictionary[parentID])
                    newNode["ChildList"] = [rid]
                    FilteredDictionary[parentID] = newNode
                    rid = parentID
                    parentID = newNode["Parent"]
                if parentID in FilteredDictionary :
                    if rid not in FilteredDictionary[parentID]["ChildList"] :
                        FilteredDictionary[parentID]["ChildList"].append (rid)                
        return rcc_radlex (self._homePath, self._basePythonPath, RadLexDictionary = FilteredDictionary)
    
    def find (self, searchText) :
        lowerSearchTxt = searchText.lower ()
        for key in self._RadLexDictionary.keys () :
            nameList, pid, childerenLst = self._RadLexDictionary[key]
            for name in nameList :
                txt = name.lower ()
                if (lowerSearchTxt in txt) :
                    print ("Found in: " + name + " (%s)" %key)
                    break
    
    def climbTree (self, child) :
        while child in self._RadLexDictionary :
            print (self.getRIDName (child)[0] + " (%s)" % child)
            child = self._RadLexDictionary[child]["Parent"]
    
    def getRIDChildren (self, RID):
            return self._RadLexDictionary[RID]["ChildList"]
        
    def getRIDParent (self, RID):
            return self._RadLexDictionary[RID]["Parent"]
        
    def getRIDName (self, RID) :
            return self._RadLexDictionary[RID]["NameList"]
    
    def printRIDChildrenName (self, RID) :
        childrenLst = self.getRIDChildren (RID) 
        NameLst = []
        for childRID in childrenLst :
            Name = self.getRIDName (childRID)
            NameLst.append (Name[0] + " (%s)" % childRID)
        print ("   " + ", ".join (NameLst))
    
    @staticmethod
    def getRootRID () :
        return "RID1"


class RadLexUITree  (QtCore.QAbstractItemModel):                     
        def __init__(self, projectDataset, parent, *args):                    
            QtCore.QAbstractItemModel.__init__(self, parent, *args)                  
            self._FilterText = None
            self._RadLexTree = {}
            self._OrigionalRadLexTree = rcc_radlex (projectDataset.getHomeFilePath (), projectDataset.getPythonProjectBasePath (), projectDataset)            
            self._rootMemory = []
            self.setFilter ("") 
        
        def getOrigionalRadLexTree (self) :
            return self._OrigionalRadLexTree
        
        def setFilter (self, filtertxt):
            filtertxt = filtertxt.lower().strip ()
            if (self._FilterText != filtertxt) :
                self.beginResetModel()  
                
                self._FilterText = filtertxt
                #self._oldTree = self._RadLexTree
                if len (filtertxt) == 0 :
                    self._RadLexTree = self._OrigionalRadLexTree
                else:
                    self._RadLexTree = self._OrigionalRadLexTree.filterDictionary (filtertxt)                            
                self._initRootMemory ()
                
                self.endResetModel () 
           
        
        def _initRootMemory (self) :
            self._rootMemory = []
            for rid, value in self._RadLexTree.items ():
                parentID = value["Parent"]
                if parentID not in self._RadLexTree.keys() :
                    self._rootMemory.append (rid)
            
        def index (self, row, column, parent):
            if not parent.isValid():            
                return self.createIndex(row, column, self._rootMemory[row])            
            else :
                parent = parent.internalPointer()                
                return self.createIndex(row, column, self._RadLexTree[parent]["ChildList"][row])                
                
        def data(self, index, role):
            if not index.isValid():
                return None
            index = index.internalPointer()
            namelst = self._RadLexTree[index]["NameList"]                
            if role == QtCore.Qt.DisplayRole :                                              
                if (str(index).lower().startswith ("rid")):
                    return namelst[0] + "   [" + str (index) + "]"
                else:
                    return namelst[0] 
            elif role == QtCore.Qt.FontRole :
                font = QtGui.QFont ()
                if  len (self._FilterText) > 0 :
                    for name in namelst :
                        if self._FilterText in name.lower () :
                            font.setBold (True)
                            return  font                            
                font.setBold (False)
                return  font
            else:            
                return None
              
        def hasIndex (self, row, column, parent) :
            if (row < 0) :
                return False
            if not parent.isValid():
                return row < len (self._rootMemory)            
            parent = parent.internalPointer()
            childlst = self._RadLexTree[parent]["ChildList"]                            
            count = len (childlst)
            return (row < count)         
        
        def rowCount(self, parent):
          if not parent.isValid():
               return len (self._rootMemory)
          else:
              parent = parent.internalPointer()
              childlst = self._RadLexTree[parent]["ChildList"]                            
              return len (childlst)              
              
        def parent (self, index) :
            if not index.isValid():
                return QModelIndex()
            index = index.internalPointer()
            node = self._RadLexTree[index]            
            pid = node["Parent"]
            if (pid not in self._RadLexTree.keys ()) :
                return QModelIndex()
            else:
                if pid in self._rootMemory :
                    parentRow = self._rootMemory.index(pid)
                else:
                    ppid = self._RadLexTree[pid]["Parent"]
                    parentRow = self._RadLexTree[ppid]["ChildList"].index(pid)
                return self.createIndex(parentRow, 0, pid)                
            
        def flags (self, index) :
            try:
                rid = index.internalPointer ()        
                childlst = self._RadLexTree[rid]["ChildList"]              
                
                if (self._RadLexTree[rid]["ROIDefType"].lower () == "area" or self._RadLexTree[rid]["ROIDefType"].lower () == "point") :
                    itemisSelectable = QtCore.Qt.ItemIsSelectable
                else:
                    return QtCore.Qt.NoItemFlags    
                if (len (childlst) == 0) :
                    return  QtCore.Qt.ItemIsEnabled |  itemisSelectable | QtCore.Qt.ItemNeverHasChildren
                else:
                    return  QtCore.Qt.ItemIsEnabled |  itemisSelectable
                 
            except:
                return  QtCore.Qt.NoItemFlags  
        
        
        def headerData (self, section, orientation, role):
            if role != QtCore.Qt.DisplayRole  :
                return(None)
            return str ("RadLex Classification: " + self._OrigionalRadLexTree.getTemplateType())
        
        def columnCount(self, parent):
            return 1